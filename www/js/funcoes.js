/**
 * 2015 - Jonathan Rodrigues
 */

function setMask() {

    jQuery('.telefone').focusout(function () {
        var phone, element;
        element = jQuery(this);
        element.unmask();
        phone = element.val().replace(/\D/g, '');
        if (phone.length > 10) {
            element.mask("(99) 99999-999?9");
        } else {
            element.mask("(99) 9999-9999?9");
        }
    }).trigger('focusout');
    jQuery(".cpf").mask("999.999.999-99");
    jQuery(".cpf2").mask("999.999.999-99");
    jQuery(".cnpj").mask("99.999.999/9999-99");
    jQuery(".cnpj2").mask("99.999.999/9999-99");
    jQuery(".cep").mask("99999-999");
    jQuery(".data").mask("99/99/9999");
    jQuery(".hora").mask("99:99");
    jQuery(".ano").mask("9999");
}

jQuery(setMask);

function unsetMask() {

    jQuery(".cep").unmask();
    jQuery(".telefone").unmask();
    jQuery(".cpf").unmask();
    jQuery(".cnpj").unmask();
}

jQuery(document).on('change', '.btn-file :file', function () {

    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');

    var input = $(this).parents('.input-group').find(':text'),
        log = numFiles > 1 ? numFiles + ' arquivos selecionados' : label;

    if (input.length) {
        input.val(log);
    } else {
        if (log) alert(log);
    }

});

jQuery(document).ready(function () {

    jQuery(".validar_formulario").validate();

    jQuery(".validar_formulario_com_senha").validate({

        rules: {
            conf_senha: {
                equalTo: "#senha"
            }
        }
    });

    jQuery(".treeview").treed();

    jQuery(".fancybox-thumbs").fancybox({

        prevEffect: 'none',
        nextEffect: 'none',
        helpers: {
            thumbs: {
                width: 50,
                height: 50
            }
        }
    });

    jQuery('.fancybox-media').fancybox({

        openEffect: 'none',
        closeEffect: 'none',
        helpers: {
            media: {}
        }
    });
});

jQuery(function () {

    jQuery(function () {
        jQuery('[data-toggle="tooltip"]').tooltip()
    });

    jQuery("#tipoLoginCpf").on("click", function () {

        jQuery(unsetMask);
        jQuery("input").removeClass("error");
        jQuery("label.error").hide();

        jQuery("#label_login").html("CPF");
        jQuery("#label_login").attr("for", "loginCPF")
        jQuery("#loginCPF").addClass("cpf required show").removeClass("hidden");
        jQuery("#loginCNPJ").removeClass("cnpj required show").addClass("hidden");
        jQuery(setMask);

    });

    jQuery("#tipoLoginCnpj").on("click", function () {

        jQuery(unsetMask);
        jQuery("input").removeClass("error");
        jQuery("label.error").hide();

        jQuery("#label_login").html("CNPJ");
        jQuery("#label_login").attr("for", "loginCNPJ")
        jQuery("#loginCNPJ").addClass("cnpj required show").removeClass("hidden");
        jQuery("#loginCPF").removeClass("cpf required show").addClass("hidden");
        jQuery(setMask);

    });

    jQuery("#recSenhaCpf").on("click", function () {

        jQuery(unsetMask);
        jQuery("input").removeClass("error");
        jQuery("label.error").hide();

        jQuery("#label_rec_senha").html("CPF");
        jQuery("#label_rec_senha").attr("for", "recSenhaCPF")
        jQuery("#recSenhaCPF").addClass("cpf required show").removeClass("hidden");
        jQuery("#recSenhaCNPJ").removeClass("cnpj required show").addClass("hidden");
        jQuery(setMask);

    });

    jQuery("#recSenhaCnpj").on("click", function () {

        jQuery(unsetMask);
        jQuery("input").removeClass("error");
        jQuery("label.error").hide();

        jQuery("#label_rec_senha").html("CNPJ");
        jQuery("#label_rec_senha").attr("for", "recSenhaCNPJ")
        jQuery("#recSenhaCNPJ").addClass("cnpj required show").removeClass("hidden");
        jQuery("#recSenhaCPF").removeClass("cpf required show").addClass("hidden");
        jQuery(setMask);

    });

    jQuery("#tipoF").on("click", function () {

        jQuery(unsetMask);
        jQuery("input").removeClass("error");
        jQuery("label.error").hide();

        jQuery(".label_nome").html("Nome");
        jQuery(".for_fis").removeClass("hidden").addClass("show");
        jQuery(".for_jur").removeClass("show").addClass("hidden");
        jQuery("#cpf").addClass("cpf required");
        jQuery("#cnpj").removeClass("cnpj required");
        jQuery(setMask);

    });

    jQuery("#tipoJ").on("click", function () {

        jQuery(unsetMask);
        jQuery("input").removeClass("error");
        jQuery("label.error").hide();

        jQuery(".label_nome").html("Raz&atilde;o Social");
        jQuery(".for_fis").removeClass("show").addClass("hidden");
        jQuery(".for_jur").removeClass("hidden").addClass("show");
        jQuery("#cpf").removeClass("cpf required");
        jQuery("#cnpj").addClass("cnpj required");
        jQuery(setMask);

    });

    jQuery("#fornecedor_F").on("click", function () {

        jQuery(unsetMask);
        jQuery("input").removeClass("error");
        jQuery("select").removeClass("error");
        jQuery("label.error").hide();

        jQuery(".label_razao").html("Nome");
        jQuery("#razao_social").attr("placeholder", "Informe seu nome...");
        jQuery(".for_fis").removeClass("hidden").addClass("show");
        jQuery(".for_jur").removeClass("show").addClass("hidden");
        if (!jQuery("#cpf").hasClass("cpf2"))
            jQuery("#cpf").addClass("cpf required").attr('required', true);
        if (!jQuery("#cnpj").hasClass("cnpj2"))
            jQuery("#cnpj").removeClass("cnpj required").removeAttr("required");
        jQuery("#inscricao_estadual").removeClass("required").removeAttr("required");
        jQuery("#data_inicio_atividade").removeClass("required").removeAttr("required");
        jQuery("#data_ultimo_registro_contrato_social").removeClass("required").removeAttr("required");
        jQuery("#numero_ultimo_registro_contrato_social").removeClass("required").removeAttr("required");
        jQuery("#cpf_responsavel").removeClass("cpf");
        jQuery("#id_porte_empresarial").removeClass("required").removeAttr("required");
        jQuery("#id_tipo_natureza_juridica").removeClass("required").removeAttr("required");
        jQuery("#id_natureza_juridica").removeClass("required").removeAttr("required");
        jQuery("#responsavel").removeClass("required").removeAttr("required");
        jQuery("#endereco_responsavel").removeClass("required").removeAttr("required");
        jQuery("#bairro_responsavel").removeClass("required").removeAttr("required");
        jQuery("#cep_responsavel").removeClass("required").removeAttr("required");
        jQuery("#id_estado_responsavel").removeClass("required").removeAttr("required");
        jQuery("#id_municipio_responsavel").removeClass("required").removeAttr("required");
        jQuery(setMask);

    });

    jQuery("#fornecedor_J").on("click", function () {

        jQuery(unsetMask);
        jQuery("input").removeClass("error");
        jQuery("select").removeClass("error");
        jQuery("label.error").hide();

        jQuery(".label_razao").html("Raz&atilde;o Social");
        jQuery("#razao_social").attr("placeholder", "Informe sua raz\u00E3o social...");
        jQuery(".for_fis").removeClass("show").addClass("hidden");
        jQuery(".for_jur").removeClass("hidden").addClass("show");
        if (!jQuery("#cpf").hasClass("cpf2"))
            jQuery("#cpf").removeClass("cpf required").removeAttr("required");
        if (!jQuery("#cnpj").hasClass("cnpj2"))
            jQuery("#cnpj").addClass("cnpj required").attr('required', true);
        jQuery("#inscricao_estadual").addClass("required").attr('required', true);
        jQuery("#data_inicio_atividade").addClass("required").attr('required', true);
        jQuery("#data_ultimo_registro_contrato_social").addClass("required").attr('required', true);
        jQuery("#numero_ultimo_registro_contrato_social").addClass("required").attr('required', true);
        jQuery("#cpf_responsavel").addClass("cpf");
        jQuery("#id_porte_empresarial").addClass("required").attr('required', true);
        jQuery("#id_tipo_natureza_juridica").addClass("required").attr('required', true);
        jQuery("#id_natureza_juridica").addClass("required").attr('required', true);
        jQuery("#responsavel").addClass("required").attr('required', true);
        jQuery("#endereco_responsavel").addClass("required").attr('required', true);
        jQuery("#bairro_responsavel").addClass("required").attr('required', true);
        jQuery("#cep_responsavel").addClass("required").attr('required', true);
        jQuery("#id_estado_responsavel").addClass("required").attr('required', true);
        jQuery("#id_municipio_responsavel").addClass("required").attr('required', true);
        jQuery(setMask);

    });

    jQuery("#id_estado").change(function () {

        if (jQuery(this).val()) {
            jQuery("#id_municipio").hide();
            jQuery(".carregando").removeClass("hidden");

            jQuery.getJSON("ajax/municipios.php?search=", {id_estado: jQuery(this).val(), ajax: 'true'}, function (j) {

                var options = "<option value=''>&raquo;&nbsp;Selecione o munic&iacute;pio</option>";

                if (j != null) {

                    for (var i = 0; i < j.length; i++) {
                        options += "<option value='" + j[i].id_municipio + "'>" + j[i].nome + "</option>";
                    }

                }

                jQuery("#id_municipio").html(options).show();
                jQuery(".carregando").addClass("hidden");
            });
        } else {
            jQuery("#id_municipio").html("<option value=''>-- Escolha um estado --</option>");
        }
    });

    jQuery("#id_instituicao").change(function () {

        if (jQuery(this).val()) {
            jQuery("#id_tipo_mensagem").hide();
            jQuery(".carregando_tipo").removeClass("hidden");

            jQuery.getJSON("ajax/tipo_mensagem.php?search=", {
                id_instituicao: jQuery(this).val(),
                ajax: 'true'
            }, function (j) {

                var options = "<option value=''>&raquo;&nbsp;Selecione o tipo de mensagem</option>";

                if (j != null) {

                    for (var i = 0; i < j.length; i++) {
                        options += "<option value='" + j[i].id_tipo_mensagem + "'>" + j[i].descricao + "</option>";
                    }

                }

                jQuery("#id_tipo_mensagem").html(options).show();
                jQuery(".carregando_tipo").addClass("hidden");
            });
        } else {
            jQuery("#id_tipo_mensagem").html("<option value=''>-- Escolha uma institui&ccedil;&atilde;o --</option>");
        }
    });

    jQuery("#id_tipo_mensagem").change(function () {

        if (jQuery(this).val()) {
            jQuery("#id_assunto").hide();
            jQuery(".carregando_assunto").removeClass("hidden");

            jQuery.getJSON("ajax/assunto.php?search=", {
                id_tipo_mensagem: jQuery(this).val(),
                ajax: 'true'
            }, function (j) {

                var options = "<option value=''>&raquo;&nbsp;Selecione o assunto</option>";

                if (j != null) {

                    for (var i = 0; i < j.length; i++) {
                        options += "<option value='" + j[i].id_assunto + "'>" + j[i].descricao + "</option>";
                    }

                }

                jQuery("#id_assunto").html(options).show();
                jQuery(".carregando_assunto").addClass("hidden");
            });
        } else {
            jQuery("#id_assunto").html("<option value=''>-- Escolha um tipo de mensagem --</option>");
        }
    });

    jQuery("#id_ano").change(function () {

        if (jQuery(this).val()) {
            jQuery("#id_categoria").hide();
            jQuery(".carregando_categoria").removeClass("hidden");

            jQuery.getJSON("ajax/diario_categoria.php?search=", {
                id_ano: jQuery(this).val(),
                ajax: 'true'
            }, function (j) {

                var options = "<option value=''>&raquo;&nbsp;Selecione a categoria</option>";

                if (j != null) {

                    for (var i = 0; i < j.length; i++) {
                        options += "<option value='" + j[i].id_categoria + "'>" + j[i].descricao + "</option>";
                    }

                }

                jQuery("#id_categoria").html(options).show();
                jQuery(".carregando_categoria").addClass("hidden");
            });
        } else {
            jQuery("#id_categoria").html("<option value=''>-- Escolha um ano --</option>");
        }
    });

    jQuery("#id_categoria").change(function () {

        if (jQuery(this).val()) {
            jQuery("#id_subcategoria").hide();
            jQuery(".carregando_subcategoria").removeClass("hidden");

            jQuery.getJSON("ajax/diario_subcategoria.php?search=", {
                id_categoria: jQuery(this).val(),
                ajax: 'true'
            }, function (j) {

                var options = "<option value=''>&raquo;&nbsp;Selecione a subcategoria</option>";

                if (j != null) {

                    for (var i = 0; i < j.length; i++) {
                        options += "<option value='" + j[i].id_subcategoria + "'>" + j[i].descricao + "</option>";
                    }

                }

                jQuery("#id_subcategoria").html(options).show();
                jQuery(".carregando_subcategoria").addClass("hidden");

            });
        } else {
            jQuery("#id_subcategoria").html("<option value=''>-- Escolha uma categoria --</option>");
        }
    });

    jQuery("#id_tipo_natureza_juridica").change(function () {

        if (jQuery(this).val()) {
            jQuery("#id_natureza_juridica").hide();
            jQuery(".carregando_natureza").removeClass("hidden");

            jQuery.getJSON("ajax/natureza_juridica.php?search=", {
                id_tipo_natureza_juridica: jQuery(this).val(),
                ajax: 'true'
            }, function (j) {

                var options = "<option value=''>&raquo;&nbsp;Selecione a natureza jur&iacute;dica</option>";

                if (j != null) {

                    for (var i = 0; i < j.length; i++) {
                        options += "<option value='" + j[i].id_natureza_juridica + "'>" + j[i].descricao + "</option>";
                    }

                }

                jQuery("#id_natureza_juridica").html(options).show();
                jQuery(".carregando_natureza").addClass("hidden");

            });
        } else {
            jQuery("#id_natureza_juridica").html("<option value=''>-- Escolha uma tipo de natureza jur&iacute;dica --</option>");
        }
    });

    jQuery("#id_estado_responsavel").change(function () {

        if (jQuery(this).val()) {
            jQuery("#id_municipio_responsavel").hide();
            jQuery(".carregando_responsavel").removeClass("hidden");

            jQuery.getJSON("ajax/municipios.php?search=", {id_estado: jQuery(this).val(), ajax: 'true'}, function (j) {

                var options = "<option value=''>&raquo;&nbsp;Selecione o munic&iacute;pio do respons&aacute;vel</option>";

                if (j != null) {

                    for (var i = 0; i < j.length; i++) {
                        options += "<option value='" + j[i].id_municipio + "'>" + j[i].nome + "</option>";
                    }

                }

                jQuery("#id_municipio_responsavel").html(options).show();
                jQuery(".carregando_responsavel").addClass("hidden");
            });
        } else {
            jQuery("#id_municipio_responsavel").html("<option value=''>-- Escolha um estado --</option>");
        }
    });

    jQuery("#id_tipo_digitalizacao").change(function () {

        if (jQuery(this).val()) {
            jQuery("#id_categoria_digitalizacao").hide();
            jQuery(".carregando_categoria").removeClass("hidden");

            jQuery.getJSON("ajax/digitalizacao_categoria.php?search=", {
                id_tipo: jQuery(this).val(),
                id_cliente: jQuery("#id_cliente").val(),
                ajax: 'true'
            }, function (j) {

                var options = "<option value=''>&raquo;&nbsp;Selecione a categoria</option>";

                if (j != null) {

                    for (var i = 0; i < j.length; i++) {
                        options += "<option value='" + j[i].id_categoria + "'>" + j[i].descricao + "</option>";
                    }

                }

                jQuery("#id_categoria_digitalizacao").html(options).show();
                jQuery(".carregando_categoria").addClass("hidden");

            });
        } else {
            jQuery("#id_categoria_digitalizacao").html("<option value=''>-- Escolha um tipo --</option>");
        }
    });

    jQuery("#id_categoria_digitalizacao").change(function () {

        if (jQuery(this).val()) {
            jQuery("#id_subcategoria").hide();
            jQuery(".carregando_subcategoria").removeClass("hidden");

            jQuery.getJSON("ajax/digitalizacao_subcategoria.php?search=", {
                id_categoria: jQuery(this).val(),
                id_cliente: jQuery("#id_cliente").val(),
                ajax: 'true'
            }, function (j) {

                var options = "<option value=''>&raquo;&nbsp;Selecione a subcategoria</option>";

                if (j != null) {

                    for (var i = 0; i < j.length; i++) {
                        options += "<option value='" + j[i].id_subcategoria + "'>" + j[i].descricao + "</option>";
                    }

                }

                jQuery("#id_subcategoria").html(options).show();
                jQuery(".carregando_subcategoria").addClass("hidden");

            });
        } else {
            jQuery("#id_subcategoria").html("<option value=''>-- Escolha uma categoria --</option>");
        }
    });

    jQuery("#add_socio").on("click", function () {

        var html = '<div>' +

            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">Nome Completo</label>' +
            '<div class="col-sm-10">' +
            '<input type="text" name="nome_socio[]" class="form-control" placeholder="Informe o nome completo do s&oacute;cio...">' +
            '</div>' +
            '</div>' +

            '<div class="form-group">' +
            '<label class="col-sm-2 control-label">CPF</label>' +
            '<div class="col-sm-10">' +
            '<div class="input-group">' +
            '<input type="text" name="cpf_socio[]" class="form-control cpf" placeholder="Informe o CPF do s&oacute;cio...">' +
            '<span class="input-group-btn">' +
            '<button type="button" class="btn btn-danger" onclick="jQuery(this).parent().parent().parent().parent().parent().fadeOut(\'slow\', function(){jQuery(this).remove();})"><i class="glyphicon glyphicon-trash"></i></button>' +
            '</span>' +
            '</div>' +
            '</div>' +
            '</div>' +

            '</div>';

        jQuery(html).insertAfter("#mais_socio");
        setMask();

    });

    jQuery("#anonimo").on("change", function () {

        if (jQuery(this).is(':checked')) {

            jQuery('.esconde_anonimo').hide();

        } else {
            jQuery('.esconde_anonimo').show();
        }

    });


    jQuery("#id_categoria_contrato").change(function () {

        if (jQuery(this).val()) {
            jQuery("#id_subcategoria").hide();
            jQuery(".carregando_subcategoria").removeClass("hidden");

            jQuery.getJSON("ajax/contrato_subcategoria.php?search=", {
                id_categoria: jQuery(this).val(),
                ajax: 'true'
            }, function (j) {

                var options = "<option value=''>&raquo;&nbsp;Selecione a subcategoria</option>";

                if (j != null) {

                    for (var i = 0; i < j.length; i++) {
                        options += "<option value='" + j[i].id_subcategoria + "'>" + j[i].descricao + "</option>";
                    }

                }

                jQuery("#id_subcategoria").html(options).show();
                jQuery(".carregando_subcategoria").addClass("hidden");

            });
        } else {
            jQuery("#id_subcategoria").html("<option value=''>-- Escolha uma categoria --</option>");
        }
    });

});

var captchaCallback = function () {

    grecaptcha.render('recaptcha1', {'sitekey': '6Lda8QkTAAAAAGKKmk5CVIsZ7BFqC3JapMO7Iy6j'});
    grecaptcha.render('recaptcha2', {'sitekey': '6Lda8QkTAAAAAGKKmk5CVIsZ7BFqC3JapMO7Iy6j'});
    grecaptcha.render('recaptcha3', {'sitekey': '6Lda8QkTAAAAAGKKmk5CVIsZ7BFqC3JapMO7Iy6j'});
};

(function (i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r;
    i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
    a = s.createElement(o),
        m = s.getElementsByTagName(o)[0];
    a.async = 1;
    a.src = g;
    m.parentNode.insertBefore(a, m)
})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

ga('create', 'UA-66531202-1', 'auto');
ga('send', 'pageview');