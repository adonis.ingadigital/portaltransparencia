<?php
//Cabecalho para permitir o acesso externo a este arquivo
header('Access-Control-Allow-Origin: *');

header('Cache-Control: no-cache');
header('Content-type: application/xml; charset="utf-8"', true);

require_once "../../../privado/transparencia/conexao.php";

try {

	$conn = new PDO("mysql:host=$servidor;dbname=$database;charset=utf8", $usuario, $senha);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	$conteudo = array();

	$stTipoMensagem = $conn->prepare("SELECT * FROM ouvidoria_tipo_mensagem WHERE id_instituicao = :id_instituicao AND status_registro = :status_registro ORDER BY descricao");
	$stTipoMensagem->execute(array("id_instituicao" => secure($_REQUEST['id_instituicao']), "status_registro" => "A"));
	$qryTipoMensagem = $stTipoMensagem->fetchAll();

	if(count($qryTipoMensagem)) {

		foreach ($qryTipoMensagem as $row) {

			$conteudo[] = array(
				'id_tipo_mensagem'	=> $row['id'],
				'descricao'			=> $row['descricao']
			);
		}
	}

	echo json_encode($conteudo);

	$conn = null;

} catch(PDOException $e) {

	echo "Erro ao conectar a $servidor: " . $e->getMessage();
}