<?php
//Cabecalho para permitir o acesso externo a este arquivo
header('Access-Control-Allow-Origin: *');

header('Cache-Control: no-cache');
header('Content-type: application/xml; charset="utf-8"', true);

require_once "../../../privado/transparencia/conexao.php";

try {

	$conn = new PDO("mysql:host=$servidor;dbname=$database;charset=utf8", $usuario, $senha);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	$cidades = array();

	$stMunicipio = $conn->prepare("SELECT * FROM municipio WHERE id_estado = :id_estado AND status_registro = :status_registro ORDER BY nome");
	$stMunicipio->execute(array("id_estado" => secure($_REQUEST['id_estado']), "status_registro" => "A"));
	$qryMunicipio = $stMunicipio->fetchAll();

	if(count($qryMunicipio)) {

		foreach ($qryMunicipio as $row) {

			$cidades[] = array(
				'id_municipio'	=> $row['id'],
				'nome'			=> $row['nome']
			);
		}
	}

	echo json_encode($cidades);

	$conn = null;

} catch(PDOException $e) {

	echo "Erro ao conectar a $servidor: " . $e->getMessage();
}