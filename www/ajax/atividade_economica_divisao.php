<?php
header('Access-Control-Allow-Origin: *');

header('Cache-Control: no-cache');
header('Content-type: text/html; charset="UTF-8"', true);

require_once "../../../privado/transparencia/conexao.php";

$id = $_REQUEST['secao'];

try {

    $conn = new PDO("mysql:host=$servidor;dbname=$database;charset=utf8", $usuario, $senha);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stDivisao = $conn->prepare("SELECT divisao,
                                    denominacao
                               FROM geral_atividade_economica
                              WHERE secao = :secao
                                AND divisao <> ''
                                AND grupo = ''
                                AND classe = ''
                                AND subclasse = ''
                                AND status_registro = :status_registro");

    $stDivisao->execute(array("secao" => $id, "status_registro" => "A"));
    $qryDivisao = $stDivisao->fetchAll();

    echo'<option value="">&raquo;&nbsp;Selecione</option>';
    if(count($qryDivisao)) {

        foreach ($qryDivisao as $divisao) {

            echo"<option value='$divisao[divisao]'>$divisao[divisao] - $divisao[denominacao]</option>";
            
        }
    }

} catch (PDOException $e){
    echo'<option value="">&raquo;&nbsp;houve um erro</option>';
    echo"<script>console.log('$e')</script>";
}
