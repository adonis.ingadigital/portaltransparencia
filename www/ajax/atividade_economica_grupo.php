<?php
header('Access-Control-Allow-Origin: *');

header('Cache-Control: no-cache');
header('Content-type: text/html; charset="UTF-8"', true);

require_once "../../../privado/transparencia/conexao.php";

$id = $_REQUEST['divisao'];

try {

    $conn = new PDO("mysql:host=$servidor;dbname=$database;charset=utf8", $usuario, $senha);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    $stGrupo = $conn->prepare("SELECT grupo,
                                      denominacao
                                 FROM geral_atividade_economica
                                WHERE divisao = :grupo
                                  AND grupo <> ''
                                  AND classe = ''
                                  AND subclasse = ''
                                  AND status_registro = :status_registro");
    
    $stGrupo->execute(array("grupo" => $id, "status_registro" => "A"));
    $qryGrupo = $stGrupo->fetchAll();
    
    echo'<option value="">&raquo;&nbsp;Selecione</option>';
    
    if(count($qryGrupo)) {
    
        foreach ($qryGrupo as $grupo) {
            echo"<option value='$grupo[grupo]'>$grupo[grupo] - $grupo[denominacao]</option>";
        }
    }
} catch (PDOException $e){
    echo'<option value="">&raquo;&nbsp;houve um erro</option>';
    echo"<script>console.log('$e')</script>";
}
