<?php
//Cabecalho para permitir o acesso externo a este arquivo
header('Access-Control-Allow-Origin: *');

header('Cache-Control: no-cache');
header('Content-type: application/xml; charset="utf-8"', true);

require_once "../../../privado/transparencia/conexao.php";

try {

	$conn = new PDO("mysql:host=$servidor;dbname=$database;charset=utf8", $usuario, $senha);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	$stmt = $conn->prepare("SELECT * FROM cliente_digitalizacao WHERE id_cliente = :id_cliente AND status_registro = :status_registro LIMIT 1");
	$stmt->execute(array("id_cliente" => $_REQUEST['id_cliente'], "status_registro" => "A"));

	$row = $stmt->fetch();

	$servidor = $row['servidor'];
	$database = $row['database'];
	$usuario = "root";
	$senha = "1ng@1nf11sql";

	try {

		$conn_dig = new PDO("mysql:host=$servidor;dbname=$database", $usuario, $senha);
		$conn_dig->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		$conteudo = array();

		$stCategoria = $conn_dig->prepare("SELECT * FROM documento_categoria WHERE id_tipo = :id_tipo AND status_registro = :status_registro ORDER BY descricao");
		$stCategoria->execute(array("id_tipo" => secure($_REQUEST['id_tipo']), "status_registro" => "A"));
		$qryCategoria = $stCategoria->fetchAll();

		if(count($qryCategoria)) {

			foreach ($qryCategoria as $row) {

				$conteudo[] = array(
					'id_categoria'	=> $row['id'],
					'descricao'			=> $row['descricao']
				);
			}
		}

		echo json_encode($conteudo);

		$conn_dig = null;

	} catch(PDOException $e) {

	    echo "Erro ao conectar a $servidor: " . $e->getMessage();
	}

	$conn = null;

} catch(PDOException $e) {

	echo "Erro ao conectar a $servidor: " . $e->getMessage();
}