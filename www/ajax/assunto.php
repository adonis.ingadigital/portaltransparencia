<?php
//Cabecalho para permitir o acesso externo a este arquivo
header('Access-Control-Allow-Origin: *');

header('Cache-Control: no-cache');
header('Content-type: application/xml; charset="utf-8"', true);

require_once "../../../privado/transparencia/conexao.php";

try {

	$conn = new PDO("mysql:host=$servidor;dbname=$database;charset=utf8", $usuario, $senha);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	$conteudo = array();

	$stAssunto = $conn->prepare("SELECT * FROM ouvidoria_assunto WHERE id_tipo_mensagem = :id_tipo_mensagem AND status_registro = :status_registro ORDER BY descricao");
	$stAssunto->execute(array("id_tipo_mensagem" => secure($_REQUEST['id_tipo_mensagem']), "status_registro" => "A"));
	$qryAssunto = $stAssunto->fetchAll();

	if(count($qryAssunto)) {

		foreach ($qryAssunto as $row) {

			$conteudo[] = array(
				'id_assunto'	=> $row['id'],
				'descricao'		=> $row['descricao']
			);
		}
	}

	echo json_encode($conteudo);

	$conn = null;

} catch(PDOException $e) {

	echo "Erro ao conectar a $servidor: " . $e->getMessage();
}