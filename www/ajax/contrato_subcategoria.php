<?php
header('Access-Control-Allow-Origin: *');

header('Cache-Control: no-cache');
header('Content-type: application/xml; charset="utf-8"', true);

require_once "../../../privado/transparencia/conexao.php";

try {

    $conn = new PDO("mysql:host=$servidor;dbname=$database;charset=utf8", $usuario, $senha);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $sql = "SELECT `contrato_transparencia`.`id_subcategoria`,
                   `contrato_transparencia_subcategoria`.`descricao`,
                   COUNT(`contrato_transparencia`.`id_subcategoria`) AS 'quantidade'
              FROM `contrato_transparencia`
              JOIN `contrato_transparencia_subcategoria`
                ON `contrato_transparencia`.`id_subcategoria` = `contrato_transparencia_subcategoria`.`id`
             WHERE `contrato_transparencia`.`status_registro` = :status_registro
               AND `contrato_transparencia`.`id_categoria` = :id_categoria
          GROUP BY `contrato_transparencia`.`id_subcategoria`
          ORDER BY `contrato_transparencia_subcategoria`.`descricao`";

    $conteudo = array();
    $stSubCategoria = $conn->prepare($sql);
    $stSubCategoria->execute(array("id_categoria" => secure($_REQUEST['id_categoria']), "status_registro" => "A"));
    $qrySubCategoria = $stSubCategoria->fetchAll();

    foreach ($qrySubCategoria as $indiceSubCategoria => $itemSubCategoria) {
        $conteudo[] = array(
            'id_subcategoria' => $itemSubCategoria['id_subcategoria'],
            'descricao' => $itemSubCategoria['descricao']
        );
    }

    echo json_encode($conteudo);

    unset($conn);

} catch (PDOException $e) {
    echo "Erro ao conectar o $servidor: " . $e->getMessage();
}