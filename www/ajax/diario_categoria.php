<?php
//Cabecalho para permitir o acesso externo a este arquivo
header('Access-Control-Allow-Origin: *');

header('Cache-Control: no-cache');
header('Content-type: application/xml; charset="utf-8"', true);

require_once "../../../privado/transparencia/conexao.php";

try {

	$conn = new PDO("mysql:host=$servidor;dbname=$database;charset=utf8", $usuario, $senha);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	$conteudo = array();

	$stCategoria = $conn->prepare("SELECT * FROM di_categoria WHERE id_ano = :id_ano AND status_registro = :status_registro ORDER BY categoria");
	$stCategoria->execute(array("id_ano" => secure($_REQUEST['id_ano']), "status_registro" => "A"));
	$qryCategoria = $stCategoria->fetchAll();

	if(count($qryCategoria)) {

		foreach ($qryCategoria as $row) {

			$conteudo[] = array(
				'id_categoria'	=> $row['id'],
				'descricao'		=> $row['categoria']
			);
		}
	}

	echo json_encode($conteudo);

	$conn = null;

} catch(PDOException $e) {

	echo "Erro ao conectar a $servidor: " . $e->getMessage();
}