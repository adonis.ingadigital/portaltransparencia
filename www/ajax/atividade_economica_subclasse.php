<?php
header('Access-Control-Allow-Origin: *');

header('Cache-Control: no-cache');
header('Content-type: text/html; charset="UTF-8"', true);

require_once "../../../privado/transparencia/conexao.php";

$id = $_REQUEST['classe'];

try {

    $conn = new PDO("mysql:host=$servidor;dbname=$database;charset=utf8", $usuario, $senha);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stSubclasse = $conn->prepare("SELECT subclasse,
                                        denominacao
                                   FROM geral_atividade_economica
                                  WHERE classe = :subclasse
                                    AND subclasse <> ''
                                    AND status_registro = :status_registro");
    
    $stSubclasse->execute(array("subclasse" => $id, "status_registro" => "A"));
    $qrySubclasse = $stSubclasse->fetchAll();
    
    echo'<option value="">&raquo;&nbsp;Selecione</option>';
    
    if(count($qrySubclasse)) {
        foreach ($qrySubclasse as $subclasse) {
            echo"<option value='$subclasse[subclasse]'>$subclasse[subclasse] - $subclasse[denominacao]</option>";
        }
    }
} catch (PDOException $e){
    echo'<option value="">&raquo;&nbsp;houve um erro</option>';
    echo"<script>console.log('$e')</script>";
}
