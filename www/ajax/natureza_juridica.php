<?php
//Cabecalho para permitir o acesso externo a este arquivo
header('Access-Control-Allow-Origin: *');

header('Cache-Control: no-cache');
header('Content-type: application/xml; charset="utf-8"', true);

require_once "../../../privado/transparencia/conexao.php";

try {

	$conn = new PDO("mysql:host=$servidor;dbname=$database;charset=utf8", $usuario, $senha);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	$conteudo = array();

	$stNatureza = $conn->prepare("SELECT * FROM geral_natureza_juridica WHERE id_tipo_natureza_juridica = :id_tipo_natureza_juridica AND status_registro = :status_registro ORDER BY id");
	$stNatureza->execute(array("id_tipo_natureza_juridica" => secure($_REQUEST['id_tipo_natureza_juridica']), "status_registro" => "A"));
	$qryNatureza = $stNatureza->fetchAll();

	if(count($qryNatureza)) {

		foreach ($qryNatureza as $row) {

			$conteudo[] = array(
				'id_natureza_juridica'	=> $row['id'],
				'descricao'				=> $row['codigo'] . " - " . $row['denominacao']
			);
		}
	}

	echo json_encode($conteudo);

	$conn = null;

} catch(PDOException $e) {

	echo "Erro ao conectar a $servidor: " . $e->getMessage();
}