<?php
header('Access-Control-Allow-Origin: *');

header('Cache-Control: no-cache');
header('Content-type: text/html; charset="UTF-8"', true);

require_once "../../../privado/transparencia/conexao.php";

$id = $_REQUEST['subclasse'];

try {

    $conn = new PDO("mysql:host=$servidor;dbname=$database;charset=utf8", $usuario, $senha);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stAtividade = $conn->prepare("SELECT id,
                                        denominacao
                                   FROM geral_atividade_economica
                                  WHERE subclasse = :subclasse
                                    AND status_registro = :status_registro");

    $stAtividade->execute(array("subclasse" => $id, "status_registro" => "A"));
    $qryAtividade = $stAtividade->fetchAll();

    echo'<option value="">&raquo;&nbsp;Selecione</option>';

    if(count($qryAtividade)) {

        foreach ($qryAtividade as $atividade) {
            echo"<option value='$atividade[id]'>$atividade[denominacao]</option>";
        }
    }
} catch (PDOException $e){
    echo'<option value="">&raquo;&nbsp;houve um erro</option>';
    echo"<script>console.log('$e')</script>";
}
