<?php
header('Access-Control-Allow-Origin: *');

header('Cache-Control: no-cache');
header('Content-type: text/html; charset="UTF-8"', true);

require_once "../../../privado/transparencia/conexao.php";

$id = $_REQUEST['grupo'];

try {

    $conn = new PDO("mysql:host=$servidor;dbname=$database;charset=utf8", $usuario, $senha);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stClasse = $conn->prepare("SELECT classe,
                                       denominacao
                                  FROM geral_atividade_economica
                                 WHERE grupo = :grupo
                                   AND classe <> ''
                                   AND subclasse = ''
                                   AND status_registro = :status_registro");
    
    $stClasse->execute(array("grupo" => $id, "status_registro" => "A"));
    $qryClasse = $stClasse->fetchAll();
    
    echo'<option value="">&raquo;&nbsp;Selecione</option>';
    
    if(count($qryClasse)) {
    
        foreach ($qryClasse as $classe) {
            echo"<option value='$classe[classe]'>$classe[classe] - $classe[denominacao]</option>";
        }
    }
} catch (PDOException $e){
    echo'<option value="">&raquo;&nbsp;houve um erro</option>';
    echo"<script>console.log('$e')</script>";
}
