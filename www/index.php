<?php
	session_start();

	$cliente = "1150";


	include "../privado/conexao.php";


	try {
		

		$conn = new PDO("mysql:host=$servidor;dbname=$database;charset=utf8", $usuario, $senha);


		
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		if(!$cliente && substr($_GET['sessao'], -4, 2) != "cl")
			header("Location: $CAMINHO/?sessao=$sequencia" . $listaCliente . "$complemento&url=" . urlencode("http://".$_SERVER['SERVER_NAME']."".$_SERVER ['REQUEST_URI']));

		$stCliente = $conn->prepare("SELECT cliente.razao_social,
											cliente.nome_fantasia,
											cliente.site,
											cliente.id_municipio,
											cliente.logo,
											cliente_configuracao.*,
											estado.nome estado,
											estado.uf,
											municipio.nome municipio
									   FROM cliente
								  LEFT JOIN cliente_configuracao 
								         ON cliente_configuracao.id_cliente = cliente.id
								  LEFT JOIN municipio 
								         ON cliente_configuracao.id_municipio = municipio.id
								  LEFT JOIN estado 
								         ON municipio.id_estado = estado.id
									  WHERE cliente.id = :id_cliente
										AND cliente_configuracao.status_registro = :status_registro");

		$stCliente->execute(array("id_cliente" => $cliente, "status_registro" => "A"));

		$dadosCliente = $stCliente->fetch();

		$stResponsavel = $conn->prepare("SELECT servidor_responsavel
											FROM cliente_configuracao_transparencia
										WHERE id_cliente = :id_cliente
											AND status_registro = :status_registro
										ORDER BY id LIMIT 1");

		$stResponsavel->execute(array("id_cliente" => $cliente, "status_registro" => "A"));

		$nomeResponsavel = $stResponsavel->fetch();



		$stConf = $conn->prepare("SELECT *
									FROM cliente_configuracao_transparencia
								   WHERE id_cliente = :id_cliente
									 AND status_registro = :status_registro
								ORDER BY id 
								   LIMIT 1");

		$stConf->execute(array("id_cliente" => $cliente, "status_registro" => "A"));

		$configuracaoTransparencia = $stConf->fetch();


		$stSistema = $conn->prepare("SELECT id_sistema 
									   FROM controle_sistema_cliente 
									  WHERE id_cliente = :id_cliente 
									    AND id_sistema 
										 IN (3,4)");
		$stSistema->execute(array("id_cliente" => $cliente));
		$sistemaCliente = $stSistema->fetch();
		$outroSistema = $sistemaCliente['id_sistema'] == "3" ? "4" : "3";

		$stOutroCliente = $conn->prepare("SELECT id
											FROM cliente
										   WHERE id_municipio = :id_municipio
											 AND id <> :id_cliente
											 AND id 
											  IN (
										  SELECT id_cliente 
										    FROM controle_sistema_cliente 
										   WHERE id_sistema = :outro_sistema)
											 AND status_registro = :status_registro
										ORDER BY id 
										   LIMIT 1");

		$stOutroCliente->execute(array("id_municipio" => $dadosCliente["id_municipio"], "id_cliente" => $cliente, "outro_sistema" => $outroSistema, "status_registro" => "A"));
		$outroCliente = $stOutroCliente->fetch();

		if(empty($configuracaoTransparencia['ordem_categoria'])) 
			$ordemCategoria = "ORDER BY id";
		else 
			$ordemCategoria = "ORDER BY $configuracaoTransparencia[ordem_categoria]";

		$stCategoriaMenu = $conn->prepare("SELECT * 
											 FROM cliente_configuracao_transparencia_icone_categoria 
											WHERE (id_cliente = 1 
											   OR id_cliente = :id_cliente) 
											  AND id 
											   IN (
										   SELECT id_categoria 
										     FROM cliente_configuracao_transparencia_icone_cliente 
											WHERE id_cliente = :id_cliente)
											  AND status_registro = :status_registro $ordemCategoria");

		$stCategoriaMenu->execute(array("id_cliente" => $cliente, "status_registro" => "A"));
		$buscaCategoriaMenu = $stCategoriaMenu->fetchAll();

		include "../privado/menu.php";
?>

<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<title><?=$dadosCliente['razao_social'] ?> - Portal da Transpar&ecirc;ncia</title>
		<meta charset="utf-8">
		<meta name="robots" content="noindex,nofollow">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
		<meta name="description" content="Portal da Transpar&ecirc;ncia do(a) <?=$dadosCliente['razao_social'] ?>">
		<meta name="keywords" content="portal da transparencia, transparencia municipal, prestacao de contas, <?=$dadosCliente['razao_social'] ?>">

		<link rel="shortcut icon" href="<?=$CAMINHOCANAL ?>/<?=$dadosCliente['logo'] ?>">
		<link rel="stylesheet" type="text/css" href="<?= $CAMINHO ?>/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="<?= $CAMINHO ?>/css/bootstrap-treeview.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="<?= $CAMINHO ?>/css/fancybox/jquery.fancybox.css" media="screen">
		<link rel="stylesheet" type="text/css" href="<?= $CAMINHO ?>/css/fancybox/helpers/jquery.fancybox-thumbs.css" media="screen">
		<link rel="stylesheet" type="text/css" href="<?= $CAMINHO ?>/css/style.css">

		<script type="text/javascript" src="<?= $CAMINHO ?>/js/jquery-1.11.3.min.js"></script>
		<script type="text/javascript" src="<?= $CAMINHO ?>/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?= $CAMINHO ?>/js/bootstrap-treeview.js"></script>
		<script type="text/javascript" src="<?= $CAMINHO ?>/js/jquery.validate.js"></script>
		<script type="text/javascript" src="<?= $CAMINHO ?>/js/fancybox/jquery.fancybox.js"></script>
		<script type="text/javascript" src="<?= $CAMINHO ?>/js/fancybox/jquery.fancybox-thumbs.js"></script>
		<script type="text/javascript" src="<?= $CAMINHO ?>/js/fancybox/jquery.fancybox-media.js"></script>
		<script type="text/javascript" src="<?= $CAMINHO ?>/js/maskedinput.js"></script>
		<script type="text/javascript" src="https://www.google.com/recaptcha/api.js?onload=captchaCallback&render=explicit" async defer></script>
		<script type="text/javascript" src="<?= $CAMINHO ?>/js/funcoes.js"></script>

	</head>

	<body>
		<div class="wrapper">
			<header class="navbar navbar-default">
				<div class="container-fluid">
					<div class="navbar-header">
						<!-- Este button irá gerar o botão do menu para resoluções pequenas automaticamente -->
						<button type="button"
								class="navbar-toggle colapsed"
								data-toggle="collapse"
								data-target="#menu">
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
						</button>
					
<?php 
		if($dadosCliente['logo'] != "") { 
?>
					
						<a href="<?=$CAMINHO ?>" class="navbar-brand">
							<img src="<?=$CAMINHOCANAL ?>/<?=$dadosCliente['logo'] ?>" alt="<?=$dadosCliente['razao_social'] ?>" />
						</a>
					
<?php 
		} 
?>

						<a href="<?=$CAMINHO ?>" class="navbar-text">
							<span class="titulo1">PORTAL DA TRANSPAR&Ecirc;NCIA</span><br><span class="titulo2"><?=$dadosCliente['razao_social'] ?></span>
						</a>

					</div>

					<nav class="collapse navbar-collapse" id="menu">
						<ul class="nav navbar-nav navbar-right">
						
<?php 
		if($cliente == "12133") { 
?>

							<li><a href="http://www.paranavai.pr.gov.br/" target="_blank"><i class="glyphicon glyphicon-home"></i> Prefeitura</a></li>
						
<?php 
		}
		if($cliente == "17") { 
?>

							<li><a href="http://www.controlemunicipal.com.br/inga/sistema/arquivos/17/decreto_156_pdf.pdf" target="_blank"><i class="glyphicon glyphicon-cog"></i> Regulamentação do Portal</a></li>
						
<?php 
		}
		if($cliente == "12101") { 
			?>
			
				<li><a href="http://www.paranavai.pr.gov.br/lai" target="_blank"><i class="glyphicon glyphicon-info-sign"></i> Acesso à Informação</a></li>
									
			<?php 
		}
		if($configuracaoTransparencia['exibir_sobre'] == "S") { 
?>

							<li><a href="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$sobre.$complemento); ?>"><i class="glyphicon glyphicon-book"></i> Sobre o Portal</a></li>
						
<?php 
		}
		if($configuracaoTransparencia['exibir_pergunta_frequente'] == "S") { 
?>

							<li><a href="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$perguntas.$complemento); ?>"><i class="glyphicon glyphicon-question-sign"></i> Perguntas Frequentes</a></li>
						
<?php 
		}
		if($configuracaoTransparencia['exibir_download'] == "S") { 
?>

							<li><a href="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$downloadLista.$complemento); ?>"><i class="glyphicon glyphicon-cloud-download"></i> Downloads</a></li>
						
<?php 
		} 
		if($configuracaoTransparencia['exibir_link_util'] == "S") { 
?>

							<li><a href="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$linkLista.$complemento); ?>"><i class="glyphicon glyphicon-globe"></i> Links &Uacute;teis</a></li>
						
<?php 
		}
		if($configuracaoTransparencia['exibir_fale_conosco'] == "S") {
			if($configuracaoTransparencia['fale_conosco_ouvidoria'] == "S") {
				if(count($confSistemaOuvidoria) > 0) {
?>

							<li><a href="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$sistemaGestaoOuvidoria.$complemento); ?>"><i class="glyphicon glyphicon-comment"></i> Fale Conosco</a></li>
						
<?php 
				} else { 
?>

							<li><a href="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$ouvidoria.$complemento); ?>"><i class="glyphicon glyphicon-comment"></i> Fale Conosco</a></li>
						
<?php
				}
			} else {
?>

							<li><a href="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$contato.$complemento); ?>"><i class="glyphicon glyphicon-comment"></i> Fale Conosco</a></li>
						
<?php 
			} 
		} 
		if($configuracaoTransparencia['exibir_acesso_informacao'] == "S") { 
?>

							<li><a href="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$acesso.$complemento); ?>"><i class="glyphicon glyphicon-info-sign"></i> Acesso a Informa&ccedil;&atilde;o</a></li>
						
<?php 
		} 
		if($_GET['sessao'] != "") {
			foreach ($link as $indice => $menu) {
?>

							<li><a href="<?= $link[$indice] ?>" target="<?= $target[$indice] ?>" class="hidden-lg"><i class="glyphicon glyphicon-triangle-right"></i> <?= $nomeMenu[$indice] ?></a></li>
						
<?php 
			} 
		} 
?>

						</ul>
					</nav>
				</div>
			</header>

			<div class="container-fluid">
				<main>
					<div class="visible-xs text-center busca_mobile">
						<form class="form-inline" action="" method="get">
							<div class="input-group">
								<input type="search" name="busca" class="form-control" value="<?= $_GET['busca'] ?>" placeholder="Pesquisar...">
								<input type="hidden" name="sessao" value="<?= verifica($sequencia.$buscaAvancada.$complemento); ?>" />
								<span class="input-group-btn">
									<button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-search"></i></button>
								</span>
							</div>
						</form>
					</div>

					<div class="visible-sm visible-md row busca_mobile">
						<div class="col-sm-6 text-right">
							<strong class="h3">BUSCA AVAN&Ccedil;ADA:</strong>
						</div>
						<div class="col-sm-6">
							<form class="form-inline" action="" method="get">
								<div class="input-group">
									<input type="search" name="busca" class="form-control" value="<?= $_GET['busca'] ?>" placeholder="Pesquisar...">
									<input type="hidden" name="sessao" value="<?= verifica($sequencia.$buscaAvancada.$complemento); ?>" />
									<span class="input-group-btn">
										<button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-search"></i></button>
									</span>
								</div>
							</form>
						</div>
					</div>

					<div class="visible-lg coluna_esquerda">

						<div class="busca_inicio">
							<h3>BUSCA AVAN&Ccedil;ADA</h3>
							<p>Fa&ccedil;a uma busca por Palavra-Chave e localize o conte&uacute;do desejado em qualquer m&oacute;dulo do portal e encontre &iacute;cones que deseja na lista.</p>

							<form class="form-inline" action="" method="get">
								<div class="input-group" style="width: 100%;">
									<input type="search" name="busca" class="form-control" value="<?= $_GET['busca'] ?>" placeholder="Pesquisar...">
									<input type="hidden" name="sessao" value="<?= verifica($sequencia.$buscaAvancada.$complemento); ?>" />
									<span class="input-group-btn">
										<button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-search"></i></button>
									</span>
								</div>
							</form>

						</div>

<?php 
		if(empty($_GET['sessao'])) { 
			if($configuracaoTransparencia['exibir_sobre'] == "S") { 
?>

						<h3 class="titulo_sobre"><?= !empty($configuracaoTransparencia['sobre_titulo']) ? $configuracaoTransparencia['sobre_titulo'] : "O Portal da Transpar&ecirc;ncia do Poder Executivo  Municipal &eacute; um instrumento de controle social  que possibilita ao cidad&atilde;o acompanhar a  arrecada&ccedil;&atilde;o das receitas e a aplica&ccedil;&atilde;o dos  recursos p&uacute;blicos." ?></h3>
						<div class="texto_sobre">
					
<?php
				if(!empty($configuracaoTransparencia['sobre_artigo']) && $configuracaoTransparencia['sobre_artigo'] != "<br>") {
					echo verifica($configuracaoTransparencia['sobre_artigo']);
				} else {

						echo "<p>O portal disponibiliza diariamente informa&ccedil;&otilde;es, obtidas do Sistema Integrado de Planejamento e Gest&atilde;o Fiscal (SIGEF), sobre as receitas, os gastos na manuten&ccedil;&atilde;o dos servi&ccedil;os p&uacute;blicos e os investimentos realizados.</p>";
						echo "<p>Nele &eacute; poss&iacute;vel obter informa&ccedil;&otilde;es detalhadas sobre os pagamentos efetuados aos fornecedores, pagamentos de di&aacute;rias aos servidores, conv&ecirc;nios e repasses aos munic&iacute;pios, subven&ccedil;&otilde;es sociais, al&eacute;m de outras informa&ccedil;&otilde;es de interesse da sociedade.</p>";
				}
?>
						</div>
					
<?php 
			} 
?>

						<div class="panel panel-primary">

							<div class="panel-heading">
								<h2 class="panel-title"><i class="glyphicon glyphicon-envelope"></i> Fale Conosco</h2>
							</div>
							<div class="panel-body">
								<address>
									<p><i class="glyphicon glyphicon-road"></i> <strong><?= $dadosCliente['endereco'] ?></strong><?= !empty($dadosCliente['complemento']) ? " - " . $dadosCliente['complemento'] : "" ?></p>
									<p><i class="glyphicon glyphicon-map-marker"></i> CEP: <?= $dadosCliente['cep'] ?> - <?= ucwords(strtolower($dadosCliente['municipio'])) ?> - <?= $dadosCliente['estado'] ?></p>
									<p><i class="glyphicon glyphicon-phone-alt"></i> <strong><?= $dadosCliente['telefone'] ?></strong></p>
									<?php $emailCliente = explode("@", $dadosCliente['email']); ?>
									<p><i class="glyphicon glyphicon-envelope"></i> <strong><?= $emailCliente[0] ?></strong>@<?= $emailCliente[1] ?></p>
									<p><i class="glyphicon glyphicon-user"></i> <strong><?= $nomeResponsavel['servidor_responsavel'] ?></p>

									
<?php 
			if($dadosCliente['site'] != "" && !filter_var($dadosCliente['site'], FILTER_VALIDATE_URL) === false) { 
?>

									<p><a href="<?= $dadosCliente['site'] ?>"><i class="glyphicon glyphicon-log-out"></i> Voltar ao Site</a></p>
									
<?php 
			} 
?>

								</address>
							</div>
						
<?php 
			if(!empty($dadosCliente['horario_atendimento'])) { 
?>

							<div class="panel-footer text-center">
								<p><strong>HOR&Aacute;RIO DE ATENDIMENTO</strong></p>
								<p><?= $dadosCliente['horario_atendimento'] ?></p>
							</div>
						
<?php 
			} 
			if(!empty($dadosCliente['horario_sessao'])) { 
?>

							<div class="panel-footer text-center">
								<p><strong>HOR&Aacute;RIO DA SESS&Atilde;O</strong></p>
								<p><?= $dadosCliente['horario_sessao'] ?></p>
							</div>
						
<?php 
			} 
?>

						</div>
					
<?php 
		} else { 
?>

						<p class="clearfix"></p>
					
<?php
				foreach ($buscaCategoriaMenu as $menuCategoria) {
?>

						<div class="list-group">
							<div class="list-group-item active">
								<i class="<?= $menuCategoria['icone'] ?>"></i> <?= $menuCategoria['descricao'] ?>
							</div>
						
<?php
					foreach ($link as $indice => $menu) {
						if($categoriaMenu[$indice] == $menuCategoria['id']) {
?>

							<a href="<?= $link[$indice] ?>" target="<?= $target[$indice] ?>" class="list-group-item">
								<i class="glyphicon glyphicon-triangle-right"></i> <?= $nomeMenu[$indice] ?>
							</a>
							
<?php 
						} 
					} 
?>

						</div>
					
<?php 
				} 
?>

						<div class="list-group">
						
<?php		foreach ($link as $indice => $menu) {
					if(empty($categoriaMenu[$indice])) { ?>

							<a href="<?= $link[$indice] ?>" target="<?= $target[$indice] ?>" class="list-group-item">
								<i class="glyphicon glyphicon-triangle-right"></i> <?= $nomeMenu[$indice] ?>
							</a>
							
					<?php } 
							} ?>

						</div>
					
					<?php } ?>

					</div>
					<div class="coluna_centro">
						<?php include "../privado/sequencia.php"; ?>
					</div>
					<p class="hidden-lg clearfix">&nbsp;</p>

					<div class="hidden-lg panel panel-primary">
						<div class="panel-heading">
							<h2 class="panel-title"><i class="glyphicon glyphicon-envelope"></i> Fale Conosco</h2>
						</div>
						<div class="panel-body">
							<address>
								<p><i class="glyphicon glyphicon-road"></i> <strong><?= $dadosCliente['endereco'] ?></strong><?= !empty($dadosCliente['complemento']) ? " - " . $dadosCliente['complemento'] : "" ?></p>
								<p><i class="glyphicon glyphicon-map-marker"></i> CEP: <?= $dadosCliente['cep'] ?> - <?= ucwords(strtolower($dadosCliente['municipio'])) ?> - <?= $dadosCliente['estado'] ?></p>
								<p><i class="glyphicon glyphicon-phone-alt"></i> <strong><?= $dadosCliente['telefone'] ?></strong></p>
								<?php $emailCliente = explode("@", $dadosCliente['email']); ?>
								<p><i class="glyphicon glyphicon-envelope"></i> <strong><?= $emailCliente[0] ?></strong>@<?= $emailCliente[1] ?></p>
								<p><i class="glyphicon glyphicon-user"></i> <strong><?= $nomeResponsavel['servidor_responsavel'] ?></p>
								
								<?php if($dadosCliente['site'] != "" && !filter_var($dadosCliente['site'], FILTER_VALIDATE_URL) === false) { ?>
								<p><a href="<?= $dadosCliente['site'] ?>"><i class="glyphicon glyphicon-log-out"></i> Voltar ao Site</a></p>
								<?php } ?>
							</address>
						</div>
					
						<?php	if(!empty($dadosCliente['horario_atendimento'])) { ?>
						<div class="panel-footer text-center">
							<p><strong>HOR&Aacute;RIO DE ATENDIMENTO</strong></p>
							<p><?= $dadosCliente['horario_atendimento'] ?></p>
						</div>
						<?php }
							if(!empty($dadosCliente['horario_sessao'])) { ?>

						<div class="panel-footer text-center">
							<p><strong>HOR&Aacute;RIO DA SESS&Atilde;O</strong></p>
							<p><?= $dadosCliente['horario_sessao'] ?></p>
						</div>
						<?php } ?>

					</div>
				</main>
			</div>
		</div>

		<footer class="col-xs-12 alinhamento" style="padding: 0;margin: 0;">
		
		<?php
				$stAtualizacao = $conn->prepare("(SELECT data_acesso
													FROM controle_log_acesso
													WHERE id_cliente = :id_cliente
												ORDER BY id DESC LIMIT 1)
													UNION ALL
													(SELECT data_acesso
													FROM pref_log_acesso
														WHERE id_cliente = :id_cliente
												ORDER BY id DESC LIMIT 1)
													UNION ALL
													(SELECT data_acesso
													FROM camara_log_acesso
														WHERE id_cliente = :id_cliente
												ORDER BY id DESC LIMIT 1)
												ORDER BY data_acesso DESC LIMIT 1");

				$stAtualizacao->execute(array("id_cliente" => $cliente));
				$confAtualizacao = $stAtualizacao->fetch();
		?>

			<div id="atualizacao" class="col-md-4 col-xs-12" style="margin-left: 0 !important">
				<p>
					<img src="<?= $CAMINHO ?>/images/ico_atualizacao.png" />&nbsp;&nbsp;
					<strong>&Uacute;ltima Atualiza&ccedil;&atilde;o:&nbsp;&nbsp;</strong>
					<?= formata_data_hora($confAtualizacao['data_acesso']) ?>
				</p>
			</div>
			<div class="col-md-6 col-xs-12 alinhamento">
			<? if($configuracaoTransparencia['servidor_responsavel'] != NULL){ ?>	
				<p class="" style="color:#fff; margin:0;padding: 17px 0px"><i class="fa fa-user" aria-hidden="true" style="color:#eebf30;font-size: 20px;"></i>  
					<span style="color:#eebf30; font-weight: 700">Servidor Respons&aacute;vel: </span>
					<?=$configuracaoTransparencia['servidor_responsavel']?>
				</p>
			<? } ?>
			</div>
			<div class="col-md-2 col-xs-12">
				<a href="http://ingadigital.com.br" target="_blank" data-toggle="tooltip" data-placement="top" title="Ing&aacute; Digital Ltda. &quot;O seu sistema onde voc&ecirc; estiver!&quot;" id="logo_inga"></a>
			</div>
		</footer>

		<div class="modal fade" id="ouvidoria_ubirata">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Ouvidoria</h4>
					</div>
					<div class="modal-body text-center">
						<a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$ouvidoria.$complemento); ?>&ouvidoria_geral">
							<img src="http://ubirata.pr.gov.br/images/ouvidoria_geral.png" alt="Ouvidoria Geral" />
						</a>
						<a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$ouvidoria.$complemento); ?>&ouvidoria_saude">
							<img src="http://ubirata.pr.gov.br/images/ouvidoria_saude.png" alt="Ouvidoria da Sa&uacute;de" />
						</a>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="diario_ubirata">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Di&aacute;rio Oficial</h4>
					</div>
					<div class="modal-body text-center">
						<a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$downloadLista.$complemento); ?>">
							<img src="<?= $CAMINHOCMGERAL ?>/images/public_anteriores.jpg" alt="Publica&ccedil;&otilde;es Anteriores" class="imagem btn-margin" />
						</a>
						<a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$transparenciaOnlineVisualiza.$complemento); ?>&origem=diario&redir=link">
							<img src="<?= $CAMINHOCMGERAL ?>/images/public_atuais.jpg" alt="Publica&ccedil;&otilde;es Atuais" class="imagem btn-margin" />
						</a>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>

<?php
	$conn = null;

	} catch(PDOException $e) {
		echo "Erro ao conectar a $servidor: " . $e->getMessage();
	}