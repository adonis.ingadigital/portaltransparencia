<?php
$cliente = !empty($_GET['id_cliente']) ? $_GET['id_cliente'] : $_COOKIE['id_cliente'];

include "../../privado/transparencia/conexao.php";

try {

    $conn = new PDO("mysql:host=$servidor;dbname=$database;charset=utf8", $usuario, $senha);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stCliente = $conn->prepare("SELECT cliente.razao_social,
									  	cliente.nome_fantasia,
									  	cliente.site,
									  	cliente.id_municipio,
									  	cliente.logo,
									  	cliente_configuracao.*,
									  	estado.nome estado,
										estado.uf,
									  	municipio.nome municipio
								   FROM cliente
							  LEFT JOIN cliente_configuracao ON cliente_configuracao.id_cliente = cliente.id
							  LEFT JOIN municipio ON cliente_configuracao.id_municipio = municipio.id
							  LEFT JOIN estado ON municipio.id_estado = estado.id
								  WHERE cliente.id = :id_cliente
								  	AND cliente_configuracao.status_registro = :status_registro");

    $stCliente->execute(array("id_cliente" => $cliente, "status_registro" => "A"));

    $dadosCliente = $stCliente->fetch();

?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title><?=$dadosCliente['razao_social'] ?> - Portal da Transpar&ecirc;ncia</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
    <meta name="description" content="Portal da Transpar&ecirc;ncia do(a) <?=$dadosCliente['razao_social'] ?>">
    <meta name="keywords" content="portal da transparencia, transparencia municipal, prestacao de contas, <?=$dadosCliente['razao_social'] ?>">

    <link rel="shortcut icon" href="<?=$CAMINHOCANAL ?>/<?=$dadosCliente['logo'] ?>">
    <link rel="stylesheet" type="text/css" href="<?= $CAMINHO ?>/css/bootstrap.min.css">

</head>

<body>
    <div class="container-fluid">
        <div class="text-center">
            <img src="<?=$CAMINHOCANAL ?>/<?=$dadosCliente['logo'] ?>" alt="<?=$dadosCliente['razao_social'] ?>" style="max-height: 200px;">
            <h2 class="text-primary" style="text-transform: uppercase;">A <?=$dadosCliente['razao_social'] ?> n&atilde;o adota este tipo de procedimento</h2>
            <h3>Para mais informa&ccedil;&otilde;es, acesse:</h3>
            <a href="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$acesso.$complemento); ?>" target="_parent">
                <img src="<?=$CAMINHO ?>/images/ico_acesso_informacao.png" alt="Acesso a Informa&ccedil;&atilde;o">
            </a>
            <br><br>
            <div class="col-md-offset-3 col-md-6 col-md-offset-3">
                <div class="panel panel-primary">

                    <div class="panel-heading text-left">
                        <h2 class="panel-title"><i class="glyphicon glyphicon-envelope"></i> Fale Conosco</h2>
                    </div>
                    <div class="panel-body text-left">
                        <address>
                            <p><i class="glyphicon glyphicon-road"></i> <strong><?= $dadosCliente['endereco'] ?></strong><?= !empty($dadosCliente['complemento']) ? " - " . $dadosCliente['complemento'] : "" ?></p>
                            <p><i class="glyphicon glyphicon-map-marker"></i> CEP: <?= $dadosCliente['cep'] ?> - <?= ucwords(strtolower($dadosCliente['municipio'])) ?> - <?= $dadosCliente['estado'] ?></p>
                            <p><i class="glyphicon glyphicon-phone-alt"></i> <strong><?= $dadosCliente['telefone'] ?></strong></p>
                            <?php $emailCliente = explode("@", $dadosCliente['email']); ?>
                            <p><i class="glyphicon glyphicon-envelope"></i> <strong><?= $emailCliente[0] ?></strong>@<?= $emailCliente[1] ?></p>
                        </address>
                    </div>
                    <?php if(!empty($dadosCliente['horario_atendimento'])) { ?>
                    <div class="panel-footer text-center">
                        <p><strong>HOR&Aacute;RIO DE ATENDIMENTO</strong></p>
                        <p><?= $dadosCliente['horario_atendimento'] ?></p>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
<?php
$conn = null;

} catch(PDOException $e) {

    echo "Erro ao conectar a $servidor: " . $e->getMessage();
}