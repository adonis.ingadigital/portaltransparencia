<?php
ini_set('memory_limit', '-1');

$decoded = base64_decode($_GET['data']);
$dados = explode(";", $decoded);

$servidor = $dados[0];
$database = $dados[1];
$cliente = $dados[2];
$id_documento = $dados[3];

$cliente = $_REQUEST['val'];


if($cliente == '111'){
  $conn = new PDO("mysql:host=$_GET[servidor];dbname=$_GET[database]", "root", "1ng@1nf11sql");

} else {
$conn = new PDO("mysql:host=$_GET[servidor];dbname=$_GET[database]", "root_externo", "1553bb7b4a8712761c334cb357de5fd2");
}


//$conexao = new PDO("mysql:host=$servidor;dbname=$database", "root", "1ng@1nf11sql");
//$conexao = new PDO("mysql:host=$servidor;dbname=$database", "root", "95fbc7198fcbe6731294f5f1731cdcde");
$conexao->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

require "lib/fpdf.php";
require "lib/fpdi.php";
require "lib/RmDirTree.php";

try {

    $sql = "SELECT imagem
              FROM documento_anexo
             WHERE id_documento = :id_documento 
               AND status_registro = :status_registro
          ORDER BY id";

    $parameter = array(
        "id_documento" => $id_documento,
        "status_registro" => 'A'
    );

    $stBusca = $conexao->prepare($sql);
    $stBusca->execute($parameter);
    $resultado = $stBusca->fetchAll(PDO::FETCH_ASSOC);

    unset($stBusca);

    $clientePath = "tmp/$cliente/";

    if (is_dir($clientePath))
        chmod($clientePath, 0777);

    RmDirTree($clientePath);

    mkdir($clientePath, 0777, true);
    chmod($clientePath, 0777);

    $pdf = new FPDI();

    foreach ($resultado as $i => $item) {

        $file = "{$clientePath}$i.pdf";

        file_put_contents($file, $item['imagem']);
        chmod($file, 0777);

        $pageNumber = $pdf->setSourceFile($file);

        $templateIndex = $pdf->importPage($pageNumber);
        $size = $pdf->getTemplateSize($templateIndex);

        $pdf->AddPage('P', array($size['w'], $size['h']));
        $pdf->useTemplate($templateIndex);

        $i++;
    }

    unset($resultado);

    $pdf->Output();

    RmDirTree($clientePath);

} catch (PDOException $e) {

    echo "Erro ao conectar a $_GET[servidor]: " . $e->getMessage();

}