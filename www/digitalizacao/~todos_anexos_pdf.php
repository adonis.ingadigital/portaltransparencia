<?php
ini_set('memory_limit', '-1');

$conexao = new PDO("mysql:host=$_GET[servidor];dbname=$_GET[database]", "root", "1553bb7b4a8712761c334cb357de5fd2");
$conexao->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$cliente = $_GET['cliente'];
$id_documento = isset($_GET['id_documento']) ? $_GET['id_documento'] : 0;

echo "Carregando...";

require "lib/ConcatPdf.php";
require "lib/RmDirTree.php";

try {

    $sql = "SELECT imagem
              FROM documento_anexo
             WHERE id_documento = :id_documento 
               AND status_registro = :status_registro
          ORDER BY id";

    $parameter = array(
        "id_documento" => $id_documento,
        "status_registro" => 'A'
    );

    $stBusca = $conexao->prepare($sql);
    $stBusca->execute($parameter);
    $resultado = $stBusca->fetchAll(PDO::FETCH_ASSOC);

    $arrayArquivos = array();

    $clientePath = "tmp/$cliente/";
    $pathTmp = "{$clientePath}tmp/";

    if (is_dir($clientePath))
        chmod($clientePath, 0777);

    if (is_dir($pathTmp))
        chmod($pathTmp, 0777);

    RmDirTree($pathTmp);
    RmDirTree($clientePath);

    mkdir($pathTmp, 0777, true);
    chmod($clientePath, 0777);
    chmod($pathTmp, 0777);

    foreach ($resultado as $i => $item) {
        file_put_contents("{$pathTmp}$i.pdf", $item['imagem']);
        array_push($arrayArquivos, "{$pathTmp}$i.pdf");
        $i++;
    }

    $pdf = new ConcatPdf();
    $pdf->setFiles($arrayArquivos);
    $pdf->concat();

    $pdf->Output("$pathTmp/final.pdf", 'I');

    RmDirTree($pathTmp);
    RmDirTree($clientePath);

} catch (PDOException $e) {

    echo "Erro ao conectar a $servidor: " . $e->getMessage();

}


