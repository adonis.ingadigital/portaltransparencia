<h2>Galeria de Obras</h2>
<ol class="breadcrumb">
	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
	<li class="active">Galeria de Obras</li>
</ol>

<?php
$stCategoria = $conn->prepare("SELECT * FROM pref_obras WHERE id_cliente = :id_cliente AND status_registro = :status_registro ORDER BY id DESC");
$stCategoria->execute(array("id_cliente" => $cliente, "status_registro" => "A"));
$qryCategoria = $stCategoria->fetchAll();
if(count($qryCategoria)) {
?>


<div class="panel-group">
	<?php
	foreach ($qryCategoria as $categoria) {

	?>

    <div class="panel panel-primary">
      <div class="panel-heading"><?= $categoria['obra'] ?></div>
      <div class="panel-body"><?= $categoria['descricao'] ?></div>
    </div>

	<?php } ?>
</div>
<?php
}

$atualizacao = atualizacao("quantitativo_cargos", $cliente, $conn);
if($atualizacao != "") {
?>
<p class="text-right"><small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small></p>
<?php
}