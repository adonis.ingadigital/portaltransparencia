<?php
$busca = secure($_REQUEST['busca']);

?>

<h2>BUSCA AVAN&Ccedil;ADA</h2>



<ol class="breadcrumb">

	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>

	<li class="active">Busca Avan&ccedil;ada</li>

</ol>



<?php

if(empty($busca)) {



	echo "<p>Nenhum termo informado para a pesquisa!</p>";



} else {


	$buscaValor = secure_busca($_REQUEST['busca']);
	
	$stIcones = $conn->prepare("SELECT cliente_configuracao_transparencia_icone.*, cliente_configuracao_transparencia_icone_cliente.id_categoria 
	
									FROM cliente_configuracao_transparencia_icone, cliente_configuracao_transparencia_icone_cliente
	
								 WHERE cliente_configuracao_transparencia_icone_cliente.id_icone = cliente_configuracao_transparencia_icone.id
	
								 AND cliente_configuracao_transparencia_icone_cliente.id_cliente = :id_cliente
	
								 AND cliente_configuracao_transparencia_icone.status_registro = :status_registro
								 AND cliente_configuracao_transparencia_icone.descricao LIKE :busca_icone 
								ORDER BY cliente_configuracao_transparencia_icone_cliente.ordem");
	
	
	
	$stIcones->execute(array("id_cliente" => $cliente, "status_registro" => "A", "busca_icone" => "%".$buscaValor."%"));
	
	$qryIcones = $stIcones->fetchAll();

	$link = [];
	
	
	if($qryIcones){
		foreach ($qryIcones as $indice => $menu) {
		
		
		
			$nomeMenu[$indice] = $menu['descricao'];
		
			$imagemMenu[$indice] = $menu['imagem'];
		
				$categoriaMenu[$indice] = $menu['id_categoria'];
		
		
		
			switch ($menu['id']) {
		
		
		
				case "1":
		
					$stLink = $conn->prepare("SELECT id FROM link_transparencia WHERE id_cliente = :id_cliente AND status_registro = :status_registro");
		
					$stLink->execute(array("id_cliente" => $cliente, "status_registro" => "A"));
		
					$confLink = $stLink->fetchAll();
		
		
		
					switch(count($confLink)) {
		
		
		
						case 0:
		
							$link[$indice] = "#";
		
							$target[$indice] = "_self";
		
							break;
		
						case 1:
		
							foreach ($confLink as $buscaLink) {
		
		
		
								$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&id=$buscaLink[id]&redir=link";
		
								$target[$indice] = "_self";
		
							}
		
							break;
		
						default:
		
							$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnline . "$complemento";
		
							$target[$indice] = "_self";
		
							break;
		
					}
		
					break;
		
				case "2":
		
					if($configuracaoTransparencia['demonstrativos_transparencia_online'] == "S") {
		
		
		
						$stLink = $conn->prepare("SELECT id FROM link_transparencia WHERE id_cliente = :id_cliente AND status_registro = :status_registro");
		
						$stLink->execute(array("id_cliente" => $cliente, "status_registro" => "A"));
		
						$confLink = $stLink->fetchAll();
		
		
		
						switch(count($confLink)) {
		
		
		
							case 0:
		
								$link[$indice] = "#";
		
								$target[$indice] = "_self";
		
								break;
		
							case 1:
		
								foreach ($confLink as $buscaLink) {
		
									$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&id=$buscaLink[id]&redir=link";
		
									$target[$indice] = "_self";
		
								}
		
								break;
		
							default:
		
								$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnline . "$complemento";
		
								$target[$indice] = "_self";
		
								break;
		
						}
		
		
		
		
		
					} else if(empty($configuracaoTransparencia['caminho_demonstrativos'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $gastossessao . "$complemento";
		
		
		
						if($configuracaoTransparencia['demonstrativo_outro_cliente'] == "S")
		
							$link[$indice] .= "&nc=" . $outroCliente['id'];
		
		
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=demonstrativos&redir=link";
		
		
		
					}
		
					$target[$indice] = "_self";
		
					break;
		
				case "3":
		
					if($cliente == "46") {
		
		
		
						$link[$indice] = '#" role="button" data-toggle="modal" data-target="#diario_ubirata';
		
		
		
					} else if($configuracaoTransparencia['diario_download'] == "S") {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $downloadLista . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						if(empty($configuracaoTransparencia['caminho_diario'])) {
		
		
		
							$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $diarioLista . "$complemento";
		
							$target[$indice] = "_self";
		
		
		
							if($configuracaoTransparencia['diario_outro_cliente'] == "S")
		
								$link[$indice] .= "&nc=" . $outroCliente['id'];
		
		
		
						} else {
		
		
		
							$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=diario&redir=link";
		
							$target[$indice] = "_self";
		
		
		
						}
		
					}
		
					break;
		
				case "4":
		
					if(!empty($configuracaoTransparencia['caminho_digitalizacao_licitacao']) && $configuracaoTransparencia['digitalizacao_licitacao'] == "N" && $cliente != '43') {
						
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $digitalizacaoLista . "$complemento&id_tipo=" . $configuracaoTransparencia['caminho_digitalizacao_licitacao'];
		
		
		
						if($configuracaoTransparencia['licitacao_outro_cliente'] == "S")
		
						$link[$indice] .= "&nc=" . $outroCliente['id'];
		
						
		
		
		
		
		
					} else if(empty($configuracaoTransparencia['caminho_licitacao'])) {
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $licitacaoModalidade . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
						if($configuracaoTransparencia['licitacao_outro_cliente'] == "S")
		
							$link[$indice] .= "&nc=" . $outroCliente['id'];
		
		
		
					} else {
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=licitacao&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "5":
		
					if(empty($configuracaoTransparencia['caminho_legislacao'])) {
		
		
		
						if(!empty($configuracaoTransparencia['caminho_digitalizacao_legislacao']) && $configuracaoTransparencia['digitalizacao_legislacao'] == "N") {
		
		
		
							$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $digitalizacaoLista . "$complemento&id_tipo=" . $configuracaoTransparencia['caminho_digitalizacao_legislacao'];
		
		
		
						} else if($configuracaoTransparencia['legislacao_diario'] == "S") {
		
		
		
							$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $diarioLista . "$complemento";
		
		
		
						} else {
		
		
		
							if($cliente == "46") {
		
		
		
								$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $legislacaoLista . "$complemento&tipo=1";
		
		
		
							} else {
		
		
		
								$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $legislacaoLista . "$complemento&ntipo=3,4,5,18,19,20,22,23,32";
		
		
		
							}
		
		
		
							if($configuracaoTransparencia['legislacao_outro_cliente'] == "S")
		
								$link[$indice] .= "&nc=" . $outroCliente['id'];
		
		
		
						}
		
		
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=legislacao&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "6":
		
					if(empty($configuracaoTransparencia['caminho_atos_normativos'])) {
		
		
		
						if(!empty($configuracaoTransparencia['caminho_digitalizacao_atos']) && $configuracaoTransparencia['digitalizacao_atos'] == "N") {
		
		
		
							$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $digitalizacaoLista . "$complemento&id_tipo=" . $configuracaoTransparencia['caminho_digitalizacao_atos'];
		
		
		
						} else if($configuracaoTransparencia['atos_legislacao'] == "S") {
		
		
		
							$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $legislacaoLista . "$complemento&tela=atos&tipo=3,4,5,22,23,32,50";
		
		
		
						} else if($configuracaoTransparencia['atos_diario'] == "S") {
		
		
		
							$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $diarioLista . "$complemento";
		
		
		
						} else {
		
		
		
							$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $atoLista . "$complemento";
		
						}
		
		
		
						if($configuracaoTransparencia['atos_outro_cliente'] == "S")
		
							$link[$indice] .= "&nc=" . $outroCliente['id'];
		
		
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=atos_normativos&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "7":
		
					if(empty($configuracaoTransparencia['caminho_organograma'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $organograma . "$complemento";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=organograma&redir=link";
		
		
		
					}
		
					$target[$indice] = "_self";
		
					break;
		
				case "8":
		
					if(empty($configuracaoTransparencia['caminho_concurso'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $concursoLista . "$complemento";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=concurso&redir=link";
		
		
		
					}
		
					$target[$indice] = "_self";
		
					break;
		
				case "9":
		
					if(empty($configuracaoTransparencia['caminho_ppa'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $legislacaoLista . "$complemento&tela=ppa&tipo=18";
		
		
		
						if($configuracaoTransparencia['ppa_outro_cliente'] == "S")
		
							$link[$indice] .= "&nc=" . $outroCliente['id'];
		
		
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=ppa&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "10":
		
					if(empty($configuracaoTransparencia['caminho_ldo'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $legislacaoLista . "$complemento&tela=ldo&tipo=19";
		
		
		
						if($configuracaoTransparencia['ldo_outro_cliente'] == "S")
		
							$link[$indice] .= "&nc=" . $outroCliente['id'];
		
		
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=ldo&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "11":
		
					if(empty($configuracaoTransparencia['caminho_loa'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $legislacaoLista . "$complemento&tela=loa&tipo=20";
		
		
		
						if($configuracaoTransparencia['loa_outro_cliente'] == "S")
		
							$link[$indice] .= "&nc=" . $outroCliente['id'];
		
		
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=loa&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "12":
		
					if(empty($configuracaoTransparencia['caminho_plano_diretor'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $plano . "$complemento";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=plano_diretor&redir=link";
		
		
		
					}
		
					$target[$indice] = "_self";
		
					break;
		
				case "13":
		
					if(empty($configuracaoTransparencia['caminho_audiencia_publica'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $audienciaPublica . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=audiencia_publica&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "14":
		
					if(empty($configuracaoTransparencia['caminho_convenio'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $convenio . "$complemento";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=convenio&redir=link";
		
		
		
					}
		
					$target[$indice] = "_self";
		
					break;
		
				case "15":
		
					$stOuvidoria = $conn->prepare("SELECT id
		
														FROM controle_sistema_cliente
		
														WHERE id_cliente = :id_cliente
		
														AND id_sistema = :id_sistema");
		
		
		
					$stOuvidoria->execute(array("id_cliente" => $cliente, "id_sistema" => "7"));
		
					$confSistemaOuvidoria = $stOuvidoria->fetchAll();
		
		
		
					if(count($confSistemaOuvidoria) > 0) {
		
						if($cliente == "12113") {
		
							$link[$indice] = "https://www.govfacilcidadao.com.br/login";
		
						} else {
		
							$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $sistemaGestaoOuvidoria . "$complemento";
		
		
						}
		
		
		
					} else {
		
		
		
						if($cliente == "46") {
		
		
		
							$link[$indice] = '#" role="button" data-toggle="modal" data-target="#ouvidoria_ubirata';
		
		
		
						} else if($cliente == "1040") {
		
		
		
							$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $ouvidoria2 . "$complemento";
		
		
		
						} else {
		
		
		
							$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $ouvidoria . "$complemento";
		
		
		
						}
		
		
		
					}
		
					$target[$indice] = "_self";
		
					break;
		
				case "16":
		
					if(empty($configuracaoTransparencia['caminho_servidores'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $servidorLista . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=servidores&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "17":
		
					if(empty($configuracaoTransparencia['caminho_contrato'])) {
		
		
		
						if($configuracaoTransparencia['contratos_licitacao'] == "S") {
		
		
		
							if(!empty($configuracaoTransparencia['caminho_digitalizacao_licitacao']) && $configuracaoTransparencia['digitalizacao_licitacao'] == "N") {
		
		
		
								$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $digitalizacaoLista . "$complemento&id_tipo=" . $configuracaoTransparencia['caminho_digitalizacao_licitacao'];
		
		
		
							} else if(empty($configuracaoTransparencia['caminho_licitacao'])) {
		
		
		
								$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $licitacaoModalidade . "$complemento";
		
								$target[$indice] = "_self";
		
		
		
								if($configuracaoTransparencia['contrato_outro_cliente'] == "S")
		
									$link[$indice] .= "&nc=" . $outroCliente['id'];
		
		
		
							} else {
		
		
		
								$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=licitacao&redir=link";
		
								$target[$indice] = "_self";
		
		
		
							}
		
		
		
						} else {
		
		
		
							$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $contrato . "$complemento";
		
		
		
							if($configuracaoTransparencia['contrato_outro_cliente'] == "S")
		
								$link[$indice] .= "&nc=" . $outroCliente['id'];
		
		
		
						}
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=contrato&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "18":
		
					if(empty($configuracaoTransparencia['caminho_diarias'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $diariaLista . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=diarias&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "19":
		
					if(empty($configuracaoTransparencia['caminho_outros_documentos'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $outrosDocumentos . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=outros_documentos&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "20":
		
					if(empty($configuracaoTransparencia['caminho_despesas'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $despesa . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=despesas&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "21":
		
					if(empty($configuracaoTransparencia['caminho_enderecos'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $enderecoOficial . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=enderecos&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "22":
		
					if(empty($configuracaoTransparencia['caminho_receitas'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $receita . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=receitas&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "23":
		
					if(empty($configuracaoTransparencia['caminho_plano_contas'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $planoConta . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=plano_contas&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "24":
		
					if(empty($configuracaoTransparencia['caminho_patrimonio'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $patrimonio . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=patrimonio&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "25":
		
					if(empty($configuracaoTransparencia['caminho_vereadores'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $vereadorLista . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=vereadores&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "26":
		
					if(empty($configuracaoTransparencia['caminho_nfe'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $nfeLista . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=nfe&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "27":
		
					if(empty($configuracaoTransparencia['caminho_reembolso'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $reembolso . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=reembolso&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "28":
		
					if(empty($configuracaoTransparencia['caminho_percentual_aplicacao_saude'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $aplicacaoSaude . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=percentual_aplicacao_saude&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "29":
		
					if(empty($configuracaoTransparencia['caminho_cessao_doacao_permuta'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $cessaoPermuta . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=cessao_doacao_permuta&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "30":
		
					if(empty($configuracaoTransparencia['caminho_relatorio_resumido_execucao_orcamentaria'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $execucaoOrcamentaria . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=relatorio_resumido_execucao_orcamentaria&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "31":
		
					if(empty($configuracaoTransparencia['caminho_operacoes_financeiras'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $operacaoFinanceira . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=operacoes_financeiras&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "32":
		
					if(empty($configuracaoTransparencia['caminho_controle_estoque'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $controleEstoque . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=controle_estoque&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "33":
		
					if(empty($configuracaoTransparencia['caminho_cartao_corporativo'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $cartaoCorporativo . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=cartao_corporativo&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "34":
		
					if(empty($configuracaoTransparencia['caminho_justificativa_contratacao_direta'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $justificativaDireta . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=justificativa_contratacao_direta&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "35":
		
					if(empty($configuracaoTransparencia['caminho_aquisicao_passagem_aerea'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $passagem . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=aquisicao_passagem_aerea&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "36":
		
					if(empty($configuracaoTransparencia['caminho_movimentacao_fundo'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $fundos . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=movimentacao_fundo&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "37":
		
					if(empty($configuracaoTransparencia['caminho_percentual_aplicacao_educacao'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $aplicacaoEducacao . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=percentual_aplicacao_educacao&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "38":
		
					if(empty($configuracaoTransparencia['caminho_relatorio_gestao_fiscal'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $relatorioFiscal . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=relatorio_gestao_fiscal&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "39":
		
					if(empty($configuracaoTransparencia['caminho_execucao_orcamentaria_tempo_real'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $execucaoTempoReal . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=execucao_orcamentaria_tempo_real&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "40":
		
					if(empty($configuracaoTransparencia['caminho_extrato_conta_unica'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $extratoUnico . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=extrato_conta_unica&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "41":
		
					if(empty($configuracaoTransparencia['caminho_adiantamento'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $adiantamento . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=adiantamento&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "42":
		
					if(empty($configuracaoTransparencia['caminho_notificacao'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $notificacao . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=notificacao&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "43":
		
					if(empty($configuracaoTransparencia['caminho_lei_organica'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $legislacaoLista . "$complemento&tipo=2";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=lei_organica&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "44":
		
					if(empty($configuracaoTransparencia['caminho_regimento_interno'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $regimentoLista . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=regimento_interno&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "45":
		
					if(empty($configuracaoTransparencia['caminho_repasse_transferencia'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $repasseTransferencia . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=repasse_transferencia&redir=link";
		
						$target[$indice] = "_blank";
		
		
		
					}
		
					break;
		
				case "46":
		
					if(empty($configuracaoTransparencia['caminho_bolsa_familia'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $bolsaFamilia . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=bolsa_familia&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "47":
		
					if(empty($configuracaoTransparencia['caminho_transferencia_voluntaria'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transferenciaVoluntaria . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=transferencia_voluntaria&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "48":
		
					if(empty($configuracaoTransparencia['caminho_controle_interno'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $controleInterno . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=controle_interno&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "49":
		
					if(empty($configuracaoTransparencia['caminho_formulario_atendimento'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $formularioAtendimento . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=formulario_atendimento&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "50":
		
					if(empty($configuracaoTransparencia['caminho_relatorio_atendimento'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $relatorioAtendimento . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=relatorio_atendimento&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "51":
		
					if(empty($configuracaoTransparencia['caminho_sic_cidadao'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $sicCidadao . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=sic_cidadao&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "52":
		
					if(empty($configuracaoTransparencia['caminho_ata_registro_preco'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $ataRegistroPreco . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=ata_registro_preco&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "53":
		
					if(empty($configuracaoTransparencia['caminho_prestacao_contas'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $prestacaoContas . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=prestacao_contas&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "54":
		
					if(empty($configuracaoTransparencia['caminho_lei_4320'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $lei4320 . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
						if($configuracaoTransparencia['anexos_4320_outro_cliente'] == "S")
		
							$link[$indice] .= "&nc=" . $outroCliente['id'];
		
		
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=lei_4320&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "55":
		
					if(empty($configuracaoTransparencia['caminho_lei_responsabilidade_fiscal'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $leiResponsabilidadeFiscal . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=lei_responsabilidade_fiscal&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "56":
		
					if(empty($configuracaoTransparencia['caminho_compra_direta'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $compraDireta . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=compra_direta&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "57":
		
					if(empty($configuracaoTransparencia['caminho_veiculo'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $veiculo . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=veiculo&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "58":
		
					if(empty($configuracaoTransparencia['caminho_acesso_informacao'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $acesso . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=acesso_informacao&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "59":
		
					if(!empty($configuracaoTransparencia['caminho_digitalizacao_licitacao'])) {
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $digitalizacaoLista . "$complemento&id_tipo=" . $configuracaoTransparencia['caminho_digitalizacao_licitacao'];
		
						
		
					} else if(empty($configuracaoTransparencia['caminho_licitacao_integra'])) {
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $licitacaoIntegra . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=licitacao_integra&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "60":
		
					if(empty($configuracaoTransparencia['caminho_codigo_tributario'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $codigoTributario . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=codigo_tributario&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "61":
		
					if(empty($configuracaoTransparencia['caminho_estatuto_servidor'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $estatutoServidor . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=estatuto_servidor&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "62":
		
					if(empty($configuracaoTransparencia['caminho_instrucao_normativa'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $instrucaoNormativa . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=instrucao_normativa&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "63":
		
					if(empty($configuracaoTransparencia['caminho_rpps'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $rpps . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=rpps&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "64":
		
					if(empty($configuracaoTransparencia['caminho_extrato_conta'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $extratoConta . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=extrato_conta&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "65":
		
					if(empty($configuracaoTransparencia['caminho_servidor_cedido'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $servidorCedido . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=servidor_cedido&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "66":
		
					if(empty($configuracaoTransparencia['caminho_servidor_temporario'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $servidorTemporario . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=servidor_temporario&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "67":
		
					if(empty($configuracaoTransparencia['caminho_recurso_educacao'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $recursoEducacao . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=recurso_educacao&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "68":
		
					if(empty($configuracaoTransparencia['caminho_credor'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $credor . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=credor&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "69":
		
					if(empty($configuracaoTransparencia['caminho_plano_educacao'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $planoEducacao . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=plano_educacao&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "70":
		
					if(empty($configuracaoTransparencia['caminho_plano_habitacao'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $planoHabitacao . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=plano_habitacao&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "71":
		
					if(empty($configuracaoTransparencia['caminho_ajuda_custo'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $ajudaCusto . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=ajuda_custo&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "72":
		
					if(empty($configuracaoTransparencia['caminho_verba_gabinete'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $verbaGabinete . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=verba_gabinete&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "73":
		
					if(empty($configuracaoTransparencia['caminho_servidor_recebido'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $servidorRecebido . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=servidor_recebido&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "74":
		
					if(empty($configuracaoTransparencia['caminho_juridico'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $juridico . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=juridico&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "75":
		
					if(empty($configuracaoTransparencia['caminho_programa_acao'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $programaAcao . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=programa_acao&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "76":
		
					if(empty($configuracaoTransparencia['caminho_relatorio_saude'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $relatorioSaude . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=relatorio_saude&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "77":
		
					if(empty($configuracaoTransparencia['caminho_verba_representacao'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $verbaRepresentacao . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=verba_representacao&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "78":
		
					if(empty($configuracaoTransparencia['caminho_portarias_nomeacoes'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $portariasNomeacoesLista . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=portarias_nomeacoes&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
				case "79":
		
					if(empty($configuracaoTransparencia['caminho_atos_secretarias'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $atosSecretarias . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=atos_secretarias&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
		
		
						case "80":
		
								if(empty($configuracaoTransparencia['caminho_precatorio'])) {
		
		
		
										$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $precatorio . "$complemento";
		
										$target[$indice] = "_self";
		
		
		
								} else {
		
		
		
										$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=precatorio&redir=link";
		
										$target[$indice] = "_self";
		
		
		
								}
		
								break;
		
		
		
						case "81":
		
								if(empty($configuracaoTransparencia['caminho_extrato_bancario'])) {
		
		
		
										$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $extratoBancario . "$complemento";
		
										$target[$indice] = "_self";
		
		
		
								} else {
		
		
		
										$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=extrato_bancario&redir=link";
		
										$target[$indice] = "_self";
		
		
		
								}
		
								break;
		
		
		
						case "82":
		
								if(empty($configuracaoTransparencia['caminho_documento_fiscal'])) {
		
		
		
										$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $documentosFiscais . "$complemento";
		
										$target[$indice] = "_self";
		
		
		
								} else {
		
		
		
										$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=documento_fiscal&redir=link";
		
										$target[$indice] = "_self";
		
		
		
								}
		
								break;
		
		
		
						case  '83':
		
								if(empty($configuracaoTransparencia['caminho_plano_carreira'])) {
		
		
		
										$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $planoCarreira . "$complemento";
		
										$target[$indice] = "_self";
		
		
		
								} else {
		
		
		
										$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=plano_carreira&redir=link";
		
										$target[$indice] = "_self";
		
		
		
								}
		
								break;
		
		
		
						case  '84':
		
								if(empty($configuracaoTransparencia['caminho_fundeb'])) {
		
		
		
										$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $fundeb . "$complemento";
		
										$target[$indice] = "_self";
		
		
		
								} else {
		
		
		
										$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=fundeb&redir=link";
		
										$target[$indice] = "_self";
		
		
		
								}
		
								break;
		
		
		
						case  '85':
		
								if(empty($configuracaoTransparencia['caminho_rem_agente_publico'])) {
		
		
		
										$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $remAgentePublico . "$complemento";
		
										$target[$indice] = "_self";
		
		
		
								} else {
		
		
		
										$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=rem_agente_publico&redir=link";
		
										$target[$indice] = "_self";
		
		
		
								}
		
								break;
		
		
		
						case  '86':
		
								if(empty($configuracaoTransparencia['caminho_quadro_funcional'])) {
		
		
		
										$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $quadroFuncional . "$complemento";
		
										$target[$indice] = "_self";
		
		
		
								} else {
		
		
		
										$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=quadro_funcional&redir=link";
		
										$target[$indice] = "_self";
		
		
		
								}
		
								break;
		
		
		
						case  '87':
		
								if(empty($configuracaoTransparencia['caminho_res_combustivel'])) {
		
		
		
										$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $resCombustivel . "$complemento";
		
										$target[$indice] = "_self";
		
		
		
								} else {
		
		
		
										$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=res_combustivel&redir=link";
		
										$target[$indice] = "_self";
		
		
		
								}
		
								break;
		
		
		
						case  '88':
		
								if(empty($configuracaoTransparencia['caminho_licitacoes_abertas'])) {
		
		
		
										$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $licitacoesAbertas . "$complemento";
		
										$target[$indice] = "_self";
		
		
		
								} else {
		
		
		
										$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=licitacoes_abertas&redir=link";
		
										$target[$indice] = "_self";
		
		
		
								}
		
								break;
		
		
		
				case  '89':
		
								if(empty($configuracaoTransparencia['caminho_controle_medicamentos'])) {
		
		
		
										$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $controleMedicacao . "$complemento";
		
										$target[$indice] = "_self";
		
		
		
								} else {
		
		
		
										$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=controle_medicamentos&redir=link";
		
										$target[$indice] = "_self";
		
		
		
								}
		
								break;
		
		
		
				case  '90':
		
								if(empty($configuracaoTransparencia['caminho_licitacao_andamento'])) {
		
		
		
										$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $licitacaoAndamento . "$complemento";
		
										$target[$indice] = "_self";
		
		
		
								} else {
		
		
		
										$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=licitacao_andamento&redir=link";
		
										$target[$indice] = "_self";
		
		
		
								}
		
								break;
		
		
		
				case  '91':
		
								if(empty($configuracaoTransparencia['caminho_licitacao_realizada'])) {
		
		
		
										$link[$indice] = $CAMINHO . "/index.php?lici=realizadas&sessao=$sequencia" . $licitacaoRealizada . "$complemento";
		
										$target[$indice] = "_self";
		
		
		
								} else {
		
		
		
										$link[$indice] = $CAMINHO . "/index.php?lici=realizadas&sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=licitacao_realizada&redir=link";
		
										$target[$indice] = "_self";
		
		
		
								}
		
								break;
		
		
		
				case  '92':
		
								if(empty($configuracaoTransparencia['caminho_aviso_licitacao'])) {
		
		
		
										$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $avisoLicitacao . "$complemento";
		
										$target[$indice] = "_self";
		
		
		
								} else {
		
		
		
										$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=aviso_licitacao&redir=link";
		
										$target[$indice] = "_self";
		
		
		
								}
		
								break;
		
		
		
				case  '93':
		
								if(empty($configuracaoTransparencia['caminho_dispensa_licitacao'])) {
		
		
		
										$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $dispensaLicitacao . "$complemento";
		
										$target[$indice] = "_self";
		
		
		
								} else {
		
		
		
										$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=dispensa_licitacao&redir=link";
		
										$target[$indice] = "_self";
		
		
		
								}
		
								break;
		
		
		
				case  '94':
		
								if(empty($configuracaoTransparencia['caminho_inexigibilidade_licitacao'])) {
		
		
		
										$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $inexigibilidadeLicitacao . "$complemento";
		
										$target[$indice] = "_self";
		
		
		
								} else {
		
		
		
										$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=inexigibilidade_licitacao&redir=link";
		
										$target[$indice] = "_self";
		
		
		
								}
		
								break;
		
		
		
				case  '95':
		
								if(empty($configuracaoTransparencia['caminho_secretaria_executiva'])) {
		
		
		
										$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $secretariaExecutiva . "$complemento";
		
										$target[$indice] = "_self";
		
		
		
								} else {
		
		
		
										$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=secretaria_executiva&redir=link";
		
										$target[$indice] = "_self";
		
		
		
								}
		
								break;
		
		
		
				case  '96':
		
								if(empty($configuracaoTransparencia['caminho_contracheque'])) {
		
		
		
										$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $contracheque . "$complemento";
		
										$target[$indice] = "_self";
		
		
		
								} else {
		
		
		
										$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=contracheque&redir=link";
		
										$target[$indice] = "_self";
		
		
		
								}
		
								break;
		
		
		
				case  '97':
		
								if(empty($configuracaoTransparencia['caminho_projetos_leis'])) {
		
		
		
										$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $projetosLeis . "$complemento";
		
										$target[$indice] = "_self";
		
		
		
								} else {
		
		
		
										$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=projetos_leis&redir=link";
		
										$target[$indice] = "_self";
		
		
		
								}
		
								break;
		
		
		
				case  '98':
		
					if(empty($configuracaoTransparencia['caminho_servidor_salario'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $servidorSalario . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=servidor_salario&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
		
		
				case "99":
		
					if(empty($configuracaoTransparencia['caminho_estoque_saude'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $controleEstoqueSaude . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=estoque_saude&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
		
		
				case "100":
		
					if(empty($configuracaoTransparencia['caminho_interferencia_financeira'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $interferenciaFinanceira . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=interferencia_financeira&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
		
		
				case "101":
		
					if(empty($configuracaoTransparencia['caminho_servidor_ferias_licenca'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $servidorFeriasLicenca . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=servidor_ferias_licenca&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
		
		
				case "102":
		
					if(empty($configuracaoTransparencia['caminho_plano_residuo'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $planoResiduo . "$complemento";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=plano_residuo&redir=link";
		
		
		
					}
		
					$target[$indice] = "_self";
		
					break;
		
		
		
					case "103":
		
					if(empty($configuracaoTransparencia['caminho_laudos_tecnicos'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $laudosTecnicos . "$complemento";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=laudos_tecnicos&redir=link";
		
		
		
					}
		
					$target[$indice] = "_self";
		
					break;
		
					case "104":
					
								if(empty($configuracaoTransparencia['caminho_assistencia_social'])) {
					
					
					
									$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $assistenciaSocial . "$complemento";
					
					
					
								} else {
					
					
					
									$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=assistencia_social&redir=link";
					
					
					
								}
					
								$target[$indice] = "_self";
					
					break;
		
					case "105":
		
								if(empty($configuracaoTransparencia['caminho_previdencia_investimento'])) {
		
		
		
									$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $investimento . "$complemento";
		
		
		
								} else {
		
		
		
									$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=previdencia_investimento&redir=link";
		
		
		
								}
								$target[$indice] = "_self";
								
					break;
		
					case "106":
		
								if(empty($configuracaoTransparencia['caminho_calculo_atuarial'])) {
		
		
		
									$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $calculoAtuarial . "$complemento";
		
		
		
								} else {
		
		
		
									$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=calculo_atuarial&redir=link";
		
		
		
								}
								$target[$indice] = "_self";
		
					break;
		
					case "107":
		
								if(empty($configuracaoTransparencia['caminho_ministerio_pf'])) {
		
		
		
									$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $ministerioPF . "$complemento";
		
		
		
								} else {
		
		
		
									$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=ministerio_pf&redir=link";
		
		
		
								}
								$target[$indice] = "_self";
		
					break;
		
					case "108":
		
								if(empty($configuracaoTransparencia['caminho_lista_creche'])) {
		
		
		
									$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $listaCreche . "$complemento";
		
		
		
								} else {
		
		
		
									$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=lista_creche&redir=link";
		
		
		
								}
								$target[$indice] = "_self";
		
					break;
		
					case "109":
		
								if(empty($configuracaoTransparencia['caminho_chamamento_publico'])) {
		
		
		
									$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $chamamentoPublico . "$complemento";
		
		
		
								} else {
		
		
		
									$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=chamamento_publico&redir=link";
		
		
		
								}
								$target[$indice] = "_self";
		
					break;
		
					case "110":
		
								if(empty($configuracaoTransparencia['caminho_relatorio_viagem_saude'])) {
		
		
		
									$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $relatorioViagem . "$complemento";
		
		
		
								} else {
		
		
		
									$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=relatorio_viagem_saude&redir=link";
		
		
		
								}
								$target[$indice] = "_self";
		
					break;
		
					case "111":
		
								if(empty($configuracaoTransparencia['caminho_emails_oficiais'])) {
		
		
		
									$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $emails . "$complemento";
		
		
		
								} else {
		
		
		
									$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=emails&redir=link";
		
		
		
								}
								$target[$indice] = "_self";
		
					break;
		
					case "112":
		
								if(empty($configuracaoTransparencia['caminho_quantitativo_cargos'])) {
		
		
		
									$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $quantiCargos . "$complemento";
		
		
		
								} else {
		
		
		
									$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=quantitativo_cargos&redir=link";
		
		
		
								}
								$target[$indice] = "_self";
		
					break;
		
					case "113":
		
								if(empty($configuracaoTransparencia['caminho_servidor_ativo'])) {
		
		
		
									$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $ServAtivos . "$complemento";
		
		
		
								} else {
		
		
		
									$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=servidor_ativo&redir=link";
		
		
		
								}
								$target[$indice] = "_self";
		
					break;
		
					case "114":
		
								if(empty($configuracaoTransparencia['caminho_servidor_inativo'])) {
		
		
		
									$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $ServInativos . "$complemento";
		
		
		
								} else {
		
		
		
									$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=servidor_inativo&redir=link";
		
		
		
								}
								$target[$indice] = "_self";
		
					break;
		
					case "115":
		
								if(empty($configuracaoTransparencia['caminho_acoes_programas_gov'])) {
		
		
		
									$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $acoesProgramas . "$complemento";
		
		
		
								} else {
		
		
		
									$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=acoes_programas_gov&redir=link";
		
		
		
								}
								$target[$indice] = "_self";
		
					break;
		
					case "116":
		
								
								if(empty($configuracaoTransparencia['caminho_portal_obras'])) {
		
		
		
									$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $portalObras . "$complemento";
		
		
		
								} else {
		
		
		
									$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=portal_obras&redir=link";
		
		
		
								}
								$target[$indice] = "_self";
		
					break;
		
					case "117":
		
								if(empty($configuracaoTransparencia['caminho_servidores_comissionados'])) {
		
		
		
									$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $ServComissionados . "$complemento";
		
		
		
								} else {
		
		
		
									$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=servidores_comissionados&redir=link";
		
		
		
								}
								$target[$indice] = "_self";
		
					break;
		
					case "118":
		
								if(empty($configuracaoTransparencia['caminho_servidores_efetivos'])) {
		
		
		
									$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $ServEfetivos . "$complemento";
		
		
		
								} else {
		
		
		
									$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=servidores_efetivos&redir=link";
		
		
		
								}
								$target[$indice] = "_self";
		
					break;
		
					case "119":
		
								if(empty($configuracaoTransparencia['caminho_licenciamento_ambiental'])) {
		
		
		
									$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $licenciamentoAmbiental . "$complemento";
		
		
		
								} else {
		
		
		
									$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=licenciamento_ambiental&redir=link";
		
		
		
								}
								$target[$indice] = "_self";
		
					break;
		
					case "120":
					
					if(empty($configuracaoTransparencia['caminho_legislacao'])) {
		
						if(!empty($configuracaoTransparencia['caminho_digitalizacao_legislacao']) && $configuracaoTransparencia['digitalizacao_legislacao'] == "N") {
		
							$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $digitalizacaoLista . "$complemento&id_tipo=" . $configuracaoTransparencia['caminho_digitalizacao_legislacao'];
		
						} else if($configuracaoTransparencia['legislacao_diario'] == "S") {
		
							$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $diarioLista . "$complemento";
		
						} else {
		
							if($cliente == "46") {
		
								$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $legislacaoLista . "$complemento&tipo=1";
		
							} else {
		
								$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $legislacaoLista . "$complemento&ntipo=3,4,5,18,19,20,22,23,32";
		
							}
		
							if($configuracaoTransparencia['legislacao_outro_cliente'] == "S")
		
								$link[$indice] .= "&nc=" . $outroCliente['id'];
		
						}
		
						$target[$indice] = "_self";
		
		
					} else {
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=legislacao&redir=link";
		
						$target[$indice] = "_self";
		
					}
		
					break;
		
					case "121":
					
						if(empty($configuracaoTransparencia['caminho_atos_normativos'])) {
			
			
			
							if(!empty($configuracaoTransparencia['caminho_digitalizacao_atos']) && $configuracaoTransparencia['digitalizacao_atos'] == "N") {
			
			
			
								$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $digitalizacaoLista . "$complemento&id_tipo=" . $configuracaoTransparencia['caminho_digitalizacao_atos'];
			
			
			
							} else if($configuracaoTransparencia['atos_legislacao'] == "S") {
			
			
			
								$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $legislacaoLista . "$complemento&tela=atos&tipo=3,4,5,22,23,32,50";
			
			
			
							} else if($configuracaoTransparencia['atos_diario'] == "S") {
			
			
			
								$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $diarioLista . "$complemento";
			
			
			
							} else {
			
			
			
								$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $atoLista . "$complemento";
			
							}
			
			
			
							if($configuracaoTransparencia['atos_outro_cliente'] == "S")
			
								$link[$indice] .= "&nc=" . $outroCliente['id'];
			
			
			
							$target[$indice] = "_self";
			
			
			
						} else {
			
			
			
							$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=atos_normativos&redir=link";
			
							$target[$indice] = "_self";
			
			
			
						}
			
					break;
		
					case "122":
		
								if(empty($configuracaoTransparencia['caminho_acoes_programas'])) {
		
		
		
									$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $acaoPrograma . "$complemento";
		
		
		
								} else {
		
		
		
									$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=acao_programa&redir=link";
		
		
		
								}
								$target[$indice] = "_self";
		
					break;
		
					case "123":
		
								if(empty($configuracaoTransparencia['caminho_leis_atos'])) {
		
		
		
									$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $leisAtos . "$complemento";
		
		
		
								} else {
		
		
		
									$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=leis_atos&redir=link";
		
		
		
								}
								$target[$indice] = "_self";
		
					break;
		
					case "124":
		
								if(empty($configuracaoTransparencia['caminho_concurso_pss'])) {
		
		
		
									$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $concursoPssLista . "$complemento";
		
		
		
								} else {
		
		
		
									$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=concurso_pss&redir=link";
		
		
		
								}
								$target[$indice] = "_self";
		
					break;
		
					case "125":
		
					if(empty($configuracaoTransparencia['caminho_estagiarios'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $estagiarios . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=estagiarios&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
					case "126":
		
					if(empty($configuracaoTransparencia['caminho_plano_saude'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $planoSaude . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=plano_saude&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
					case "127":
		
								if(empty($configuracaoTransparencia['caminho_cardapio_escolar'])) {
		
		
		
									$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $cardapioEscolar . "$complemento";
		
		
		
								} else {
		
		
		
									$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=cardapio_escolar&redir=link";
		
		
		
								}
								$target[$indice] = "_self";
		
					break;
		
					case "128":
		
					if(empty($configuracaoTransparencia['caminho_contratacao_direta'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $contratacaoDireta . "$complemento";
		
						$target[$indice] = "_self";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=contratacao_direta&redir=link";
		
						$target[$indice] = "_self";
		
		
		
					}
		
					break;
		
					case "129":
		
					if(empty($configuracaoTransparencia['caminho_bolsa_atleta'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $bolsaAtleta . "$complemento";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=bolsa_atleta&redir=link";
		
		
		
					}
		
					$target[$indice] = "_self";
		
					break;
		
					case "130":
		
					if(empty($configuracaoTransparencia['caminho_julgamento_contas'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $julgamentoContas . "$complemento";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=julgamento_contas&redir=link";
		
		
		
					}
		
					$target[$indice] = "_self";
		
					break;
		
					case "131":
		
					if(empty($configuracaoTransparencia['caminho_consorcio'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $consorcio . "$complemento";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=consorcio&redir=link";
		
		
		
					}
		
					$target[$indice] = "_self";
		
					break;
		
					case "132":
		
					if(empty($configuracaoTransparencia['caminho_tabela_vencimento'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $tabelaVencimento . "$complemento";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=tabela_vencimento&redir=link";
		
		
		
					}
		
					$target[$indice] = "_self";
		
					break;
		
					case "133":
		
								if(empty($configuracaoTransparencia['caminho_transporte_escolar'])) {
		
		
		
									$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transporteEscolar . "$complemento";
		
		
		
								} else {
		
		
		
									$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=transporte_escolar&redir=link";
		
		
		
								}
								$target[$indice] = "_self";
		
					break;
		
					case "134":
		
					if(empty($configuracaoTransparencia['caminho_empenhos'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $empenhos . "$complemento";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=empenhos&redir=link";
		
		
		
					}
					$target[$indice] = "_self";
		
					break;
		
					case "135":
		
					if(empty($configuracaoTransparencia['caminho_contas_chefe_poder_executivo'])) {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $contasChefePoderExecutivo . "$complemento";
		
		
		
					} else {
		
		
		
						$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=contas_chefe_poder_executivo&redir=link";
		
		
		
					}
					$target[$indice] = "_self";
		
					break;
		
					case  '136':
		
						if(empty($configuracaoTransparencia['caminho_lista_espera_consulta'])) {
			
			
			
							$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $listaEsperaConsulta . "$complemento";
			
							$target[$indice] = "_self";
			
			
			
						} else {
			
			
			
							$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=lista_espera_consulta&redir=link";
			
							$target[$indice] = "_self";
			
			
			
						}
			
						break;
		
						case  '137':
		
							if(empty($configuracaoTransparencia['caminho_lista_espera_sus'])) {
				
				
				
								$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $listaEsperaSus . "$complemento";
				
								$target[$indice] = "_self";
				
				
				
							} else {
				
				
				
								$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=lista_espera_sus&redir=link";
				
								$target[$indice] = "_self";
				
				
				
							}
				
							break;
		
						case  '139':
		
							if(empty($configuracaoTransparencia['caminho_logistica_distribuicao'])) {
				
				
				
								$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $logisticaDistribuicao . "$complemento";
				
								$target[$indice] = "_self";
				
				
				
							} else {
				
				
				
								$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=logistica_distribuicao&redir=link";
				
								$target[$indice] = "_self";
				
				
				
							}
				
							break;
		
						case "140":
		
							if(empty($configuracaoTransparencia['caminho_covid19'])) {
				
				
				
								$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $covid19 . "$complemento";
				
								$target[$indice] = "_self";
				
				
				
							} else {
				
				
				
								$link[$indice] = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=covid19&redir=link";
				
								$target[$indice] = "_self";
				
				
				
							}
				
							break;
		
		
					default:
		
						$link[$indice] = "#";
		
						$target[$indice] = "_self";
		
					break;
		
			}
		
		
		
		}
	}


?>
<div class="text-center test-class">


	<div class="panel panel-primary">
		<div class="panel-heading">
			<h2 class="panel-title text-left"><i class="<?= $menuCategoria['icone'] ?>"></i> Icones Encontrados</h2>
		</div>
		<div class="panel-body">

		<?
		foreach ($link as $indice => $menu) {
	
			?>
				<a href="<?= $link[$indice] ?>" target="<?= $target[$indice] ?>" title="<?= $nomeMenu[$indice] ?>" class="link_inicio vrau">
					<img src="<?= $CAMINHO ?>/images/<?= $imagemMenu[$indice] ?>" alt="<?= $nomeMenu[$indice] ?>" />
				</a>
			<?php
		}
		?>
			
			
		</div>
	</div>

</div>
<div class="panel panel-primary">
	<div class="panel-heading">
		<h2 class="panel-title text-left"><i class="<?= $menuCategoria['icone'] ?>"></i> Artigos Encontrados</h2>
	</div>
	<div class="panel-body">
<?php
	// BUSCA INTERNA
	$stMenu = $conn->prepare("SELECT cliente_configuracao_transparencia_icone.*

							    FROM cliente_configuracao_transparencia_icone, cliente_configuracao_transparencia_icone_cliente

							   WHERE cliente_configuracao_transparencia_icone_cliente.id_icone = cliente_configuracao_transparencia_icone.id

								 AND cliente_configuracao_transparencia_icone_cliente.id_cliente = :id_cliente

								 AND cliente_configuracao_transparencia_icone.status_registro = :status_registro

						    ORDER BY cliente_configuracao_transparencia_icone_cliente.ordem");



	$stMenu->execute(array("id_cliente" => $cliente, "status_registro" => "A"));

	$qryMenu = $stMenu->fetchAll();



	$novoCliente = $cliente;


	$contaResultados = 0;
	foreach ($qryMenu as $indice => $menu) {



		switch ($menu['id']) {



			case "1":

				$qryLinkTransparencia = $conn->query("SELECT *

													    FROM link_transparencia

													   WHERE id_cliente = '$cliente'

													     AND status_registro = 'A'

													     AND descricao LIKE '%$busca%'")->fetchAll(PDO::FETCH_ASSOC);


				$contador = count($qryLinkTransparencia);
				if(count($qryLinkTransparencia) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $transparenciaOnline . "$complemento'>Transpar&ecirc;ncia Online</a><span class='badge'>$contador</span></h3>";



					foreach($qryLinkTransparencia as $linkTransparencia) {

						if($cliente != 12014) {

							echo "<p><a href='$CAMINHO/index.php?sessao=$sequencia$transparenciaOnlineVisualiza$complemento&id=$linkTransparencia[id]&redir=link'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$linkTransparencia[descricao]</strong></a></p>";

						}else{

							echo "<p><a href='$linkTransparencia[link]'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$linkTransparencia[descricao]</strong></a></p>";

						}

					}



					echo "<p>&nbsp;</p>";

				}

				break;

			case "2":

				$qryDemonstrativo = $conn->query("SELECT demonstrativo_contabil_arquivo.*, demonstrativo_contabil_categoria.descricao categoria, demonstrativo_contabil_subcategoria.descricao subcategoria, demonstrativo_contabil_ano.descricao ano

												    FROM demonstrativo_contabil_arquivo

											   LEFT JOIN demonstrativo_contabil_subcategoria ON demonstrativo_contabil_arquivo.id_subcategoria = demonstrativo_contabil_subcategoria.id

											   LEFT JOIN demonstrativo_contabil_categoria ON demonstrativo_contabil_arquivo.id_categoria = demonstrativo_contabil_categoria.id

											   LEFT JOIN demonstrativo_contabil_ano ON demonstrativo_contabil_arquivo.id_ano = demonstrativo_contabil_ano.id

											  	   WHERE demonstrativo_contabil_arquivo.id_cliente = '$cliente'

											  	     AND demonstrativo_contabil_arquivo.status_registro = 'A'

											  	     AND (demonstrativo_contabil_arquivo.descricao LIKE '%$busca%' OR demonstrativo_contabil_subcategoria.descricao LIKE '%$busca%' OR demonstrativo_contabil_categoria.descricao LIKE '%$busca%')")->fetchAll(PDO::FETCH_ASSOC);


				$contador = count($qryDemonstrativo);
				if(count($qryDemonstrativo) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $gastossessao . "$complemento'>Demonstrativos Cont&aacute;beis</a><span class='badge'>$contador</span></h3>";



					foreach($qryDemonstrativo as $demonstrativo) {



						if($demonstrativo['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$demonstrativo[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$demonstrativo[ano]&nbsp;&raquo;&nbsp;$demonstrativo[categoria]&nbsp;&raquo;&nbsp;$demonstrativo[subcategoria]&nbsp;&raquo;&nbsp;$demonstrativo[descricao]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$demonstrativo[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$demonstrativo[ano]&nbsp;&raquo;&nbsp;$demonstrativo[categoria]&nbsp;&raquo;&nbsp;$demonstrativo[subcategoria]&nbsp;&raquo;&nbsp;$demonstrativo[descricao]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}



				break;

			case "3":

				$qryDiario = $conn->query("SELECT di_arquivo.*, di_categoria.categoria, di_subcategoria.subcategoria, di_ano.ano

										     FROM di_arquivo

									    LEFT JOIN di_subcategoria ON di_arquivo.id_subcategoria = di_subcategoria.id

									    LEFT JOIN di_categoria ON di_arquivo.id_categoria = di_categoria.id

									    LEFT JOIN di_ano ON di_arquivo.id_ano = di_ano.id

									  	    WHERE di_arquivo.id_cliente = '$cliente'

									  	      AND di_arquivo.status_registro = 'A'

									  	      AND (di_arquivo.descricao LIKE '%$busca%' OR di_subcategoria.subcategoria LIKE '%$busca%' OR di_categoria.categoria LIKE '%$busca%')")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryDiario);
				if(count($qryDiario) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $diarioLista . "$complemento'>Di&aacute;rio Oficial</a><span class='badge'>$contador</span></h3>";

					?>

					<div class="table-responsive">

						<table class="table table-striped table-hover table-bordered table-condensed">

							<tr>

								<th>&nbsp;</th>

								<th>Data Publica&ccedil;&atilde;o</th>

								<?php if($novoCliente == "43" || $novoCliente == "11992") { ?><th>Edi&ccedil;&atilde;o</th><?php } ?>

								<th>Arquivo</th>

								<th>Categoria</th>

								<th>Subcategoria</th>

							</tr>

							<?php foreach($qryDiario as $diario) { ?>

							<tr>

								<td>

									<a href="http://www.controlemunicipal.com.br/site/diario/publicacao.php?id=<?=$diario['id'] ?>&id_cliente=<?=$novoCliente ?>" class="btn btn-primary" data-toggle="tooltip" data-placement="right" title="Visualizar" target="_blank">

										<i class="glyphicon glyphicon-search"></i>

									</a>

								</td>

								<td class="text-right"><?= formata_data_hora($diario['data_insercao']) ?></td>

								<?php if($novoCliente == "43" || $novoCliente == "11992") { ?><td><?= $diario['sequencia'] ?></td><?php } ?>

								<td><?= $diario['descricao'] ?></td>

								<td><?= $diario['categoria'] ?></td>

								<td><?= $diario['subcategoria'] ?></td>

							</tr>

							<?php } ?>

						</table>

					</div>

					<?php

				}



				break;

			case "4":

				$qryLicitacao = $conn->query("SELECT licitacao_edital.*, licitacao_modalidade.descricao modalidade

											    FROM licitacao_edital, licitacao_modalidade

											   WHERE licitacao_edital.id_modalidade = licitacao_modalidade.id

												 AND licitacao_edital.id_cliente = '$cliente'

												 AND licitacao_edital.status_registro = 'A'

												 AND (licitacao_edital.titulo LIKE '%$busca%' OR licitacao_edital.objeto LIKE '%$busca%' OR licitacao_modalidade.descricao LIKE '%$busca%')")->fetchAll(PDO::FETCH_ASSOC);


				$contador = count($qryLicitacao);
				if(count($qryLicitacao) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $licitacaoModalidade . "$complemento'>Processos Licitat&oacute;rios</a><span class='badge'>$contador</span></h3>";

					echo '<div class="panel-group" id="accordion_licitacao" role="tablist" aria-multiselectable="true">';



					$qryFiltro = $conn->query("SELECT tipo FROM licitacao_filtro WHERE id_cliente = '$cliente' AND status_registro = 'A' ORDER BY id DESC LIMIT 1")->fetch(PDO::FETCH_ASSOC);

					$filtro = (empty($qryFiltro['tipo']) ? "P" : $qryFiltro['tipo']);



					foreach($qryLicitacao as $licitacao) {



					?>

		    		<div class="panel panel-default">

					    <div class="panel-heading" role="tab" id="heading_<?= $licitacao['id'] ?>">

					     	<h4 class="panel-title">

					        	<a role="button" data-toggle="collapse" data-parent="#accordion_licitacao" href="#collapse_<?= $licitacao['id'] ?>" aria-expanded="true" aria-controls="collapse_<?= $licitacao['id'] ?>">

					          		<div class="row">

					          			<div class="col-sm-6"><i class="glyphicon glyphicon-triangle-right"></i> <strong><?= $licitacao['titulo'] ?></strong></div>

					          			<div class="col-sm-3"><strong>Publica&ccedil;&atilde;o:</strong> <?= formata_data($licitacao['data_publicacao']) ?></div>

					          			<div class="col-sm-3"><strong>Abertura:</strong> <?= formata_data($licitacao['data_abertura']) ?><? if(!empty($licitacao['hora_abertura'])) echo " &agrave;s " . $licitacao['hora_abertura'] ?></div>

					          		</div>

					        	</a>

					    	</h4>

					    </div>

						<div id="collapse_<?= $licitacao['id'] ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_<?= $licitacao['id'] ?>">

							<div class="panel-body">

								<p><strong>Modalidade:</strong> <?= $licitacao['modalidade'] ?></p>

								<? if($licitacao['valor_maximo'] != "") { ?><p><strong>Valor M&aacute;ximo:</strong> <?= $licitacao['valor_maximo'] ?></p><? } ?>

								<p><strong>Objeto:</strong> <?= verifica($licitacao['objeto']) ?></p>



								<?php

								$stAnexo = $conn->prepare("SELECT * FROM licitacao_edital_anexo WHERE id_edital = :id_edital AND status_registro = :status_registro ORDER BY ordem ASC, id DESC");

								$stAnexo->execute(array("id_edital" => $licitacao['id'], "status_registro" => "A"));

								$qryAnexo = $stAnexo->fetchAll();



								if(count($qryAnexo)) {

								?>

								<div class="panel panel-primary">

									<div class="panel-heading">

								    	<h2 class="panel-title"><i class="glyphicon glyphicon-cloud-download"></i> Anexos</h2>

								  	</div>

									<div class="panel-body">

									<?php

									foreach ($qryAnexo as $anexo) {



										if($filtro == "B" || strtotime($licitacao['data_abertura']) <= strtotime(date("Y-m-d")) || $_SESSION['login_licitacao'] == true) {

										?>

										<p><strong><a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$licitacaoDownload.$complemento); ?>&id=<?= verifica($anexo['id']) ?>" class="text-danger"><i class="glyphicon glyphicon-cloud-download"></i> <?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?></a></strong></p>

										<?php

										} else if($configuracaoTransparencia['permitir_download_sem_cadastro'] == "S") {

										?>

										<p><strong><a href="#" role="button" data-toggle="modal" data-target="#anexo_<?= $anexo['id'] ?>"><i class="glyphicon glyphicon-cloud-download"></i> <?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?></a></a></strong></p>

										<div class="modal fade" id="anexo_<?= $anexo['id'] ?>">

									  		<div class="modal-dialog">

									    		<div class="modal-content">

									      			<div class="modal-header">

									        			<button type="button" class="close" data-dismiss="modal" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>

									        			<h4 class="modal-title">Baixar <?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?></h4>

									      			</div>

										      		<div class="modal-body text-center">

										      			<button type="button" class="btn btn-danger btn-margin" onclick="jQuery('#confirmacao_<?= $anexo['id'] ?>').removeClass('hidden'); return false;"><i class="glyphicon glyphicon-alert"></i> Download sem cadastro</button>

														<a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$licitacaoLogin.$complemento); ?>&id=<?= verifica($anexo['id']) ?>" class="btn btn-danger btn-margin"><i class="glyphicon glyphicon-user"></i> Download com cadastro</a><br><br>

														<div class="text-justify">

															<?php if(empty($configuracaoTransparencia['texto_download_sem_cadastro'])) { ?>

															O cadastro no sistema de licita&ccedil;&otilde;es garante o recebimento por e-mail de todas as informa&ccedil;&otilde;es pertinentes ao processo

															licitat&oacute;rio, como atas, avisos de revoga&ccedil;&atilde;o, retifica&ccedil;&otilde;es ou cancelamento. Caso optar por baixar o edital sem cadastramento, a pessoa

															f&iacute;sica ou jur&iacute;dica n&atilde;o receber&aacute; as informa&ccedil;&otilde;es atualizadas via e-mail.<br><br>

															&Eacute; de responsabilidade do fornecedor fazer acessos no site da licitante para verificar quaisquer altera&ccedil;&otilde;es/retifica&ccedil;&otilde;es nos editais de licita&ccedil;&atilde;o de seu interesse.<br><br>

															<?php

															} else {



																echo verifica($configuracaoTransparencia['texto_download_sem_cadastro']);



															}

															?>

															<label id="confirmacao_<?= $anexo['id'] ?>" class="hidden"><input type="checkbox" name="concordo_<?= $anexo['id'] ?>" id="concordo_<?= $anexo['id'] ?>" onclick="jQuery('#baixar_<?= $anexo['id'] ?>').toggle().removeClass('hidden');" /><strong>&nbsp;Li e concordo com o texto acima.</strong></label><br>

															<script>jQuery("#concordo_<?= $anexo['id'] ?>").prop("checked", false);</script>

														</div>

														<a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$licitacaoDownload.$complemento); ?>&confirm=1&id=<?= verifica($anexo['id']) ?>" id="baixar_<?= $anexo['id'] ?>" class="btn btn-primary text-center hidden">

															<i class="glyphicon glyphicon-cloud-download"></i> Baixar anexo

														</a>

										      		</div>

									    		</div>

									  		</div>

										</div>

										<?php

										} else {

										?>

										<p><strong><a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$licitacaoLogin.$complemento); ?>&id=<?= verifica($anexo['id']) ?>" class="text-danger"><i class="glyphicon glyphicon-cloud-download"></i> <?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?></a></strong></p>

										<?php } ?>

									<?php } ?>

									</div>

								</div>

								<?php } ?>

							</div>

						</div>

					</div>

		    		<?

					}



					echo "</div>";

				}

				break;

			case "5":

				$qryLegislacao = $conn->query("SELECT lei.*, lei_tipo.descricao tipo

												 FROM lei, lei_tipo

											    WHERE lei.id_tipo = lei_tipo.id

												  AND lei.id_cliente = '$cliente'

												  AND lei.status_registro = 'A'

												  AND (lei.numero LIKE '%$busca%' OR lei.sumula LIKE '%$busca%' OR lei_tipo.descricao LIKE '%$busca%')")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryLegislacao);
				if(count($qryLegislacao) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $legislacaoLista . "$complemento'>Legisla&ccedil;&atilde;o</a><span class='badge'>$contador</span></h3>";

					?>

					<div class="panel-group" id="accordion_lei" role="tablist" aria-multiselectable="true">

					<?php

					foreach ($qryLegislacao as $lei) {

					?>

					<div class="panel panel-default">

					    <div class="panel-heading" role="tab" id="heading_<?= $lei['id'] ?>">

					     	<h4 class="panel-title">

					        	<a role="button" data-toggle="collapse" data-parent="#accordion_lei" href="#collapse_<?= $lei['id'] ?>" aria-expanded="true" aria-controls="collapse_<?= $lei['id'] ?>">

					          		<div class="row">

					          			<div class="col-sm-4"><i class="glyphicon glyphicon-triangle-right"></i> <strong class="text-danger"> <?= $lei['tipo'] ?></strong></div>

					          			<div class="col-sm-4"><strong>N&ordm;</strong> <?= $lei['numero']?>/<?= $lei['ano']?><?php if($lei['projeto'] != "") { ?> - <strong>Projeto:</strong> <?= $lei['projeto'] ?><? } ?></div>

					          			<div class="col-sm-4 texto_resumido"><strong>S&uacute;mula:</strong> <?= strip_tags($lei['sumula']) ?></div>

					          		</div>

					        	</a>

					    	</h4>

					    </div>

						<div id="collapse_<?= $lei['id'] ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_<?= $lei['id'] ?>">

							<div class="panel-body">

								<p><strong>S&uacute;mula:</strong> <?= verifica($lei['sumula']) ?></p>

								<?php if($lei['observacao'] != "") { ?>

								<p><strong>Observa&ccedil;&otilde;es:</strong> <?= verifica($lei['observacao']) ?></p>

								<?php } ?>



								<?php

								$stAnexo = $conn->prepare("SELECT * FROM lei_anexo WHERE id_artigo = :id_artigo ORDER BY ordem ASC, id DESC");

								$stAnexo->execute(array("id_artigo" => $lei['id']));

								$qryAnexo = $stAnexo->fetchAll();



								if(count($qryAnexo)) {

								?>

								<div class="panel panel-primary">

									<div class="panel-heading">

								    	<h2 class="panel-title"><i class="glyphicon glyphicon-cloud-download"></i> Anexos</h2>

								  	</div>

									<div class="panel-body">

									<?php

									foreach ($qryAnexo as $anexo) {

									?>

										<p><strong><a href="<?= str_replace($cliente, $novoCliente, $CAMINHOARQ) ?>/<?= $anexo['arquivo'] ?>" target="_blank"><i class="glyphicon glyphicon-cloud-download"></i> <?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?></a></strong></p>

									<?php } ?>

									</div>

								</div>

								<?php } ?>



								<?php

								$stLink = $conn->prepare("SELECT * FROM lei_link WHERE id_artigo = :id_artigo ORDER BY ordem ASC, id DESC");

								$stLink->execute(array("id_artigo" => $lei['id']));

								$qryLink = $stLink->fetchAll();



								if(count($qryLink)) {

								?>

								<div class="panel panel-primary">

									<div class="panel-heading">

								    	<h2 class="panel-title"><i class="glyphicon glyphicon-globe"></i> Links</h2>

								  	</div>

									<div class="panel-body">

									<?php

									foreach ($qryLink as $linkLei) {

									?>

										<p><strong><a href="<?= $linkLei['link'] ?>" target="_blank"><i class="glyphicon glyphicon-globe"></i> <?= empty($linkLei['descricao']) ? "Link" : $linkLei['descricao'] ?></a></strong></p>

									<?php } ?>

									</div>

								</div>

								<?php } ?>

							</div>

						</div>

					</div>

					<?php } ?>

					</div>

					<?php

				}



				break;

			case "7":

				$qryOrganograma = $conn->query("SELECT * FROM organograma WHERE titulo LIKE '%$busca%' AND id_cliente = '$cliente' AND status_registro = 'A'")->fetchAll(PDO::FETCH_ASSOC);



					$contador = count($qryOrganograma);
				if(count($qryOrganograma) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $organograma . "$complemento'>Organograma</a><span class='badge'>$contador</span></h3>";



					foreach($qryOrganograma as $organograma) {



						echo "<p style='margin: 3px 0px;'><a href='$CAMINHO/index.php?sessao=$sequencia" . $organogramaVisualiza . "$complemento&id=$organograma[id]'><i class='glyphicon glyphicon-list-alt'></i><strong>&nbsp;$organograma[titulo]</strong></a></p>";



					}

					echo "<p>&nbsp;</p>";



				}



				break;

			case "8":

				$qryConcurso = $conn->query("SELECT *

											   FROM concurso

											  WHERE id_cliente = '$cliente'

											    AND status_registro = 'A'

											    AND (titulo LIKE '%$busca%' OR artigo LIKE '%$busca%')

										   ORDER BY data_publicacao DESC, id DESC")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryConcurso);
				if(count($qryConcurso) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $concursoLista . "$complemento'>Concursos</a><span class='badge'>$contador</span></h3>";

					?>

					<div class="panel-group" id="accordion_concurso" role="tablist" aria-multiselectable="true">



					<?php foreach($qryConcurso as $concurso) { ?>

					<div class="panel panel-default">

					    <div class="panel-heading" role="tab" id="heading_<?= $concurso['id'] ?>">

					     	<h4 class="panel-title">

					        	<a role="button" data-toggle="collapse" data-parent="#accordion_concurso" href="#collapse_<?= $concurso['id'] ?>" aria-expanded="true" aria-controls="collapse_<?= $concurso['id'] ?>">

					          		<i class="glyphicon glyphicon-triangle-right"></i> <strong><?= $concurso['titulo'] ?></strong>

					        	</a>

					    	</h4>

					    </div>

						<div id="collapse_<?= $concurso['id'] ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_<?= $concurso['id'] ?>">

							<div class="panel-body">

								<p><strong>Data da Publica&ccedil;&atilde;o:</strong> <?= formata_data($concurso['data_publicacao']) ?></p>

								<p><strong>Data da Prova:</strong> <?= formata_data($concurso['data_concurso']) ?></p>

								<?= verifica($concurso['artigo']) ?>

								<?php

								$conf1 = (strtotime(date("Y-m-d")) >= strtotime($concurso['data_inicio_inscricao'])) && (strtotime(date("Y-m-d")) <= strtotime($concurso['data_fim_inscricao']));

								$conf2 = (strtotime(date("Y-m-d")) > strtotime($concurso['data_fim_inscricao'])) && (strtotime(date("Y-m-d")) <= strtotime($concurso['data_limite_recurso']));



								$stVaga = $conn->prepare("SELECT * FROM concurso_vaga WHERE id_cliente = :id_cliente AND id_artigo = :id_artigo AND status_registro = :status_registro");

								$stVaga->execute(array("id_cliente" => $cliente, "id_artigo" => $concurso['id'], "status_registro" => "A"));

								$qryVaga = $stVaga->fetchAll();



								if($concurso['inscricao'] == "S" && ($conf1 || $conf2) && count($qryVaga)) {

								?>

								<div class="panel panel-danger">

									<div class="panel-heading">

								    	<h2 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i> <?= $conf1 ? "FA&Ccedil;A SUA INSCRI&Ccedil;&Atilde;O" : "&Aacute;REA DO CANDIDATO" ?></h2>

								  	</div>

									<div class="panel-body">

										<?php

										foreach ($qryVaga as $vaga) {

										?>

											<p><a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$concursoInscricao.$complemento); ?>&id_vaga=<?= $vaga['id'] ?>"><strong><i class="glyphicon glyphicon-triangle-right"></i>&nbsp;<?= $vaga['vaga'] ?></strong></a></p>

										<?php } ?>

									</div>

								</div>

								<?php } ?>

								<?php

								$stAnexo = $conn->prepare("SELECT * FROM concurso_anexo WHERE id_artigo = :id_artigo ORDER BY ordem ASC, id DESC");

								$stAnexo->execute(array("id_artigo" => $concurso['id']));

								$qryAnexo = $stAnexo->fetchAll();



								if(count($qryAnexo)) {

								?>

								<div class="panel panel-primary">

									<div class="panel-heading">

								    	<h2 class="panel-title"><i class="glyphicon glyphicon-cloud-download"></i> Anexos</h2>

								  	</div>

									<div class="panel-body">

									<?php

									foreach ($qryAnexo as $anexo) {

									?>

										<p><strong><a href="<?= $CAMINHOARQ ?>/<?= $anexo['arquivo'] ?>" target="_blank"><i class="glyphicon glyphicon-cloud-download"></i> <?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?></a></strong></p>

									<?php } ?>

									</div>

								</div>

								<?php } ?>



								<?php

								$stLink = $conn->prepare("SELECT * FROM concurso_link WHERE id_artigo = :id_artigo ORDER BY ordem ASC, id DESC");

								$stLink->execute(array("id_artigo" => $concurso['id']));

								$qryLink = $stLink->fetchAll();



								if(count($qryLink)) {

								?>

								<div class="panel panel-primary">

									<div class="panel-heading">

								    	<h2 class="panel-title"><i class="glyphicon glyphicon-globe"></i> Links</h2>

								  	</div>

									<div class="panel-body">

									<?php

									foreach ($qryLink as $linkConcurso) {

									?>

										<p><strong><a href="<?= $linkConcurso['link'] ?>" target="_blank"><i class="glyphicon glyphicon-globe"></i> <?= empty($linkConcurso['descricao']) ? "Link" : $linkConcurso['descricao'] ?></a></strong></p>

									<?php } ?>

									</div>

								</div>

								<?php } ?>

							</div>

						</div>

					</div>

					<?php } ?>

				</div>

				<?php

				}



				break;

			case "12":

				$qryPlano = $conn->query("SELECT pref_plano_diretor.*, pref_plano_diretor_categoria.categoria

										    FROM pref_plano_diretor, pref_plano_diretor_categoria

										   WHERE pref_plano_diretor.id_categoria = pref_plano_diretor_categoria.id

											 AND pref_plano_diretor_categoria.id_cliente = '$cliente'

											 AND pref_plano_diretor.status_registro = 'A'

											 AND (pref_plano_diretor.titulo LIKE '%$busca%' OR pref_plano_diretor_categoria.categoria LIKE '%$busca%')

									    ORDER BY pref_plano_diretor.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryPlano);
				if(count($qryPlano) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $plano . "$complemento'>Plano Diretor</a><span class='badge'>$contador</span></h3>";



					foreach($qryPlano as $plano) {



						echo "<p style='margin: 3px 0px;'><a href='$CAMINHO/index.php?sessao=$sequencia" . $planoVisualiza . "$complemento&id=$plano[id]'><i class='glyphicon glyphicon-list-alt'></i><strong>&nbsp;$plano[categoria]&nbsp;&raquo;&nbsp;$plano[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}



				break;

			case "13":

				$qryAudiencia = $conn->query("SELECT * FROM audiencia_publica WHERE titulo LIKE '%$busca%' AND id_cliente = '$cliente' AND status_registro = 'A'")->fetchAll(PDO::FETCH_ASSOC);



					$contador = count($qryAudiencia);
				if(count($qryAudiencia) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $audienciaPublica . "$complemento'>Audi&ecirc;ncia P&uacute;blica</a><span class='badge'>$contador</span></h3>";

					?>

					<div class="panel-group" id="accordion_audiencia" role="tablist" aria-multiselectable="true">

					<?php foreach($qryAudiencia as $audiencia) { ?>

					<div class="panel panel-default">

					    <div class="panel-heading" role="tab" id="heading_<?= $audiencia['id'] ?>">

					     	<h4 class="panel-title">

					        	<a role="button" data-toggle="collapse" data-parent="#accordion_audiencia" href="#collapse_<?= $audiencia['id'] ?>" aria-expanded="true" aria-controls="collapse_<?= $audiencia['id'] ?>">

					          		<div class="row">

					          			<div class="col-sm-4"><i class="glyphicon glyphicon-triangle-right"></i> <strong><?= $audiencia['titulo'] ?></strong></div>

					          			<div class="col-sm-4"><strong>Publica&ccedil;&atilde;o:</strong> <?= formata_data_hora($audiencia['data_publicacao']) ?></div>

					          			<div class="col-sm-4"><strong>Audi&ecirc;ncia:</strong> <?= formata_data($audiencia['data_audiencia']) ?></div>

					          		</div>

					        	</a>

					    	</h4>

					    </div>

						<div id="collapse_<?= $audiencia['id'] ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_<?= $audiencia['id'] ?>">

							<div class="panel-body">

								<?= verifica($audiencia['artigo']) ?>



								<?php

								$stAnexo = $conn->prepare("SELECT * FROM audiencia_publica_anexo WHERE id_artigo = :id_artigo ORDER BY ordem ASC, id DESC");

								$stAnexo->execute(array("id_artigo" => $audiencia['id']));

								$qryAnexo = $stAnexo->fetchAll();



								if(count($qryAnexo)) {

								?>

								<div class="panel panel-primary">

									<div class="panel-heading">

								    	<h2 class="panel-title"><i class="glyphicon glyphicon-cloud-download"></i> Anexos</h2>

								  	</div>

									<div class="panel-body">

									<?php

									foreach ($qryAnexo as $anexo) {

									?>

										<p><strong><a href="<?= $CAMINHOARQ ?>/<?= $anexo['arquivo'] ?>" target="_blank"><i class="glyphicon glyphicon-cloud-download"></i> <?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?></a></strong></p>

									<?php } ?>

									</div>

								</div>

								<?php } ?>



								<?php

								$stLink = $conn->prepare("SELECT * FROM audiencia_publica_link WHERE id_artigo = :id_artigo ORDER BY ordem ASC, id DESC");

								$stLink->execute(array("id_artigo" => $audiencia['id']));

								$qryLink = $stLink->fetchAll();



								if(count($qryLink)) {

								?>

								<div class="panel panel-primary">

									<div class="panel-heading">

								    	<h2 class="panel-title"><i class="glyphicon glyphicon-globe"></i> Links</h2>

								  	</div>

									<div class="panel-body">

									<?php

									foreach ($qryLink as $linkAudiencia) {

									?>

										<p><strong><a href="<?= $linkAudiencia['link'] ?>" target="_blank"><i class="glyphicon glyphicon-globe"></i> <?= empty($linkAudiencia['descricao']) ? "Link" : $linkAudiencia['descricao'] ?></a></strong></p>

									<?php } ?>

									</div>

								</div>

								<?php } ?>

							</div>

						</div>

					</div>

					<?php } ?>

					</div>

					<?php

				}

				break;

			case "14":

				$qryConvenio = $conn->query("SELECT convenio.*, convenio_categoria.descricao categoria

										  	   FROM convenio, convenio_categoria

										  	  WHERE convenio.id_categoria = convenio_categoria.id

											    AND convenio_categoria.id_cliente = '$cliente'

											    AND convenio.status_registro = 'A'

											    AND (convenio.titulo LIKE '%$busca%' OR convenio_categoria.descricao LIKE '%$busca%')

									   	   ORDER BY convenio.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryConvenio);
				if(count($qryConvenio) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $convenio . "$complemento'>Conv&ecirc;nios</a><span class='badge'>$contador</span></h3>";

					?>

					<div class="panel-group" id="accordion_convenio" role="tablist" aria-multiselectable="true">

					<?php foreach($qryConvenio as $convenio) { ?>

					<div class="panel panel-default">

					    <div class="panel-heading" role="tab" id="heading_<?= $convenio['id'] ?>">

					     	<h4 class="panel-title">

					        	<a role="button" data-toggle="collapse" data-parent="#accordion_convenio" href="#collapse_<?= $convenio['id'] ?>" aria-expanded="true" aria-controls="collapse_<?= $convenio['id'] ?>">

					          		<i class="glyphicon glyphicon-triangle-right"></i> <?= $convenio['titulo'] ?>

					        	</a>

					    	</h4>

					    </div>

						<div id="collapse_<?= $convenio['id'] ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_<?= $convenio['id'] ?>">

							<div class="panel-body">

								<?php if($convenio['data_inicio'] != "") { ?><strong>Data de In&iacute;cio:</strong> <?= formata_data($convenio['data_inicio']) ?><br><?php } ?>

								<?php if($convenio['data_fim'] != "") { ?><strong>Fim da Vig&ecirc;ncia:</strong> <?= formata_data($convenio['data_fim']) ?><br><?php } ?>

								<?php if($convenio['numero'] != "") { ?><strong>N&ordm; do Processo:</strong> <?= $convenio['numero'] ?><br><?php } ?>

								<?php if($convenio['valor'] != "") { ?><strong>Valor:</strong> <?= $convenio['valor'] ?><br><?php } ?>

								<?php if($convenio['contrapartida'] != "") { ?><strong>Contra partida:</strong> <?= $convenio['contrapartida'] ?><br><?php } ?>

								<?php if($convenio['proponente'] != "") { ?><strong>Proponente:</strong> <?= $convenio['proponente'] ?><br><?php } ?>

								<?php if($convenio['fornecedor'] != "") { ?><strong>Fornecedor:</strong> <?= $convenio['fornecedor'] ?><br><?php } ?>

								<?php if($convenio['objeto'] != "") { ?><strong>Objeto:</strong> <?= verifica($convenio['objeto']) ?><br><?php } ?>



								<?php

								$stAnexo = $conn->prepare("SELECT * FROM convenio_anexo WHERE id_artigo = :id_artigo ORDER BY ordem ASC, id DESC");

								$stAnexo->execute(array("id_artigo" => $convenio['id']));

								$qryAnexo = $stAnexo->fetchAll();



								if(count($qryAnexo)) {

								?>

								<div class="panel panel-primary">

									<div class="panel-heading">

								    	<h2 class="panel-title"><i class="glyphicon glyphicon-cloud-download"></i> Anexos</h2>

								  	</div>

									<div class="panel-body">

									<?php

									foreach ($qryAnexo as $anexo) {

									?>

										<p><strong><a href="<?= $CAMINHOARQ ?>/<?= $anexo['arquivo'] ?>" target="_blank"><i class="glyphicon glyphicon-cloud-download"></i> <?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?></a></strong></p>

									<?php } ?>

									</div>

								</div>

								<?php } ?>



								<?php

								$stLink = $conn->prepare("SELECT * FROM convenio_link WHERE id_artigo = :id_artigo ORDER BY ordem ASC, id DESC");

								$stLink->execute(array("id_artigo" => $convenio['id']));

								$qryLink = $stLink->fetchAll();



								if(count($qryLink)) {

								?>

								<div class="panel panel-primary">

									<div class="panel-heading">

								    	<h2 class="panel-title"><i class="glyphicon glyphicon-globe"></i> Links</h2>

								  	</div>

									<div class="panel-body">

									<?php

									foreach ($qryLink as $linkConvenio) {

									?>

										<p><strong><a href="<?= $linkConvenio['link'] ?>" target="_blank"><i class="glyphicon glyphicon-globe"></i> <?= empty($linkConvenio['descricao']) ? "Link" : $linkConvenio['descricao'] ?></a></strong></p>

									<?php } ?>

									</div>

								</div>

								<?php } ?>

							</div>

						</div>

					</div>

					<?php } ?>

					</div>

					<?php

				}



				break;

			case "16":

				$qryServidor = $conn->query("SELECT * FROM servidor WHERE id_cliente = '$cliente' AND status_registro = 'A' AND (nome LIKE '%$busca%' OR cargo LIKE '%$busca%')")->fetchAll(PDO::FETCH_ASSOC);



				$qryServidorAnexo = $conn->query("SELECT servidor_anexo_link.*, servidor_categoria.descricao categoria

												    FROM servidor_anexo_link, servidor_categoria

												   WHERE servidor_anexo_link.id_categoria = servidor_categoria.id

												     AND servidor_categoria.id_cliente = '$cliente'

												     AND servidor_anexo_link.status_registro = 'A'

												     AND (servidor_anexo_link.titulo LIKE '%$busca%' OR servidor_categoria.descricao LIKE '%$busca%')

											    ORDER BY servidor_anexo_link.titulo")->fetchAll(PDO::FETCH_ASSOC);



				if(count($qryServidor) || count($qryServidorAnexo)) {



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $servidorLista . "$complemento'>Servidores</a><span class='badge'>$contador</span></h3>";



					foreach($qryServidor as $servidor) {



						echo "<p style='margin: 3px 0px;'><a href='$CAMINHO/index.php?sessao=$sequencia" . $servidorVisualiza . "$complemento&id=$servidor[id]'><i class='glyphicon glyphicon-list-alt'></i><strong>&nbsp;$servidor[nome]&nbsp;($servidor[cargo])</strong></a></p>";



					}



					foreach($qryServidorAnexo as $servidorAnexo) {



						if($servidorAnexo['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$servidorAnexo[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$servidorAnexo[categoria]&nbsp;&raquo;&nbsp;$servidorAnexo[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$servidorAnexo[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$servidorAnexo[categoria]&nbsp;&raquo;&nbsp;$servidorAnexo[titulo]</strong></a></p>";



					}



				}



				break;

			case "17":

				$qryContrato = $conn->query("SELECT *

											   FROM contrato_transparencia

											  WHERE id_cliente = '$cliente'

											    AND status_registro = 'A'

											    AND (contratado LIKE '%$busca%' OR numero LIKE '%$busca%' OR objeto LIKE '%$busca%')

										   ORDER BY data_assinatura DESC, id DESC")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryContrato);
				if(count($qryContrato) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $contrato . "$complemento'>Contratos</a><span class='badge'>$contador</span></h3>";



					echo '<div class="panel-group" id="accordion_contrato" role="tablist" aria-multiselectable="true">';



					foreach ($qryContrato as $contrato) {

					?>

					<div class="panel panel-default">

					    <div class="panel-heading" role="tab" id="heading_<?= $contrato['id'] ?>">

					     	<h4 class="panel-title">

					        	<a role="button" data-toggle="collapse" data-parent="#accordion_contrato" href="#collapse_<?= $contrato['id'] ?>" aria-expanded="true" aria-controls="collapse_<?= $contrato['id'] ?>">

					          		<div class="row">

					          			<div class="col-sm-6"><i class="glyphicon glyphicon-triangle-right"></i> <strong>Contratado:</strong> <?= $contrato['contratado'] ?></div>

					          			<div class="col-sm-3"><strong>Processo:</strong> <?= $contrato['processo'] ?></div>

					          			<div class="col-sm-3"><strong>N&uacute;mero:</strong> <?= $contrato['numero'] ?></div>

					          		</div>

					        	</a>

					    	</h4>

					    </div>

						<div id="collapse_<?= $contrato['id'] ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_<?= $contrato['id'] ?>">

							<div class="panel-body">

								<?php if($contrato['cnpj'] != "") { ?><p><strong>CNPJ:</strong> <?= $contrato['cnpj'] ?></p><?php } ?>

								<p><strong>Domic&iacute;lio:</strong> <?= $contrato['domicilio'] ?></p>

								<p><strong>Data da Assinatura:</strong> <?= formata_data($contrato['data_assinatura']) ?></p>

								<p><strong>Vig&ecirc;ncia:</strong> <?= $contrato['vigencia'] ?></p>

								<p><strong>N&uacute;mero:</strong> <?= $contrato['numero'] ?></p>

								<?php if($contrato['modalidade'] != "") { ?><p><strong>Modalidade:</strong> <?= $contrato['modalidade'] ?></p><?php } ?>

								<p><strong>Valor Global:</strong> <?= $contrato['valor_global'] ?></p>

								<p><strong>Objeto:</strong> <?= verifica($contrato['objeto']) ?></p>

								<?php if($contrato['aditivos'] != "") { ?><p><strong>Aditivos:</strong> <?= $contrato['aditivos'] ?></p><?php } ?>



								<?php

								$stAnexo = $conn->prepare("SELECT * FROM contrato_transparencia_anexo WHERE id_artigo = :id_artigo ORDER BY ordem ASC, id DESC");

								$stAnexo->execute(array("id_artigo" => $contrato['id']));

								$qryAnexo = $stAnexo->fetchAll();



								if(count($qryAnexo)) {

								?>

								<div class="panel panel-primary">

									<div class="panel-heading">

								    	<h2 class="panel-title"><i class="glyphicon glyphicon-cloud-download"></i> Anexos</h2>

								  	</div>

									<div class="panel-body">

										<?php

										foreach ($qryAnexo as $anexo) {

										?>

										<p><strong><a href="<?= $CAMINHOARQ ?>/<?= $anexo['arquivo'] ?>" target="_blank"><i class="glyphicon glyphicon-cloud-download"></i> <?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?></a></strong></p>

										<?php } ?>

									</div>

								</div>

								<?php } ?>

							</div>

						</div>

					</div>

					<?php

					}



					echo "</div>";

				}

				break;

			case "18":

				$qryDiaria = $conn->query("SELECT diaria.*, diaria_categoria.descricao categoria

											 FROM diaria, diaria_categoria

										    WHERE diaria.id_categoria = diaria_categoria.id

											  AND diaria_categoria.id_cliente = '$cliente'

											  AND diaria.status_registro = 'A'

											  AND (diaria.titulo LIKE '%$busca%' OR diaria_categoria.descricao LIKE '%$busca%')

										 ORDER BY diaria.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryDiaria);
				if(count($qryDiaria) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $diariaLista . "$complemento'>Di&aacute;rias</a><span class='badge'>$contador</span></h3>";

					echo '<ul class="treeview">';



					foreach($qryDiaria as $diaria) {



						if($diaria['link'] != "") {



							$linkDiaria = $diaria['link'];

							$targetDiaria = "_blank";

							$imagem = "glyphicon-globe";



						} else if($diaria['arquivo'] != "") {



							$linkDiaria = $CAMINHOARQ . "/" . $diaria['arquivo'];

							$targetDiaria = "_blank";

							$imagem = "glyphicon-cloud-download";



						} else {



							$linkDiaria = "#";

							$targetDiaria = "_self";

							$imagem = "glyphicon-chevron-right";



						}

					?>

					<li>

						<a href="<?= $linkDiaria ?>" target="<?= $targetDiaria ?>"><span class="text-primary"><i class="glyphicon <?= $imagem ?>"></i>&nbsp;&nbsp;<?= $diaria['categoria'] ?>&nbsp;&raquo;&nbsp;<?= $diaria['titulo'] ?></span></a><br>

						<?php if($diaria['destino'] != "") { ?><strong style="margin-left: 22px;">Destino:</strong> <?= $diaria['destino'] ?><br><?php } ?>

						<?php if($diaria['beneficiario'] != "") { ?><strong style="margin-left: 22px;">Benefici&aacute;rio:</strong> <?= $diaria['beneficiario'] ?><br><?php } ?>

		                <?php if($diaria['rg_beneficiario'] != "") { ?><strong style="margin-left: 22px;">RG do Benefici&aacute;rio:</strong> <?= $diaria['rg_beneficiario'] ?><br><?php } ?>

		                <?php if($diaria['cargo'] != "") { ?><strong style="margin-left: 22px;">Cargo:</strong> <?= $diaria['cargo'] ?><br><?php } ?>

		                <?php if($diaria['justificativa'] != "") { ?><strong style="margin-left: 22px;">Justificativa:</strong> <?= $diaria['justificativa'] ?><br><?php } ?>

		                <?php if($diaria['data_inicio_viagem'] != "") { ?><strong style="margin-left: 22px;">Data Inicial da Viagem:</strong> <?= formata_data($diaria['data_inicio_viagem']) ?><br><?php } ?>

		                <?php if($diaria['data_fim_viagem'] != "") { ?><strong style="margin-left: 22px;">Data Final da Viagem:</strong> <?= formata_data($diaria['data_fim_viagem']) ?><br><?php } ?>

		                <?php if($diaria['meio_transporte'] != "") { ?><strong style="margin-left: 22px;">Meio de Transporte:</strong> <?= $diaria['meio_transporte'] ?><br><?php } ?>

		                <?php if($diaria['custo_meio_transporte'] != "") { ?><strong style="margin-left: 22px;">Custo do Meio de Transporte:</strong> <?= $diaria['custo_meio_transporte'] ?><br><?php } ?>

		                <?php if($diaria['quantidade_diaria'] != "") { ?><strong style="margin-left: 22px;">Quantidade de Di&aacute;rias:</strong> <?= $diaria['quantidade_diaria'] ?><br><?php } ?>

		                <?php if($diaria['valor_unitario_diaria'] != "") { ?><strong style="margin-left: 22px;">Valor Unit&aacute;rio da Di&aacute;ria:</strong> <?= $diaria['valor_unitario_diaria'] ?><br><?php } ?>

		                <?php if($diaria['certificado'] != "") { ?><strong style="margin-left: 22px;"><span class="text-primary"><a href="<?= $CAMINHOARQ ?>/<?= $diaria['certificado'] ?>" target="_blank"><i class="glyphicon glyphicon-cloud-download"></i>Baixar Certificado</a><?php } ?>

					</li>

					<?php

					}



					echo "</ul>";



				}

				break;

			case "19":

				$qryOutros = $conn->query("SELECT outros_documentos.*, outros_documentos_categoria.descricao categoria, outros_documentos_subcategoria.descricao subcategoria

											 FROM outros_documentos

									    LEFT JOIN outros_documentos_subcategoria ON outros_documentos.id_subcategoria = outros_documentos_subcategoria.id

									    LEFT JOIN outros_documentos_categoria ON outros_documentos.id_categoria = outros_documentos_categoria.id

										    WHERE outros_documentos_categoria.id_cliente = '$cliente'

											  AND outros_documentos.status_registro = 'A'

											  AND (outros_documentos.titulo LIKE '%$busca%' OR outros_documentos_subcategoria.descricao LIKE '%$busca%' OR outros_documentos_categoria.descricao LIKE '%$busca%')")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryOutros);
				if(count($qryOutros) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $outrosDocumentos . "$complemento'>Outros Documentos</a><span class='badge'>$contador</span></h3>";



					foreach($qryOutros as $outros) {



						if($outros['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$outros[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$outros[categoria]&nbsp;&raquo;&nbsp;$outros[subcategoria]&nbsp;&raquo;&nbsp;$outros[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$outros[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$outros[categoria]&nbsp;&raquo;&nbsp;$outros[subcategoria]&nbsp;&raquo;&nbsp;$outros[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}

				break;

			case "20":

			    $qryDespesa = $conn->query("SELECT despesa.*, despesa_categoria.descricao categoria

                        			          FROM despesa, despesa_categoria

                        			         WHERE despesa.id_categoria = despesa_categoria.id

                        			           AND despesa_categoria.id_cliente = '$cliente'

                        			           AND despesa.status_registro = 'A'

                        			           AND (despesa.titulo LIKE '%$busca%' OR despesa_categoria.descricao LIKE '%$busca%')

                        			      ORDER BY despesa.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryDespesa);
			 	if(count($qryDespesa) > 0) {
					 $contaResultados++;



    				echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $despesa . "$complemento'>Despesas</a><span class='badge'>$contador</span></h3>";



    				foreach($qryDespesa as $despesa) {



	    				if($despesa['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$despesa[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$despesa[categoria]&nbsp;&raquo;&nbsp;$despesa[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$despesa[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$despesa[categoria]&nbsp;&raquo;&nbsp;$despesa[titulo]</strong></a></p>";



			         }



			         echo "<p>&nbsp;</p>";



			    }



			    break;

			case "21":

				$qryEndereco = $conn->query("SELECT endereco_oficial.*, municipio.nome municipio, estado.uf estado

											   FROM endereco_oficial

										  LEFT JOIN municipio ON endereco_oficial.id_municipio = municipio.id

										  LEFT JOIN estado ON municipio.id_estado = estado.id

											  WHERE endereco_oficial.id_cliente = '$cliente'

											    AND endereco_oficial.status_registro = 'A'

											    AND endereco_oficial.titulo LIKE '%$busca%'

										   ORDER BY endereco_oficial.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryEndereco);
				if(count($qryEndereco) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $enderecoOficial . "$complemento'>Endere&ccedil;os Oficiais</a><span class='badge'>$contador</span></h3>";



					foreach($qryEndereco as $endereco) {

					?>

					<address>

						<strong class="h4"><?= $endereco['titulo'] ?></strong><br>

						<i class="glyphicon glyphicon-road"></i> <?= $endereco['endereco'] ?>, <?= $endereco['bairro'] ?> - <?= $endereco['complemento'] != "" ? "$endereco[complemento] - " : "" ?><br>

						<i class="glyphicon glyphicon-map-marker"></i> <strong>CEP:</strong> <?= $endereco['cep'] ?> - <?= $endereco['municipio'] ?> - <?= $endereco['estado'] ?><br>

						<? if($endereco['telefone'] != "") { ?><i class="glyphicon glyphicon-phone-alt"></i> <strong>Telefone(s):</strong> <?= $endereco['telefone'] ?><br><? } ?>

						<? if($endereco['email'] != "") { ?><i class="glyphicon glyphicon-envelope"></i> <strong>E-mail:</strong> <?= $endereco['email'] ?><br><? } ?>

						<? if($endereco['site'] != "") { ?><i class="glyphicon glyphicon-globe"></i> <strong>Website:</strong> <a href="<?= $endereco['site'] ?>" target="_blank"><?= $endereco['site'] ?></a><br><? } ?>

						<? if($endereco['horario_atendimento'] != "") { ?><i class="glyphicon glyphicon glyphicon-time"></i> <strong>Hor&aacute;rio de Atendimento:</strong> <?= $endereco['horario_atendimento'] ?><? } ?>

					</address>

					<?php

					}



				}



				break;

            case "22":

                $qryReceita = $conn->query("SELECT receita.*, receita_categoria.descricao categoria

                                              FROM receita, receita_categoria

                                             WHERE receita.id_categoria = receita_categoria.id

                                               AND receita_categoria.id_cliente = '$cliente'

                                               AND receita.status_registro = 'A'

                                               AND (receita.titulo LIKE '%$busca%' OR receita_categoria.descricao LIKE '%$busca%')

                                          ORDER BY receita.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryReceita);
                if(count($qryReceita) > 0) {
					$contaResultados++;



                    echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $receita . "$complemento'>Receitas</a><span class='badge'>$contador</span></h3>";



                    foreach($qryReceita as $receita) {



                        if($receita['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$receita[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$receita[categoria]&nbsp;&raquo;&nbsp;$receita[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$receita[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$receita[categoria]&nbsp;&raquo;&nbsp;$receita[titulo]</strong></a></p>";



                    }



                echo "<p>&nbsp;</p>";



                }



                break;

			case "23":

				$qryPlano = $conn->query("SELECT plano_conta.*, plano_conta_categoria.descricao categoria

										    FROM plano_conta, plano_conta_categoria

										   WHERE plano_conta.id_categoria = plano_conta_categoria.id

											 AND plano_conta_categoria.id_cliente = '$cliente'

											 AND plano_conta.status_registro = 'A'

											 AND (plano_conta.titulo LIKE '%$busca%' OR plano_conta_categoria.descricao LIKE '%$busca%')

									    ORDER BY plano_conta.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryPlano);
				if(count($qryPlano) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $planoConta . "$complemento'>Plano de Contas</a><span class='badge'>$contador</span></h3>";



					foreach($qryPlano as $plano) {



						if($plano['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$plano[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$plano[categoria]&nbsp;&raquo;&nbsp;$plano[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$plano[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$plano[categoria]&nbsp;&raquo;&nbsp;$plano[titulo]</strong></a></p>";



					}



				}



				break;

			case "24":

				$qryPatrimonio = $conn->query("SELECT patrimonio.*, patrimonio_categoria.descricao categoria, patrimonio_subcategoria.descricao subcategoria

											     FROM patrimonio

											LEFT JOIN patrimonio_categoria ON patrimonio.id_categoria = patrimonio_categoria.id

											LEFT JOIN patrimonio_subcategoria ON patrimonio.id_subcategoria = patrimonio_subcategoria.id

											    WHERE patrimonio_categoria.id_cliente = '$cliente'

												  AND patrimonio.status_registro = 'A'

												  AND (patrimonio.titulo LIKE '%$busca%' OR patrimonio_categoria.descricao LIKE '%$busca%' OR patrimonio_subcategoria.descricao LIKE '%$busca%')

										     ORDER BY patrimonio.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryPatrimonio);
				if(count($qryPatrimonio) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $patrimonio . "$complemento'>Patrim&ocirc;nio</a><span class='badge'>$contador</span></h3>";



					foreach($qryPatrimonio as $patrimonio) {



						if($patrimonio['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$patrimonio[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$patrimonio[categoria]&nbsp;&raquo;&nbsp;$patrimonio[subcategoria]&nbsp;&raquo;&nbsp;$patrimonio[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$patrimonio[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$patrimonio[categoria]&nbsp;&raquo;&nbsp;$patrimonio[subcategoria]&nbsp;&raquo;&nbsp;$patrimonio[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}



				break;

			case "25":

				$qryVereador = $conn->query("SELECT * FROM vereador WHERE id_cliente = '$cliente' AND status_registro = 'A' AND (nome LIKE '%$busca%' OR cargo LIKE '%$busca%')")->fetchAll(PDO::FETCH_ASSOC);



				$qryVereadorAnexo = $conn->query("SELECT vereador_anexo_link.*, vereador_categoria.descricao categoria

												    FROM vereador_anexo_link, vereador_categoria

												   WHERE vereador_anexo_link.id_categoria = vereador_categoria.id

												     AND vereador_categoria.id_cliente = '$cliente'

												     AND vereador_anexo_link.status_registro = 'A'

												     AND (vereador_anexo_link.titulo LIKE '%$busca%' OR vereador_categoria.descricao LIKE '%$busca%')

											    ORDER BY vereador_anexo_link.titulo")->fetchAll(PDO::FETCH_ASSOC);



				if(count($qryVereador) || count($qryVereadorAnexo)) {



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $vereadorLista . "$complemento'>Vereadores</a><span class='badge'>$contador</span></h3>";



					if(count($qryVereador)) {

					?>

					<div class="table-responsive">

						<table class="table table-striped table-hover table-bordered table-condensed">

							<tr>

								<th>Nome</th>

								<th>Cargo</th>

								<th>Sal&aacute;rio</th>

								<th>Anexo</th>

							</tr>

						<?php

						foreach($qryVereador as $vereador) {

						?>

						<tr>

							<td><?= $vereador['nome'] ?></td>

							<td><?= $vereador['cargo'] ?></td>

							<td><?= formata_valor($vereador['salario']) ?></td>

							<td>

								<?php if($vereador['arquivo'] != "") { ?>

								<a href="<?= $CAMINHOARQ ?>/<?= $vereador['arquivo'] ?>" class="btn btn-warning" data-toggle="tooltip" data-placement="left" title="Baixar arquivo" target="_blank">

									<i class="glyphicon glyphicon-cloud-download"></i>

								</a>

								<?php } else { ?>

								<button type="button" class="btn btn-warning" disabled><i class="glyphicon glyphicon-cloud-download"></i></button>

								<?php } ?>

							</td>

						</tr>

						<?php } ?>

						</table>

					</div>

					<?php



					}



					foreach($qryVereadorAnexo as $vereadorAnexo) {



						if($vereadorAnexo['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$vereadorAnexo[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$vereadorAnexo[categoria]&nbsp;&raquo;&nbsp;$vereadorAnexo[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$vereadorAnexo[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$vereadorAnexo[categoria]&nbsp;&raquo;&nbsp;$vereadorAnexo[titulo]</strong></a></p>";



					}



				}



				break;

			case "26":

				$qryNfe = $conn->query("SELECT nfe.*, nfe_categoria.descricao categoria, nfe_subcategoria.descricao subcategoria

									      FROM nfe

									 LEFT JOIN nfe_categoria ON nfe.id_categoria = nfe_categoria.id

									 LEFT JOIN nfe_subcategoria ON nfe.id_subcategoria = nfe_subcategoria.id

									     WHERE nfe_categoria.id_cliente = '$cliente'

										   AND nfe.status_registro = 'A'

										   AND (nfe.titulo LIKE '%$busca%' OR nfe_categoria.descricao LIKE '%$busca%' OR nfe_subcategoria.descricao LIKE '%$busca%')

								      ORDER BY nfe.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryNfe);
				if(count($qryNfe) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $nfeLista . "$complemento'>Nota Fiscal Eletr&ocirc;nica</a><span class='badge'>$contador</span></h3>";



					foreach($qryNfe as $nfe) {



						if($nfe['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$nfe[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$nfe[categoria]&nbsp;&raquo;&nbsp;$nfe[subcategoria]&nbsp;&raquo;&nbsp;$nfe[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$nfe[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$nfe[categoria]&nbsp;&raquo;&nbsp;$nfe[subcategoria]&nbsp;&raquo;&nbsp;$nfe[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}



				break;

			case "27":

				$qryReembolso = $conn->query("SELECT reembolso.*, reembolso_categoria.descricao categoria, reembolso_subcategoria.descricao subcategoria

										        FROM reembolso

										   LEFT JOIN reembolso_categoria ON reembolso.id_categoria = reembolso_categoria.id

										   LEFT JOIN reembolso_subcategoria ON reembolso.id_subcategoria = reembolso_subcategoria.id

										       WHERE reembolso_categoria.id_cliente = '$cliente'

											     AND reembolso.status_registro = 'A'

											     AND (reembolso.titulo LIKE '%$busca%' OR reembolso_categoria.descricao LIKE '%$busca%' OR reembolso_subcategoria.descricao LIKE '%$busca%')

									        ORDER BY reembolso.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryReembolso);
				if(count($qryReembolso) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $reembolso . "$complemento'>Reembolso</a><span class='badge'>$contador</span></h3>";



					foreach($qryReembolso as $reembolso) {



						if($reembolso['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$reembolso[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$reembolso[categoria]&nbsp;&raquo;&nbsp;$reembolso[subcategoria]&nbsp;&raquo;&nbsp;$reembolso[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$reembolso[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$reembolso[categoria]&nbsp;&raquo;&nbsp;$reembolso[subcategoria]&nbsp;&raquo;&nbsp;$reembolso[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}



				break;

			case "28":

				$qryPercentualSaude = $conn->query("SELECT percentual_aplicacao_saude.*, percentual_aplicacao_saude_categoria.descricao categoria, percentual_aplicacao_saude_subcategoria.descricao subcategoria

											          FROM percentual_aplicacao_saude

											     LEFT JOIN percentual_aplicacao_saude_categoria ON percentual_aplicacao_saude.id_categoria = percentual_aplicacao_saude_categoria.id

											     LEFT JOIN percentual_aplicacao_saude_subcategoria ON percentual_aplicacao_saude.id_subcategoria = percentual_aplicacao_saude_subcategoria.id

											         WHERE percentual_aplicacao_saude_categoria.id_cliente = '$cliente'

												       AND percentual_aplicacao_saude.status_registro = 'A'

												       AND (percentual_aplicacao_saude.titulo LIKE '%$busca%' OR percentual_aplicacao_saude_categoria.descricao LIKE '%$busca%' OR percentual_aplicacao_saude_subcategoria.descricao LIKE '%$busca%')

										          ORDER BY percentual_aplicacao_saude.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryPercentualSaude);
				if(count($qryPercentualSaude) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $aplicacaoSaude . "$complemento'>Percentual de Aplica&ccedil;&atilde;o na Sa&uacute;de</a><span class='badge'>$contador</span></h3>";



					foreach($qryPercentualSaude as $percentual_aplicacao_saude) {



						if($percentual_aplicacao_saude['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$percentual_aplicacao_saude[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$percentual_aplicacao_saude[categoria]&nbsp;&raquo;&nbsp;$percentual_aplicacao_saude[subcategoria]&nbsp;&raquo;&nbsp;$percentual_aplicacao_saude[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$percentual_aplicacao_saude[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$percentual_aplicacao_saude[categoria]&nbsp;&raquo;&nbsp;$percentual_aplicacao_saude[subcategoria]&nbsp;&raquo;&nbsp;$percentual_aplicacao_saude[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}



				break;

			case "29":

				$qryCessao = $conn->query("SELECT cessao_doacao_permuta.*, cessao_doacao_permuta_categoria.descricao categoria, cessao_doacao_permuta_subcategoria.descricao subcategoria

										     FROM cessao_doacao_permuta

										LEFT JOIN cessao_doacao_permuta_categoria ON cessao_doacao_permuta.id_categoria = cessao_doacao_permuta_categoria.id

										LEFT JOIN cessao_doacao_permuta_subcategoria ON cessao_doacao_permuta.id_subcategoria = cessao_doacao_permuta_subcategoria.id

										    WHERE cessao_doacao_permuta_categoria.id_cliente = '$cliente'

											  AND cessao_doacao_permuta.status_registro = 'A'

											  AND (cessao_doacao_permuta.titulo LIKE '%$busca%' OR cessao_doacao_permuta_categoria.descricao LIKE '%$busca%' OR cessao_doacao_permuta_subcategoria.descricao LIKE '%$busca%')

									     ORDER BY cessao_doacao_permuta.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryCessao);
				if(count($qryCessao) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $cessaoPermuta . "$complemento'>Cess&atilde;o / Doa&ccedil;&atilde;o / Permuta de Bens</a><span class='badge'>$contador</span></h3>";



					foreach($qryCessao as $cessao_doacao_permuta) {



						if($cessao_doacao_permuta['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$cessao_doacao_permuta[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$cessao_doacao_permuta[categoria]&nbsp;&raquo;&nbsp;$cessao_doacao_permuta[subcategoria]&nbsp;&raquo;&nbsp;$cessao_doacao_permuta[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$cessao_doacao_permuta[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$cessao_doacao_permuta[categoria]&nbsp;&raquo;&nbsp;$cessao_doacao_permuta[subcategoria]&nbsp;&raquo;&nbsp;$cessao_doacao_permuta[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}



				break;

			case "30":

				$qryRelatorio = $conn->query("SELECT relatorio_resumido_execucao_orcamentaria.*, relatorio_resumido_execucao_orcamentaria_categoria.descricao categoria, relatorio_resumido_execucao_orcamentaria_subcategoria.descricao subcategoria

										     	FROM relatorio_resumido_execucao_orcamentaria

										   LEFT JOIN relatorio_resumido_execucao_orcamentaria_categoria ON relatorio_resumido_execucao_orcamentaria.id_categoria = relatorio_resumido_execucao_orcamentaria_categoria.id

										   LEFT JOIN relatorio_resumido_execucao_orcamentaria_subcategoria ON relatorio_resumido_execucao_orcamentaria.id_subcategoria = relatorio_resumido_execucao_orcamentaria_subcategoria.id

										       WHERE relatorio_resumido_execucao_orcamentaria_categoria.id_cliente = '$cliente'

											  	 AND relatorio_resumido_execucao_orcamentaria.status_registro = 'A'

											  	 AND (relatorio_resumido_execucao_orcamentaria.titulo LIKE '%$busca%' OR relatorio_resumido_execucao_orcamentaria_categoria.descricao LIKE '%$busca%' OR relatorio_resumido_execucao_orcamentaria_subcategoria.descricao LIKE '%$busca%')

									     	ORDER BY relatorio_resumido_execucao_orcamentaria.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryRelatorio);
				if(count($qryRelatorio) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $execucaoOrcamentaria . "$complemento'>Relat&oacute;rio Resumido de Execu&ccedil;&atilde;o Or&ccedil;ament&aacute;ria</a><span class='badge'>$contador</span></h3>";



					foreach($qryRelatorio as $relatorio_resumido_execucao_orcamentaria) {



						if($relatorio_resumido_execucao_orcamentaria['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$relatorio_resumido_execucao_orcamentaria[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$relatorio_resumido_execucao_orcamentaria[categoria]&nbsp;&raquo;&nbsp;$relatorio_resumido_execucao_orcamentaria[subcategoria]&nbsp;&raquo;&nbsp;$relatorio_resumido_execucao_orcamentaria[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$relatorio_resumido_execucao_orcamentaria[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$relatorio_resumido_execucao_orcamentaria[categoria]&nbsp;&raquo;&nbsp;$relatorio_resumido_execucao_orcamentaria[subcategoria]&nbsp;&raquo;&nbsp;$relatorio_resumido_execucao_orcamentaria[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}



				break;

			case "31":

				$qryOperacoes = $conn->query("SELECT operacoes_financeiras.*, operacoes_financeiras_categoria.descricao categoria, operacoes_financeiras_subcategoria.descricao subcategoria

										     	FROM operacoes_financeiras

										   LEFT JOIN operacoes_financeiras_categoria ON operacoes_financeiras.id_categoria = operacoes_financeiras_categoria.id

										   LEFT JOIN operacoes_financeiras_subcategoria ON operacoes_financeiras.id_subcategoria = operacoes_financeiras_subcategoria.id

										       WHERE operacoes_financeiras_categoria.id_cliente = '$cliente'

											  	 AND operacoes_financeiras.status_registro = 'A'

											  	 AND (operacoes_financeiras.titulo LIKE '%$busca%' OR operacoes_financeiras_categoria.descricao LIKE '%$busca%' OR operacoes_financeiras_subcategoria.descricao LIKE '%$busca%')

									     	ORDER BY operacoes_financeiras.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryOperacoes);
				if(count($qryOperacoes) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $operacaoFinanceira . "$complemento'>Opera&ccedil;&otilde;es Financeiras</a><span class='badge'>$contador</span></h3>";



					foreach($qryOperacoes as $operacoes_financeiras) {



						if($operacoes_financeiras['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$operacoes_financeiras[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$operacoes_financeiras[categoria]&nbsp;&raquo;&nbsp;$operacoes_financeiras[subcategoria]&nbsp;&raquo;&nbsp;$operacoes_financeiras[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$operacoes_financeiras[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$operacoes_financeiras[categoria]&nbsp;&raquo;&nbsp;$operacoes_financeiras[subcategoria]&nbsp;&raquo;&nbsp;$operacoes_financeiras[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}



				break;

			case "32":

				$qryControleEstoque = $conn->query("SELECT controle_estoque.*, controle_estoque_categoria.descricao categoria, controle_estoque_subcategoria.descricao subcategoria

												      FROM controle_estoque

												 LEFT JOIN controle_estoque_categoria ON controle_estoque.id_categoria = controle_estoque_categoria.id

												 LEFT JOIN controle_estoque_subcategoria ON controle_estoque.id_subcategoria = controle_estoque_subcategoria.id

												     WHERE controle_estoque_categoria.id_cliente = '$cliente'

													   AND controle_estoque.status_registro = 'A'

													   AND (controle_estoque.titulo LIKE '%$busca%' OR controle_estoque_categoria.descricao LIKE '%$busca%' OR controle_estoque_subcategoria.descricao LIKE '%$busca%')

											      ORDER BY controle_estoque.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryControleEstoque);
				if(count($qryControleEstoque) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $controleEstoque . "$complemento'>Controle de Estoque</a><span class='badge'>$contador</span></h3>";



					foreach($qryControleEstoque as $controle_estoque) {



						if($controle_estoque['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$controle_estoque[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$controle_estoque[categoria]&nbsp;&raquo;&nbsp;$controle_estoque[subcategoria]&nbsp;&raquo;&nbsp;$controle_estoque[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$controle_estoque[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$controle_estoque[categoria]&nbsp;&raquo;&nbsp;$controle_estoque[subcategoria]&nbsp;&raquo;&nbsp;$controle_estoque[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}



				break;

			case "33":

				$qryCartao = $conn->query("SELECT cartao_corporativo.*, cartao_corporativo_categoria.descricao categoria, cartao_corporativo_subcategoria.descricao subcategoria

										     FROM cartao_corporativo

										LEFT JOIN cartao_corporativo_categoria ON cartao_corporativo.id_categoria = cartao_corporativo_categoria.id

										LEFT JOIN cartao_corporativo_subcategoria ON cartao_corporativo.id_subcategoria = cartao_corporativo_subcategoria.id

										    WHERE cartao_corporativo_categoria.id_cliente = '$cliente'

											  AND cartao_corporativo.status_registro = 'A'

											  AND (cartao_corporativo.titulo LIKE '%$busca%' OR cartao_corporativo_categoria.descricao LIKE '%$busca%' OR cartao_corporativo_subcategoria.descricao LIKE '%$busca%')

									     ORDER BY cartao_corporativo.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryCartao);
				if(count($qryCartao) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $cartaoCorporativo . "$complemento'>Cart&atilde;o Corporativo</a><span class='badge'>$contador</span></h3>";



					foreach($qryCartao as $cartao_corporativo) {



						if($cartao_corporativo['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$cartao_corporativo[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$cartao_corporativo[categoria]&nbsp;&raquo;&nbsp;$cartao_corporativo[subcategoria]&nbsp;&raquo;&nbsp;$cartao_corporativo[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$cartao_corporativo[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$cartao_corporativo[categoria]&nbsp;&raquo;&nbsp;$cartao_corporativo[subcategoria]&nbsp;&raquo;&nbsp;$cartao_corporativo[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}



				break;

			case "34":

				$qryJustificativa = $conn->query("SELECT justificativa_contratacao_direta.*, justificativa_contratacao_direta_categoria.descricao categoria, justificativa_contratacao_direta_subcategoria.descricao subcategoria

												    FROM justificativa_contratacao_direta

											   LEFT JOIN justificativa_contratacao_direta_categoria ON justificativa_contratacao_direta.id_categoria = justificativa_contratacao_direta_categoria.id

											   LEFT JOIN justificativa_contratacao_direta_subcategoria ON justificativa_contratacao_direta.id_subcategoria = justificativa_contratacao_direta_subcategoria.id

												   WHERE justificativa_contratacao_direta_categoria.id_cliente = '$cliente'

													 AND justificativa_contratacao_direta.status_registro = 'A'

													 AND (justificativa_contratacao_direta.titulo LIKE '%$busca%' OR justificativa_contratacao_direta_categoria.descricao LIKE '%$busca%' OR justificativa_contratacao_direta_subcategoria.descricao LIKE '%$busca%')

											    ORDER BY justificativa_contratacao_direta.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryJustificativa);
				if(count($qryJustificativa) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $justificativaDireta . "$complemento'>Justificativa de Contrata&ccedil;&atilde;o Direta</a><span class='badge'>$contador</span></h3>";



					foreach($qryJustificativa as $justificativa_contratacao_direta) {



						if($justificativa_contratacao_direta['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$justificativa_contratacao_direta[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$justificativa_contratacao_direta[categoria]&nbsp;&raquo;&nbsp;$justificativa_contratacao_direta[subcategoria]&nbsp;&raquo;&nbsp;$justificativa_contratacao_direta[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$justificativa_contratacao_direta[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$justificativa_contratacao_direta[categoria]&nbsp;&raquo;&nbsp;$justificativa_contratacao_direta[subcategoria]&nbsp;&raquo;&nbsp;$justificativa_contratacao_direta[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}



				break;

			case "35":

				$qryPassagem = $conn->query("SELECT aquisicao_passagem_aerea.*, aquisicao_passagem_aerea_categoria.descricao categoria, aquisicao_passagem_aerea_subcategoria.descricao subcategoria

											   FROM aquisicao_passagem_aerea

										  LEFT JOIN aquisicao_passagem_aerea_categoria ON aquisicao_passagem_aerea.id_categoria = aquisicao_passagem_aerea_categoria.id

										  LEFT JOIN aquisicao_passagem_aerea_subcategoria ON aquisicao_passagem_aerea.id_subcategoria = aquisicao_passagem_aerea_subcategoria.id

											  WHERE aquisicao_passagem_aerea_categoria.id_cliente = '$cliente'

												AND aquisicao_passagem_aerea.status_registro = 'A'

												AND (aquisicao_passagem_aerea.titulo LIKE '%$busca%' OR aquisicao_passagem_aerea_categoria.descricao LIKE '%$busca%' OR aquisicao_passagem_aerea_subcategoria.descricao LIKE '%$busca%')

										   ORDER BY aquisicao_passagem_aerea.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryPassagem);
				if(count($qryPassagem) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $passagem . "$complemento'>Aquisi&ccedil;&atilde;o de Passagens</a><span class='badge'>$contador</span></h3>";



					foreach($qryPassagem as $aquisicao_passagem_aerea) {



						if($aquisicao_passagem_aerea['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$aquisicao_passagem_aerea[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$aquisicao_passagem_aerea[categoria]&nbsp;&raquo;&nbsp;$aquisicao_passagem_aerea[subcategoria]&nbsp;&raquo;&nbsp;$aquisicao_passagem_aerea[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$aquisicao_passagem_aerea[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$aquisicao_passagem_aerea[categoria]&nbsp;&raquo;&nbsp;$aquisicao_passagem_aerea[subcategoria]&nbsp;&raquo;&nbsp;$aquisicao_passagem_aerea[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}



				break;

			case "36":

				$qryMovimentacao = $conn->query("SELECT movimentacao_fundo.*, movimentacao_fundo_categoria.descricao categoria, movimentacao_fundo_subcategoria.descricao subcategoria

												   FROM movimentacao_fundo

											  LEFT JOIN movimentacao_fundo_categoria ON movimentacao_fundo.id_categoria = movimentacao_fundo_categoria.id

											  LEFT JOIN movimentacao_fundo_subcategoria ON movimentacao_fundo.id_subcategoria = movimentacao_fundo_subcategoria.id

												  WHERE movimentacao_fundo_categoria.id_cliente = '$cliente'

													AND movimentacao_fundo.status_registro = 'A'

													AND (movimentacao_fundo.titulo LIKE '%$busca%' OR movimentacao_fundo_categoria.descricao LIKE '%$busca%' OR movimentacao_fundo_subcategoria.descricao LIKE '%$busca%')

											   ORDER BY movimentacao_fundo.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryMovimentacao);
				if(count($qryMovimentacao) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $fundos . "$complemento'>Movimenta&ccedil;&atilde;o dos Fundos</a><span class='badge'>$contador</span></h3>";



					foreach($qryMovimentacao as $movimentacao_fundo) {



						if($movimentacao_fundo['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$movimentacao_fundo[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$movimentacao_fundo[categoria]&nbsp;&raquo;&nbsp;$movimentacao_fundo[subcategoria]&nbsp;&raquo;&nbsp;$movimentacao_fundo[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$movimentacao_fundo[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$movimentacao_fundo[categoria]&nbsp;&raquo;&nbsp;$movimentacao_fundo[subcategoria]&nbsp;&raquo;&nbsp;$movimentacao_fundo[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}



				break;

			case "37":

				$qryEducacao = $conn->query("SELECT percentual_aplicacao_educacao.*, percentual_aplicacao_educacao_categoria.descricao categoria, percentual_aplicacao_educacao_subcategoria.descricao subcategoria

											   FROM percentual_aplicacao_educacao

										  LEFT JOIN percentual_aplicacao_educacao_categoria ON percentual_aplicacao_educacao.id_categoria = percentual_aplicacao_educacao_categoria.id

										  LEFT JOIN percentual_aplicacao_educacao_subcategoria ON percentual_aplicacao_educacao.id_subcategoria = percentual_aplicacao_educacao_subcategoria.id

											  WHERE percentual_aplicacao_educacao_categoria.id_cliente = '$cliente'

												AND percentual_aplicacao_educacao.status_registro = 'A'

												AND (percentual_aplicacao_educacao.titulo LIKE '%$busca%' OR percentual_aplicacao_educacao_categoria.descricao LIKE '%$busca%' OR percentual_aplicacao_educacao_subcategoria.descricao LIKE '%$busca%')

										   ORDER BY percentual_aplicacao_educacao.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryEducacao);
				if(count($qryEducacao) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $aplicacaoEducacao . "$complemento'>Percentual de Aplica&ccedil;&atilde;o na Educa&ccedil;&atilde;o</a><span class='badge'>$contador</span></h3>";



					foreach($qryEducacao as $percentual_aplicacao_educacao) {



						if($percentual_aplicacao_educacao['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$percentual_aplicacao_educacao[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$percentual_aplicacao_educacao[categoria]&nbsp;&raquo;&nbsp;$percentual_aplicacao_educacao[subcategoria]&nbsp;&raquo;&nbsp;$percentual_aplicacao_educacao[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$percentual_aplicacao_educacao[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$percentual_aplicacao_educacao[categoria]&nbsp;&raquo;&nbsp;$percentual_aplicacao_educacao[subcategoria]&nbsp;&raquo;&nbsp;$percentual_aplicacao_educacao[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}



				break;

			case "38":

				$qryGestaoFiscal = $conn->query("SELECT relatorio_gestao_fiscal.*, relatorio_gestao_fiscal_categoria.descricao categoria, relatorio_gestao_fiscal_subcategoria.descricao subcategoria

												   FROM relatorio_gestao_fiscal

											  LEFT JOIN relatorio_gestao_fiscal_categoria ON relatorio_gestao_fiscal.id_categoria = relatorio_gestao_fiscal_categoria.id

											  LEFT JOIN relatorio_gestao_fiscal_subcategoria ON relatorio_gestao_fiscal.id_subcategoria = relatorio_gestao_fiscal_subcategoria.id

												  WHERE relatorio_gestao_fiscal_categoria.id_cliente = '$cliente'

													AND relatorio_gestao_fiscal.status_registro = 'A'

													AND (relatorio_gestao_fiscal.titulo LIKE '%$busca%' OR relatorio_gestao_fiscal_categoria.descricao LIKE '%$busca%' OR relatorio_gestao_fiscal_subcategoria.descricao LIKE '%$busca%')

											   ORDER BY relatorio_gestao_fiscal.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryGestaoFiscal);
				if(count($qryGestaoFiscal) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $relatorioFiscal . "$complemento'>Relat&oacute;rio de Gest&atilde;o Fiscal</a><span class='badge'>$contador</span></h3>";



					foreach($qryGestaoFiscal as $relatorio_gestao_fiscal) {



						if($relatorio_gestao_fiscal['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$relatorio_gestao_fiscal[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$relatorio_gestao_fiscal[categoria]&nbsp;&raquo;&nbsp;$relatorio_gestao_fiscal[subcategoria]&nbsp;&raquo;&nbsp;$relatorio_gestao_fiscal[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$relatorio_gestao_fiscal[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$relatorio_gestao_fiscal[categoria]&nbsp;&raquo;&nbsp;$relatorio_gestao_fiscal[subcategoria]&nbsp;&raquo;&nbsp;$relatorio_gestao_fiscal[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}



				break;

			case "39":

				$qryExecucao = $conn->query("SELECT execucao_orcamentaria_tempo_real.*, execucao_orcamentaria_tempo_real_categoria.descricao categoria, execucao_orcamentaria_tempo_real_subcategoria.descricao subcategoria

											   FROM execucao_orcamentaria_tempo_real

										  LEFT JOIN execucao_orcamentaria_tempo_real_categoria ON execucao_orcamentaria_tempo_real.id_categoria = execucao_orcamentaria_tempo_real_categoria.id

										  LEFT JOIN execucao_orcamentaria_tempo_real_subcategoria ON execucao_orcamentaria_tempo_real.id_subcategoria = execucao_orcamentaria_tempo_real_subcategoria.id

											  WHERE execucao_orcamentaria_tempo_real_categoria.id_cliente = '$cliente'

												AND execucao_orcamentaria_tempo_real.status_registro = 'A'

												AND (execucao_orcamentaria_tempo_real.titulo LIKE '%$busca%' OR execucao_orcamentaria_tempo_real_categoria.descricao LIKE '%$busca%' OR execucao_orcamentaria_tempo_real_subcategoria.descricao LIKE '%$busca%')

										   ORDER BY execucao_orcamentaria_tempo_real.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryExecucao);
				if(count($qryExecucao) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $execucaoTempoReal . "$complemento'>Execu&ccedil;&atilde;o Or&ccedil;ament&aacute;ria em Tempo Real</a><span class='badge'>$contador</span></h3>";



					foreach($qryExecucao as $execucao_orcamentaria_tempo_real) {



						if($execucao_orcamentaria_tempo_real['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$execucao_orcamentaria_tempo_real[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$execucao_orcamentaria_tempo_real[categoria]&nbsp;&raquo;&nbsp;$execucao_orcamentaria_tempo_real[subcategoria]&nbsp;&raquo;&nbsp;$execucao_orcamentaria_tempo_real[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$execucao_orcamentaria_tempo_real[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$execucao_orcamentaria_tempo_real[categoria]&nbsp;&raquo;&nbsp;$execucao_orcamentaria_tempo_real[subcategoria]&nbsp;&raquo;&nbsp;$execucao_orcamentaria_tempo_real[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}



				break;

			case "40":

				$qryExtrato = $conn->query("SELECT extrato_conta_unica.*, extrato_conta_unica_categoria.descricao categoria, extrato_conta_unica_subcategoria.descricao subcategoria

											  FROM extrato_conta_unica

										 LEFT JOIN extrato_conta_unica_categoria ON extrato_conta_unica.id_categoria = extrato_conta_unica_categoria.id

										 LEFT JOIN extrato_conta_unica_subcategoria ON extrato_conta_unica.id_subcategoria = extrato_conta_unica_subcategoria.id

											 WHERE extrato_conta_unica_categoria.id_cliente = '$cliente'

											   AND extrato_conta_unica.status_registro = 'A'

											   AND (extrato_conta_unica.titulo LIKE '%$busca%' OR extrato_conta_unica_categoria.descricao LIKE '%$busca%' OR extrato_conta_unica_subcategoria.descricao LIKE '%$busca%')

										  ORDER BY extrato_conta_unica.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryExtrato);
				if(count($qryExtrato) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $extratoUnico . "$complemento'>Extrato de Conta &Uacute;nica</a><span class='badge'>$contador</span></h3>";



					foreach($qryExtrato as $extrato_conta_unica) {



						if($extrato_conta_unica['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$extrato_conta_unica[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$extrato_conta_unica[categoria]&nbsp;&raquo;&nbsp;$extrato_conta_unica[subcategoria]&nbsp;&raquo;&nbsp;$extrato_conta_unica[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$extrato_conta_unica[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$extrato_conta_unica[categoria]&nbsp;&raquo;&nbsp;$extrato_conta_unica[subcategoria]&nbsp;&raquo;&nbsp;$extrato_conta_unica[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}



				break;

			case "41":

				$qryAdiantamento = $conn->query("SELECT adiantamento.*, adiantamento_categoria.descricao categoria, adiantamento_subcategoria.descricao subcategoria

												   FROM adiantamento

											  LEFT JOIN adiantamento_categoria ON adiantamento.id_categoria = adiantamento_categoria.id

											  LEFT JOIN adiantamento_subcategoria ON adiantamento.id_subcategoria = adiantamento_subcategoria.id

												  WHERE adiantamento_categoria.id_cliente = '$cliente'

												    AND adiantamento.status_registro = 'A'

												    AND (adiantamento.titulo LIKE '%$busca%' OR adiantamento_categoria.descricao LIKE '%$busca%' OR adiantamento_subcategoria.descricao LIKE '%$busca%')

											   ORDER BY adiantamento.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryAdiantamento);
				if(count($qryAdiantamento) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $adiantamento . "$complemento'>Adiantamento</a><span class='badge'>$contador</span></h3>";



					foreach($qryAdiantamento as $adiantamento) {



						if($adiantamento['link'] != "") {



							$linkAdiantamento = $adiantamento['link'];

							$targetDiaria = "_blank";

							$imagem = "glyphicon-globe";



						} else if($adiantamento['arquivo'] != "") {



							$linkAdiantamento = $CAMINHOARQ . "/" . $adiantamento['arquivo'];

							$targetDiaria = "_blank";

							$imagem = "glyphicon-cloud-download";



						} else {



							$linkAdiantamento = "#";

							$targetDiaria = "_self";

							$imagem = "glyphicon glyphicon-chevron-right";



						}

					?>

					<div>

						<a href="<?= $linkAdiantamento ?>" target="_blank"><span class="text-primary"><i class="glyphicon <?= $imagem ?>"></i>&nbsp;&nbsp;<?= $adiantamento['categoria'] ?>&nbsp;&raquo;&nbsp;<?= $adiantamento['subcategoria'] ?>&nbsp;&raquo;&nbsp;<?= $adiantamento['titulo'] ?></span></a><br>

						<?php if($adiantamento['beneficiario'] != "") { ?><strong style="margin-left: 22px;">Benefici&aacute;rio:</strong> <?= $adiantamento['beneficiario'] ?><br><?php } ?>

		                <?php if($adiantamento['rg_beneficiario'] != "") { ?><strong style="margin-left: 22px;">RG do Benefici&aacute;rio:</strong> <?= $adiantamento['rg_beneficiario'] ?><br><?php } ?>

		                <?php if($adiantamento['justificativa'] != "") { ?><strong style="margin-left: 22px;">Justificativa:</strong> <?= $adiantamento['justificativa'] ?><br><?php } ?>

		                <?php if($adiantamento['data_inicio_viagem'] != "") { ?><strong style="margin-left: 22px;">Data Inicial da Viagem:</strong> <?= formata_data($adiantamento['data_inicio_viagem']) ?><br><?php } ?>

		                <?php if($adiantamento['data_fim_viagem'] != "") { ?><strong style="margin-left: 22px;">Data Final da Viagem:</strong> <?= formata_data($adiantamento['data_fim_viagem']) ?><br><?php } ?>

		                <?php if($adiantamento['meio_transporte'] != "") { ?><strong style="margin-left: 22px;">Meio de Transporte:</strong> <?= $adiantamento['meio_transporte'] ?><br><?php } ?>

		                <?php if($adiantamento['custo_meio_transporte'] != "") { ?><strong style="margin-left: 22px;">Custo do Meio de Transporte:</strong> <?= $adiantamento['custo_meio_transporte'] ?><br><?php } ?>

		                <?php if($adiantamento['quantidade_adiantamento'] != "") { ?><strong style="margin-left: 22px;">Quantidade Unit&aacute;ria de Adiantamento:</strong> <?= $adiantamento['quantidade_adiantamento'] ?><br><?php } ?>

		                <?php if($adiantamento['valor_unitario_adiantamento'] != "") { ?><strong style="margin-left: 22px;">Valor Unit&aacute;rio de Adiantamento:</strong> <?= $adiantamento['valor_unitario_adiantamento'] ?><br><?php } ?>

					</div>

				<?php

					}



					echo "<p>&nbsp;</p>";



				}



				break;

			case "42":

				$qryNotificacao = $conn->query("SELECT notificacao.*, notificacao_categoria.descricao categoria, notificacao_subcategoria.descricao subcategoria

												  FROM notificacao

											 LEFT JOIN notificacao_categoria ON notificacao.id_categoria = notificacao_categoria.id

											 LEFT JOIN notificacao_subcategoria ON notificacao.id_subcategoria = notificacao_subcategoria.id

												 WHERE notificacao_categoria.id_cliente = '$cliente'

												   AND notificacao.status_registro = 'A'

												   AND (notificacao.titulo LIKE '%$busca%' OR notificacao_categoria.descricao LIKE '%$busca%' OR notificacao_subcategoria.descricao LIKE '%$busca%')

											  ORDER BY notificacao.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryNotificacao);
				if(count($qryNotificacao) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $notificacao . "$complemento'>Notifica&ccedil;&otilde;es</a><span class='badge'>$contador</span></h3>";



					foreach($qryNotificacao as $notificacao) {



						if($notificacao['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$notificacao[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$notificacao[categoria]&nbsp;&raquo;&nbsp;$notificacao[subcategoria]&nbsp;&raquo;&nbsp;$notificacao[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$notificacao[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$notificacao[categoria]&nbsp;&raquo;&nbsp;$notificacao[subcategoria]&nbsp;&raquo;&nbsp;$notificacao[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}



				break;

			case "44":

				$qryRegimento = $conn->query("SELECT * FROM camara_regimento_interno WHERE id_cliente = '$cliente' AND status_registro = 'A' AND descricao LIKE '%$busca%' ORDER BY id")->fetchAll(PDO::FETCH_ASSOC);



					$contador = count($qryRegimento);
				if(count($qryRegimento) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $regimentoLista . "$complemento'>Regimento Interno</a><span class='badge'>$contador</span></h3>";



					?>

					<div class="table-responsive">

						<table class="table table-striped table-hover table-bordered table-condensed">

							<tr>

								<th class="col-sm-1 col-xs-2">Arquivo</th>

								<th>Descri&ccedil;&atilde;o</th>

							</tr>

							<?php foreach($qryRegimento as $regimento) { ?>

							<tr>

								<td>

									<?php if($regimento['arquivo'] != "") { ?>

									<a href="<?= $CAMINHOARQ ?>/<?= $regimento['arquivo'] ?>" class="btn btn-warning" data-toggle="tooltip" data-placement="right" title="Baixar arquivo" target="_blank">

										<i class="glyphicon glyphicon-cloud-download"></i>

									</a>

									<?php } else { ?>

									<button type="button" class="btn btn-warning" disabled><i class="glyphicon glyphicon-cloud-download"></i></button>

									<?php } ?>

								</td>

								<td class="col-sm-11  col-xs-10"><?= verifica($regimento['descricao']) ?></td>

							</tr>

							<?php } ?>

						</table>

					</div>

					<?php

				}



				break;

			case "45":

				$qryRepasse = $conn->query("SELECT repasse_transferencia.*, repasse_transferencia_categoria.descricao categoria, repasse_transferencia_subcategoria.descricao subcategoria

											  FROM repasse_transferencia

										 LEFT JOIN repasse_transferencia_categoria ON repasse_transferencia.id_categoria = repasse_transferencia_categoria.id

										 LEFT JOIN repasse_transferencia_subcategoria ON repasse_transferencia.id_subcategoria = repasse_transferencia_subcategoria.id

											 WHERE repasse_transferencia_categoria.id_cliente = '$cliente'

											   AND repasse_transferencia.status_registro = 'A'

											   AND (repasse_transferencia.titulo LIKE '%$busca%' OR repasse_transferencia_categoria.descricao LIKE '%$busca%' OR repasse_transferencia_subcategoria.descricao LIKE '%$busca%')

										  ORDER BY repasse_transferencia.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryRepasse);
				if(count($qryRepasse) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $repasseTransferencia . "$complemento'>Repasses / Transfer&ecirc;ncias</a><span class='badge'>$contador</span></h3>";



					foreach($qryRepasse as $repasse_transferencia) {



						if($repasse_transferencia['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$repasse_transferencia[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$repasse_transferencia[categoria]&nbsp;&raquo;&nbsp;$repasse_transferencia[subcategoria]&nbsp;&raquo;&nbsp;$repasse_transferencia[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$repasse_transferencia[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$repasse_transferencia[categoria]&nbsp;&raquo;&nbsp;$repasse_transferencia[subcategoria]&nbsp;&raquo;&nbsp;$repasse_transferencia[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}



				break;

			case "46":

				$qryBolsaFamilia = $conn->query("SELECT bolsa_familia.*, bolsa_familia_categoria.descricao categoria, bolsa_familia_subcategoria.descricao subcategoria

												   FROM bolsa_familia

											  LEFT JOIN bolsa_familia_categoria ON bolsa_familia.id_categoria = bolsa_familia_categoria.id

											  LEFT JOIN bolsa_familia_subcategoria ON bolsa_familia.id_subcategoria = bolsa_familia_subcategoria.id

												  WHERE bolsa_familia_categoria.id_cliente = '$cliente'

												    AND bolsa_familia.status_registro = 'A'

												    AND (bolsa_familia.titulo LIKE '%$busca%' OR bolsa_familia_categoria.descricao LIKE '%$busca%' OR bolsa_familia_subcategoria.descricao LIKE '%$busca%')

											   ORDER BY bolsa_familia.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryBolsaFamilia);
				if(count($qryBolsaFamilia) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $bolsaFamilia . "$complemento'>Programa Bolsa Fam&iacute;lia</a><span class='badge'>$contador</span></h3>";



					foreach($qryBolsaFamilia as $bolsa_familia) {



						if($bolsa_familia['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$bolsa_familia[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$bolsa_familia[categoria]&nbsp;&raquo;&nbsp;$bolsa_familia[subcategoria]&nbsp;&raquo;&nbsp;$bolsa_familia[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$bolsa_familia[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$bolsa_familia[categoria]&nbsp;&raquo;&nbsp;$bolsa_familia[subcategoria]&nbsp;&raquo;&nbsp;$bolsa_familia[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}



				break;

			case "47":

				$qryTransferencia = $conn->query("SELECT transferencia_voluntaria.*, transferencia_voluntaria_categoria.descricao categoria, transferencia_voluntaria_subcategoria.descricao subcategoria

												    FROM transferencia_voluntaria

											   LEFT JOIN transferencia_voluntaria_categoria ON transferencia_voluntaria.id_categoria = transferencia_voluntaria_categoria.id

											   LEFT JOIN transferencia_voluntaria_subcategoria ON transferencia_voluntaria.id_subcategoria = transferencia_voluntaria_subcategoria.id

												   WHERE transferencia_voluntaria_categoria.id_cliente = '$cliente'

												     AND transferencia_voluntaria.status_registro = 'A'

												     AND (transferencia_voluntaria.titulo LIKE '%$busca%' OR transferencia_voluntaria_categoria.descricao LIKE '%$busca%' OR transferencia_voluntaria_subcategoria.descricao LIKE '%$busca%')

											    ORDER BY transferencia_voluntaria.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryTransferencia);
				if(count($qryTransferencia) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $transferenciaVoluntaria . "$complemento'>Transfer&ecirc;ncias Volunt&aacute;rias</a><span class='badge'>$contador</span></h3>";



					foreach($qryTransferencia as $transferencia_voluntaria) {



						if($transferencia_voluntaria['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$transferencia_voluntaria[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$transferencia_voluntaria[categoria]&nbsp;&raquo;&nbsp;$transferencia_voluntaria[subcategoria]&nbsp;&raquo;&nbsp;$transferencia_voluntaria[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$transferencia_voluntaria[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$transferencia_voluntaria[categoria]&nbsp;&raquo;&nbsp;$transferencia_voluntaria[subcategoria]&nbsp;&raquo;&nbsp;$transferencia_voluntaria[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}



				break;

			case "48":

				$qryControle = $conn->query("SELECT controle_interno.*, controle_interno_categoria.descricao categoria, controle_interno_subcategoria.descricao subcategoria

											   FROM controle_interno

										  LEFT JOIN controle_interno_categoria ON controle_interno.id_categoria = controle_interno_categoria.id

										  LEFT JOIN controle_interno_subcategoria ON controle_interno.id_subcategoria = controle_interno_subcategoria.id

											  WHERE controle_interno_categoria.id_cliente = '$cliente'

											    AND controle_interno.status_registro = 'A'

											    AND (controle_interno.titulo LIKE '%$busca%' OR controle_interno_categoria.descricao LIKE '%$busca%' OR controle_interno_subcategoria.descricao LIKE '%$busca%')

										   ORDER BY controle_interno.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryControle);
				if(count($qryControle) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $controleInterno . "$complemento'>Controle Interno</a><span class='badge'>$contador</span></h3>";



					foreach($qryControle as $controle_interno) {



						if($controle_interno['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$controle_interno[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$controle_interno[categoria]&nbsp;&raquo;&nbsp;$controle_interno[subcategoria]&nbsp;&raquo;&nbsp;$controle_interno[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$controle_interno[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$controle_interno[categoria]&nbsp;&raquo;&nbsp;$controle_interno[subcategoria]&nbsp;&raquo;&nbsp;$controle_interno[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}



				break;

			case "49":

				$qryFormulario = $conn->query("SELECT formulario_atendimento.*, formulario_atendimento_categoria.descricao categoria, formulario_atendimento_subcategoria.descricao subcategoria

											   	 FROM formulario_atendimento

										  	LEFT JOIN formulario_atendimento_categoria ON formulario_atendimento.id_categoria = formulario_atendimento_categoria.id

										  	LEFT JOIN formulario_atendimento_subcategoria ON formulario_atendimento.id_subcategoria = formulario_atendimento_subcategoria.id

											 	WHERE formulario_atendimento_categoria.id_cliente = '$cliente'

											      AND formulario_atendimento.status_registro = 'A'

											      AND (formulario_atendimento.titulo LIKE '%$busca%' OR formulario_atendimento_categoria.descricao LIKE '%$busca%' OR formulario_atendimento_subcategoria.descricao LIKE '%$busca%')

										   	 ORDER BY formulario_atendimento.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryFormulario);
				if(count($qryFormulario) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $formularioAtendimento . "$complemento'>Download Formul&aacute;rios de Atendimento</a><span class='badge'>$contador</span></h3>";



					foreach($qryFormulario as $formulario_atendimento) {



						if($formulario_atendimento['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$formulario_atendimento[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$formulario_atendimento[categoria]&nbsp;&raquo;&nbsp;$formulario_atendimento[subcategoria]&nbsp;&raquo;&nbsp;$formulario_atendimento[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$formulario_atendimento[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$formulario_atendimento[categoria]&nbsp;&raquo;&nbsp;$formulario_atendimento[subcategoria]&nbsp;&raquo;&nbsp;$formulario_atendimento[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}



				break;

			case "50":

				$qryAtendimento = $conn->query("SELECT relatorio_atendimento.*, relatorio_atendimento_categoria.descricao categoria, relatorio_atendimento_subcategoria.descricao subcategoria

											   	  FROM relatorio_atendimento

										  	 LEFT JOIN relatorio_atendimento_categoria ON relatorio_atendimento.id_categoria = relatorio_atendimento_categoria.id

										  	 LEFT JOIN relatorio_atendimento_subcategoria ON relatorio_atendimento.id_subcategoria = relatorio_atendimento_subcategoria.id

											 	 WHERE relatorio_atendimento_categoria.id_cliente = '$cliente'

											       AND relatorio_atendimento.status_registro = 'A'

											       AND (relatorio_atendimento.titulo LIKE '%$busca%' OR relatorio_atendimento_categoria.descricao LIKE '%$busca%' OR relatorio_atendimento_subcategoria.descricao LIKE '%$busca%')

										   	  ORDER BY relatorio_atendimento.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryAtendimento);
				if(count($qryAtendimento) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $relatorioAtendimento . "$complemento'>Relat&oacute;rios Estat&iacute;sticos de Atendimento</a><span class='badge'>$contador</span></h3>";



					foreach($qryAtendimento as $relatorio_atendimento) {



						if($relatorio_atendimento['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$relatorio_atendimento[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$relatorio_atendimento[categoria]&nbsp;&raquo;&nbsp;$relatorio_atendimento[subcategoria]&nbsp;&raquo;&nbsp;$relatorio_atendimento[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$relatorio_atendimento[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$relatorio_atendimento[categoria]&nbsp;&raquo;&nbsp;$relatorio_atendimento[subcategoria]&nbsp;&raquo;&nbsp;$relatorio_atendimento[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}



				break;

			case "51":

				$qrySic = $conn->query("SELECT * FROM sic_cidadao WHERE id_cliente = '$cliente' AND status_registro = 'A' AND (titulo LIKE '%$busca%' OR orgao LIKE '%$busca%') ORDER BY titulo")->fetchAll(PDO::FETCH_ASSOC);



					$contador = count($qrySic);
				if(count($qrySic) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $sicCidadao . "$complemento'>Servi&ccedil;o F&iacute;sico de Informa&ccedil;&atilde;o ao Cidad&atilde;o</a><span class='badge'>$contador</span></h3>";

					echo '<div class="panel-group" id="accordion_sic" role="tablist" aria-multiselectable="true">';



					foreach($qrySic as $sic) {

					?>

					<div class="panel panel-default">

					    <div class="panel-heading" role="tab" id="heading_<?= $sic['id'] ?>">

					     	<h4 class="panel-title">

					        	<a role="button" data-toggle="collapse" data-parent="#accordion_sic" href="#collapse_<?= $sic['id'] ?>" aria-expanded="true" aria-controls="collapse_<?= $sic['id'] ?>">

					          	<i class="glyphicon glyphicon-triangle-right"></i> <?= verifica($sic['titulo']) ?>

					        	</a>

					    	</h4>

					    </div>

						<div id="collapse_<?= $sic['id'] ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_<?= $sic['id'] ?>">

							<div class="panel-body">

								<?php if($sic['orgao'] != "") { ?><p><strong>&Oacute;rg&atilde;o:</strong> <?= $sic['orgao'] ?></p><?php } ?>

								<?php if($sic['responsavel'] != "") { ?><p><strong>Respons&aacute;vel:</strong> <?= $sic['responsavel'] ?></p><?php } ?>

								<?php if($sic['endereco'] != "") { ?><p><strong>Endere&ccedil;o:</strong> <?= $sic['endereco'] ?></p><?php } ?>

								<?php if($sic['telefone'] != "") { ?><p><strong>Telefone:</strong> <?= $sic['telefone'] ?></p><?php } ?>

								<?php if($sic['horario_atendimento'] != "") { ?><p><strong>Hor&aacute;rio de Atendimento:</strong> <?= $sic['horario_atendimento'] ?></p><?php } ?>

								<?php if($sic['observacao'] != "") { ?><p><strong>Observa&ccedil;&otilde;es:</strong> <?= $sic['observacao'] ?></p><?php } ?>

							</div>

						</div>

					</div>

					<?php

					}



					echo "</div>";



				}



				break;

			case "52":

				$qryAta = $conn->query("SELECT * FROM ata_registro_preco WHERE id_cliente = '$cliente' AND status_registro = 'A' AND (numero LIKE '%$busca%' OR objeto LIKE '%$busca%') ORDER BY data_inicio DESC, id DESC")->fetchAll(PDO::FETCH_ASSOC);



					$contador = count($qryAta);
				if(count($qryAta) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $ataRegistroPreco . "$complemento'>Ata de Registro de Pre&ccedil;os</a><span class='badge'>$contador</span></h3>";

					echo '<div class="panel-group" id="accordion_ata" role="tablist" aria-multiselectable="true">';



					foreach($qryAta as $ata) {

					?>

					<div class="panel panel-default">

					    <div class="panel-heading" role="tab" id="heading_<?= $ata['id'] ?>">

					     	<h4 class="panel-title">

					        	<a role="button" data-toggle="collapse" data-parent="#accordion_ata" href="#collapse_<?= $ata['id'] ?>" aria-expanded="true" aria-controls="collapse_<?= $ata['id'] ?>">

					          		<div class="row">

					          			<div class="col-sm-6"><i class="glyphicon glyphicon-triangle-right"></i> <strong>Contratado:</strong> <?= $ata['contratado'] ?></div>

					          			<div class="col-sm-3"><strong>Processo:</strong> <?= $ata['processo'] ?></div>

					          			<div class="col-sm-3"><strong>N&uacute;mero:</strong> <?= $ata['numero'] ?></div>

					          		</div>

					        	</a>

					    	</h4>

					    </div>

						<div id="collapse_<?= $ata['id'] ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_<?= $ata['id'] ?>">

							<div class="panel-body">

								<p><strong>Domic&iacute;lio:</strong> <?= $ata['domicilio'] ?></p>

								<p><strong>Partes:</strong> <?= $ata['partes'] ?></p>

								<p><strong>Data de In&iacute;cio:</strong> <?= formata_data($ata['data_inicio']) ?></p>

								<p><strong>Data de T&eacute;rmino:</strong> <?= formata_data($ata['data_fim']) ?></p>

								<p><strong>Data da Assinatura:</strong> <?= formata_data($ata['data_assinatura']) ?></p>

								<p><strong>Vig&ecirc;ncia:</strong> <?= $ata['vigencia'] ?></p>

								<p><strong>Valor Global:</strong> <?= $ata['valor_global'] ?></p>

								<p><strong>Objeto:</strong> <?= verifica($ata['objeto']) ?></p>



								<?php

								$stAnexo = $conn->prepare("SELECT * FROM ata_registro_preco_anexo WHERE id_artigo = :id_artigo ORDER BY ordem ASC, id DESC");

								$stAnexo->execute(array("id_artigo" => $ata['id']));

								$qryAnexo = $stAnexo->fetchAll();



								if(count($qryAnexo)) {

								?>

								<div class="panel panel-primary">

									<div class="panel-heading">

								    	<h2 class="panel-title"><i class="glyphicon glyphicon-cloud-download"></i> Anexos</h2>

								  	</div>

									<div class="panel-body">

										<?php

										foreach ($qryAnexo as $anexo) {

										?>

										<p><strong><a href="<?= $CAMINHOARQ ?>/<?= $anexo['arquivo'] ?>" target="_blank"><i class="glyphicon glyphicon-cloud-download"></i> <?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?></a></strong></p>

										<?php } ?>

									</div>

								</div>

								<?php } ?>

							</div>

						</div>

					</div>

					<?php

					}



					echo "</div>";



				}



				break;

			case "53":

				$qryPrestacaoContas = $conn->query("SELECT prestacao_contas.*, prestacao_contas_categoria.descricao categoria, prestacao_contas_subcategoria.descricao subcategoria

												   	  FROM prestacao_contas

											  	 LEFT JOIN prestacao_contas_categoria ON prestacao_contas.id_categoria = prestacao_contas_categoria.id

											  	 LEFT JOIN prestacao_contas_subcategoria ON prestacao_contas.id_subcategoria = prestacao_contas_subcategoria.id

												 	 WHERE prestacao_contas_categoria.id_cliente = '$cliente'

												       AND prestacao_contas.status_registro = 'A'

												       AND (prestacao_contas.titulo LIKE '%$busca%' OR prestacao_contas_categoria.descricao LIKE '%$busca%' OR prestacao_contas_subcategoria.descricao LIKE '%$busca%')

											   	  ORDER BY prestacao_contas.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryPrestacaoContas);
				if(count($qryPrestacaoContas) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $prestacaoContas . "$complemento'>Presta&ccedil;&atilde;o de Contas</a><span class='badge'>$contador</span></h3>";



					foreach($qryPrestacaoContas as $prestacao_contas) {



						if($prestacao_contas['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$prestacao_contas[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$prestacao_contas[categoria]&nbsp;&raquo;&nbsp;$prestacao_contas[subcategoria]&nbsp;&raquo;&nbsp;$prestacao_contas[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$prestacao_contas[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$prestacao_contas[categoria]&nbsp;&raquo;&nbsp;$prestacao_contas[subcategoria]&nbsp;&raquo;&nbsp;$prestacao_contas[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}



				break;

			case "54":

				$qryLei4320 = $conn->query("SELECT lei_4320.*, lei_4320_categoria.descricao categoria, lei_4320_subcategoria.descricao subcategoria

										   	  FROM lei_4320

									  	 LEFT JOIN lei_4320_categoria ON lei_4320.id_categoria = lei_4320_categoria.id

									  	 LEFT JOIN lei_4320_subcategoria ON lei_4320.id_subcategoria = lei_4320_subcategoria.id

										 	 WHERE lei_4320_categoria.id_cliente = '$cliente'

										       AND lei_4320.status_registro = 'A'

										       AND (lei_4320.titulo LIKE '%$busca%' OR lei_4320_categoria.descricao LIKE '%$busca%' OR lei_4320_subcategoria.descricao LIKE '%$busca%')

									   	  ORDER BY lei_4320.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryLei4320);
				if(count($qryLei4320) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $lei4320 . "$complemento'>Anexos da Lei 4320/64</a><span class='badge'>$contador</span></h3>";



					foreach($qryLei4320 as $lei_4320) {



						if($lei_4320['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$lei_4320[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$lei_4320[categoria]&nbsp;&raquo;&nbsp;$lei_4320[subcategoria]&nbsp;&raquo;&nbsp;$lei_4320[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$lei_4320[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$lei_4320[categoria]&nbsp;&raquo;&nbsp;$lei_4320[subcategoria]&nbsp;&raquo;&nbsp;$lei_4320[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}



				break;

			case "55":

				$qryLeiResponsabilidadeFiscal = $conn->query("SELECT lei_reponsabilidade_fiscal.*, lei_reponsabilidade_fiscal_categoria.descricao categoria, lei_reponsabilidade_fiscal_subcategoria.descricao subcategoria

															   	FROM lei_reponsabilidade_fiscal

														   LEFT JOIN lei_reponsabilidade_fiscal_categoria ON lei_reponsabilidade_fiscal.id_categoria = lei_reponsabilidade_fiscal_categoria.id

														   LEFT JOIN lei_reponsabilidade_fiscal_subcategoria ON lei_reponsabilidade_fiscal.id_subcategoria = lei_reponsabilidade_fiscal_subcategoria.id

															   WHERE lei_reponsabilidade_fiscal_categoria.id_cliente = '$cliente'

															     AND lei_reponsabilidade_fiscal.status_registro = 'A'

															     AND (lei_reponsabilidade_fiscal.titulo LIKE '%$busca%' OR lei_reponsabilidade_fiscal_categoria.descricao LIKE '%$busca%' OR lei_reponsabilidade_fiscal_subcategoria.descricao LIKE '%$busca%')

														   	ORDER BY lei_reponsabilidade_fiscal.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryLeiResponsabilidadeFiscal);
				if(count($qryLeiResponsabilidadeFiscal) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $leiResponsabilidadeFiscal . "$complemento'>Lei de Responsabilidade Fiscal</a><span class='badge'>$contador</span></h3>";



					foreach($qryLeiResponsabilidadeFiscal as $lei_reponsabilidade_fiscal) {



						if($lei_reponsabilidade_fiscal['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$lei_reponsabilidade_fiscal[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$lei_reponsabilidade_fiscal[categoria]&nbsp;&raquo;&nbsp;$lei_reponsabilidade_fiscal[subcategoria]&nbsp;&raquo;&nbsp;$lei_reponsabilidade_fiscal[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$lei_reponsabilidade_fiscal[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$lei_reponsabilidade_fiscal[categoria]&nbsp;&raquo;&nbsp;$lei_reponsabilidade_fiscal[subcategoria]&nbsp;&raquo;&nbsp;$lei_reponsabilidade_fiscal[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}



				break;

			case "56":

				$qryCompraDireta = $conn->query("SELECT compra_direta.*, compra_direta_categoria.descricao categoria, compra_direta_subcategoria.descricao subcategoria

											   	   FROM compra_direta

										  	  LEFT JOIN compra_direta_categoria ON compra_direta.id_categoria = compra_direta_categoria.id

										  	  LEFT JOIN compra_direta_subcategoria ON compra_direta.id_subcategoria = compra_direta_subcategoria.id

											 	  WHERE compra_direta_categoria.id_cliente = '$cliente'

											        AND compra_direta.status_registro = 'A'

											        AND (compra_direta.titulo LIKE '%$busca%' OR compra_direta_categoria.descricao LIKE '%$busca%' OR compra_direta_subcategoria.descricao LIKE '%$busca%')

										   	   ORDER BY compra_direta.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryCompraDireta);
				if(count($qryCompraDireta) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $compraDireta . "$complemento'>Compra Direta</a><span class='badge'>$contador</span></h3>";



					foreach($qryCompraDireta as $compra_direta) {



						if($compra_direta['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$compra_direta[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$compra_direta[categoria]&nbsp;&raquo;&nbsp;$compra_direta[subcategoria]&nbsp;&raquo;&nbsp;$compra_direta[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$compra_direta[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$compra_direta[categoria]&nbsp;&raquo;&nbsp;$compra_direta[subcategoria]&nbsp;&raquo;&nbsp;$compra_direta[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}



				break;

			case "57":

				$qryVeiculo = $conn->query("SELECT veiculo.*, veiculo_categoria.descricao categoria, veiculo_subcategoria.descricao subcategoria

										   	  FROM veiculo

									  	 LEFT JOIN veiculo_categoria ON veiculo.id_categoria = veiculo_categoria.id

									  	 LEFT JOIN veiculo_subcategoria ON veiculo.id_subcategoria = veiculo_subcategoria.id

										 	 WHERE veiculo_categoria.id_cliente = '$cliente'

										       AND veiculo.status_registro = 'A'

										       AND (veiculo.titulo LIKE '%$busca%' OR veiculo_categoria.descricao LIKE '%$busca%' OR veiculo_subcategoria.descricao LIKE '%$busca%')

									   	  ORDER BY veiculo.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryVeiculo);
				if(count($qryVeiculo) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $veiculo . "$complemento'>Frota de Ve&iacute;culos</a><span class='badge'>$contador</span></h3>";



					foreach($qryVeiculo as $veiculo) {



						if($veiculo['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$veiculo[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$veiculo[categoria]&nbsp;&raquo;&nbsp;$veiculo[subcategoria]&nbsp;&raquo;&nbsp;$veiculo[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$veiculo[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$veiculo[categoria]&nbsp;&raquo;&nbsp;$veiculo[subcategoria]&nbsp;&raquo;&nbsp;$veiculo[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}



				break;

			case "58":

				if($configuracaoTransparencia['exibir_acesso_informacao'] != "S") {



					$qryInformacao = $conn->query("SELECT acesso_informacao.*, acesso_informacao_categoria.descricao categoria, acesso_informacao_subcategoria.descricao subcategoria

													 FROM acesso_informacao

												LEFT JOIN acesso_informacao_categoria ON acesso_informacao.id_categoria = acesso_informacao_categoria.id

												LEFT JOIN acesso_informacao_subcategoria ON acesso_informacao.id_subcategoria = acesso_informacao_subcategoria.id

													WHERE acesso_informacao_categoria.id_cliente = '$cliente'

													  AND acesso_informacao.status_registro = 'A'

													  AND (acesso_informacao.titulo LIKE '%$busca%' OR acesso_informacao_categoria.descricao LIKE '%$busca%' OR acesso_informacao_subcategoria.descricao LIKE '%$busca%')

												 ORDER BY acesso_informacao.titulo")->fetchAll(PDO::FETCH_ASSOC);



					$contador = count($qryInformacao);
					if(count($qryInformacao) > 0) {
						$contaResultados++;



						echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $acesso . "$complemento'>Lei de Acesso &agrave; Informa&ccedil;&atilde;o</a><span class='badge'>$contador</span></h3>";



						foreach($qryInformacao as $acesso_informacao) {



							if($acesso_informacao['arquivo'] != "")

								echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$acesso_informacao[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$acesso_informacao[categoria]&nbsp;&raquo;&nbsp;$acesso_informacao[subcategoria]&nbsp;&raquo;&nbsp;$acesso_informacao[titulo]</strong></a></p>";

							else

								echo "<p style='margin: 3px 0px;'><a href='$acesso_informacao[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$acesso_informacao[categoria]&nbsp;&raquo;&nbsp;$acesso_informacao[subcategoria]&nbsp;&raquo;&nbsp;$acesso_informacao[titulo]</strong></a></p>";



						}



						echo "<p>&nbsp;</p>";



					}



				}



				break;

			case "59":

				$qryIntegra = $conn->query("SELECT licitacao_integra.*, licitacao_integra_categoria.descricao categoria, licitacao_integra_subcategoria.descricao subcategoria

										   	  FROM licitacao_integra

									  	 LEFT JOIN licitacao_integra_categoria ON licitacao_integra.id_categoria = licitacao_integra_categoria.id

									  	 LEFT JOIN licitacao_integra_subcategoria ON licitacao_integra.id_subcategoria = licitacao_integra_subcategoria.id

										 	 WHERE licitacao_integra_categoria.id_cliente = '$cliente'

										       AND licitacao_integra.status_registro = 'A'

										       AND (licitacao_integra.titulo LIKE '%$busca%' OR licitacao_integra_categoria.descricao LIKE '%$busca%' OR licitacao_integra_subcategoria.descricao LIKE '%$busca%')

									   	  ORDER BY licitacao_integra.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryIntegra);
				if(count($qryIntegra) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $licitacaoIntegra . "$complemento'>Licita&ccedil;&otilde;es na &Iacute;ntegra</a><span class='badge'>$contador</span></h3>";



					foreach($qryIntegra as $licitacao_integra) {



						if($licitacao_integra['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$licitacao_integra[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$licitacao_integra[categoria]&nbsp;&raquo;&nbsp;$licitacao_integra[subcategoria]&nbsp;&raquo;&nbsp;$licitacao_integra[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$licitacao_integra[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$licitacao_integra[categoria]&nbsp;&raquo;&nbsp;$licitacao_integra[subcategoria]&nbsp;&raquo;&nbsp;$licitacao_integra[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}



				break;

			case "60":

				$qryCodigo = $conn->query("SELECT codigo_tributario.*, codigo_tributario_categoria.descricao categoria, codigo_tributario_subcategoria.descricao subcategoria

										   	 FROM codigo_tributario

									  	LEFT JOIN codigo_tributario_categoria ON codigo_tributario.id_categoria = codigo_tributario_categoria.id

									  	LEFT JOIN codigo_tributario_subcategoria ON codigo_tributario.id_subcategoria = codigo_tributario_subcategoria.id

										 	WHERE codigo_tributario_categoria.id_cliente = '$cliente'

										      AND codigo_tributario.status_registro = 'A'

										      AND (codigo_tributario.titulo LIKE '%$busca%' OR codigo_tributario_categoria.descricao LIKE '%$busca%' OR codigo_tributario_subcategoria.descricao LIKE '%$busca%')

									   	 ORDER BY codigo_tributario.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryCodigo);
				if(count($qryCodigo) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $licitacaoCodigo . "$complemento'>C&oacute;digo Tribut&aacute;rio Municipal</a><span class='badge'>$contador</span></h3>";



					foreach($qryCodigo as $codigo_tributario) {



						if($codigo_tributario['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$codigo_tributario[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$codigo_tributario[categoria]&nbsp;&raquo;&nbsp;$codigo_tributario[subcategoria]&nbsp;&raquo;&nbsp;$codigo_tributario[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$codigo_tributario[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$codigo_tributario[categoria]&nbsp;&raquo;&nbsp;$codigo_tributario[subcategoria]&nbsp;&raquo;&nbsp;$codigo_tributario[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}



				break;

			case "61":

				$qryEstatuto = $conn->query("SELECT estatuto_servidor.*, estatuto_servidor_categoria.descricao categoria, estatuto_servidor_subcategoria.descricao subcategoria

										   	   FROM estatuto_servidor

									  	  LEFT JOIN estatuto_servidor_categoria ON estatuto_servidor.id_categoria = estatuto_servidor_categoria.id

									  	  LEFT JOIN estatuto_servidor_subcategoria ON estatuto_servidor.id_subcategoria = estatuto_servidor_subcategoria.id

										 	  WHERE estatuto_servidor_categoria.id_cliente = '$cliente'

										        AND estatuto_servidor.status_registro = 'A'

										        AND (estatuto_servidor.titulo LIKE '%$busca%' OR estatuto_servidor_categoria.descricao LIKE '%$busca%' OR estatuto_servidor_subcategoria.descricao LIKE '%$busca%')

									   	   ORDER BY estatuto_servidor.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryEstatuto);
				if(count($qryEstatuto) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $licitacaoEstatuto . "$complemento'>Estatuto dos Servidores</a><span class='badge'>$contador</span></h3>";



					foreach($qryEstatuto as $estatuto_servidor) {



						if($estatuto_servidor['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$estatuto_servidor[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$estatuto_servidor[categoria]&nbsp;&raquo;&nbsp;$estatuto_servidor[subcategoria]&nbsp;&raquo;&nbsp;$estatuto_servidor[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$estatuto_servidor[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$estatuto_servidor[categoria]&nbsp;&raquo;&nbsp;$estatuto_servidor[subcategoria]&nbsp;&raquo;&nbsp;$estatuto_servidor[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}



				break;

			case "62":

				$qryInstrucao = $conn->query("SELECT * FROM instrucao_normativa 

											   WHERE id_cliente = '$cliente' 

												 AND status_registro = 'A' 

												 AND (titulo LIKE '%$busca%' OR artigo LIKE '%$busca%') 

											ORDER BY titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryInstrucao);
				if(count($qryInstrucao) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $instrucaoNormativa . "$complemento'>Instru&ccedil;&atilde;o Normativa</a><span class='badge'>$contador</span></h3>";



					?>

					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

						<?php foreach ($qryInstrucao as $instrucao) { ?>

						<div class="panel panel-default">

						    <div class="panel-heading" role="tab" id="heading_<?= $instrucao['id'] ?>">

						     	<h4 class="panel-title">

						        	<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?= $instrucao['id'] ?>" aria-expanded="true" aria-controls="collapse_<?= $instrucao['id'] ?>">

						          	<strong><i class="glyphicon glyphicon-triangle-right"></i> <?= verifica($instrucao['titulo']) ?></strong>

						        	</a>

						    	</h4>

						    </div>

							<div id="collapse_<?= $instrucao['id'] ?>" class="panel-collapse <?php if(count($qryInstrucao) > 1) echo "collapse"; ?>" role="tabpanel" aria-labelledby="heading_<?= $instrucao['id'] ?>">

								<div class="panel-body">

									<?php if($instrucao['data_publicacao'] != "") { ?><p><strong>Data de Publica&ccedil;&atilde;o:</strong> <?= formata_data($instrucao['data_publicacao']) ?></p><?php } ?>

									<?= $instrucao['artigo'] ?>

									<?php if($instrucao['arquivo'] != "") { ?><p><a href="<?= $CAMINHOARQ ?>/<?= $instrucao['arquivo'] ?>" target="_blank"><strong class="text-primary"><i class="glyphicon glyphicon-cloud-download"></i> Baixar Arquivo</strong></a></p><?php } ?>

								</div>

							</div>

						</div>

						<?php } ?>

					</div>

					<?php



				}

				break;

			case "63":

		        $qryRpps = $conn->query("SELECT rpps.*, rpps_categoria.descricao categoria, rpps_subcategoria.descricao subcategoria

									   	   FROM rpps

								  	  LEFT JOIN rpps_categoria ON rpps.id_categoria = rpps_categoria.id

								  	  LEFT JOIN rpps_subcategoria ON rpps.id_subcategoria = rpps_subcategoria.id

									 	  WHERE rpps_categoria.id_cliente = '$cliente'

									        AND rpps.status_registro = 'A'

									        AND (rpps.titulo LIKE '%$busca%' OR rpps_categoria.descricao LIKE '%$busca%' OR rpps_subcategoria.descricao LIKE '%$busca%')

								   	   ORDER BY rpps.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryRpps);
				if(count($qryRpps) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $rpps . "$complemento'>RPPS - Fundo de Previd&ecirc;ncia</a><span class='badge'>$contador</span></h3>";



					foreach($qryRpps as $rpps) {



						if($rpps['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$rpps[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$rpps[categoria]&nbsp;&raquo;&nbsp;$rpps[subcategoria]&nbsp;&raquo;&nbsp;$rpps[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$rpps[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$rpps[categoria]&nbsp;&raquo;&nbsp;$rpps[subcategoria]&nbsp;&raquo;&nbsp;$rpps[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}

				break;

			case "64":

				$qryExtratoConta = $conn->query("SELECT extrato_conta.*, extrato_conta_categoria.descricao categoria, extrato_conta_subcategoria.descricao subcategoria

												   FROM extrato_conta

											  LEFT JOIN extrato_conta_categoria ON extrato_conta.id_categoria = extrato_conta_categoria.id

											  LEFT JOIN extrato_conta_subcategoria ON extrato_conta.id_subcategoria = extrato_conta_subcategoria.id

												  WHERE extrato_conta_categoria.id_cliente = '$cliente'

													AND extrato_conta.status_registro = 'A'

													AND (extrato_conta.titulo LIKE '%$busca%' OR extrato_conta_categoria.descricao LIKE '%$busca%' OR extrato_conta_subcategoria.descricao LIKE '%$busca%')

											   ORDER BY extrato_conta.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryExtratoConta);
				if(count($qryExtratoConta) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $extratoConta . "$complemento'>Extrato de Contas</a><span class='badge'>$contador</span></h3>";



					foreach($qryExtratoConta as $extrato_conta) {



						if($extrato_conta['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$extrato_conta[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$extrato_conta[categoria]&nbsp;&raquo;&nbsp;$extrato_conta[subcategoria]&nbsp;&raquo;&nbsp;$extrato_conta[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$extrato_conta[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$extrato_conta[categoria]&nbsp;&raquo;&nbsp;$extrato_conta[subcategoria]&nbsp;&raquo;&nbsp;$extrato_conta[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}

				break;

			case "65":

		        $qryServidorCedido = $conn->query("SELECT servidor_cedido.*, servidor_cedido_categoria.descricao categoria, servidor_cedido_subcategoria.descricao subcategoria

												   	 FROM servidor_cedido

											  	LEFT JOIN servidor_cedido_categoria ON servidor_cedido.id_categoria = servidor_cedido_categoria.id

											  	LEFT JOIN servidor_cedido_subcategoria ON servidor_cedido.id_subcategoria = servidor_cedido_subcategoria.id

												  	WHERE servidor_cedido_categoria.id_cliente = '$cliente'

													  AND servidor_cedido.status_registro = 'A'

													  AND (servidor_cedido.titulo LIKE '%$busca%' OR servidor_cedido_categoria.descricao LIKE '%$busca%' OR servidor_cedido_subcategoria.descricao LIKE '%$busca%')

											   	 ORDER BY servidor_cedido.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryServidorCedido);
				if(count($qryServidorCedido) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $servidorCedido . "$complemento'>Servidores Cedidos</a><span class='badge'>$contador</span></h3>";



					foreach($qryServidorCedido as $servidor_cedido) {



						if($servidor_cedido['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$servidor_cedido[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$servidor_cedido[categoria]&nbsp;&raquo;&nbsp;$servidor_cedido[subcategoria]&nbsp;&raquo;&nbsp;$servidor_cedido[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$servidor_cedido[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$servidor_cedido[categoria]&nbsp;&raquo;&nbsp;$servidor_cedido[subcategoria]&nbsp;&raquo;&nbsp;$servidor_cedido[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}

				break;

			case "66":

				$qryServidorTemporario = $conn->query("SELECT servidor_temporario.*, servidor_temporario_categoria.descricao categoria, servidor_temporario_subcategoria.descricao subcategoria

													   	 FROM servidor_temporario

												  	LEFT JOIN servidor_temporario_categoria ON servidor_temporario.id_categoria = servidor_temporario_categoria.id

												  	LEFT JOIN servidor_temporario_subcategoria ON servidor_temporario.id_subcategoria = servidor_temporario_subcategoria.id

													 	WHERE servidor_temporario_categoria.id_cliente = '$cliente'

														  AND servidor_temporario.status_registro = 'A'

														  AND (servidor_temporario.titulo LIKE '%$busca%' OR servidor_temporario_categoria.descricao LIKE '%$busca%' OR servidor_temporario_subcategoria.descricao LIKE '%$busca%')

												   	 ORDER BY servidor_temporario.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryServidorTemporario);
				if(count($qryServidorTemporario) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $servidorTemporario . "$complemento'>Servidores Tempor&aacute;rios</a><span class='badge'>$contador</span></h3>";



					foreach($qryServidorTemporario as $servidor_temporario) {



						if($servidor_temporario['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$servidor_temporario[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$servidor_temporario[categoria]&nbsp;&raquo;&nbsp;$servidor_temporario[subcategoria]&nbsp;&raquo;&nbsp;$servidor_temporario[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$servidor_temporario[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$servidor_temporario[categoria]&nbsp;&raquo;&nbsp;$servidor_temporario[subcategoria]&nbsp;&raquo;&nbsp;$servidor_temporario[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}

				break;

			case "67":

		        $qryRecursoEducacao = $conn->query("SELECT recurso_educacao.*, recurso_educacao_categoria.descricao categoria, recurso_educacao_subcategoria.descricao subcategoria

													  FROM recurso_educacao

												 LEFT JOIN recurso_educacao_categoria ON recurso_educacao.id_categoria = recurso_educacao_categoria.id

												 LEFT JOIN recurso_educacao_subcategoria ON recurso_educacao.id_subcategoria = recurso_educacao_subcategoria.id

													 WHERE recurso_educacao_categoria.id_cliente = '$cliente'

													   AND recurso_educacao.status_registro = 'A'

													   AND (recurso_educacao.titulo LIKE '%$busca%' OR recurso_educacao_categoria.descricao LIKE '%$busca%' OR recurso_educacao_subcategoria.descricao LIKE '%$busca%')

												  ORDER BY recurso_educacao.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryRecursoEducacao);
				if(count($qryRecursoEducacao) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $recursoEducacao . "$complemento'>Recursos da Educa&ccedil;&atilde;o - Governo Federal</a><span class='badge'>$contador</span></h3>";



					foreach($qryRecursoEducacao as $recurso_educacao) {



						if($recurso_educacao['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$recurso_educacao[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$recurso_educacao[categoria]&nbsp;&raquo;&nbsp;$recurso_educacao[subcategoria]&nbsp;&raquo;&nbsp;$recurso_educacao[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$recurso_educacao[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$recurso_educacao[categoria]&nbsp;&raquo;&nbsp;$recurso_educacao[subcategoria]&nbsp;&raquo;&nbsp;$recurso_educacao[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}

				break;

			case "68":

		        $qryCredor = $conn->query("SELECT credor.*, credor_categoria.descricao categoria, credor_subcategoria.descricao subcategoria

											 FROM credor

										LEFT JOIN credor_categoria ON credor.id_categoria = credor_categoria.id

										LEFT JOIN credor_subcategoria ON credor.id_subcategoria = credor_subcategoria.id

											WHERE credor_categoria.id_cliente = '$cliente'

											  AND credor.status_registro = 'A'

											  AND (credor.titulo LIKE '%$busca%' OR credor_categoria.descricao LIKE '%$busca%' OR credor_subcategoria.descricao LIKE '%$busca%')

										 ORDER BY credor.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryCredor);
				if(count($qryCredor) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $credor . "$complemento'>Credores</a><span class='badge'>$contador</span></h3>";



					foreach($qryCredor as $credor) {



						if($credor['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$credor[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$credor[categoria]&nbsp;&raquo;&nbsp;$credor[subcategoria]&nbsp;&raquo;&nbsp;$credor[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$credor[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$credor[categoria]&nbsp;&raquo;&nbsp;$credor[subcategoria]&nbsp;&raquo;&nbsp;$credor[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}

				break;

			case "69":

		        $qryPlanoEducacao = $conn->query("SELECT plano_educacao.*, plano_educacao_categoria.descricao categoria, plano_educacao_subcategoria.descricao subcategoria

													FROM plano_educacao

											   LEFT JOIN plano_educacao_categoria ON plano_educacao.id_categoria = plano_educacao_categoria.id

											   LEFT JOIN plano_educacao_subcategoria ON plano_educacao.id_subcategoria = plano_educacao_subcategoria.id

												   WHERE plano_educacao_categoria.id_cliente = '$cliente'

													 AND plano_educacao.status_registro = 'A'

													 AND (plano_educacao.titulo LIKE '%$busca%' OR plano_educacao_categoria.descricao LIKE '%$busca%' OR plano_educacao_subcategoria.descricao LIKE '%$busca%')

												ORDER BY plano_educacao.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryPlanoEducacao);
				if(count($qryPlanoEducacao) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $planoEducacao . "$complemento'>Plano Municipal de Educa&ccedil;&atilde;o</a><span class='badge'>$contador</span></h3>";



					foreach($qryPlanoEducacao as $plano_educacao) {



						if($plano_educacao['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$plano_educacao[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$plano_educacao[categoria]&nbsp;&raquo;&nbsp;$plano_educacao[subcategoria]&nbsp;&raquo;&nbsp;$plano_educacao[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$plano_educacao[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$plano_educacao[categoria]&nbsp;&raquo;&nbsp;$plano_educacao[subcategoria]&nbsp;&raquo;&nbsp;$plano_educacao[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}

				break;

			case "70":

		        $qryPlanoHabitacao = $conn->query("SELECT plano_habitacao.*, plano_habitacao_categoria.descricao categoria, plano_habitacao_subcategoria.descricao subcategoria

													 FROM plano_habitacao

											    LEFT JOIN plano_habitacao_categoria ON plano_habitacao.id_categoria = plano_habitacao_categoria.id

											    LEFT JOIN plano_habitacao_subcategoria ON plano_habitacao.id_subcategoria = plano_habitacao_subcategoria.id

												    WHERE plano_habitacao_categoria.id_cliente = '$cliente'

													  AND plano_habitacao.status_registro = 'A'

													  AND (plano_habitacao.titulo LIKE '%$busca%' OR plano_habitacao_categoria.descricao LIKE '%$busca%' OR plano_habitacao_subcategoria.descricao LIKE '%$busca%')

												 ORDER BY plano_habitacao.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryPlanoHabitacao);
				if(count($qryPlanoHabitacao) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $planoHabitacao . "$complemento'>Plano Municipal de Habita&ccedil;&atilde;o</a><span class='badge'>$contador</span></h3>";



					foreach($qryPlanoHabitacao as $plano_habitacao) {



						if($plano_habitacao['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$plano_habitacao[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$plano_habitacao[categoria]&nbsp;&raquo;&nbsp;$plano_habitacao[subcategoria]&nbsp;&raquo;&nbsp;$plano_habitacao[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$plano_habitacao[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$plano_habitacao[categoria]&nbsp;&raquo;&nbsp;$plano_habitacao[subcategoria]&nbsp;&raquo;&nbsp;$plano_habitacao[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}

				break;

			case "71":

		        $qryAjudaCusto = $conn->query("SELECT ajuda_custo.*, ajuda_custo_categoria.descricao categoria, ajuda_custo_subcategoria.descricao subcategoria

												 FROM ajuda_custo

											LEFT JOIN ajuda_custo_categoria ON ajuda_custo.id_categoria = ajuda_custo_categoria.id

											LEFT JOIN ajuda_custo_subcategoria ON ajuda_custo.id_subcategoria = ajuda_custo_subcategoria.id

												WHERE ajuda_custo_categoria.id_cliente = '$cliente'

												  AND ajuda_custo.status_registro = 'A'

												  AND (ajuda_custo.titulo LIKE '%$busca%' OR ajuda_custo_categoria.descricao LIKE '%$busca%' OR ajuda_custo_subcategoria.descricao LIKE '%$busca%')

											 ORDER BY ajuda_custo.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryAjudaCusto);
				if(count($qryAjudaCusto) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $ajudaCusto . "$complemento'>Ajuda de Custos</a><span class='badge'>$contador</span></h3>";



					foreach($qryAjudaCusto as $ajuda_custo) {



						if($ajuda_custo['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$ajuda_custo[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$ajuda_custo[categoria]&nbsp;&raquo;&nbsp;$ajuda_custo[subcategoria]&nbsp;&raquo;&nbsp;$ajuda_custo[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$ajuda_custo[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$ajuda_custo[categoria]&nbsp;&raquo;&nbsp;$ajuda_custo[subcategoria]&nbsp;&raquo;&nbsp;$ajuda_custo[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}

				break;

			case "72":

		        $qryVerbaGabinete = $conn->query("SELECT verba_gabinete.*, verba_gabinete_categoria.descricao categoria, verba_gabinete_subcategoria.descricao subcategoria

													FROM verba_gabinete

											   LEFT JOIN verba_gabinete_categoria ON verba_gabinete.id_categoria = verba_gabinete_categoria.id

											   LEFT JOIN verba_gabinete_subcategoria ON verba_gabinete.id_subcategoria = verba_gabinete_subcategoria.id

												   WHERE verba_gabinete_categoria.id_cliente = '$cliente'

													 AND verba_gabinete.status_registro = 'A'

													 AND (verba_gabinete.titulo LIKE '%$busca%' OR verba_gabinete_categoria.descricao LIKE '%$busca%' OR verba_gabinete_subcategoria.descricao LIKE '%$busca%')

												ORDER BY verba_gabinete.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryVerbaGabinete);
				if(count($qryVerbaGabinete) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $verbaGabinete . "$complemento'>Verbas de Gabinete</a><span class='badge'>$contador</span></h3>";



					foreach($qryVerbaGabinete as $verba_gabinete) {



						if($verba_gabinete['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$verba_gabinete[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$verba_gabinete[categoria]&nbsp;&raquo;&nbsp;$verba_gabinete[subcategoria]&nbsp;&raquo;&nbsp;$verba_gabinete[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$verba_gabinete[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$verba_gabinete[categoria]&nbsp;&raquo;&nbsp;$verba_gabinete[subcategoria]&nbsp;&raquo;&nbsp;$verba_gabinete[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}

				break;

			case "73":

		        $qryServidorRecebido = $conn->query("SELECT servidor_recebido.*, servidor_recebido_categoria.descricao categoria, servidor_recebido_subcategoria.descricao subcategoria

													   FROM servidor_recebido

												  LEFT JOIN servidor_recebido_categoria ON servidor_recebido.id_categoria = servidor_recebido_categoria.id

												  LEFT JOIN servidor_recebido_subcategoria ON servidor_recebido.id_subcategoria = servidor_recebido_subcategoria.id

													  WHERE servidor_recebido_categoria.id_cliente = '$cliente'

													    AND servidor_recebido.status_registro = 'A'

													    AND (servidor_recebido.titulo LIKE '%$busca%' OR servidor_recebido_categoria.descricao LIKE '%$busca%' OR servidor_recebido_subcategoria.descricao LIKE '%$busca%')

												   ORDER BY servidor_recebido.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryServidorRecebido);
				if(count($qryServidorRecebido) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $servidorRecebido . "$complemento'>Servidores Recebidos</a><span class='badge'>$contador</span></h3>";



					foreach($qryServidorRecebido as $servidor_recebido) {



						if($servidor_recebido['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$servidor_recebido[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$servidor_recebido[categoria]&nbsp;&raquo;&nbsp;$servidor_recebido[subcategoria]&nbsp;&raquo;&nbsp;$servidor_recebido[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$servidor_recebido[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$servidor_recebido[categoria]&nbsp;&raquo;&nbsp;$servidor_recebido[subcategoria]&nbsp;&raquo;&nbsp;$servidor_recebido[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}

				break;

			case "74":

		        $qryJuridico = $conn->query("SELECT juridico.*, juridico_categoria.descricao categoria, juridico_subcategoria.descricao subcategoria

											   FROM juridico

										  LEFT JOIN juridico_categoria ON juridico.id_categoria = juridico_categoria.id

										  LEFT JOIN juridico_subcategoria ON juridico.id_subcategoria = juridico_subcategoria.id

											  WHERE juridico_categoria.id_cliente = '$cliente'

											 	AND juridico.status_registro = 'A'

											  	AND (juridico.titulo LIKE '%$busca%' OR juridico_categoria.descricao LIKE '%$busca%' OR juridico_subcategoria.descricao LIKE '%$busca%')

										   ORDER BY juridico.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryJuridico);
				if(count($qryJuridico) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $juridico . "$complemento'>Jur&iacute;dico</a><span class='badge'>$contador</span></h3>";



					foreach($qryJuridico as $juridico) {



						if($juridico['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$juridico[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$juridico[categoria]&nbsp;&raquo;&nbsp;$juridico[subcategoria]&nbsp;&raquo;&nbsp;$juridico[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$juridico[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$juridico[categoria]&nbsp;&raquo;&nbsp;$juridico[subcategoria]&nbsp;&raquo;&nbsp;$juridico[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}

				break;

			case "75":

		        $qryProgramaAcao = $conn->query("SELECT programa_acao.*, programa_acao_categoria.descricao categoria, programa_acao_subcategoria.descricao subcategoria

												   FROM programa_acao

											  LEFT JOIN programa_acao_categoria ON programa_acao.id_categoria = programa_acao_categoria.id

											  LEFT JOIN programa_acao_subcategoria ON programa_acao.id_subcategoria = programa_acao_subcategoria.id

												  WHERE programa_acao_categoria.id_cliente = '$cliente'

												    AND programa_acao.status_registro = 'A'

												    AND (programa_acao.titulo LIKE '%$busca%' OR programa_acao_categoria.descricao LIKE '%$busca%' OR programa_acao_subcategoria.descricao LIKE '%$busca%')

											   ORDER BY programa_acao.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryProgramaAcao);
				if(count($qryProgramaAcao) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $programaAcao . "$complemento'>Programas e A&ccedil;&otilde;es</a><span class='badge'>$contador</span></h3>";



					foreach($qryProgramaAcao as $programa_acao) {



						if($programa_acao['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$programa_acao[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$programa_acao[categoria]&nbsp;&raquo;&nbsp;$programa_acao[subcategoria]&nbsp;&raquo;&nbsp;$programa_acao[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$programa_acao[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$programa_acao[categoria]&nbsp;&raquo;&nbsp;$programa_acao[subcategoria]&nbsp;&raquo;&nbsp;$programa_acao[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}

				break;

			case "76":

				$qryInformacao = $conn->query("SELECT relatorio_saude.*, relatorio_saude_categoria.descricao categoria, relatorio_saude_subcategoria.descricao subcategoria

										 FROM relatorio_saude

									LEFT JOIN relatorio_saude_categoria ON relatorio_saude.id_categoria = relatorio_saude_categoria.id

									LEFT JOIN relatorio_saude_subcategoria ON relatorio_saude.id_subcategoria = relatorio_saude_subcategoria.id

										WHERE relatorio_saude_categoria.id_cliente = '$cliente'

										  AND relatorio_saude.status_registro = 'A'

										  AND (relatorio_saude.titulo LIKE '%$busca%' OR relatorio_saude_categoria.descricao LIKE '%$busca%' OR relatorio_saude_subcategoria.descricao LIKE '%$busca%')

									 ORDER BY relatorio_saude.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryInformacao);
				if(count($qryInformacao) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $relatorioSaude . "$complemento'>Relat&oacute;rios de Gest&atilde;o - SA&Uacute;DE</a><span class='badge'>$contador</span></h3>";



					foreach($qryInformacao as $relatorio_saude) {



						if($relatorio_saude['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$relatorio_saude[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$relatorio_saude[categoria]&nbsp;&raquo;&nbsp;$relatorio_saude[subcategoria]&nbsp;&raquo;&nbsp;$relatorio_saude[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$relatorio_saude[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$relatorio_saude[categoria]&nbsp;&raquo;&nbsp;$relatorio_saude[subcategoria]&nbsp;&raquo;&nbsp;$relatorio_saude[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}

				break;

			case "77":

				$qryInformacao = $conn->query("SELECT verba_representacao.*, verba_representacao_categoria.descricao categoria, verba_representacao_subcategoria.descricao subcategoria

												 FROM verba_representacao

											LEFT JOIN verba_representacao_categoria ON verba_representacao.id_categoria = verba_representacao_categoria.id

											LEFT JOIN verba_representacao_subcategoria ON verba_representacao.id_subcategoria = verba_representacao_subcategoria.id

												WHERE verba_representacao_categoria.id_cliente = '$cliente'

												  AND verba_representacao.status_registro = 'A'

												  AND (verba_representacao.titulo LIKE '%$busca%' OR verba_representacao_categoria.descricao LIKE '%$busca%' OR verba_representacao_subcategoria.descricao LIKE '%$busca%')

											 ORDER BY verba_representacao.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryInformacao);
				if(count($qryInformacao) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $verbaRepresentacao . "$complemento'>Verbas de Representa&ccedil;&atilde;o de Gabinete</a><span class='badge'>$contador</span></h3>";



					foreach($qryInformacao as $verba_representacao) {



						if($verba_representacao['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$verba_representacao[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$verba_representacao[categoria]&nbsp;&raquo;&nbsp;$verba_representacao[subcategoria]&nbsp;&raquo;&nbsp;$verba_representacao[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$verba_representacao[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$verba_representacao[categoria]&nbsp;&raquo;&nbsp;$verba_representacao[subcategoria]&nbsp;&raquo;&nbsp;$verba_representacao[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}

				break;

			case "78":

				$qryInformacao = $conn->query("SELECT portarias_nomeacoes.*, portarias_nomeacoes_categoria.descricao categoria, portarias_nomeacoes_subcategoria.descricao subcategoria

												 FROM portarias_nomeacoes

											LEFT JOIN portarias_nomeacoes_categoria ON portarias_nomeacoes.id_categoria = portarias_nomeacoes_categoria.id

											LEFT JOIN portarias_nomeacoes_subcategoria ON portarias_nomeacoes.id_subcategoria = portarias_nomeacoes_subcategoria.id

												WHERE portarias_nomeacoes_categoria.id_cliente = '$cliente'

												  AND portarias_nomeacoes.status_registro = 'A'

												  AND (portarias_nomeacoes.titulo LIKE '%$busca%' OR portarias_nomeacoes_categoria.descricao LIKE '%$busca%' OR portarias_nomeacoes_subcategoria.descricao LIKE '%$busca%')

											 ORDER BY portarias_nomeacoes.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryInformacao);
				if(count($qryInformacao) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $portariasNomeacoesLista . "$complemento'>Portarias de Nomeac&ccedil;&otilde;es</a><span class='badge'>$contador</span></h3>";



					foreach($qryInformacao as $portarias_nomeacoes) {



						if($portarias_nomeacoes['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$portarias_nomeacoes[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$portarias_nomeacoes[categoria]&nbsp;&raquo;&nbsp;$portarias_nomeacoes[subcategoria]&nbsp;&raquo;&nbsp;$portarias_nomeacoes[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$portarias_nomeacoes[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$portarias_nomeacoes[categoria]&nbsp;&raquo;&nbsp;$portarias_nomeacoes[subcategoria]&nbsp;&raquo;&nbsp;$portarias_nomeacoes[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}

				break;

			case "79":

				$qryInformacao = $conn->query("SELECT atos_secretarias.*, atos_secretarias_categoria.descricao categoria, atos_secretarias_subcategoria.descricao subcategoria

												 FROM atos_secretarias

											LEFT JOIN atos_secretarias_categoria ON atos_secretarias.id_categoria = atos_secretarias_categoria.id

											LEFT JOIN atos_secretarias_subcategoria ON atos_secretarias.id_subcategoria = atos_secretarias_subcategoria.id

												WHERE atos_secretarias_categoria.id_cliente = '$cliente'

												  AND atos_secretarias.status_registro = 'A'

												  AND (atos_secretarias.titulo LIKE '%$busca%' OR atos_secretarias_categoria.descricao LIKE '%$busca%' OR atos_secretarias_subcategoria.descricao LIKE '%$busca%')

											 ORDER BY atos_secretarias.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryInformacao);
				if(count($qryInformacao) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $atosSecretarias . "$complemento'>Atos da Secretarias</a><span class='badge'>$contador</span></h3>";



					foreach($qryInformacao as $atos_secretarias) {



						if($atos_secretarias['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$atos_secretarias[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$atos_secretarias[categoria]&nbsp;&raquo;&nbsp;$atos_secretarias[subcategoria]&nbsp;&raquo;&nbsp;$atos_secretarias[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$atos_secretarias[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$atos_secretarias[categoria]&nbsp;&raquo;&nbsp;$atos_secretarias[subcategoria]&nbsp;&raquo;&nbsp;$atos_secretarias[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}

				break;



			case "80":

				$qryInformacao = $conn->query("SELECT precatorio.*, precatorio_categoria.descricao categoria, precatorio_subcategoria.descricao subcategoria

												 FROM precatorio

											LEFT JOIN precatorio_categoria ON precatorio.id_categoria = precatorio_categoria.id

											LEFT JOIN precatorio_subcategoria ON precatorio.id_subcategoria = precatorio_subcategoria.id

												WHERE precatorio_categoria.id_cliente = '$cliente'

												  AND precatorio.status_registro = 'A'

												  AND (precatorio.titulo LIKE '%$busca%' OR precatorio_categoria.descricao LIKE '%$busca%' OR precatorio_subcategoria.descricao LIKE '%$busca%')

											 ORDER BY precatorio.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryInformacao);
				if(count($qryInformacao) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $precatorio . "$complemento'>Precat&oacute;rios</a><span class='badge'>$contador</span></h3>";



					foreach($qryInformacao as $precatorio) {



						if($precatorio['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$precatorio[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$precatorio[categoria]&nbsp;&raquo;&nbsp;$precatorio[subcategoria]&nbsp;&raquo;&nbsp;$precatorio[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$precatorio[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$precatorio[categoria]&nbsp;&raquo;&nbsp;$precatorio[subcategoria]&nbsp;&raquo;&nbsp;$precatorio[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}

				break;



			case "81":

				$qryInformacao = $conn->query("SELECT extrato_bancario.*, extrato_bancario_categoria.descricao categoria, extrato_bancario_subcategoria.descricao subcategoria

												 FROM extrato_bancario

											LEFT JOIN extrato_bancario_categoria ON extrato_bancario.id_categoria = extrato_bancario_categoria.id

											LEFT JOIN extrato_bancario_subcategoria ON extrato_bancario.id_subcategoria = extrato_bancario_subcategoria.id

												WHERE extrato_bancario_categoria.id_cliente = '$cliente'

												  AND extrato_bancario.status_registro = 'A'

												  AND (extrato_bancario.titulo LIKE '%$busca%' OR extrato_bancario_categoria.descricao LIKE '%$busca%' OR extrato_bancario_subcategoria.descricao LIKE '%$busca%')

											 ORDER BY extrato_bancario.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryInformacao);
				if(count($qryInformacao) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $extratoBancario . "$complemento'>Extrato Banc&aacute;rio </a><span class='badge'>$contador</span></h3>";



					foreach($qryInformacao as $extBan) {



						if($extBan['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$extBan[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$extBan[categoria]&nbsp;&raquo;&nbsp;$extBan[subcategoria]&nbsp;&raquo;&nbsp;$extBan[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$extBan[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$extBan[categoria]&nbsp;&raquo;&nbsp;$extBan[subcategoria]&nbsp;&raquo;&nbsp;$extBan[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}

				break;





			case "82":

				$qryInformacao = $conn->query("SELECT documento_fiscal.*, documento_fiscal_categoria.descricao categoria, documento_fiscal_subcategoria.descricao subcategoria

												 FROM documento_fiscal

											LEFT JOIN documento_fiscal_categoria ON documento_fiscal.id_categoria = documento_fiscal_categoria.id

											LEFT JOIN documento_fiscal_subcategoria ON documento_fiscal.id_subcategoria = documento_fiscal_subcategoria.id

												WHERE documento_fiscal_categoria.id_cliente = '$cliente'

												  AND documento_fiscal.status_registro = 'A'

												  AND (documento_fiscal.titulo LIKE '%$busca%' OR documento_fiscal_categoria.descricao LIKE '%$busca%' OR documento_fiscal_subcategoria.descricao LIKE '%$busca%')

											 ORDER BY documento_fiscal.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryInformacao);
				if(count($qryInformacao) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $documentosFiscais . "$complemento'>Documento Fiscal</a><span class='badge'>$contador</span></h3>";



					foreach($qryInformacao as $documentoFiscal) {



						if($documentoFiscal['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$documentoFiscal[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$documentoFiscal[categoria]&nbsp;&raquo;&nbsp;$documentoFiscal[subcategoria]&nbsp;&raquo;&nbsp;$documentoFiscal[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$documentoFiscal[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$documentoFiscal[categoria]&nbsp;&raquo;&nbsp;$documentoFiscal[subcategoria]&nbsp;&raquo;&nbsp;$documentoFiscal[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}

				break;





			case "83":

				$qryInformacao = $conn->query("SELECT plano_carreira.*, plano_carreira_categoria.descricao categoria, plano_carreira_subcategoria.descricao subcategoria

												 FROM plano_carreira

											LEFT JOIN plano_carreira_categoria ON plano_carreira.id_categoria = plano_carreira_categoria.id

											LEFT JOIN plano_carreira_subcategoria ON plano_carreira.id_subcategoria = plano_carreira_subcategoria.id

												WHERE plano_carreira_categoria.id_cliente = '$cliente'

												  AND plano_carreira.status_registro = 'A'

												  AND (plano_carreira.titulo LIKE '%$busca%' OR plano_carreira_categoria.descricao LIKE '%$busca%' OR plano_carreira_subcategoria.descricao LIKE '%$busca%')

											 ORDER BY plano_carreira.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryInformacao);
				if(count($qryInformacao) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $planoCarreira . "$complemento'>Plano de Carreira</a><span class='badge'>$contador</span></h3>";



					foreach($qryInformacao as $plano_carreira) {



						if($plano_carreira['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$plano_carreira[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$plano_carreira[categoria]&nbsp;&raquo;&nbsp;$plano_carreira[subcategoria]&nbsp;&raquo;&nbsp;$plano_carreira[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$plano_carreira[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$plano_carreira[categoria]&nbsp;&raquo;&nbsp;$plano_carreira[subcategoria]&nbsp;&raquo;&nbsp;$plano_carreira[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}

				break;







			case "84":

				$qryInformacao = $conn->query("SELECT fundeb.*, fundeb_categoria.descricao categoria, fundeb_subcategoria.descricao subcategoria

												 FROM fundeb

											LEFT JOIN fundeb_categoria ON fundeb.id_categoria = fundeb_categoria.id

											LEFT JOIN fundeb_subcategoria ON fundeb.id_subcategoria = fundeb_subcategoria.id

												WHERE fundeb_categoria.id_cliente = '$cliente'

												  AND fundeb.status_registro = 'A'

												  AND (fundeb.titulo LIKE '%$busca%' OR fundeb_categoria.descricao LIKE '%$busca%' OR fundeb_subcategoria.descricao LIKE '%$busca%')

											 ORDER BY fundeb.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryInformacao);
				if(count($qryInformacao) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $fundeb . "$complemento'>FUNDEB</a><span class='badge'>$contador</span></h3>";



					foreach($qryInformacao as $fun) {



						if($fun['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$fun[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$fun[categoria]&nbsp;&raquo;&nbsp;$fun[subcategoria]&nbsp;&raquo;&nbsp;$fun[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$fun[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$fun[categoria]&nbsp;&raquo;&nbsp;$fun[subcategoria]&nbsp;&raquo;&nbsp;$fun[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}

				break;







			case "85":

				$qryInformacao = $conn->query("SELECT rem_agente_publico.*, rem_agente_publico_categoria.descricao categoria, rem_agente_publico_subcategoria.descricao subcategoria

												 FROM rem_agente_publico

											LEFT JOIN rem_agente_publico_categoria ON rem_agente_publico.id_categoria = rem_agente_publico_categoria.id

											LEFT JOIN rem_agente_publico_subcategoria ON rem_agente_publico.id_subcategoria = rem_agente_publico_subcategoria.id

												WHERE rem_agente_publico_categoria.id_cliente = '$cliente'

												  AND rem_agente_publico.status_registro = 'A'

												  AND (rem_agente_publico.titulo LIKE '%$busca%' OR rem_agente_publico_categoria.descricao LIKE '%$busca%' OR rem_agente_publico_subcategoria.descricao LIKE '%$busca%')

											 ORDER BY rem_agente_publico.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryInformacao);
				if(count($qryInformacao) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $remAgentePublico . "$complemento'>Remunera&cedil;&atilde;o Agentes P&uacute;blicos</a><span class='badge'>$contador</span></h3>";



					foreach($qryInformacao as $rem_agente_publico) {



						if($rem_agente_publico['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$rem_agente_publico[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$rem_agente_publico[categoria]&nbsp;&raquo;&nbsp;$rem_agente_publico[subcategoria]&nbsp;&raquo;&nbsp;$rem_agente_publico[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$rem_agente_publico[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$rem_agente_publico[categoria]&nbsp;&raquo;&nbsp;$rem_agente_publico[subcategoria]&nbsp;&raquo;&nbsp;$rem_agente_publico[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}

				break;





			case "86":

				$qryInformacao = $conn->query("SELECT quadro_funcional.*, quadro_funcional_categoria.descricao categoria, quadro_funcional_subcategoria.descricao subcategoria

												 FROM quadro_funcional

											LEFT JOIN quadro_funcional_categoria ON quadro_funcional.id_categoria = quadro_funcional_categoria.id

											LEFT JOIN quadro_funcional_subcategoria ON quadro_funcional.id_subcategoria = quadro_funcional_subcategoria.id

												WHERE quadro_funcional_categoria.id_cliente = '$cliente'

												  AND quadro_funcional.status_registro = 'A'

												  AND (quadro_funcional.titulo LIKE '%$busca%' OR quadro_funcional_categoria.descricao LIKE '%$busca%' OR quadro_funcional_subcategoria.descricao LIKE '%$busca%')

											 ORDER BY quadro_funcional.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryInformacao);
				if(count($qryInformacao) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $quadroFuncional . "$complemento'>Quadro Funcional</a><span class='badge'>$contador</span></h3>";



					foreach($qryInformacao as $quadro_funcional) {



						if($quadro_funcional['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$quadro_funcional[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$quadro_funcional[categoria]&nbsp;&raquo;&nbsp;$quadro_funcional[subcategoria]&nbsp;&raquo;&nbsp;$quadro_funcional[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$quadro_funcional[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$quadro_funcional[categoria]&nbsp;&raquo;&nbsp;$quadro_funcional[subcategoria]&nbsp;&raquo;&nbsp;$quadro_funcional[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}

				break;







			case "87":

				$qryInformacao = $conn->query("SELECT res_combustivel.*, res_combustivel_categoria.descricao categoria, res_combustivel_subcategoria.descricao subcategoria

												 FROM res_combustivel

											LEFT JOIN res_combustivel_categoria ON res_combustivel.id_categoria = res_combustivel_categoria.id

											LEFT JOIN res_combustivel_subcategoria ON res_combustivel.id_subcategoria = res_combustivel_subcategoria.id

												WHERE res_combustivel_categoria.id_cliente = '$cliente'

												  AND res_combustivel.status_registro = 'A'

												  AND (res_combustivel.titulo LIKE '%$busca%' OR res_combustivel_categoria.descricao LIKE '%$busca%' OR res_combustivel_subcategoria.descricao LIKE '%$busca%')

											 ORDER BY res_combustivel.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryInformacao);
				if(count($qryInformacao) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $resCombustivel . "$complemento'>Ressarcimento de Combust&iacute;veis</a><span class='badge'>$contador</span></h3>";



					foreach($qryInformacao as $res_combustivel) {



						if($res_combustivel['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$res_combustivel[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$res_combustivel[categoria]&nbsp;&raquo;&nbsp;$res_combustivel[subcategoria]&nbsp;&raquo;&nbsp;$res_combustivel[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$res_combustivel[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$res_combustivel[categoria]&nbsp;&raquo;&nbsp;$res_combustivel[subcategoria]&nbsp;&raquo;&nbsp;$res_combustivel[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}

				break;







			case "88":

				$qryLicitacoesAbertas = $conn->query("SELECT licitacoes_abertas.*, licitacoes_abertas_categoria.descricao categoria, licitacoes_abertas_subcategoria.descricao subcategoria

													    FROM licitacoes_abertas

												   LEFT JOIN licitacoes_abertas_categoria ON licitacoes_abertas.id_categoria = licitacoes_abertas_categoria.id

												   LEFT JOIN licitacoes_abertas_subcategoria ON licitacoes_abertas.id_subcategoria = licitacoes_abertas_subcategoria.id

													   WHERE licitacoes_abertas_categoria.id_cliente = '$cliente'

													  	 AND licitacoes_abertas.status_registro = 'A'

													  	 AND (licitacoes_abertas.titulo LIKE '%$busca%' OR licitacoes_abertas_categoria.descricao LIKE '%$busca%' OR licitacoes_abertas_subcategoria.descricao LIKE '%$busca%')

												 	ORDER BY licitacoes_abertas.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryLicitacoesAbertas);
				if(count($qryLicitacoesAbertas) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $licitacoesAbertas . "$complemento'>Licita&ccedil;&otilde;es Abertas</a><span class='badge'>$contador</span></h3>";



					foreach($qryLicitacoesAbertas as $licitacoes_abertas) {



						if($licitacoes_abertas['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$licitacoes_abertas[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$licitacoes_abertas[categoria]&nbsp;&raquo;&nbsp;$licitacoes_abertas[subcategoria]&nbsp;&raquo;&nbsp;$licitacoes_abertas[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$licitacoes_abertas[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$licitacoes_abertas[categoria]&nbsp;&raquo;&nbsp;$licitacoes_abertas[subcategoria]&nbsp;&raquo;&nbsp;$licitacoes_abertas[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}

				break;



			case "89":

				$qryControleMeicamentos = $conn->query("SELECT controle_medicamento.*, controle_medicamento_categoria.descricao categoria, controle_medicamento_subcategoria.descricao subcategoria

														  FROM controle_medicamento

													 LEFT JOIN controle_medicamento_categoria ON controle_medicamento.id_categoria = controle_medicamento_categoria.id

													 LEFT JOIN controle_medicamento_subcategoria ON controle_medicamento.id_subcategoria = controle_medicamento_subcategoria.id

														 WHERE controle_medicamento_categoria.id_cliente = '$cliente'

														   AND controle_medicamento.status_registro = 'A'

														   AND (controle_medicamento.titulo LIKE '%$busca%' OR controle_medicamento_categoria.descricao LIKE '%$busca%' OR controle_medicamento_subcategoria.descricao LIKE '%$busca%')

													  ORDER BY controle_medicamento.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryControleMeicamentos);
				if(count($qryControleMeicamentos) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $controleMedicacao . "$complemento'>Controle de Distribui&ccedil;&atilde;o de Medicamentos</a><span class='badge'>$contador</span></h3>";



					foreach($qryControleMeicamentos as $controle_medicamentos) {



						if($controle_medicamentos['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$controle_medicamentos[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$controle_medicamentos[categoria]&nbsp;&raquo;&nbsp;$controle_medicamentos[subcategoria]&nbsp;&raquo;&nbsp;$controle_medicamentos[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$controle_medicamentos[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$controle_medicamentos[categoria]&nbsp;&raquo;&nbsp;$controle_medicamentos[subcategoria]&nbsp;&raquo;&nbsp;$controle_medicamentos[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}

				break;



			case "90":

				$qryLicitacoesAndamento = $conn->query("SELECT licitacao_andamento.*, licitacao_andamento_categoria.descricao categoria, licitacao_andamento_subcategoria.descricao subcategoria

														  FROM licitacao_andamento

													 LEFT JOIN licitacao_andamento_categoria ON licitacao_andamento.id_categoria = licitacao_andamento_categoria.id

													 LEFT JOIN licitacao_andamento_subcategoria ON licitacao_andamento.id_subcategoria = licitacao_andamento_subcategoria.id

														 WHERE licitacao_andamento_categoria.id_cliente = '$cliente'

														   AND licitacao_andamento.status_registro = 'A'

														   AND (licitacao_andamento.titulo LIKE '%$busca%' OR licitacao_andamento_categoria.descricao LIKE '%$busca%' OR licitacao_andamento_subcategoria.descricao LIKE '%$busca%')

													  ORDER BY licitacao_andamento.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryLicitacoesAndamento);
				if(count($qryLicitacoesAndamento) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $licitacaoAndamento . "$complemento'> licita&ccedil;&atilde;o em Andamento</a><span class='badge'>$contador</span></h3>";



					foreach($qryLicitacoesAndamento as $licitacao_andamento) {



						if($licitacao_andamento['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$licitacao_andamento[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$licitacao_andamento[categoria]&nbsp;&raquo;&nbsp;$licitacao_andamento[subcategoria]&nbsp;&raquo;&nbsp;$licitacao_andamento[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$licitacao_andamento[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$licitacao_andamento[categoria]&nbsp;&raquo;&nbsp;$licitacao_andamento[subcategoria]&nbsp;&raquo;&nbsp;$licitacao_andamento[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}

				break;



			case "91":

				$qryLicitacaoRealizada = $conn->query("SELECT licitacao_realizada.*, licitacao_realizada_categoria.descricao categoria, licitacao_realizada_subcategoria.descricao subcategoria

														  FROM licitacao_realizada

													 LEFT JOIN licitacao_realizada_categoria ON licitacao_realizada.id_categoria = licitacao_realizada_categoria.id

													 LEFT JOIN licitacao_realizada_subcategoria ON licitacao_realizada.id_subcategoria = licitacao_realizada_subcategoria.id

														 WHERE licitacao_realizada_categoria.id_cliente = '$cliente'

														   AND licitacao_realizada.status_registro = 'A'

														   AND (licitacao_realizada.titulo LIKE '%$busca%' OR licitacao_realizada_categoria.descricao LIKE '%$busca%' OR licitacao_realizada_subcategoria.descricao LIKE '%$busca%')

													  ORDER BY licitacao_realizada.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryLicitacaoRealizada);
				if(count($qryLicitacaoRealizada) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $licitacaoRealizada . "$complemento'> licita&ccedil;&atilde;o Realizada</a><span class='badge'>$contador</span></h3>";



					foreach($qryLicitacaoRealizada as $licitacao_realizada) {



						if($licitacao_realizada['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$licitacao_realizada[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$licitacao_realizada[categoria]&nbsp;&raquo;&nbsp;$licitacao_realizada[subcategoria]&nbsp;&raquo;&nbsp;$licitacao_realizada[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$licitacao_realizada[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$licitacao_realizada[categoria]&nbsp;&raquo;&nbsp;$licitacao_realizada[subcategoria]&nbsp;&raquo;&nbsp;$licitacao_realizada[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}

				break;



			case "92":

				$qryAvisoLicitacao = $conn->query("SELECT aviso_licitacao.*, aviso_licitacao_categoria.descricao categoria, aviso_licitacao_subcategoria.descricao subcategoria

												  	 FROM aviso_licitacao

											 	LEFT JOIN aviso_licitacao_categoria ON aviso_licitacao.id_categoria = aviso_licitacao_categoria.id

											 	LEFT JOIN aviso_licitacao_subcategoria ON aviso_licitacao.id_subcategoria = aviso_licitacao_subcategoria.id

												 	WHERE aviso_licitacao_categoria.id_cliente = '$cliente'

												   	  AND aviso_licitacao.status_registro = 'A'

												   	  AND (aviso_licitacao.titulo LIKE '%$busca%' OR aviso_licitacao_categoria.descricao LIKE '%$busca%' OR aviso_licitacao_subcategoria.descricao LIKE '%$busca%')

											  	 ORDER BY aviso_licitacao.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryAvisoLicitacao);
				if(count($qryAvisoLicitacao) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $avisoLicitacao . "$complemento'>Aviso de licita&ccedil;&atilde;o</a><span class='badge'>$contador</span></h3>";



					foreach($qryAvisoLicitacao as $aviso_licitacao) {



						if($aviso_licitacao['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$aviso_licitacao[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$aviso_licitacao[categoria]&nbsp;&raquo;&nbsp;$aviso_licitacao[subcategoria]&nbsp;&raquo;&nbsp;$aviso_licitacao[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$aviso_licitacao[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$aviso_licitacao[categoria]&nbsp;&raquo;&nbsp;$aviso_licitacao[subcategoria]&nbsp;&raquo;&nbsp;$aviso_licitacao[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}

				break;



			case "93":

				$qryDispensaLicitacao = $conn->query("SELECT dispensa_licitacao.*, dispensa_licitacao_categoria.descricao categoria, dispensa_licitacao_subcategoria.descricao subcategoria

													    FROM dispensa_licitacao

												   LEFT JOIN dispensa_licitacao_categoria ON dispensa_licitacao.id_categoria = dispensa_licitacao_categoria.id

												   LEFT JOIN dispensa_licitacao_subcategoria ON dispensa_licitacao.id_subcategoria = dispensa_licitacao_subcategoria.id

													   WHERE dispensa_licitacao_categoria.id_cliente = '$cliente'

													     AND dispensa_licitacao.status_registro = 'A'

													     AND (dispensa_licitacao.titulo LIKE '%$busca%' OR dispensa_licitacao_categoria.descricao LIKE '%$busca%' OR dispensa_licitacao_subcategoria.descricao LIKE '%$busca%')

												    ORDER BY dispensa_licitacao.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryDispensaLicitacao);
				if(count($qryDispensaLicitacao) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $dispensaLicitacao . "$complemento'>Dispensa de licita&ccedil;&atilde;o</a><span class='badge'>$contador</span></h3>";



					foreach($qryDispensaLicitacao as $dispensa_licitacao) {



						if($dispensa_licitacao['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$dispensa_licitacao[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$dispensa_licitacao[categoria]&nbsp;&raquo;&nbsp;$dispensa_licitacao[subcategoria]&nbsp;&raquo;&nbsp;$dispensa_licitacao[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$dispensa_licitacao[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$dispensa_licitacao[categoria]&nbsp;&raquo;&nbsp;$dispensa_licitacao[subcategoria]&nbsp;&raquo;&nbsp;$dispensa_licitacao[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}

				break;



			case "94":

				$qryInexigibilidadeLicitacao = $conn->query("SELECT inexigibilidade_licitacao.*, inexigibilidade_licitacao_categoria.descricao categoria, inexigibilidade_licitacao_subcategoria.descricao subcategoria

															   FROM inexigibilidade_licitacao

													   	  LEFT JOIN inexigibilidade_licitacao_categoria ON inexigibilidade_licitacao.id_categoria = inexigibilidade_licitacao_categoria.id

													   	  LEFT JOIN inexigibilidade_licitacao_subcategoria ON inexigibilidade_licitacao.id_subcategoria = inexigibilidade_licitacao_subcategoria.id

														   	  WHERE inexigibilidade_licitacao_categoria.id_cliente = '$cliente'

															 	AND inexigibilidade_licitacao.status_registro = 'A'

															 	AND (inexigibilidade_licitacao.titulo LIKE '%$busca%' OR inexigibilidade_licitacao_categoria.descricao LIKE '%$busca%' OR inexigibilidade_licitacao_subcategoria.descricao LIKE '%$busca%')

														   ORDER BY inexigibilidade_licitacao.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryInexigibilidadeLicitacao);
				if(count($qryInexigibilidadeLicitacao) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $inexigibilidadeLicitacao . "$complemento'>Inexigibilidade licita&ccedil;&atilde;o</a><span class='badge'>$contador</span></h3>";



					foreach($qryInexigibilidadeLicitacao as $inexigibilidade_licitacao) {



						if($inexigibilidade_licitacao['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$inexigibilidade_licitacao[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$inexigibilidade_licitacao[categoria]&nbsp;&raquo;&nbsp;$inexigibilidade_licitacao[subcategoria]&nbsp;&raquo;&nbsp;$inexigibilidade_licitacao[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$inexigibilidade_licitacao[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$inexigibilidade_licitacao[categoria]&nbsp;&raquo;&nbsp;$inexigibilidade_licitacao[subcategoria]&nbsp;&raquo;&nbsp;$inexigibilidade_licitacao[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}

				break;



			case "95":

				$qrySecretariaExecutiva = $conn->query("SELECT secretaria_executiva.*, secretaria_executiva_categoria.descricao categoria, secretaria_executiva_subcategoria.descricao subcategoria

													    FROM secretaria_executiva

												   LEFT JOIN secretaria_executiva_categoria ON secretaria_executiva.id_categoria = secretaria_executiva_categoria.id

												   LEFT JOIN secretaria_executiva_subcategoria ON secretaria_executiva.id_subcategoria = secretaria_executiva_subcategoria.id

													   WHERE secretaria_executiva_categoria.id_cliente = '$cliente'

													     AND secretaria_executiva.status_registro = 'A'

													     AND (secretaria_executiva.titulo LIKE '%$busca%' OR secretaria_executiva_categoria.descricao LIKE '%$busca%' OR secretaria_executiva_subcategoria.descricao LIKE '%$busca%')

												    ORDER BY secretaria_executiva.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qrySecretariaExecutiva);
				if(count($qrySecretariaExecutiva) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $secretariaExecutiva . "$complemento'>Secretaria Executiva dos Conselhos Municipais</a><span class='badge'>$contador</span></h3>";



					foreach($qrySecretariaExecutiva as $secretaria_executiva) {



						if($secretaria_executiva['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$secretaria_executiva[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$secretaria_executiva[categoria]&nbsp;&raquo;&nbsp;$secretaria_executiva[subcategoria]&nbsp;&raquo;&nbsp;$secretaria_executiva[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$secretaria_executiva[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$secretaria_executiva[categoria]&nbsp;&raquo;&nbsp;$secretaria_executiva[subcategoria]&nbsp;&raquo;&nbsp;$secretaria_executiva[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}

				break;



			case "96":

				$qryContracheque = $conn->query("SELECT contracheque.*, contracheque_categoria.descricao categoria, contracheque_subcategoria.descricao subcategoria

												  FROM contracheque

											 LEFT JOIN contracheque_categoria ON contracheque.id_categoria = contracheque_categoria.id

											 LEFT JOIN contracheque_subcategoria ON contracheque.id_subcategoria = contracheque_subcategoria.id

												 WHERE contracheque_categoria.id_cliente = '$cliente'

												   AND contracheque.status_registro = 'A'

												   AND (contracheque.titulo LIKE '%$busca%' OR contracheque_categoria.descricao LIKE '%$busca%' OR contracheque_subcategoria.descricao LIKE '%$busca%')

											  ORDER BY contracheque.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryContracheque);
				if(count($qryContracheque) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $contracheque . "$complemento'>Contracheque</a><span class='badge'>$contador</span></h3>";



					foreach($qryContracheque as $contracheque) {



						if($contracheque['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$contracheque[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$contracheque[categoria]&nbsp;&raquo;&nbsp;$contracheque[subcategoria]&nbsp;&raquo;&nbsp;$contracheque[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$contracheque[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$contracheque[categoria]&nbsp;&raquo;&nbsp;$contracheque[subcategoria]&nbsp;&raquo;&nbsp;$contracheque[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}

				break;



			case "97":

          $qryProjetosLeis = $conn->query("SELECT projetos_leis.*, projetos_leis_categoria.descricao categoria, projetos_leis_subcategoria.descricao subcategoria

													FROM projetos_leis

											   LEFT JOIN projetos_leis_categoria ON projetos_leis.id_categoria = projetos_leis_categoria.id

											   LEFT JOIN projetos_leis_subcategoria ON projetos_leis.id_subcategoria = projetos_leis_subcategoria.id

												   WHERE projetos_leis_categoria.id_cliente = '$cliente'

													 AND projetos_leis.status_registro = 'A'

													 AND (projetos_leis.titulo LIKE '%$busca%' OR projetos_leis_categoria.descricao LIKE '%$busca%' OR projetos_leis_subcategoria.descricao LIKE '%$busca%')

												ORDER BY projetos_leis.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryProjetosLeis);
          if(count($qryProjetosLeis) > 0) {
			  $contaResultados++;



            echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $projetosLeis . "$complemento'>Projetos de Lei</a><span class='badge'>$contador</span></h3>";



            foreach($qryProjetosLeis as $projetos_leis) {



              if($projetos_leis['arquivo'] != "")

                echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$projetos_leis[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$projetos_leis[categoria]&nbsp;&raquo;&nbsp;$projetos_leis[subcategoria]&nbsp;&raquo;&nbsp;$projetos_leis[titulo]</strong></a></p>";

              else

                echo "<p style='margin: 3px 0px;'><a href='$projetos_leis[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$projetos_leis[categoria]&nbsp;&raquo;&nbsp;$projetos_leis[subcategoria]&nbsp;&raquo;&nbsp;$projetos_leis[titulo]</strong></a></p>";



            }



            echo "<p>&nbsp;</p>";



          }

          break;



          case "98":

            $qryProjetosLeis = $conn->query("SELECT servidor_salario.*, servidor_salario_categoria.descricao categoria, servidor_salario_subcategoria.descricao subcategoria

													FROM servidor_salario

											   LEFT JOIN servidor_salario_categoria ON servidor_salario.id_categoria = servidor_salario_categoria.id

											   LEFT JOIN servidor_salario_subcategoria ON servidor_salario.id_subcategoria = servidor_salario_subcategoria.id

												   WHERE servidor_salario_categoria.id_cliente = '$cliente'

													 AND servidor_salario.status_registro = 'A'

													 AND (servidor_salario.titulo LIKE '%$busca%' OR servidor_salario_categoria.descricao LIKE '%$busca%' OR servidor_salario_subcategoria.descricao LIKE '%$busca%')

												ORDER BY servidor_salario.titulo")->fetchAll(PDO::FETCH_ASSOC);



			$contador = count($qryProjetosLeis);
            if(count($qryProjetosLeis) > 0) {
				$contaResultados++;



              echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $servidorSalario . "$complemento'>Sal&aacute;rio dos Servidores</a><span class='badge'>$contador</span></h3>";



              foreach($qryProjetosLeis as $servidor_salario) {



                if($servidor_salario['arquivo'] != "")

                  echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$servidor_salario[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$servidor_salario[categoria]&nbsp;&raquo;&nbsp;$servidor_salario[subcategoria]&nbsp;&raquo;&nbsp;$servidor_salario[titulo]</strong></a></p>";

                else

                  echo "<p style='margin: 3px 0px;'><a href='$servidor_salario[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$servidor_salario[categoria]&nbsp;&raquo;&nbsp;$servidor_salario[subcategoria]&nbsp;&raquo;&nbsp;$servidor_salario[titulo]</strong></a></p>";



              }



              echo "<p>&nbsp;</p>";



            }

            break;



			case "99":

				$qryControleEstoqueSaude = $conn->query("SELECT estoque_saude.*, estoque_saude_categoria.descricao categoria, estoque_saude_subcategoria.descricao subcategoria

												      FROM estoque_saude

												 LEFT JOIN estoque_saude_categoria ON estoque_saude.id_categoria = estoque_saude_categoria.id

												 LEFT JOIN estoque_saude_subcategoria ON estoque_saude.id_subcategoria = estoque_saude_subcategoria.id

												     WHERE estoque_saude_categoria.id_cliente = '$cliente'

													   AND estoque_saude.status_registro = 'A'

													   AND (estoque_saude.titulo LIKE '%$busca%' OR estoque_saude_categoria.descricao LIKE '%$busca%' OR estoque_saude_subcategoria.descricao LIKE '%$busca%')

											      ORDER BY estoque_saude.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryControleEstoqueSaude);
				if(count($qryControleEstoqueSaude) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $controleEstoqueSaude . "$complemento'>Controle de Estoque Sa&uacute;de</a><span class='badge'>$contador</span></h3>";



					foreach($qryControleEstoqueSaude as $estoque_saude) {



						if($estoque_saude['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$estoque_saude[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$estoque_saude[categoria]&nbsp;&raquo;&nbsp;$estoque_saude[subcategoria]&nbsp;&raquo;&nbsp;$estoque_saude[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$estoque_saude[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$estoque_saude[categoria]&nbsp;&raquo;&nbsp;$estoque_saude[subcategoria]&nbsp;&raquo;&nbsp;$estoque_saude[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}

			break;



			case "100":

				$qryInterferenciaFinanceira = $conn->query("SELECT interferencia_financeira.*, interferencia_financeira_categoria.descricao categoria, interferencia_financeira_subcategoria.descricao subcategoria

												    FROM interferencia_financeira

											   LEFT JOIN interferencia_financeira_categoria ON interferencia_financeira.id_categoria = interferencia_financeira_categoria.id

											   LEFT JOIN interferencia_financeira_subcategoria ON interferencia_financeira.id_subcategoria = interferencia_financeira_subcategoria.id

												   WHERE interferencia_financeira_categoria.id_cliente = '$cliente'

												     AND interferencia_financeira.status_registro = 'A'

												     AND (interferencia_financeira.titulo LIKE '%$busca%' OR interferencia_financeira_categoria.descricao LIKE '%$busca%' OR interferencia_financeira_subcategoria.descricao LIKE '%$busca%')

											    ORDER BY interferencia_financeira.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryInterferenciaFinanceira);
				if(count($qryInterferenciaFinanceira) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $interferenciaFinanceira . "$complemento'>Interfer&ecirc;ncia Financeira</a><span class='badge'>$contador</span></h3>";



					foreach($qryInterferenciaFinanceira as $interferencia_financeira) {



						if($interferencia_financeira['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$interferencia_financeira[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$interferencia_financeira[categoria]&nbsp;&raquo;&nbsp;$interferencia_financeira[subcategoria]&nbsp;&raquo;&nbsp;$interferencia_financeira[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$interferencia_financeira[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$interferencia_financeira[categoria]&nbsp;&raquo;&nbsp;$interferencia_financeira[subcategoria]&nbsp;&raquo;&nbsp;$interferencia_financeira[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}



			break;



			case "101":

            $qryServFerLic = $conn->query("SELECT servidor_ferias_licenca.*, servidor_ferias_licenca_categoria.descricao categoria, servidor_ferias_licenca_subcategoria.descricao subcategoria

													FROM servidor_ferias_licenca

											   LEFT JOIN servidor_ferias_licenca_categoria ON servidor_ferias_licenca.id_categoria = servidor_ferias_licenca_categoria.id

											   LEFT JOIN servidor_ferias_licenca_subcategoria ON servidor_ferias_licenca.id_subcategoria = servidor_ferias_licenca_subcategoria.id

												   WHERE servidor_ferias_licenca_categoria.id_cliente = '$cliente'

													 AND servidor_ferias_licenca.status_registro = 'A'

													 AND (servidor_ferias_licenca.titulo LIKE '%$busca%' OR servidor_ferias_licenca_categoria.descricao LIKE '%$busca%' OR servidor_ferias_licenca_subcategoria.descricao LIKE '%$busca%')

												ORDER BY servidor_ferias_licenca.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryServFerLic);
            if(count($qryServFerLic) > 0) {
				$contaResultados++;



              echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $servidorFeriasLicenca . "$complemento'>Servidores em F&eacute;rias ou de Licen&ccedil;a</a><span class='badge'>$contador</span></h3>";



              foreach($qryServFerLic as $serv_fer_lic) {



                if($serv_fer_lic['arquivo'] != "")

                  echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$serv_fer_lic[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$serv_fer_lic[categoria]&nbsp;&raquo;&nbsp;$serv_fer_lic[subcategoria]&nbsp;&raquo;&nbsp;$serv_fer_lic[titulo]</strong></a></p>";

                else

                  echo "<p style='margin: 3px 0px;'><a href='$serv_fer_lic[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$serv_fer_lic[categoria]&nbsp;&raquo;&nbsp;$serv_fer_lic[subcategoria]&nbsp;&raquo;&nbsp;$serv_fer_lic[titulo]</strong></a></p>";



              }



              echo "<p>&nbsp;</p>";



            }

			break;

			

			case "102":

			$qryResiduo = $conn->query("SELECT pref_plano_residuo.*, pref_plano_residuo_categoria.categoria

										FROM pref_plano_residuo, pref_plano_residuo_categoria

									   WHERE pref_plano_residuo.id_categoria = pref_plano_residuo_categoria.id

										 AND pref_plano_residuo_categoria.id_cliente = '$cliente'

										 AND pref_plano_residuo.status_registro = 'A'

										 AND (pref_plano_residuo.titulo LIKE '%$busca%' OR pref_plano_residuo_categoria.categoria LIKE '%$busca%')

									ORDER BY pref_plano_residuo.titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryResiduo);
			if(count($qryResiduo) > 0) {
				$contaResultados++;



				echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $planoResiduo . "$complemento'>Plano de Gerenciamento de Res&iacute;duos</a><span class='badge'>$contador</span></h3>";



				foreach($qryResiduo as $plano) {



					echo "<p style='margin: 3px 0px;'><a href='$CAMINHO/index.php?sessao=$sequencia" . $planoResiduoVisualiza . "$complemento&id=$plano[id]'><i class='glyphicon glyphicon-list-alt'></i><strong>&nbsp;$plano[categoria]&nbsp;&raquo;&nbsp;$plano[titulo]</strong></a></p>";



				}



				echo "<p>&nbsp;</p>";



			}



			break;





            case "103":

                $qryLaudos = $conn->query("SELECT laudos_tecnicos.*, laudos_tecnicos_categoria.descricao

                                            FROM laudos_tecnicos, laudos_tecnicos_categoria

                                           WHERE laudos_tecnicos.id_categoria = laudos_tecnicos_categoria.id

                                             AND laudos_tecnicos_categoria.id_cliente = '$cliente'

                                             AND laudos_tecnicos.status_registro = 'A'

                                             AND (laudos_tecnicos.titulo LIKE '%$busca%' OR laudos_tecnicos_categoria.descricao LIKE '%$busca%')

                                        ORDER BY laudos_tecnicos.titulo")->fetchAll(PDO::FETCH_ASSOC);

    

				$contador = count($qryLaudos);
                if(count($qryLaudos) > 0) {
					$contaResultados++;

    



                    echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $laudosTecnicos . "$complemento'>Laudos T&ecute;cnicos</a><span class='badge'>$contador</span></h3>";



                    foreach ($qryLaudos as $laudos) {



                        if ($laudos['arquivo'] != "")

                            echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$laudos[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$laudos[categoria]&nbsp;&raquo;&nbsp;$laudos[subcategoria]&nbsp;&raquo;&nbsp;$laudos[titulo]</strong></a></p>";

                        else

                            echo "<p style='margin: 3px 0px;'><a href='$laudos[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$laudos[categoria]&nbsp;&raquo;&nbsp;$laudos[subcategoria]&nbsp;&raquo;&nbsp;$laudos[titulo]</strong></a></p>";



                    }



                    echo "<p>&nbsp;</p>";



                }



                break;
			
				case "104":
				
								$qryAssistencia = $conn->query("SELECT assistencia_social.*, assistencia_social_categoria.descricao
				
															FROM assistencia_social, assistencia_social_categoria
				
														   WHERE assistencia_social.id_categoria = assistencia_social_categoria.id
				
															 AND assistencia_social_categoria.id_cliente = '$cliente'
				
															 AND assistencia_social.status_registro = 'A'
				
															 AND (assistencia_social.titulo LIKE '%$busca%' OR assistencia_social_categoria.descricao LIKE '%$busca%')
				
														ORDER BY assistencia_social.titulo")->fetchAll(PDO::FETCH_ASSOC);
				
					
				
					$contador = count($qryAssistencia);
								if(count($qryAssistencia) > 0) {
									$contaResultados++;
				
					
				
				
				
									echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $assistenciaSocial . "$complemento'>Assist&ecirc;ncia Social</a><span class='badge'>$contador</span></h3>";
				
				
				
									foreach ($qryAssistencia as $assistencia) {
				
				
				
										if ($assistencia['arquivo'] != "")
				
											echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$assistencia[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$assistencia[categoria]&nbsp;&raquo;&nbsp;$assistencia[subcategoria]&nbsp;&raquo;&nbsp;$assistencia[titulo]</strong></a></p>";
				
										else
				
											echo "<p style='margin: 3px 0px;'><a href='$assistencia[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$assistencia[categoria]&nbsp;&raquo;&nbsp;$assistencia[subcategoria]&nbsp;&raquo;&nbsp;$assistencia[titulo]</strong></a></p>";
				
				
				
									}
				
				
				
									echo "<p>&nbsp;</p>";
				
				
				
								}
				
				
				
								break;
								
				case "105":

				$qryInvestimento = $conn->query("SELECT previdencia_investimento.*, previdencia_investimento_categoria.descricao

											FROM previdencia_investimento, previdencia_investimento_categoria

											WHERE previdencia_investimento.id_categoria = previdencia_investimento_categoria.id

												AND previdencia_investimento_categoria.id_cliente = '$cliente'
												
												AND previdencia_investimento.status_registro = 'A'
												
												AND (previdencia_investimento.titulo LIKE '%$busca%' OR previdencia_investimento_categoria.descricao LIKE '%$busca%')
										
										ORDER BY previdencia_investimento.titulo")->fetchAll(PDO::FETCH_ASSOC);
	
					$contador = count($qryInvestimento);
				if(count($qryInvestimento) > 0) {
					$contaResultados++;
	

					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $investimento . "$complemento'>Investimentos</a><span class='badge'>$contador</span></h3>";

					foreach ($qryInvestimento as $invest) {

						if ($invest['arquivo'] != "")
							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$invest[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$invest[categoria]&nbsp;&raquo;&nbsp;$invest[subcategoria]&nbsp;&raquo;&nbsp;$invest[titulo]</strong></a></p>";
						else
							echo "<p style='margin: 3px 0px;'><a href='$invest[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$invest[categoria]&nbsp;&raquo;&nbsp;$invest[subcategoria]&nbsp;&raquo;&nbsp;$invest[titulo]</strong></a></p>";

					}

					echo "<p>&nbsp;</p>";

				}

				break;

				case "106":

                $qryCalculoAtuarial = $conn->query("SELECT calculo_atuarial.*, calculo_atuarial_categoria.descricao

                                            FROM calculo_atuarial, calculo_atuarial_categoria

                                           WHERE calculo_atuarial.id_categoria = calculo_atuarial_categoria.id

                                             AND calculo_atuarial_categoria.id_cliente = '$cliente'

                                             AND calculo_atuarial.status_registro = 'A'

                                             AND (calculo_atuarial.titulo LIKE '%$busca%' OR calculo_atuarial_categoria.descricao LIKE '%$busca%')
											 
                                        ORDER BY calculo_atuarial.titulo")->fetchAll(PDO::FETCH_ASSOC);
	
					$contador = count($qryCalculoAtuarial);
                if(count($qryCalculoAtuarial) > 0) {
					$contaResultados++;
    

                    echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $calculoAtuarial . "$complemento'>C&aacute;lculo Atuarial</a><span class='badge'>$contador</span></h3>";

                    foreach ($qryCalculoAtuarial as $calculo) {

                        if ($calculo['arquivo'] != "")
                            echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$calculo[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$calculo[categoria]&nbsp;&raquo;&nbsp;$calculo[subcategoria]&nbsp;&raquo;&nbsp;$calculo[titulo]</strong></a></p>";
                        else
                            echo "<p style='margin: 3px 0px;'><a href='$calculo[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$calculo[categoria]&nbsp;&raquo;&nbsp;$calculo[subcategoria]&nbsp;&raquo;&nbsp;$calculo[titulo]</strong></a></p>";

                    }

                    echo "<p>&nbsp;</p>";

                }

                break;

                case "107":

                $qryMinisterioPF = $conn->query("SELECT ministerio_pf.*, ministerio_pf_categoria.descricao

                                            FROM ministerio_pf, ministerio_pf_categoria

                                           WHERE ministerio_pf.id_categoria = ministerio_pf_categoria.id

                                             AND ministerio_pf_categoria.id_cliente = '$cliente'

                                             AND ministerio_pf.status_registro = 'A'

                                             AND (ministerio_pf.titulo LIKE '%$busca%' OR ministerio_pf_categoria.descricao LIKE '%$busca%')
											 
                                        ORDER BY ministerio_pf.titulo")->fetchAll(PDO::FETCH_ASSOC);
	
					$contador = count($qryMinisterioPF);
                if(count($qryMinisterioPF) > 0) {
					$contaResultados++;
    

                    echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $ministerioPF . "$complemento'>Minist&eacute;rio P&uacute;blico Federal</a><span class='badge'>$contador</span></h3>";

                    foreach ($qryMinisterioPF as $calculo) {

                        if ($calculo['arquivo'] != "")
                            echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$calculo[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$calculo[categoria]&nbsp;&raquo;&nbsp;$calculo[subcategoria]&nbsp;&raquo;&nbsp;$calculo[titulo]</strong></a></p>";
                        else
                            echo "<p style='margin: 3px 0px;'><a href='$calculo[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$calculo[categoria]&nbsp;&raquo;&nbsp;$calculo[subcategoria]&nbsp;&raquo;&nbsp;$calculo[titulo]</strong></a></p>";

                    }

                    echo "<p>&nbsp;</p>";

                }

                break;

                case "108":

				$qryListaCreche = $conn->query("SELECT lista_creche.*, lista_creche_categoria.descricao categoria, lista_creche_subcategoria.descricao subcategoria

												   FROM lista_creche

											  LEFT JOIN lista_creche_categoria ON lista_creche.id_categoria = lista_creche_categoria.id

											  LEFT JOIN lista_creche_subcategoria ON lista_creche.id_subcategoria = lista_creche_subcategoria.id

												  WHERE lista_creche_categoria.id_cliente = '$cliente'

												    AND lista_creche.status_registro = 'A'

												    AND (lista_creche.titulo LIKE '%$busca%' OR lista_creche_categoria.descricao LIKE '%$busca%' OR lista_creche_subcategoria.descricao LIKE '%$busca%')

											   ORDER BY lista_creche.titulo")->fetchAll(PDO::FETCH_ASSOC);



					$contador = count($qryListaCreche);
				if(count($qryListaCreche) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $listaCreche . "$complemento'>Lista de creche</a><span class='badge'>$contador</span></h3>";



					foreach($qryListaCreche as $lista_creche) {



						if($lista_creche['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$lista_creche[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$lista_creche[categoria]&nbsp;&raquo;&nbsp;$lista_creche[subcategoria]&nbsp;&raquo;&nbsp;$lista_creche[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$lista_creche[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$lista_creche[categoria]&nbsp;&raquo;&nbsp;$lista_creche[subcategoria]&nbsp;&raquo;&nbsp;$lista_creche[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}



				break;


				case "109":
				
								$qryChamamento = $conn->query("SELECT chamamento_publico.*, chamamento_publico_categoria.descricao, chamamento_publico.descricao as titulo
				
															FROM chamamento_publico, chamamento_publico_categoria
				
														   WHERE chamamento_publico.id_categoria = chamamento_publico_categoria.id
				
															 AND chamamento_publico_categoria.id_cliente = '$cliente'
				
															 AND chamamento_publico.status_registro = 'A'
				
															 AND (chamamento_publico.descricao LIKE '%$busca%' OR chamamento_publico_categoria.descricao LIKE '%$busca%')
				
														ORDER BY chamamento_publico.descricao")->fetchAll(PDO::FETCH_ASSOC);
				
					
				
					$contador = count($qryChamamento);
								if(count($qryChamamento) > 0) {
									$contaResultados++;
				
					
				
				
				
									echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $chamamentoPublico . "$complemento'>Chamamento Publico</a><span class='badge'>$contador</span></h3>";
				
				
				
									foreach ($qryChamamento as $chamamento) {
				
				
				
										if ($chamamento['arquivo'] != "")
				
											echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$chamamento[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$chamamento[categoria]&nbsp;&raquo;&nbsp;$chamamento[subcategoria]&nbsp;&raquo;&nbsp;$chamamento[titulo]</strong></a></p>";
				
										else
				
											echo "<p style='margin: 3px 0px;'><a href='$chamamento[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$chamamento[categoria]&nbsp;&raquo;&nbsp;$chamamento[subcategoria]&nbsp;&raquo;&nbsp;$chamamento[titulo]</strong></a></p>";
				
				
				
									}
				
				
				
									echo "<p>&nbsp;</p>";
				
				
				
								}
				
				
				break;


				case "110":
				
								$qryRelatorioViagem = $conn->query("SELECT relatorio_viagem_saude.*, relatorio_viagem_saude.descricao as titulo, relatorio_viagem_saude_categoria.descricao
				
															FROM relatorio_viagem_saude, relatorio_viagem_saude_categoria
				
														   WHERE relatorio_viagem_saude.id_categoria = relatorio_viagem_saude_categoria.id
				
															 AND relatorio_viagem_saude_categoria.id_cliente = '$cliente'
				
															 AND relatorio_viagem_saude.status_registro = 'A'
				
															 AND (relatorio_viagem_saude.descricao LIKE '%$busca%' OR relatorio_viagem_saude_categoria.descricao LIKE '%$busca%')
				
														ORDER BY relatorio_viagem_saude.descricao")->fetchAll(PDO::FETCH_ASSOC);
				
					
				
					$contador = count($qryRelatorioViagem);
								if(count($qryRelatorioViagem) > 0) {
									$contaResultados++;
				
					
				
				
				
									echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $relatorioViagem . "$complemento'>Relatorio Viagem Saude</a><span class='badge'>$contador</span></h3>";
				
				
				
									foreach ($qryRelatorioViagem as $relatorioViagemSaude) {
				
				
				
										if ($relatorioViagemSaude['arquivo'] != "")
				
											echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$relatorioViagemSaude[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$relatorioViagemSaude[categoria]&nbsp;&raquo;&nbsp;$relatorioViagemSaude[subcategoria]&nbsp;&raquo;&nbsp;$relatorioViagemSaude[titulo]</strong></a></p>";
				
										else
				
											echo "<p style='margin: 3px 0px;'><a href='$relatorioViagemSaude[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$relatorioViagemSaude[categoria]&nbsp;&raquo;&nbsp;$relatorioViagemSaude[subcategoria]&nbsp;&raquo;&nbsp;$relatorioViagemSaude[titulo]</strong></a></p>";
				
				
				
									}
				
				
				
									echo "<p>&nbsp;</p>";
				
				
				
								}
				
				
				
				break;

				case "111":
				
								$qryQuantCargo = $conn->query("SELECT quantitativo_cargos.*, quantitativo_cargos.descricao as titulo, quantitativo_cargos_categoria.descricao
				
															FROM quantitativo_cargos, quantitativo_cargos_categoria
				
														   WHERE quantitativo_cargos.id_categoria = quantitativo_cargos_categoria.id
				
															 AND quantitativo_cargos_categoria.id_cliente = '$cliente'
				
															 AND quantitativo_cargos.status_registro = 'A'
				
															 AND (quantitativo_cargos.descricao LIKE '%$busca%' OR quantitativo_cargos_categoria.descricao LIKE '%$busca%')
				
														ORDER BY quantitativo_cargos.descricao")->fetchAll(PDO::FETCH_ASSOC);
				
					
				
					$contador = count($qryQuantCargo);
								if(count($qryQuantCargo) > 0) {
									$contaResultados++;
				
					
				
				
				
									echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $QuantCargo . "$complemento'>Quantitativo de Cargos</a><span class='badge'>$contador</span></h3>";
				
				
				
									foreach ($qryQuantCargo as $valorQuantCargo) {
				
				
				
										if ($valorQuantCargo['arquivo'] != "")
				
											echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$valorQuantCargo[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$valorQuantCargo[categoria]&nbsp;&raquo;&nbsp;$valorQuantCargo[subcategoria]&nbsp;&raquo;&nbsp;$valorQuantCargo[titulo]</strong></a></p>";
				
										else
				
											echo "<p style='margin: 3px 0px;'><a href='$valorQuantCargo[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$valorQuantCargo[categoria]&nbsp;&raquo;&nbsp;$valorQuantCargo[subcategoria]&nbsp;&raquo;&nbsp;$valorQuantCargo[titulo]</strong></a></p>";
				
				
				
									}
				
				
				
									echo "<p>&nbsp;</p>";
				
				
				
								}
				
				
				
				break;

				case "112":
				
								$qryServidoresAtivos = $conn->query("SELECT servidores_ativos.*, servidores_ativos_categoria.descricao
				
															FROM servidores_ativos, servidores_ativos_categoria
				
														   WHERE servidores_ativos.id_categoria = servidores_ativos_categoria.id
				
															 AND servidores_ativos_categoria.id_cliente = '$cliente'
				
															 AND servidores_ativos.status_registro = 'A'
				
															 AND (servidores_ativos.titulo LIKE '%$busca%' OR servidores_ativos_categoria.descricao LIKE '%$busca%')
				
														ORDER BY servidores_ativos.titulo")->fetchAll(PDO::FETCH_ASSOC);
				
					
				
					$contador = count($qryServidoresAtivos);
								if(count($qryServidoresAtivos) > 0) {
									$contaResultados++;
				
					
				
				
				
									echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $ServidoresAtivos . "$complemento'>Servidores Ativos</a><span class='badge'>$contador</span></h3>";
				
				
				
									foreach ($qryServidoresAtivos as $valorServidorAtivo) {
				
				
				
										if ($valorServidorAtivo['arquivo'] != "")
				
											echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$valorServidorAtivo[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$valorServidorAtivo[categoria]&nbsp;&raquo;&nbsp;$valorServidorAtivo[subcategoria]&nbsp;&raquo;&nbsp;$valorServidorAtivo[titulo]</strong></a></p>";
				
										else
				
											echo "<p style='margin: 3px 0px;'><a href='$valorServidorAtivo[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$valorServidorAtivo[categoria]&nbsp;&raquo;&nbsp;$valorServidorAtivo[subcategoria]&nbsp;&raquo;&nbsp;$valorServidorAtivo[titulo]</strong></a></p>";
				
				
				
									}
				
				
				
									echo "<p>&nbsp;</p>";
				
				
				
								}
				
				
				
				break;

				case "113":
				
								$qryServidoresInativos = $conn->query("SELECT servidores_inativos.*, servidores_inativos.descricao as titulo, servidores_inativos_categoria.descricao
				
															FROM servidores_inativos, servidores_inativos_categoria
				
														   WHERE servidores_inativos.id_categoria = servidores_inativos_categoria.id
				
															 AND servidores_inativos_categoria.id_cliente = '$cliente'
				
															 AND servidores_inativos.status_registro = 'A'
				
															 AND (servidores_inativos.descricao LIKE '%$busca%' OR servidores_inativos_categoria.descricao LIKE '%$busca%')
				
														ORDER BY servidores_inativos.descricao")->fetchAll(PDO::FETCH_ASSOC);
				
					
				
					$contador = count($qryServidoresInativos);
								if(count($qryServidoresInativos) > 0) {
									$contaResultados++;
				
					
				
				
				
									echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $relatorioViagem . "$complemento'>Servidores Inativos</a><span class='badge'>$contador</span></h3>";
				
				
				
									foreach ($qryServidoresInativos as $valorServidorInativo) {
				
				
				
										if ($valorServidorInativo['arquivo'] != "")
				
											echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$valorServidorInativo[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$valorServidorInativo[categoria]&nbsp;&raquo;&nbsp;$valorServidorInativo[subcategoria]&nbsp;&raquo;&nbsp;$valorServidorInativo[titulo]</strong></a></p>";
				
										else
				
											echo "<p style='margin: 3px 0px;'><a href='$valorServidorInativo[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$valorServidorInativo[categoria]&nbsp;&raquo;&nbsp;$valorServidorInativo[subcategoria]&nbsp;&raquo;&nbsp;$valorServidorInativo[titulo]</strong></a></p>";
				
				
				
									}
				
				
				
									echo "<p>&nbsp;</p>";
				
				
				
								}
				
				
				
				break;

				case "114":
				
								$qryAcoesProgramas = $conn->query("SELECT acoes_programas.*, acoes_programas_categoria.descricao
				
															FROM acoes_programas, acoes_programas_categoria
				
														   WHERE acoes_programas.id_categoria = acoes_programas_categoria.id
				
															 AND acoes_programas_categoria.id_cliente = '$cliente'
				
															 AND acoes_programas.status_registro = 'A'
				
															 AND (acoes_programas.titulo LIKE '%$busca%' OR acoes_programas_categoria.descricao LIKE '%$busca%')
				
														ORDER BY acoes_programas.titulo")->fetchAll(PDO::FETCH_ASSOC);
				
					
				
					$contador = count($qryAcoesProgramas);
								if(count($qryAcoesProgramas) > 0) {
									$contaResultados++;
				
					
				
				
				
									echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $relatorioViagem . "$complemento'>Relacoes e programas</a><span class='badge'>$contador</span></h3>";
				
				
				
									foreach ($qryAcoesProgramas as $valorAcoesProgramas) {
				
				
				
										if ($valorAcoesProgramas['arquivo'] != "")
				
											echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$valorAcoesProgramas[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$valorAcoesProgramas[categoria]&nbsp;&raquo;&nbsp;$valorAcoesProgramas[subcategoria]&nbsp;&raquo;&nbsp;$valorAcoesProgramas[titulo]</strong></a></p>";
				
										else
				
											echo "<p style='margin: 3px 0px;'><a href='$valorAcoesProgramas[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$valorAcoesProgramas[categoria]&nbsp;&raquo;&nbsp;$valorAcoesProgramas[subcategoria]&nbsp;&raquo;&nbsp;$valorAcoesProgramas[titulo]</strong></a></p>";
				
				
				
									}
				
				
				
									echo "<p>&nbsp;</p>";
				
				
				
								}
				
				
				
				break;

				case "115":
				
								$qryServidoresInativos = $conn->query("SELECT servidores_comissionados.*, servidores_comissionados_categoria.descricao, servidores_comissionados.descricao as titulo
				
															FROM servidores_comissionados, servidores_comissionados_categoria
				
														   WHERE servidores_comissionados.id_categoria = servidores_comissionados_categoria.id
				
															 AND servidores_comissionados_categoria.id_cliente = '$cliente'
				
															 AND servidores_comissionados_categoria.status_registro = 'A'
				
															 AND (servidores_comissionados.descricao LIKE '%$busca%' OR servidores_comissionados_categoria.descricao LIKE '%$busca%')
				
														ORDER BY servidores_comissionados.descricao")->fetchAll(PDO::FETCH_ASSOC);
				
					
				
					$contador = count($qryServidoresComissionados);
								if(count($qryServidoresComissionados) > 0) {
									$contaResultados++;
				
					
				
				
				
									echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $relatorioViagem . "$complemento'>Servidores Comissionados</a><span class='badge'>$contador</span></h3>";
				
				
				
									foreach ($qryServidoresComissionados as $valorServidorComissionado) {
				
				
				
										if ($valorServidorComissionado['arquivo'] != "")
				
											echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$valorServidorComissionado[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$valorServidorComissionado[categoria]&nbsp;&raquo;&nbsp;$valorServidorComissionado[subcategoria]&nbsp;&raquo;&nbsp;$valorServidorComissionado[titulo]</strong></a></p>";
				
										else
				
											echo "<p style='margin: 3px 0px;'><a href='$valorServidorComissionado[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$valorServidorComissionado[categoria]&nbsp;&raquo;&nbsp;$valorServidorComissionado[subcategoria]&nbsp;&raquo;&nbsp;$valorServidorComissionado[titulo]</strong></a></p>";
				
				
				
									}
				
				
				
									echo "<p>&nbsp;</p>";
				
				
				
								}
				
				
				
				break;

				case "116":
				
								$qryServidoresEfetivos = $conn->query("SELECT servidores_efetivos.*, servidores_efetivos_categoria.descricao
				
															FROM servidores_efetivos, servidores_efetivos_categoria
				
														   WHERE servidores_efetivos.id_categoria = servidores_efetivos_categoria.id
				
															 AND servidores_efetivos_categoria.id_cliente = '$cliente'
				
															 AND servidores_efetivos.status_registro = 'A'
				
															 AND (servidores_efetivos.titulo LIKE '%$busca%' OR servidores_efetivos_categoria.descricao LIKE '%$busca%')
				
														ORDER BY servidores_efetivos.titulo")->fetchAll(PDO::FETCH_ASSOC);
				
					
				
					$contador = count($qryServidoresEfetivos);
								if(count($qryServidoresEfetivos) > 0) {
									$contaResultados++;
				
					
				
				
				
									echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $relatorioViagem . "$complemento'>Servidores Efetivos</a><span class='badge'>$contador</span></h3>";
				
				
				
									foreach ($qryServidoresEfetivos as $valorServidorEfetivo) {
				
				
				
										if ($valorServidorEfetivo['arquivo'] != "")
				
											echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$valorServidorEfetivo[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$valorServidorEfetivo[categoria]&nbsp;&raquo;&nbsp;$valorServidorEfetivo[subcategoria]&nbsp;&raquo;&nbsp;$valorServidorEfetivo[titulo]</strong></a></p>";
				
										else
				
											echo "<p style='margin: 3px 0px;'><a href='$valorServidorEfetivo[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$valorServidorEfetivo[categoria]&nbsp;&raquo;&nbsp;$valorServidorEfetivo[subcategoria]&nbsp;&raquo;&nbsp;$valorServidorEfetivo[titulo]</strong></a></p>";
				
				
				
									}
				
				
				
									echo "<p>&nbsp;</p>";
				
				
				
								}
				
				
				
				break;

				case "117":
				
								$qryLicenciamentoAmbiental = $conn->query("SELECT licenciamento_ambiental.*, licenciamento_ambiental_categoria.descricao
				
															FROM licenciamento_ambiental, licenciamento_ambiental_categoria
				
														   WHERE licenciamento_ambiental.id_categoria = licenciamento_ambiental_categoria.id
				
															 AND licenciamento_ambiental_categoria.id_cliente = '$cliente'
				
															 AND licenciamento_ambiental.status_registro = 'A'
				
															 AND (licenciamento_ambiental.titulo LIKE '%$busca%' OR licenciamento_ambiental_categoria.descricao LIKE '%$busca%')
				
														ORDER BY licenciamento_ambiental.titulo")->fetchAll(PDO::FETCH_ASSOC);
				
					
				
					$contador = count($qryLicenciamentoAmbiental);
								if(count($qryLicenciamentoAmbiental) > 0) {
									$contaResultados++;
				
					
				
				
				
									echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $relatorioViagem . "$complemento'>Licenciamento Ambiental</a><span class='badge'>$contador</span></h3>";
				
				
				
									foreach ($qryLicenciamentoAmbiental as $valorLicenciamentoAmbiental) {
				
				
				
										if ($valorLicenciamentoAmbiental['arquivo'] != "")
				
											echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$valorLicenciamentoAmbiental[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$valorLicenciamentoAmbiental[categoria]&nbsp;&raquo;&nbsp;$valorLicenciamentoAmbiental[subcategoria]&nbsp;&raquo;&nbsp;$valorLicenciamentoAmbiental[titulo]</strong></a></p>";
				
										else
				
											echo "<p style='margin: 3px 0px;'><a href='$valorLicenciamentoAmbiental[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$valorLicenciamentoAmbiental[categoria]&nbsp;&raquo;&nbsp;$valorLicenciamentoAmbiental[subcategoria]&nbsp;&raquo;&nbsp;$valorLicenciamentoAmbiental[titulo]</strong></a></p>";
				
				
				
									}
				
				
				
									echo "<p>&nbsp;</p>";
				
				
				
								}
				
				
				
				break;


				case "118":

				$qryConcursoPss = $conn->query("SELECT *
											   FROM concurso_pss
											  WHERE id_cliente = '$cliente'
											    AND status_registro = 'A'
											    AND (titulo LIKE '%$busca%' OR artigo LIKE '%$busca%');
												ORDER BY data_publicacao DESC, id DESC")->fetchAll(PDO::FETCH_ASSOC);
				
				$contador = count($qryConcursoPss);
				if(count($qryConcursoPss) > 0) {
					$contaResultados++;

					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $concursoPssLogin . "$complemento'>Concursos</a><span class='badge'>$contador</span></h3>";
					?>
					<div class="panel-group" id="accordion_concurso_pss" role="tablist" aria-multiselectable="true">

					<?php foreach($qryConcursoPss as $concursoPss) { ?>

					<div class="panel panel-default">
					    <div class="panel-heading" role="tab" id="heading_<?= $concursoPss['id'] ?>">
					     	<h4 class="panel-title">
					        	<a role="button" data-toggle="collapse" data-parent="#accordion_concurso_pss" href="#collapse_<?= $concursoPss['id'] ?>" aria-expanded="true" aria-controls="collapse_<?= $concursoPss['id'] ?>">
					          		<i class="glyphicon glyphicon-triangle-right"></i> <strong><?= $concursoPss['titulo'] ?></strong>
					        	</a>
					    	</h4>
					    </div>
						<div id="collapse_<?= $concursoPss['id'] ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_<?= $concursoPss['id'] ?>">
							<div class="panel-body">
								<p><strong>Data da Publica&ccedil;&atilde;o:</strong> <?= formata_data($concursoPss['data_publicacao']) ?></p>
								<p><strong>Data da Prova:</strong> <?= formata_data($concursoPss['data_concurso']) ?></p>
								<?= verifica($concursoPss['artigo']) ?>
								<?php
								$conf1 = (strtotime(date("Y-m-d")) >= strtotime($concursoPss['data_inicio_inscricao'])) && (strtotime(date("Y-m-d")) <= strtotime($concursoPss['data_fim_inscricao']));
								$conf2 = (strtotime(date("Y-m-d")) > strtotime($concursoPss['data_fim_inscricao'])) && (strtotime(date("Y-m-d")) <= strtotime($concursoPss['data_limite_recurso']));

								$stVaga = $conn->prepare("SELECT * FROM concurso_pss_vaga WHERE id_cliente = :id_cliente AND id_artigo = :id_artigo AND status_registro = :status_registro");
								$stVaga->execute(array("id_cliente" => $cliente, "id_artigo" => $concursoPss['id'], "status_registro" => "A"));
								$qryVaga = $stVaga->fetchAll();

								if($concursoPss['inscricao'] == "S" && ($conf1 || $conf2) && count($qryVaga)) {
								?>
								<div class="panel panel-danger">
									<div class="panel-heading">
								    	<h2 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i> <?= $conf1 ? "FA&Ccedil;A SUA INSCRI&Ccedil;&Atilde;O" : "&Aacute;REA DO CANDIDATO" ?></h2>
								  	</div>
									<div class="panel-body">
										<?php
										foreach ($qryVaga as $vaga) {
										?>
											<p><a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$concursoPssLista.$complemento); ?>&id_vaga=<?= $vaga['id'] ?>"><strong><i class="glyphicon glyphicon-triangle-right"></i>&nbsp;<?= $vaga['vaga'] ?></strong></a></p>
										<?php } ?>
									</div>
								</div>
								<?php } ?>
								<?php
								$stAnexo = $conn->prepare("SELECT * FROM concurso_pss_anexo WHERE id_artigo = :id_artigo ORDER BY ordem ASC, id DESC");
								$stAnexo->execute(array("id_artigo" => $concursoPss['id']));
								$qryAnexo = $stAnexo->fetchAll();
								if(count($qryAnexo)) {
								?>
								<div class="panel panel-primary">
									<div class="panel-heading">
								    	<h2 class="panel-title"><i class="glyphicon glyphicon-cloud-download"></i> Anexos</h2>
								  	</div>
									<div class="panel-body">
									<?php
									foreach ($qryAnexo as $anexo) {
									?>
										<p><strong><a href="<?= $CAMINHOARQ ?>/<?= $anexo['arquivo'] ?>" target="_blank"><i class="glyphicon glyphicon-cloud-download"></i> <?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?></a></strong></p>
									<?php } ?>
									</div>
								</div>
								<?php } ?>
								<?php
								$stLink = $conn->prepare("SELECT * FROM concurso_pss_link WHERE id_artigo = :id_artigo ORDER BY ordem ASC, id DESC");
								$stLink->execute(array("id_artigo" => $concursoPss['id']));
								$qryLink = $stLink->fetchAll();
								if(count($qryLink)) {
								?>
								<div class="panel panel-primary">
									<div class="panel-heading">
								    	<h2 class="panel-title"><i class="glyphicon glyphicon-globe"></i> Links</h2>
								  	</div>
									<div class="panel-body">
									<?php
									foreach ($qryLink as $linkConcursoPss) {
									?>
										<p><strong><a href="<?= $linkConcursoPss['link'] ?>" target="_blank"><i class="glyphicon glyphicon-globe"></i> <?= empty($linkConcursoPss['descricao']) ? "Link" : $linkConcursoPss['descricao'] ?></a></strong></p>
									<?php } ?>
									</div>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>
					<?php } ?>
				</div>
				<?php
				}

				break;

				case "119":

		        $qryEstagiarios = $conn->query("SELECT estagiarios.*, estagiarios_categoria.descricao categoria, estagiarios_subcategoria.descricao subcategoria

													   FROM estagiarios

												  LEFT JOIN estagiarios_categoria ON estagiarios.id_categoria = estagiarios_categoria.id

												  LEFT JOIN estagiarios_subcategoria ON estagiarios.id_subcategoria = estagiarios_subcategoria.id

													  WHERE estagiarios_categoria.id_cliente = '$cliente'

													    AND estagiarios.status_registro = 'A'

													    AND (estagiarios.titulo LIKE '%$busca%' OR estagiarios_categoria.descricao LIKE '%$busca%' OR estagiarios_subcategoria.descricao LIKE '%$busca%')

												   ORDER BY estagiarios.titulo")->fetchAll(PDO::FETCH_ASSOC);



					$contador = count($qryEstagiarios);
				if(count($qryEstagiarios) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $estagiarios . "$complemento'>Estagiarios</a><span class='badge'>$contador</span></h3>";



					foreach($qryEstagiarios as $estagiarios) {



						if($estagiarios['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$estagiarios[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$estagiarios[categoria]&nbsp;&raquo;&nbsp;$estagiarios[subcategoria]&nbsp;&raquo;&nbsp;$estagiarios[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$estagiarios[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$estagiarios[categoria]&nbsp;&raquo;&nbsp;$estagiarios[subcategoria]&nbsp;&raquo;&nbsp;$estagiarios[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}

				break;

				case "120":

		        $qryPlanoSaude = $conn->query("SELECT plano_saude.*, plano_saude_categoria.descricao categoria, plano_saude_subcategoria.descricao subcategoria

													 FROM plano_saude

											    LEFT JOIN plano_saude_categoria ON plano_saude.id_categoria = plano_saude_categoria.id

											    LEFT JOIN plano_saude_subcategoria ON plano_saude.id_subcategoria = plano_saude_subcategoria.id

												    WHERE plano_saude_categoria.id_cliente = '$cliente'

													  AND plano_saude.status_registro = 'A'

													  AND (plano_saude.titulo LIKE '%$busca%' OR plano_saude_categoria.descricao LIKE '%$busca%' OR plano_saude_subcategoria.descricao LIKE '%$busca%')

												 ORDER BY plano_saude.titulo")->fetchAll(PDO::FETCH_ASSOC);



					$contador = count($qryPlanoSaude);
				if(count($qryPlanoSaude) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $planoSaude . "$complemento'>Plano Municipal de Habita&ccedil;&atilde;o</a><span class='badge'>$contador</span></h3>";



					foreach($qryPlanoSaude as $plano_saude) {



						if($plano_saude['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$plano_saude[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$plano_saude[categoria]&nbsp;&raquo;&nbsp;$plano_saude[subcategoria]&nbsp;&raquo;&nbsp;$plano_saude[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$plano_saude[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$plano_saude[categoria]&nbsp;&raquo;&nbsp;$plano_saude[subcategoria]&nbsp;&raquo;&nbsp;$plano_saude[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}

				break;

				case "121":
				
								$qryCardapioEscolar = $conn->query("SELECT cardapio_escolar.*, cardapio_escolar_categoria.descricao
				
															FROM cardapio_escolar, cardapio_escolar_categoria
				
														   WHERE cardapio_escolar.id_categoria = cardapio_escolar_categoria.id
				
															 AND cardapio_escolar_categoria.id_cliente = '$cliente'
				
															 AND cardapio_escolar.status_registro = 'A'
				
															 AND (cardapio_escolar.titulo LIKE '%$busca%' OR cardapio_escolar_categoria.descricao LIKE '%$busca%')
				
														ORDER BY cardapio_escolar.titulo")->fetchAll(PDO::FETCH_ASSOC);
				
					
				
					$contador = count($qryCardapioEscolar);
								if(count($qryCardapioEscolar) > 0) {
									$contaResultados++;
				
					
				
				
				
									echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $cardapioEscolar . "$complemento'>Cardapio Escolar</a><span class='badge'>$contador</span></h3>";
				
				
				
									foreach ($qryCardapioEscolar as $valorCardapioEscolar) {
				
				
				
										if ($valorCardapioEscolar['arquivo'] != "")
				
											echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$valorCardapioEscolar[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$valorCardapioEscolar[categoria]&nbsp;&raquo;&nbsp;$valorCardapioEscolar[subcategoria]&nbsp;&raquo;&nbsp;$valorCardapioEscolar[titulo]</strong></a></p>";
				
										else
				
											echo "<p style='margin: 3px 0px;'><a href='$valorCardapioEscolar[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$valorCardapioEscolar[categoria]&nbsp;&raquo;&nbsp;$valorCardapioEscolar[subcategoria]&nbsp;&raquo;&nbsp;$valorCardapioEscolar[titulo]</strong></a></p>";
				
				
				
									}
				
				
				
									echo "<p>&nbsp;</p>";
				
				
				
								}
				
				
				
				break;

				case "122":

				$qryContratacaoDireta = $conn->query("SELECT contratacao_direta.*, contratacao_direta_categoria.descricao categoria, contratacao_direta_subcategoria.descricao subcategoria

											   	   FROM contratacao_direta

										  	  LEFT JOIN contratacao_direta_categoria ON contratacao_direta.id_categoria = contratacao_direta_categoria.id

										  	  LEFT JOIN contratacao_direta_subcategoria ON contratacao_direta.id_subcategoria = contratacao_direta_subcategoria.id

											 	  WHERE contratacao_direta_categoria.id_cliente = '$cliente'

											        AND contratacao_direta.status_registro = 'A'

											        AND (contratacao_direta.titulo LIKE '%$busca%' OR contratacao_direta_categoria.descricao LIKE '%$busca%' OR contratacao_direta_subcategoria.descricao LIKE '%$busca%')

										   	   ORDER BY contratacao_direta.titulo")->fetchAll(PDO::FETCH_ASSOC);



					$contador = count($qryContratacaoDireta);
				if(count($qryContratacaoDireta) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $contratacaoDireta . "$complemento'>Compra Direta</a><span class='badge'>$contador</span></h3>";



					foreach($qryContratacaoDireta as $contratacao_direta) {



						if($contratacao_direta['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$contratacao_direta[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$contratacao_direta[categoria]&nbsp;&raquo;&nbsp;$contratacao_direta[subcategoria]&nbsp;&raquo;&nbsp;$contratacao_direta[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$contratacao_direta[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$contratacao_direta[categoria]&nbsp;&raquo;&nbsp;$contratacao_direta[subcategoria]&nbsp;&raquo;&nbsp;$contratacao_direta[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}



				break;

				case "123":

				$qryBolsaAtleta = $conn->query("SELECT bolsa_atleta.*, bolsa_atleta_categoria.descricao categoria

										  	   FROM bolsa_atleta, bolsa_atleta_categoria

										  	  WHERE bolsa_atleta.id_categoria = bolsa_atleta_categoria.id

											    AND bolsa_atleta_categoria.id_cliente = '$cliente'

											    AND bolsa_atleta.status_registro = 'A'

											    AND (bolsa_atleta.titulo LIKE '%$busca%' OR bolsa_atleta_categoria.descricao LIKE '%$busca%')

									   	   ORDER BY bolsa_atleta.titulo")->fetchAll(PDO::FETCH_ASSOC);



					$contador = count($qryBolsaAtleta);
				if(count($qryBolsaAtleta) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $bolsaAtleta . "$complemento'>Bolsa Atléta</a><span class='badge'>$contador</span></h3>";

					?>

					<div class="panel-group" id="accordion_bolsa_atleta" role="tablist" aria-multiselectable="true">

					<?php foreach($qryBolsaAtleta as $bolsaAtleta) { ?>

					<div class="panel panel-default">

					    <div class="panel-heading" role="tab" id="heading_<?= $bolsaAtleta['id'] ?>">

					     	<h4 class="panel-title">

					        	<a role="button" data-toggle="collapse" data-parent="#accordion_bolsa_atleta" href="#collapse_<?= $bolsaAtleta['id'] ?>" aria-expanded="true" aria-controls="collapse_<?= $bolsaAtleta['id'] ?>">

					          		<i class="glyphicon glyphicon-triangle-right"></i> <?= $bolsaAtleta['titulo'] ?>

					        	</a>

					    	</h4>

					    </div>

						<div id="collapse_<?= $bolsaAtleta['id'] ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_<?= $bolsaAtleta['id'] ?>">

							<div class="panel-body">

								<?php if($bolsaAtleta['data_inicio'] != "") { ?><strong>Data de In&iacute;cio:</strong> <?= formata_data($bolsaAtleta['data_inicio']) ?><br><?php } ?>

								<?php if($bolsaAtleta['data_fim'] != "") { ?><strong>Fim da Vig&ecirc;ncia:</strong> <?= formata_data($bolsaAtleta['data_fim']) ?><br><?php } ?>

								<?php if($bolsaAtleta['numero'] != "") { ?><strong>N&ordm; do Processo:</strong> <?= $bolsaAtleta['numero'] ?><br><?php } ?>

								<?php if($bolsaAtleta['valor'] != "") { ?><strong>Valor:</strong> <?= $bolsaAtleta['valor'] ?><br><?php } ?>

								<?php if($bolsaAtleta['contrapartida'] != "") { ?><strong>Contra partida:</strong> <?= $bolsaAtleta['contrapartida'] ?><br><?php } ?>

								<?php if($bolsaAtleta['proponente'] != "") { ?><strong>Proponente:</strong> <?= $bolsaAtleta['proponente'] ?><br><?php } ?>

								<?php if($bolsaAtleta['fornecedor'] != "") { ?><strong>Fornecedor:</strong> <?= $bolsaAtleta['fornecedor'] ?><br><?php } ?>

								<?php if($bolsaAtleta['objeto'] != "") { ?><strong>Objeto:</strong> <?= verifica($bolsaAtleta['objeto']) ?><br><?php } ?>



								<?php

								$stAnexo = $conn->prepare("SELECT * FROM bolsa_atleta_anexo WHERE id_artigo = :id_artigo ORDER BY ordem ASC, id DESC");

								$stAnexo->execute(array("id_artigo" => $bolsaAtleta['id']));

								$qryAnexo = $stAnexo->fetchAll();



								if(count($qryAnexo)) {

								?>

								<div class="panel panel-primary">

									<div class="panel-heading">

								    	<h2 class="panel-title"><i class="glyphicon glyphicon-cloud-download"></i> Anexos</h2>

								  	</div>

									<div class="panel-body">

									<?php

									foreach ($qryAnexo as $anexo) {

									?>

										<p><strong><a href="<?= $CAMINHOARQ ?>/<?= $anexo['arquivo'] ?>" target="_blank"><i class="glyphicon glyphicon-cloud-download"></i> <?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?></a></strong></p>

									<?php } ?>

									</div>

								</div>

								<?php } ?>



								<?php

								$stLink = $conn->prepare("SELECT * FROM bolsa_atleta_link WHERE id_artigo = :id_artigo ORDER BY ordem ASC, id DESC");

								$stLink->execute(array("id_artigo" => $bolsaAtleta['id']));

								$qryLink = $stLink->fetchAll();



								if(count($qryLink)) {

								?>

								<div class="panel panel-primary">

									<div class="panel-heading">

								    	<h2 class="panel-title"><i class="glyphicon glyphicon-globe"></i> Links</h2>

								  	</div>

									<div class="panel-body">

									<?php

									foreach ($qryLink as $linkBolsaAtleta) {

									?>

										<p><strong><a href="<?= $linkBolsaAtleta['link'] ?>" target="_blank"><i class="glyphicon glyphicon-globe"></i> <?= empty($linkBolsaAtleta['descricao']) ? "Link" : $linkBolsaAtleta['descricao'] ?></a></strong></p>

									<?php } ?>

									</div>

								</div>

								<?php } ?>

							</div>

						</div>

					</div>

					<?php } ?>

					</div>

					<?php

				}



				break;

				case "124":

				$qryJulgamentoContas = $conn->query("SELECT julgamento_contas.*, julgamento_contas_categoria.descricao categoria, julgamento_contas_subcategoria.descricao subcategoria

											   	   FROM julgamento_contas

										  	  LEFT JOIN julgamento_contas_categoria ON julgamento_contas.id_categoria = julgamento_contas_categoria.id

										  	  LEFT JOIN julgamento_contas_subcategoria ON julgamento_contas.id_subcategoria = julgamento_contas_subcategoria.id

											 	  WHERE julgamento_contas_categoria.id_cliente = '$cliente'

											        AND julgamento_contas.status_registro = 'A'

											        AND (julgamento_contas.titulo LIKE '%$busca%' OR julgamento_contas_categoria.descricao LIKE '%$busca%' OR julgamento_contas_subcategoria.descricao LIKE '%$busca%')

										   	   ORDER BY julgamento_contas.titulo")->fetchAll(PDO::FETCH_ASSOC);



					$contador = count($qryJulgamentoContas);
				if(count($qryJulgamentoContas) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $julgamentoContas . "$complemento'>Julgamento de Contas</a><span class='badge'>$contador</span></h3>";



					foreach($qryJulgamentoContas as $julgamento_contas) {



						if($julgamento_contas['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$julgamento_contas[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$julgamento_contas[categoria]&nbsp;&raquo;&nbsp;$julgamento_contas[subcategoria]&nbsp;&raquo;&nbsp;$julgamento_contas[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$julgamento_contas[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$julgamento_contas[categoria]&nbsp;&raquo;&nbsp;$julgamento_contas[subcategoria]&nbsp;&raquo;&nbsp;$julgamento_contas[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}



				break;

				case "124":

				$qryConsoricio = $conn->query("SELECT consorcio.*, consorcio_categoria.descricao categoria, consorcio_subcategoria.descricao subcategoria

											   	   FROM consorcio

										  	  LEFT JOIN consorcio_categoria ON consorcio.id_categoria = consorcio_categoria.id

										  	  LEFT JOIN consorcio_subcategoria ON consorcio.id_subcategoria = consorcio_subcategoria.id

											 	  WHERE consorcio_categoria.id_cliente = '$cliente'

											        AND consorcio.status_registro = 'A'

											        AND (consorcio.titulo LIKE '%$busca%' OR consorcio_categoria.descricao LIKE '%$busca%' OR consorcio_subcategoria.descricao LIKE '%$busca%')

										   	   ORDER BY consorcio.titulo")->fetchAll(PDO::FETCH_ASSOC);



					$contador = count($qryConsoricio);
				if(count($qryConsoricio) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $consorcio . "$complemento'>Consorcio</a><span class='badge'>$contador</span></h3>";



					foreach($qryConsoricio as $consorcio) {



						if($consorcio['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$consorcio[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$consorcio[categoria]&nbsp;&raquo;&nbsp;$consorcio[subcategoria]&nbsp;&raquo;&nbsp;$consorcio[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$consorcio[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$consorcio[categoria]&nbsp;&raquo;&nbsp;$consorcio[subcategoria]&nbsp;&raquo;&nbsp;$consorcio[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}



				break;


				case "125":

				$qryTabelaVencimento = $conn->query("SELECT tabela_vencimento.*, tabela_vencimento_categoria.descricao categoria, tabela_vencimento_subcategoria.descricao subcategoria

											   	   FROM tabela_vencimento

										  	  LEFT JOIN tabela_vencimento_categoria ON tabela_vencimento.id_categoria = tabela_vencimento_categoria.id

										  	  LEFT JOIN tabela_vencimento_subcategoria ON tabela_vencimento.id_subcategoria = tabela_vencimento_subcategoria.id

											 	  WHERE tabela_vencimento_categoria.id_cliente = '$cliente'

											        AND tabela_vencimento.status_registro = 'A'

											        AND (tabela_vencimento.titulo LIKE '%$busca%' OR tabela_vencimento_categoria.descricao LIKE '%$busca%' OR tabela_vencimento_subcategoria.descricao LIKE '%$busca%')

										   	   ORDER BY tabela_vencimento.titulo")->fetchAll(PDO::FETCH_ASSOC);



					$contador = count($qryTabelaVencimento);
				if(count($qryTabelaVencimento) > 0) {
					$contaResultados++;



					echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $tabelaVencimento . "$complemento'>Tabela de vencimento</a><span class='badge'>$contador</span></h3>";



					foreach($qryTabelaVencimento as $tabela_vencimento) {



						if($tabela_vencimento['arquivo'] != "")

							echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$tabela_vencimento[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$tabela_vencimento[categoria]&nbsp;&raquo;&nbsp;$tabela_vencimento[subcategoria]&nbsp;&raquo;&nbsp;$tabela_vencimento[titulo]</strong></a></p>";

						else

							echo "<p style='margin: 3px 0px;'><a href='$tabela_vencimento[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$tabela_vencimento[categoria]&nbsp;&raquo;&nbsp;$tabela_vencimento[subcategoria]&nbsp;&raquo;&nbsp;$tabela_vencimento[titulo]</strong></a></p>";



					}



					echo "<p>&nbsp;</p>";



				}

				break;

				case "126":
				
								$qryTransporteEscolar = $conn->query("SELECT transporte_escolar.*, transporte_escolar_categoria.descricao
				
															FROM transporte_escolar, transporte_escolar_categoria
				
														   WHERE transporte_escolar.id_categoria = transporte_escolar_categoria.id
				
															 AND transporte_escolar_categoria.id_cliente = '$cliente'
				
															 AND transporte_escolar.status_registro = 'A'
				
															 AND (transporte_escolar.titulo LIKE '%$busca%' OR transporte_escolar_categoria.descricao LIKE '%$busca%')
				
														ORDER BY transporte_escolar.titulo")->fetchAll(PDO::FETCH_ASSOC);
				
					
				
					$contador = count($qryTransporteEscolar);
								if(count($qryTransporteEscolar) > 0) {
									$contaResultados++;
				
					
				
				
				
									echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $transporteEscolar . "$complemento'>Transporte Escolar</a><span class='badge'>$contador</span></h3>";
				
				
				
									foreach ($qryTransporteEscolar as $valorTransporteEscolar) {
				
				
				
										if ($valorTransporteEscolar['arquivo'] != "")
				
											echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$valorTransporteEscolar[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$valorTransporteEscolar[categoria]&nbsp;&raquo;&nbsp;$valorTransporteEscolar[subcategoria]&nbsp;&raquo;&nbsp;$valorTransporteEscolar[titulo]</strong></a></p>";
				
										else
				
											echo "<p style='margin: 3px 0px;'><a href='$valorTransporteEscolar[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$valorTransporteEscolar[categoria]&nbsp;&raquo;&nbsp;$valorTransporteEscolar[subcategoria]&nbsp;&raquo;&nbsp;$valorTransporteEscolar[titulo]</strong></a></p>";
				
				
				
									}
				
				
				
									echo "<p>&nbsp;</p>";
				
				
				
								}
				
				
				
				break;


				case "127":

					$qryInformacao = $conn->query("SELECT lista_espera_consulta.*, lista_espera_consulta_categoria.descricao categoria, lista_espera_consulta_subcategoria.descricao subcategoria
	
													 FROM lista_espera_consulta
	
												LEFT JOIN lista_espera_consulta_categoria ON lista_espera_consulta.id_categoria = lista_espera_consulta_categoria.id
	
												LEFT JOIN lista_espera_consulta_subcategoria ON lista_espera_consulta.id_subcategoria = lista_espera_consulta_subcategoria.id
	
													WHERE lista_espera_consulta_categoria.id_cliente = '$cliente'
	
													  AND lista_espera_consulta.status_registro = 'A'
	
													  AND (lista_espera_consulta.titulo LIKE '%$busca%' OR lista_espera_consulta_categoria.descricao LIKE '%$busca%' OR lista_espera_consulta_subcategoria.descricao LIKE '%$busca%')
	
												 ORDER BY lista_espera_consulta.titulo")->fetchAll(PDO::FETCH_ASSOC);
	
	
	
					$contador = count($qryInformacao);
					if(count($qryInformacao) > 0) {
						$contaResultados++;
	
	
	
						echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $listaEsperaConsulta . "$complemento'>Lista de espera Consulta Esecializada</a><span class='badge'>$contador</span></h3>";
	
	
	
						foreach($qryInformacao as $lista_espera_consulta) {
	
	
	
							if($lista_espera_consulta['arquivo'] != "")
	
								echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$lista_espera_consulta[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$lista_espera_consulta[categoria]&nbsp;&raquo;&nbsp;$lista_espera_consulta[subcategoria]&nbsp;&raquo;&nbsp;$lista_espera_consulta[titulo]</strong></a></p>";
	
							else
	
								echo "<p style='margin: 3px 0px;'><a href='$lista_espera_consulta[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$lista_espera_consulta[categoria]&nbsp;&raquo;&nbsp;$lista_espera_consulta[subcategoria]&nbsp;&raquo;&nbsp;$lista_espera_consulta[titulo]</strong></a></p>";
	
	
	
						}
	
	
	
						echo "<p>&nbsp;</p>";
	
	
	
					}
	
					break;

					case "128":

						$qryInformacao = $conn->query("SELECT lista_espera_sus.*, lista_espera_sus_categoria.descricao categoria, lista_espera_sus_subcategoria.descricao subcategoria
		
														 FROM lista_espera_sus
		
													LEFT JOIN lista_espera_sus_categoria ON lista_espera_sus.id_categoria = lista_espera_sus_categoria.id
		
													LEFT JOIN lista_espera_sus_subcategoria ON lista_espera_sus.id_subcategoria = lista_espera_sus_subcategoria.id
		
														WHERE lista_espera_sus_categoria.id_cliente = '$cliente'
		
														  AND lista_espera_sus.status_registro = 'A'
		
														  AND (lista_espera_sus.titulo LIKE '%$busca%' OR lista_espera_sus_categoria.descricao LIKE '%$busca%' OR lista_espera_sus_subcategoria.descricao LIKE '%$busca%')
		
													 ORDER BY lista_espera_sus.titulo")->fetchAll(PDO::FETCH_ASSOC);
		
		
						$contador = count($qryInformacao);
						if(count($qryInformacao) > 0) {
							$contaResultados++;
		
							echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $listaEsperaSus . "$complemento'>Lista de espera do SUS</a><span class='badge'>$contador</span></h3>";
		
							foreach($qryInformacao as $lista_espera_sus) {
		
		
								if($lista_espera_sus['arquivo'] != "")
		
									echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$lista_espera_sus[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$lista_espera_sus[categoria]&nbsp;&raquo;&nbsp;$lista_espera_sus[subcategoria]&nbsp;&raquo;&nbsp;$lista_espera_sus[titulo]</strong></a></p>";
		
								else
		
									echo "<p style='margin: 3px 0px;'><a href='$lista_espera_sus[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$lista_espera_sus[categoria]&nbsp;&raquo;&nbsp;$lista_espera_sus[subcategoria]&nbsp;&raquo;&nbsp;$lista_espera_sus[titulo]</strong></a></p>";
		
							}
		
							echo "<p>&nbsp;</p>";
		
						}
		
						break;

						case "128":

						$qryInformacao = $conn->query("SELECT logistica_distribuicao.*, logistica_distribuicao_categoria.descricao categoria, logistica_distribuicao_subcategoria.descricao subcategoria
		
														 FROM logistica_distribuicao
		
													LEFT JOIN logistica_distribuicao_categoria ON logistica_distribuicao.id_categoria = logistica_distribuicao_categoria.id
		
													LEFT JOIN logistica_distribuicao_subcategoria ON logistica_distribuicao.id_subcategoria = logistica_distribuicao_subcategoria.id
		
														WHERE logistica_distribuicao_categoria.id_cliente = '$cliente'
		
														  AND logistica_distribuicao.status_registro = 'A'
		
														  AND (logistica_distribuicao.titulo LIKE '%$busca%' OR logistica_distribuicao_categoria.descricao LIKE '%$busca%' OR logistica_distribuicao_subcategoria.descricao LIKE '%$busca%')
		
													 ORDER BY logistica_distribuicao.titulo")->fetchAll(PDO::FETCH_ASSOC);
		
		
						$contador = count($qryInformacao);
						if(count($qryInformacao) > 0) {
							$contaResultados++;
		
							echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $logisticaDistribuicao . "$complemento'>Lista de espera do SUS</a><span class='badge'>$contador</span></h3>";
		
							foreach($qryInformacao as $logistica_distribuicao) {
		
		
								if($logistica_distribuicao['arquivo'] != "")
		
									echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$logistica_distribuicao[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$logistica_distribuicao[categoria]&nbsp;&raquo;&nbsp;$logistica_distribuicao[subcategoria]&nbsp;&raquo;&nbsp;$logistica_distribuicao[titulo]</strong></a></p>";
		
								else
		
									echo "<p style='margin: 3px 0px;'><a href='$logistica_distribuicao[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$logistica_distribuicao[categoria]&nbsp;&raquo;&nbsp;$logistica_distribuicao[subcategoria]&nbsp;&raquo;&nbsp;$logistica_distribuicao[titulo]</strong></a></p>";
		
							}
		
							echo "<p>&nbsp;</p>";
		
						}
		
						break;

						case "129":

							$qryCovid = $conn->query("SELECT * FROM covid_19_transparencia WHERE titulo LIKE '%$busca%' AND id_cliente = '$cliente' AND status_registro = 'A'")->fetchAll(PDO::FETCH_ASSOC);
			
			
			
								$contador = count($qryCovid);
							if(count($qryCovid) > 0) {
								$contaResultados++;
			
			
			
								echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $audienciaPublica . "$complemento'>Covid 19</a><span class='badge'>$contador</span></h3>";
			
								?>
			
								<div class="panel-group" id="accordion_covid" role="tablist" aria-multiselectable="true">
			
								<?php foreach($qryCovid as $covid) { ?>
			
								<div class="panel panel-default">
			
									<div class="panel-heading" role="tab" id="heading_<?= $covid['id'] ?>">
			
										 <h4 class="panel-title">
			
											<a role="button" data-toggle="collapse" data-parent="#accordion_covid" href="#collapse_<?= $covid['id'] ?>" aria-expanded="true" aria-controls="collapse_<?= $covid['id'] ?>">
			
												  <div class="row">
			
													  <div class="col-sm-4"><i class="glyphicon glyphicon-triangle-right"></i> <strong><?= $covid['titulo'] ?></strong></div>
			
												  </div>
			
											</a>
			
										</h4>
			
									</div>
			
									<div id="collapse_<?= $covid['id'] ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_<?= $covid['id'] ?>">
			
										<div class="panel-body">
			
											<?= verifica($covid['artigo']) ?>
			
			
			
											<?php
			
											$stAnexo = $conn->prepare("SELECT * FROM covid_19_transparencia_anexo WHERE id_artigo = :id_artigo ORDER BY ordem ASC, id DESC");
			
											$stAnexo->execute(array("id_artigo" => $covid['id']));
			
											$qryAnexo = $stAnexo->fetchAll();
			
			
			
											if(count($qryAnexo)) {
			
											?>
			
											<div class="panel panel-primary">
			
												<div class="panel-heading">
			
													<h2 class="panel-title"><i class="glyphicon glyphicon-cloud-download"></i> Anexos</h2>
			
												  </div>
			
												<div class="panel-body">
			
												<?php
			
												foreach ($qryAnexo as $anexo) {
			
												?>
			
													<p><strong><a href="<?= $CAMINHOARQ ?>/<?= $anexo['arquivo'] ?>" target="_blank"><i class="glyphicon glyphicon-cloud-download"></i> <?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?></a></strong></p>
			
												<?php } ?>
			
												</div>
			
											</div>
			
											<?php } ?>
			
			
			
											<?php
			
											$stLink = $conn->prepare("SELECT * FROM covid_19_transparencia_link WHERE id_artigo = :id_artigo ORDER BY ordem ASC, id DESC");
			
											$stLink->execute(array("id_artigo" => $covid['id']));
			
											$qryLink = $stLink->fetchAll();
			
			
			
											if(count($qryLink)) {
			
											?>
			
											<div class="panel panel-primary">
			
												<div class="panel-heading">
			
													<h2 class="panel-title"><i class="glyphicon glyphicon-globe"></i> Links</h2>
			
												  </div>
			
												<div class="panel-body">
			
												<?php
			
												foreach ($qryLink as $linkCovid) {
			
												?>
			
													<p><strong><a href="<?= $linkCovid['link'] ?>" target="_blank"><i class="glyphicon glyphicon-globe"></i> <?= empty($linkCovid['descricao']) ? "Link" : $linkCovid['descricao'] ?></a></strong></p>
			
												<?php } ?>
			
												</div>
			
											</div>
			
											<?php } ?>
			
										</div>
			
									</div>
			
								</div>
			
								<?php } ?>
			
								</div>
			
								<?php
			
							}
			
							break;
				

		}

		
	}
		if($contaResultados == 0){
?>
		<div class="alert alert-danger">
			<strong>Nenhum Resultado encontrado</strong>
		</div>
<?php
		} 
		if($configuracaoTransparencia['exibir_pergunta_frequente'] == "S") {



			$qryPergunta = $conn->query("SELECT * FROM pergunta_frequente WHERE id_cliente = '$cliente' AND status_registro = 'A' AND (titulo LIKE '%$busca%' OR artigo LIKE '%$busca%') ORDER BY titulo")->fetchAll(PDO::FETCH_ASSOC);



				$contador = count($qryPergunta);
			if(count($qryPergunta) > 0) {
				$contaResultados++;



				echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $perguntas . "$complemento'>Perguntas Frequentes</a><span class='badge'>$contador</span></h3>";

				echo '<div class="panel-group" id="accordion_pergunta" role="tablist" aria-multiselectable="true">';



				foreach ($qryPergunta as $pergunta) {

				?>

				<div class="panel panel-default">

						<div class="panel-heading" role="tab" id="heading_pergunta_<?= $pergunta['id'] ?>">

							<h4 class="panel-title">

									<a role="button" data-toggle="collapse" data-parent="#accordion_pergunta" href="#collapse_pergunta_<?= $pergunta['id'] ?>" aria-expanded="true" aria-controls="collapse_pergunta_<?= $pergunta['id'] ?>">

										<i class="glyphicon glyphicon-triangle-right"></i> <?= verifica($pergunta['titulo']) ?>

									</a>

							</h4>

						</div>

					<div id="collapse_pergunta_<?= $pergunta['id'] ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_pergunta_<?= $pergunta['id'] ?>">

						<div class="panel-body">

							<?= verifica($pergunta['artigo']) ?>

						</div>

					</div>

				</div>

				<?php

				}



				echo "</div>";



			}

		}



		if($configuracaoTransparencia['exibir_download'] == "S") {



			$qryDownload = $conn->query("SELECT download.*, download_categoria.descricao categoria, download_subcategoria.descricao subcategoria

											FROM download

										LEFT JOIN download_subcategoria ON download.id_subcategoria = download_subcategoria.id

										LEFT JOIN download_categoria ON download.id_categoria = download_categoria.id

											WHERE download_categoria.id_cliente = '$cliente'

												AND download.status_registro = 'A'

												AND (download.descricao LIKE '%$busca%' OR download_subcategoria.descricao LIKE '%$busca%' OR download_categoria.descricao LIKE '%$busca%')")->fetchAll(PDO::FETCH_ASSOC);



			$contador = count($qryDownload);
			if(count($qryDownload) > 0) {
				$contaResultados++;



				echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $downloadLista . "$complemento'>Downloads</a><span class='badge'>$contador</span></h3>";



				foreach($qryDownload as $download) {



					echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$download[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$download[categoria]&nbsp;&raquo;&nbsp;$download[subcategoria]&nbsp;&raquo;&nbsp;$download[descricao]</strong></a></p>";



				}



			}



		}



		if($configuracaoTransparencia['exibir_link_util'] == "S") {



			$qryLink = $conn->query("SELECT *

										FROM link

										WHERE id_cliente = '$cliente'

											AND status_registro = 'A'

											AND titulo LIKE '%$busca%'")->fetchAll(PDO::FETCH_ASSOC);



			$contador = count($qryLink);
			if(count($qryLink) > 0) {
				$contaResultados++;



				echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $linkLista . "$complemento'>Links &Uacute;teis</a><span class='badge'>$contador</span></h3>";



				foreach ($qryLink as $link) {



					echo "<p style='margin: 3px 0px;'><a href='$link[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$link[titulo]</strong></a></p>";



				}



			}



		}



		if($configuracaoTransparencia['exibir_acesso_informacao'] == "S") {



			$qryInformacao = $conn->query("SELECT acesso_informacao.*, acesso_informacao_categoria.descricao categoria, acesso_informacao_subcategoria.descricao subcategoria

											FROM acesso_informacao

										LEFT JOIN acesso_informacao_categoria ON acesso_informacao.id_categoria = acesso_informacao_categoria.id

										LEFT JOIN acesso_informacao_subcategoria ON acesso_informacao.id_subcategoria = acesso_informacao_subcategoria.id

											WHERE acesso_informacao_categoria.id_cliente = '$cliente'

												AND acesso_informacao.status_registro = 'A'

												AND (acesso_informacao.titulo LIKE '%$busca%' OR acesso_informacao_categoria.descricao LIKE '%$busca%' OR acesso_informacao_subcategoria.descricao LIKE '%$busca%')

										ORDER BY acesso_informacao.titulo")->fetchAll(PDO::FETCH_ASSOC);



			$contador = count($qryInformacao);
			if(count($qryInformacao) > 0) {
				$contaResultados++;



				echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $acesso . "$complemento'>Lei de Acesso &agrave; Informa&ccedil;&atilde;o</a><span class='badge'>$contador</span></h3>";



				foreach($qryInformacao as $acesso_informacao) {



					if($acesso_informacao['arquivo'] != "")

						echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$acesso_informacao[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$acesso_informacao[categoria]&nbsp;&raquo;&nbsp;$acesso_informacao[subcategoria]&nbsp;&raquo;&nbsp;$acesso_informacao[titulo]</strong></a></p>";

					else

						echo "<p style='margin: 3px 0px;'><a href='$acesso_informacao[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$acesso_informacao[categoria]&nbsp;&raquo;&nbsp;$acesso_informacao[subcategoria]&nbsp;&raquo;&nbsp;$acesso_informacao[titulo]</strong></a></p>";



				}



				echo "<p>&nbsp;</p>";



			}



		}



		if($configuracaoTransparencia['exibir_relatorio_saude'] == "S") {



			$qryInformacao = $conn->query("SELECT relatorio_saude.*, relatorio_saude_categoria.descricao categoria, relatorio_saude_subcategoria.descricao subcategoria

											FROM relatorio_saude

										LEFT JOIN relatorio_saude_categoria ON relatorio_saude.id_categoria = relatorio_saude_categoria.id

										LEFT JOIN relatorio_saude_subcategoria ON relatorio_saude.id_subcategoria = relatorio_saude_subcategoria.id

											WHERE relatorio_saude_categoria.id_cliente = '$cliente'

												AND relatorio_saude.status_registro = 'A'

												AND (relatorio_saude.titulo LIKE '%$busca%' OR relatorio_saude_categoria.descricao LIKE '%$busca%' OR relatorio_saude_subcategoria.descricao LIKE '%$busca%')

										ORDER BY relatorio_saude.titulo")->fetchAll(PDO::FETCH_ASSOC);



			$contador = count($qryInformacao);
			if(count($qryInformacao) > 0) {
				$contaResultados++;



				echo "<h3><a href='$CAMINHO/index.php?sessao=$sequencia" . $relatorioSaude . "$complemento'>Relat&oacute;rios de Gest&atilde;o - SA&Uacute;DE</a><span class='badge'>$contador</span></h3>";



				foreach($qryInformacao as $acesso_informacao) {



					if($acesso_informacao['arquivo'] != "")

						echo "<p style='margin: 3px 0px;'><a href='$CAMINHOARQ/$relatorio_saude[arquivo]' target='_blank'><i class='glyphicon glyphicon-cloud-download'></i><strong>&nbsp;$relatorio_saude[categoria]&nbsp;&raquo;&nbsp;$relatorio_saude[subcategoria]&nbsp;&raquo;&nbsp;$relatorio_saude[titulo]</strong></a></p>";

					else

						echo "<p style='margin: 3px 0px;'><a href='$relatorio_saude[link]' target='_blank'><i class='glyphicon glyphicon-globe'></i><strong>&nbsp;$relatorio_saude[categoria]&nbsp;&raquo;&nbsp;$relatorio_saude[subcategoria]&nbsp;&raquo;&nbsp;$relatorio_saude[titulo]</strong></a></p>";



				}



				echo "<p>&nbsp;</p>";



			}



		}
	}
	?>
	</div>
</div>