<?php
 
$novoCliente = empty($_GET['nc']) ? $cliente : secure($_GET['nc']);

include "../../privado/transparencia/recaptchalib.php";

$id = secure($_REQUEST["id"]);

// ini_set('display_errors', 1);   
// ini_set('display_startup_errors', 1);   
// error_reporting(E_ALL);

if(count($_FILES['arquivo']['name'])){
    var_dump(count($_FILES['arquivo']['name']));
    exit();
}
// ------------------LOGIN LICITACAO------------------
if ($_SESSION['login_licitacao']) {
        $stValidacao = $conn->prepare("SELECT id 
                                         FROM geral_fornecedor 
                                        WHERE id = :id 
                                          AND id_cliente = :id_cliente 
                                          AND status_registro = :status_registro");
        $stValidacao->execute(array("id" => $_SESSION['id_fornecedor'], "id_cliente" => $novoCliente, "status_registro" => "A"));
        $qryValidacao = $stValidacao->fetchAll();
        if (count($qryValidacao) < 1) {
            $_SESSION['login_licitacao'] = false;
            unset($_SESSION['id_fornecedor']);
            unset($_SESSION['razao_fornecedor']);
        } else {
            $tipo = "";
            if($_GET["tipo"] == 1) $tipo = '&tipo=1';
                echo "<script>window.location='$CAMINHO/index.php?sessao=$sequencia".$licitacaoDownload."$complemento&id=$id&nc=$novoCliente".$tipo."'</script>";
        }
    }

    $stFiltro = $conn->prepare("SELECT tipo 
                                  FROM licitacao_filtro 
                                 WHERE id_cliente = :id_cliente 
                                   AND status_registro = :status_registro 
                              ORDER BY id DESC 
                                 LIMIT 1");
    $stFiltro->execute(array("id_cliente" => $novoCliente, "status_registro" => "A"));
    $qryFiltro = $stFiltro->fetch();
    $filtro = empty($qryFiltro['tipo']) ? "P" : $qryFiltro['tipo'];
?>

<h2>Processos Licitat&oacute;rios <span class="label label-default"><?= $filtro == "S" ? "Cadastro" : "Cadastro" ?></span></h2>

<ol class="breadcrumb">
    <li><a href="<?= $CAMINHO ?>">In&iacute;cio</a></li>
    <li>
        <a href="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia . $licitacaoModalidade . $complemento); ?>&nc=<?= $novoCliente ?>">Processos Licitat&oacute;rios</a></li>
    <li>
        <a href="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia . $licitacaoLista . $complemento); ?>&nc=<?= $novoCliente ?>">Lista</a>
    </li>
    <li class="active"><?= $filtro == "S" ? "Cadastro" : "Cadastro" ?></li>
</ol>
<!-- ------------------ALERTE------------------ -->
<div class="alert alert-warning" role="alert">
    <i class="glyphicon glyphicon-alert"></i> &Eacute; de responsabilidade do fornecedor fazer acessos no site da
    licitante para verificar quaisquer altera&ccedil;&otilde;es/retifica&ccedil;&otilde;es nos editais de licita&ccedil;&atilde;o
    de seu interesse.
</div>

<?php
    if ($_POST['acao'] == "cadastrar") {

        foreach ($_POST as $campo => $valor) {
            $$campo = secure($valor);
        }
        if($_POST['tipo'] == "J"){
            $tipo = "J";
        }else{
            $tipo = "F";
        }
        if ($tipo == "F") {
            $sql = "SELECT id 
                      FROM geral_fornecedor 
                     WHERE id_cliente = :id_cliente 
                       AND (cpf = :cpf_cnpj 
                        OR cpf = :cpf_cnpj2) 
                       AND status_registro = :status_registro";
            $cpf_cnpj = $_POST['cpf'];
        } else {
            $sql = "SELECT id 
                      FROM geral_fornecedor 
                     WHERE id_cliente = :id_cliente 
                       AND (cnpj = :cpf_cnpj 
                        OR cnpj = :cpf_cnpj2) 
                       AND status_registro = :status_registro";
            $cpf_cnpj = $_POST['cnpj'];
        }
        $stConf = $conn->prepare($sql);
        $stConf->execute(array("id_cliente" => $novoCliente, "cpf_cnpj" => $cpf_cnpj, "cpf_cnpj2" => preg_replace('#[^0-9]#', '', $cpf_cnpj), "status_registro" => "A"));
        $conf = $stConf->fetchAll();
        $cap = new GoogleRecaptcha();
        $verified = $cap->VerifyCaptcha($_POST['g-recaptcha-response']);
        if (!$verified) {
            echo '<p class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    O Captcha n&atilde;o foi resolvido! Verifique.
                  </p>';
        } else if ($tipo == "F" && count($conf) > 0) {
            echo '<p class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    Aten&ccedil;&atildeo! J&aacute; existe um cadastro com este CPF.
                  </p>';
        } else if ($tipo == "J" && count($conf) > 0) {
            echo '<p class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    Aten&ccedil;&atildeo! J&aacute; existe um cadastro com este CNPJ.
                  </p>';
        } else if ($tipo == "J" && empty($razao_social) && $novoCliente != "46") {
            echo '<p class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    Preencha o campo ' . ($tipo == "F" ? "Nome" : "Raz&atilde;o Social") . '!
                  </p>';
        } else if ($tipo == "F" && validaCPF($cpf_cnpj) == false && $novoCliente != "46") {
            echo '<p class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    O CPF informado &eacute; inv&aacute;lido!
                  </p>';
        } else if ($tipo == "J" && validaCNPJ($cpf_cnpj) == false && $novoCliente != "46") {
            echo '<p class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    O CNPJ informado &eacute; inv&aacute;lido!
                  </p>';
        } else if ($tipo == "J" && empty($email)) {
            echo '<p class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    Forne&ccedil;a um e-mail v&aacute;lido!
                  </p>';
        } else if (empty($senha) || ($_POST['senha'] != $_POST['conf_senha'])) {
            echo '<p class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    As senhas informadas n&atilde;o conferem!
                  </p>';
        } else if (empty($_POST['endereco']) || empty($_POST['bairro']) || empty($_POST['cep'])) {
            echo '<p class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    Informe seu endere&ccedil;o, bairro e CEP!
                  </p>';
        } else if (empty($_POST['id_municipio'])) {
            echo '<p class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    Informe seu estado e sua cidade!
                  </p>';
        } else if ($tipo == "J" && empty($responsavel)) {
            echo '<p class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    Informe o nome do respons&aacute;vel da empresa!
                  </p>';
        } else {
            $razao_social = $_POST['razao_social'];
            $nome_fantasia = $_POST['nome_fantasai'];
        
            $stCadastro = $conn->prepare("INSERT INTO geral_fornecedor (id_cliente,
                                                                        id_porte_empresarial,
                                                                        id_natureza_juridica,
                                                                        tipo,
                                                                        data_cadastro,
                                                                        data_atualizacao,
                                                                        data_inicio_atividade,
                                                                        razao_social,
                                                                        nome_fantasia,
                                                                        endereco,
                                                                        complemento,
                                                                        bairro,
                                                                        cep,
                                                                        id_municipio,
                                                                        rg,
                                                                        cpf,
                                                                        cnpj,
                                                                        inscricao_estadual,
                                                                        data_ultimo_registro_contrato_social,
                                                                        numero_ultimo_registro_contrato_social,
                                                                        telefone_fixo,
                                                                        telefone_celular,
                                                                        email,
                                                                        responsavel,
                                                                        cpf_responsavel,
                                                                        rg_responsavel,
                                                                        email_responsavel,
                                                                        endereco_responsavel,
                                                                        bairro_responsavel,
                                                                        cep_responsavel,
                                                                        id_municipio_responsavel,
                                                                        senha)
                                               VALUES (?,
                                                       ?,
                                                       ?,
                                                       ?,
                                                       NOW(),
                                                       NOW(),
                                                       ?,
                                                       ?,
                                                       ?,
                                                       ?,
                                                       ?,
                                                       ?,
                                                       ?,
                                                       ?,
                                                       ?,
                                                       ?,
                                                       ?,
                                                       ?,
                                                       ?,
                                                       ?,
                                                       ?,
                                                       ?,
                                                       ?,
                                                       ?,
                                                       ?,
                                                       ?,
                                                       ?,
                                                       ?,
                                                       ?,
                                                       ?,
                                                       ?,
                                                       ?)");

            $id_porte_empresarial = empty($id_porte_empresarial) ? NULL : $id_porte_empresarial;
            $id_natureza_juridica = empty($id_natureza_juridica) ? NULL : $id_natureza_juridica;
            $id_municipio_responsavel = empty($id_municipio_responsavel) ? NULL : $id_municipio_responsavel;
            $data_inicio_atividade = empty($data_inicio_atividade) ? NULL : formata_data_banco($data_inicio_atividade);
            $data_ultimo_registro_contrato_social = empty($data_ultimo_registro_contrato_social) ? NULL : formata_data_banco($data_ultimo_registro_contrato_social);
            $stCadastro->bindParam(1, $novoCliente, PDO::PARAM_INT);
            $stCadastro->bindParam(2, $id_porte_empresarial, PDO::PARAM_INT);
            $stCadastro->bindParam(3, $id_natureza_juridica, PDO::PARAM_INT);
            $stCadastro->bindParam(4, $tipo, PDO::PARAM_STR);
            $stCadastro->bindParam(5, $data_inicio_atividade, PDO::PARAM_STR);
            $stCadastro->bindParam(6, $razao_social, PDO::PARAM_STR);
            $stCadastro->bindParam(7, $nome_fantasia, PDO::PARAM_STR);
            $stCadastro->bindParam(8, $endereco, PDO::PARAM_STR);
            $stCadastro->bindParam(9, $complemento_endereco, PDO::PARAM_STR);
            $stCadastro->bindParam(10, $bairro, PDO::PARAM_STR);
            $stCadastro->bindParam(11, $cep, PDO::PARAM_STR);
            $stCadastro->bindParam(12, $id_municipio, PDO::PARAM_INT);
            $stCadastro->bindParam(13, $rg, PDO::PARAM_STR);
            $stCadastro->bindParam(14, $cpf, PDO::PARAM_STR);
            $stCadastro->bindParam(15, $cnpj, PDO::PARAM_STR);
            $stCadastro->bindParam(16, $inscricao_estadual, PDO::PARAM_STR);
            $stCadastro->bindParam(17, $data_ultimo_registro_contrato_social, PDO::PARAM_STR);
            $stCadastro->bindParam(18, $numero_ultimo_registro_contrato_social, PDO::PARAM_STR);
            $stCadastro->bindParam(19, $telefone_fixo, PDO::PARAM_STR);
            $stCadastro->bindParam(20, $telefone_celular, PDO::PARAM_STR);
            $stCadastro->bindParam(21, $email, PDO::PARAM_STR);
            $stCadastro->bindParam(22, $responsavel, PDO::PARAM_STR);
            $stCadastro->bindParam(23, $cpf_responsavel, PDO::PARAM_STR);
            $stCadastro->bindParam(24, $rg_responsavel, PDO::PARAM_STR);
            $stCadastro->bindParam(25, $email_responsavel, PDO::PARAM_STR);
            $stCadastro->bindParam(26, $endereco_responsavel, PDO::PARAM_STR);
            $stCadastro->bindParam(27, $bairro_responsavel, PDO::PARAM_STR);
            $stCadastro->bindParam(28, $cep_responsavel, PDO::PARAM_STR);
            $stCadastro->bindParam(29, $id_municipio_responsavel, PDO::PARAM_INT);
            $stCadastro->bindParam(30, $senha, PDO::PARAM_STR);
            $cad = $stCadastro->execute();
            if ($cad) {
                $razao_social = $_POST['razao_social'];
                $id_fornecedor = $conn->lastInsertId();
                foreach ($_POST['nome_socio'] as $indice => $nomeSocio) {
                    $stSocio = $conn->prepare("INSERT INTO geral_fornecedor_socio (id_fornecedor,
                                                                                   tipo,
                                                                                   data_cadastro,
                                                                                   razao_social,
                                                                                   cnpj_cpf) 
                                                    VALUES (?,
                                                            ?,
                                                            NOW(),
                                                            ?,
                                                            ?)");
                    $stSocio->bindValue(1, $id_fornecedor, PDO::PARAM_INT);
                    $stSocio->bindValue(2, "F", PDO::PARAM_STR);
                    $stSocio->bindValue(3, secure($nomeSocio), PDO::PARAM_STR);
                    $stSocio->bindValue(4, secure($_POST['cpf_socio'][$indice]), PDO::PARAM_STR);
                    $stSocio->execute();
                }
                $_SESSION['login_licitacao'] = true;
                $_SESSION['id_fornecedor'] = $id_fornecedor;
                $_SESSION['razao_fornecedor'] = $razao_social;
                //echo "<script>window.location='$CAMINHO/index.php?sessao=$sequencia".$licitacaoConta."$complemento&cadastro=1".($_GET['id'] ? "&id=$id" : "")."'</script>";
                $tipo = "";
                if($_GET["tipo"] == 1) $tipo = '&tipo=1';

                    echo "<script>window.location='$CAMINHO/index.php?sessao=$sequencia" . $licitacaoDownload . "$complemento&cadastro=1&id=$id&nc=$novoCliente".$tipo."'</script>";
            } else {
                echo '<p class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        N&atilde;o foi poss&iacute;vel realizar seu cadastro! Tente novamente mais tarde.
                      </p>';
            }
        }

        $extensoes = array("gif","png","jpg","jpeg", "pdf", "txt", "doc", "docx", "xls", "xlsx", "csv", "ppt", "pptx", "pps", "ppsx");

        $destino = "../../controlemunicipal/www/inga/sistema/arquivos/$cliente";
        
        $msgFiles = "";
  
        for($i = 0; $i < count($_FILES['anexo']['name']); $i++) {

            if($_FILES['anexo']['size'][$i] > 0) {
               
                $ext = strtolower(array_pop(explode(".", $_FILES['anexo']['name'][$i])));
          
                $nome_arquivo = date("dmyHis") . "_" . nomear_pasta($_FILES['anexo']['name'][$i]) . "." . $ext;
      
                if(array_search($ext, $extensoes) !== false) {
        
                    if(move_uploaded_file($_FILES['anexo']['tmp_name'][$i], $destino . "/" . $nome_arquivo)) {
                      
                        $stAnexo = $conn->prepare("INSERT INTO licitacao_documento_fornecedor (id_cliente,
                                                                                               id_fornecedor,
                                                                                               id_documento,
                                                                                               data_cadastro,
                                                                                               data_validade,
                                                                                               data_expedicao,
                                                                                               codigo_validacao,
                                                                                               arquivo)
                                                         VALUES (?,
                                                                 ?,
                                                                 ?,
                                                                 NOW(),
                                                                 ?,
                                                                 ?,
                                                                 ?,
                                                                 ?)");
                        
                        $data_validade = empty($data_validade) ? NULL : formata_data_banco($data_validade);
                        $data_expedicao = empty($data_expedicao) ? NULL : formata_data_banco($data_expedicao);
                        
                        $stAnexo->bindParam(1, $novoCliente, PDO::PARAM_INT);
                        $stAnexo->bindValue(2, $id_fornecedor, PDO::PARAM_INT);
                        $stAnexo->bindParam(3, $id_documento, PDO::PARAM_INT);
                        $stAnexo->bindParam(4, $data_validade, PDO::PARAM_STR);
                        $stAnexo->bindParam(5, $data_expedicao, PDO::PARAM_STR);
                        $stAnexo->bindParam(6, $codigo_validacao, PDO::PARAM_STR);
                        $stAnexo->bindParam(7, $_FILES['anexo']['name'][$i], PDO::PARAM_STR);
                        $stAnexo->execute();
                        $cad = $stAnexo->fetchAll();
                     
                        if(!$stAnexo->execute()) {
        
                            $msgFiles .= $_FILES['anexo']['name'][$i] . " erro ao salvar o arquivo<br>";
        
                        }else {
        
                        $msgFiles .= $_FILES['anexo']['name'][$i] . " erro ao mover o arquivo para a pasta<br>";
        
                    }
        
                } else {
        
                    $msgFiles .= $_FILES['anexo']['name'][$i] . " n&atilde;o possui um form&aacute;to v&aacute;lido<br>";
        
                }
        
            } else {
        
                // $msgFiles .= $_FILES['arquivo']['name'][$i] . " &eacute; um arquivo vazio ou inv&aacute;lido<br>";
        
            }
        }
        
        if($msgFiles != "") {
        
            echo '<p class="alert alert-danger">
        
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
                    Um ou mais arquivos enviados apresentaram algum problema:<br>'.$msgFiles.'
        
                  </p>';
        
        }
}


    } else if ($_POST['acao'] == "cadastrar_simples") {
        foreach ($_POST as $campo => $valor) {
            $$campo = secure($valor);
        }
        $cap = new GoogleRecaptcha();
        $verified = $cap->VerifyCaptcha($_POST['g-recaptcha-response']);
        if (!$verified) {
            echo '<p class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    O Captcha n&atilde;o foi resolvido! Verifique.
                  </p>';
        } else if ($tipo == "J" && empty($razao_social) && $novoCliente != "46") {
            echo '<p class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    Preencha o campo ' . ($tipo == "F" ? "Nome" : "Raz&atilde;o Social") . '!
                  </p>';
        } else if ($tipo == "F" && validaCPF($cpf) == false && $novoCliente != "46") {
            echo '<p class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    O CPF informado &eacute; inv&aacute;lido!
                  </p>';
        } else if ($tipo == "J" && validaCNPJ($cnpj) == false && $novoCliente != "46") {
            echo '<p class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    O CNPJ informado &eacute; inv&aacute;lido!
                  </p>';
        } else if ($tipo == "J" && empty($email)) {
            echo '<p class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    Forne&ccedil;a um e-mail v&aacute;lido!
                  </p>';
        } else {

            $login_login = ($tipo == "F" ? $cpf : $cnpj);
            $campoLogin = ($tipo == "F" ? "cpf" : "cnpj");
            $senha = str_replace(".", "", substr($login_login, 7));
            $stConf = $conn->prepare("SELECT id, razao_social
                                        FROM geral_fornecedor
                                       WHERE id_cliente = :id_cliente
                                         AND ($campoLogin = :cpf_cnpj OR $campoLogin = :cpf_cnpj2)
                                         AND status_registro = :status_registro
                                       LIMIT 1");
            $stConf->execute(array("id_cliente" => $novoCliente, "cpf_cnpj" => $login_login, "cpf_cnpj2" => preg_replace('#[^0-9]#', '', $login_login), "status_registro" => "A"));
            $conf = $stConf->fetchAll();
            if (empty($conf['id'])) {

                $razao_social = $_POST['razao_social'];
                $stCadastro = $conn->prepare("INSERT INTO geral_fornecedor (id_cliente,
                                                                            tipo,
                                                                            data_cadastro,
                                                                            data_atualizacao,
                                                                            razao_social,
                                                                            $campoLogin,
                                                                            senha,
                                                                            email)
                                                   VALUES (?,
                                                           ?,
                                                           NOW(),
                                                           NOW(),
                                                           ?,
                                                           ?,
                                                           ?,
                                                           ?)");

                $stCadastro->bindParam(1, $novoCliente, PDO::PARAM_INT);
                $stCadastro->bindParam(2, $tipo, PDO::PARAM_STR);
                $stCadastro->bindParam(3, $razao_social, PDO::PARAM_STR);
                $stCadastro->bindParam(4, $login_login, PDO::PARAM_STR);
                $stCadastro->bindParam(5, $senha, PDO::PARAM_STR);
                $stCadastro->bindParam(6, $email, PDO::PARAM_STR);

                $cad = $stCadastro->execute();

                if ($cad) {
                    $id_fornecedor = $conn->lastInsertId();

                    $_SESSION['login_licitacao'] = true;
                    $_SESSION['id_fornecedor'] = $id_fornecedor;
                    $_SESSION['razao_fornecedor'] = $razao_social;
                } else {
                    echo '<p class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            N&atilde;o foi poss&iacute;vel realizar seu cadastro! Tente novamente mais tarde.
                          </p>';
                }
            } else {
                $_SESSION['login_licitacao'] = true;
                $_SESSION['id_fornecedor'] = $conf['id'];
                $_SESSION['razao_fornecedor'] = $conf['razao_social'];
            }
            $tipo = "";
            if($_GET["tipo"] == 1) 
                $tipo = '&tipo=1';
            echo "<script>window.location='$CAMINHO/index.php?sessao=$sequencia" . $licitacaoDownload . "$complemento&id=$id&nc=$novoCliente".$tipo."'</script>";
        }
    } else if ($_POST['acao'] == "logar") {
        foreach ($_POST as $campo => $valor) {
            $$campo = secure($valor);
        }
        $cap = new GoogleRecaptcha();
        $verified = $cap->VerifyCaptcha($_POST['g-recaptcha-response']);
        if (!$verified) {
            echo '<p class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    O Captcha n&atilde;o foi resolvido! Verifique.
                  </p>';
        } else {
            $login_login = ($tipoLogin == "cpf" ? $loginCPF : $loginCNPJ);
            $stLogin = $conn->prepare("SELECT id, 
                                              razao_social 
                                         FROM geral_fornecedor 
                                        WHERE id_cliente = :cliente 
                                          AND ($tipoLogin = :login 
                                           OR $tipoLogin = :login2) 
                                          AND senha = :senha 
                                          AND status_registro = :status_registro 
                                        LIMIT 1");
            $stLogin->execute(array("cliente" => $novoCliente, "login" => $login_login, "login2" => preg_replace("#[^0-9]#", "", $login_login), "senha" => $login_senha, "status_registro" => "A"));
            $buscaLogin = $stLogin->fetch();
            if ($buscaLogin["id"] != "") {
                $_SESSION['login_licitacao'] = true;
                $_SESSION['id_fornecedor'] = $buscaLogin['id'];
                $_SESSION['razao_fornecedor'] = $buscaLogin['razao_social'];
                $tipo = "";
                if($_GET["tipo"] == 1) $tipo = '&tipo=1';
                echo "<script>window.location='$CAMINHO/index.php?sessao=$sequencia" . $licitacaoDownload . "$complemento&id=$id&nc=$novoCliente".$tipo."'</script>";
            } else {
                echo '<p class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        Aten&ccedil;&atilde;o! Login e/ou senha n&atilde;o encontrados.
                    </p>';
            }
        }
    } else if ($_POST['acao'] == "rec_senha") {

        $cap = new GoogleRecaptcha();
        $verified = $cap->VerifyCaptcha($_POST['g-recaptcha-response']);
        if (!$verified) {
            echo '<p class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    O Captcha n&atilde;o foi resolvido! Verifique.
                  </p>';
        } else {
            if (!empty($_POST["recSenhaCNPJ"])) {

                $stSenha = $conn->prepare("SELECT * 
                                             FROM geral_fornecedor 
                                            WHERE id_cliente = :cliente 
                                              AND (cnpj = :cnpj 
                                               OR cnpj = :cnpj2) 
                                              AND status_registro = :status_registro");
                $stSenha->execute(array("cliente" => $novoCliente, "cnpj" => secure($_POST['recSenhaCNPJ']), "cnpj2" => preg_replace("#[^0-9]#", "", secure($_POST['recSenhaCNPJ'])), "status_registro" => "A"));

            } else if (!empty($_POST["recSenhaCPF"])) {
                $stSenha = $conn->prepare("SELECT * 
                                             FROM geral_fornecedor 
                                            WHERE id_cliente = :cliente 
                                              AND (cpf = :cpf 
                                               OR cpf = :cpf2) 
                                              AND status_registro = :status_registro");
                $stSenha->execute(array("cliente" => $novoCliente, "cpf" => secure($_POST['recSenhaCPF']), "cpf2" => preg_replace("#[^0-9]#", "", secure($_POST['recSenhaCPF'])), "status_registro" => "A"));
            }
            $senha = $stSenha->fetch();
            if ($senha["id"] != "") {
                if (!empty($senha['email']) || filter_var($senha['email'], FILTER_VALIDATE_EMAIL) !== false) {
                    $subject = html_entity_decode("Recupera&ccedil;&atilde;o da senha do cadastro de licitantes da $dadosCliente[razao_social]");

                    $html .= "<p>Ol&aacute; <b>$senha[nome]</b>, segue abaixo sua senha de acesso &agrave;s licita&ccedil;&otilde;es da $dadosCliente[razao_social]:</p>";
                    $html .= "<p><strong>Sua senha &eacute;:&nbsp;&nbsp;$senha[senha]</strong></p>";
                    $html .= "<p>&nbsp;</p>";
                    $html .= "<p>Caso haja alguma informa&ccedil;&atilde;o errada, entre em contato com $dadosCliente[razao_social].</p>";
                    $html .= "<p>&nbsp;</p><p>Atenciosamente,</p><p>$dadosCliente[razao_social]</p>";
                    $headers = html_entity_decode("Recupera&ccedil;&atilde;o de Senha") . "<nao_responda@ingadigital.com.br"  . ">";
                    $to[0]['email'] = $senha['email'];
                    $to[0]['nome'] = "Recuperação de Senha";
                    $vai = envia_email_aws($to, $subject, $html, $headers, [], './ingadigital.com.br/privado/transparencia/licitacao/licitacao_cadastro.php:555');

                    if ($vai) {
                        echo '<p class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                Login encontrado, estamos enviando um e-mail para o endere&ccedil;o <strong>' . $senha["email"] . '</strong>!
                              </p>';
                    } else {
                        echo '<p class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                Aten&ccedil;&atilde;o! Algum problema ocorreu com o envio do e-mail. Entre em contato com a ' . $dadosCliente["razao_social"] . '
                              </p>';
                    }

                } else {

                    echo '<p class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            Aten&ccedil;&atilde;o! N&atilde;o encontramos um e-mail v&aacute;lido em seu cadastro. Entre em contato com a ' . $dadosCliente["razao_social"] . '
                          </p>';
                }
            } else {
                echo '<p class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        Aten&ccedil;&atilde;o! Cadastro n&atilde;o encontrado com o CNPJ/CPF informado.
                      </p>';
            }
        }
    }
?>

<!-- ------------------IDENTIFICACAO------------------ -->
<form class="form-horizontal collapse in validar_formulario_com_senha" id="form-fornecedor" action="" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <h3 class="text-info">Identifica&ccedil;&atilde;o</h3>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Pessoa</label>
        <div class="col-sm-10">
            <label class="radio-inline">
                <input type="radio" name="tipo" id="fornecedor_J" value="J" checked> Jur&iacute;dica
            </label>
            <label class="radio-inline">
                <input type="radio" name="tipo" id="fornecedor_F" value="F"> F&iacute;sica
            </label>
        </div>
    </div>
    <div class="form-group for_jur">
        <label for="id_porte_empresarial" class="col-sm-2 control-label">Porte Empresarial</label>
        <div class="col-sm-10">
            <select name="id_porte_empresarial" id="id_porte_empresarial" class="form-control required" required>
                <option value="">Selecione o porte empresarial</option>         
                <?php
                    $stPorte = $conn->prepare("SELECT * 
                                                 FROM geral_porte_empresarial 
                                                WHERE status_registro = :status_registro 
                                             ORDER BY id");
                    $stPorte->execute(array("status_registro" => "A"));
                    $qryPorte = $stPorte->fetchAll();

                    if (count($qryPorte)) {

                        foreach ($qryPorte as $porteEmpresarial) {

                            echo "<option value='$porteEmpresarial[id]' ";
                            if ($porteEmpresarial['id'] == $id_porte_empresarial) 
                                echo "selected";
                            echo ">$porteEmpresarial[sigla] - $porteEmpresarial[descricao]</option>";
                        }
                    }
                ?>

            </select>
        </div>
    </div>
    <div class="form-group for_jur">
        <label for="id_tipo_natureza_juridica" class="col-sm-2 control-label">Tipo de Natureza Jur&iacute;dica</label>
        <div class="col-sm-10">
            <select name="id_tipo_natureza_juridica" id="id_tipo_natureza_juridica" class="form-control required" required>
                <option value="">Selecione o tipo</option>
                <?php
                    $stTipoNatureza = $conn->prepare("SELECT * 
                                                        FROM geral_tipo_natureza_juridica 
                                                       WHERE status_registro = :status_registro 
                                                    ORDER BY id");
                    $stTipoNatureza->execute(array("status_registro" => "A"));
                    $qryTipoNatureza = $stTipoNatureza->fetchAll();

                    if (count($qryTipoNatureza)) {

                        foreach ($qryTipoNatureza as $tipoNatureza) {
                            echo "<option value='$tipoNatureza[id]' ";
                            if ($tipoNatureza['id'] == $id_tipo_natureza_juridica) 
                                echo "selected";
                            echo ">$tipoNatureza[descricao]</option>";
                        }
                    }
                ?>
            </select>
        </div>
    </div>
    <div class="form-group for_jur">
        <label for="id_natureza_juridica" class="col-sm-4 col-md-2 control-label">Natureza Jur&iacute;dica</label>
        <div class="col-sm-8 col-md-10">
            <p class="carregando_natureza form-control-static hidden">Aguarde, carregando...</p>
                <select name="id_natureza_juridica" id="id_natureza_juridica" class="form-control required" required>
                    <option value="">Selecione a natureza jur&iacute;dica</option>
                    <?php
                        if ($id_tipo_natureza_juridica != "") {

                            $stNatureza = $conn->prepare("SELECT * 
                                                            FROM geral_natureza_juridica 
                                                           WHERE id_tipo_natureza_juridica = :id_tipo_natureza_juridica 
                                                             AND status_registro = :status_registro 
                                                        ORDER BY id");
                            $stNatureza->execute(array("id_tipo_natureza_juridica" => $id_tipo_natureza_juridica, "status_registro" => "A"));
                            $qryNatureza = $stNatureza->fetchAll();

                            if (count($qryNatureza)) {

                                foreach ($qryNatureza as $natureza) {

                                    echo "<option value='$natureza[id]' ";
                                    if ($natureza['id'] == $id_natureza_juridica) 
                                        echo "selected";
                                    echo ">$natureza[codigo] - $natureza[denominacao]</option>";
                                }
                            }
                        }
                    ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="razao_social" class="col-sm-2 control-label label_razao">Raz&atilde;o Social</label>
        <div class="col-sm-10">
            <input type="text" name="razao_social" id="razao_social" class="form-control required" required value="<?= $razao_social ?>" placeholder="Informe sua raz&atilde;o social...">
        </div>
    </div>
    <div class="form-group for_jur">
        <label for="nome_fantasia" class="col-sm-2 control-label">Nome Fantasia</label>
        <div class="col-sm-10">
            <input type="text" name="nome_fantasia" id="nome_fantasia" class="form-control" value="<?= $nome_fantasia ?>" placeholder="Informe seu nome fantasia...">
        </div>
    </div>
    <div class="form-group for_jur">
        <label for="cnpj" class="col-sm-2 control-label">CNPJ</label>
        <div class="col-sm-10">
            <input type="text" name="cnpj" id="cnpj" class="form-control cnpj" value="<?= $cnpj ?>" placeholder="Informe seu CNPJ...">
        </div>
    </div>
    <div class="form-group for_jur">
        <label for="inscricao_estadual" class="col-sm-2 control-label">Inscri&ccedil;&atilde;o Estadual</label>
        <div class="col-sm-10">
            <input type="text" name="inscricao_estadual" id="inscricao_estadual" class="form-control required" required value="<?= $inscricao_estadual ?>"
                placeholder="Informe sua inscri&ccedil;&atilde;o estadual...">
        </div>
    </div>
    <div class="form-group for_jur">
        <label for="data_inicio_atividade" class="col-sm-2 control-label">In&iacute;cio das Atividades da
            Empresa</label>
        <div class="col-sm-10">
            <input type="text" name="data_inicio_atividade" id="data_inicio_atividade" class="form-control data required" required value="<?= $data_inicio_atividade ?>"
                placeholder="Informe a data do in&iacute;cio das atividades da empresa...">
        </div>
    </div>
    <div class="form-group for_jur">
        <label for="data_ultimo_registro_contrato_social" class="col-sm-2 control-label">Data do &Uacute;ltimo
            Registro do Contrato Social na Junta Comercial</label>
        <div class="col-sm-10">
            <input type="text" name="data_ultimo_registro_contrato_social" id="data_ultimo_registro_contrato_social" class="form-control data required" required value="<?= $data_ultimo_registro_contrato_social ?>"
                placeholder="Informe a data do &uacute;ltimo registro do contrato social na Junta Comercial...">
        </div>
    </div>
    <div class="form-group for_jur">
        <label for="numero_ultimo_registro_contrato_social" class="col-sm-2 control-label">N&uacute;mero do &Uacute;ltimo
            Registro do Contrato Social na Junta Comercial</label>
        <div class="col-sm-10">
            <input type="text" name="numero_ultimo_registro_contrato_social" id="numero_ultimo_registro_contrato_social" class="form-control required" required value="<?= $numero_ultimo_registro_contrato_social ?>"
                placeholder="Informe o n&uacute;mero do &uacute;ltimo registro do contrato social na Junta Comercial...">
        </div>
    </div>
    <div class="form-group for_fis hidden">
        <label for="cpf" class="col-sm-2 control-label">CPF</label>
        <div class="col-sm-10">
            <input type="text" name="cpf" id="cpf" class="form-control cpf required" required value="<?= $cpf ?>" placeholder="Informe seu CPF...">
        </div>
    </div>
    <div class="form-group for_fis hidden">
        <label for="rg" class="col-sm-2 control-label">RG</label>
        <div class="col-sm-10">
            <input type="text" name="rg" id="rg" class="form-control required" required value="<?= $rg ?>" placeholder="Informe seu RG...">
        </div>
    </div>
    <!-- ------------------INFORMACOES DE CONTATO------------------ -->
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <h3 class="text-info">Informa&ccedil;&otilde;es de Contato</h3>
        </div>
    </div>
    <div class="form-group">
        <label for="endereco" class="col-sm-2 control-label">Endere&ccedil;o</label>
        <div class="col-sm-10">
            <input type="text" name="endereco" id="endereco" class="form-control required" required value="<?= $endereco ?>" placeholder="Informe seu endere&ccedil;o...">
        </div>
    </div>
    <div class="form-group">
        <label for="complemento_endereco" class="col-sm-2 control-label">Complemento</label>
        <div class="col-sm-10">
            <input type="text" name="complemento_endereco" id="complemento_endereco" class="form-control" value="<?= $complemento_endereco ?>"
                placeholder="Informe o complemento do seu endere&ccedil;o...">
        </div>
    </div>
    <div class="form-group">
        <label for="bairro" class="col-sm-2 control-label">Bairro</label>
        <div class="col-sm-10">
            <input type="text" name="bairro" id="bairro" class="form-control required" required value="<?= $bairro ?>" placeholder="Informe seu bairro...">
        </div>
    </div>
    <div class="form-group">
        <label for="cep" class="col-sm-2 control-label">CEP</label>
        <div class="col-sm-10">
            <input type="text" name="cep" id="cep" class="form-control cep required" required value="<?= $cep ?>"
                placeholder="Informe seu CEP...">
        </div>
    </div>
    <div class="form-group">
        <label for="id_estado" class="col-sm-2 control-label">Estado</label>
        <div class="col-sm-10">
            <select name="id_estado" id="id_estado" class="form-control required" required>
                <option value="">Selecione o estado</option>
                <?php
                    $stEstado = $conn->prepare("SELECT * 
                                                  FROM estado 
                                                 WHERE status_registro = :status_registro 
                                              ORDER BY nome");
                    $stEstado->execute(array("status_registro" => "A"));
                    $qryEstado = $stEstado->fetchAll();

                    if (count($qryEstado)) {

                        foreach ($qryEstado as $buscaEstado) {

                            echo "<option value='$buscaEstado[id]' ";
                            if ($buscaEstado['id'] == $id_estado) 
                                echo "selected";
                            echo ">$buscaEstado[nome]</option>";
                        }
                    }
                ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="id_municipio" class="col-sm-2 control-label">Munic&iacute;pio</label>
        <div class="col-sm-10">
            <p class="carregando form-control-static hidden">Aguarde, carregando...</p>
            <select name="id_municipio" id="id_municipio" class="form-control required" required>
                <option value="">Selecione o munic&iacute;pio</option>
                <?php
                    if ($id_estado != "") {

                        $stMunicipio = $conn->prepare("SELECT * 
                                                         FROM municipio 
                                                        WHERE id_estado = :id_estado 
                                                          AND status_registro = :status_registro 
                                                     ORDER BY nome");
                        $stMunicipio->execute(array("id_estado" => $id_estado, "status_registro" => "A"));
                        $qryMunicipio = $stMunicipio->fetchAll();

                        if (count($qryMunicipio)) {

                            foreach ($qryMunicipio as $municipio) {

                                echo "<option value='$municipio[id]' ";
                                if ($municipio['id'] == $id_municipio) 
                                    echo "selected";
                                echo ">$municipio[nome]</option>";
                            }
                        }
                    }
                ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="telefone_fixo" class="col-sm-2 control-label">Telefone Fixo</label>
        <div class="col-sm-10">
            <input type="tel" name="telefone_fixo" id="telefone_fixo" class="form-control telefone" value="<?= $telefone_fixo ?>" placeholder="Informe seu telefone fixo...">
        </div>
    </div>
    <div class="form-group">
        <label for="telefone_celular" class="col-sm-2 control-label">Telefone Celular</label>
        <div class="col-sm-10">
            <input type="tel" name="telefone_celular" id="telefone_celular" class="form-control telefone" value="<?= $telefone_celular ?>" placeholder="Informe seu telefone celular...">
        </div>
    </div>
    <div class="form-group">
        <label for="email" class="col-sm-2 control-label">E-mail</label>
        <div class="col-sm-10">
            <input type="email" name="email" id="email" class="form-control email required" required value="<?= $email ?>" placeholder="Informe seu e-mail...">
        </div>
    </div>
<!-- ------------------INFORMACOES DO RESPONSAVEL------------------ --> 
    <div class="form-group for_jur">
        <div class="col-sm-offset-2 col-sm-10">
            <h3 class="text-info">Informa&ccedil;&otilde;es do Respons&aacute;vel</h3>
        </div>
    </div>
    <div class="form-group for_jur">
        <label for="responsavel" class="col-sm-2 control-label">Nome Completo do Respons&aacute;vel</label>
        <div class="col-sm-10">
            <input type="text" name="responsavel" id="responsavel" class="form-control required" required value="<?= $responsavel ?>"
                placeholder="Informe o nome completo do respons&aacute;vel pela empresa...">
        </div>
    </div>
    <div class="form-group for_jur">
        <label for="cpf_responsavel" class="col-sm-2 control-label">CPF do Respons&aacute;vel</label>
        <div class="col-sm-10">
            <input type="text" name="cpf_responsavel" id="cpf_responsavel" class="form-control cpf" value="<?= $cpf_responsavel ?>"
                placeholder="Informe o CPF do respons&aacute;vel pela empresa...">
        </div>
    </div>
    <div class="form-group for_jur">
        <label for="rg_responsavel" class="col-sm-2 control-label">RG do Respons&aacute;vel</label>
        <div class="col-sm-10">
            <input type="text" name="rg_responsavel" id="rg_responsavel" class="form-control"value="<?= $rg_responsavel ?>"
                placeholder="Informe o RG do respons&aacute;vel pela empresa...">
        </div>
    </div>
    <div class="form-group for_jur">
        <label for="email_responsavel" class="col-sm-2 control-label">E-mail do Respons&aacute;vel</label>
        <div class="col-sm-10">
            <input type="email" name="email_responsavel" id="email_responsavel" class="form-control email" value="<?= $email_responsavel ?>"
                placeholder="Informe o e-mail do respons&aacute;vel pela empresa...">
        </div>
    </div>
    <div class="form-group for_jur">
        <label for="endereco_responsavel" class="col-sm-2 control-label">Endere&ccedil;o do Respons&aacute;vel</label>
        <div class="col-sm-10">
            <input type="text" name="endereco_responsavel" id="endereco_responsavel" class="form-control required" required value="<?= $endereco_responsavel ?>"
                placeholder="Informe o endere&ccedil;o completo do respons&aacute;vel pela empresa...">
        </div>
    </div>
    <div class="form-group">
        <label for="bairro_responsavel" class="col-sm-2 control-label">Bairro do Respons&aacute;vel</label>
        <div class="col-sm-10">
            <input type="text" name="bairro_responsavel" id="bairro_responsavel" class="form-control required" required value="<?= $bairro_responsavel ?>"
                placeholder="Informe o bairro do respons&aacute;vel pela empresa...">
        </div>
    </div>
    <div class="form-group">
        <label for="cep_responsavel" class="col-sm-2 control-label">CEP do Respons&aacute;vel</label>
        <div class="col-sm-10">
            <input type="text" name="cep_responsavel" id="cep_responsavel" class="form-control cep required" required value="<?= $cep_responsavel ?>"
                placeholder="Informe o CEP do respons&aacute;vel pela empresa...">
        </div>
    </div>
    <div class="form-group for_jur">
        <label for="id_estado_responsavel" class="col-sm-2 control-label">Estado do Respons&aacute;vel</label>
        <div class="col-sm-10">
            <select name="id_estado_responsavel" id="id_estado_responsavel" required class="form-control required">
                <option value="">Selecione o estado do respons&aacute;vel</option> 
                <?php
                    if (count($qryEstado)) {
                        foreach ($qryEstado as $buscaEstado) {
                            echo "<option value='$buscaEstado[id]' ";

                            if ($buscaEstado['id'] == $id_estado_responsavel) 
                                echo "selected";
                            echo ">$buscaEstado[nome]</option>";
                        }
                    }
                ?>
            </select>
        </div>
    </div>
    <div class="form-group for_jur">
        <label for="id_municipio_responsavel" class="col-sm-2 control-label">Munic&iacute;pio do Respons&aacute;vel</label>
        <div class="col-sm-10">
            <p class="carregando_responsavel form-control-static hidden">Aguarde, carregando...</p>
            <select name="id_municipio_responsavel" id="id_municipio_responsavel" required class="form-control required">
                <option value="">Selecione o munic&iacute;pio do respons&aacute;vel</option>         
                <?php
                    if ($id_estado_responsavel != "") {
                        $stMunicipio = $conn->prepare("SELECT * 
                                                         FROM municipio 
                                                        WHERE id_estado = :id_estado 
                                                          AND status_registro = :status_registro 
                                                     ORDER BY nome");
                        $stMunicipio->execute(array("id_estado" => $id_estado_responsavel, "status_registro" => "A"));
                        $qryMunicipio = $stMunicipio->fetchAll();

                        if (count($qryMunicipio)) {
                            foreach ($qryMunicipio as $municipio) {
                                echo "<option value='$municipio[id]' ";
                                if ($municipio['id'] == $id_municipio_responsavel) 
                                    echo "selected";
                                echo ">$municipio[nome]</option>";
                            }
                        }
                    }
                ?>
            </select>
        </div>
    </div>
<!-- ------------------SOCIOS - CADASTRO DE PESSOA JURIDICA------------------ -->

    <div class="form-group for_jur">
        <div class="col-sm-offset-2 col-sm-10">
            <h3 class="text-info">S&oacute;cios</h3>
        </div>
    </div>
    <div class="form-group for_jur" id="mais_socio">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="button" class="btn btn-success pull-right" id="add_socio">
            <i class="glyphicon glyphicon-plus-sign"></i> Adicionar S&oacute;cio
            </button>
        </div>
    </div>
<!-- ------------------DEFINIR SENHA------------------ -->
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <h3 class="text-info">Definir Senha</h3>
        </div>
    </div>
    <div class="form-group">
        <label for="senha" class="col-sm-2 control-label">Senha</label>
        <div class="col-sm-10">
            <input type="password" name="senha" id="senha" class="form-control required" required
                placeholder="Informe sua senha...">
        </div>
    </div>
    <div class="form-group">
        <label for="conf_senha" class="col-sm-2 control-label">Confirme a Senha</label>
        <div class="col-sm-10">
            <input type="password" name="conf_senha" id="conf_senha" class="form-control required" required
                placeholder="Confirme sua senha...">
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10 captcha" id="recaptcha1"></div>
    </div>
<!-- ------------------BOTOES SALVAR E LIMPAR------------------ -->
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <input type="hidden" name="acao" value="cadastrar">
            <button type="reset" class="btn btn-danger">Limpar</button>
            <button type="submit" class="btn btn-primary">Enviar</button>
        </div>
    </div>
</form>

<!-- ------------------ESQUECI MINHA SENHA------------------ -->
<div class="modal fade" id="esqueci_senha">
    <div class="modal-dialog">
        <form class="form-horizontal" name="esqueci_senha" action="" method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Esqueci minha senha</h4>
                </div>
                <div class="modal-body">
                    <p>Informe o CNPJ ou CPF abaixo que enviaremos sua senha para o e-mail informado no cadastro:</p>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Pessoa</label>
                        <div class="col-sm-8">
                            <label class="radio-inline">
                                <input type="radio" name="tipoLogin" id="recSenhaCnpj" value="cnpj" checked> Jur&iacute;dica
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="tipoLogin" id="recSenhaCpf" value="cpf"> F&iacute;sica
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="recSenhaCNPJ" id="label_rec_senha" class="col-sm-4 control-label">CNPJ</label>
                        <div class="col-sm-8">
                            <input type="text" name="recSenhaCNPJ" id="recSenhaCNPJ" class="form-control cnpj required"  placeholder="Informe seu cnpj...">
                            <input type="text" name="recSenhaCPF" id="recSenhaCPF" class="form-control hidden" placeholder="Informe seu cpf...">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-4 col-sm-8 captcha" id="recaptcha3"></div>
                    </div>
                </div>
<!-- ------------------BOTOES CANCELAR E ENVIAR------------------ -->
                <div class="modal-footer">
                    <input type="hidden" name="acao" value="rec_senha">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Enviar</button>
                </div>
            </div>
        </form>
    </div>
</div>


<script src='https://www.google.com/recaptcha/api.js'></script>