<?php 
    $novoCliente = empty($_GET['nc']) ? $cliente : secure($_GET['nc']); 
?>

<h2>Processos Licitat&oacute;rios <span class="label label-default">Baixar Anexo</span></h2>
<ol class="breadcrumb">
    <li><a href="<?= $CAMINHO ?>">In&iacute;cio</a></li>
    <li>
        <a href="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia . $licitacaoModalidade . $complemento); ?>&nc=<?= $novoCliente ?>">
            Processos Licitat&oacute;rios
        </a>
    </li>
    <li>
        <a href="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia . $licitacaoLista . $complemento); ?>&nc=<?= $novoCliente ?>">Lista</a>
    </li>
    <li class="active">Baixar Anexo</li>
</ol>

<?php
    if ($_GET['cadastro'] == "1") {

        echo '<p class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                Cadastro efetuado com sucesso!
              </p>';
    }

    if ($_SESSION['login_licitacao']) {
?>
        
        <p class="text-right"> Voc&ecirc; est&aacute; logado como <strong><?= $_SESSION['razao_fornecedor'] ?></strong>&nbsp;
            <a href="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia . $licitacaoConta . $complemento) ?>&nc=<?= $novoCliente ?>"
                class="label label-primary">Acessar minha conta.</a>
            <a href="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia . $licitacaoConta . $complemento) ?>&nc=<?= $novoCliente ?>&acao=logout">
                <strong class="text-danger"><i class="glyphicon glyphicon-off"></i> Sair.</strong>
            </a>
        </p>
        
<?php
    }

    $id = secure($_REQUEST["id"]);
    
    if(!empty($id)){

        $id_fornecedor = secure($_SESSION['id_fornecedor']);

        $stFiltro = $conn->prepare("SELECT tipo 
                                      FROM licitacao_filtro 
                                     WHERE id_cliente = :id_cliente 
                                       AND status_registro = :status_registro 
                                  ORDER BY id DESC 
                                     LIMIT 1");
        $stFiltro->execute(array("id_cliente" => $novoCliente, "status_registro" => "A"));
        $qryFiltro = $stFiltro->fetch();
        $filtro = empty($qryFiltro['tipo']) ? "P" : $qryFiltro['tipo'];

        $stAnexo = $conn->prepare("SELECT licitacao_edital_anexo.*, licitacao_edital.hora_abertura,
                                          licitacao_edital.data_abertura
                                     FROM licitacao_edital_anexo, licitacao_edital
                                    WHERE licitacao_edital_anexo.id_edital = licitacao_edital.id
                                      AND licitacao_edital_anexo.id = :id
                                      AND licitacao_edital_anexo.status_registro = :status_registro
                                 ORDER BY licitacao_edital_anexo.id DESC 
                                    LIMIT 1");
        $stAnexo->execute(array("id" => $id, "status_registro" => "A"));
        $anexo = $stAnexo->fetch();

        if ($anexo['arquivo'] != "") {
            
            if ($_GET["confirm"] == "1" && $configuracaoTransparencia['permitir_download_sem_cadastro'] == "S") {

                $id_fornecedor = '1000001'; // DOWNLOAD SEM CADASTRO
                
                $stDownload = $conn->prepare("INSERT INTO licitacao_edital_download (id_edital_anexo,
                                                                                             id_fornecedor,
                                                                                             data,
                                                                                             ip)
                                                                                        VALUES (?,
                                                                                                ?,
                                                                                                NOW(),
                                                                                            ?)");

                        $stDownload->bindParam(1, $id, PDO::PARAM_INT);
                        $stDownload->bindParam(2, $id_fornecedor, PDO::PARAM_INT);
                        var_dump($id_fornecedor);
                        $stDownload->bindParam(3, $_SERVER["REMOTE_ADDR"], PDO::PARAM_STR);
                        $stDownload->execute();

                echo "<p>&nbsp;</p><h3 class='text-primary'>Obrigado por baixar o arquivo.</h3>";
                echo "<p><strong><a href='$CAMINHOARQ/$anexo[arquivo]' target='_blank'>CLIQUE AQUI</a></strong> caso o download n&atilde;o inicie automaticamente.</p>";
                echo "<iframe src='$CAMINHOCMGERAL/licitacao/download.php?arquivo=$anexo[arquivo]&cliente=$novoCliente' width='100%' height='30' scrolling='no' frameborder='0' vspace='0' hspace='0'></iframe>";
                
            } else if ($filtro != "B" && strtotime($anexo['data_abertura']) >= strtotime(date("Y-m-d")) && strtotime($anexo['hora_abertura']) >= strtotime(date("G:H"))) {
                
                if ($_SESSION['login_licitacao'] == true) {

                    $stFornecedor = $conn->prepare("SELECT *
                                                      FROM geral_fornecedor
                                                     WHERE id = :id_fornecedor
                                                       AND id_cliente = :id_cliente
                                                       AND status_registro = :status_registro");
                    $stFornecedor->execute(array("id_fornecedor" => $id_fornecedor, "id_cliente" => $novoCliente, "status_registro" => "A"));
                    $fornecedor = $stFornecedor->fetch();

                    $ok = true;

                    if ($ok) {
                        $stDownload = $conn->prepare("INSERT INTO licitacao_edital_download (id_edital_anexo,
                                                                                             id_fornecedor,
                                                                                             data,
                                                                                             ip)
                                                           VALUES (?,
                                                                   ?,
                                                                   NOW(),
                                                                   ?)");

                        $stDownload->bindParam(1, $id, PDO::PARAM_INT);
                        $stDownload->bindParam(2, $id_fornecedor, PDO::PARAM_INT);
                        $stDownload->bindParam(3, $_SERVER["REMOTE_ADDR"], PDO::PARAM_STR);
                        $stDownload->execute();
                        
                        echo "<p>&nbsp;</p><h3 class='text-primary'>Obrigado por baixar o arquivo.</h3>";
                        echo "<p><strong><a href='$CAMINHOARQ/$anexo[arquivo]' target='_blank'>CLIQUE AQUI</a></strong> caso o download n&atilde;o inicie automaticamente.</p>";
                        echo "<iframe src='$CAMINHOCMGERAL/licitacao/download.php?arquivo=$anexo[arquivo]&cliente=$novoCliente' width='100%' height='30' scrolling='no' frameborder='0' vspace='0' hspace='0'></iframe>";

                    }

                } else {
                    echo '<p class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            Voc&ecirc; precisa estar logado para baixar este arquivo.
                          </p>';
                }

            } else {

                if ($_SESSION['login_licitacao'] == true) {
                    
                    $stFornecedor = $conn->prepare("SELECT *
                                                        FROM geral_fornecedor
                                                        WHERE id = :id_fornecedor
                                                        AND id_cliente = :id_cliente
                                                        AND status_registro = :status_registro");
                    $stFornecedor->execute(array("id_fornecedor" => $id_fornecedor, "id_cliente" => $novoCliente, "status_registro" => "A"));
                    $fornecedor = $stFornecedor->fetch();

                    $ok = true;
                    
                    if ($ok) {
                        $stDownload = $conn->prepare("INSERT INTO licitacao_edital_download (id_edital_anexo,
                                                                                                id_fornecedor,
                                                                                                data,
                                                                                                ip)
                                                            VALUES (?,
                                                                    ?,
                                                                    NOW(),
                                                                    ?)");

                        $stDownload->bindParam(1, $id, PDO::PARAM_INT);
                        $stDownload->bindParam(2, $id_fornecedor, PDO::PARAM_INT);
                        $stDownload->bindParam(3, $_SERVER["REMOTE_ADDR"], PDO::PARAM_STR);
                        $stDownload->execute();

                    }

                } else {
                        if($novoCliente != 12103){
                            // echo '<p class="alert alert-danger">
                            //     <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            //     Voc&ecirc; precisa estar logado para baixar este arquivo.
                            //     </p>';
                        }
                }
                echo "<p>&nbsp;</p><h3 class='text-primary'>Obrigado por baixar o arquivo.</h3>";
                echo "<p><strong><a href='$CAMINHOARQ/$anexo[arquivo]' target='_blank'>CLIQUE AQUI</a></strong> caso o download n&atilde;o inicie automaticamente.</p>";
                echo "<iframe src='$CAMINHOCMGERAL/licitacao/download.php?arquivo=$anexo[arquivo]&cliente=$novoCliente' width='100%' height='30' scrolling='no' frameborder='0' vspace='0' hspace='0'></iframe>";
            }

        } else {

            echo '<p class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    Desculpe! O arquivo n&atilde;o foi encontrado.
                  </p>';
        }
    }
    