<?php $novoCliente = empty($_GET['nc']) ? $cliente : secure($_GET['nc']); ?>

<h2>Processos Licitat&oacute;rios</h2>

<ol class="breadcrumb">

	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>

	<li><a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$licitacaoModalidade.$complemento); ?>&nc=<?= $novoCliente ?>">Processos Licitat&oacute;rios</a></li>

	<li class="active">Lista</li>

</ol>

<!-- Cafelandia -->
<?php if($cliente == "45"){ ?>
	<a href="<?=$CAMINHO ?>/index.php?id_cliente=45&sessao=<?= verifica($sequencia.$transparenciaOnlineVisualiza.$complemento);?>&origem=licitacao&redir=link" class="btn btn-primary btn-block" style="margin-bottom: 20px">
		VER LICITAÇÕES SUPERIORES A 2018
	</a>
<?php } ?>

<!-- querencia do norte -->
<?php if($cliente == "13"){ ?>
	<a href="<?=$CAMINHO ?>/index.php?id_cliente=13&sessao=<?= verifica($sequencia.$transparenciaOnlineVisualiza.$complemento);?>&origem=licitacao&redir=link" class="btn btn-primary btn-block" style="margin-bottom: 20px">
		VER LICITAÇÕES SUPERIORES A 2019
	</a>
<?php } ?>

<?php

$stFiltro = $conn->prepare("SELECT tipo FROM licitacao_filtro WHERE id_cliente = :id_cliente AND status_registro = :status_registro ORDER BY id DESC LIMIT 1");

$stFiltro->execute(array("id_cliente" => $novoCliente, "status_registro" => "A"));

$qryFiltro = $stFiltro->fetch();

$filtro = empty($qryFiltro['tipo']) ? "P" : $qryFiltro['tipo'];



$stModalidade = $conn->prepare("SELECT * FROM licitacao_modalidade WHERE id IN (SELECT id_modalidade FROM licitacao_edital WHERE id_cliente = :id_cliente AND status_registro = :status_registro) AND status_registro = :status_registro");

$stModalidade->execute(array("id_cliente" => $novoCliente, "status_registro" => "A"));

$qryModalidade = $stModalidade->fetchAll();



if($_SESSION['login_licitacao']) {

?>

<p class="text-right">

	Voc&ecirc; est&aacute; logado como <strong><?=$_SESSION['razao_fornecedor'] ?></strong>&nbsp;

	<a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$licitacaoConta.$complemento) ?>&nc=<?= $novoCliente ?>" class="label label-primary">Acessar minha conta.</a>

	<a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$licitacaoConta.$complemento) ?>&nc=<?= $novoCliente ?>&acao=logout"><strong class="text-danger"><i class="glyphicon glyphicon-off"></i> Sair.</strong></a>

</p>

<?php } ?>




<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#form-licitacao" aria-expanded="false" aria-controls="form-licitacao">

	<i class="glyphicon glyphicon-search"></i> Pesquisar Licita&ccedil;&atilde;o

</button>

 <?php if($cliente == "12186"){ ?>


      <a href="http://www.controlemunicipal.com.br/site/geral/licitacao/LobatoCompletoAutoCotacao2025.exe" class="btn btn-warning btn-lg" style="margin-left: 15em; margin-right: 5px;" target="_blank">
		<i class="glyphicon glyphicon-download-alt"></i> Download do Software Proposta
	</a>
	<a href="#" class="btn btn-info" target="_blank">
		<i class="glyphicon glyphicon-list-alt"></i> Manual de Ajuda
	</a>
     
	
 <?php } ?>	
 <?php
if($_REQUEST['id_categoria_emergencial'] == 3){
?>

<div class="alert alert-danger" style="margin-top: 10px; text-align: center; font-size: 140%;">
	<strong>Contrata&ccedil;&otilde;es COVID-19</strong>
</div>

<?php 
}
?>
<div class="filtrodeano" style="margin-top:20px">
	<button class="btn <?= ($_POST['ano'] == '' )? 'btn-success' : 'btn-default' ?>" onclick="filtrar_form('')" style="width: 75px;">TODOS</button>
    <?php

    $anos = $conn->prepare("SELECT distinct ano
    
                                              FROM licitacao_edital, licitacao_modalidade
    
                                             WHERE licitacao_edital.id_modalidade = licitacao_modalidade.id
    
                                               AND licitacao_edital.id_cliente = :id_cliente
    
                                               AND licitacao_edital.status_registro = :status_registro
    
                                               AND (licitacao_edital.data_publicacao <= (NOW()))
    
                                          ORDER BY licitacao_edital.data_abertura DESC, licitacao_edital.id DESC");

    $anos->execute(array("id_cliente" => $novoCliente, "status_registro" => "A"));

    $ano = $anos->fetchAll();

    foreach ($ano as $key => $valor) {

       ?>
        
        <button class="btn <?= ($_POST['ano'] == $valor['ano'] )? 'btn-success' : 'btn-default' ?>" onclick="filtrar_form('<?= $valor['ano']?>')" style="width: 75px;"><span><?= $valor['ano']?></span></button>
        
        <?php
    }

    ?>

    <script>
            function filtrar_form(campo){
                $('#ano').val(campo);
                $('#form-licitacao').submit();
            }
    </script>
</div>

<?php
if($cliente == "12140"){ ?>
	<br>
	<br>
	<a class="center" style="margin-bottom: 35px" href="http://www.controlemunicipal.com.br/site/geral/licitacao/proposta_santa_lucia.exe" target="_blank"><img style="display: block;margin-left: auto;margin-right: auto" src="http://www.controlemunicipal.com.br/site/geral/images/proposta.jpg" class="imagem"></a>
	
 <?php } ?>

 <?php if($cliente == "28"){ ?>
	<p class="text-center">
		<a href="<?=$CAMINHOCMGERAL ?>/licitacao/proposta_barbosa_ferraz.exe" target="_blank">
			<img src="<?=$CAMINHOCMGERAL ?>/images/proposta.jpg" class="imagem" />
		</a><br>
		<span class="text-danger">ATEN&Ccedil;&Atilde;O!!!  Os Arquivos  proposta_barbosa_ferraz.exe e comercial.xml devem ser salvos na mesma pasta.</span><br />
		<span class="text-danger">ATEN&Ccedil;&Atilde;O!!!  Renomeie o arquivo .xml para comercial.xml.</span>
	</p>
 <?php } ?>

 <?php if($cliente == "12038"){ ?>
	<br>
	<br>
	<a class="center" style="margin-bottom: 35px" href="http://www.controlemunicipal.com.br/site/geral/licitacao/proposta_florida.exe" target="_blank"><img style="display: block;margin-left: auto;margin-right: auto" src="http://www.controlemunicipal.com.br/site/geral/images/proposta.jpg" class="imagem"></a>
	
 <?php } ?>	


<?php if($novoCliente == "19") { ?>
<br>
<div style="text-align: center; font-weight: bold; color: #808080;">
    <p style="font-size: 16px;">
        <span style="color: #f00; font-size: 26px; font-weight:bold; ">ATEN&Ccedil;&Atilde;O!</span>
        <br>
        As informa&ccedil;&otilde;es referentes &agrave;s licita&ccedil;&otilde;es publicadas a partir do dia 19/06/2018, devem ser consultadas no novo Portal da Transpar&ecirc;ncia, 
        <a style="color: #0027ff;" href="http://200.150.98.187:8090/portaltransparencia/licitacoes">clicando aqui.</a>
    </p>
<br>
</div>

<?php } ?>

<?php if($novoCliente == "12000" || $novoCliente == "19") { ?>

<p class="text-center"><a href="<?=$CAMINHOCMGERAL ?>/licitacao/propostas.exe" target="_blank"><img src="<?=$CAMINHOCMGERAL ?>/images/proposta.jpg" class="imagem" /></a></p>

<?php } ?>


<?php if($novoCliente == "1058") { ?>
    <br>
	<a href="http://187.109.199.130:8080/portaltransparencia/licitacoes" class="btn btn-primary" target="_blank">
		<i class="glyphicon glyphicon-list-alt"></i> Processos Licitatórios Online
	</a>

<p class="text-center">

    <a href="<?=$CAMINHOCMGERAL ?>/licitacao/Manual_Software_Proposta_Marialva.pdf" target="_blank">
        <img alt="Manual Kit Proposta" src="<?=$CAMINHOCMGERAL ?>/images/manual_proposta.png" width="150px" class="imagem" />
    </a>
    
    <a href="<?=$CAMINHOCMGERAL ?>/licitacao/Proposta_Marialva.exe" target="_blank">
        <img alt="Baixe o Software Proposta" src="<?=$CAMINHOCMGERAL ?>/images/software_proposta.png" width="150px" class="imagem" />
    </a>
</p>

<?php } ?>


<p class="clearfix"></p>



<form class="form-horizontal collapse" id="form-licitacao" action="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$licitacaoLista.$complemento) ?>&nc=<?= $novoCliente ?>" method="post">

	<div class="form-group">

		<label for="autarquia" class="col-sm-2 control-label">Autarquia</label>

		<div class="col-sm-10">

			<select name="id_autarquia" id="autarquia" class="form-control">

				<option value="">Selecione a Autarquia</option>

<?php
	$qryAutarquia = $conn->prepare("SELECT id, 
								
										   autarquia 
										   
									  FROM pref_autarquia 
									 
									 WHERE id_cliente = :id_cliente 
									   
									   AND status_registro = :status_registro 
								  
								  ORDER BY autarquia ASC");

	$qryAutarquia->execute(array("id_cliente" => $novoCliente, "status_registro" => "A"));

	$autarquias = $qryAutarquia->fetchAll();
	foreach ($autarquias as $key => $autarquia) {
?>

				<option value="<?= $autarquia['id'] ?>" <?php if($_REQUEST['id_autarquia'] == $autarquia['id']) echo "selected"; ?>><?= $autarquia['autarquia'] ?></option>

<?
	}
?>

			</select>

		</div>

	</div>
	
	<div class="form-group">

		<label for="id_modalidade" class="col-sm-2 control-label">Modalidade</label>

		<div class="col-sm-10">

			<select name="id_modalidade" id="id_modalidade" class="form-control">

				<option value="">Selecione a Modalidade</option>

				<?php

				if(count($qryModalidade)) {



					foreach ($qryModalidade as $modalidade) {



						echo "<option value='$modalidade[id]' ";

						if($modalidade['id'] == $_REQUEST['id_modalidade']) echo "selected";

						echo ">$modalidade[descricao]</option>";

					}

				}

				?>

			</select>

		</div>

	</div>

	<div class="form-group">

		<label for="data_abertura" class="col-sm-2 control-label">Data de Abertura</label>

		<div class="col-sm-10">

			<input type="text" name="data_abertura" id="data_abertura" class="form-control data" value="<?= $_POST['data_abertura'] ?>" placeholder="Informe a data de abertura do edital...">

		</div>

	</div>

	<div class="form-group">

		<label for="situacao" class="col-sm-2 control-label">Situa&ccedil;&atilde;o</label>

		<div class="col-sm-10">

			<select name="situacao" id="situacao" class="form-control">

				<option value="">Selecione a Situa&ccedil;&atilde;o</option>

				<option value="A" <?php if($_REQUEST['situacao'] == "A") echo "selected"; ?>>Em Andamento</option>

				<option value="F" <?php if($_REQUEST['situacao'] == "F") echo "selected"; ?>>Finalizadas</option>

			</select>

		</div>

	</div>

	<div class="form-group">

		<label for="data1" class="col-sm-2 control-label">Publicados a partir de</label>

		<div class="col-sm-10">

			<input type="text" name="data1" id="data1" class="form-control data" value="<?= $_POST['data1'] ?>" placeholder="Informe uma data inicial de publica&ccedil;&atilde;o...">

		</div>

	</div>

	<div class="form-group">

		<label for="data2" class="col-sm-2 control-label">Publicados at&eacute;</label>

		<div class="col-sm-10">

			<input type="text" name="data2" id="data2" class="form-control data" value="<?= $_POST['data2'] ?>" placeholder="Informe uma data final de publica&ccedil;&atilde;o...">

		</div>

	</div>

	<div class="form-group">

		<label for="numero_licitacao" class="col-sm-2 control-label">N&ordm; Licita&ccedil;&atilde;o</label>

		<div class="col-sm-10">

			<input type="text" name="numero_licitacao" id="numero_licitacao" class="form-control" value="<?= $_POST['numero_licitacao'] ?>" placeholder="Informe o n&uacute;mero da licita&ccedil;&atilde;o...">

		</div>

	</div>

	<div class="form-group">

		<label for="ano" class="col-sm-2 control-label">Ano</label>

		<div class="col-sm-10">

			<input type="text" name="ano" id="ano" class="form-control ano" value="<?= $_POST['ano'] ?>" placeholder="Informe o ano da licita&ccedil;&atilde;o...">
			<input type="hidden" name="id_categoria_emergencial" id="id_categoria_emergencial" value="<?= $_POST['id_categoria_emergencial'] ?>">
		</div>

	</div>

	<div class="form-group">

		<label for="objetoLicitacao" class="col-sm-2 control-label">Objeto</label>

		<div class="col-sm-10">

			<input type="text" name="objetoLicitacao" id="objetoLicitacao" class="form-control" value="<?= $_POST['objetoLicitacao'] ?>" placeholder="Informe um trecho do objeto da licita&ccedil;&atilde;o...">

		</div>

	</div>

	<div class="form-group">

		<div class="col-sm-offset-2 col-sm-10">

			<input type="hidden" name="acao" value="cadastrar">

			<button type="reset" class="btn btn-danger">Limpar</button>

			<button type="submit" class="btn btn-primary">Enviar</button>

		</div>

	</div>

</form>



<p class="clearfix"></p>



<div class="row">



	<div class="col-md-3">

		<?php

		if(count($qryModalidade)) {

		?>

		<div class="list-group">

			<?php

			foreach ($qryModalidade as $modalidade) {
				if($_REQUEST['id_categoria_emergencial'] != "") {
					$id_covid = $_REQUEST['id_categoria_emergencial'];
					$andCovid = " AND id_categoria_emergencial = '$id_covid' ";
					$linkCovid = "&id_categoria_emergencial=".$id_covid;
				}
				
				if($_POST['ano'] == ''){
					$sql = "SELECT COUNT(id) AS total
					
								FROM licitacao_edital

							WHERE id_cliente = :id_cliente

								AND id_modalidade = :id_modalidade
								$andCovid
								AND status_registro = :status_registro";
								
											
					$stTotal = $conn->prepare($sql);
					
					$stTotal->execute(array("id_cliente" => $novoCliente, "id_modalidade" => $modalidade['id'], "status_registro" => "A"));

					$totalModalidade = $stTotal->fetch();
				}else{
				$sql = "SELECT COUNT(id) AS total
				
							FROM licitacao_edital

						WHERE id_cliente = :id_cliente

							AND id_modalidade = :id_modalidade
							$andCovid
							AND status_registro = :status_registro
							
							AND ano = :ano";
										
				$stTotal = $conn->prepare($sql);

				$data = getdate();

				$ano_pesquisa = ($_POST['ano'] != '') ? secure($_POST['ano']) : $data['year'];
				
				$stTotal->execute(array("id_cliente" => $novoCliente, "id_modalidade" => $modalidade['id'], "status_registro" => "A", "ano" => $ano_pesquisa ));

				$totalModalidade = $stTotal->fetch();
			}

			?>

			<a class="list-group-item <?php if($_REQUEST['id_modalidade'] == $modalidade['id']) echo "active"; ?>" href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$licitacaoLista.$complemento) ?>&nc=<?= $novoCliente ?>&id_modalidade=<?= $modalidade['id'] ?><?=$linkCovid?>">

				<span class="badge"><?= $totalModalidade['total'] ?></span>

				<?= $modalidade['descricao'] ?>

			</a>

			<?php } ?>

		</div>

		<?php } ?>

	</div>



	<div class="col-md-9">



		<?php

		$pagina = isset($_REQUEST['pagina']) ? $_REQUEST['pagina'] : 1;

		$max = 20;

		$inicio = $max * ($pagina - 1);



		$link = $CAMINHO . "/index.php?sessao=" . $_GET['sessao'];



		$sql = "SELECT licitacao_edital.*, licitacao_modalidade.descricao modalidade

				  FROM licitacao_edital, licitacao_modalidade

				 WHERE licitacao_edital.id_modalidade = licitacao_modalidade.id

				   AND licitacao_edital.id_cliente = :id_cliente

				   AND licitacao_edital.status_registro = :status_registro ";

		$vetor["id_cliente"] = $novoCliente;

		$vetor["status_registro"] = "A";

		if($_REQUEST['id_modalidade'] != "") {

			$sql .= "AND licitacao_edital.id_modalidade = :id_modalidade ";

			$vetor["id_modalidade"] = secure($_REQUEST['id_modalidade']);

			$link .= "&id_modalidade=" . $_REQUEST['id_modalidade'];

		}

		if($_REQUEST['data_abertura'] != "") {

			$sql .= "AND licitacao_edital.data_abertura = :data_abertura ";

			$vetor["data_abertura"] = formata_data_banco(secure($_REQUEST['data_abertura']));

			$link .= "&data_abertura=" . $_REQUEST['data_abertura'];

		}

		if($_REQUEST['situacao'] == "A") {

			$sql .= "AND (licitacao_edital.data_abertura > (NOW() - INTERVAL 1 DAY)) ";

			$link .= "&situacao=A";

		}

		if($_REQUEST['situacao'] == "F") {

			$sql .= "AND (licitacao_edital.data_abertura < (NOW() - INTERVAL 1 DAY)) ";

			$link .= "&situacao=F";

		}

		if($_REQUEST['data1'] != "") {

			$sql .= "AND licitacao_edital.data_publicacao >= :data1 ";

			$vetor["data1"] = formata_data_banco(secure($_REQUEST['data1']));

			$link .= "&data1=$_REQUEST[data1]";

		}

		if($_REQUEST['data2'] != "") {

			$sql .= "AND licitacao_edital.data_publicacao <= :data2 ";

			$vetor["data2"] = formata_data_banco(secure($_REQUEST['data2']));

			$link .= "&data2=$_REQUEST[data2]";

		}

		if($_REQUEST['ano'] != "") {

			$sql .= "AND licitacao_edital.ano = :ano ";

			$vetor["ano"] = secure($_REQUEST['ano']);

			$link .= "&ano=" . $_REQUEST['ano'];

		}

		if($_REQUEST['id_categoria_emergencial'] != "") {

			$sql .= "AND licitacao_edital.id_categoria_emergencial = :id_categoria_emergencial ";

			$vetor["id_categoria_emergencial"] = secure($_REQUEST['id_categoria_emergencial']);

			$link .= "&id_categoria_emergencial=" . $_REQUEST['id_categoria_emergencial'];

		}

		if($_REQUEST['numero_licitacao'] != "") {

			$sql .= "AND licitacao_edital.titulo LIKE :numero_licitacao ";

			$vetor["numero_licitacao"] = "%" . secure($_REQUEST['numero_licitacao']) . "%";

			$link .= "&numero_licitacao=" . $_REQUEST['numero_licitacao'];

		}

		if($_REQUEST['objetoLicitacao'] != "") {

			$sql .= "AND (licitacao_edital.titulo LIKE :objetoLicitacao OR licitacao_edital.objeto LIKE :objetoLicitacao) ";

			$vetor["objetoLicitacao"] = "%" . secure($_REQUEST['objetoLicitacao']) . "%";

			$link .= "&objetoLicitacao=".secure($_REQUEST['objetoLicitacao']);

		}

		if($_REQUEST['id_autarquia'] != ""){
			
			$sql .= "AND licitacao_edital.id_autarquia = :id_autarquia ";
			
			$vetor["id_autarquia"] = secure($_REQUEST['id_autarquia']);
			
			$link .= "&id_autarquia=".secure($_REQUEST['id_autarquia']);

		}
		

		$stLinha = $conn->prepare($sql);

		$stLinha->execute($vetor);

		$qryLinha = $stLinha->fetchAll();

		$totalLinha = count($qryLinha);



		$sql .= "ORDER BY licitacao_edital.data_abertura DESC, licitacao_edital.id DESC LIMIT $inicio, $max";



		$stLicitacao = $conn->prepare($sql);

		$stLicitacao->execute($vetor);

		$qryLicitacao = $stLicitacao->fetchAll();



		if(count($qryLicitacao)) {

		?>



		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

		<?php foreach ($qryLicitacao as $licitacao) { ?>

		<div class="panel panel-default">

		    <div class="panel-heading" role="tab" id="heading_<?= $licitacao['id'] ?>">

		     	<h4 class="panel-title">

		        	<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?= $licitacao['id'] ?>" aria-expanded="true" aria-controls="collapse_<?= $licitacao['id'] ?>">

		          		<div class="row">

							<?php 
								if($novoCliente == "12101"){ 

									$stSecretaria = $conn->prepare("SELECT nome_menu FROM pref_secretaria WHERE id_cliente = :id_cliente AND id = :id AND status_registro = :status_registro LIMIT 1");
									
									$stSecretaria->execute(array("id_cliente" => $novoCliente, "id" => $licitacao['id_secretaria'], "status_registro" => "A"));
									
									$qrySecretaria = $stSecretaria->fetch(); 
							?>

							<div class="col-sm-4"><i class="glyphicon glyphicon-triangle-right"></i> <strong><?= $licitacao['titulo'] ?></strong></div>

							<div class="col-sm-2"><strong>Publica&ccedil;&atilde;o:</strong> <?= formata_data($licitacao['data_publicacao']) ?></div>

							<div class="col-sm-3"><strong>Secretaria: </strong><?= $qrySecretaria['nome_menu'] ?></div>  
							
							<div class="col-sm-3"><strong>Abertura:</strong> <?= formata_data($licitacao['data_abertura']) ?><? if(!empty($licitacao['hora_abertura'])) echo " &agrave;s " . $licitacao['hora_abertura'] ?></div>
							
							<?php 
								} else {
							?>

		          			<div class="col-sm-12" style="margin-bottom: 10px;"><strong>Abertura:</strong> <?= formata_data($licitacao['data_abertura']) ?><? if(!empty($licitacao['hora_abertura'])) echo " &agrave;s " . $licitacao['hora_abertura'] ?></div>

		          			<div class="col-sm-12" style="margin-bottom: 10px;"><i class="glyphicon glyphicon-triangle-right"></i> <strong><?= $licitacao['titulo'] ?></strong></div>

		          			<div class="col-sm-12" style="margin-bottom: 10px;"><strong>Publica&ccedil;&atilde;o:</strong> <?= formata_data($licitacao['data_publicacao']) ?></div>
		          			<?php
								if(!empty($licitacao['id_autarquia']) && $licitacao['id_autarquia'] != NULL){
									$autarquia = $conn->prepare("SELECT autarquia FROM pref_autarquia WHERE id = :id AND status_registro = :status_registro");
									$autarquia->execute(array("id" => $licitacao['id_autarquia'], "status_registro" => "A"));
									$autarquia = $autarquia->fetch();
							?>

							<div class="col-sm-12"><strong>Autarquia: </strong><?= $autarquia['autarquia'] ?></div>
							<?
								} }
							?>

		          		</div>

		        	</a>

		    	</h4>

		    </div>

			<div id="collapse_<?= $licitacao['id'] ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_<?= $licitacao['id'] ?>">

				<div class="panel-body">

					<p><strong>Modalidade:</strong> <?= $licitacao['modalidade'] ?></p>

					<? if($licitacao['valor_maximo'] != "") { ?><p><strong>Valor M&aacute;ximo:</strong> <?= $licitacao['valor_maximo'] ?></p><? } ?>

					<p><strong>Objeto:</strong> <?= verifica($licitacao['objeto']) ?></p>

					<?php
					
					$stAnexo = $conn->prepare("SELECT * FROM licitacao_edital_anexo WHERE id_edital = :id_edital AND status_registro = :status_registro ORDER BY ordem ASC, id DESC");

					$stAnexo->execute(array("id_edital" => $licitacao['id'], "status_registro" => "A"));

					$qryAnexo = $stAnexo->fetchAll();



					if(count($qryAnexo)) {

					?>

					<div class="panel panel-primary">

						<div class="panel-heading">

					    	<h2 class="panel-title"><i class="glyphicon glyphicon-cloud-download"></i> Anexos</h2>

					  	</div>

						<div class="panel-body">

						<?php

						foreach ($qryAnexo as $anexo) {



							if($filtro == "B" || strtotime($licitacao['data_abertura']) < strtotime(date("Y-m-d"))) {

							?>

							<p><strong><a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$licitacaoDownload.$complemento); ?>&nc=<?= $novoCliente ?>&id=<?= verifica($anexo['id']) ?>" class="text-danger"><i class="glyphicon glyphicon-cloud-download"></i> <?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?></a></strong></p>

							<?php

							} else if($configuracaoTransparencia['permitir_download_sem_cadastro'] == "S") {

							?>

							<p><strong><a href="#" role="button" data-toggle="modal" data-target="#anexo_<?= $anexo['id'] ?>"><i class="glyphicon glyphicon-cloud-download"></i> <?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?></a></a></strong></p>

							<div class="modal fade" id="anexo_<?= $anexo['id'] ?>">

						  		<div class="modal-dialog">

						    		<div class="modal-content">

						      			<div class="modal-header">

						        			<button type="button" class="close" data-dismiss="modal" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>

						        			<h4 class="modal-title">Baixar <?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?></h4>

						      			</div>

							      		<div class="modal-body text-center">

							      			<button type="button" class="btn btn-danger btn-margin" onclick="jQuery('#confirmacao_<?= $anexo['id'] ?>').removeClass('hidden'); return false;"><i class="glyphicon glyphicon-alert"></i> Download sem cadastro</button>

											<a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$licitacaoLogin.$complemento); ?>&nc=<?= $novoCliente ?>&id=<?= verifica($anexo['id']) ?>" class="btn btn-danger btn-margin"><i class="glyphicon glyphicon-user"></i> Download com cadastro</a><br><br>

											<div class="text-justify">

												<?php if(empty($configuracaoTransparencia['texto_download_sem_cadastro'])) { ?>

												O cadastro no sistema de licita&ccedil;&otilde;es garante o recebimento por e-mail de todas as informa&ccedil;&otilde;es pertinentes ao processo

												licitat&oacute;rio, como atas, avisos de revoga&ccedil;&atilde;o, retifica&ccedil;&otilde;es ou cancelamento. Caso optar por baixar o edital sem cadastramento, a pessoa

												f&iacute;sica ou jur&iacute;dica n&atilde;o receber&aacute; as informa&ccedil;&otilde;es atualizadas via e-mail.<br><br>

												&Eacute; de responsabilidade do fornecedor fazer acessos no site da licitante para verificar quaisquer altera&ccedil;&otilde;es/retifica&ccedil;&otilde;es nos editais de licita&ccedil;&atilde;o de seu interesse.<br><br>

												<?php

												} else {



													echo verifica($configuracaoTransparencia['texto_download_sem_cadastro']);



												}

												?>

												<label id="confirmacao_<?= $anexo['id'] ?>" class="hidden"><input type="checkbox" name="concordo_<?= $anexo['id'] ?>" id="concordo_<?= $anexo['id'] ?>" onclick="jQuery('#baixar_<?= $anexo['id'] ?>').toggle().removeClass('hidden');" /><strong>&nbsp;Li e concordo com o texto acima.</strong></label><br>

												<script>jQuery("#concordo_<?= $anexo['id'] ?>").prop("checked", false);</script>

											</div>

											<a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$licitacaoDownload.$complemento); ?>&nc=<?= $novoCliente ?>&confirm=1&id=<?= verifica($anexo['id']) ?>" id="baixar_<?= $anexo['id'] ?>" class="btn btn-primary text-center hidden">

												<i class="glyphicon glyphicon-cloud-download"></i> Baixar anexo

											</a>

							      		</div>

						    		</div>

						  		</div>

							</div>

							<?php

							} else {

							?>

							<p><strong><a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$licitacaoLogin.$complemento); ?>&nc=<?= $novoCliente ?>&id=<?= verifica($anexo['id']) ?>" class="text-danger"><i class="glyphicon glyphicon-cloud-download"></i> <?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?></a></strong></p>

							<?php } ?>

						<?php } ?>

						</div>

					</div>

					<?php } ?>

				</div>

			</div>

		</div>

		<?php } ?>

		</div>



		<?php

		$menos = $pagina - 1;

		$mais = $pagina + 1;

		$paginas = ceil($totalLinha / $max);

		if($paginas > 1) {

		?>

		<nav>

			<ul class="pagination">

				<?php if($pagina == 1) { ?>

			    <li class="disabled"><a href="#" aria-label="Anterior"><span aria-hidden="true">&laquo;</span></a></li>

			    <?php } else { ?>

			    <li><a href="<?= $link ?>&pagina=<?= $menos ?>" aria-label="Anterior"><span aria-hidden="true">&laquo;</span></a></li>

			    <?php

				}



			    if(($pagina - 4) < 1) $anterior = 1;

			    else $anterior = $pagina - 4;



			    if(($pagina + 4) > $paginas) $posterior = $paginas;

			    else $posterior = $pagina + 4;



			    for($i = $anterior; $i <= $posterior; $i++) {



			    	if($i != $pagina) {

			    ?>

			    	<li><a href="<?= $link ?>&pagina=<?= $i ?>"><?= $i ?></a></li>

			    	<?php } else { ?>

			    	<li class="active"><a href="#"><?= $i ?><span class="sr-only">(current)</span></a></li>

			   	<?php

			    	}

			    }



			    if($mais <= $paginas) {

			   	?>

			   	<li><a href="<?= $link ?>&pagina=<?= $mais ?>" aria-label="Pr&oacute;ximo"><span aria-hidden="true">&raquo;</span></a></li>

			   	<?php } ?>

			</ul>

		</nav>

		<?php } ?>



		<?php } else if($novoCliente == '12141'){ ?>

		<h4 style="	border: 1px solid #ddd;
    				padding: 15px;
    				background-color: #ffc2c2;
    				font-size: 17px;
					border-radius: 10px;
					line-height: 1.6;">
					INFORMATIVO: A C&acirc;mara de Florest&oacute;polis informa que no ano de <?=$_POST['ano']?> , n&atilde;o realizou nenhum procedimento licitat&oacute;rio, dispensa ou inexigibilidade de licita&ccedil;&atilde;o.</h4>
					

		<?php } else { ?>

		<h4 style="	border: 1px solid #ddd;
			padding: 15px;
			background-color: #ffc2c2;
			font-size: 17px;
			border-radius: 10px;
			line-height: 1.6;">
			Desculpe, n&atilde;o foi poss&iacute;vel encontrar um resultado correspondente. Por favor, verifique o termo pesquisado.</h4>

		<?php } ?>

	</div>

</div>



<?php

$atualizacao = atualizacao("compras_licitacao/licitacao", $novoCliente, $conn);

if($atualizacao != "") {

?>

<p class="text-right"><small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small></p>

<?php

}