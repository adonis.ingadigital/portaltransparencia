<h2>Aviso de Processos Licitat&oacute;rios</h2>



<?php

$novoCliente = empty($_GET['nc']) ? $cliente : secure($_GET['nc']);

?>

<ol class="breadcrumb">

    <li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>

    <li class="active">Processos Licitat&oacute;rios</li>

</ol>



<?php

$stFiltro = $conn->prepare("SELECT tipo FROM licitacao_filtro WHERE id_cliente = :id_cliente AND status_registro = :status_registro ORDER BY id DESC LIMIT 1");

$stFiltro->execute(array("id_cliente" => $novoCliente, "status_registro" => "A"));

$qryFiltro = $stFiltro->fetch();

$filtro = empty($qryFiltro['tipo']) ? "P" : $qryFiltro['tipo'];



$stModalidade = $conn->prepare("SELECT * FROM licitacao_modalidade WHERE id IN (SELECT id_modalidade FROM licitacao_edital WHERE id_cliente = :id_cliente AND status_registro = :status_registro) AND status_registro = :status_registro");

$stModalidade->execute(array("id_cliente" => $novoCliente, "status_registro" => "A"));

$qryModalidade = $stModalidade->fetchAll();



$stProximaLicitacao = $conn->prepare("SELECT licitacao_edital.*, licitacao_modalidade.descricao modalidade

	        						    FROM licitacao_edital, licitacao_modalidade

	        						   WHERE licitacao_edital.id_modalidade = licitacao_modalidade.id

										 AND licitacao_edital.id_cliente = :id_cliente

	        							 AND licitacao_edital.status_registro = :status_registro

	        							 AND (licitacao_edital.data_abertura > (NOW() - INTERVAL 1 DAY))

								    ORDER BY licitacao_edital.data_abertura DESC, licitacao_edital.id DESC");

$stProximaLicitacao->execute(array("id_cliente" => $novoCliente, "status_registro" => "A"));

$qryProximaLicitacao = $stProximaLicitacao->fetchAll();



$stLicitacaoRealizada = $conn->prepare("SELECT licitacao_edital.*, licitacao_modalidade.descricao modalidade

		        						  FROM licitacao_edital, licitacao_modalidade

		        						 WHERE licitacao_edital.id_modalidade = licitacao_modalidade.id

										   AND licitacao_edital.id_cliente = :id_cliente

		        						   AND licitacao_edital.status_registro = :status_registro

		        						   AND (licitacao_edital.data_abertura < (NOW() - INTERVAL 1 DAY))

									  ORDER BY licitacao_edital.data_abertura DESC, licitacao_edital.id DESC LIMIT 15");

$stLicitacaoRealizada->execute(array("id_cliente" => $novoCliente, "status_registro" => "A"));

$qryLicitacaoRealizada = $stLicitacaoRealizada->fetchAll();



if($_SESSION['login_licitacao']) {

    ?>

    <p class="text-right">

        Voc&ecirc; est&aacute; logado como <strong><?=$_SESSION['razao_fornecedor'] ?></strong>&nbsp;

        <a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$licitacaoConta.$complemento) ?>&nc=<?= $novoCliente ?>" class="label label-primary">Acessar minha conta.</a>

        <a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$licitacaoConta.$complemento) ?>&nc=<?= $novoCliente ?>&acao=logout"><strong class="text-danger"><i class="glyphicon glyphicon-off"></i> Sair.</strong></a>

    </p>

    <?php

}
?>

<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#form-contrato" aria-expanded="false" aria-controls="form-contrato">

    <i class="glyphicon glyphicon-search"></i> Pesquisar Licita&ccedil;&atilde;o

</button>

<p class="clearfix"></p>

<form class="form-horizontal collapse" id="form-contrato" action="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$licitacaoLista.$complemento) ?>&nc=<?= $novoCliente ?>" method="post">

    <div class="form-group">

        <label for="id_modalidade" class="col-sm-2 control-label">Modalidade</label>

        <div class="col-sm-10">

            <select name="id_modalidade" id="id_modalidade" class="form-control">

                <option value="">Selecione a Modalidade</option>

                <?php

                if(count($qryModalidade)) {



                    foreach ($qryModalidade as $modalidade) {



                        echo "<option value='$modalidade[id]' ";

                        if($modalidade['id'] == $_REQUEST['id_modalidade']) echo "selected";

                        echo ">$modalidade[descricao]</option>";

                    }

                }

                ?>

            </select>

        </div>

    </div>

    <div class="form-group">

        <label for="data_abertura" class="col-sm-2 control-label">Data de Abertura</label>

        <div class="col-sm-10">

            <input type="text" name="data_abertura" id="data_abertura" class="form-control data" value="<?= $_POST['data_abertura'] ?>" placeholder="Informe a data de abertura do edital...">

        </div>

    </div>

    <div class="form-group">

        <label for="situacao" class="col-sm-2 control-label">Situa&ccedil;&atilde;o</label>

        <div class="col-sm-10">

            <select name="situacao" id="situacao" class="form-control">

                <option value="">Selecione a Situa&ccedil;&atilde;o</option>

                <option value="A" <?php if($_REQUEST['situacao'] == "A") echo "selected"; ?>>Em Andamento</option>

                <option value="F" <?php if($_REQUEST['situacao'] == "F") echo "selected"; ?>>Finalizadas</option>

            </select>

        </div>

    </div>

    <div class="form-group">

        <label for="data1" class="col-sm-2 control-label">Publicados a partir de</label>

        <div class="col-sm-10">

            <input type="text" name="data1" id="data1" class="form-control data" value="<?= $_POST['data1'] ?>" placeholder="Informe uma data inicial de publica&ccedil;&atilde;o...">

        </div>

    </div>

    <div class="form-group">

        <label for="data2" class="col-sm-2 control-label">Publicados at&eacute;</label>

        <div class="col-sm-10">

            <input type="text" name="data2" id="data2" class="form-control data" value="<?= $_POST['data2'] ?>" placeholder="Informe uma data final de publica&ccedil;&atilde;o...">

        </div>

    </div>

    <div class="form-group">

        <label for="numero_licitacao" class="col-sm-2 control-label">N&ordm; Licita&ccedil;&atilde;o</label>

        <div class="col-sm-10">

            <input type="text" name="numero_licitacao" id="numero_licitacao" class="form-control" value="<?= $_POST['numero_licitacao'] ?>" placeholder="Informe o n&uacute;mero da licita&ccedil;&atilde;o...">

        </div>

    </div>

    <div class="form-group">

        <label for="ano" class="col-sm-2 control-label">Ano</label>

        <div class="col-sm-10">

            <input type="text" name="ano" id="ano" class="form-control ano" value="<?= $_POST['ano'] ?>" placeholder="Informe o ano da licita&ccedil;&atilde;o...">

        </div>

    </div>

    <div class="form-group">

        <label for="objetoLicitacao" class="col-sm-2 control-label">Objeto</label>

        <div class="col-sm-10">

            <input type="text" name="objetoLicitacao" id="objetoLicitacao" class="form-control" value="<?= $_POST['objetoLicitacao'] ?>" placeholder="Informe um trecho do objeto da licita&ccedil;&atilde;o...">

        </div>

    </div>

    <div class="form-group">

        <div class="col-sm-offset-2 col-sm-10">

            <input type="hidden" name="acao" value="cadastrar">

            <button type="reset" class="btn btn-danger">Limpar</button>

            <button type="submit" class="btn btn-primary">Enviar</button>

        </div>

    </div>

</form>

<p class="clearfix"></p>

<div class="row">



    <div class="col-md-3">

        <?php

        if(count($qryModalidade)) {

            ?>

            <div class="list-group">

                <?php

                foreach ($qryModalidade as $modalidade) {



                    $stTotal = $conn->prepare("SELECT COUNT(id) AS total

											 FROM licitacao_edital

											WHERE id_cliente = :id_cliente

											  AND id_modalidade = :id_modalidade

											  AND status_registro = :status_registro");

                    $stTotal->execute(array("id_cliente" => $novoCliente, "id_modalidade" => $modalidade['id'], "status_registro" => "A"));

                    $totalModalidade = $stTotal->fetch();

                    ?>

                    <a class="list-group-item" href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$licitacaoLista.$complemento) ?>&nc=<?= $novoCliente ?>&id_modalidade=<?= $modalidade['id'] ?>">

                        <span class="badge"><?= $totalModalidade['total'] ?></span>

                        <?= $modalidade['descricao'] ?>

                    </a>

                <?php } ?>

            </div>

        <?php } ?>

    </div>



    <div class="col-md-9">

        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

            <p></p>

            <?php foreach ($qryProximaLicitacao as $licitacao) { ?>

                <div class="panel panel-default">

                    <div class="panel-heading" role="tab" id="heading_<?= $licitacao['id'] ?>">

                        <h4 class="panel-title">

                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?= $licitacao['id'] ?>" aria-expanded="true" aria-controls="collapse_<?= $licitacao['id'] ?>">

                                <div class="row">

                                    <div class="col-sm-4"><i class="glyphicon glyphicon-triangle-right"></i> <strong><?= $licitacao['titulo'] ?></strong></div>

                                    <div class="col-sm-4"><strong>Publica&ccedil;&atilde;o:</strong> <?= formata_data($licitacao['data_publicacao']) ?></div>

                                    <div class="col-sm-4"><strong>Abertura:</strong> <?= formata_data($licitacao['data_abertura']) ?><? if(!empty($licitacao['hora_abertura'])) echo " &agrave;s " . $licitacao['hora_abertura'] ?></div>

                                </div>

                            </a>

                        </h4>

                    </div>

                    <div id="collapse_<?= $licitacao['id'] ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_<?= $licitacao['id'] ?>">

                        <div class="panel-body">

                            <p><strong>Modalidade:</strong> <?= $licitacao['modalidade'] ?></p>

                            <? if($licitacao['valor_maximo'] != "") { ?><p><strong>Valor M&aacute;ximo:</strong> <?= $licitacao['valor_maximo'] ?></p><? } ?>

                            <p><strong>Objeto:</strong> <?= verifica($licitacao['objeto']) ?></p>



                            <?php

                            $stAnexo = $conn->prepare("SELECT * FROM licitacao_edital_anexo WHERE id_edital = :id_edital AND status_registro = :status_registro ORDER BY ordem ASC, id DESC");

                            $stAnexo->execute(array("id_edital" => $licitacao['id'], "status_registro" => "A"));

                            $qryAnexo = $stAnexo->fetchAll();



                            if(count($qryAnexo)) {

                                ?>

                                <div class="panel panel-primary">

                                    <div class="panel-heading">

                                        <h2 class="panel-title"><i class="glyphicon glyphicon-cloud-download"></i> Anexos</h2>

                                    </div>

                                    <div class="panel-body">

                                        <?php

                                        foreach ($qryAnexo as $anexo) {



                                            if($filtro == "B" || $_SESSION['login_licitacao'] == true) {

                                                ?>

                                                <p><strong><a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$licitacaoDownload.$complemento); ?>&nc=<?= $novoCliente ?>&id=<?= verifica($anexo['id']) ?>" class="text-danger"><i class="glyphicon glyphicon-cloud-download"></i> <?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?></a></strong></p>

                                                <?php

                                            } else if($configuracaoTransparencia['permitir_download_sem_cadastro'] == "S") {

                                                ?>

                                                <p><strong><a href="#" role="button" data-toggle="modal" data-target="#anexo_<?= $anexo['id'] ?>"><i class="glyphicon glyphicon-cloud-download"></i> <?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?></a></a></strong></p>

                                                <div class="modal fade" id="anexo_<?= $anexo['id'] ?>">

                                                    <div class="modal-dialog">

                                                        <div class="modal-content">

                                                            <div class="modal-header">

                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>

                                                                <h4 class="modal-title">Baixar <?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?></h4>

                                                            </div>

                                                            <div class="modal-body text-center">

                                                                <button type="button" class="btn btn-danger btn-margin" onclick="jQuery('#confirmacao_<?= $anexo['id'] ?>').removeClass('hidden'); return false;"><i class="glyphicon glyphicon-alert"></i> Download sem cadastro</a></button>

                                                                <a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$licitacaoLogin.$complemento); ?>&nc=<?= $novoCliente ?>&id=<?= verifica($anexo['id']) ?>" class="btn btn-danger btn-margin"><i class="glyphicon glyphicon-user"></i> Download com cadastro</a><br><br>

                                                                <div class="text-justify">

                                                                    <?php if(empty($configuracaoTransparencia['texto_download_sem_cadastro'])) { ?>

                                                                        O cadastro no sistema de licita&ccedil;&otilde;es garante o recebimento por e-mail de todas as informa&ccedil;&otilde;es pertinentes ao processo

                                                                                                                                                                                              licitat&oacute;rio, como atas, avisos de revoga&ccedil;&atilde;o, retifica&ccedil;&otilde;es ou cancelamento. Caso optar por baixar o edital sem cadastramento, a pessoa

                                                                                                                                                                                                                                                                                        f&iacute;sica ou jur&iacute;dica n&atilde;o receber&aacute; as informa&ccedil;&otilde;es atualizadas via e-mail.<br><br>

                                                                        &Eacute; de responsabilidade do fornecedor fazer acessos no site da licitante para verificar quaisquer altera&ccedil;&otilde;es/retifica&ccedil;&otilde;es nos editais de licita&ccedil;&atilde;o de seu interesse.<br><br>

                                                                        <label id="confirmacao_<?= $anexo['id'] ?>" class="hidden"><input type="checkbox" name="concordo_<?= $anexo['id'] ?>" id="concordo_<?= $anexo['id'] ?>" onclick="jQuery('#baixar_<?= $anexo['id'] ?>').toggle().removeClass('hidden');" /><strong>&nbsp;Li e concordo com o texto acima.</strong></label><br>

                                                                        <script>jQuery("#concordo_<?= $anexo['id'] ?>").prop("checked", false);</script>

                                                                        <?php

                                                                    } else {



                                                                        echo verifica($configuracaoTransparencia['texto_download_sem_cadastro']);



                                                                    }

                                                                    ?>

                                                                </div>

                                                                <a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$licitacaoDownload.$complemento); ?>&nc=<?= $novoCliente ?>&confirm=1&id=<?= verifica($anexo['id']) ?>" id="baixar_<?= $anexo['id'] ?>" class="btn btn-primary text-center hidden">

                                                                    <i class="glyphicon glyphicon-cloud-download"></i> Baixar anexo

                                                                </a>

                                                            </div>

                                                        </div>

                                                    </div>

                                                </div>

                                                <?php

                                            } else {

                                                ?>

                                                <p><strong><a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$licitacaoLogin.$complemento); ?>&nc=<?= $novoCliente ?>&id=<?= verifica($anexo['id']) ?>" class="text-danger"><i class="glyphicon glyphicon-cloud-download"></i> <?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?></a></strong></p>

                                            <?php } ?>

                                        <?php } ?>

                                    </div>

                                </div>

                            <?php } ?>

                        </div>

                    </div>

                </div>

            <?php } ?>

        </div>

    </div>

</div>

<p class="clearfix"></p>
<?php
$atualizacao = atualizacao("compras_licitacao/licitacao", $novoCliente, $conn);

if($atualizacao != "") {

    ?>

    <p class="text-right"><small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small></p>
<?php }