<h2>Processos Licitat&oacute;rios</h2>

<style type="text/css">
	.espacoFlexivel{
		display: flex;
	    align-items: center;
	    justify-content: space-between;
	}
	.espacoLinhas{
		margin-bottom: 10px;
	}
</style>

<?php
	$novoCliente = empty($_GET['nc']) ? $cliente : secure($_GET['nc']);

	if($novoCliente == "37") { 
?>

<p class="text-right" style="font-size: 14pt;"><small><strong><i class="glyphicon glyphicon-time"></i> Hor&aacute;rio de Atendimento: </strong> das 08:00 &agrave;s 11:30 e das 13:00 &agrave;s 17:30 de segunda a sexta</small></p>

<?php 
	} 
?>

<ol class="breadcrumb">
	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
	<li class="active">Processos Licitat&oacute;rios</li>
</ol>

<?php
	$stFiltro = $conn->prepare("SELECT tipo 
								  FROM licitacao_filtro 
								 WHERE id_cliente = :id_cliente 
								   AND status_registro = :status_registro 
							  ORDER BY id DESC 
							     LIMIT 1");

	$stFiltro->execute(array("id_cliente" => $novoCliente, "status_registro" => "A"));
	$qryFiltro = $stFiltro->fetch();
	$filtro = empty($qryFiltro['tipo']) ? "P" : $qryFiltro['tipo'];

	$stModalidade = $conn->prepare("SELECT * 
									  FROM licitacao_modalidade 
									 WHERE id 
									    IN (
									SELECT id_modalidade 
									  FROM licitacao_edital 
									 WHERE id_cliente = :id_cliente 
									   AND status_registro = :status_registro) 
									   AND status_registro = :status_registro 
								  ORDER BY descricao");

	$stModalidade->execute(array("id_cliente" => $novoCliente, "status_registro" => "A"));
	$qryModalidade = $stModalidade->fetchAll();
	$stProximaLicitacao = $conn->prepare("SELECT licitacao_edital.*, 
												 licitacao_modalidade.descricao modalidade
											FROM licitacao_edital, 
											     licitacao_modalidade
										   WHERE licitacao_edital.id_modalidade = licitacao_modalidade.id
											 AND licitacao_edital.id_cliente = :id_cliente
											 AND licitacao_edital.id_autarquia IS NULL
											 AND licitacao_edital.em_andamento = :em_andamento
											 AND licitacao_edital.status_registro = :status_registro
											 AND licitacao_edital.data_abertura > CURRENT_DATE()
										ORDER BY licitacao_edital.data_abertura ASC, 
												 licitacao_edital.hora_abertura ASC");

	$stProximaLicitacao->execute(array(":id_cliente" => $novoCliente, ":em_andamento" => "F", ":status_registro" => "A"));
	$qryProximaLicitacao = $stProximaLicitacao->fetchAll();
	$stLicitacaoRealizada = $conn->prepare("SELECT licitacao_edital.*, 
	 											   licitacao_modalidade.descricao modalidade
		        						      FROM licitacao_edital, licitacao_modalidade
		        						     WHERE licitacao_edital.id_modalidade = licitacao_modalidade.id
										       AND licitacao_edital.id_cliente = :id_cliente
											   AND licitacao_edital.id_autarquia IS NULL
											   AND licitacao_edital.em_andamento = :em_andamento
		        						       AND licitacao_edital.status_registro = :status_registro
		        						       AND licitacao_edital.data_abertura <= CURRENT_DATE()
									      ORDER BY licitacao_edital.data_abertura DESC, 
										           licitacao_edital.hora_abertura ASC 
											 LIMIT 15");

	$stLicitacaoRealizada->execute(array(":id_cliente" => $novoCliente, ":em_andamento" => "F", ":status_registro" => "A"));
	$qryLicitacaoRealizada = $stLicitacaoRealizada->fetchAll();

	if($_SESSION['login_licitacao']) {
?>

<p class="text-right">
	Voc&ecirc; est&aacute; logado como <strong><?=$_SESSION['razao_fornecedor'] ?></strong>&nbsp;
	<a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$licitacaoConta.$complemento) ?>&nc=<?= $novoCliente ?>" class="label label-primary">Acessar minha conta.</a>
	<a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$licitacaoConta.$complemento) ?>&nc=<?= $novoCliente ?>&acao=logout">
		<strong class="text-danger"><i class="glyphicon glyphicon-off"></i> Sair.</strong>
	</a>
</p>

<?php
	}

	if($novoCliente == "31") {
?>

<div class="well">Telefone e e-mail para contato: (44) 3236-1222 || licitacao@floresta.pr.gov.br</div>
	<div class="well">
		<h2>ATENÇÃO: As informações referente às licitações publicadas a partir 23/10/2018 devem ser consultadas no Portal da Transparência:
			<a style="color: #d22552 !important;" href="http://186.226.249.250:8090/portaltransparencia/licitacoes/" target="_blank">
			http://186.226.249.250:8090/portaltransparencia/licitacoes/ 
			</a>
		</h2>	
		</div>

<p class="text-center">
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/AVISO_E_EDITAL_DE_CADASTRO_FORNECEDOR.pdf" target="_blank">
		<img src="<?=$CAMINHOCMGERAL ?>/images/editalcadastrodefornecedores_floresta.png" class="imagem" />
	</a>
</p>

<?php
	}

	if($novoCliente == "39") {
?>

	<div class="well">
		<h2>Para acesso as Licitações na Íntega, acesse:
			<a style="color: #d22552 !important;" href="http://www.ingadigital.com.br/transparencia/index.php?id_cliente=39&sessao=17a2c90ff4li17" target="_blank">
			http://www.ingadigital.com.br/transparencia/index.php?id_cliente=39&sessao=17a2c90ff4li17
			</a>
			</h2>	
		</div>
<?php
	}


	if($novoCliente != "111") {
?>

<p>
	<a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$licitacaoLista.$complemento) ?>&nc=<?= $novoCliente ?>" class="label label-primary">CLIQUE AQUI</a> e veja a lista de todas as Licita&ccedil;&otilde;es ou clique no bot&atilde;o abaixo para realizar uma busca.
</p>

<?php 
	} 
	if($novoCliente == "111") { 
?>

<a href="https://e-gov.betha.com.br/transparencia/con_licitacoes.faces?mun=ttfC-RLThtImD58phibapFo1oP_l71oz" class="btn btn-success" target="_blank">
	</i>Extratos de Licita&ccedil;&otilde;es</a>

<a href="http://www.ingabyte.com.br/sistema/arquivos/111/160518153012_documentos_para_cadastro_de_fornecedores_pdf.pdf" class="btn btn-success" target="_blank">
	</i>Rela&ccedil;&atilde;o de documentos para Cadastro de Fornecedores</a>

<!--
<div class="panel panel-warning">
	<div class="panel-heading">
    	<h2 class="panel-title"><i class="glyphicon glyphicon-alert"></i> ATEN&Ccedil;&Atilde;O</h2>
  	</div>
	<div class="panel-body">
		PARA GARANTIR A COMPATIBILIDADE ENTRE O ARQUIVO DA PROPOSTA E O SISTEMA BETHA AUTOCOTA&Ccedil;&Atilde;O &Eacute; NECESS&Aacute;RIO ATUALIZAR O SISTEMA BETHA AUTO COTA&Ccedil;&Atilde;O PARA VERS&Atilde;O 2.019 OU SUPERIOR.<br>
		NO SITE DO MUNIC&Iacute;PIO ENCONTRA-SE DISPON&Iacute;VEL A VERS&Atilde;O 2.8 PARA DOWNLOAD.
	</div>
</div>
-->

<?php 
	}
	if($novoCliente == "12126") { 
?>

<p>
	<a href="http://177.92.23.218:7474/esportal/slclicitacao.load.logic" target="_blank"><b>INFORMATIVO:</b> O Poder Legislativo Municipal de Miraselva, mantém suas atividades administrativas centralizadas ao Poder Executivo Municipal, desta forma, as demais licitações públicas encontram-se no Portal de Transparência do Município de Miraselva.</a>
</p>

<?php 
	}
?>

<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#form-contrato" aria-expanded="false" aria-controls="form-contrato">
	<i class="glyphicon glyphicon-search"></i> Pesquisar Licita&ccedil;&atilde;o
</button>

<?php
	if($novoCliente == '11994'){
		echo 	"<a style=\"text-decoration:none;display:block;width:fit-content;\" class=\"button-mandaguacu-pdf\" target=\"_blank\" href=\"http://www.cmmandaguacu.pr.gov.br/images/100619084739_poportaria.pdf\">
					<button class=\"btn btn-danger\" style=\"display:block;margin-top:15px;\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Portaria n° 012/2019 - Diretrizes para procedimentos em Licitações\">
						Diretrizes para procedimentos em Licitações
					</button>
				</a>
				<style>
					@media only screen and (max-width: 348px){
						.button-mandaguacu-pdf button{
							font-size: 12px;
						}
					}
				</style>";
	}


?>

<?if($novoCliente == '12038'){?>
	<br>
	<br>
	<a class="center btn-primary" style="padding: 9px 12px; border-radius: 4px;" href="http://www.ingadigital.com.br/transparencia/index.php?sessao=b851fa2cfblib8">
		Licita&ccedil;&otilde;es na &Iacute;ntegra
	</a>
	<a class="center" style="margin-bottom: 35px" href="http://www.controlemunicipal.com.br/site/geral/licitacao/proposta_florida.exe" target="_blank">
		<img style="display: block;margin-left: auto;margin-right: auto" src="http://www.controlemunicipal.com.br/site/geral/images/proposta.jpg" class="imagem">
	</a>
<?}?>

<?php
	if($novoCliente == "12168"){
?>

	<a href="http://187.87.208.214/pronimtb/index.asp?acao=21&item=1" class="btn btn-default" target="_blank">
		<i class="glyphicon glyphicon-list-alt"></i> Processos Licitatórios Anteriores à 2018
	</a>
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/LC_KitProposta_519_01_00_000_xambre.zip" class="btn btn-default" target="_blank">
		<i class="glyphicon glyphicon-download-alt"></i> Download do Kit Proposta
	</a>

<?
}
	
	if($novoCliente == "12165"){
?>

<a href="http://200.150.82.50:8086/portaltransparencia/licitacoes" class="btn btn-default" target="_blank">
	<i class="glyphicon glyphicon-list-alt"></i> Processos Licitatórios Anteriores à Março de 2018
</a>
<a href="<?=$CAMINHOCMGERAL ?>/licitacao/propostas_cambara.exe" class="btn btn-default" target="_blank">
	<i class="glyphicon glyphicon-download-alt"></i> Download do Software Proposta
</a>

<?
	}

	if($novoCliente == "12186"){
?>

<a href="http://www.controlemunicipal.com.br/site/geral/licitacao/LobatoCompletoAutoCotacao2025.exe" class="btn btn-warning btn-lg" style="margin-left: 15em; margin-right: 5px;" target="_blank">
	<i class="glyphicon glyphicon-download-alt"></i> Download do Software Proposta
</a>
<a href="#" class="btn btn-info" target="_blank">
	<i class="glyphicon glyphicon-list-alt"></i> Manual de Ajuda
</a>

<?
	}

	if($novoCliente == "15"){
?>

	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/propostas_california.exe" class="btn btn-default" target="_blank">
	<i class="glyphicon glyphicon-download-alt"></i> Download do Software Proposta
</a>

<br>
<p align="center" class="text-center">ATENÇÃO!!! Os Arquivos Proposta.exe e comercial.xml devem ser salvos na mesma pasta.
</p>
<p align="center" class="text-center">ATENÇÃO!!! Renomeie o arquivo .xml para comercial.xml </p>

<?
} if($novoCliente == "12178"){
?>
	<a href="https://e-gov.betha.com.br/transparencia/con_licitacoes.faces?mun=UF--6hGTekZ8gkjsoE_Eyf52-ZMHm00i" class="btn btn-default" target="_blank">
		<i class="glyphicon glyphicon-list-alt"></i> Processos Licitatórios Anteriores à Maio de 2019
	</a>
<? } ?>

<div class="filtrodeano" style="margin-top:20px">
    <button class="btn <?= ($_POST['ano'] == '' )? 'btn-success' : 'btn-default' ?>" onclick="filtrar_form('')" style="width: 75px;">TODOS</button>
<?php
    $anos = $conn->prepare("SELECT distinct ano
                              FROM licitacao_edital, 
							       licitacao_modalidade
                             WHERE licitacao_edital.id_modalidade = licitacao_modalidade.id
                               AND licitacao_edital.id_cliente = :id_cliente
                               AND licitacao_edital.status_registro = :status_registro
                               AND (licitacao_edital.data_publicacao <= (NOW()))
                          ORDER BY licitacao_edital.data_abertura DESC, 
						           licitacao_edital.id DESC");

    $anos->execute(array("id_cliente" => $novoCliente, "status_registro" => "A"));
    $ano = $anos->fetchAll();
    foreach ($ano as $key => $valor) {
?>
        
    <button class="btn <?= ($_POST['ano'] == $valor['ano'] )? 'btn-success' : 'btn-default' ?>" onclick="filtrar_form('<?= $valor['ano']?>')" style="width: 75px;">
		<span><?= $valor['ano']?></span>
	</button>
        
<?php
    }   
?>
</div>
<div class="filtrodeano" style="margin-top:20px; display: flex; align-items: center; justify-content: center;">
	
	<?php
		if($cliente != '1027'){
			if($cliente == '11991'){
	?>
				<a id="a-covid-19-licitacao" href="http://170.79.63.245:10081/portaltransparencia/licitacoes-covid" target="_blank">
					<span style="font-size: 200%">Contrata&ccedil;&otilde;es COVID-19</span>
				</a>

				<style>
					#a-covid-19-licitacao{
						height: 54px;
						display: flex;
						justify-content: center;
						align-items: center;
						width: 500px;
						background-color: #d9534f;
						color: white;
					}

					#a-covid-19-licitacao:hover{
						text-decoration: none;
						background-color: #d43f3a;
						border-radius: 4px;
					}
				</style>

	<?php
			}else if($cliente == '53'){
	?>
			<a class="btn <?= ($_POST['id_categoria_emergencial'] == 3 )? 'btn-warning' : 'btn-danger' ?>" href="http://177.73.209.254:7474/transparencia/licitacoesCovid19" style="width: 500px;">
				<span style="font-size: 200%">Contrata&ccedil;&otilde;es COVID-19</span>
			</a>
	<?php
			}else{
	?>

		<button class="btn <?= ($_POST['id_categoria_emergencial'] == 3 )? 'btn-warning' : 'btn-danger' ?>" onclick="filtrar_form_covid('3')" style="width: 500px;">
			<span style="font-size: 200%">Contrata&ccedil;&otilde;es COVID-19</span>
		</button>

	<?php
			}
		}else{
	?>
			<a class="btn <?= ($_POST['id_categoria_emergencial'] == 3 )? 'btn-warning' : 'btn-danger' ?>" href="http://tupassicovid19.hsyssoftwares.com/" style="width: 500px;">
				<span style="font-size: 200%">Contrata&ccedil;&otilde;es COVID-19</span>
			</a>
	<?php 
		}
	?>
</div>
<script>
	function filtrar_form(campo){
		$('#ano').val(campo);
		$('#form-contrato').submit();
	}
	function filtrar_form_covid(campo){
		$('#id_categoria_emergencial').val(campo);
		$('#form-contrato').submit();
	}
</script>
<?php 
	if($novoCliente == "12088") { 
?>

<a href="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$licitacaoIntegra.$complemento); ?>&nc=<?= $novoCliente ?>" class="btn btn-success">
	<i class="glyphicon glyphicon-book"></i> Acesse as Licita&ccedil;&otilde;es na &Iacute;ntegra
</a>

<?php 
	} 
	if($novoCliente == "46") { 
?>

<br>
<a href="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$outrosDocumentos.$complemento); ?>&nc=<?= $novoCliente ?>" class="btn btn-success">
	<i class="glyphicon glyphicon-list"></i> Relat&oacute;rio de Licita&ccedil;&otilde;es de 2013 &agrave; 2015
</a>

<a href="<?=$CAMINHOCMGERAL ?>/licitacao/LC_KitProposta_517_01_34-000.zip" class="btn btn-default" target="_blank">
	<i class="glyphicon glyphicon-download-alt"></i> Download do Kit Proposta
</a>

<?php 
	} 
	if( $novoCliente == "1027") { 
?>

<a href="http://www.ingadigital.com.br/transparencia/index.php?sessao=0dd1f266c2dw0d" class="btn btn-success"><i class="glyphicon glyphicon-list"></i> Relat&oacute;rio de Licita&ccedil;&otilde;es de 2013 &agrave; 2015</a>
<a href="http://www.controlemunicipal.com.br/inga/sistema/arquivos/1027/CALENDARIO_ANUAL_PLANO_DE_COMPRAS_2020.pdf" target="_blank" class="btn btn-warning"><i class="glyphicon glyphicon-calendar"></i> Calend&aacute;rio de Compras</a>

<?php 
	} 
?>

<p class="clearfix"></p>

<form class="form-horizontal collapse" id="form-contrato" action="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$licitacaoLista.$complemento) ?>&nc=<?= $novoCliente ?>" method="post">
	<div class="form-group">
		<label for="autarquia" class="col-sm-2 control-label">Autarquia</label>
		<div class="col-sm-10">
			<select name="id_autarquia" id="autarquia" class="form-control">
				<option value="">Selecione a Autarquia</option>

<?php
	$qryAutarquia = $conn->prepare("SELECT id, 
										   autarquia 
									  FROM pref_autarquia 
									 WHERE id_cliente = :id_cliente 
									   AND status_registro = :status_registro 
								  ORDER BY autarquia ASC");

	$qryAutarquia->execute(array("id_cliente" => $novoCliente, "status_registro" => "A"));

	$autarquias = $qryAutarquia->fetchAll();
	foreach ($autarquias as $key => $autarquia) {
?>

				<option value="<?= $autarquia['id'] ?>" <?php if($_POST['id_autarquia'] == $autarquia['id']) echo "selected"; ?>><?= $autarquia['autarquia'] ?></option>

<?
	}
?>

			</select>
		</div>
	</div>
	<div class="form-group">
		<label for="id_modalidade" class="col-sm-2 control-label">Modalidade</label>
		<div class="col-sm-10">
			<select name="id_modalidade" id="id_modalidade" class="form-control">
				<option value="">Selecione a Modalidade</option>

<?php
	if(count($qryModalidade)) {
		foreach ($qryModalidade as $modalidade) {
			echo "<option value='$modalidade[id]' ";
			if($modalidade['id'] == $_REQUEST['id_modalidade']) 
				echo "selected";
			echo ">$modalidade[descricao]</option>";
		}
	}
?>

			</select>
		</div>
	</div>
	<div class="form-group">
		<label for="data_abertura" class="col-sm-2 control-label">Data de Abertura</label>
		<div class="col-sm-10">
			<input type="text" name="data_abertura" id="data_abertura" class="form-control data" value="<?= $_POST['data_abertura'] ?>" placeholder="Informe a data de abertura do edital...">
		</div>
	</div>

	<div class="form-group">
		<label for="situacao" class="col-sm-2 control-label">Situa&ccedil;&atilde;o</label>
		<div class="col-sm-10">
			<select name="situacao" id="situacao" class="form-control">
				<option value="">Selecione a Situa&ccedil;&atilde;o</option>
				<option value="A" <?php if($_REQUEST['situacao'] == "A") echo "selected"; ?>>Em Andamento</option>
				<option value="F" <?php if($_REQUEST['situacao'] == "F") echo "selected"; ?>>Finalizadas</option>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label for="data1" class="col-sm-2 control-label">Publicados a partir de</label>
		<div class="col-sm-10">
			<input type="text" name="data1" id="data1" class="form-control data" value="<?= $_POST['data1'] ?>" placeholder="Informe uma data inicial de publica&ccedil;&atilde;o...">
		</div>
	</div>

	<div class="form-group">
		<label for="data2" class="col-sm-2 control-label">Publicados at&eacute;</label>
		<div class="col-sm-10">
			<input type="text" name="data2" id="data2" class="form-control data" value="<?= $_POST['data2'] ?>" placeholder="Informe uma data final de publica&ccedil;&atilde;o...">
		</div>
	</div>

	<div class="form-group">
		<label for="numero_licitacao" class="col-sm-2 control-label">N&ordm; Licita&ccedil;&atilde;o</label>
		<div class="col-sm-10">
			<input type="text" name="numero_licitacao" id="numero_licitacao" class="form-control" value="<?= $_POST['numero_licitacao'] ?>" placeholder="Informe o n&uacute;mero da licita&ccedil;&atilde;o...">
		</div>
	</div>

	<div class="form-group">
		<label for="ano" class="col-sm-2 control-label">Ano</label>
		<div class="col-sm-10">
			<input type="text" name="ano" id="ano" class="form-control ano" value="<?= $_POST['ano'] ?>" placeholder="Informe o ano da licita&ccedil;&atilde;o...">
			<input type="hidden" name="id_categoria_emergencial" id="id_categoria_emergencial" value="<?= $_POST['id_categoria_emergencial'] ?>" class="form-control">
		</div>
	</div>

	<div class="form-group">
		<label for="objetoLicitacao" class="col-sm-2 control-label">Objeto</label>
		<div class="col-sm-10">
			<input type="text" name="objetoLicitacao" id="objetoLicitacao" class="form-control" value="<?= $_POST['objetoLicitacao'] ?>" placeholder="Informe um trecho do objeto da licita&ccedil;&atilde;o...">
		</div>
	</div>

	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<input type="hidden" name="acao" value="cadastrar">
			<button type="reset" class="btn btn-danger">Limpar</button>
			<button type="submit" class="btn btn-primary">Enviar</button>
		</div>
	</div>
</form>

<p class="clearfix"></p>

<?php 
	if($novoCliente == "43" || $novoCliente == "34" || $novoCliente == "11999" || $novoCliente == "53" || $novoCliente == "12064" || $novoCliente == "12046" || $novoCliente == "12141") { 
?>

<p class="text-center">
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/esproposta.exe" target="_blank"><img src="<?=$CAMINHOCMGERAL ?>/images/esproposta.jpg" class="imagem" /></a>
</p>

<?php 
	} 
	if($novoCliente == "12063") {
?>

<p class="text-center">
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/elotechproposta_carlopolis.exe" target="_blank"><img src="<?=$CAMINHOCMGERAL ?>/images/elotechproposta.jpg" class="imagem" /></a>
</p>

<?php 
	}
	if($novoCliente == "1015") { 
?>

<p class="text-center">
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/proposta_ivai.exe" target="_blank"><img src="<?=$CAMINHOCMGERAL ?>/images/esproposta.jpg" class="imagem" /></a>
</p>

<?php 
	} 
	if($novoCliente == "12056") { 
?>

<p class="text-center">
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/esProposta_imbituva.exe" target="_blank"><img src="<?=$CAMINHOCMGERAL ?>/images/kit_proposta.png" class="imagem" /></a>
</p>

<?php 
	}
	if($novoCliente == "18") { 
?>

<p class="text-center">
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/esProposta_rosario.exe" target="_blank"><img src="<?=$CAMINHOCMGERAL ?>/images/kit_proposta.png" class="imagem" /></a>
</p>

<?php 
	}
	if($novoCliente == "102") { 
?>

<p class="text-center">
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/102_lunardelli_proposta.exe" target="_blank"><img src="<?=$CAMINHOCMGERAL ?>/images/kit_proposta.png" class="imagem" /></a>
</p>

<?php 
	} 
	if($novoCliente == "50") { 
?>

<p class="text-center">
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/Propostas-Amapora.exe" target="_blank"><img src="<?=$CAMINHOCMGERAL ?>/images/kit_proposta.png" class="imagem" /></a>
</p>

<?php 
	} 
	if($novoCliente == "1025") { 
?>

<!--<p class="text-center">-->
<!--	<a style="font-weight: bold; border: solid 1px #dddddd; border-radius: 5px; padding: 10px; margin-bottom: 5px;" href="http://200.195.133.226:8080/portaltransparencia/" target="_blank">-->
<!--	CLIQUE AQUI PARA VISUALIZAR AS LICITAÇÕES PUBLICADAS A PARTIR DE 07/03/2017.-->
<!--	</a>-->
<!--</p>-->

<?php 
	} 
	if($novoCliente == "12113"){
?>

<style> 
	u { 
		text-decoration: underline; 
		color: #E08200; 
	} 
</style>

<p style="font-size: large; margin-bottom: 25px;">
	<strong>Obtenha o  CRC - Certificado de Registro Cadastral do Município de Ortigueira" acompanhando o link (<a href="<?=$CAMINHOCMGERAL ?>/licitacao/150118093047_edital_cadastramento_de_empresas_pdf.pdf"><span style="color: #0000FF;">clique aqui</span></a>).</strong>
</p>

<?
	}
	if($novoCliente == "1181" || $novoCliente == "12113" || $novoCliente == "111") { 
?>

<p class="text-center">
	<a href="http://download.betha.com.br/versoesdisp.jsp?s=33" target="_blank">
		<img style="width: 40%;" src="<?=$CAMINHOCMGERAL ?>/images/autocotacao_ourizona.png" class="imagem" />
	</a>
</p>

<?
	}
	if($novoCliente == "111") {
?>

	<p class="text-center">
		<a href="http://www.controlemunicipal.com.br/inga/sistema/arquivos/111/091018170421_manual_sistema_autocotacao_pdf.pdf" target="_blank">
			<img style="width: 15%;" src="<?=$CAMINHOCMGERAL ?>/images/manualCotacao.png" class="imagem" />
		</a>
	</p>

<?php 
	} 
	if($novoCliente == "65") { 
?>

<p align="center" class="text-center">
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/Proposta_araruna.exe" target="_blank"><img src="<?=$CAMINHOCMGERAL ?>/images/proposta.jpg" class="imagem" /></a>
    <br>ATENÇÃO!!! Os Arquivos Proposta.exe e comercial.xml devem ser salvos na mesma pasta.
</p>
<p align="center" class="text-center">ATENÇÃO!!! Renomeie o arquivo .xml para comercial.xml </p>

<?php 
	} 
	if($cliente == "12140") { 
?>

<p class="text-center">
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/proposta_santa_lucia.exe" target="_blank">
		<img src="<?=$CAMINHOCMGERAL ?>/images/proposta.jpg" class="imagem" />
	</a>
	<br>
</p>

</p>

<?php 
	} 
	if($novoCliente == "11944") { 
?>

<p class="text-center">
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/Edital_de_Cadasto_de_Fornecedor.doc" target="_blank">
		<img src="<?=$CAMINHOCMGERAL ?>/images/editalDeCadastroDeFornecedores.png" class="imagem" />
	</a>
	<a href="http://download.betha.com.br/versoesdisp.jsp?s=33" target="_blank">
		<img alt="Realize o download da &uacute;ltima vers&atilde;o" src="<?=$CAMINHOCMGERAL ?>/images/download_betha.png" class="imagem" />
	</a>
</p>


<?php 
	} 
	if($novoCliente == "13") { 
?>

<p class="text-center">
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/sofware_querenciadonorte.exe" target="_blank">
		<img alt="Realize o download do software Proposta" src="<?=$CAMINHOCMGERAL ?>/images/proposta_querenciadonorte.png" class="imagem" />
	</a>
</p>

<?php 
	} 
	if($novoCliente == "19") { 
?>

<br>
<div style="text-align: center; font-weight: bold; color: #808080;">
    <p style="font-size: 16px;">
        <span style="color: #f00; font-size: 26px; font-weight:bold; ">ATEN&Ccedil;&Atilde;O!</span>
        <br>
        As informa&ccedil;&otilde;es referentes &agrave;s licita&ccedil;&otilde;es publicadas a partir do dia 19/06/2018, devem ser consultadas no novo Portal da Transpar&ecirc;ncia, 
        <a style="color: #0027ff;" href="http://200.150.98.187:8090/portaltransparencia/licitacoes">clicando aqui.</a>
    </p>
	<br>
</div>

<?php 
	}
	if($novoCliente == "12000" || $novoCliente == "19") { 
?>

<p class="text-center">
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/propostas.exe" target="_blank">
		<img src="<?=$CAMINHOCMGERAL ?>/images/proposta.jpg" class="imagem" />
	</a>
</p>

<?php 
	}
	if($novoCliente == "1027") { 
?>

<p class="text-center">
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/LC_KitProposta_519_01_00_001.msi" target="_blank">
		<img src="<?=$CAMINHOCMGERAL ?>/images/kit_proposta.png" class="imagem" />
	</a>
</p>

<?
	}
if($novoCliente == "60"){ ?>
<p class="text-center">
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/LC_KitProposta_assis.zip" target="_blank">
		<img src="<?=$CAMINHOCMGERAL ?>/images/kit_proposta.png" class="imagem" />
	</a>
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/ANEXO_MODELO_DECLARACAO_DE_NEPOTISMO.pdf" target="_blank">
		<img src="<?=$CAMINHOCMGERAL ?>/images/nepotismo.png" class="imagem" />
	</a>
</p>
<? } ?>

<?php 
	if($novoCliente == "1040") { 
?>

<p class="text-center">
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/LC_KitProposta_519_01_00_000_1040.msi" target="_blank">
		<img src="<?=$CAMINHOCMGERAL ?>/images/kit_proposta.png" class="imagem" />
	</a>
</p>

<?php 
	}
	if($novoCliente == "12087") { 
?>

<p class="text-center">
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/LC_KitProposta_519_01_00_000_spi.rar">
		<img src="<?=$CAMINHOCMGERAL ?>/images/proposta.jpg" class="imagem" />
	</a>  &nbsp; 
	<a href="http://www.ingadigital.com.br/transparencia/?id_cliente=12087&sessao=af090d2a04ldaf&id=2275593#form-fornecedor">
		<img src="<?=$CAMINHOCMGERAL ?>/images/cad_fornecedor.jpg" class="imagem" />
	</a>
</p>

<?php 
	}
	if($novoCliente == "12066") { 
?>

<p class="text-center">
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/LC_KitProposta_519_01_00_000_perobal.msi">
		<img src="<?=$CAMINHOCMGERAL ?>/images/proposta.jpg" class="imagem" />
	</a>
</p>

<?php 
	} 
	if($novoCliente == "1032") { 
?>

<p class="text-center">
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/LC_KitProposta_516_01_06-002.msi">
		<img src="<?=$CAMINHOCMGERAL ?>/images/proposta.jpg" class="imagem" />
	</a>
</p>

<?php 
	}
	if($novoCliente == "37") { 
?>

<!-- <p class="text-center"><a href="<?=$CAMINHOCMGERAL ?>/licitacao/LC_KitProposta_516_01_06-002.rar" target="_blank"><img src="<?=$CAMINHOCMGERAL ?>/images/kit_proposta.png" class="imagem" /></a></p> -->
<a href="<?=$CAMINHOCMGERAL ?>/licitacao/LC_KitProposta_517_01_34-000.msi" target="_blank" style="text-decoration= none;">
	<p class="text-center" style="font-weight: 600; font-size: 18px; color: #337ab7;" >BAIXE AQUI O PROGRAMA PARA PREENCHIMENTO DA PROPOSTA 
		<i class="glyphicon glyphicon-hand-right" style="font-size: 20px;"></i>
		<img src="<?=$CAMINHOCMGERAL ?>/images/kit_proposta.png" class="imagem" />
	</p>
</a>

<a href="<?=$CAMINHOCMGERAL ?>/licitacao/manual__kit_proposta_SANTO_ANTONIO_PLATINA.pdf" target="_blank" style="text-decoration= none;">
	<p class="text-center" style="font-weight: 600; font-size: 18px; color: #337ab7;" >BAIXE AQUI O MANUAL DO PARA PREENCHIMENTO DA PROPOSTA 
		<i class="glyphicon glyphicon-hand-right" style="font-size: 20px;"></i>
		<img src="<?=$CAMINHOCMGERAL ?>/images/manual_cadastro.png" class="imagem" />
	</p>
</a>

<?php 
	}
	if($novoCliente == "11973") { 
?>

<p class="text-center" style="font-weight: 600; font-size: 16px; color: #337ab7;" >BAIXE O KIT PARA PREENCHIMENTO DA PROPOSTA <i class="glyphicon glyphicon-hand-right" style="font-size: 20px;"></i>&nbsp;
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/Kit_Proposta_Pitangueiras.rar" target="_blank" style="text-decoration= none;">
		<img src="<?=$CAMINHOCMGERAL ?>/images/kit_proposta.png" class="imagem" />
	</a>
</p>

<?php 
	} 
	if($novoCliente == "123") { 
?>

<!-- <p class="text-center" style="font-weight: bold;"><a href="<?=$CAMINHOCMGERAL ?>/licitacao/LC_KitProposta_516_01_02-001.msi" target="_blank"> -->
<p class="text-center" style="font-weight: bold;">
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/AVISO_PARA_PARTICIPAÇÃO_DE_LICITAÇÃO.pdf" target="_blank" style="margin-right:30px;">Aviso para participação de licitação</a>
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/LC_KitProposta_JT.msi" target="_blank">Baixe aqui o arquivo da Proposta!!!</a>
</p>

<?php 
	}
	if($novoCliente == "11928") { 
?>

<p class="text-center" style="font-weight: bold; padding: 10px; border: solid 1px #dddddd; width: 300px; text-align: center; margin: auto;">
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/LC_KitProposta_519_01_00_001_perola.msi" target="_blank">Baixe aqui o programa do Kit Proposta!!</a>
</p>

<?php 
	}
	if($novoCliente == "1179") { 
?>

<p class="text-center" style="font-weight: bold; padding: 10px; border: solid 1px #dddddd; width: 300px; text-align: center; margin: auto;">
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/LC_KitProposta_519_01_00_000.rar" target="_blank">
		<img src="<?=$CAMINHOCMGERAL ?>/images/proposta.jpg" class="imagem" />
	</a>
</p>

<?php 
	} 
	if($novoCliente == "1190") { 
?>

<p class="text-center">
	<a style="margin-right: 120px;" href="http://download.betha.com.br/versoesdisp.jsp?s=33&rdn=090114151112" target="_blank">
		<img src="<?=$CAMINHOCMGERAL ?>/images/betha-novo.png" style="height: 78px;" class="imagem" class="imagem" />
	</a><!--Baixe aqui o arquivo de Auto Cota&ccedil;&atilde;o!!! -->
</p>

<?php 
	}
	if($novoCliente == "12064" || $novoCliente == "12141") { 
?>

<p class="text-center">
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/manual_esproposta.pdf" target="_blank">
		<img src="<?=$CAMINHOCMGERAL ?>/images/manual_esproposta.jpg" class="imagem" />
	</a>
</p>

<?php 
	}
	if($novoCliente == '12083') { 
?>

<p class="text-center">
	<a style="margin-right: 8px;" href="<?=$CAMINHOCMGERAL ?>/licitacao/manual_cadastro.pdf" target="_blank">
		<img src="<?=$CAMINHOCMGERAL ?>/images/manual_cadastro.png" class="imagem" />
	</a>
	<a style="margin-right: 8px;" href="<?=$CAMINHOCMGERAL ?>/licitacao/esproposta.exe" target="_blank">
		<img src="<?=$CAMINHOCMGERAL ?>/images/esproposta.jpg" class="imagem" />
	</a>
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/manual_esproposta.pdf" target="_blank">
		<img src="<?=$CAMINHOCMGERAL ?>/images/manual_esproposta.jpg" class="imagem" />
	</a>
</p>

<?php 
	}
	if($novoCliente == "138") { 
?>

<p class="text-center">
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/esproposta_fernandes_pinheiro.exe" target="_blank">
		<img src="<?=$CAMINHOCMGERAL ?>/images/esproposta.jpg" class="imagem" />
	</a>
</p>

<p class="text-center">
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/Manual_Para_Digitacao_das_Propostas.pdf" target="_blank">
		<img src="<?=$CAMINHOCMGERAL ?>/images/manual_digitacao_proposta.jpg" class="imagem" />
	</a>
</p>

<?php 
	}
	if($novoCliente == "48") { 
?>

<p class="text-center">
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/esproposta_iguatu.exe" target="_blank">
		<img src="<?=$CAMINHOCMGERAL ?>/images/esproposta.jpg" class="imagem" />
	</a>
</p>

<?php 
	}
	if($novoCliente == "12074") { 
?>
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      	<div class="modal-content">
        	<div class="modal-header"style="background-color: #0064C9;color: white;text-align: center;">
          		<button type="button" class="close" data-dismiss="modal">&times;</button>
          		<h4 class="modal-title">Sala de licitações da Prefeitura de Ivaiporã-PR</h4>
        	</div>
        	<div class="modal-body">
				<p style="text-align: justify;margin: .1em;">
					<iframe src="http://zoevideos.net/player/aovivo/707" width="570px" style="height: 324px;"></iframe>
				</p>
        	</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
			</div>
      	</div> 
    </div>
</div>

<p class="text-center">
	<a style="margin-right: 8px;" href="<?=$CAMINHOCMGERAL ?>/licitacao/proposta_ivaipora.exe" target="_blank">
		<img src="<?=$CAMINHOCMGERAL ?>/images/proposta.jpg" class="imagem" />
	</a>
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/formulario_cadastro_ivaipora.doc" target="_blank">
		<img src="<?=$CAMINHOCMGERAL ?>/images/formulario_cadastro.png" class="imagem" />
	</a>
	<!-- <a href="http://zoevideos.net/player/aovivo/707" target="_blank">
		<img src="http://www.controlemunicipal.com.br/site/geral/images/licitacao_ivaipora.jpg" class="imagem" />
	</a> -->
	<a data-toggle="modal" data-target="#myModal" target="_blank">
		<img src="http://www.controlemunicipal.com.br/site/geral/images/licitacao_ivaipora.jpg">
	</a>
	
</p>



<?php 
	}
	if($novoCliente == "1180") { 
?>
	<!-- <div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog">
			  <div class="modal-content">
				<div class="modal-header"style="background-color: #0064C9;color: white;text-align: center;">
					  <button type="button" class="close" data-dismiss="modal">&times;</button>
					  <h4 class="modal-title">Sala de Licitações da Câmara de Doutor Camargo</h4>
				</div>
				<div class="modal-body">
					<p style="text-align: justify;margin: .1em;">
						<iframe src="http://zoevideos.net/player/aovivo/1236" width="570px" style="height: 324px;"></iframe>
						</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
				</div>
			  </div> 
		</div>
	</div>
		
	<p class="text-center">
		<button class="btn"  style="width: 300px;" data-toggle="modal" data-target="#myModal" target="_blank">
			<span style="font-size: 170%">Licitação AO VIVO</span>
		</button>
	</p> -->
		
<?php 
	}

	if($novoCliente == "109") { 
?>

<p class="text-center">
	<!-- <a style="margin-right: 8px;" href="<?=$CAMINHOCMGERAL ?>/licitacao/programa_cotacao.exe" target="_blank"><img src="<?=$CAMINHOCMGERAL ?>/images/cotacao_preco_turvo.jpg" class="imagem" /></a> -->
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/esProposta_turvo.exe" target="_blank">
		<img src="<?=$CAMINHOCMGERAL ?>/images/proposta_preco_turvo.jpg" class="imagem" />
	</a>
	<a href="http://www.ingadigital.com.br/transparencia/?id_client=109&sessao=cadfae5cf4inca" target="_blank">
		<img src="<?=$CAMINHOCMGERAL ?>/images/botao_normativas_turvo.png" class="imagem" />
	</a>
</p>

<?php 
	}
	if($novoCliente == "1093") { 
?>

<p class="text-center">
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/esProposta2016candidodeabreu.exe" target="_blank">
		<img src="<?=$CAMINHOCMGERAL ?>/images/esproposta.jpg" class="imagem" />
	</a>
</p>

<?php 
	} 
	if($novoCliente == "11969") { 
?>

<p class="text-center">
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/proposta_cmguapirama.exe" target="_blank">
		<img src="<?=$CAMINHOCMGERAL ?>/images/esproposta.jpg" class="imagem" />
	</a>
</p>

<?php 
	} 
	if($novoCliente == "137") { 
?>

<p class="text-center">
	<a style="margin-right: 8px;" href="<?=$CAMINHOCMGERAL ?>/licitacao/Propostas.exe" target="_blank">
		<img src="<?=$CAMINHOCMGERAL ?>/images/proposta.jpg" class="imagem" />
	</a>
	<a href="http://186.233.207.3:7474/esportal/slclicitacao.load.logic" target="_blank">
		<img src="<?=$CAMINHOCMGERAL ?>/images/aviso_pitanga.png" class="imagem" />
	</a>
</p>

<?php 
	}
	if($novoCliente == "12145") { 
?>

<p class="text-center">
	<a style="margin-right: 8px;" href="<?=$CAMINHOCMGERAL ?>/licitacao/proposta_cafezal_do_sul.exe" target="_blank">
		<img src="<?=$CAMINHOCMGERAL ?>/images/proposta.jpg" class="imagem" />
	</a>
</p>

<?php 
	}
	if($novoCliente == "11924") { 
?>

<p class="text-center">
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/proposta_catanduvas.zip" target="_blank">
		<img alt="Baixe o Software Proposta" src="<?=$CAMINHOCMGERAL ?>/images/aviso_catanduvas.png" class="imagem" />
	</a>
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/100217104714_manual_kit_pdf.pdf" target="_blank">
		<img alt="Manual Kit Proposta" src="<?=$CAMINHOCMGERAL ?>/images/manual_proposta_catanduvas.jpg" class="imagem" />
	</a>
</p>

<?php 
	}
	if($novoCliente == "1046") { 
?>

<p class="text-center">
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/LC_KitProposta_519_01_00_000_altoparaiso.zip" target="_blank">
		<img alt="Baixe o Software Proposta" src="<?=$CAMINHOCMGERAL ?>/images/software_proposta.png" width="150px" class="imagem"/>
	</a>
	
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/manual_alto_paraiso.pdf" target="_blank">
		<img alt="Manual Kit Proposta" src="<?=$CAMINHOCMGERAL ?>/images/manual_proposta.png" width="150px" class="imagem"/>
	</a>
</p>

<?php 
	}
	if($novoCliente == "12210") { 
?>

	<p class="text-center">
		<a href="<?=$CAMINHOCMGERAL ?>/licitacao/esProposta_Guairaca.zip" target="_blank">
			<img alt="Baixe o Software Proposta" src="<?=$CAMINHOCMGERAL ?>/images/software_proposta.png" width="150px" class="imagem"/>
		</a>
	</p>

<?php
	}
	if($novoCliente == "12102") {
?>
		<p class="text-center">
				<a href="http://www.controlemunicipal.com.br/site/geral/licitacao/bethaautocotacao2025.exe" target="_blank">
					<img alt="Baixe o Software Proposta" src="http://www.controlemunicipal.com.br/site/prefeitura/images/uniflor_autocotacao.jpg" width="150px" class="imagem"/>
				</a>
			</a>
		</p>

<?php 
	}
	if($novoCliente == "1058") { 
?>

<a href="http://187.109.199.130:8080/portaltransparencia/licitacoes" class="btn btn-primary" target="_blank">
	<i class="glyphicon glyphicon-list-alt"></i> Processos Licitatórios Online
</a>

<a href="http://177.125.215.99:9001/" class="btn btn-danger" target="_blank">
	<i class="glyphicon glyphicon-list-alt"></i> Licita&ccedil;&otilde;es na &Iacute;ntegra
</a>

<p class="text-center">
    <a href="<?=$CAMINHOCMGERAL ?>/licitacao/Manual_Software_Proposta_Marialva.pdf" target="_blank">
        <img alt="Manual Kit Proposta" src="<?=$CAMINHOCMGERAL ?>/images/manual_proposta.png" width="150px" class="imagem" />
    </a>
    <a href="<?=$CAMINHOCMGERAL ?>/licitacao/Proposta_Marialva.exe" target="_blank">
        <img alt="Baixe o Software Proposta" src="<?=$CAMINHOCMGERAL ?>/images/software_proposta.png" width="150px" class="imagem" />
    </a>
</p>

<?php 
	} 
	if($novoCliente == "12152" ) { 
?>

<p class="text-center">
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/proposta_catanduvas.rar" target="_blank">
		<img alt="Baixe o Software Proposta" src="<?=$CAMINHOCMGERAL ?>/images/aviso_catanduvas.png" class="imagem" />
	</a>
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/100217104714_manual_kit_pdf.pdf" target="_blank">
		<img alt="Manual Kit Proposta" src="<?=$CAMINHOCMGERAL ?>/images/manual_proposta_catanduvas.jpg" class="imagem" />
	</a>
</p>

<?php 
	}
	if($novoCliente == "12159") { 
?>

<p class="text-center">
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/proposta_itaunadosul.zip" target="_blank">
		<img alt="Baixe o Software Proposta" src="<?=$CAMINHOCMGERAL ?>/images/aviso_catanduvas.png" class="imagem" />
	</a>
</p>

<?php 
	}
	if($novoCliente == "41") { 
?>

<p class="text-center">
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/proposta_sao_jorge_patrocinio.msi" target="_blank">
		<img alt="Baixe o Software Proposta" src="<?=$CAMINHOCMGERAL ?>/images/aviso_catanduvas.png" class="imagem" />
	</a>
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/manual_kit_proposta_sao_jorge_patrocinio.pdf" target="_blank">
		<img alt="Manual Kit Proposta" src="<?=$CAMINHOCMGERAL ?>/images/manual_proposta_catanduvas.jpg" class="imagem" />
	</a>
</p>

<?php 
	}
	if($novoCliente == "11982") { 
?>

<p class="text-center">
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/mediador.exe" target="_blank">
		<img src="<?=$CAMINHOCMGERAL ?>/images/mediador.jpg" class="imagem" />
	</a>
</p>

<?php 
	}
	if($novoCliente == "17") { 
?>

<p class="text-center">
	<a href="http://www.controlemunicipal.com.br/inga/sistema/arquivos/17/InstaladorASPD1.00.13.zip" target="_blank">
		<img src="<?=$CAMINHOCMGERAL ?>/images/proposta.jpg" class="imagem" />
	</a>
</p>

<?php 
	}
	if($novoCliente == "128") { 
?>

<p class="text-center">
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/esproposta_imbau.exe" target="_blank">
		<img src="<?=$CAMINHOCMGERAL ?>/images/esproposta.jpg" class="imagem" />
	</a>
</p>

<?php 
	}
	if($novoCliente == "104") { 
?>

<p class="text-center">
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/Propostas_fenix.exe" target="_blank">
		<img src="<?=$CAMINHOCMGERAL ?>/images/esproposta.jpg" class="imagem" />
	</a>
</p>

<span class="text-danger text-center" style="display: block;">ATEN&Ccedil;&Atilde;O!!!  Os Arquivos  Propostas_fenix.exe e comercial.xml devem ser salvos na mesma pasta.</span><br />
<span class="text-danger text-center" style="display: block;">ATEN&Ccedil;&Atilde;O!!!  Renomeie o arquivo .xml para comercial.xml.</span>

<?php 
	}
	if($novoCliente == "59") { 
?>

<p class="text-center">
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/sjivai_proposta.exe" target="_blank">
		<img src="<?=$CAMINHOCMGERAL ?>/images/proposta.jpg" class="imagem" />
	</a>
</p>

<?php 
	}
	if($novoCliente == "12080") { 
?>

<p class="text-center">
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/proposta_cambira.exe" target="_blank"><img src="<?=$CAMINHOCMGERAL ?>/images/proposta.jpg" class="imagem" /></a>
	<a style="padding: 17px 10px 20px 10px;  border: solid 1px #cccccc; font-weight: bold;" href="https://cambira.atende.net/" target="_blank">
		Acesse toda tramitass&atilde;o dos processos licitat&oacute;rios
	</a>
	<br>
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/manual_cambira.pdf" target="_blank">Veja aqui o manual da proposta!</a><br>
	<span class="text-danger">ATEN&Ccedil;&Atilde;O!!!  Os Arquivos  proposta_cambira.exe e comercial.xml devem ser salvos na mesma pasta.</span>
</p>

<?php 
	}
	if($novoCliente == "26") { 
?>

<p class="text-center">
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/proposta_doutor_camargo.exe" target="_blank"><img src="<?=$CAMINHOCMGERAL ?>/images/proposta.jpg" class="imagem" /></a><br>
	<span class="text-danger">ATEN&Ccedil;&Atilde;O!!!  Os Arquivos  proposta_doutor_camargo.exe e comercial.xml devem ser salvos na mesma pasta.</span>
</p>

<?php 
	}
	if($novoCliente == "1065") { 
?>

<p class="text-center">
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/proposta_iretama.exe" target="_blank"><img src="<?=$CAMINHOCMGERAL ?>/images/proposta.jpg" class="imagem" /></a><br>
	<span class="text-danger">ATEN&Ccedil;&Atilde;O!!!  Os Arquivos  proposta_iretama.exe e comercial.xml devem ser salvos na mesma pasta.</span>
</p>

<?php 
	}
	if($novoCliente == "12103") { 
?>

<p class="text-center">
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/propostas_angulo.exe" target="_blank"><img src="<?=$CAMINHOCMGERAL ?>/images/proposta.jpg" class="imagem" /></a><br>
	<span class="text-danger">ATEN&Ccedil;&Atilde;O!!!  O arquivo .xml DEVE ser renomeado para: comercial <br> Os Arquivos  propostas_angulo.exe e comercial.xml devem ser salvos na mesma pasta.</span>
</p>

<?php 
	}
	if($novoCliente == "28") { 
?>

<p class="text-center">
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/proposta_barbosa_ferraz.exe" target="_blank"><img src="<?=$CAMINHOCMGERAL ?>/images/proposta.jpg" class="imagem" /></a><br>
	<span class="text-danger">ATEN&Ccedil;&Atilde;O!!!  Os Arquivos  proposta_barbosa_ferraz.exe e comercial.xml devem ser salvos na mesma pasta.</span><br />
	<span class="text-danger">ATEN&Ccedil;&Atilde;O!!!  Renomeie o arquivo .xml para comercial.xml.</span>
</p>

<?php 
	}
	if($novoCliente == "1110") { 
?>

<p class="text-center">
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/proposta_sabaudia.exe" target="_blank"><img src="<?=$CAMINHOCMGERAL ?>/images/proposta.jpg" class="imagem" /></a><br>
	<span class="text-danger">ATEN&Ccedil;&Atilde;O!!!  Os Arquivos  proposta_sabaudia.exe e comercial.xml devem ser salvos na mesma pasta.</span>
</p>

<?php 
	}
	if($novoCliente == "11933") { 
?>

<p class="text-center">
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/proposta_paranacity.exe" target="_blank"><img src="<?=$CAMINHOCMGERAL ?>/images/proposta.jpg" class="imagem" /></a><br>
	<span class="text-danger">ATEN&Ccedil;&Atilde;O!!!  Os Arquivos  proposta_paranacity.exe e comercial.xml devem ser salvos na mesma pasta.</span>
</p>

<?php 
	}
	if($novoCliente == "1163") { 
?>

<p class="text-center">
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/proposta_quatigua.exe" target="_blank"><img src="<?=$CAMINHOCMGERAL ?>/images/proposta.jpg" class="imagem" /></a><br>
	<span class="text-danger">ATEN&Ccedil;&Atilde;O!!!  Os Arquivos  proposta_quatigua.exe e comercial.xml devem ser salvos na mesma pasta.</span>
</p>

<?php 
	}
	if($novoCliente == "11986") { 
?>

<p class="text-center">
	<a href="http://download.betha.com.br/versoesdisp.jsp?s=33" target="_blank">
		<img src="http://www.controlemunicipal.com.br/site/geral/images/autocotacao_paranavai.png" class="imagem" />
	</a><br><br>
	<a style="padding: 10px 77px 10px 77px; border: solid 1px #dddddd; font-weight: bold; margin-top: 10px;" href="https://www.adrive.com/public/BSd7PF/Licita%C3%A7%C3%B5es%20na%20Integra">
		Licita&ccedil;&otilde;es na &Iacute;ntegra
	</a>
</p>

<?php 
	}
	if($novoCliente == "12101") { 
?>

<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      	<div class="modal-content">
        	<div class="modal-header"style="background-color: #0064C9;color: white;text-align: center;">
          		<button type="button" class="close" data-dismiss="modal">&times;</button>
          		<h4 class="modal-title">Sala de licitações da Prefeitura de Paranavaí-PR</h4>
        	</div>
        	<div class="modal-body">
          		<h4>Visualise aqui o<a href="http://131.255.52.36:8080/"> - Link da Sala - </a></h4>
				<span>Usuário: licit <br>Senha: licit</span>
				<h4>Selecionar as camera 01 e 03</h4>
				<p style="text-align: justify;margin: .1em;">ATENÇÃO:  visualização somente no navegador Microsoft Internet Explorer, 
					caso apresentar problemas na visualização verificar o video abaixo com a explicação dos procedimentos a serem adotados para correta visualização.
					Segurança | Como configurar o Internet Explorer para acessar o DVR Intelbras 
					<a href="https://www.youtube.com/watch?v=xofEtxvrLsI">- Link do Video -</a>
				</p>
        	</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
			</div>
      	</div> 
    </div>
</div>

<p class="text-center">
	<a data-toggle="modal" data-target="#myModal" target="_blank">
		<img src="<?=$CAMINHOCMGERAL ?>/images/banner_licitacoes.png" class="imagem" style="width: 25%; cursor: pointer;" />
	</a>
</p>
<p class="text-center">
	<a href="http://download.betha.com.br/versoesdisp.jsp?s=33" target="_blank">
		<img src="<?=$CAMINHOCMGERAL ?>/images/autocotacao_paranavai.png" class="imagem" />
	</a>
</p>

<?php 
	}
	if($novoCliente == "12113") { 
?>

<style> 
	u { 
		text-decoration: underline; 
		color: #E08200; 
	} 
</style>

<p style="font-size: large">
	<strong>Comunicado Importante: a partir de 01/10/2016 este departamento n&atilde;o utilizar&aacute; mais o e-mail <u>licita.ortigueira@gmail.com</u> logo, o e-mail oficial/institucional ser&aacute; <u>licitacao@ortigueira.pr.gov.br</u> assim, toda a comunica&ccedil;&atilde;o via e-mail dever&aacute; ser obrigatoriamente no e-mail: <u>licitacao@ortigueira.pr.gov.br</u> para tanto, este &oacute;rg&atilde;o n&atilde;o ser&aacute; respons&aacute;vel caso ocorram desencontro de informa&ccedil;&otilde;es entre os e-mails informados conforme datas citadas.</strong>
</p>

<!-- <p class="text-center">
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/Completo-AutoCotacao-2022.exe" target="_blank"><img src="<?=$CAMINHOCMGERAL ?>/images/proposta.jpg" class="imagem" /></a>
</p> -->

<?php 
	} 
?>

<p class="clearfix"></p>
<div class="row">
	<div class="col-md-3">

<?php
	if(count($qryModalidade)) {
?>

		<div class="list-group">

<?php
		foreach ($qryModalidade as $modalidade) {
			if($_POST['ano'] == ''){
				$sql = "SELECT COUNT(id) AS total
						  FROM licitacao_edital
						 WHERE id_cliente = :id_cliente
						   AND id_modalidade = :id_modalidade
						   AND status_registro = :status_registro";
													
				$stTotal = $conn->prepare($sql);
				$stTotal->execute(array("id_cliente" => $novoCliente, "id_modalidade" => $modalidade['id'], "status_registro" => "A"));
				$totalModalidade = $stTotal->fetch();
			}else{
			
				$sql = "SELECT COUNT(id) AS total
						  FROM licitacao_edital
						 WHERE id_cliente = :id_cliente
						   AND id_modalidade = :id_modalidade
						   AND status_registro = :status_registro
						   AND ano = :ano";
									
				$stTotal = $conn->prepare($sql);
				$data = getdate();
				$ano_pesquisa = ($_POST['ano'] != '') ? secure($_POST['ano']) : $data['year'];
				$stTotal->execute(array("id_cliente" => $novoCliente, "id_modalidade" => $modalidade['id'], "status_registro" => "A", "ano" => $ano_pesquisa ));
				$totalModalidade = $stTotal->fetch();
			}
?>

			<a class="list-group-item" href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$licitacaoLista.$complemento) ?>&nc=<?= $novoCliente ?>&id_modalidade=<?= $modalidade['id'] ?>">
				<span class="badge"><?= $totalModalidade['total'] ?></span>	<?= $modalidade['descricao'] ?>
			</a>

<?php 
		} 
?>

		</div>

<?php 
	} 
	if($novoCliente != "12050" && $novoCliente != "31"){
?>

		<div class="col-md-12">
            <a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$licitacaoCadastro.$complemento) ?>" role="button" class="btn btn-primary btn-lg" data-toggle="collapse"
            	data-target="#form-fornecedor" aria-expanded="false" aria-controls="form-fornecedor">
                <i class="glyphicon glyphicon-plus-sign"></i> Efetuar Cadastro
            </a>
        </div>

        <div class="col-md-12" style="margin-top: 20px;">
            <a href="http://www.ingadigital.com.br/transparencia/index.php?sessao=0129e125c7ld01&nc=<?=$cliente?>" role="button" class="col-xs-12 btn btn-success btn-lg" data-toggle="collapse"
               data-target="#form-fornecedor" aria-expanded="false" aria-controls="form-fornecedor">
                <i class="glyphicon glyphicon-user"></i> Efetuar Login
            </a>
        </div>
		<?php } ?>
	</div>
	<div class="col-md-9">
		<ul class="nav nav-pills" role="tablist" style="margin-bottom: 15px;">
<?php
	if($novoCliente == "17"){
?>

			<li role="presentation" class="active"><a href="#todos" aria-controls="todos" role="tab" data-toggle="tab"><strong>PREFEITURA</strong></a></li>
			
<?
	} else{
?>

			<li role="presentation" class="active">
				<a href="#todos" aria-controls="todos" style="margin-left: 10px;font-size: 15px; padding-bottom: 15px;">
					<i class="glyphicon glyphicon-th-list" style="top: 5px; font-size: 19px; margin-right: 8px;"></i>TODOS OS PROCESSOS
				</a>
			</li>
			
<?php
	}
	if ($novoCliente == "86") {
		?>
		<li role="presentation" class="active">
			<a href="http://www.ingadigital.com.br/transparencia/?id_cliente=86&sessao=ee3c047194odee" style="margin-left: 10px;font-size: 15px; padding-bottom: 15px;background-color: #27576f;">
				<i class="glyphicon glyphicon-signal" style="top: 5px; font-size: 19px; margin-right: 8px;"></i>Realtórios de 2013 a 2018
			</a>
		</li>
	<?php }

	if($novoCliente == "1015") { 
		?>
		<a href="http://ivai.pr.gov.br/aovivo.php" target="_blank" class="col-lg-4 btn btn-success btn-lg" style="margin-left: 10px;font-size: 15px; padding-bottom: 15px;">
            <i class="glyphicon glyphicon-play-circle" style="top: 5px; font-size: 19px; margin-right: 8px;"></i> TRASMISSÃO AO VIVO DAS LICITA&Ccedil;&Otilde;ES
        </a>
	<?}
	if($novoCliente == "12039"){ ?>
		<p class="text-center">
			<a href="http://abatia.pr.gov.br/transparencia/?id_cliente=12039&sessao=1f119e67aeli1f" class="col-lg-3 btn btn-success btn-lg" style="margin-left: 10px; font-size: 13px; padding-bottom: 15px; text-transform: uppercase;padding-top: 15px;">
			<i class="glyphicon glyphicon-book"></i>  Dispensa na &Iacute;ntegra
			</a>
		</p>
	<?}
	$qryAutarquia = $conn->prepare("SELECT id,
										   nome_menu
									  FROM pref_autarquia
									 WHERE id_cliente = :id_cliente
									   AND status_registro = :status_registro
								  ORDER BY nome_menu ASC");
	$qryAutarquia->execute(array("id_cliente" => $novoCliente, "status_registro" => "A"));
	$abaAutarquia = $qryAutarquia->fetchAll();
	if($qryAutarquia->rowCount() > 0){
		foreach($abaAutarquia as $key => $autarquia){
?>

			<li role="presentation">
				<a href="#autarquia_<?= $autarquia['id'] ?>" aria-controls="autarquia_<?= $autarquia['id'] ?>" role="tab" data-toggle="tab">
					<strong><?= $autarquia['nome_menu'] ?></strong>
				</a>
			</li>

<?php
		}
	} 
	if ($novoCliente == "1188") {
?>

			<li role="presentation">
				<a href="http://www.ingadigital.com.br/transparencia/index.php?sessao=d379514abfd2d3">
					<strong>ADITIVOS E CONTRATOS</strong>
				</a>
			</li>
			
<?
	}
	if ($novoCliente == "12046") {
		?>
		
					<li role="presentation">
						<a href="http://www.santamariana.pr.gov.br/transparencia/?id_cliente=12046&sessao=9fc0225112li9f">
							<strong>LICITA&Ccedil;&Otilde;ES NA &Iacute;NTEGRA</strong>
						</a>
					</li>
					
		<?
	}
	if(!empty($configuracaoTransparencia['caminho_digitalizacao_licitacao'])) { 
?>

			<li role="presentation">
				<a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$digitalizacaoLista.$complemento); ?>&nc=<?= $novoCliente ?>&id_tipo=<?= $configuracaoTransparencia['caminho_digitalizacao_licitacao'] ?>">
					<strong>LICITA&Ccedil;&Otilde;ES NA &Iacute;NTEGRA</strong>
				</a>
			</li>

<?php 
	}
	if($novoCliente == "46") { 
?>

			<!-- <li role="presentation"><a href="http://suporte.ubirata.pr.gov.br:8081/pronimtb/anexos/" target="_blank"><strong>LICITA&Ccedil;&Otilde;ES ANTERIORES</strong></a></li> -->

<?php 
	}
	if($novoCliente == "111") { 
?>

			<li role="presentation"><a href="https://e-gov.betha.com.br/transparencia/con_licitacoes.faces?mun=ttfC-RLThtImD58phibapFo1oP_l71oz" target="_blank"><strong>EXTRATOS DE LICITA&Ccedil;&Otilde;ES</strong></a></li>

<?php 
	}
	if($novoCliente == "17") { 
?>

			<li role="presentation"><a href="http://s2.asp.srv.br/etransparencia.pm.paicandu.pr/servlet/wplicitacaoconsulta" target="_blank"><strong>PROCESSOS LICITAT&Oacute;RIOS</strong></a></li>

<?php 
	}
?>

		</ul>

		<div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="todos">
				<ul class="nav nav-tabs">
					<li role="presentation" <?= !isset($_GET['lici']) ? 'class="active"' : "" ?>>
						<a href="#todos-proximas" aria-controls="todos-proximas" role="tab" data-toggle="tab">
							<strong><?= $novoCliente == "12113" ? "EDITAL EM ABERTO" : "PR&Oacute;XIMAS" ?></strong>
						</a>
					</li>

<?php 
	if($novoCliente == "12113"){
?>

					<li role="presentation">
						<a href="#todos-andamento" aria-controls="todos-andamento" role="tab" data-toggle="tab">
							<strong>EM ANDAMENTO</strong>
						</a>
					</li>

<?
	}
?>

					<li role="presentation" <?= isset($_GET['lici']) ? 'class="active"' : "" ?> >
						<a href="#todos-anteriores" aria-controls="todos-anteriores" role="tab" data-toggle="tab">
							<strong><?= $novoCliente == "12113" ? "FINALIZADAS" : "REALIZADAS" ?></strong>
						</a>
					</li>
				</ul>
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="todos-proximas">

<?php
	if(count($qryProximaLicitacao)) {
?>

						<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
							<p></p>

<?php 
		foreach ($qryProximaLicitacao as $licitacao) { 
?>

							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="heading_<?= $licitacao['id'] ?>">
									<h4 class="panel-title">
										<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?= $licitacao['id'] ?>" aria-expanded="true" aria-controls="collapse_<?= $licitacao['id'] ?>">
											<div class="row testeaqui">
									
<?php 
				$stSecretaria = $conn->prepare("SELECT nome_menu 
												  FROM pref_secretaria 
												 WHERE id_cliente = :id_cliente 
												   AND id = :id 
												   AND status_registro = :status_registro 
												 LIMIT 1");
				
				$stSecretaria->execute(array("id_cliente" => $novoCliente, "id" => $licitacao['id_secretaria'], "status_registro" => "A"));
				$qrySecretaria = $stSecretaria->fetch(); 
?>

												<div class="col-sm-12 espacoFlexivel espacoLinhas" style="padding: 0;">
										
<?
				if(!empty($licitacao['data_disputa'])){
?>

													<div class="col-sm-4">
														<strong>Abertura:</strong> <?= formata_data($licitacao['data_abertura']) ?><? if(!empty($licitacao['hora_abertura'])) echo " &agrave;s " . $licitacao['hora_abertura'] ?>
													</div>
													<div class="col-sm-4">
														<strong>Disputa:</strong> <?= formata_data($licitacao['data_disputa']) ?><? if(!empty($licitacao['hora_disputa'])) echo " &agrave;s " . $licitacao['hora_disputa'] ?>
													</div>
										
<? 
				} else {
?>
									
							  						<div class="col-sm-4"><strong>Abertura:</strong> <?= formata_data($licitacao['data_abertura']) ?><? if(!empty($licitacao['hora_abertura'])) echo " &agrave;s " . $licitacao['hora_abertura'] ?></div>			
										
<? 
				} 
?>

												</div>	
												<? if($qrySecretaria['nome_menu'] != ""){ ?>
												<div class="col-sm-12"><strong>Secretaria: </strong><?= $qrySecretaria['nome_menu'] ?></div>
												<? } ?>
												<div class="col-sm-12 espacoLinhas"><i class="glyphicon glyphicon-triangle-right c"></i> <strong><?= $licitacao['titulo'] ?></strong></div>

												<div class="col-sm-4 espacoLinhas"><strong>Publica&ccedil;&atilde;o:</strong> <?= formata_data($licitacao['data_publicacao']) ?></div>
												<div class="col-sm-12 espacoLinhas"><strong>Objeto:</strong> <?= verifica($licitacao['objeto']) ?></div>

<?php
				if(!empty($licitacao['id_autarquia']) && $licitacao['id_autarquia'] != NULL){
					$autarquia = $conn->prepare("SELECT autarquia 
												   FROM pref_autarquia 
												  WHERE id = :id 
												    AND status_registro = :status_registro");
					$autarquia->execute(array("id" => $licitacao['id_autarquia'], "status_registro" => "A"));
					$autarquia = $autarquia->fetch();
?>

												<div class="col-sm-12"><strong>Autarquia: </strong><?= $autarquia['autarquia'] ?></div>
									
<?
				} 
?>
								
											</div>
										</a>
									</h4>
								</div>
								<div id="collapse_<?= $licitacao['id'] ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_<?= $licitacao['id'] ?>">
									<div class="panel-body">
										<p><strong>Modalidade:</strong> <?= $licitacao['modalidade'] ?></p>

<? 
			if($licitacao['valor_maximo'] != "") { 
?>

										<p><strong>Valor M&aacute;ximo:</strong> <?= $licitacao['valor_maximo'] ?></p>
							
<? 
			} 
?>

										<!-- <p><strong>Objeto:</strong> <?= verifica($licitacao['objeto']) ?></p> -->

<?php
			$stAnexo = $conn->prepare("SELECT * 
										 FROM licitacao_edital_anexo 
										WHERE id_edital = :id_edital 
										  AND status_registro = :status_registro 
									 ORDER BY ordem ASC, 
									          id DESC");
			$stAnexo->execute(array("id_edital" => $licitacao['id'], "status_registro" => "A"));
			$qryAnexo = $stAnexo->fetchAll();

			if(count($qryAnexo)) {
?>

										<div class="panel panel-primary">
											<div class="panel-heading">
												<h2 class="panel-title"><i class="glyphicon glyphicon-cloud-download"></i> Anexos.</h2>
											</div>
											<div class="panel-body">

<?php
				foreach ($qryAnexo as $anexo) {
					$ata = true;
					if($cliente == '12101' && $anexo['id_categoria'] == '18' ){
						$ata = false;
					}
					if($cliente == '12088'){
						$ata = false;
					}
					if($filtro == "B" ||  $_SESSION['login_licitacao'] == true) {
?>
									
												<p>
													<strong>
														<a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$licitacaoDownload.$complemento); ?>&nc=<?= $novoCliente ?>&id=<?= verifica($anexo['id']) ?><?= (!$ata)? '&tipo=1':'' ?>" class="text-danger">
															<i class="glyphicon glyphicon-cloud-download"></i> 
															<?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?>
															<? if(!$ata) echo ' - ATA '; ?>
														</a>
													</strong>
												</p>

<?php
					} else if($configuracaoTransparencia['permitir_download_sem_cadastro'] == "S") {	
?>

												<p>
													<strong>
														<a href="#" role="button" data-toggle="modal" data-target="#anexo_<?= $anexo['id'] ?>">
															<i class="glyphicon glyphicon-cloud-download"></i> 
															<?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?>
															<? if(!$ata) echo ' - Ata '; ?>
														</a>
													</strong>
												</p>

												<div class="modal fade" id="anexo_<?= $anexo['id'] ?>">
													<div class="modal-dialog">
														<div class="modal-content">
															<div class="modal-header">
																<button type="button" class="close" data-dismiss="modal" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>
																<h4 class="modal-title">Baixar <?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?></h4>
															</div>

															<div class="modal-body text-center">
									      			
<?
						if($ata){ 
?>
																<button type="button" class="btn btn-danger btn-margin" onclick="jQuery('#confirmacao_<?= $anexo['id'] ?>').removeClass('hidden'); return false;">
																	<i class="glyphicon glyphicon-alert"></i> Download sem cadastro</a>
																</button>
									      			
<? 
						} 
?>
													
																<a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$licitacaoLogin.$complemento); ?>&nc=<?= $novoCliente ?>&id=<?= verifica($anexo['id']) ?><?= (!$ata)? '&tipo=1':'' ?>" class="btn btn-danger btn-margin"><i class="glyphicon glyphicon-user"></i> Download com cadastro</a><br><br>

																<div class="text-justify">

<?php 
						if(empty($configuracaoTransparencia['texto_download_sem_cadastro'])) { 
?>

																	O cadastro no sistema de licita&ccedil;&otilde;es garante o recebimento por e-mail de todas as informa&ccedil;&otilde;es pertinentes ao processo
																	licitat&oacute;rio, como atas, avisos de revoga&ccedil;&atilde;o, retifica&ccedil;&otilde;es ou cancelamento. Caso optar por baixar o edital sem cadastramento, a pessoa
																	f&iacute;sica ou jur&iacute;dica n&atilde;o receber&aacute; as informa&ccedil;&otilde;es atualizadas via e-mail.<br><br>
																	&Eacute; de responsabilidade do fornecedor fazer acessos no site da licitante para verificar quaisquer altera&ccedil;&otilde;es/retifica&ccedil;&otilde;es nos editais de licita&ccedil;&atilde;o de seu interesse.<br><br>

<?php
						} else {
							echo verifica($configuracaoTransparencia['texto_download_sem_cadastro']);
						}
?>

																	<label id="confirmacao_<?= $anexo['id'] ?>" class="hidden"><input type="checkbox" name="concordo_<?= $anexo['id'] ?>" id="concordo_<?= $anexo['id'] ?>" onclick="jQuery('#baixar_<?= $anexo['id'] ?>').toggle().removeClass('hidden');" /><strong>&nbsp;Li e concordo com o texto acima.</strong></label><br>
																	<script>jQuery("#concordo_<?= $anexo['id'] ?>").prop("checked", false);</script>
																</div>

																<a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$licitacaoDownload.$complemento); ?>&nc=<?= $novoCliente ?>&confirm=1&id=<?= verifica($anexo['id'])?><?= (!$ata)? '&tipo=1':'' ?>" id="baixar_<?= $anexo['id'] ?>" class="btn btn-primary text-center hidden">
																	<i class="glyphicon glyphicon-cloud-download"></i> Baixar anexo
																</a>
															</div>
														</div>
													</div>
												</div>

<?php

					} else {
?>

												<p><strong><a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$licitacaoLogin.$complemento); ?>&nc=<?= $novoCliente ?>&id=<?= verifica($anexo['id']) ?>" class="text-danger"><i class="glyphicon glyphicon-cloud-download"></i> <?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?></a></strong></p>

<?php 
					}
				} 
?>

											</div>
										</div>

<?php 
			} 
?>

									</div>
								</div>
							</div>

<?php 
		}
?>

						</div>

<?php 
	} 
?>

					</div>
					<div role="tabpanel" class="tab-pane" id="todos-anteriores">

<?php
	if(count($qryLicitacaoRealizada)) {
?>

						<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
							<p></p>

<?php 
		foreach ($qryLicitacaoRealizada as $licitacao) { 
?>

							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="heading_<?= $licitacao['id'] ?>">
									<h4 class="panel-title">
										<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?= $licitacao['id'] ?>" aria-expanded="true" aria-controls="collapse_<?= $licitacao['id'] ?>">
											<div class="row">

<?php
			if($novoCliente == "12101"){
				$stSecretaria = $conn->prepare("SELECT nome_menu 
												  FROM pref_secretaria 
												 WHERE id_cliente = :id_cliente 
												   AND id = :id 
												   AND status_registro = :status_registro 
												 LIMIT 1");
				$stSecretaria->execute(array("id_cliente" => $novoCliente, "id" => $licitacao['id_secretaria'], "status_registro" => "A"));
				$qrySecretaria = $stSecretaria->fetch();
?>

												<div class="col-sm-4"><i class="glyphicon glyphicon-triangle-right a"></i> <strong><?= $licitacao['titulo'] ?></strong></div>
												<div class="col-sm-2"><strong>Publica&ccedil;&atilde;o:</strong> <?= formata_data($licitacao['data_publicacao']) ?></div>
												<div class="col-sm-3"><strong>Secretaria: </strong><?= $qrySecretaria['nome_menu'] ?></div>  
									
<?
				if(!empty($licitacao['data_disputa'])){
?>

												<div class="col-sm-3">
													<strong>Abertura:</strong> <?= formata_data($licitacao['data_abertura']) ?><? if(!empty($licitacao['hora_abertura'])) echo " &agrave;s " . $licitacao['hora_abertura'] ?><br>
													<strong>Disputa:</strong> <?= formata_data($licitacao['data_disputa']) ?><? if(!empty($licitacao['hora_disputa'])) echo " &agrave;s " . $licitacao['hora_disputa'] ?>
												</div>

<?
				} else {
?>

												<div class="col-sm-3"><strong>Abertura:</strong> <?= formata_data($licitacao['data_abertura']) ?><? if(!empty($licitacao['hora_abertura'])) echo " &agrave;s " . $licitacao['hora_abertura'] ?></div>			
									
<?php
				}
			} else {
?>

												<div class="col-sm-12 espacoFlexivel espacoLinhas" style="padding: 0;">
										
<?
				if(!empty($licitacao['data_disputa'])){
?>

													<div class="col-sm-4">
														<strong>Abertura:</strong> <?= formata_data($licitacao['data_abertura']) ?><? if(!empty($licitacao['hora_abertura'])) echo " &agrave;s " . $licitacao['hora_abertura'] ?>
													</div>
													<div class="col-sm-4">
														<strong>Disputa:</strong> <?= formata_data($licitacao['data_disputa']) ?><? if(!empty($licitacao['hora_disputa'])) echo " &agrave;s " . $licitacao['hora_disputa'] ?>
													</div>
										
<? 
				} else {
?>
									
							  						<div class="col-sm-4"><strong>Abertura:</strong> <?= formata_data($licitacao['data_abertura']) ?><? if(!empty($licitacao['hora_abertura'])) echo " &agrave;s " . $licitacao['hora_abertura'] ?></div>			
										
<? 
				} 
?>
												</div>	
												<div class="col-sm-12 espacoLinhas"><i class="glyphicon glyphicon-triangle-right c"></i> <strong><?= $licitacao['titulo'] ?></strong></div>
												<div class="col-sm-4"><strong>Publica&ccedil;&atilde;o:</strong> <?= formata_data($licitacao['data_publicacao']) ?></div>
									
<?
				if(!empty($licitacao['id_autarquia']) && $licitacao['id_autarquia'] != NULL){
					$autarquia = $conn->prepare("SELECT autarquia 
												   FROM pref_autarquia 
												  WHERE id = :id 
													AND status_registro = :status_registro");
					$autarquia->execute(array("id" => $licitacao['id_autarquia'], "status_registro" => "A"));
					$autarquia = $autarquia->fetch();
?>

												<div class="col-sm-12"><strong>Autarquia: </strong><?= $autarquia['autarquia'] ?></div>
								
<?
				} 
			} 
?>

											</div>
										</a>
									</h4>
								</div>
								<div id="collapse_<?= $licitacao['id'] ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_<?= $licitacao['id'] ?>">
									<div class="panel-body">
										<p><strong>Modalidade:</strong> <?= $licitacao['modalidade'] ?></p>

<? 
			if($licitacao['valor_maximo'] != "") { 
?>
		
										<p><strong>Valor M&aacute;ximo:</strong> <?= $licitacao['valor_maximo'] ?></p>
							
<? 
			} 
?>
								
										<p><strong>Objeto:</strong> <?= verifica($licitacao['objeto']) ?></p>

<?php
			$stAnexo = $conn->prepare("SELECT * 
										 FROM licitacao_edital_anexo 
										WHERE id_edital = :id_edital 
										  AND status_registro = :status_registro 
									 ORDER BY ordem ASC, 
									 		  id DESC");

			$stAnexo->execute(array("id_edital" => $licitacao['id'], "status_registro" => "A"));
			$qryAnexo = $stAnexo->fetchAll();

			if(count($qryAnexo)) {
?>

										<div class="panel panel-primary">
											<div class="panel-heading">
												<h2 class="panel-title"><i class="glyphicon glyphicon-cloud-download"></i> Anexos</h2>
											</div>

											<div class="panel-body">

<?php
				foreach ($qryAnexo as $anexo) {
					$ata = true;
					if($cliente == '12101' && $anexo['id_categoria'] == '18' ){
						$ata = false;
					}
?>

												<p>
													<strong>
														<a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$licitacaoDownload.$complemento); ?>&nc=<?= $novoCliente ?>&id=<?= verifica($anexo['id']) ?>" class="text-danger">
															<i class="glyphicon glyphicon-cloud-download"></i> 
															<?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?>
															<?
																if(!$ata){
																	echo ' - Ata ';
																}
															?>
														</a>
													</strong>
												</p>

<?php 
				} 
?>

											</div>
										</div>

<?php 
			} 
?>

									</div>
								</div>
							</div>

<?php 
		} 
?>

							<p></p>
						</div>

<?php 
	} 
?>

					</div>

<?php
	if($novoCliente == "12113"){
		$stLicitacaoAndamento = $conn->prepare("SELECT licitacao_edital.*, 
													   licitacao_modalidade.descricao modalidade
											      FROM licitacao_edital, 
													   licitacao_modalidade
											     WHERE licitacao_edital.id_modalidade = licitacao_modalidade.id
												   AND licitacao_edital.id_cliente = :id_cliente
												   AND licitacao_edital.em_andamento = :em_andamento
												   AND licitacao_edital.status_registro = :status_registro
											  ORDER BY licitacao_edital.data_abertura ASC, 
													   licitacao_edital.hora_abertura ASC");

		$stLicitacaoAndamento->execute(array(":id_cliente" => $novoCliente, "em_andamento" => "T", ":status_registro" => "A"));
		$qryLicitacaoAndamento = $stLicitacaoAndamento->fetchAll();
?>
					<div role="tabpanel" class="tab-pane" id="todos-andamento">

<?php
		if(count($qryLicitacaoAndamento)) {
?>

						<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
							<p></p>

<?php 
			foreach ($qryLicitacaoAndamento as $licitacao) { 
?>

							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="heading_<?= $licitacao['id'] ?>">
									<h4 class="panel-title">
										<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?= $licitacao['id'] ?>" aria-expanded="true" aria-controls="collapse_<?= $licitacao['id'] ?>">
											<div class="row">
												<div class="col-sm-12 espacoFlexivel espacoLinhas" style="padding: 0;">
										
<?
				if(!empty($licitacao['data_disputa'])){
?>

													<div class="col-sm-4">
														<strong>Abertura:</strong> <?= formata_data($licitacao['data_abertura']) ?><? if(!empty($licitacao['hora_abertura'])) echo " &agrave;s " . $licitacao['hora_abertura'] ?>
													</div>
													<div class="col-sm-4">
														<strong>Disputa:</strong> <?= formata_data($licitacao['data_disputa']) ?><? if(!empty($licitacao['hora_disputa'])) echo " &agrave;s " . $licitacao['hora_disputa'] ?>
													</div>
										
<? 
				} else {
?>
									
							  						<div class="col-sm-4"><strong>Abertura:</strong> <?= formata_data($licitacao['data_abertura']) ?><? if(!empty($licitacao['hora_abertura'])) echo " &agrave;s " . $licitacao['hora_abertura'] ?></div>			
										
<? 
				} 
?>
												</div>	
												<div class="col-sm-12 espacoLinhas"><i class="glyphicon glyphicon-triangle-right c"></i> <strong><?= $licitacao['titulo'] ?></strong></div>
												<div class="col-sm-4"><strong>Publica&ccedil;&atilde;o:</strong> <?= formata_data($licitacao['data_publicacao']) ?></div>
									
<?
				if(!empty($licitacao['id_autarquia']) && $licitacao['id_autarquia'] != NULL){
					$autarquia = $conn->prepare("SELECT autarquia 
												   FROM pref_autarquia 
												  WHERE id = :id 
													AND status_registro = :status_registro");
					$autarquia->execute(array("id" => $licitacao['id_autarquia'], "status_registro" => "A"));
					$autarquia = $autarquia->fetch();
?>

												<div class="col-sm-12"><strong>Autarquia: </strong><?= $autarquia['autarquia'] ?></div>
								
<?
				} 
?>

											</div>
										</a>
									</h4>
								</div>
								<div id="collapse_<?= $licitacao['id'] ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_<?= $licitacao['id'] ?>">
									<div class="panel-body">
										<p><strong>Modalidade:</strong> <?= $licitacao['modalidade'] ?></p>

<? 
			if($licitacao['valor_maximo'] != "") { 
?>
		
										<p><strong>Valor M&aacute;ximo:</strong> <?= $licitacao['valor_maximo'] ?></p>
							
<? 
			} 
?>
								
										<p><strong>Objeto:</strong> <?= verifica($licitacao['objeto']) ?></p>

<?php
			$stAnexo = $conn->prepare("SELECT * 
										 FROM licitacao_edital_anexo 
										WHERE id_edital = :id_edital 
										  AND status_registro = :status_registro 
									 ORDER BY ordem ASC, 
									 		  id DESC");

			$stAnexo->execute(array("id_edital" => $licitacao['id'], "status_registro" => "A"));
			$qryAnexo = $stAnexo->fetchAll();

			if(count($qryAnexo)) {
?>

										<div class="panel panel-primary">
											<div class="panel-heading">
												<h2 class="panel-title"><i class="glyphicon glyphicon-cloud-download"></i> Anexos</h2>
											</div>

											<div class="panel-body">

<?php
				foreach ($qryAnexo as $anexo) {
?>

												<p>
													<strong>
														<a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$licitacaoDownload.$complemento); ?>&nc=<?= $novoCliente ?>&id=<?= verifica($anexo['id']) ?>" class="text-danger">
															<i class="glyphicon glyphicon-cloud-download"></i> 
															<?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?>
														</a>
													</strong>
												</p>

<?php 
				} 
?>

											</div>
										</div>

<?php 
			} 
?>

									</div>
								</div>
							</div>

<?php 
		} 
?>

							<p></p>
						</div>

<?php 
	} 
?>

					</div>

<?	
	}
?>

				</div>
			</div>

<?php
	$qryAutarquia = $conn->prepare("SELECT id,
										   nome_menu,
										   autarquia
									  FROM pref_autarquia
									 WHERE id_cliente = :id_cliente
									   AND status_registro = :status_registro
								  ORDER BY nome_menu ASC");
	$qryAutarquia->execute(array("id_cliente" => $novoCliente, "status_registro" => "A"));
	$abaAutarquia = $qryAutarquia->fetchAll();
	if($qryAutarquia->rowCount() > 0){
		foreach($abaAutarquia as $key => $autarquia){
?>

			<div role="tabpanel" class="tab-pane" id="autarquia_<?= $autarquia['id'] ?>">
				<ul class="nav nav-tabs">
					<li role="presentation" class="active">
						<a href="#autarquia-proximas_<?= $autarquia['id'] ?>" aria-controls="autarquia-proximas_<?= $autarquia['id'] ?>" role="tab" data-toggle="tab">
							<strong>PR&Oacute;XIMAS</strong>
						</a>
					</li>
					<li role="presentation">
						<a href="#autarquia-anteriores_<?= $autarquia['id'] ?>" aria-controls="autarquia-anteriores_<?= $autarquia['id'] ?>" role="tab" data-toggle="tab">
							<strong>REALIZADAS</strong>
						</a>
					</li>
				</ul>
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="autarquia-proximas_<?= $autarquia['id'] ?>">

<?php
			$qryProximaAutarquia = $conn->prepare("SELECT licitacao_edital.*, 
												 		  licitacao_modalidade.descricao modalidade
											         FROM licitacao_edital, 
											              licitacao_modalidade
										            WHERE licitacao_edital.id_modalidade = licitacao_modalidade.id
											          AND licitacao_edital.id_cliente = :id_cliente
													  AND licitacao_edital.id_autarquia = :id_autarquia
													  AND licitacao_edital.em_andamento = :em_andamento
											          AND licitacao_edital.status_registro = :status_registro
											          AND licitacao_edital.data_abertura > CURRENT_DATE()
										         ORDER BY licitacao_edital.data_abertura ASC, 
												          licitacao_edital.hora_abertura ASC");
														  
			$qryProximaAutarquia->execute(array(":id_cliente" => $novoCliente, ":id_autarquia" => $autarquia['id'], ":em_andamento"=> "F", ":status_registro" => "A"));
			$abaProximaAutarquia = $qryProximaAutarquia->fetchAll();
			if($qryProximaAutarquia->rowCount() > 0){
?>

						<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
							<p></p>

<?php 
				foreach ($abaProximaAutarquia as $key => $licitacao) { 
?>

							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="heading_proxima_<?= $key ?>">
									<h4 class="panel-title">
										<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_proxima_<?= $autarquia['id'] ?>_<?= $key ?>" aria-expanded="true" aria-controls="collapse_proxima_<?= $autarquia['id'] ?>_<?= $key ?>">
											<div class="row">
									
<?php 
					if($novoCliente == "12101"){
						$stSecretaria = $conn->prepare("SELECT nome_menu 
														  FROM pref_secretaria 
														 WHERE id_cliente = :id_cliente 
														   AND id = :id 
														   AND status_registro = :status_registro 
														 LIMIT 1");
						
						$stSecretaria->execute(array("id_cliente" => $novoCliente, "id" => $licitacao['id_secretaria'], "status_registro" => "A"));
						$qrySecretaria = $stSecretaria->fetch(); 
?>

												<div class="col-sm-4"><i class="glyphicon glyphicon-triangle-right d"></i> <strong><?= $licitacao['titulo'] ?></strong></div>
												<div class="col-sm-2"><strong>Publica&ccedil;&atilde;o:</strong> <?= formata_data($licitacao['data_publicacao']) ?></div>
												<div class="col-sm-3"><strong>Secretaria: </strong><?= $qrySecretaria['nome_menu'] ?></div>  
									
<?
						if(!empty($licitacao['data_disputa'])){
?>

												<div class="col-sm-3">
													<strong>Abertura:</strong> <?= formata_data($licitacao['data_abertura']) ?><? if(!empty($licitacao['hora_abertura'])) echo " &agrave;s " . $licitacao['hora_abertura'] ?><br>
													<strong>Disputa:</strong> <?= formata_data($licitacao['data_disputa']) ?><? if(!empty($licitacao['hora_disputa'])) echo " &agrave;s " . $licitacao['hora_disputa'] ?>
												</div>

<?
						} else {
?>
									
												<div class="col-sm-3"><strong>Abertura:</strong> <?= formata_data($licitacao['data_abertura']) ?><? if(!empty($licitacao['hora_abertura'])) echo " &agrave;s " . $licitacao['hora_abertura'] ?></div>			
									
<?php
						}
					} else {
?>

												<div class="col-sm-12 espacoFlexivel espacoLinhas" style="padding: 0;">
										
<?
						if(!empty($licitacao['data_disputa'])){
?>

													<div class="col-sm-4">
														<strong>Abertura:</strong> <?= formata_data($licitacao['data_abertura']) ?><? if(!empty($licitacao['hora_abertura'])) echo " &agrave;s " . $licitacao['hora_abertura'] ?>
													</div>
													<div class="col-sm-4">
														<strong>Disputa:</strong> <?= formata_data($licitacao['data_disputa']) ?><? if(!empty($licitacao['hora_disputa'])) echo " &agrave;s " . $licitacao['hora_disputa'] ?>
													</div>
										
<? 
						} else {
?>
									
							  						<div class="col-sm-4"><strong>Abertura:</strong> <?= formata_data($licitacao['data_abertura']) ?><? if(!empty($licitacao['hora_abertura'])) echo " &agrave;s " . $licitacao['hora_abertura'] ?></div>			
										
<? 
						} 
?>

												</div>	
												<div class="col-sm-12 espacoLinhas"><i class="glyphicon glyphicon-triangle-right c"></i> <strong><?= $licitacao['titulo'] ?></strong></div>

												<div class="col-sm-4 espacoLinhas"><strong>Publica&ccedil;&atilde;o:</strong> <?= formata_data($licitacao['data_publicacao']) ?></div>

												<div class="col-sm-12"><strong>Autarquia: </strong><?= $autarquia['autarquia'] ?></div>
									
<?
					}
?>
								
											</div>
										</a>
									</h4>
								</div>
								<div id="collapse_proxima_<?= $autarquia['id'] ?>_<?= $key ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_proxima_<?= $key ?>">
									<div class="panel-body">
										<p><strong>Modalidade:</strong> <?= $licitacao['modalidade'] ?></p>

<? 
					if($licitacao['valor_maximo'] != "") { 
?>

										<p><strong>Valor M&aacute;ximo:</strong> <?= $licitacao['valor_maximo'] ?></p>
							
<? 
					} 
?>

										<p><strong>Objeto:</strong> <?= verifica($licitacao['objeto']) ?></p>

<?php
					$stAnexo = $conn->prepare("SELECT * 
												 FROM licitacao_edital_anexo 
												WHERE id_edital = :id_edital 
												  AND status_registro = :status_registro 
											 ORDER BY ordem ASC, id DESC");
					$stAnexo->execute(array("id_edital" => $licitacao['id'], "status_registro" => "A"));
					$qryAnexo = $stAnexo->fetchAll();

					if(count($qryAnexo)) {
?>

										<div class="panel panel-primary">
											<div class="panel-heading">
												<h2 class="panel-title"><i class="glyphicon glyphicon-cloud-download"></i> Anexos.</h2>
											</div>
											<div class="panel-body">

<?php
						foreach ($qryAnexo as $anexo) {
							$ata = true;
							if($cliente == '12101' && $anexo['id_categoria'] == '18' ){
								$ata = false;
							}
							if($cliente == '12088'){
								$ata = false;
							}
							if($filtro == "B" ||  $_SESSION['login_licitacao'] == true) {
?>
									
												<p>
													<strong>
														<a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$licitacaoDownload.$complemento); ?>&nc=<?= $novoCliente ?>&id=<?= verifica($anexo['id']) ?><?= (!$ata)? '&tipo=1':'' ?>" class="text-danger">
															<i class="glyphicon glyphicon-cloud-download"></i> 
															<?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?>
															<? if(!$ata) echo ' - ATA '; ?>
														</a>
													</strong>
												</p>

<?php
							} else if($configuracaoTransparencia['permitir_download_sem_cadastro'] == "S") {	
?>

												<p>
													<strong>
														<a href="#" role="button" data-toggle="modal" data-target="#anexo_autarquia_<?= $anexo['id'] ?>">
															<i class="glyphicon glyphicon-cloud-download"></i> 
															<?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?>
															<? if(!$ata) echo ' - Ata '; ?>
														</a>
													</strong>
												</p>

												<div class="modal fade" id="anexo_autarquia_<?= $anexo['id'] ?>">
													<div class="modal-dialog">
														<div class="modal-content">
															<div class="modal-header">
																<button type="button" class="close" data-dismiss="modal" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>
																<h4 class="modal-title">Baixar <?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?></h4>
															</div>

															<div class="modal-body text-center">
									      			
<?
								if($ata){ 
?>
																<button type="button" class="btn btn-danger btn-margin" onclick="jQuery('#confirmacao_<?= $anexo['id'] ?>').removeClass('hidden'); return false;">
																	<i class="glyphicon glyphicon-alert"></i> Download sem cadastro</a>
																</button>
									      			
<? 
								} 
?>
													
																<a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$licitacaoLogin.$complemento); ?>&nc=<?= $novoCliente ?>&id=<?= verifica($anexo['id']) ?><?= (!$ata)? '&tipo=1':'' ?>" class="btn btn-danger btn-margin"><i class="glyphicon glyphicon-user"></i> Download com cadastro</a><br><br>

																<div class="text-justify">

<?php 
								if(empty($configuracaoTransparencia['texto_download_sem_cadastro'])) { 
?>

																	O cadastro no sistema de licita&ccedil;&otilde;es garante o recebimento por e-mail de todas as informa&ccedil;&otilde;es pertinentes ao processo
																	licitat&oacute;rio, como atas, avisos de revoga&ccedil;&atilde;o, retifica&ccedil;&otilde;es ou cancelamento. Caso optar por baixar o edital sem cadastramento, a pessoa
																	f&iacute;sica ou jur&iacute;dica n&atilde;o receber&aacute; as informa&ccedil;&otilde;es atualizadas via e-mail.<br><br>
																	&Eacute; de responsabilidade do fornecedor fazer acessos no site da licitante para verificar quaisquer altera&ccedil;&otilde;es/retifica&ccedil;&otilde;es nos editais de licita&ccedil;&atilde;o de seu interesse.<br><br>

<?php
								} else {
									echo verifica($configuracaoTransparencia['texto_download_sem_cadastro']);
								}
?>

																	<label id="confirmacao_<?= $anexo['id'] ?>" class="hidden"><input type="checkbox" name="concordo_<?= $anexo['id'] ?>" id="concordo_<?= $anexo['id'] ?>" onclick="jQuery('#baixar_<?= $anexo['id'] ?>').toggle().removeClass('hidden');" /><strong>&nbsp;Li e concordo com o texto acima.</strong></label><br>
																	<script>jQuery("#concordo_<?= $anexo['id'] ?>").prop("checked", false);</script>
																</div>

																<a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$licitacaoDownload.$complemento); ?>&nc=<?= $novoCliente ?>&confirm=1&id=<?= verifica($anexo['id'])?><?= (!$ata)? '&tipo=1':'' ?>" id="baixar_<?= $anexo['id'] ?>" class="btn btn-primary text-center hidden">
																	<i class="glyphicon glyphicon-cloud-download"></i> Baixar anexo
																</a>
															</div>
														</div>
													</div>
												</div>

<?php

							} else {
?>

												<p><strong><a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$licitacaoLogin.$complemento); ?>&nc=<?= $novoCliente ?>&id=<?= verifica($anexo['id']) ?>" class="text-danger"><i class="glyphicon glyphicon-cloud-download"></i> <?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?></a></strong></p>

<?php 
							}
						} 
?>

											</div>
										</div>

<?php 
					} 
?>

									</div>
								</div>
							</div>

<?php 
				}
?>

						</div>

<?php 
			} 
?>

					</div>
					<div role="tabpanel" class="tab-pane" id="autarquia-anteriores_<?= $autarquia['id'] ?>">

<?php
			$qryAutarquiaAnterior = $conn->prepare("SELECT licitacao_edital.*, 
												 		   licitacao_modalidade.descricao modalidade
											          FROM licitacao_edital, 
											               licitacao_modalidade
										             WHERE licitacao_edital.id_modalidade = licitacao_modalidade.id
											           AND licitacao_edital.id_cliente = :id_cliente
													   AND licitacao_edital.em_andamento = :em_andamento
													   AND licitacao_edital.id_autarquia = :id_autarquia
											           AND licitacao_edital.status_registro = :status_registro
											           AND licitacao_edital.data_abertura < CURRENT_DATE()
										          ORDER BY licitacao_edital.data_abertura ASC, 
												           licitacao_edital.hora_abertura ASC");
			$qryAutarquiaAnterior->execute(array(":id_cliente" => $novoCliente, ":em_andamento" => "F", ":id_autarquia" => $autarquia['id'], ":status_registro" => "A"));
			$abaAutarquiaAnterior = $qryAutarquiaAnterior->fetchAll();
			
			if($qryAutarquiaAnterior->rowCount() > 0){
				
?>

						<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
							<p></p>

<?php 

				foreach ($abaAutarquiaAnterior as $key => $licitacao) { 
?>

							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="heading_anterior_<?= $key ?>">
									<h4 class="panel-title">
										<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_anterior_<?= $autarquia['id'] ?>_<?= $key ?>" aria-expanded="true" aria-controls="collapse_anterior_<?= $autarquia['id'] ?>_<?= $key ?>">
											<div class="row">

<?php

					if($novoCliente == "12101"){
						$stSecretaria = $conn->prepare("SELECT nome_menu 
														  FROM pref_secretaria 
														 WHERE id_cliente = :id_cliente 
														   AND id = :id 
														   AND status_registro = :status_registro 
														 LIMIT 1");
						$stSecretaria->execute(array("id_cliente" => $novoCliente, "id" => $licitacao['id_secretaria'], "status_registro" => "A"));
						$qrySecretaria = $stSecretaria->fetch();
?>

												<div class="col-sm-4"><i class="glyphicon glyphicon-triangle-right a"></i> <strong><?= $licitacao['titulo'] ?></strong></div>
												<div class="col-sm-2"><strong>Publica&ccedil;&atilde;o:</strong> <?= formata_data($licitacao['data_publicacao']) ?></div>
												<div class="col-sm-3"><strong>Secretaria: </strong><?= $qrySecretaria['nome_menu'] ?></div>  
									
<?
						if(!empty($licitacao['data_disputa'])){
?>

												<div class="col-sm-3">
													<strong>Abertura:</strong> <?= formata_data($licitacao['data_abertura']) ?><? if(!empty($licitacao['hora_abertura'])) echo " &agrave;s " . $licitacao['hora_abertura'] ?><br>
													<strong>Disputa:</strong> <?= formata_data($licitacao['data_disputa']) ?><? if(!empty($licitacao['hora_disputa'])) echo " &agrave;s " . $licitacao['hora_disputa'] ?>
												</div>

<?
						} else {
?>

												<div class="col-sm-3"><strong>Abertura:</strong> <?= formata_data($licitacao['data_abertura']) ?><? if(!empty($licitacao['hora_abertura'])) echo " &agrave;s " . $licitacao['hora_abertura'] ?></div>			
									
<?php
						}
					} else {
?>

												<div class="col-sm-12 espacoFlexivel espacoLinhas" style="padding: 0;">
										
<?
						if(!empty($licitacao['data_disputa'])){
?>

													<div class="col-sm-4">
														<strong>Abertura:</strong> <?= formata_data($licitacao['data_abertura']) ?><? if(!empty($licitacao['hora_abertura'])) echo " &agrave;s " . $licitacao['hora_abertura'] ?>
													</div>
													<div class="col-sm-4">
														<strong>Disputa:</strong> <?= formata_data($licitacao['data_disputa']) ?><? if(!empty($licitacao['hora_disputa'])) echo " &agrave;s " . $licitacao['hora_disputa'] ?>
													</div>
										
<? 
						} else {
?>
									
							  						<div class="col-sm-4"><strong>Abertura:</strong> <?= formata_data($licitacao['data_abertura']) ?><? if(!empty($licitacao['hora_abertura'])) echo " &agrave;s " . $licitacao['hora_abertura'] ?></div>			
										
<? 
						} 
?>
												</div>	
												<div class="col-sm-12 espacoLinhas"><i class="glyphicon glyphicon-triangle-right c"></i> <strong><?= $licitacao['titulo'] ?></strong></div>
												<div class="col-sm-4"><strong>Publica&ccedil;&atilde;o:</strong> <?= formata_data($licitacao['data_publicacao']) ?></div>
												<div class="col-sm-12"><strong>Autarquia: </strong><?= $autarquia['autarquia'] ?></div>
									
<? 
					} 
?>

											</div>
										</a>
									</h4>
								</div>
								<div id="collapse_anterior_<?= $autarquia['id'] ?>_<?= $key ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_anterior_<?= $autarquia['id'] ?>_<?= $key ?>">
									<div class="panel-body">
										<p><strong>Modalidade:</strong> <?= $licitacao['modalidade'] ?></p>

<? 
					if($licitacao['valor_maximo'] != "") { 
?>
		
										<p><strong>Valor M&aacute;ximo:</strong> <?= $licitacao['valor_maximo'] ?></p>
							
<? 
					} 
?>
								
										<p><strong>Objeto:</strong> <?= verifica($licitacao['objeto']) ?></p>

<?php
					$stAnexo = $conn->prepare("SELECT * 
												 FROM licitacao_edital_anexo 
												WHERE id_edital = :id_edital 
												  AND status_registro = :status_registro 
											 ORDER BY ordem ASC, 
													id DESC");

					$stAnexo->execute(array("id_edital" => $licitacao['id'], "status_registro" => "A"));
					$qryAnexo = $stAnexo->fetchAll();

					if(count($qryAnexo)) {
?>

										<div class="panel panel-primary">
											<div class="panel-heading">
												<h2 class="panel-title"><i class="glyphicon glyphicon-cloud-download"></i> Anexos</h2>
											</div>

											<div class="panel-body">

<?php
						foreach ($qryAnexo as $anexo) {
							$ata = true;
							if($cliente == '12101' && $anexo['id_categoria'] == '18' ){
								$ata = false;
							}
?>

												<p>
													<strong>
														<a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$licitacaoDownload.$complemento); ?>&nc=<?= $novoCliente ?>&id=<?= verifica($anexo['id']) ?>" class="text-danger">
															<i class="glyphicon glyphicon-cloud-download"></i> 
															<?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?>
															<?
																if(!$ata){
																	echo ' - Ata ';
																}
															?>
														</a>
													</strong>
												</p>

<?php 
						} 
?>

											</div>
										</div>

<?php 
					} 
?>

									</div>
								</div>
							</div>

<?php 
				} 
?>

							<p></p>
						</div>

<?php 
			} 
?>

					</div>
				</div>
			</div>

<?
		}
	}
?>
			<a href="<?=$CAMINHO?>/index.php?sessao=<?= $sequencia.$licitacaoLista.$complemento?>&nc=<?= $novoCliente ?>&situacao=F" class="btn btn-primary pull-right">
				<i class="glyphicon glyphicon-search"></i> Ver Todas
			</a>			
		</div>
	</div>
</div>

	<p class="clearfix"></p>

<?php 
	if($novoCliente == "33") { 
?>

	<p class="text-center">
		<a href="<?=$CAMINHOCMGERAL ?>/licitacao/documento_cadastro_itambe.pdf" target="_blank">
			<img src="<?=$CAMINHOCMGERAL ?>/images/doc_cadastro.jpg" class="imagem" />
		</a>
	</p>
	<p class="text-center">
		<a href="http://www.itambe.pr.gov.br/download/propostas.exe" target="_blank"><img src="<?=$CAMINHOCMGERAL ?>/images/proposta.jpg" class="imagem" /></a>
		<a href="http://www.itambe.pr.gov.br/download/propostas.rar" target="_blank"><img src="<?=$CAMINHOCMGERAL ?>/images/proposta_rar.png" class="imagem" /></a><br>
		<span class="text-danger">ATEN&Ccedil;&Atilde;O!!!  Os Arquivos  proposta_itambe.exe e comercial.xml devem ser salvos na mesma pasta.</span>
	</p>

<?php 
	} 
	if($novoCliente == "12023") { 
?>

	<p class="text-center">
		<a href="<?=$CAMINHOCMGERAL ?>/licitacao/proposta_porto_rico.exe" target="_blank"><img src="<?=$CAMINHOCMGERAL ?>/images/proposta.jpg" class="imagem" /></a><br>
		<span class="text-danger">ATEN&Ccedil;&Atilde;O!!!  Os Arquivos  proposta_porto_rico.exe e comercial.xml devem ser salvos na mesma pasta.</span>
	</p>

<?php 
	} 
	if($novoCliente == "1050" || $novoCliente == "38" || $novoCliente == "98" || $novoCliente == "11987" || $novoCliente == "39" || $novoCliente == "11996" || $novoCliente == "1199" || $novoCliente == "2" || $novoCliente == "1165" || $novoCliente == "86") { 
?>

	<div style="display: flex; justify-content: space-around;">
		<p class="text-center" style="margin-bottom: 20px;">
			<a href="http://download.betha.com.br/versoesdisp.jsp?s=33" target="_blank">
				<img src="<?=$CAMINHOCMGERAL ?>/images/autocotacao.png" class="imagem" />
			</a>
		</p>
	
<?php 
	if($novoCliente == "1050") { 
?>
	
		<p class="text-center" style="margin-bottom: 20px;">
			<a href="http://www.ingadigital.com.br/transparencia/?id_cliente=1050&sessao=b0546033683mb0" target="_blank">
				<img src="<?=$CAMINHOCMGERAL ?>/images/060218135822_modeloaviso_png.png" class="imagem" />
			</a>
		</p>
	
<?php 
	} 
?>

	</div>

<?php 
	} 
	if($novoCliente == "12004") { 
?>

	<p class="text-center">
		<a href="http://download.betha.com.br/versoesdisp.jsp?s=33" target="_blank">
			<img src="http://www.controlemunicipal.com.br/site/geral/images/autocotacao_paranavai.png" class="imagem" />
		</a><br><br>
		<a style="padding: 10px 77px 10px 77px; border: solid 1px #dddddd; font-weight: bold; margin-top: 10px;" href="https://www.adrive.com/public/BSd7PF/Licita%C3%A7%C3%B5es%20na%20Integra">
			Licita&ccedil;&otilde;es na &Iacute;ntegra
		</a>
	</p>

<?php 
	} 
	if($novoCliente == "2") { 
?>

	<p class="text-center">
		<a style="padding: 10px; border: solid 1px #dddddd; font-weight: bold;" title="Baixe aqui a documenta&ccedil;&atilde;o" href="<?=$CAMINHOCMGERAL ?>/licitacao/documentacao_cadastro.doc" target="_blank">
			Documenta&ccedil;&atilde;o para Cadastro
		</a>
	</p>

<?php 
	} 
	if($novoCliente == "17") { 
?>

	<p class="text-left">
		<img src="<?=$CAMINHOCMGERAL ?>/images/doc_cadastro.jpg" class="imagem"><br>
		<a href="http://paicandu.pr.gov.br/licitacao/Edital_01-2013_001.pdf" target="_blank"><i class="glyphicon glyphicon-cloud-download"></i> Edital 01-2013 001</a><br>
		<a href="http://paicandu.pr.gov.br/licitacao/Ficha_do_Fornecedor_2013.doc" target="_blank"><i class="glyphicon glyphicon-cloud-download"></i> Ficha do Fornecedor 2013</a><br>
		<a href="http://paicandu.pr.gov.br/licitacao/Requerimento_de_Fornecedor.doc" target="_blank"><i class="glyphicon glyphicon-cloud-download"></i> Requerimento de Fornecedor</a><br>
	</p>

<?php 
	} 
	if($novoCliente == "11946") { 
?>

	<p class="text-center">
		<a href="<?=$CAMINHOCMGERAL ?>/licitacao/LC_KitProposta_519_01_00_000_11946.msi" target="_blank">
			<img src="<?=$CAMINHOCMGERAL ?>/images/260417170201_avisoesperancanova_png.png" class="imagem" />
		</a>
	</p>

<?php 
	} 
	if($novoCliente == "11973" || $novoCliente == "15") { 
?>

	<p class="text-center">
		<a href="http://www.tce.pr.gov.br/TCEPR/Municipal/AML/ConsultarProcessoCompraWeb.aspx" target="_blank">
			<img src="<?=$CAMINHOCMGERAL ?>/images/licita_tce.jpg" class="imagem" />
		</a>
	</p>

<?php 
	} 
	if($novoCliente == "1119") { 
?>

	<p class="text-left">
		<a href="http://cmsabaudia.pr.gov.br/licitacao/cadastro_fornecedores_cmsabaudia.pdf" target="_blank">
			<strong><i class="glyphicon glyphicon-cloud-download"></i> CADASTRO DE FORNECEDORES</strong>
		</a>
	</p>

<?php
	}

	$atualizacao = atualizacao("compras_licitacao/licitacao", $novoCliente, $conn);

	if($atualizacao != "") {
?>

	<p class="text-right"><small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small></p>

<?php
	}
?>
