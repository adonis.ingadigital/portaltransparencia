<h2>Ficha de inscri&ccedil;&atilde;o de Processo Seletivo</h2>
<ol class="breadcrumb">
	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
	<li><a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$concursoLista.$complemento); ?>">Concursos P&uacute;blicos</a></li>
	<li class="active">Ficha de inscri&ccedil;&atilde;o</li>
</ol>

<?php
include "../../privado/transparencia/recaptchalib.php";

$id_vaga = secure($_GET['id_vaga']);

$stConcurso = $conn->prepare("SELECT concurso.*, concurso_vaga.vaga, concurso_vaga.valor_inscricao
								FROM concurso, concurso_vaga
							   WHERE concurso.id = concurso_vaga.id_artigo
								 AND concurso_vaga.id = :id_vaga
								 AND concurso_vaga.status_registro = :status_registro");
$stConcurso->execute(array("id_vaga" => $id_vaga, "status_registro" => "A"));
$buscaConcurso = $stConcurso->fetch();

$stEstado = $conn->prepare("SELECT * FROM estado WHERE status_registro = :status_registro ORDER BY nome");
$stEstado->execute(array("status_registro" => "A"));
$qryEstado = $stEstado->fetchAll();

if($_POST['acao'] == "cadastrar") {

	foreach ($_POST as $campo => $valor) {
		$$campo = secure($valor);
	}

	$stConf = $conn->prepare("SELECT id FROM concurso_inscricao WHERE id_cliente = :id_cliente AND cpf = :cpf AND id_vaga = :id_vaga AND status_registro = :status_registro");
	$stConf->execute(array("id_cliente" => $cliente, "cpf" => $cpf, "id_vaga" => $id_vaga, "status_registro" => "A"));
	$qryConf = $stConf->fetchAll();
	$conf = count($qryConf);

	$cap = new GoogleRecaptcha();
	$verified = $cap->VerifyCaptcha($_POST['g-recaptcha-response']);

	$extensoes = array("pdf","txt","doc","docx","jpeg","jpg","png");
	$ext = strtolower(array_pop(explode(".", $_FILES['arquivo']['name'])));

	if(!$verified) {

		echo'<p class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				O Captcha n&atilde;o foi resolvido! Verifique.
			</p>';

	} else if($conf > 0) {

		echo'<p class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				Aten&ccedil;&atildeo! J&aacute; existe um cadastro com este CPF para esta vaga.
			</p>';

	} else if(empty($nome)) {

		echo'<p class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				Preencha o campo Nome!
			</p>';

	} else if(validaCPF($cpf) == false) {

		echo'<p class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				O CPF informado &eacute; inv&aacute;lido!
			</p>';

	} else if(empty($telefone_fixo) || empty($telefone_celular)) {

		echo'<p class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				Informe os n&uacute;meros de telefone solicitados!
			</p>';

	} else if(empty($email) || filter_var($email, FILTER_VALIDATE_EMAIL) === false) {

		echo'<p class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				Forne&ccedil;a um e-mail v&aacute;lido!
			</p>';

	} else if(empty($endereco) || empty($bairro) || empty($cep)) {

		echo'<p class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				Informe seu endere&ccedil;o, bairro e CEP!
			</p>';

	} else if(empty($id_municipio)) {

		echo'<p class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				Informe seu estado e sua cidade!
			</p>';

	} else if(empty($instituicao)) {

		echo'<p class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				Informe a institui&ccedil;&atilde;o de ensino!
			</p>';

	} else if(is_uploaded_file($_FILES['arquivo']['tmp_name']) && array_search($ext, $extensoes) === false) {

		echo'<p class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				Formato de arquivo <strong>'.$ext.'</strong> inv&aacute;lido! Insira apenas os seguintes tipos de arquivos: <strong>PDF, .TXT, .DOC, .DOCX, .JPG, .JPEG</strong>!
			</p>';

	} else {

		if(is_uploaded_file($_FILES['arquivo']['tmp_name'])) {

			$destino = "../../controlemunicipal/www/inga/sistema/arquivos/$cliente";
			$arquivo = date("dmyHis") . "_" . nomear_pasta($_FILES['arquivo']['name']) . "." . $ext;

			if(!move_uploaded_file($_FILES['arquivo']['tmp_name'], $destino . "/" . $arquivo)) {

				echo'<p class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					Houve um erro ao fazer o upload do arquivo! Entre em contato.
				</p>';
			}

		} else {

			$arquivo = NULL;

		}

		$deficiencia = $deficiencia == "S" ? "S" : "N";

		$stInscricao = $conn->prepare("INSERT INTO concurso_inscricao (id_cliente,
																	   id_artigo,
																	   id_vaga,
																	   data_inscricao,
																	   nome,
																	   data_nascimento,
																	   sexo,
																	   deficiencia,
																	   deficiencia_qual,
																	   telefone_fixo,
																	   telefone_celular,
																	   email,
																	   cpf,
																	   rg,
																	   data_expedicao,
																	   orgao_expedidor,
																	   nome_pai,
																	   nome_mae,
																	   endereco,
																	   bairro,
																	   id_municipio,
																	   cep,
																	   instituicao,
																	   curso,
																	   periodo,
																	   conclusao,
																	   nivel,
																	   arquivo)
												VALUES (?,
														?,
														?,
														NOW(),
														?,
														?,
														?,
														?,
														?,
														?,
														?,
														?,
														?,
														?,
														?,
														?,
														?,
														?,
														?,
														?,
														?,
														?,
														?,
														?,
														?,
														?,
														?,
														?)");

		$stInscricao->bindParam(1, $cliente, PDO::PARAM_INT);
		$stInscricao->bindParam(2, $buscaConcurso["id"], PDO::PARAM_INT);
		$stInscricao->bindParam(3, $id_vaga, PDO::PARAM_INT);
		$stInscricao->bindParam(4, $nome, PDO::PARAM_STR);
		$stInscricao->bindParam(5, formata_data_banco($data_nascimento), PDO::PARAM_STR);
		$stInscricao->bindParam(6, $sexo, PDO::PARAM_STR);
		$stInscricao->bindParam(7, $deficiencia, PDO::PARAM_STR);
		$stInscricao->bindParam(8, $deficiencia_qual, PDO::PARAM_STR);
		$stInscricao->bindParam(9, $telefone_fixo, PDO::PARAM_STR);
		$stInscricao->bindParam(10, $telefone_celular, PDO::PARAM_STR);
		$stInscricao->bindParam(11, $email, PDO::PARAM_STR);
		$stInscricao->bindParam(12, $cpf, PDO::PARAM_STR);
		$stInscricao->bindParam(13, $rg, PDO::PARAM_STR);
		$stInscricao->bindParam(14, formata_data_banco($data_expedicao), PDO::PARAM_STR);
		$stInscricao->bindParam(15, $orgao_expedidor, PDO::PARAM_STR);
		$stInscricao->bindParam(16, $nome_pai, PDO::PARAM_STR);
		$stInscricao->bindParam(17, $nome_mae, PDO::PARAM_STR);
		$stInscricao->bindParam(18, $endereco, PDO::PARAM_STR);
		$stInscricao->bindParam(19, $bairro, PDO::PARAM_STR);
		$stInscricao->bindParam(20, $id_municipio, PDO::PARAM_INT);
		$stInscricao->bindParam(21, $cep, PDO::PARAM_STR);
		$stInscricao->bindParam(22, $instituicao, PDO::PARAM_STR);
		$stInscricao->bindParam(23, $curso, PDO::PARAM_STR);
		$stInscricao->bindParam(24, $periodo, PDO::PARAM_STR);
		$stInscricao->bindParam(25, $conclusao, PDO::PARAM_STR);
		$stInscricao->bindParam(26, $nivel, PDO::PARAM_STR);
		$stInscricao->bindParam(27, $arquivo, PDO::PARAM_STR);

		$cad = $stInscricao->execute();

		if($cad) {

			$id = $conn->lastInsertId();
			$numero_inscricao = str_pad($id, 6, "0", STR_PAD_LEFT);

			if($buscaConcurso['boleto'] == "S") {

				$stNumBoleto = $conn->prepare("SELECT Auto_increment numero
												 FROM information_schema.tables
												WHERE table_name='concurso_inscricao_boleto'
												  AND table_schema = :database");
				$stNumBoleto->execute(array("database" => $database));
				$numeroBoleto = $stNumBoleto->fetch();

				$stBoleto = $conn->prepare("INSERT INTO concurso_inscricao_boleto (id_inscricao,
																				   data_documento,
																				   data_vencimento,
																				   valor_documento,
																				   numero_boleto)
														VALUES (?,
																NOW(),
																?,
																?,
																?)");

				$stBoleto->bindParam(1, $id, PDO::PARAM_INT);
				$stBoleto->bindParam(2, $buscaConcurso["data_vencimento_boleto"], PDO::PARAM_STR);
				$stBoleto->bindParam(3, $buscaConcurso["valor_inscricao"], PDO::PARAM_STR);
				$stBoleto->bindParam(4, $numeroBoleto["numero"], PDO::PARAM_INT);
				$stBoleto->execute();

				$id_boleto = $conn->lastInsertId();

				$stBanco = $conn->prepare("SELECT numero FROM fi_banco WHERE id = :id_banco");
				$stBanco->execute(array("id_banco" => $buscaConcurso["id_banco"]));
				$banco = $stBanco->fetch();

				$numero_banco = str_pad($banco['numero'], 3, "0", STR_PAD_LEFT);
			}

			if(!empty($email) || filter_var($email, FILTER_VALIDATE_EMAIL) !== false) {

				//E-mail de confirmacao
				if($cliente == "61") $dadosCliente["razao_social"] = "C&acirc;mara Municipal de Loanda";

				$subject = html_entity_decode("Inscri&ccedil;&atilde;o Recebida com Sucesso - $buscaConcurso[titulo] - $dadosCliente[razao_social]");

				$html  = "<p>$nome, essa &eacute; a sua confirma&ccedil;&atilde;o de inscri&ccedil;&atilde;o no concurso <strong>$buscaConcurso[titulo]</strong> da $dadosCliente[razao_social]. Confira os dados preenchidos na ficha de inscri&ccedil;&atilde;o:</p>";
				$html .= "<p>&nbsp;</p>";
				$html .= "<p>Anote seu n&uacute;mero de inscri&ccedil;&atilde;o: <strong>$numero_inscricao</strong></p>";
				$html .= "<p><a href='$CAMINHOCMGERAL/impressao/processo_seletivo.php?id=$id&idCliente=$cliente' target='_blank'><strong>CLIQUE AQUI</strong> para imprimir sua ficha de inscri&ccedil;&atilde;o.</a></p>";
				if($id_boleto != "") $html .= "<p><a href='$CAMINHOCMGERAL/impressao/boleto/$numero_banco.php?id=$id_boleto' target='_blank'><strong>CLIQUE AQUI</strong> para imprimir seu boleto para pagamento.</a></p>";
				$html .= "<p>&nbsp;</p>";
				$html .= "<p>Caso haja alguma informa&ccedil;&atilde;o errada, entre em contato com $dadosCliente[razao_social] e informe seu n&uacute;mero de inscri&ccedil;&atilde;o.</p>";
				$html .= "<p>&nbsp;</p><p>Atenciosamente,</p><p>$dadosCliente[razao_social]</p>";

				$headers = "$dadosCliente[razao_social] <".(empty($dadosCliente['email']) ? "contato@ingadigital.com.br" : $dadosCliente['email']).">";
				$to[0]['email'] = $email;
				$to[0]['nome'] = "Confirmação de Inscrição";
				$vai = envia_email_aws($email, $subject, $html, $headers, [], './ingadigital.com.br/privado/transparencia/concurso/inscricao.php:281');

				$msg = '<div class="panel panel-success">
							<div class="panel-heading">
						    	<h2 class="panel-title"><i class="glyphicon glyphicon-user"></i> INSCRI&Ccedil;&Atilde;O CADASTRADA COM SUCESSO</h2>
						  	</div>
							<div class="panel-body">
								<p>Anote o n&uacute;mero da sua inscri&ccedil;&atilde;o caso necessite emitir novas vias da mesma: <span class="label label-default">'.$numero_inscricao.'</span></p>';

				if($id_boleto != "") {

					$msg .= 	'<p><a href="'.$CAMINHOCMGERAL.'/impressao/boleto/'.$numero_banco.'.php?id='.$id_boleto.'" target="_blank"><i class="glyphicon glyphicon-barcode"></i> Clique aqui para imprimir o boleto.</a></p>';
				}

				$msg .= 		'<p><a href="'.$CAMINHOCMGERAL.'/impressao/processo_seletivo.php?id='.$id.'&idCliente='.$cliente.'" target="_blank"><i class="glyphicon glyphicon-print"></i> Clique aqui para imprimir sua ficha de inscri&ccedil;&atilde;o.</a></p>
							</div>
						</div>
						<div class="clearfix"></div>';

				echo $msg;
			}

		} else {

			echo'<p class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					N&atilde;o foi poss&iacute;vel realizar sua inscri&ccedil;&atilde;o! Tente novamente mais tarde.
				</p>';
		}
	}
}

if($_POST['mensagem_recurso'] != "" || is_uploaded_file($_FILES['arquivo_recurso']['tmp_name'])) {

	$cap = new GoogleRecaptcha();
	$verified = $cap->VerifyCaptcha($_POST['g-recaptcha-response']);

	if($verified) {

		$extensoes = array("pdf","txt","doc","docx","PDF","DOC","DOCX","TXT");

		$ext = array_pop(explode(".", $_FILES['arquivo_recurso']['name']));

		if(array_search($ext, $extensoes) === false) {

			echo'<p class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					Formato de arquivo <strong>'.$ext.'</strong> inv&aacute;lido! Insira apenas arquivos <strong>.PDF, .TXT, .DOC, .DOCX, .JPG, .JPEG</strong>!
				</p>';

		} else {

			$stInscricao = $conn->prepare("SELECT * FROM concurso_inscricao WHERE id = :id AND id_cliente = :id_cliente AND status_registro = :status_registro");
			$stInscricao->execute(array("id" => $_POST['id_inscricao'], "id_cliente" => $cliente, "status_registro" => "A"));
			$inscricao = $stInscricao->fetch();

			if($cliente == "61") $dadosCliente["razao_social"] = "C&acirc;mara Municipal de Loanda";

			if(!empty($buscaConcurso['email_recurso']) || filter_var($buscaConcurso['email_recurso'], FILTER_VALIDATE_EMAIL) !== false) {

				$email_recurso = $buscaConcurso['email_recurso'];
			} else {

				$email_recurso = $dadosCliente['email'];
			}

			if(!empty($email_recurso) || filter_var($email_recurso, FILTER_VALIDATE_EMAIL) !== false) {

				$subject = html_entity_decode("Recurso enviado pelo site - $buscaConcurso[titulo]");

				$html    = "<html><body>";
				$html   .= "<p>" . date("d/m/Y H:i") . "</p>";
				$html   .= "<p><strong>Nome: </strong>";
				$html   .= "$inscricao[nome]</p>";
				$html   .= "<p><strong>E-mail: </strong>";
				$html   .= "$inscricao[email]</p>";
				$html   .= "<p><strong>CPF: </strong>";
				$html   .= "$inscricao[cpf]</p>";
				$html   .= "<p><strong>Concurso: </strong>";
				$html   .= "$buscaConcurso[titulo]</p>";
				$html   .= "<p><strong>Vaga: </strong>";
				$html   .= "$buscaConcurso[vaga]</p>";
				$html   .= "<p>".nl2br($_POST['mensagem_recurso'])."</p>";
				$html   .= "<p><strong>IP do remetente: </strong>";
				$html   .= $_SERVER['REMOTE_ADDR'] . "</p>";
				$html   .= "</body></html>";

				$headers  = "MIME-Version: 1.1\n";
				$headers .= "Return-Path: $dadosCliente[razao_social] <$email_recurso> \n";
				$headers .= "From: $dadosCliente[razao_social] <$email_recurso> \n";
				$headers .= "Sender: $dadosCliente[razao_social] <$email_recurso> \n";
				if($inscricao['email'] != "") $headers .= "Reply-To: $inscricao[nome] <$inscricao[email]> \n";
				$headers .= "Organization: $dadosCliente[razao_social] \n";

				$arquivo = $_FILES["arquivo_recurso"];

				if(file_exists($arquivo["tmp_name"]) and !empty($arquivo)){


					$boundary = "XYZ-" . date("dmYis") . "-ZYX";
					$headers.= "Content-type: multipart/mixed; boundary=\"$boundary\"\r\n";
					$headers.= "$boundary\n";

					// Nesta linha abaixo, abrimos o arquivo enviado.
					$fp = fopen($_FILES["arquivo_recurso"]["tmp_name"],"rb");
					// Agora vamos ler o arquivo aberto na linha anterior
					$anexo = fread($fp,filesize($_FILES["arquivo_recurso"]["tmp_name"]));
					// Codificamos os dados com MIME para o e-mail
					$anexo = base64_encode($anexo);
					// Fechamos o arquivo aberto anteriormente
					fclose($fp);
					// Nesta linha a seguir, vamos dividir a variável do arquivo em pequenos pedaços para podermos enviar
					$anexo = chunk_split($anexo);
					// Nas linhas abaixo vamos passar os parâmetros de formatação e codificação, juntamente com a inclusão do arquivo anexado no corpo da mensagem.
					$mensagem = "--$boundary\n";
					$mensagem.= "Content-Transfer-Encoding: 8bits\n";
					$mensagem.= "Content-Type: text/html; charset=\"ISO-8859-1\"\n\n";
					$mensagem.= "$html\n";
					$mensagem.= "--$boundary\n";
					$mensagem.= "Content-Type: ".$arquivo["type"]."\n";
					$mensagem.= "Content-Disposition: attachment; filename=\"".$arquivo["name"]."\"\n";
					$mensagem.= "Content-Transfer-Encoding: base64\n\n";
					$mensagem.= "$anexo\n";
					$mensagem.= "--$boundary--\r\n";

				} else {

					$headers.= "Content-Type: text/html; charset=ISO-8859-1 \r\n";
					$mensagem = $html;
				}

				$vai = mail("$email_recurso, $inscricao[email]", $subject, $mensagem, $headers);

				if($vai) {

					echo'<p class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						Recurso enviado com sucesso!
					</p>';

				} else {

					echo'<p class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							Erro ao enviar o recurso! Tente novamente mais tarde.
						</p>';
				}

			} else {

				echo'<p class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						N&atilde;o foi poss&iacute;vel enviar sua solicita&ccedil;&atilde;o! N&atilde;o encontramos nenhum e-mail de retorno! Entre em contato com '.$dadosCliente['nome_fantasia'].'!
					</p>';

			}
		}

	} else {

		echo'<p class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			O Captcha n&atilde;o foi resolvido! Verifique.
		</p>';

	}
}

if($_POST['numero_inscricao'] != "" || $_POST['cpf_inscricao'] != "") {

	$cap = new GoogleRecaptcha();
	$verified = $cap->VerifyCaptcha($_POST['g-recaptcha-response']);

	if(!$verified) {

		echo'<p class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				O Captcha n&atilde;o foi resolvido! Verifique.
			</p>';

	} else {

		if($_POST['numero_inscricao'] != "") {

			$stInscricao = $conn->prepare("SELECT * FROM concurso_inscricao WHERE id = :numero_inscricao AND id_vaga = :id_vaga AND id_cliente = :id_cliente AND status_registro = :status_registro");
			$stInscricao->execute(array("numero_inscricao" => $_POST['numero_inscricao'], "id_vaga" => $id_vaga, "id_cliente" => $cliente, "status_registro" => "A"));

		} else if($_POST['cpf_inscricao'] != "") {

			$stInscricao = $conn->prepare("SELECT * FROM concurso_inscricao WHERE cpf = :cpf_inscricao AND id_vaga = :id_vaga AND id_cliente = :id_cliente AND status_registro = :status_registro");
			$stInscricao->execute(array("cpf_inscricao" => $_POST['cpf_inscricao'], "id_vaga" => $id_vaga, "id_cliente" => $cliente, "status_registro" => "A"));

		}

		$inscricao = $stInscricao->fetch();
		if($inscricao["id"] != "") {

			$stBoleto = $conn->prepare("SELECT id FROM concurso_inscricao_boleto WHERE id_inscricao = :id_inscricao AND data_vencimento >= CURRENT_DATE() AND status_registro = :status_registro");
			$stBoleto->execute(array("id_inscricao" => $inscricao['id'], "status_registro" => "A"));
			$boleto = $stBoleto->fetch();

			$stBanco = $conn->prepare("SELECT numero FROM fi_banco WHERE id = (SELECT id_banco FROM concurso WHERE id = :id_concurso)");
			$stBanco->execute(array("id_concurso" => $inscricao["id_artigo"]));
			$banco = $stBanco->fetch();

			$numero_inscricao = str_pad($inscricao["id"], 6, "0", STR_PAD_LEFT);
			$numero_banco = str_pad($banco["numero"], 3, "0", STR_PAD_LEFT);

			?>
			<div class="panel panel-info">
				<div class="panel-heading">
			    	<h2 class="panel-title"><i class="glyphicon glyphicon-user"></i> INSCRI&Ccedil;&Atilde;O ENCONTRADA</h2>
			  	</div>
				<div class="panel-body">
					<p>N&uacute;mero da inscri&ccedil;&atilde;o: <span class="label label-default"><?= $numero_inscricao ?></span></p>
					<?php if($buscaConcurso['boleto'] == "S" && $boleto["id"] != "") { ?>
					<p><a href="<?= $CAMINHOCMGERAL ?>/impressao/boleto/<?= $numero_banco ?>.php?id=<?= $boleto['id'] ?>" target="_blank"><i class="glyphicon glyphicon-barcode"></i> Clique aqui para imprimir a 2&ordf; via do boleto.</a></p>
					<?php } ?>
					<p><a href="<?= $CAMINHOCMGERAL ?>/impressao/processo_seletivo.php?id=<?= $inscricao['id'] ?>&idCliente=<?= $cliente ?>" target="_blank"><i class="glyphicon glyphicon-print"></i> Clique aqui para imprimir a 2&ordf; via da ficha de inscri&ccedil;&atilde;o.</a></p>
					<button class="btn btn-danger" type="button" data-toggle="collapse" data-target="#recurso" aria-expanded="false" aria-controls="recurso">
						<i class="glyphicon glyphicon-list-alt"></i> Interpor Recurso
					</button>
					<form class="form-horizontal collapse" name="recurso" id="recurso" action="" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label for="arquivo_recurso" class="col-sm-2 control-label">Anexo</label>
							<div class="col-sm-10">
								<div class="input-group">
					                <span class="input-group-btn">
					                    <span class="btn btn-primary btn-file">
					                        <i class="glyphicon glyphicon-folder-open"></i> Selecionar&hellip;
					                        <input type="file" name="arquivo_recurso" id="arquivo_recurso" aria-describedby="texto_arquivo">
					                    </span>
					                </span>
					                <input type="text" class="form-control" readonly>
					            </div>
								<span id="texto_arquivo" class="help-block">Apenas arquivos no formato PDF s&atilde;o permitidos.</span>
							</div>
						</div>
						<div class="form-group">
							<label for="mensagem_recurso" class="col-sm-2 control-label">Descri&ccedil;&atilde;o</label>
							<div class="col-sm-10">
								<textarea name="mensagem_recurso" id="mensagem_recurso" class="form-control" rows="3"></textarea>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10 captcha" id="recaptcha2"></div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<input type="hidden" name="id_inscricao" value="<?= $inscricao['id'] ?>" />
								<button type="submit" class="btn btn-primary">Enviar</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="clearfix"></div>
			<?php

		} else {

			echo'<p class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					N&atilde;o foi encontrado nenhuma ficha de inscri&ccedil;&atilde;o com o numero <strong>'.$_POST[numero_inscricao].'</strong> ou o cpf <strong>'.$_POST[cpf_inscricao].'</strong> informados!
				</p>';

		}

	}

}
?>
<div class="row">
	<div class="col-sm-7">
		<p><strong>Concurso: </strong><?=$buscaConcurso['titulo'] ?></p>
	  	<p><strong>Vaga: </strong><?=$buscaConcurso['vaga'] ?></p>
	  	<? if($buscaConcurso['boleto'] == "S") { ?>
	  		<p><strong>Valor da Inscri&ccedil;&atilde;o: </strong>R$ <?=formata_valor($buscaConcurso['valor_inscricao']) ?></p>
	  		<p><strong>Vencimento do Boleto: </strong><?=formata_data($buscaConcurso['data_vencimento_boleto']) ?></p>
	  	<? } ?>
	</div>
	<div class="col-sm-5">
		<button class="btn btn-primary" type="button" data-toggle="modal" data-target="#area_candidato">
			<i class="glyphicon glyphicon-user"></i> &Aacute;rea do Candidato
		</button>

		<div class="modal fade" id="area_candidato">
  			<div class="modal-dialog">
  				<form class="form-horizontal" name="requerimento" action="" method="post">
	    			<div class="modal-content">
	      				<div class="modal-header">
	        				<button type="button" class="close" data-dismiss="modal" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>
	        				<h4 class="modal-title">&Aacute;rea do Candidato</h4>
	      				</div>
		      			<div class="modal-body">
							<p>Para imprimir segunda via da inscri&ccedil;&atilde;o ou do boleto, ou interpor um recurso, digite o n&uacute;mero da sua inscri&ccedil;&atilde;o ou seu CPF abaixo:</p>
							<div class="form-group">
								<label for="numero_inscricao" class="col-sm-4 control-label">N&ordm; Inscri&ccedil;&atilde;o</label>
								<div class="col-sm-8">
									<input type="text" name="numero_inscricao" id="numero_inscricao" class="form-control" value="<?= $_POST['numero_inscricao'] ?>" placeholder="Informe o n&uacute;mero da sua inscri&ccedil;&atilde;o...">
								</div>
							</div>
							<div class="form-group">
								<label for="cpf_inscricao" class="col-sm-4 control-label">CPF</label>
								<div class="col-sm-8">
									<input type="text" name="cpf_inscricao" id="cpf_inscricao" class="form-control cpf2" value="<?= $_POST['cpf_inscricao'] ?>" placeholder="Informe o n&uacute;mero de seu CPF...">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-4 col-sm-8 captcha" id="recaptcha1"></div>
							</div>
		      			</div>
		      			<div class="modal-footer">
		        			<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
		        			<button type="submit" class="btn btn-primary">Enviar</button>
		      			</div>
	    			</div>
    			</form>
  			</div>
		</div>
	</div>
</div>
<p class="clearfix"></p>
<?php if(!$cad && (strtotime(date("Y-m-d")) >= strtotime($buscaConcurso['data_inicio_inscricao']) && strtotime(date("Y-m-d")) <= strtotime($buscaConcurso['data_fim_inscricao'])) && empty($inscricao["id"])) { ?>
<form class="form-horizontal validar_formulario" action="" method="post" enctype="multipart/form-data">
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<h3 class="text-info">Informa&ccedil;&otilde;es Pessoais</h3>
		</div>
	</div>
	<div class="form-group">
		<label for="nome" class="col-sm-2 control-label">Nome</label>
		<div class="col-sm-10">
			<input type="text" name="nome" id="nome" class="form-control required" required value="<?= $nome ?>" placeholder="Informe seu nome...">
		</div>
	</div>
	<div class="form-group">
		<label for="data_nascimento" class="col-sm-2 control-label">Data de Nascimento</label>
		<div class="col-sm-10">
			<input type="text" name="data_nascimento" id="data_nascimento" class="form-control data required" required value="<?= $data_nascimento ?>" placeholder="Informe sua data de nascimento...">
		</div>
	</div>
	<div class="form-group">
		<label for="sexo" class="col-sm-2 control-label">Sexo</label>
		<div class="col-sm-10">
			<label class="radio-inline">
				<input type="radio" name="sexo" value="M" <? if($sexo == "M" || !$sexo) echo "checked"; ?>> Masculino
			</label>
			<label class="radio-inline">
				<input type="radio" name="sexo" value="F" <? if($sexo == "F") echo "checked"; ?>> Feminino
			</label>
		</div>
	</div>
	<div class="form-group">
		<label for="deficiencia_qual" class="col-sm-2 control-label">Possui Defici&ecirc;ncia?</label>
		<div class="col-sm-10">
			<div class="input-group">
		      	<span class="input-group-addon">
		        	<input type="checkbox" aria-label="Sim" name="deficiencia" value="S" <? if($deficiencia == "S") echo "checked"; ?>>
		      	</span>
		      	<input type="text" name="deficiencia_qual" id="deficiencia_qual" class="form-control" value="<?= $deficiencia_qual ?>" placeholder="Qual defici&ecirc;ncia?" aria-label="...">
			</div>
		</div>
	</div>
	<div class="form-group">
		<label for="telefone_fixo" class="col-sm-2 control-label">Telefone Fixo</label>
		<div class="col-sm-10">
			<input type="tel" name="telefone_fixo" id="telefone_fixo" class="form-control telefone required" required value="<?= $telefone_fixo ?>" placeholder="Informe seu telefone fixo...">
		</div>
	</div>
	<div class="form-group">
		<label for="telefone_celular" class="col-sm-2 control-label">Telefone Celular</label>
		<div class="col-sm-10">
			<input type="tel" name="telefone_celular" id="telefone_celular" class="form-control telefone required" required value="<?= $telefone_celular ?>" placeholder="Informe seu telefone celular...">
		</div>
	</div>
	<div class="form-group">
		<label for="email" class="col-sm-2 control-label">E-mail</label>
		<div class="col-sm-10">
			<input type="email" name="email" id="email" class="form-control email required" required value="<?= $email ?>" placeholder="Informe seu e-mail...">
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<h3 class="text-info">Documenta&ccedil;&atilde;o</h3>
		</div>
	</div>
	<div class="form-group">
		<label for="cpf" class="col-sm-2 control-label">CPF</label>
		<div class="col-sm-10">
			<input type="text" name="cpf" id="cpf" class="form-control cpf required" required value="<?= $cpf ?>" placeholder="Informe seu CPF...">
		</div>
	</div>
	<div class="form-group">
		<label for="rg" class="col-sm-2 control-label">RG</label>
		<div class="col-sm-4">
			<input type="text" name="rg" id="rg" class="form-control required" required value="<?= $rg ?>" placeholder="Informe seu RG...">
		</div>
		<div class="col-sm-3">
			<input type="text" name="data_expedicao" id="data_expedicao" class="form-control data required" required value="<?= $data_expedicao ?>" placeholder="Data Expedi&ccedil;&atilde;o">
		</div>
		<div class="col-sm-3">
			<input type="text" name="orgao_expedidor" id="orgao_expedidor" class="form-control required" required value="<?= $orgao_expedidor ?>" placeholder="&Oacute;rg&atilde;o Expedidor">
		</div>
	</div>
	<div class="form-group">
		<label for="arquivo" class="col-sm-2 control-label">Anexo</label>
		<div class="col-sm-10">
			<div class="input-group">
                <span class="input-group-btn">
                    <span class="btn btn-primary btn-file">
                        <i class="glyphicon glyphicon-folder-open"></i> Selecionar&hellip;
                        <input type="file" name="arquivo" id="arquivo">
                    </span>
                </span>
                <input type="text" class="form-control" readonly>
            </div>
			<span class="help-block">Use para anexar c&oacute;pias de documentos e/ou comprovantes quando solicitado no edital. Apenas arquivos nos formatos PDF, DOC, DOCX, TXT, JPG, JPEG s&atilde;o permitidos.</span>
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<h3 class="text-info">Filia&ccedil;&atilde;o</h3>
		</div>
	</div>
	<div class="form-group">
		<label for="nome_pai" class="col-sm-2 control-label">Nome do Pai</label>
		<div class="col-sm-10">
			<input type="text" name="nome_pai" id="nome_pai" class="form-control required" required value="<?= $nome_pai ?>" placeholder="Informe o nome do seu pai...">
		</div>
	</div>
	<div class="form-group">
		<label for="nome_mae" class="col-sm-2 control-label">Nome da M&atilde;e</label>
		<div class="col-sm-10">
			<input type="text" name="nome_mae" id="nome_mae" class="form-control required" required value="<?= $nome_mae ?>" placeholder="Informe o nome da sua m&atilde;e...">
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<h3 class="text-info">Endere&ccedil;o para Contato</h3>
		</div>
	</div>
	<div class="form-group">
		<label for="endereco" class="col-sm-2 control-label">Endere&ccedil;o</label>
		<div class="col-sm-10">
			<input type="text" name="endereco" id="endereco" class="form-control required" required value="<?= $endereco ?>" placeholder="Informe seu endere&ccedil;o...">
		</div>
	</div>
	<div class="form-group">
		<label for="bairro" class="col-sm-2 control-label">Bairro</label>
		<div class="col-sm-10">
			<input type="text" name="bairro" id="bairro" class="form-control required" required value="<?= $bairro ?>" placeholder="Informe seu bairro...">
		</div>
	</div>
	<div class="form-group">
		<label for="cep" class="col-sm-2 control-label">CEP</label>
		<div class="col-sm-10">
			<input type="text" name="cep" id="cep" class="form-control cep required" required value="<?= $cep ?>" placeholder="Informe seu CEP...">
		</div>
	</div>
	<div class="form-group">
		<label for="id_estado" class="col-sm-2 control-label">Estado</label>
		<div class="col-sm-10">
			<select name="id_estado" id="id_estado" class="form-control required" required>
				<option value="">Selecione o Estado</option>
				<?php
				if(count($qryEstado)) {

					foreach ($qryEstado as $buscaEstado) {

						echo "<option value='$buscaEstado[id]' ";
						if($buscaEstado['id'] == $id_estado) echo "selected";
						echo ">$buscaEstado[nome]</option>";
					}
				}
				?>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label for="id_municipio" class="col-sm-2 control-label">Munic&iacute;pio</label>
		<div class="col-sm-10">
			<p class="carregando form-control-static hidden">Aguarde, carregando...</p>
			<select name="id_municipio" id="id_municipio" class="form-control required" required>
				<option value="">Selecione o Munic&iacute;pio</option>
				<?php
				if($id_estado != "") {

					$stMunicipio = $conn->prepare("SELECT * FROM municipio WHERE id_estado = :id_estado AND status_registro = :status_registro ORDER BY nome");
					$stMunicipio->execute(array("id_estado" => $id_estado, "status_registro" => "A"));
					$qryMunicipio = $stMunicipio->fetchAll();

					if(count($qryMunicipio)) {

						foreach ($qryMunicipio as $municipio) {

							echo "<option value='$municipio[id]' ";
							if($municipio['id'] == $id_municipio) echo "selected";
							echo ">$municipio[nome]</option>";
						}

					}

				}
				?>
			</select>
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<h3 class="text-info">Escolaridade</h3>
		</div>
	</div>
	<div class="form-group">
		<label for="instituicao" class="col-sm-2 control-label">Nome da Institui&ccedil;&atilde;o</label>
		<div class="col-sm-10">
			<input type="text" name="instituicao" id="instituicao" class="form-control required" required value="<?= $instituicao ?>" placeholder="Informe o nome da institui&ccedil;&atilde;o de ensino que voc&ecirc; se formou...">
		</div>
	</div>
	<div class="form-group">
		<label for="curso" class="col-sm-2 control-label">Curso</label>
		<div class="col-sm-10">
			<input type="text" name="curso" id="curso" class="form-control" required value="<?= $curso ?>" placeholder="Informe o nome do curso...">
		</div>
	</div>
	<div class="form-group">
		<label for="periodo" class="col-sm-2 control-label">Per&iacute;odo / Turno</label>
		<div class="col-sm-10">
			<input type="text" name="periodo" id="periodo" class="form-control" required value="<?= $periodo ?>" placeholder="Informe o per&iacute;odo...">
		</div>
	</div>
	<div class="form-group">
		<label for="conclusao" class="col-sm-2 control-label">Previs&atilde;o Conclus&atilde;o do Curso</label>
		<div class="col-sm-10">
			<input type="text" name="conclusao" id="conclusao" class="form-control" required value="<?= $periodo ?>" placeholder="Informe o m&ecirc;s e ano da previs&atilde;o da conclus&atilde;o do seu curso...">
		</div>
	</div>
	<div class="form-group">
		<label for="nivel" class="col-sm-2 control-label">N&iacute;vel</label>
		<div class="col-sm-10">
			<select name="nivel" id="nivel" class="form-control" required>
				<option value="">Selecione o N&iacute;vel</option>
				<option <? if($nivel == "Fundamental") echo "selected"; ?>>Fundamental</option>
		  		<option <? if($nivel == "M&eacute;dio") echo "selected"; ?>>M&eacute;dio</option>
		  		<option <? if($nivel == "M&eacute;dio T&eacute;cnico") echo "selected"; ?>>M&eacute;dio T&eacute;cnico</option>
		  		<option <? if($nivel == "T&eacute;cnico") echo "selected"; ?>>T&eacute;cnico</option>
		  		<option <? if($nivel == "Superior") echo "selected"; ?>>Superior</option>
				<option <? if($nivel == "EJA") echo "selected"; ?>>EJA</option>
				<option <? if($nivel == "Gradua&ccedil;&atilde;o") echo "selected"; ?>>Gradua&ccedil;&atilde;o</option>
				<option <? if($nivel == "P&oacute;s-Gradua&ccedil;&atilde;o") echo "selected"; ?>>P&oacute;s-Gradua&ccedil;&atilde;o</option>
				<option <? if($nivel == "Mestrado") echo "selected"; ?>>Mestrado</option>
				<option <? if($nivel == "Doutorado") echo "selected"; ?>>Doutorado</option>
			</select>
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10 captcha" id="recaptcha2"></div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<input type="hidden" name="acao" value="cadastrar">
			<button type="reset" class="btn btn-danger">Limpar</button>
			<button type="submit" class="btn btn-primary">Enviar</button>
		</div>
	</div>
</form>
<?php
}