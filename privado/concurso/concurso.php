<h2>Concursos P&uacute;blicos e Testes Seletivos</h2>
<ol class="breadcrumb">
	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
	<li class="active">Concursos P&uacute;blicos</li>
</ol>

<div class="dropdown pull-left">
  	<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
    	<?= empty($_GET['ano']) ? "Selecione o Ano" : $_GET['ano'] ?>&nbsp;<span class="caret"></span>
  	</button>
	<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
	  	<li><a href="<?= $CAMINHO ?>/index.php?sessao=<?= $_GET['sessao'] ?>">Selecione o Ano</a></li>
  		
<?php
	$stAno = $conn->prepare("SELECT DISTINCT(YEAR(data_publicacao)) ano 
										FROM concurso 
									   WHERE id_cliente = :id_cliente 
									     AND status_registro = :status_registro 
									ORDER BY ano DESC");
	$stAno->execute(array("id_cliente" => $cliente, "status_registro" => "A"));
	$qryAno = $stAno->fetchAll();

	if(count($qryAno)) {
		foreach ($qryAno as $ano) {
?>

	    <li><a href="<?= $CAMINHO ?>/index.php?sessao=<?= $_GET['sessao'] ?>&ano=<?= $ano['ano'] ?><? if($_GET['finalizado'] == "S") echo "&finalizado=S"; ?>"><?= $ano['ano'] ?></a></li>
		
<?php 
		}  
	} 
?>

  	</ul>
</div>
<div class="dropdown pull-left">
  	<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
    	<?= $_GET['finalizado'] == "S" ? "Concursos Finalizados" : "Concursos em Andamento" ?>&nbsp;<span class="caret"></span>
  	</button>
		<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
	  	<li><a href="<?= $CAMINHO ?>/index.php?sessao=<?= $_GET['sessao'] ?><? if($_GET['ano'] != "") echo "&ano=" . $_GET['ano']; ?>">Concursos em Andamento</a></li>
	  	<li><a href="<?= $CAMINHO ?>/index.php?sessao=<?= $_GET['sessao'] ?><? if($_GET['ano'] != "") echo "&ano=" . $_GET['ano']; ?>&finalizado=S">Concursos Finalizados</a></li>
  	</ul>
</div>
<p class="clearfix"></p>

<?php

	$pagina = isset($_REQUEST['pagina']) ? $_REQUEST['pagina'] : 1;
	$max = 10;
	$inicio = $max * ($pagina - 1);

	$link = $CAMINHO . "/index.php?sessao=" . $_GET['sessao'];

	$sql = "SELECT * 
			  FROM concurso 
			 WHERE id_cliente = :id_cliente 
			   AND status_registro = :status_registro ";
	$vetor["id_cliente"] = $cliente;
	$vetor["status_registro"] = "A";

	if($_REQUEST['ano'] != "") {

		$sql .= "AND YEAR(data_publicacao) = :ano ";
		$vetor['ano'] = $_REQUEST['ano'];
		$link .= "&ano=" . $_REQUEST['ano'];
	}

	if($_REQUEST['finalizado'] == "S") {

		$sql .= "AND finalizado = :finalizado ";
		$vetor['finalizado'] = "S";
		$link .= "&finalizado=S";

	} else {

		$sql .= "AND (finalizado IS NULL OR finalizado = :finalizado) ";
		$vetor['finalizado'] = "N";

	}

	$stLinha = $conn->prepare($sql);
	$stLinha->execute($vetor);
	$qryLinha = $stLinha->fetchAll();
	$totalLinha = count($qryLinha);
	$ordenacao = " data_publicacao DESC, id DESC";

	if($cliente == '1065'){
		$ordenacao = " id DESC";
	}

	$sql .= "ORDER BY $ordenacao LIMIT $inicio, $max";

	$stConcurso = $conn->prepare($sql);
	$stConcurso->execute($vetor);
	$qryConcurso = $stConcurso->fetchAll();

	if(count($qryConcurso)) {
?>

<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
	
<?php 
		foreach ($qryConcurso as $concurso) { 
?>
	
	<div class="panel panel-default">
	    <div class="panel-heading" role="tab" id="heading_<?= $concurso['id'] ?>">
	     	<h4 class="panel-title">
	        	<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?= $concurso['id'] ?>" aria-expanded="true" aria-controls="collapse_<?= $concurso['id'] ?>">
	          		<i class="glyphicon glyphicon-triangle-right"></i> <strong><?= $concurso['titulo'] ?></strong>
	        	</a>
	    	</h4>
	    </div>
		<div id="collapse_<?= $concurso['id'] ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_<?= $concurso['id'] ?>">
			<div class="panel-body">
				<p><strong>Data da Publica&ccedil;&atilde;o:</strong> <?= formata_data($concurso['data_publicacao']) ?></p>
				<p><strong>Data da Prova:</strong> <?= formata_data($concurso['data_concurso']) ?></p>
				<p><strong>Email para Envio de Recurso:</strong> <?= $concurso['email_recurso'] ?></p>
				<?= verifica($concurso['artigo']) ?>
				
<?php
			$sqlRecurso = $conn->prepare("SELECT * 
							 				FROM concurso_data_recurso 
						    			   WHERE id_concurso = :id_concurso 
											 AND status_registro = :status_registro
										ORDER BY id ASC");
			$sqlRecurso->execute(array("id_concurso" => $concurso['id'], "status_registro" => "A"));
			if($sqlRecurso->rowCount() > 0){
?>
			<br>
			<h5 style="text-decoration: underline;">Recursos</h5>
			<ul>
<?
				$qryRecurso = $sqlRecurso->fetchAll();
				foreach($qryRecurso as $recurso){
?>

				<li><p><strong>Descri&ccedil;&atilde;o: </strong> <?= $recurso['recurso'] ?></p></li>
				<li><p><strong>Data de In&iacute;cio: </strong><?= formata_data($recurso['data_abertura_recurso']) ?></p></li>
				<li><p><strong>Data de Encerramento: </strong><?= formata_data($recurso['data_fechamento_recurso']) ?></p></li>

<?php
				}
?>

			</ul>

<?
			}
			
			$conf1 = (strtotime(date("Y-m-d")) >= strtotime($concurso['data_inicio_inscricao'])) && (strtotime(date("Y-m-d")) <= strtotime($concurso['data_fim_inscricao']));
			$conf2 = (strtotime(date("Y-m-d")) > strtotime($concurso['data_fim_inscricao'])) && (strtotime(date("Y-m-d")) <= strtotime($concurso['data_limite_recurso']));

			$stVaga = $conn->prepare("SELECT * 
										FROM concurso_vaga 
									   WHERE id_cliente = :id_cliente 
									     AND id_artigo = :id_artigo 
										 AND status_registro = :status_registro");
			$stVaga->execute(array("id_cliente" => $cliente, "id_artigo" => $concurso['id'], "status_registro" => "A"));
			$qryVaga = $stVaga->fetchAll();

			if($concurso['inscricao'] == "S" && ($conf1 || $conf2) && count($qryVaga)) {
?>

				<div class="panel panel-danger">
					<div class="panel-heading">
				    	<h2 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i> <?= $conf1 ? "FA&Ccedil;A SUA INSCRI&Ccedil;&Atilde;O" : "&Aacute;REA DO CANDIDATO" ?></h2>
				  	</div>
					<div class="panel-body">
						
<?php
				foreach ($qryVaga as $vaga) {
?>

						<p>
							<a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$concursoInscricao.$complemento); ?>&id_vaga=<?= $vaga['id'] ?>">
								<strong><i class="glyphicon glyphicon-triangle-right"></i>&nbsp;<?= $vaga['vaga'] ?></strong>
							</a>
						</p>
						
<?php 
				} 
?>
					
					</div>
				</div>
				
<?php 
			} 

			$stAnexo = $conn->prepare("SELECT * 
										 FROM concurso_anexo 
										WHERE id_artigo = :id_artigo 
									 ORDER BY ordem ASC, id DESC");
			$stAnexo->execute(array("id_artigo" => $concurso['id']));
			$qryAnexo = $stAnexo->fetchAll();

			if(count($qryAnexo)) {
?>

				<div class="panel panel-primary">
					<div class="panel-heading">
				    	<h2 class="panel-title"><i class="glyphicon glyphicon-cloud-download"></i> Anexos</h2>
				  	</div>
					<div class="panel-body">
					
<?php
					foreach ($qryAnexo as $anexo) {
?>

						<p><strong><a href="<?= $CAMINHOARQ ?>/<?= $anexo['arquivo'] ?>" target="_blank"><i class="glyphicon glyphicon-cloud-download"></i> <?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?></a></strong></p>
					
<?php 
					} 
?>

					</div>
				</div>

<?php 
			} 
				
			$stLink = $conn->prepare("SELECT * 
										FROM concurso_link 
									   WHERE id_artigo = :id_artigo 
									ORDER BY ordem ASC, id DESC");
			$stLink->execute(array("id_artigo" => $concurso['id']));
			$qryLink = $stLink->fetchAll();

			if(count($qryLink)) {
?>

				<div class="panel panel-primary">
					<div class="panel-heading">
				    	<h2 class="panel-title"><i class="glyphicon glyphicon-globe"></i> Links</h2>
				  	</div>
					<div class="panel-body">
					
<?php
				foreach ($qryLink as $linkConcurso) {
?>

						<p><strong><a href="<?= $linkConcurso['link'] ?>" target="_blank"><i class="glyphicon glyphicon-globe"></i> <?= empty($linkConcurso['descricao']) ? "Link" : $linkConcurso['descricao'] ?></a></strong></p>
					
<?php 
				} 
?>

					</div>
				</div>

<?php 
			} 
?>

			</div>
		</div>
	</div>
	
<?php 
		} 
?>

</div>

<?php
		$menos = $pagina - 1;
		$mais = $pagina + 1;
		$paginas = ceil($totalLinha / $max);
		if($paginas > 1) {
?>

<nav>
	<ul class="pagination">
		
<?php 
			if($pagina == 1) { 
?>

	    <li class="disabled"><a href="#" aria-label="Anterior"><span aria-hidden="true">&laquo;</span></a></li>
	    
<?php 
			} else { 
?>

	    <li><a href="<?= $link ?>&pagina=<?= $menos ?>" aria-label="Anterior"><span aria-hidden="true">&laquo;</span></a></li>
	    
<?php
			}

			if(($pagina - 4) < 1) 
				$anterior = 1;
			else 
				$anterior = $pagina - 4;

			if(($pagina + 4) > $paginas) 
				$posterior = $paginas;
			else 
				$posterior = $pagina + 4;

			for($i = $anterior; $i <= $posterior; $i++) {
				if($i != $pagina) {
?>

	    <li><a href="<?= $link ?>&pagina=<?= $i ?>"><?= $i ?></a></li>
	    	
<?php 
				} else { 
?>

	    <li class="active"><a href="#"><?= $i ?><span class="sr-only">(current)</span></a></li>
	   	
<?php
	    		}
	    	}

	    	if($mais <= $paginas) {
?>

	   	<li><a href="<?= $link ?>&pagina=<?= $mais ?>" aria-label="Pr&oacute;ximo"><span aria-hidden="true">&raquo;</span></a></li>
	   	
<?php 
			} 
?>

	</ul>
</nav>

<?php 
		} 
 	} else { 
?>

<h4>Nenhum registro encontrado.</h4>

<?php
	}
	$atualizacao = atualizacao("concurso", $cliente, $conn);
	if($atualizacao != "") {
?>

<p class="text-right"><small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small></p>

<?php
	}
