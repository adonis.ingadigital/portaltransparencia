<?php 
	$novoCliente = empty($_GET['nc']) ? $cliente : secure($_GET['nc']); 
?>
    <h2>Ata de Registro de Pre&ccedil;os <span class="label label-default">Baixar Anexo</span></h2>
    <ol class="breadcrumb">
        <li><a href="<?= $CAMINHO ?>">In&iacute;cio</a></li>
        <li>
        	<a href="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia . $ataRegistroPreco. $complemento); ?>&nc=<?= $novoCliente ?>">Ata de Registro de Pre&ccedil;os</a>
    	</li>
        <li class="active">Baixar Anexo</li>
    </ol>

<?php
if ($_GET['cadastro'] == "1") {

    echo '<p class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			Cadastro efetuado com sucesso!
		</p>';
}

$id = secure($_REQUEST["id"]);
$id_fornecedor = secure($_SESSION['id_fornecedor']);

if( $_POST['assinatura'] == 'on'){





$stAnexo = $conn->prepare("SELECT *
							 FROM ata_registro_preco_anexo
							WHERE ata_registro_preco_anexo.id = :id
						 ORDER BY ata_registro_preco_anexo.id DESC LIMIT 1");
$stAnexo->execute(array("id" => $id));
$anexo = $stAnexo->fetch();
if ($_SESSION['login_licitacao']) {
    ?>
    <p class="text-right">
        Voc&ecirc; est&aacute; logado como <strong><?= $_SESSION['razao_fornecedor'] ?></strong>&nbsp;
        <a href="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia . $ataRegistroPrecoCadastro . $complemento) ?>&nc=<?= $novoCliente ?>"
           class="label label-primary">Acessar minha conta.</a>
        <a href="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia . $ataRegistroPrecoCadastro . $complemento) ?>&nc=<?= $novoCliente ?>&acao=logout"><strong
                    class="text-danger"><i class="glyphicon glyphicon-off"></i> Sair.</strong></a>
    </p>
    <?php
}




if ($anexo['arquivo'] != "") {

    if ($_GET["confirm"] == "1" && $configuracaoTransparencia['permitir_download_sem_cadastro'] == "S") {

        echo "<p>&nbsp;</p><h3 class='text-primary'>Obrigado por baixar o arquivo.</h3>";
        echo "<p><strong><a href='$CAMINHOARQ/$anexo[arquivo]' target='_blank'>CLIQUE AQUI</a></strong> caso o download n&atilde;o inicie automaticamente.</p>";
        echo "<iframe src='$CAMINHOCMGERAL/licitacao/download.php?arquivo=$anexo[arquivo]&cliente=$novoCliente' width='100%' height='30' scrolling='no' frameborder='0' vspace='0' hspace='0'></iframe>";

    } else if ($id_fornecedor != '') {

        if ($_SESSION['login_licitacao'] == true) {
        	
            $stFornecedor = $conn->prepare("SELECT *
										  FROM geral_fornecedor
										 WHERE id = :id_fornecedor
										   AND id_cliente = :id_cliente
										   AND status_registro = :status_registro");
            $stFornecedor->execute(array("id_fornecedor" => $id_fornecedor, "id_cliente" => $novoCliente, "status_registro" => "A"));
            $fornecedor = $stFornecedor->fetch();

            $ok = true;

          

            if ($ok) {

                $stDownload = $conn->prepare("INSERT INTO ata_registro_preco_download (id_anexo,
																				 id_fornecedor,
																				 data,
																				 ip)
											  VALUES (?,
													  ?,
													  NOW(),
													  ?)");

                $stDownload->bindParam(1, $id, PDO::PARAM_INT);
                $stDownload->bindParam(2, $id_fornecedor, PDO::PARAM_INT);
                $stDownload->bindParam(3, get_client_ip(), PDO::PARAM_STR);
                $stDownload->execute();

                echo "<p>&nbsp;</p><h3 class='text-primary'>Obrigado por baixar o arquivo.</h3>";
                echo "<p><strong><a href='$CAMINHOARQ/$anexo[arquivo]' target='_blank'>CLIQUE AQUI</a></strong> caso o download n&atilde;o inicie automaticamente.</p>";
                echo "<iframe src='$CAMINHOCMGERAL/licitacao/download.php?arquivo=$anexo[arquivo]&cliente=$novoCliente' width='100%' height='30' scrolling='no' frameborder='0' vspace='0' hspace='0'></iframe>";

            }

        } else {

            echo '<p class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					Voc&ecirc; precisa estar logado para baixar este arquivo.
				</p>';

        }

    } else {

        echo "<p>&nbsp;</p><h3 class='text-primary'>Obrigado por baixar o arquivo.</h3>";
        echo "<p><strong><a href='$CAMINHOARQ/$anexo[arquivo]' target='_blank'>CLIQUE AQUI</a></strong> caso o download n&atilde;o inicie automaticamente.</p>";
        echo "<iframe src='$CAMINHOCMGERAL/licitacao/download.php?arquivo=$anexo[arquivo]&cliente=$novoCliente' width='100%' height='30' scrolling='no' frameborder='0' vspace='0' hspace='0'></iframe>";

    }

} else {

    echo '<p class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			Desculpe! O arquivo n&atilde;o foi encontrado.
		</p>';
}







} else {
?>


 <div class="col-md-offset-3 col-md-6">
           
          <form class="" action="" name="valida_ata" id="valida_ata" method="POST">
              <div class="panel panel-primary">
                <div class="panel-heading">Assinatura de Ata</div>
                <div class="panel-body">
                      <p>Para realizar o download é necessário assinar este documento.</p>
                        <div class="checkbox">
                          <label><input type="checkbox" name="assinatura" id="assinatura"> Assinar documento </label>
                        </div>
                </div>
                <div class="panel-footer">
                    <button type="submit" class="btn btn-default">Confirmar assinatura</button>
                </div>
              </div>
          </form>
 </div>


<? } ?>