<?php $novoCliente = empty($_GET['nc']) ? $cliente : secure($_GET['nc']); ?>
<h2>Ata de Registro de Pre&ccedil;os <span class="label label-default">Minha Conta</span></h2>
<ol class="breadcrumb">
	
	<li>
		<a href="<?= $CAMINHO ?>">In&iacute;cio</a>
	</li>

    <li>
        <a href="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia . $ataRegistroPreco. $complemento); ?>&nc=<?= $novoCliente ?>">Ata de Registro de Pre&ccedil;os</a>
    </li>
	<li class="active">Minha Conta</li>
</ol>
<?php
include "../../privado/transparencia/recaptchalib.php";

if($_SESSION['login_licitacao']) {

	$id = secure($_SESSION['id_fornecedor']);

	if($_GET['acao'] == "logout") {

		session_destroy();
		echo "<script>window.location='$CAMINHO/index.php?sessao=$sequencia".$ataRegistroPreco."$complemento&nc=$novoCliente'</script>";

	} else if($_POST['acao'] == "alterar_senha") {

		foreach ($_POST as $campo => $valor) {
			$$campo = secure($valor);
		}

		$cap = new GoogleRecaptcha();
		$verified = $cap->VerifyCaptcha($_POST['g-recaptcha-response']);

		$stConf = $conn->prepare("SELECT id FROM geral_fornecedor WHERE id = :id AND senha = :senha_antiga AND status_registro = :status_registro");
		$stConf->execute(array("id" => $id, "senha_antiga" => $senha_antiga, "status_registro" => "A"));
		$conf = $stConf->fetchAll();

		if(!$verified) {

			echo'<p class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					O Captcha n&atilde;o foi resolvido! Verifique.
				</p>';

		} else if(count($conf) < 1) {

			echo'<p class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					A senha atual n&atilde;o confere! Verifique.
				</p>';

		} else if(empty($senha) || ($senha != $conf_senha)) {

			echo'<p class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					As senhas informadas n&atilde;o conferem! Verifique.
				</p>';

		} else {

			$stMudaSenha = $conn->prepare("UPDATE geral_fornecedor SET senha = :senha WHERE id = :id");
			$stMudaSenha->bindParam("senha", $senha, PDO::PARAM_STR);
			$stMudaSenha->bindParam("id", $id, PDO::PARAM_INT);

			$cad = $stMudaSenha->execute();

			if($cad) {

				echo'<p class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						Senha alterada com sucesso.
					</p>';

			} else {

				echo'<p class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						N&atilde;o foi poss&iacute;vel alterar a senha no momento! Tente novamente mais tarde.
					</p>';

			}

		}

	} else if($_POST['acao'] == "cadastrar") {

		foreach ($_POST as $campo => $valor) {
			$$campo = secure($valor);
		}

		if($tipo == "F") {

			$sql = "SELECT id FROM geral_fornecedor WHERE id_cliente = :id_cliente AND (cpf = :cpf_cnpj OR cpf = :cpf_cnpj2) AND id <> :id AND status_registro = :status_registro";
			$cpf_cnpj = $cpf;

		} else {

			$sql = "SELECT id FROM geral_fornecedor WHERE id_cliente = :id_cliente AND (cnpj = :cpf_cnpj OR cnpj = :cpf_cnpj2) AND id <> :id AND status_registro = :status_registro";
			$cpf_cnpj = $cnpj;

		}

		$stConf = $conn->prepare($sql);
		$stConf->execute(array("id_cliente" => $novoCliente, "cpf_cnpj" => $cpf_cnpj, "cpf_cnpj2" => preg_replace('#[^0-9]#', '', $cpf_cnpj), "id" => $id, "status_registro" => "A"));
		$conf = $stConf->fetchAll();

		$cap = new GoogleRecaptcha();
		$verified = $cap->VerifyCaptcha($_POST['g-recaptcha-response']);

		if(!$verified) {

			echo'<p class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				O Captcha n&atilde;o foi resolvido! Verifique.
			</p>';

		} else if($tipo == "F" && count($conf) > 0) {

			echo'<p class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				Aten&ccedil;&atildeo! J&aacute; existe um cadastro com este CPF.
			</p>';

		} else if($tipo == "J" && count($conf) > 0) {

			echo'<p class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				Aten&ccedil;&atildeo! J&aacute; existe um cadastro com este CNPJ.
			</p>';

		} else if(empty($razao_social)) {

			echo'<p class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				Preencha o campo '.($tipo == "F" ? "Nome" : "Raz&atilde;o Social").'!
			</p>';

		} else if($tipo == "F" && validaCPF($cpf) == false) {

			echo'<p class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				O CPF informado &eacute; inv&aacute;lido!
			</p>';

		} else if($tipo == "J" && validaCNPJ($cnpj) == false) {

			echo'<p class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				O CNPJ informado &eacute; inv&aacute;lido!
			</p>';

		} else if(empty($email) || filter_var($email, FILTER_VALIDATE_EMAIL) === false) {

			echo'<p class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				Forne&ccedil;a um e-mail v&aacute;lido!
			</p>';

		} else if(empty($endereco) || empty($bairro) || empty($cep)) {

			echo'<p class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				Informe seu endere&ccedil;o, bairro e CEP!
			</p>';

		} else if(empty($id_municipio)) {

			echo'<p class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				Informe seu estado e sua cidade!
			</p>';

		} else if($tipo == "J" && empty($responsavel)) {

			echo'<p class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				Informe o nome do respons&aacute;vel da empresa!
			</p>';

		} else {

			$stCadastro = $conn->prepare("UPDATE geral_fornecedor SET id_porte_empresarial = ?,
																	  id_natureza_juridica = ?,
																	  tipo = ?,
																	  data_atualizacao = NOW(),
																	  data_inicio_atividade = ?,
																	  razao_social = ?,
																	  nome_fantasia = ?,
																	  endereco = ?,
																	  complemento = ?,
																	  bairro = ?,
																	  cep = ?,
																	  id_municipio = ?,
																	  rg = ?,
																	  cpf = ?,
																	  cnpj = ?,
																	  inscricao_estadual = ?,
																	  data_ultimo_registro_contrato_social = ?,
																	  numero_ultimo_registro_contrato_social = ?,
																	  telefone_fixo = ?,
																	  telefone_celular = ?,
																	  email = ?,
																	  responsavel = ?,
																	  cpf_responsavel = ?,
																	  rg_responsavel = ?,
																	  email_responsavel = ?,
																	  endereco_responsavel = ?,
																	  bairro_responsavel = ?,
																	  cep_responsavel = ?,
																	  id_municipio_responsavel = ?
																WHERE id = ?");

			$id_porte_empresarial = empty($id_porte_empresarial) ? NULL : $id_porte_empresarial;
			$id_natureza_juridica = empty($id_natureza_juridica) ? NULL : $id_natureza_juridica;
			$id_municipio_responsavel = empty($id_municipio_responsavel) ? NULL : $id_municipio_responsavel;
			$data_inicio_atividade = empty($data_inicio_atividade) ? NULL : formata_data_banco($data_inicio_atividade);
			$data_ultimo_registro_contrato_social = empty($data_ultimo_registro_contrato_social) ? NULL : formata_data_banco($data_ultimo_registro_contrato_social);

			$stCadastro->bindParam(1, $id_porte_empresarial, PDO::PARAM_INT);
			$stCadastro->bindParam(2, $id_natureza_juridica, PDO::PARAM_INT);
			$stCadastro->bindParam(3, $tipo, PDO::PARAM_STR);
			$stCadastro->bindParam(4, $data_inicio_atividade, PDO::PARAM_STR);
			$stCadastro->bindParam(5, $razao_social, PDO::PARAM_STR);
			$stCadastro->bindParam(6, $nome_fantasia, PDO::PARAM_STR);
			$stCadastro->bindParam(7, $endereco, PDO::PARAM_STR);
			$stCadastro->bindParam(8, $complemento_endereco, PDO::PARAM_STR);
			$stCadastro->bindParam(9, $bairro, PDO::PARAM_STR);
			$stCadastro->bindParam(10, $cep, PDO::PARAM_STR);
			$stCadastro->bindParam(11, $id_municipio, PDO::PARAM_INT);
			$stCadastro->bindParam(12, $rg, PDO::PARAM_STR);
			$stCadastro->bindParam(13, $cpf, PDO::PARAM_STR);
			$stCadastro->bindParam(14, $cnpj, PDO::PARAM_STR);
			$stCadastro->bindParam(15, $inscricao_estadual, PDO::PARAM_STR);
			$stCadastro->bindParam(16, $data_ultimo_registro_contrato_social, PDO::PARAM_STR);
			$stCadastro->bindParam(17, $numero_ultimo_registro_contrato_social, PDO::PARAM_STR);
			$stCadastro->bindParam(18, $telefone_fixo, PDO::PARAM_STR);
			$stCadastro->bindParam(19, $telefone_celular, PDO::PARAM_STR);
			$stCadastro->bindParam(20, $email, PDO::PARAM_STR);
			$stCadastro->bindParam(21, $responsavel, PDO::PARAM_STR);
			$stCadastro->bindParam(22, $cpf_responsavel, PDO::PARAM_STR);
			$stCadastro->bindParam(23, $rg_responsavel, PDO::PARAM_STR);
			$stCadastro->bindParam(24, $email_responsavel, PDO::PARAM_STR);
			$stCadastro->bindParam(25, $endereco_responsavel, PDO::PARAM_STR);
			$stCadastro->bindParam(26, $bairro_responsavel, PDO::PARAM_STR);
			$stCadastro->bindParam(27, $cep_responsavel, PDO::PARAM_STR);
			$stCadastro->bindParam(28, $id_municipio_responsavel, PDO::PARAM_INT);
			$stCadastro->bindParam(29, $id, PDO::PARAM_INT);

			$cad = $stCadastro->execute();

			if($cad) {

				if(array_key_exists('id_socio', $_POST)) {

					$ids = $_POST['id_socio'];
					$idList = implode(',', $ids);

				} else {

					$idList = "";
				}

				$stExcluiSocio = $conn->prepare("UPDATE geral_fornecedor_socio SET status_registro = :status_registro WHERE id_fornecedor = :id_fornecedor AND NOT FIND_IN_SET(id, :id_list)");
				$stExcluiSocio->execute(array("status_registro" => "I", "id_fornecedor" => $id, "id_list" => $idList));

				foreach ($_POST['id_socio'] as $indice => $idSocio) {

					$stUpSocio = $conn->prepare("UPDATE geral_fornecedor_socio SET razao_social = ?, 
																				   cnpj_cpf = ?
																		     WHERE id = ?");

					$stUpSocio->bindValue(1, secure($_POST['nome_socio_alterar'][$indice]), PDO::PARAM_STR);
					$stUpSocio->bindValue(2, secure($_POST['cpf_socio_alterar'][$indice]), PDO::PARAM_STR);
					$stUpSocio->bindValue(3, $idSocio, PDO::PARAM_INT);
					$stUpSocio->execute();

				}

				foreach ($_POST['nome_socio'] as $indice => $nomeSocio) {

					$stSocio = $conn->prepare("INSERT INTO geral_fornecedor_socio (id_fornecedor,
																				   tipo,
																				   data_cadastro,
																				   razao_social,
																				   cnpj_cpf) 
                                                VALUES (?,
                                                        ?,
                                                        NOW(),
                                                        ?,
                                                        ?)");

					$stSocio->bindValue(1, $id, PDO::PARAM_INT);
					$stSocio->bindValue(2, "F", PDO::PARAM_STR);
					$stSocio->bindValue(3, secure($nomeSocio), PDO::PARAM_STR);
					$stSocio->bindValue(4, secure($_POST['cpf_socio'][$indice]), PDO::PARAM_STR);
					$stSocio->execute();

				}

				echo'<p class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						Dados alterados com sucesso.
					</p>';

			} else {

				echo'<p class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						N&atilde;o foi poss&iacute;vel alterar os dados! Tente novamente mais tarde.
					</p>';

			}

		}

	} else if($_POST['acao'] == "excluir_atividade") {

		if($_POST['id_atividade']) {

			try{
				$sqlup = $conn->prepare("UPDATE geral_fornecedor_atividade_economica
										SET status_registro = :status_registro
									  WHERE id_fornecedor = :id_fornecedor
										AND id_atividade_economica = :id_atividade");

				$sqlup->execute(array("status_registro" => "I", "id_fornecedor" => $id, "id_atividade" => $_POST['id_atividade']));

				echo'<p class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						Atividade econ&ocirc;mica excluida com sucesso.
					</p>';

			}catch (PDOException $e){
				echo'<p class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				Aten&ccedil;&atildeo! Erro ao excluir o registro.
			</p>';
			}


		}

	} else if($_POST['acao'] == "cadastrar_atividade") {

		if($_REQUEST['id_atividade']) {

			foreach ($_POST as $campo => $valor) {
				$$campo = secure($valor);
			}

			$principal = ($_REQUEST['principal'] == 'S' ? 'S' : 'N');

			$sqlito = $conn->prepare("SELECT id
													FROM geral_fornecedor_atividade_economica
												   WHERE id_fornecedor = :id
													 AND id_atividade_economica = :id_atividade
													 AND status_registro = :status_registro");

			$sqlito->execute(array("id" => $id, "status_registro" => "A", "id_atividade" => $id_atividade));
			$qryLinha = $sqlito->fetchAll();

			if(count($qryLinha)) {

				echo'<p class="alert alert-warning">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						A atividade selecionada j&aacute; est&aacute; cadastrada!
					</p>';

			} else {

				if($principal == 'S') {
					$sqlup = $conn->prepare("UPDATE geral_fornecedor_atividade_economica
								SET principal = :principal
								WHERE id_fornecedor = :id_fornecedor");

					$sqlup->execute(array("principal" => "N", "id_fornecedor" => $id));
				}

				$stCadastro = $conn->prepare("INSERT INTO geral_fornecedor_atividade_economica 
																  SET id_fornecedor = :id_fornecedor,
															   	 	  id_atividade_economica = :id_atividade_economica,
																 	  principal = :principal");

				$stCadastro->bindValue("id_fornecedor", $id, PDO::PARAM_STR);
				$stCadastro->bindValue("id_atividade_economica", $id_atividade, PDO::PARAM_STR);
				$stCadastro->bindValue("principal", $principal, PDO::PARAM_STR);
				$cadastro = $stCadastro->execute();

				if($cadastro) {
					echo'<p class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						Atividade econ&ocirc;mica adicionada com sucesso!
					</p>';

				} else {
					echo'<p class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						N&atilde;o foi poss&iacute;vel adicionar a atividade econ&ocirc;mica!
					</p>';
				}
			}
		}
	}

	$stFornecedor = $conn->prepare("SELECT geral_fornecedor.*,
											m1.id_estado,
											m2.id_estado id_estado_responsavel,
											geral_natureza_juridica.id_tipo_natureza_juridica
									   FROM geral_fornecedor
								  LEFT JOIN geral_natureza_juridica ON geral_fornecedor.id_natureza_juridica = geral_natureza_juridica.id
								  LEFT JOIN municipio m1 ON geral_fornecedor.id_municipio = m1.id
								  LEFT JOIN municipio m2 ON geral_fornecedor.id_municipio_responsavel = m2.id
									  WHERE geral_fornecedor.id = :id
										AND geral_fornecedor.id_cliente = :id_cliente
										AND geral_fornecedor.status_registro = :status_registro");

	$stFornecedor->execute(array("id" => $id, "id_cliente" => $novoCliente, "status_registro" => "A"));
	$fornecedor = $stFornecedor->fetch();

?>
<p class="text-right">
	Voc&ecirc; est&aacute; logado como <strong><?=$_SESSION['razao_fornecedor'] ?></strong>&nbsp;
	<a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$licitacaoConta.$complemento) ?>&nc=<?= $novoCliente ?>&acao=logout"><strong class="text-danger"><i class="glyphicon glyphicon-off"></i> Sair.</strong></a>
</p>
<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#altera_senha"><i class="glyphicon glyphicon-lock"></i> Alterar senha</button>
<button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#form-fornecedor" aria-expanded="false" aria-controls="form-fornecedor"><i class="glyphicon glyphicon-pencil"></i> Alterar cadastro</button>

<form class="form-horizontal collapse validar_formulario_com_senha" id="form-fornecedor" action="" method="post">
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<h3 class="text-info">Identifica&ccedil;&atilde;o</h3>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">Pessoa</label>
		<div class="col-sm-10">
			<label class="radio-inline">
				<input type="radio" name="tipo" id="fornecedor_J" value="J" checked> Jur&iacute;dica
			</label>
			<label class="radio-inline">
				<input type="radio" name="tipo" id="fornecedor_F" value="F"> F&iacute;sica
			</label>
			<?php if($fornecedor['tipo'] == "F") { ?>
			<script>
			jQuery(document).ready(function(){

				jQuery("#fornecedor_F").click();
			});
			</script>
			<?php } ?>
		</div>
	</div>
	<div class="form-group for_jur">
		<label for="id_porte_empresarial" class="col-sm-2 control-label">Porte Empresarial</label>
		<div class="col-sm-10">
			<select name="id_porte_empresarial" id="id_porte_empresarial" class="form-control required" required>
				<option value="">Selecione o porte empresarial</option>
				<?php
				$stPorte = $conn->prepare("SELECT * FROM geral_porte_empresarial WHERE status_registro = :status_registro ORDER BY id");
				$stPorte->execute(array("status_registro" => "A"));
				$qryPorte = $stPorte->fetchAll();

				if(count($qryPorte)) {

					foreach ($qryPorte as $porteEmpresarial) {

						echo "<option value='$porteEmpresarial[id]' ";
						if($porteEmpresarial['id'] == $fornecedor['id_porte_empresarial']) echo "selected";
						echo ">$porteEmpresarial[sigla] - $porteEmpresarial[descricao]</option>";
					}
				}
				?>
			</select>
		</div>
	</div>
	<div class="form-group for_jur">
		<label for="id_tipo_natureza_juridica" class="col-sm-2 control-label">Tipo de Natureza Jur&iacute;dica</label>
		<div class="col-sm-10">
			<select name="id_tipo_natureza_juridica" id="id_tipo_natureza_juridica" class="form-control required" required>
				<option value="">Selecione o tipo</option>
				<?php
				$stTipoNatureza = $conn->prepare("SELECT * FROM geral_tipo_natureza_juridica WHERE status_registro = :status_registro ORDER BY id");
				$stTipoNatureza->execute(array("status_registro" => "A"));
				$qryTipoNatureza = $stTipoNatureza->fetchAll();

				if(count($qryTipoNatureza)) {

					foreach ($qryTipoNatureza as $tipoNatureza) {

						echo "<option value='$tipoNatureza[id]' ";
						if($tipoNatureza['id'] == $fornecedor['id_tipo_natureza_juridica']) echo "selected";
						echo ">$tipoNatureza[descricao]</option>";
					}
				}
				?>
			</select>
		</div>
	</div>
	<div class="form-group for_jur">
		<label for="id_natureza_juridica" class="col-sm-4 col-md-2 control-label">Natureza Jur&iacute;dica</label>
		<div class="col-sm-8 col-md-10">
			<p class="carregando_natureza form-control-static hidden">Aguarde, carregando...</p>
			<select name="id_natureza_juridica" id="id_natureza_juridica" class="form-control required" required>
				<option value="">Selecione a natureza jur&iacute;dica</option>
				<?php
				$stNatureza = $conn->prepare("SELECT * FROM geral_natureza_juridica WHERE id_tipo_natureza_juridica = :id_tipo_natureza_juridica AND status_registro = :status_registro ORDER BY id");
				$stNatureza->execute(array("id_tipo_natureza_juridica" => $fornecedor['id_tipo_natureza_juridica'], "status_registro" => "A"));
				$qryNatureza = $stNatureza->fetchAll();

				if(count($qryNatureza)) {

					foreach ($qryNatureza as $natureza) {

						echo "<option value='$natureza[id]' ";
						if($natureza['id'] == $fornecedor['id_natureza_juridica']) echo "selected";
						echo ">$natureza[codigo] - $natureza[denominacao]</option>";
					}
				}
				?>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label for="razao_social" class="col-sm-2 control-label label_razao">Raz&atilde;o Social</label>
		<div class="col-sm-10">
			<input type="text" name="razao_social" id="razao_social" class="form-control required" required value="<?= $fornecedor['razao_social'] ?>" placeholder="Informe sua raz&atilde;o social...">
		</div>
	</div>
	<div class="form-group for_jur">
		<label for="nome_fantasia" class="col-sm-2 control-label">Nome Fantasia</label>
		<div class="col-sm-10">
			<input type="text" name="nome_fantasia" id="nome_fantasia" class="form-control" value="<?= $fornecedor['nome_fantasia'] ?>" placeholder="Informe seu nome fantasia...">
		</div>
	</div>
	<div class="form-group for_jur">
		<label for="cnpj" class="col-sm-2 control-label">CNPJ</label>
		<div class="col-sm-10">
			<input type="text" name="cnpj" id="cnpj" class="form-control cnpj" value="<?= $fornecedor['cnpj'] ?>" placeholder="Informe seu CNPJ...">
		</div>
	</div>
	<div class="form-group for_jur">
		<label for="inscricao_estadual" class="col-sm-2 control-label">Inscri&ccedil;&atilde;o Estadual</label>
		<div class="col-sm-10">
			<input type="text" name="inscricao_estadual" id="inscricao_estadual" class="form-control required" required value="<?= $fornecedor['inscricao_estadual'] ?>" placeholder="Informe sua inscri&ccedil;&atilde;o estadual...">
		</div>
	</div>
	<div class="form-group for_jur">
		<label for="data_inicio_atividade" class="col-sm-2 control-label">In&iacute;cio das Atividades da Empresa</label>
		<div class="col-sm-10">
			<input type="text" name="data_inicio_atividade" id="data_inicio_atividade" class="form-control data required" required value="<?= formata_data($fornecedor['data_inicio_atividade']) ?>" placeholder="Informe a data do in&iacute;cio das atividades da empresa...">
		</div>
	</div>
	<div class="form-group for_jur">
		<label for="data_ultimo_registro_contrato_social" class="col-sm-2 control-label">Data do &Uacute;ltimo Registro do Contrato Social na Junta Comercial</label>
		<div class="col-sm-10">
			<input type="text" name="data_ultimo_registro_contrato_social" id="data_ultimo_registro_contrato_social" class="form-control data required" required value="<?= formata_data($fornecedor['data_ultimo_registro_contrato_social']) ?>" placeholder="Informe a data do &uacute;ltimo registro do contrato social na Junta Comercial...">
		</div>
	</div>
	<div class="form-group for_jur">
		<label for="numero_ultimo_registro_contrato_social" class="col-sm-2 control-label">N&uacute;mero do &Uacute;ltimo Registro do Contrato Social na Junta Comercial</label>
		<div class="col-sm-10">
			<input type="text" name="numero_ultimo_registro_contrato_social" id="numero_ultimo_registro_contrato_social" class="form-control required" required value="<?= $fornecedor['numero_ultimo_registro_contrato_social'] ?>" placeholder="Informe o n&uacute;mero do &uacute;ltimo registro do contrato social na Junta Comercial...">
		</div>
	</div>
	<div class="form-group for_fis hidden">
		<label for="cpf" class="col-sm-2 control-label">CPF</label>
		<div class="col-sm-10">
			<input type="text" name="cpf" id="cpf" class="form-control cpf required" required value="<?= $fornecedor['cpf'] ?>" placeholder="Informe seu CPF...">
		</div>
	</div>
	<div class="form-group for_fis hidden">
		<label for="rg" class="col-sm-2 control-label">RG</label>
		<div class="col-sm-10">
			<input type="text" name="rg" id="rg" class="form-control required" required value="<?= $fornecedor['rg'] ?>" placeholder="Informe seu RG...">
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<h3 class="text-info">Informa&ccedil;&otilde;es de Contato</h3>
		</div>
	</div>
	<div class="form-group">
		<label for="endereco" class="col-sm-2 control-label">Endere&ccedil;o</label>
		<div class="col-sm-10">
			<input type="text" name="endereco" id="endereco" class="form-control required" required value="<?= $fornecedor['endereco'] ?>" placeholder="Informe seu endere&ccedil;o...">
		</div>
	</div>
	<div class="form-group">
		<label for="complemento_endereco" class="col-sm-2 control-label">Complemento</label>
		<div class="col-sm-10">
			<input type="text" name="complemento_endereco" id="complemento_endereco" class="form-control" value="<?= $fornecedor['complemento'] ?>" placeholder="Informe o complemento do seu endere&ccedil;o...">
		</div>
	</div>
	<div class="form-group">
		<label for="bairro" class="col-sm-2 control-label">Bairro</label>
		<div class="col-sm-10">
			<input type="text" name="bairro" id="bairro" class="form-control required" required value="<?= $fornecedor['bairro'] ?>" placeholder="Informe seu bairro...">
		</div>
	</div>
	<div class="form-group">
		<label for="cep" class="col-sm-2 control-label">CEP</label>
		<div class="col-sm-10">
			<input type="text" name="cep" id="cep" class="form-control cep required" required value="<?= $fornecedor['cep'] ?>" placeholder="Informe seu CEP...">
		</div>
	</div>
	<div class="form-group">
		<label for="id_estado" class="col-sm-2 control-label">Estado</label>
		<div class="col-sm-10">
			<select name="id_estado" id="id_estado" class="form-control required" required>
				<option value="">Selecione o estado</option>
				<?php
				$stEstado = $conn->prepare("SELECT * FROM estado WHERE status_registro = :status_registro ORDER BY nome");
				$stEstado->execute(array("status_registro" => "A"));
				$qryEstado = $stEstado->fetchAll();

				if(count($qryEstado)) {

					foreach ($qryEstado as $buscaEstado) {

						echo "<option value='$buscaEstado[id]' ";
						if($buscaEstado['id'] == $fornecedor['id_estado']) echo "selected";
						echo ">$buscaEstado[nome]</option>";
					}
				}
				?>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label for="id_municipio" class="col-sm-2 control-label">Munic&iacute;pio</label>
		<div class="col-sm-10">
			<p class="carregando form-control-static hidden">Aguarde, carregando...</p>
			<select name="id_municipio" id="id_municipio" class="form-control required" required>
				<option value="">Selecione o munic&iacute;pio</option>
				<?php
				$stMunicipio = $conn->prepare("SELECT * FROM municipio WHERE id_estado = :id_estado AND status_registro = :status_registro ORDER BY nome");
				$stMunicipio->execute(array("id_estado" => $fornecedor['id_estado'], "status_registro" => "A"));
				$qryMunicipio = $stMunicipio->fetchAll();

				if(count($qryMunicipio)) {

					foreach ($qryMunicipio as $municipio) {

						echo "<option value='$municipio[id]' ";
						if($municipio['id'] == $fornecedor['id_municipio']) echo "selected";
						echo ">$municipio[nome]</option>";
					}

				}
				?>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label for="telefone_fixo" class="col-sm-2 control-label">Telefone Fixo</label>
		<div class="col-sm-10">
			<input type="tel" name="telefone_fixo" id="telefone_fixo" class="form-control telefone" value="<?= $fornecedor['telefone_fixo'] ?>" placeholder="Informe seu telefone fixo...">
		</div>
	</div>
	<div class="form-group">
		<label for="telefone_celular" class="col-sm-2 control-label">Telefone Celular</label>
		<div class="col-sm-10">
			<input type="tel" name="telefone_celular" id="telefone_celular" class="form-control telefone" value="<?= $fornecedor['telefone_celular'] ?>" placeholder="Informe seu telefone celular...">
		</div>
	</div>
	<div class="form-group">
		<label for="email" class="col-sm-2 control-label">E-mail</label>
		<div class="col-sm-10">
			<input type="email" name="email" id="email" class="form-control email required" required value="<?= $fornecedor['email'] ?>" placeholder="Informe seu e-mail...">
		</div>
	</div>
	<div class="form-group for_jur">
		<div class="col-sm-offset-2 col-sm-10">
			<h3 class="text-info">Informa&ccedil;&otilde;es do Respons&aacute;vel</h3>
		</div>
	</div>
	<div class="form-group for_jur">
		<label for="responsavel" class="col-sm-2 control-label">Nome Completo do Respons&aacute;vel</label>
		<div class="col-sm-10">
			<input type="text" name="responsavel" id="responsavel" class="form-control required" required value="<?= $fornecedor['responsavel'] ?>" placeholder="Informe o nome completo do respons&aacute;vel pela empresa...">
		</div>
	</div>
	<div class="form-group for_jur">
		<label for="cpf_responsavel" class="col-sm-2 control-label">CPF do Respons&aacute;vel</label>
		<div class="col-sm-10">
			<input type="text" name="cpf_responsavel" id="cpf_responsavel" class="form-control cpf" value="<?= $fornecedor['cpf_responsavel'] ?>" placeholder="Informe o CPF do respons&aacute;vel pela empresa...">
		</div>
	</div>
	<div class="form-group for_jur">
		<label for="rg_responsavel" class="col-sm-2 control-label">RG do Respons&aacute;vel</label>
		<div class="col-sm-10">
			<input type="text" name="rg_responsavel" id="rg_responsavel" class="form-control" value="<?= $fornecedor['rg_responsavel'] ?>" placeholder="Informe o RG do respons&aacute;vel pela empresa...">
		</div>
	</div>
	<div class="form-group for_jur">
		<label for="email_responsavel" class="col-sm-2 control-label">E-mail do Respons&aacute;vel</label>
		<div class="col-sm-10">
			<input type="email" name="email_responsavel" id="email_responsavel" class="form-control email" value="<?= $fornecedor['email_responsavel'] ?>" placeholder="Informe o e-mail do respons&aacute;vel pela empresa...">
		</div>
	</div>
	<div class="form-group for_jur">
		<label for="endereco_responsavel" class="col-sm-2 control-label">Endere&ccedil;o do Respons&aacute;vel</label>
		<div class="col-sm-10">
			<input type="text" name="endereco_responsavel" id="endereco_responsavel" class="form-control required" required value="<?= $fornecedor['endereco_responsavel'] ?>" placeholder="Informe o endere&ccedil;o completo do respons&aacute;vel pela empresa...">
		</div>
	</div>
	<div class="form-group">
		<label for="bairro_responsavel" class="col-sm-2 control-label">Bairro do Respons&aacute;vel</label>
		<div class="col-sm-10">
			<input type="text" name="bairro_responsavel" id="bairro_responsavel" class="form-control required" required value="<?= $fornecedor['bairro_responsavel'] ?>" placeholder="Informe o bairro do respons&aacute;vel pela empresa...">
		</div>
	</div>
	<div class="form-group">
		<label for="cep_responsavel" class="col-sm-2 control-label">CEP do Respons&aacute;vel</label>
		<div class="col-sm-10">
			<input type="text" name="cep_responsavel" id="cep_responsavel" class="form-control cep required" required value="<?= $fornecedor['cep_responsavel'] ?>" placeholder="Informe o CEP do respons&aacute;vel pela empresa...">
		</div>
	</div>
	<div class="form-group for_jur">
		<label for="id_estado_responsavel" class="col-sm-2 control-label">Estado do Respons&aacute;vel</label>
		<div class="col-sm-10">
			<select name="id_estado_responsavel" id="id_estado_responsavel" class="form-control required" required>
				<option value="">Selecione o estado do respons&aacute;vel</option>
				<?php
				if(count($qryEstado)) {

					foreach ($qryEstado as $buscaEstado) {

						echo "<option value='$buscaEstado[id]' ";
						if($buscaEstado['id'] == $fornecedor['id_estado_responsavel']) echo "selected";
						echo ">$buscaEstado[nome]</option>";
					}
				}
				?>
			</select>
		</div>
	</div>
	<div class="form-group for_jur">
		<label for="id_municipio_responsavel" class="col-sm-2 control-label">Munic&iacute;pio do Respons&aacute;vel</label>
		<div class="col-sm-10">
			<p class="carregando_responsavel form-control-static hidden">Aguarde, carregando...</p>
			<select name="id_municipio_responsavel" id="id_municipio_responsavel" class="form-control required" required>
				<option value="">Selecione o munic&iacute;pio do respons&aacute;vel</option>
				<?php
				$stMunicipio = $conn->prepare("SELECT * FROM municipio WHERE id_estado = :id_estado AND status_registro = :status_registro ORDER BY nome");
				$stMunicipio->execute(array("id_estado" => $fornecedor['id_estado_responsavel'], "status_registro" => "A"));
				$qryMunicipio = $stMunicipio->fetchAll();

				if(count($qryMunicipio)) {

					foreach ($qryMunicipio as $municipio) {

						echo "<option value='$municipio[id]' ";
						if($municipio['id'] == $fornecedor['id_municipio_responsavel']) echo "selected";
						echo ">$municipio[nome]</option>";
					}

				}
				?>
			</select>
		</div>
	</div>
	<div class="form-group for_jur">
		<div class="col-sm-offset-2 col-sm-10">
			<h3 class="text-info">S&oacute;cios</h3>
		</div>
	</div>
	<?php
	$stSocio = $conn->prepare("SELECT * FROM geral_fornecedor_socio WHERE id_fornecedor = :id_fornecedor AND status_registro = :status_registro");
	$stSocio->execute(array("id_fornecedor" => $fornecedor['id'], "status_registro" => "A"));
	$qrySocio = $stSocio->fetchAll();
	if(count($qrySocio)) {

		foreach ($qrySocio as $socio) {
	?>
	<div class="for_jur">
		<div class="form-group">
			<label class="col-sm-2 control-label">Nome Completo</label>
			<div class="col-sm-10">
				<input type="text" name="nome_socio_alterar[]" value="<?= $socio['razao_social'] ?>" class="form-control" placeholder="Informe o nome completo do s&oacute;cio...">
				<input type="hidden" name="id_socio[]" value="<?= $socio['id'] ?>">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">CPF</label>
			<div class="col-sm-10">
				<div class="input-group">
					<input type="text" name="cpf_socio_alterar[]" value="<?= $socio['cnpj_cpf'] ?>" class="form-control cpf" placeholder="Informe o CPF do s&oacute;cio...">
					<span class="input-group-btn">
						<button type="button" class="btn btn-danger" onclick="jQuery(this).parent().parent().parent().parent().parent().fadeOut('slow', function(){jQuery(this).remove();})"><i class="glyphicon glyphicon-trash"></i></button>
					</span>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>
	<?php } ?>
	<div class="form-group for_jur" id="mais_socio">
		<div class="col-sm-offset-2 col-sm-10">
			<button type="button" class="btn btn-success pull-right" id="add_socio"><i class="glyphicon glyphicon-plus-sign"></i> Adicionar S&oacute;cio</button>
		</div>
	</div>

	<div class="form-group for_jur">
		<div class="col-sm-offset-2 col-sm-10">
			<h3 class="text-info">Atividades Econ&ocirc;micas</h3>
		</div>
	</div>
	<div class="for_jur table-responsive">

		<table class="table table-striped table-hover table-bordered table-condensed">
		<tr>
			<th>&nbsp;</th>
			<th>Se&ccedil;&atilde;o</th>
			<th>Divis&atilde;o</th>
			<th>Grupo</th>
			<th>Classe</th>
			<th>Subclasse</th>
			<th>Denomina&ccedil;&atilde;o</th>
		</tr>
		<?php
		$pagina = isset($_REQUEST['pagina']) ? $_REQUEST['pagina'] : 1;
		$max = 10;
		$inicio = $max * ($pagina - 1);

		$sql = $conn->prepare("SELECT geral_atividade_economica.*,
														 geral_fornecedor_atividade_economica.id id_fornecedor_atividade_economica,
														 geral_fornecedor_atividade_economica.principal
												    FROM geral_atividade_economica,
														 geral_fornecedor_atividade_economica
											 	   WHERE geral_fornecedor_atividade_economica.id_atividade_economica = geral_atividade_economica.id
													 AND geral_fornecedor_atividade_economica.id_fornecedor = :id
													 AND geral_fornecedor_atividade_economica.status_registro = :status_registro
											    ORDER BY geral_atividade_economica.id");

		$sql->execute(array("id" => $id, "status_registro" => "A"));
		$qryLinha = $sql->fetchAll();
		
		$bold = ($buscaAtividadeEconomica['principal'] == 'S' ? 'font-weight: bold;' : '');
		
		if(count($qryLinha)) {
		
			foreach ($qryLinha as $buscaAtividade) {
				$bold = ($buscaAtividade['principal'] == 'S' ? 'font-weight: bold;' : '');
				?>
				<tr>
					<td>
						<form name="excluir_atividade" action="" method="post">
							<input type="hidden" name="id_atividade" value="<?=$buscaAtividade['id'] ?>"/>
							<input type="hidden" name="acao" value="excluir_atividade"/>
							<button type="submit" onclick="return confirm('Deseja realmente excluir a atividade <?= $buscaAtividade['denominacao'] ?>?');" class="btn btn-primary" data-toggle="tooltip" data-placement="right" title="Excluir atividade" >
								<i class="glyphicon glyphicon-trash"></i>
							</button>
						</form>
					</td>
					<td style="<?=$bold ?>"><?= $buscaAtividade['secao'] ?></td>
					<td style="<?=$bold ?>"><?= $buscaAtividade['divisao'] ?></td>
					<td style="<?=$bold ?>"><?= $buscaAtividade['grupo'] ?></td>
					<td style="<?=$bold ?>"><?= $buscaAtividade['classe'] ?></td>
					<td style="<?=$bold ?>"><?= $buscaAtividade['subclasse'] ?></td>
					<td style="<?=$bold ?>"><?= $buscaAtividade['denominacao'] ?></td>
				</tr>
				<?php
			}

		?>
		</table>

		<? } else { ?>
			<p class="col-sm-offset-2 col-sm-10"><strong>Voc&ecirc; ainda n&atilde;o possui Atividade Econ&ocirc;mica. Clique no bot&atilde;o abaixo e adicione quantas forem necess&aacute;rias.</strong></p>
		<? } ?>
	</div>
	<div class="form-group for_jur" id="mais_atividade">
		<div class="col-sm-offset-2 col-sm-10">
			<button type="button" href="#" class="btn btn-success pull-right" data-toggle="modal" data-target="#add_atividades" ><i class="glyphicon glyphicon-plus-sign"></i> Adicionar Atividade</button>
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10 captcha" id="recaptcha1"></div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<input type="hidden" name="acao" value="cadastrar">
			<button type="reset" class="btn btn-danger">Limpar</button>
			<button type="submit" class="btn btn-primary">Enviar</button>
		</div>
	</div>
</form>

<p class="clearfix">&nbsp;</p>
<h4>Hist&oacute;rico de downloads</h4>

<div class="table-responsive">
	<table class="table table-striped table-hover table-bordered table-condensed">
		<tr>
			<th>&nbsp;</th>
			<th>Data</th>
			<th>Modalidade</th>
			<th>Licita&ccedil;&atilde;o</th>
			<th>Arquivo</th>
			<th>Data de Publica&ccedil;&atilde;o</th>
			<th>Data de Abertura</th>
		</tr>
		<?php
		$pagina = isset($_REQUEST['pagina']) ? $_REQUEST['pagina'] : 1;
		$max = 10;
		$inicio = $max * ($pagina - 1);

		$sql = "SELECT licitacao_edital_download.*,
						licitacao_edital_anexo.descricao,
						licitacao_edital.titulo,
						licitacao_edital.data_publicacao,
						licitacao_edital.data_abertura,
						licitacao_modalidade.descricao modalidade
				   FROM licitacao_edital_download,
						licitacao_edital_anexo,
						licitacao_edital,
						licitacao_modalidade
			 	  WHERE licitacao_edital_download.id_edital_anexo = licitacao_edital_anexo.id
					AND licitacao_edital_anexo.id_edital = licitacao_edital.id
					AND licitacao_edital.id_modalidade = licitacao_modalidade.id
					AND licitacao_edital_download.id_fornecedor = :id
					AND licitacao_edital_download.status_registro = :status_registro ";

		$stLinha = $conn->prepare($sql);
		$stLinha->execute(array("id" => $fornecedor['id'], "status_registro" => "A"));
		$qryLinha = $stLinha->fetchAll();
		$totalLinha = count($qryLinha);

		$sql .= "ORDER BY licitacao_edital_download.id DESC LIMIT $inicio, $max";

		$stDownload = $conn->prepare($sql);
		$stDownload->execute(array("id" => $id, "status_registro" => "A"));
		$qryDownload = $stDownload->fetchAll();

		if(count($qryDownload)) {

			foreach ($qryDownload as $buscaDownload) {
		?>
		<tr>
			<td>
				<input type="hidden" name="acao" value="excluir_atividade">
				<a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$licitacaoDownload.$complemento) ?>&id=<?=$buscaDownload['id_edital_anexo'] ?>&nc=<?= $novoCliente ?>" class="btn btn-primary" data-toggle="tooltip" data-placement="right" title="Baixar arquivo novamente">
					<i class="glyphicon glyphicon-cloud-download"></i>
				</a>
			</td>
			<td class="text-right"><?= formata_data_hora($buscaDownload['data']) ?></td>
			<td><?= $buscaDownload['modalidade'] ?></td>
			<td><?= $buscaDownload['titulo'] ?></td>
			<td><?= $buscaDownload['descricao'] ?></td>
			<td class="text-right"><?= formata_data($buscaDownload['data_publicacao']) ?></td>
			<td class="text-right"><?= formata_data($buscaDownload['data_abertura']) ?></td>
		</tr>
		<?php
			}
		}
		?>
	</table>
</div>
<?php
$menos = $pagina - 1;
$mais = $pagina + 1;
$paginas = ceil($totalLinha / $max);
if($paginas > 1) {
?>
<nav>
	<ul class="pagination">
		<?php if($pagina == 1) { ?>
	    <li class="disabled"><a href="#" aria-label="Anterior"><span aria-hidden="true">&laquo;</span></a></li>
	    <?php } else { ?>
	    <li><a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$licitacaoConta.$complemento) ?>&nc=<?= $novoCliente ?>&pagina=<?= $menos ?>" aria-label="Anterior"><span aria-hidden="true">&laquo;</span></a></li>
	    <?php
		}

	    if(($pagina - 4) < 1) $anterior = 1;
	    else $anterior = $pagina - 4;

	    if(($pagina + 4) > $paginas) $posterior = $paginas;
	    else $posterior = $pagina + 4;

	    for($i = $anterior; $i <= $posterior; $i++) {

	    	if($i != $pagina) {
	    ?>
	    	<li><a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$licitacaoConta.$complemento) ?>&nc=<?= $novoCliente ?>&pagina=<?= $i ?>"><?= $i ?></a></li>
	    	<?php } else { ?>
	    	<li class="active"><a href="#"><?= $i ?><span class="sr-only">(current)</span></a></li>
	   	<?php
	    	}
	    }

	    if($mais <= $paginas) {
	   	?>
	   	<li><a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$licitacaoConta.$complemento) ?>&nc=<?= $novoCliente ?>&pagina=<?= $mais ?>" aria-label="Pr&oacute;ximo"><span aria-hidden="true">&raquo;</span></a></li>
	   	<?php } ?>
	</ul>
</nav>
<?php } ?>

<div class="modal fade" id="altera_senha">
	<div class="modal-dialog">
		<form class="form-horizontal validar_formulario_com_senha" name="altera_senha" action="" method="post">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Alterar minha senha</h4>
	      		</div>
		      	<div class="modal-body">
					<div class="form-group">
						<label for="senha_antiga" class="col-sm-4 control-label">Senha Atual</label>
						<div class="col-sm-8">
							<input type="password" name="senha_antiga" id="senha_antiga" class="form-control required" required placeholder="Informe sua senha atual...">
						</div>
					</div>
					<div class="form-group">
						<label for="senha" class="col-sm-4 control-label">Nova Senha</label>
						<div class="col-sm-8">
							<input type="password" name="senha" id="senha" class="form-control required" required placeholder="Informe a nova senha...">
						</div>
					</div>
					<div class="form-group">
						<label for="conf_senha" class="col-sm-4 control-label">Confirme a Senha</label>
						<div class="col-sm-8">
							<input type="password" name="conf_senha" id="conf_senha" class="form-control required" required placeholder="Confirme sua senha...">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-4 col-sm-8 captcha" id="recaptcha2"></div>
					</div>
				</div>
      			<div class="modal-footer">
      				<input type="hidden" name="acao" value="alterar_senha">
        			<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        			<button type="submit" class="btn btn-primary">Enviar</button>
      			</div>
	    	</div>
    	</form>
  	</div>
</div>

<div class="modal fade" id="add_atividades">
	<div class="modal-dialog">
		<form class="form-horizontal" name="add_atividades" action="" method="post">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Atividades Econ&ocirc;micas</h4>
				</div>
				<div class="modal-body">
					<div>
					<div class="form-group">
						<label for="secao" class="col-sm-2 control-label">Tipo</label>
						<div class="col-sm-10 col-lg-10">
							<select name="secao" id="secao" class="form-control required" required onchange="buscaDivisao()">
								<option value="">&raquo;&nbsp;Selecione</option>
								<?php
								$stSecao = $conn->prepare("SELECT secao, 
																   denominacao 
															  FROM geral_atividade_economica 
															 WHERE divisao = '' 
															   AND grupo = '' 
															   AND classe = '' 
															   AND subclasse = ''
															   AND status_registro = :status_registro");

								$stSecao->execute(array("status_registro" => "A"));
								$qrySecao = $stSecao->fetchAll();

								if(count($qrySecao)) {

									foreach ($qrySecao as $secao) {
										?>
										<option value="<?= $secao['secao'] ?>"><?= $secao['secao'] ?> - <?= $secao['denominacao'] ?></option>
									<?php }
								}?>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label for="divisao" class="col-sm-2 control-label">Divis&atilde;o</label>
						<div class="col-sm-10 col-lg-10">
							<select name="divisao" id="divisao" class="form-control required" required onchange="buscaGrupo()">
								<option value="">&raquo;&nbsp;Selecione</option>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label for="grupo" class="col-sm-2 control-label">Grupo</label>
						<div class="col-sm-10 col-lg-10">
							<select name="grupo" id="grupo" class="form-control required" required onchange="buscaClasse()">
								<option value="">&raquo;&nbsp;Selecione</option>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label for="classe" class="col-sm-2 control-label">Classe</label>
						<div class="col-sm-10 col-lg-10">
							<select name="classe" id="classe" class="form-control required" required onchange="buscaSubclasse()">
								<option value="">&raquo;&nbsp;Selecione</option>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label for="subclasse" class="col-sm-2 control-label">Subclasse</label>
						<div class="col-sm-10 col-lg-10">
							<select name="subclasse" id="subclasse" class="form-control required" required onchange="buscaAtividade()">
								<option value="">&raquo;&nbsp;Selecione</option>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label for="id_atividade" class="col-sm-2 control-label">Denomina&ccedil;&atilde;o</label>
						<div class="col-sm-10 col-lg-10">
							<select name="id_atividade" id="id_atividade" class="form-control required" required>
								<option value="">&raquo;&nbsp;Selecione</option>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-10 col-md-offset-2">
							<input style="vertical-align: middle;" type="checkbox" name="principal" value="S" />&nbsp;&Eacute; a atividade principal!
						</label>
					</div>

				</div>
				<div class="modal-footer">
					<input type="hidden" name="acao" value="cadastrar_atividade">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
					<button type="submit" class="btn btn-primary">Salvar</button>
				</div>
			</div>
		</form>
	</div>
</div>
	<script type="text/javascript"> 
		<!--
		function buscaDivisao() {
			jQuery("#divisao").load("ajax/atividade_economica_divisao.php", {secao:jQuery("#secao").val()}, function(){
				jQuery("#divisao").val("");
			});
		}

		function buscaGrupo() {
			jQuery("#grupo").load("ajax/atividade_economica_grupo.php", {divisao:jQuery("#divisao").val()}, function(){
				jQuery("#grupo").val("");
			});
		}

		function buscaClasse() {
			jQuery("#classe").load("ajax/atividade_economica_classe.php", {grupo:jQuery("#grupo").val()}, function(){
				jQuery("#classe").val("");
			});
		}

		function buscaSubclasse() {
			jQuery("#subclasse").load("ajax/atividade_economica_subclasse.php", {classe:jQuery("#classe").val()}, function(){
				jQuery("#subclasse").val("");
			});
		}

		function buscaAtividade() {
			jQuery("#id_atividade").load("ajax/atividade_economica_atividade.php", {subclasse:jQuery("#subclasse").val()}, function(){
				jQuery("#id_atividade").val("");
			});
		}

		//-->
	</script>
<?php
} else {

	echo "<script>window.location='$CAMINHO/index.php?sessao=$sequencia".$licitacaoLogin."$complemento&id=$id&nc=$novoCliente'</script>";

}