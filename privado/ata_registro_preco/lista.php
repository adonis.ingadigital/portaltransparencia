<h2>Ata de Registro de Pre&ccedil;os</h2>
<ol class="breadcrumb">
	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
	<li class="active">Ata de Registro de Pre&ccedil;os</li>
</ol>
<?php if($cliente != 33){ ?>
<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#form-contrato" aria-expanded="false" aria-controls="form-contrato">
	<i class="glyphicon glyphicon-search"></i> Pesquisar
</button>

<form class="form-horizontal collapse" id="form-contrato" action="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$ataRegistroPreco.$complemento); ?>" method="post">
	<div class="form-group">
		<label for="contratado" class="col-sm-2 control-label">Contratado</label>
		<div class="col-sm-10">
			<input type="text" name="contratado" id="contratado" class="form-control" value="<?= $_POST['contratado'] ?>" placeholder="Informe o nome do contratado...">
		</div>
	</div>
	<div class="form-group">
		<label for="assinatura" class="col-sm-2 control-label">Data da Assinatura</label>
		<div class="col-sm-10">
			<input type="text" name="assinatura" id="assinatura" class="form-control data" value="<?= $_POST['assinatura'] ?>" placeholder="Informe a data da assinatura...">
		</div>
	</div>
	<div class="form-group">
		<label for="ano" class="col-sm-2 control-label">Ano</label>
		<div class="col-sm-10">
			<input type="text" name="ano" id="ano" class="form-control ano" value="<?= $_POST['ano'] ?>" placeholder="Informe o ano da assinatura...">
		</div>
	</div>
	<div class="form-group">
		<label for="objeto" class="col-sm-2 control-label">Objeto</label>
		<div class="col-sm-10">
			<input type="text" name="objeto" id="objeto" class="form-control" value="<?= $_POST['objeto'] ?>" placeholder="Informe um trecho do objeto...">
		</div>
	</div>
	<div class="form-group">
		<label for="numero" class="col-sm-2 control-label">N&uacute;mero</label>
		<div class="col-sm-10">
			<input type="text" name="numero" id="numero" class="form-control" value="<?= $_POST['numero'] ?>" placeholder="Informe o n&uacute;mero do contrato...">
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<input type="hidden" name="acao" value="cadastrar">
			<button type="reset" class="btn btn-danger">Limpar</button>
			<button type="submit" class="btn btn-primary">Enviar</button>
		</div>
	</div>
</form>

<?php
$pagina = isset($_REQUEST['pagina']) ? $_REQUEST['pagina'] : 1;
$max = 10;
$inicio = $max * ($pagina - 1);

$link = $CAMINHO . "/index.php?sessao=" . $_GET['sessao'];

$sql = "SELECT * FROM ata_registro_preco WHERE id_cliente = :id_cliente AND status_registro = :status_registro ";
$vetor["id_cliente"] = $cliente;
$vetor["status_registro"] = "A";


if($_REQUEST['contratado'] != "") {

	$sql .= "AND contratado LIKE :contratado ";
	$vetor["contratado"] = "%" . htmlentities($_REQUEST['contratado'], ENT_QUOTES, "UTF-8") . "%";
	$link .= "&contratado=" . $_REQUEST['contratado'];
}

if($_REQUEST['assinatura'] != "") {

	$sql .= "AND data_assinatura = :data_assinatura ";
	$vetor["data_assinatura"] = formata_data_banco($_REQUEST['assinatura']);
	$link .= "&data_assinatura=" . $_REQUEST['data_assinatura'];

}

if($_REQUEST['ano'] != "") {

	$sql .= "AND YEAR(data_assinatura) = :ano ";
	$vetor["ano"] = $_REQUEST['ano'];
	$link .= "&ano=" . $_REQUEST['ano'];
}

if($_REQUEST['numero'] != "") {

	$sql .= "AND numero LIKE :numero ";
	$vetor["numero"] = "%" . htmlentities($_REQUEST['numero'], ENT_QUOTES, "UTF-8") . "%";
	$link .= "&numero=" . $_REQUEST['numero'];
}

if($_REQUEST['objeto'] != "") {

	$sql .= "AND objeto LIKE :objeto ";
	$vetor["objeto"] = "%" . htmlentities($_REQUEST['objeto'], ENT_QUOTES, "UTF-8") . "%";
	$link .= "&objeto=" . $_REQUEST['objeto'];
}

$stLinha = $conn->prepare($sql);
$stLinha->execute($vetor);
$qryLinha = $stLinha->fetchAll();
$totalLinha = count($qryLinha);

$sql .= "ORDER BY data_inicio DESC, id DESC LIMIT $inicio, $max";

$stAta = $conn->prepare($sql);
$stAta->execute($vetor);
$qryAta = $stAta->fetchAll();

if(count($qryAta)){

?>
<p class="clearfix"></p>
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
	<?php foreach ($qryAta as $ata) { ?>
	<div class="panel panel-default">
	    <div class="panel-heading" role="tab" id="heading_<?= $ata['id'] ?>">
	     	<h4 class="panel-title">
	        	<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?= $ata['id'] ?>" aria-expanded="true" aria-controls="collapse_<?= $ata['id'] ?>">
	          		<div class="row">
	          			<div class="col-sm-6"><i class="glyphicon glyphicon-triangle-right"></i> <strong>Contratado:</strong> <?= $ata['contratado'] ?></div>
	          			<div class="col-sm-3"><strong>Processo:</strong> <?= $ata['processo'] ?></div>
	          			<div class="col-sm-3"><strong>N&uacute;mero:</strong> <?= $ata['numero'] ?></div>
	          		</div>
	        	</a>
	    	</h4>
	    </div>
		<div id="collapse_<?= $ata['id'] ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_<?= $ata['id'] ?>">
			<div class="panel-body">
				<p><strong>Domic&iacute;lio:</strong> <?= $ata['domicilio'] ?></p>
				<p><strong>Partes:</strong> <?= $ata['partes'] ?></p>
				<p><strong>Data de In&iacute;cio:</strong> <?= formata_data($ata['data_inicio']) ?></p>
				<p><strong>Data de T&eacute;rmino:</strong> <?= formata_data($ata['data_fim']) ?></p>
				<p><strong>Data da Assinatura:</strong> <?= formata_data($ata['data_assinatura']) ?></p>
				<p><strong>Vig&ecirc;ncia:</strong> <?= $ata['vigencia'] ?></p>
				<p><strong>Valor Global:</strong> <?= $ata['valor_global'] ?></p>
				<p><strong>Objeto:</strong> <?= verifica($ata['objeto']) ?></p>

				<?php
				$stAnexo = $conn->prepare("SELECT * FROM ata_registro_preco_anexo WHERE id_artigo = :id_artigo ORDER BY ordem ASC, id DESC");
				$stAnexo->execute(array("id_artigo" => $ata['id']));
				$qryAnexo = $stAnexo->fetchAll();

				if(count($qryAnexo)) {
				?>
				<div class="panel panel-primary">
					<div class="panel-heading">
				    	<h2 class="panel-title"><i class="glyphicon glyphicon-cloud-download"></i> Anexos</h2>
				  	</div>
					<div class="panel-body">
						<?php
						foreach ($qryAnexo as $anexo) {
						?>
						<p><strong><a href="<?= $CAMINHOARQ ?>/<?= $anexo['arquivo'] ?>" target="_blank"><i class="glyphicon glyphicon-cloud-download"></i> <?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?></a></strong></p>
						<?php } ?>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
	<?php } ?>
</div>

<?php
$menos = $pagina - 1;
$mais = $pagina + 1;
$paginas = ceil($totalLinha / $max);
if($paginas > 1) {
?>
<nav>
	<ul class="pagination">
		<?php if($pagina == 1) { ?>
	    <li class="disabled"><a href="#" aria-label="Anterior"><span aria-hidden="true">&laquo;</span></a></li>
	    <?php } else { ?>
	    <li><a href="<?= $link ?>&pagina=<?= $menos ?>" aria-label="Anterior"><span aria-hidden="true">&laquo;</span></a></li>
	    <?php
		}

	    if(($pagina - 4) < 1) $anterior = 1;
	    else $anterior = $pagina - 4;

	    if(($pagina + 4) > $paginas) $posterior = $paginas;
	    else $posterior = $pagina + 4;

	    for($i = $anterior; $i <= $posterior; $i++) {

	    	if($i != $pagina) {
	    ?>
	    	<li><a href="<?= $link ?>&pagina=<?= $i ?>"><?= $i ?></a></li>
	    	<?php } else { ?>
	    	<li class="active"><a href="#"><?= $i ?><span class="sr-only">(current)</span></a></li>
	   	<?php
	    	}
	    }

	    if($mais <= $paginas) {
	   	?>
	   	<li><a href="<?= $link ?>&pagina=<?= $mais ?>" aria-label="Pr&oacute;ximo"><span aria-hidden="true">&raquo;</span></a></li>
	   	<?php } ?>
	</ul>
</nav>
<?php } ?>

<?php } else { ?>
<h4>Nenhum registro encontrado.</h4>
<?php
}

$atualizacao = atualizacao("ata_registro_preco", $cliente, $conn);
if($atualizacao != "") {
?>
<p class="text-right"><small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small></p>
<?php
}
}else{
	$stCategoria = $conn->prepare("SELECT * FROM ata_registro_preco_categoria WHERE (id_cliente = :id_cliente OR id_cliente = 1) AND status_registro = :status_registro ORDER BY descricao DESC");

	$stCategoria->execute(array("id_cliente" => $cliente, "status_registro" => "A"));
	$qryCategoria = $stCategoria->fetchAll();

	if(count($qryCategoria)) {
		?>
		<ul class="treeview">
			<?php
			foreach ($qryCategoria as $categoria) {
				?>
				<li><a href="#"><?= $categoria['descricao'] ?></a>
					<?php
					$stArquivo = $conn->prepare("SELECT ata_registro_preco.id, 
														ata_registro_preco.id_categoria, 
														ata_registro_preco_link.descricao descricao,
														ata_registro_preco_link.link link 
												   FROM ata_registro_preco 
											  LEFT JOIN ata_registro_preco_link ON ata_registro_preco.id = ata_registro_preco_link.id_artigo 
												  WHERE ata_registro_preco.id_categoria = :id_categoria 
													AND (ata_registro_preco.id_subcategoria IS NULL OR ata_registro_preco.id_subcategoria = '') 
													AND ata_registro_preco.status_registro = :status_registro
													AND ata_registro_preco.id_cliente = :id_cliente
											   ORDER BY id DESC;");
					$stArquivo->execute(array("id_categoria" => $categoria['id'], "status_registro" => "A", "id_cliente" => $cliente));
					$qryArquivo = $stArquivo->fetchAll();

					if(count($qryArquivo)) {
						?>
						<ul>
							<?php
							foreach($qryArquivo as $arquivo) {

								if($arquivo['link'] != "") {

									$linkDocumento = $arquivo['link'];
									$imagem = "glyphicon-globe";
									$titulo = empty($arquivo['descricao']) ? "Link" : $arquivo['descricao'];
								}
								?>
								<li><a href="<?=$linkDocumento ?>" target="_blank"><i class="glyphicon <?= $imagem ?>"></i> <?= $titulo ?></a></li>
							<?php } ?>
						</ul>
					<?php } ?>
					<?php
					$stSubcategoria = $conn->prepare("SELECT * FROM ata_registro_preco_subcategoria WHERE id_categoria = :id_categoria AND status_registro = :status_registro ORDER BY descricao ASC");

					$stSubcategoria->execute(array("id_categoria" => $categoria['id'], "status_registro" => "A"));
					$qrySubcategoria = $stSubcategoria->fetchAll();

					if(count($qrySubcategoria)) {
						?>
						<ul>
							<?php
							foreach ($qrySubcategoria as $subcategoria) {
								?>
								<li><a href="#"><?= $subcategoria['descricao'] ?></a>
									<?php
									$stArquivo = $conn->prepare("SELECT ata_registro_preco.id, 
																		ata_registro_preco.id_categoria, 
																		ata_registro_preco_link.descricao descricao,
																		ata_registro_preco_link.link link 
																   FROM ata_registro_preco 
															  LEFT JOIN ata_registro_preco_link ON ata_registro_preco.id = ata_registro_preco_link.id_artigo
																  WHERE ata_registro_preco.id_subcategoria = :id_subcategoria 
																    AND ata_registro_preco.status_registro = :status_registro 
																    AND ata_registro_preco.id_cliente = :id_cliente 
															   ORDER BY id DESC");
									$stArquivo->execute(array("id_subcategoria" => $subcategoria['id'], "status_registro" => "A", "id_cliente" => $cliente));
									$qryArquivo = $stArquivo->fetchAll();

									if(count($qryArquivo)) {
										?>
										<ul>
											<?php
											foreach($qryArquivo as $arquivo) {

												if($arquivo['link'] != "") {

													$linkDocumento = $arquivo['link'];
													$imagem = "glyphicon-globe";
													$titulo = empty($arquivo['descricao']) ? "Link" : $arquivo['descricao'];
												}
												?>
												<li><a href="<?= $linkDocumento ?>" target="_blank"><i class="glyphicon <?= $imagem ?>"></i> <?= $titulo ?></a></li>
											<?php } ?>
										</ul>
									<?php } ?>
								</li>
							<?php } ?>
						</ul>
					<?php } ?>
				</li>
			<?php } ?>
		</ul>
		<?php
	}
}
$atualizacao = atualizacao("contratos", $cliente, $conn);
if($atualizacao != "") {
	?>
	<p class="text-right"><small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small></p>
<?php }