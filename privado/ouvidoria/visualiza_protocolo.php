<?php

	$id = secure($_REQUEST['id']);

	$stSolicitacao = $conn->prepare("SELECT * 
									   
									   FROM pref_ouvidoria 
									  
									  WHERE id = :id 
									    
										AND status_registro = :status_registro");

	$stSolicitacao->execute(array("id" => $id, "status_registro" => "A"));

	$buscaSolicitacao = $stSolicitacao->fetch();


	$id_cliente = $buscaSolicitacao['id_cliente'];

	$stAnexos = $conn->prepare("SELECT * 
	  							  
								  FROM pref_ouvidoria_anexo 
								 
								 WHERE id_ouvidoria = :cod_ouvidoria 
								 
								   AND status_registro = :status_registro");

	$stAnexos->execute(array("cod_ouvidoria" => $id , "status_registro" => "A"));

	$buscaAnexos = $stAnexos->fetchAll();

?>

<h2>Ouvidoria</h2>

<ol class="breadcrumb">

	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>

	<li><a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$ouvidoria.$complemento); ?>">Ouvidoria</a></li>

	<li class="active">Protocolo</li>

</ol>

<h4>N&#176; DE PROTOCOLO <span style="font-size: 20px;" class="label label-primary"><?= $buscaSolicitacao['protocolo'] ?></span></h4>

<p class="clearfix">&nbsp;</p>

<h3 class="text-info">Dados da Solicita&ccedil;&atilde;o</h3>

<p><strong>Data da solicita&ccedil;&atilde;o:</strong> <?= formata_data_hora($buscaSolicitacao['data_solicitacao']) ?></p>

<p><strong>Nome:</strong> <?= empty($buscaSolicitacao['nome']) ? "An&ocirc;nimo" : $buscaSolicitacao['nome'] ?></p>

<p><strong>Solicita&ccedil;&atilde;o:</strong> <?= verifica($buscaSolicitacao['solicitacao']) ?></p>

<h3 class="text-info">Andamento da Solicita&ccedil;&atilde;o</h3>

<?php

	$stAndamento = $conn->prepare("SELECT * 
									 
									 FROM pref_ouvidoria_andamento 
									
									WHERE id_artigo = :id");

	$stAndamento->execute(array("id" => $buscaSolicitacao['id']));

	$qryAndamento = $stAndamento->fetchAll();

	if(count($qryAndamento)) {

		foreach ($qryAndamento as $andamento) {

?>

<p><i class="glyphicon glyphicon-triangle-right"></i><strong> <?= formata_data_hora($andamento['data']) ?> - </strong><?= $andamento['status'] ?></p>

<?= verifica($andamento['resposta']) ?>

<p>&nbsp;</p>

<?php 

		} 
		
	} 

	if(count($buscaAnexos)) { 

?>

<h3 class="text-info">Anexos <i class="glyphicon glyphicon-paperclip" style="color: #000"></i></h3>

<?
  
		foreach ($buscaAnexos as $valor) {

?>

<a href="http://www.ingabyte.com.br/sistema/arquivos/<?=$id_cliente ?>/<?=$valor['arquivo'] ?>" target="_blank">

	<!-- <img  class="img-responsive" src="http://www.ingabyte.com.br/sistema/arquivos/<?=$id_cliente ?>/<?=$valor['arquivo'] ?>"> -->
	
	<i style="font-size: 25px;" class="glyphicon glyphicon-cloud-download"></i>
	
</a>

<? 
	
		}

	}

?>

<br>

<a href="<?=$CAMINHO?>/index.php?sessao=<?= $sequencia.$ouvidoria.$complemento?>" style="margin-top: 20px" class="btn btn-primary">

	<i class="glyphicon glyphicon-arrow-left"></i> Voltar
	
</a>

<a href="<?= $CAMINHOCMGERAL ?>/impressao/protocolo_ouvidoria.php?id=<?= $buscaSolicitacao['id'] ?>" style="margin-top: 20px" class="btn btn-primary" target="_blank">

	<i class="glyphicon glyphicon-print"></i> Imprimir Solicita&ccedil;&atilde;o
	
</a>

