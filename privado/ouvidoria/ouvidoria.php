<style>
svg:not(:root) {
    overflow: hidden;
    border-right: 1px solid #337ab7;
}
</style>

<?php

	$titulo = "Ouvidoria";

	if ($cliente == "1003") {
		// var_dump($dadosCliente);
	}

	if($cliente == "46" && isset($_GET['ouvidoria_geral']))

		$titulo = "Ouvidoria Geral";

	else if($cliente == "46" && isset($_GET['ouvidoria_saude']))

		$titulo = "Ouvidoria da Sa&uacute;de";



		if($cliente == "1027" && isset($_GET['ouvidoria_geral'])){

			$titulo = "Ouvidoria Geral";
	
		}
		if($cliente == "1027" && isset($_GET['ouvidoria_saude'])){
	
			$titulo = "Ouvidoria da Sa&uacute;de";
		}

		if($cliente == "33" && isset($_GET['ouvidoria_geral'])){

			$titulo = "Ouvidoria Geral";
	
		}
		if($cliente == "33" && isset($_GET['ouvidoria_saude'])){
	
			$titulo = "Ouvidoria da Sa&uacute;de";
		}

		if($cliente == "43" && isset($_GET['ouvidoria_geral'])){

			$titulo = "Ouvidoria Geral";
	
		}
		if($cliente == "43" && isset($_GET['ouvidoria_saude'])){
	
			$titulo = "Ouvidoria da Sa&uacute;de";
		}

		if($cliente == "1163" && isset($_GET['ouvidoria_geral'])){

			$titulo = "Ouvidoria Geral";
	
		}
		if($cliente == "1163" && isset($_GET['ouvidoria_saude'])){
	
			$titulo = "Ouvidoria da Sa&uacute;de";
		}

		if($cliente == "1163" && isset($_GET['ouvidoria_saude'])){
	
			$titulo = "Ouvidoria SUS";
		}

		if($cliente == "27" && isset($_GET['ouvidoria_geral'])){
			
			$titulo = "Ouvidoria Geral";
	
		}
		if($cliente == "27" && isset($_GET['ouvidoria_saude'])){
	
			$titulo = "Ouvidoria da Sa&uacute;de";
		}
		if($cliente == "123" && isset($_GET['ouvidoria_geral'])){
	
			$titulo = "Ouvidoria Geral";
		}
		if($cliente == "123" && isset($_GET['ouvidoria_saude'])){
	
			$titulo = "Ouvidoria da Sa&uacute;de";
		}
		if($cliente == "1003" && isset($_GET['ouvidoria_geral'])){
			$titulo = "Ouvidoria Geral";
		}
		if($cliente == "1003" && isset($_GET['ouvidoria_saude'])){
	
			$titulo = "Ouvidoria da Sa&uacute;de";
		}
		if($cliente == "68" && isset($_GET['ouvidoria_geral'])){
	
			$titulo = "Ouvidoria Geral";
		}
		if($cliente == "68" && isset($_GET['ouvidoria_saude'])){
	
			$titulo = "Ouvidoria da Sa&uacute;de";
		}
?>

<h2><?= $titulo ?></h2>

<ol class="breadcrumb">

	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>

	<li class="active"><?= $titulo ?></li>

</ol>

<?php

	include "../../privado/transparencia/recaptchalib.php";

	// $_POST['telephone'] CAMPO CRIADO PARA TENTAR PARAR SPAMMER
	
	if($_POST['acao'] == "cadastrar" && $_POST['telephone'] == "") {

		foreach ($_POST as $campo => $valor) {

			$$campo = secure($valor);

		}

		$cap = new GoogleRecaptcha();

		$verified = $cap->VerifyCaptcha($_POST['g-recaptcha-response']);

		if(!$verified) {

			echo '<p class="alert alert-danger">

					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

					O Captcha n&atilde;o foi resolvido! Verifique.

				  </p>';

		} else if(empty($anonimo) && empty($nome)) {

			echo '<p class="alert alert-danger">

					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

					Preencha o campo Nome Completo!

				  </p>';

		} else if(empty($anonimo) && $cpf != "" && validaCPF($cpf) == false) {

			echo '<p class="alert alert-danger">

					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

					O CPF informado &eacute; inv&aacute;lido!

				  </p>';

		} else if(empty($anonimo) && $email != "" && filter_var($email, FILTER_VALIDATE_EMAIL) === false) {

			echo '<p class="alert alert-danger">

					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

					Forne&ccedil;a um e-mail v&aacute;lido!

				  </p>';

		} else {

			$stCadastro = $conn->prepare("INSERT INTO pref_ouvidoria (id_cliente,

																	  id_departamento,

																	  data_solicitacao,

																	  tipo,

																	  nome,

																	  endereco,

																	  cidade,

																	  bairro,

																	  cep,

																	  rg,

																	  cpf,

																 	  telefone,

																	  email,

																	  solicitacao)

											   VALUES (?,

													   ?,

													   NOW(),

													   ?,

													   ?,

													   ?,

													   ?,

													   ?,

													   ?,

													   ?,

													   ?,

													   ?,

													   ?,

													   ?)");

			$idDepartamento = empty($id_departamento) ? 1 : $id_departamento;

			$stCadastro->bindParam(1, $cliente, PDO::PARAM_INT);

			$stCadastro->bindParam(2, $idDepartamento, PDO::PARAM_INT);

			$stCadastro->bindParam(3, $tipo, PDO::PARAM_STR);

			$stCadastro->bindParam(4, $nome, PDO::PARAM_STR);

			$stCadastro->bindParam(5, $endereco, PDO::PARAM_STR);

			$stCadastro->bindParam(6, $cidade, PDO::PARAM_STR);

			$stCadastro->bindParam(7, $bairro, PDO::PARAM_STR);

			$stCadastro->bindParam(8, $cep, PDO::PARAM_STR);

			$stCadastro->bindParam(9, $rg, PDO::PARAM_STR);

			$stCadastro->bindParam(10, $cpf, PDO::PARAM_STR);

			$stCadastro->bindParam(11, $telefone, PDO::PARAM_STR);

			$stCadastro->bindParam(12, $email, PDO::PARAM_STR);

			$stCadastro->bindParam(13, $solicitacao, PDO::PARAM_STR);

			$cad = $stCadastro->execute();

			if($cad) {

				$id = $conn->lastInsertId();

				$numero_protocolo = date("dmyHi").str_pad($id, 6, "0", STR_PAD_LEFT);

				$stProtocolo = $conn->prepare("UPDATE pref_ouvidoria 
											
											      SET protocolo = ? 
												  
												WHERE id = ?");

				$stProtocolo->bindParam(1, $numero_protocolo, PDO::PARAM_STR);

				$stProtocolo->bindParam(2, $id, PDO::PARAM_INT);

				$stProtocolo->execute();

				$stAndamento = $conn->prepare("INSERT INTO pref_ouvidoria_andamento (id_artigo, 
				
																						  data, 
																						  
																						status) 
																						
												    VALUES (?, 
													        
															NOW(), 
															
															'Pendente')");

				$stAndamento->bindParam(1, $id, PDO::PARAM_INT);

				$stAndamento->execute();

				//Anexos

				$extensoes = array("gif","png","jpg","jpeg", "pdf", "txt", "doc", "docx", "xls", "xlsx", "csv", "ppt", "pptx", "pps", "ppsx");

				$destino = "../../controlemunicipal/www/inga/sistema/arquivos/$cliente";

				$msgFiles = "";

				for ($i = 0; $i < count($_FILES['anexo']['name']); $i++) {

					if($_FILES['anexo']['size'][$i] > 0) {

						$ext = strtolower(array_pop(explode(".", $_FILES['anexo']['name'][$i])));

						$nome_arquivo = date("dmyHis") . "_" . nomear_pasta($_FILES['anexo']['name'][$i]) . "." . $ext;

						if(array_search($ext, $extensoes) !== false) {

							if(move_uploaded_file($_FILES['anexo']['tmp_name'][$i], $destino . "/" . $nome_arquivo)) {

								$stAnexo = $conn->prepare("INSERT INTO pref_ouvidoria_anexo (id_ouvidoria,

																							 descricao,

																							 arquivo)

																VALUES (?,

																		?,

																		?)");

								$stAnexo->bindParam(1, $id, PDO::PARAM_INT);

								$stAnexo->bindParam(2, $_FILES['anexo']['name'][$i], PDO::PARAM_STR);

								$stAnexo->bindParam(3, $nome_arquivo, PDO::PARAM_STR);

								if(!$stAnexo->execute()) {

									$msgFiles .= $_FILES['anexo']['name'][$i] . " erro ao salvar o arquivo<br>";

								}

							} else {

								$msgFiles .= $_FILES['anexo']['name'][$i] . " erro ao mover o arquivo para a pasta<br>";

							}

						} else {

							$msgFiles .= $_FILES['anexo']['name'][$i] . " n&atilde;o possui um form&aacute;to v&aacute;lido<br>";

						}

					} else {

						//$msgFiles .= $_FILES['anexo']['name'][$i] . " &eacute; um arquivo vazio ou inv&aacute;lido<br>";

					}

				}

				if($msgFiles != "") {

					echo '<p class="alert alert-danger">

							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

							Um ou mais arquivos enviados apresentaram algum problema:<br>'.$msgFiles.'

						  </p>';

				}

			}

			if(empty($id_departamento)){

				$stEmail = $conn->prepare("SELECT email 
				
											 FROM pref_email_ouvidoria 
											 
											WHERE id_cliente = :id_cliente 
											
											  AND status_registro = :status_registro");

				$stEmail->execute(array("id_cliente" => $cliente, "status_registro" => "A"));

				$qryEmail = $stEmail->fetchAll();

				$totalEmail = count($qryEmail);

				if($totalEmail > 0) {

					$count = 1;

					foreach ($qryEmail as $buscaEmail) {

						$to[$count]['email'] = $buscaEmail["email"];
						$to[$count]['nome'] = "Ouvidoria";
						$count ++;

					}
				} else {

					$to[0]['email'] = $dadosCliente['email'];
					$to[0]['nome'] = "Ouvidoria";
				}

			} else {

				$stDepartamento = $conn->prepare("SELECT * 
				
													FROM pref_ouvidoria_departamento 
												   
												   WHERE id = :id_departamento 
												     
													 AND status_registro = :status_registro");

				$stDepartamento->execute(array("id_departamento" => $id_departamento, "status_registro" => "A"));

				$buscaDepartamento = $stDepartamento->fetch();

				$to[0]['email'] = $buscaDepartamento['email'];
				$to[0]['nome'] = "Ouvidoria";
			}
			if($cliente == "46" && isset($_GET['ouvidoria_geral'])){

				$subject = "Ouvidoria Geral - Solicitacao Enviada pelo site da Prefeitura";

			} elseif($cliente == "46" && isset($_GET['ouvidoria_saude'])){

				$subject = "Ouvidoria da Saude - Solicitacao Enviada pelo site da Prefeitura";

			} else {

		    	$subject = "Solicitação de Ouvidoria - $dadosCliente[razao_social]";

			}

			$html  = "<p>A solicita&ccedil;&atilde;o foi enviada com sucesso:</p><p>&nbsp;</p>";

			$html .= "<p><strong>Data da Solicita&ccedil;&atilde;o:</strong> ".date("d/m/Y H:i")."</p>";

			$html .= "<p><strong>N&ordm; Protocolo:</strong> $numero_protocolo</p>";

			$html .= "<p><strong>Tipo:</strong> $tipo</p>";

			if($id_departamento != '')

				$html .= "<p><strong>Departamento:</strong> $buscaDepartamento[descricao]</p>";

			if ($anonimo == 'S'){

				$html .= "<p><strong>Nome:</strong> An&ocirc;nimo</p>";

			} else {

				$html .= "<p><strong>Nome:</strong> $nome</p>";

				$html .= "<p><strong>CPF:</strong> $cpf</p>";

				$html .= "<p><strong>RG:</strong> $rg</p>";

				$html .= "<p><strong>Endere&ccedil;o:</strong> $endereco</p>";

				$html .= "<p><strong>Bairro:</strong> $bairro</p>";

				$html .= "<p><strong>Cidade:</strong> $cidade</p>";

				$html .= "<p><strong>CEP:</strong> $cep</p>";

				$html .= "<p><strong>Telefone:</strong> $telefone</p>";

				$html .= "<p><strong>E-mail:</strong> $email</p>";

			}

			$html .= "<p><strong>Solicita&ccedil;&atilde;o:</strong> ".nl2br($solicitacao)." </p>";

			$html .= "<p><strong>Endere&ccedil;o IP do remetente:</strong> ".$_SERVER['REMOTE_ADDR']."</p>";

			$headers = $dadosCliente['razao_social']." <".(empty($dadosCliente['email']) ? "contato@ingadigital.com.br" : $dadosCliente['email']).">";
			
			$vai = envia_email_aws($to, $subject, $html, $headers, [], './ingadigital.com.br/privado/transparencia/ouvidoria/ouvidoria.php:481');
			// var_dump($vai);
			if($vai) {

				echo"<script>window.location='$CAMINHO/index.php?sessao=$sequencia".$visualizaProtocolo."$complemento&id=$id'</script>";

			} else {

				echo '<p class="alert alert-danger">

						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

						Aten&ccedil;&atilde;o! Algum problema ocorreu com o envio do e-mail. Entre em contato com a '.$dadosCliente["razao_social"].'

					  </p>';

			}

		}

	} else if($_POST['busca_protocolo'] != "") {

		$stProtocolo = $conn->prepare("SELECT * 
										 
										 FROM pref_ouvidoria 
										
										WHERE protocolo = :protocolo 
										  
										  AND id_cliente = :id_cliente 
										  
										  AND status_registro = :status_registro");

		$stProtocolo->execute(array("protocolo" => secure($_POST['busca_protocolo']), "id_cliente" => $cliente, "status_registro" => "A"));

		$buscaProtocolo = $stProtocolo->fetch();

		if($buscaProtocolo['id'] != "") {

			echo"<script>window.location='$CAMINHO/index.php?sessao=$sequencia".$visualizaProtocolo."$complemento&id=$buscaProtocolo[id]'</script>";

		} else {

			echo '<p class="alert alert-danger">

					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

					Aten&ccedil;&atilde;o! N&atilde;o foi encontrada nenhuma solicita&ccedil;&atilde;o com o protocolo <span class="label label-danger">'.$_POST['busca_protocolo'].'</span>

				  </p>';

		}

	}

?>

<div class="row">

	<div class="col-md-9">

		<div class="well">

<?php
	if($cliente == "11928"){
		echo "<span><strong>Telefone da Ouvidoria: 44-3636-8332</strong></span><br>";
	}

	if($cliente != "46") { 
		
?>

			Por meio da Ouvidoria do Munic&iacute;pio, &eacute; poss&iacute;vel realizar solicita&ccedil;&otilde;es para que a administra&ccedil;&atilde;o possa tomar as provid&ecirc;ncias cab&iacute;veis, caso voc&ecirc; tenha alguma

	      	sugest&atilde;o ou reclama&ccedil;&atilde;o, participe, fa&ccedil;a com que nossa cidade cres&ccedil;a ainda mais.<br>

	      	Ao realizar sua reclama&ccedil;&atilde;o ou sugest&atilde;o, voc&ecirc; poder&aacute; fazer o acompanhamento da mesma atrav&eacute;s de nosso site.

	      	Para isso basta informar o n&uacute;mero do protocolo no campo ao lado.<br> 
			<?if($cliente == "12113") { ?>
		    As solicitações podem ser feitas também por meio de correspondências enviadas para Prefeitura Municipal de Ortigueira/Departamento de Ouvidoria Municipal localizada na Rua São Paulo nº80 Centro CEP: 84.350-000 Ortigueira PR.
		    <?}?>
<?php 

	} else { 
		
?>

	      	A Ouvidoria ser&aacute; o canal de comunica&ccedil;&atilde;o direta entre a sociedade e a administra&ccedil;&atilde;o municipal, recebendo reclama&ccedil;&otilde;es, den&uacute;ncias, sugest&otilde;es e elogios, de modo a estimular a participa&ccedil;&atilde;o do cidad&atilde;o no controle e avalia&ccedil;&atilde;o dos servi&ccedil;os prestados e na gest&atilde;o dos recursos p&uacute;blicos, al&eacute;m de receber requerimentos de acesso a informa&ccedil;&otilde;es. Para o acesso as informa&ccedil;&otilde;es p&uacute;blicas ser&aacute; considerada a Lei Federal 12.527/11 e o prazo para o fornecimento destas ser&aacute; de at&eacute; 20 dias.

<?php

	}

	if($cliente == "43" && !isset($_GET['ouvidoria_saude'])) {

?>

			<br><br><strong>SIC F&iacute;sico</strong><br>

			<strong>PREFEITURA MUNICIPAL DE LE&Oacute;POLIS</strong><br>

			Endere&ccedil;o: Rua Pedro Domingues de Souza, 374 - Centro - Le&oacute;polis – PR<br>

			Cep: 86330-000<br>

			Hor&aacute;rio: Segunda &agrave; Sexta-Feira, das 7:30 &agrave;s 11:30 - 13:00 &agrave;s 17:00<br>

			Telefone: (43) 3627-1361<br>

			Respons&aacute;vel: Carla Cristina de Oliveira Bianconi<br><br>

			<a href="http://www.ingadigital.com.br/transparencia/index.php?sessao=4177ac5c7dfa41" target="_blank" class="text-info">Clique aqui para baixar os Formul&aacute;rios de Atendimento.</a>

<?php

	}
	if($cliente == "1163" && !isset($_GET['ouvidoria_saude'])) {

		?>
					<div style="text-align: center;">	
						<br><br><strong>OUVIDORIA SUS:</strong> 0800-646-2142<br><br>
			
						<strong>E-Mail:</strong> ouvidoriasus@quatigua.pr.gov.br <br><br> 
						<strong>Telefone:</strong> (43) 3564-1814 - Ramal 22 <br><br>
			
						<strong>Atendimento presencial:</strong> Centro de Saúde de Quatiguá<br><br>
			
						<strong>Sala da Ouvidoria</strong><br>
			
						<strong>Ouvidora:</strong> Maria Janete da silva Muniz<br><br>
			
						Atendimento das 7:30 às 11 horas - das 13 às 17 horas<br>
					</div>
		
		<?php
		
	}
	if($cliente == "43" && isset($_GET['ouvidoria_saude'])) {

		?>
		
					<br><br><strong>SIC F&iacute;sico</strong><br>
		
					<strong>PREFEITURA MUNICIPAL DE LE&Oacute;POLIS</strong><br>

					Unidade B&aacute;sica de Sa&uacute;de Maria Od&iacute;lia Trombini<br>
		
					Endere&ccedil;o: Rua Marechal Deodoro, 19 - Centro - Le&oacute;polis – PR<br>
		
					Cep: 86330-000<br>
		
					Hor&aacute;rio: Segunda &agrave; Sexta-Feira, das 07h30min &agrave;s 11h30min - 13h00min &agrave;s 17h00min.<br>
		
					Telefone: (43) 3627-1532<br>
		
					Respons&aacute;vel: Fernanda Maria da Silva<br><br>
		
					<a href="http://www.ingadigital.com.br/transparencia/index.php?sessao=4177ac5c7dfa41" target="_blank" class="text-info">Clique aqui para baixar os Formul&aacute;rios de Atendimento.</a>
		
		<?php
		
			}
			
	if($cliente == "50") {

?>

			<br><br><strong><h3><center>ENDERE&Ccedil;OS ELETR&Ocirc;NICOS PARA CONTATO</center></h3></strong><br>

			<strong><h4>O prazo para abertura dos emails relacionados abaixo ser&aacute; de no m&aacute;ximo 5(cinco) dias &uacute;teis a partir do dia do seu recebimento, o mesmo prazo se aplica para ser encaminhado a resposta.</h4></strong><br>

			<br><strong>Prefeita:<strong> prefeita@amapora.pr.gov.br<br>

			<br><strong>Procurador Jur&iacute;dico:<strong> procuradoriajuridica@amapora.pr.gov.br<br>

			<br><strong>Protocolo:<strong> protocolo@amapora.pr.gov.br<br>
			
			<br><strong>Controle Interno:<strong> controleinterno@amapora.pr.gov.br<br>

			<br><a href="http://www.ingabyte.com.br/sistema/arquivos/50/280518103907_portaria_nordm_1072018_pdf.pdf" target="_blank" class="text-info">PORTARIA Nº 107/2018</a>	
<?php

	}

	if($cliente == "1040") {

?>

			<br><br>

			<span class="ouvidoria-texto" style="color: #000; font-size: 11pt; line-height: 20px">Prezado cidad&atilde;o (&atilde;), exercite a sua cidadania e participe da Administra&ccedil;&atilde;o P&uacute;blica	de Ribeir&atilde;o Claro.<br />

			Com a sua a&ccedil;&atilde;o e colabora&ccedil;&atilde;o, a Prefeitura de Ribeir&atilde;o Claro poder&aacute; atender muito mais os anseios da comunidade, realizando as modifica&ccedil;&otilde;es e aperfei&ccedil;oamentos que	v&atilde;o de encontro &agrave;s necessidades do munic&iacute;pio.<br />

			<strong style="color: #000; font-size: 12pt;">&rarr; Quando procurar</strong><br />

			O mun&iacute;cipio deve falar com a Ouvidoria quando receber atendimento inadequado ou nenhum; receber resposta insatisfat&oacute;ria ou identificar irregularidade grave cometida por funcion&aacute;rio. Portanto procure a OUVIDORIA MUNICIPAL para enviar suas sugest&otilde;es, cr&iacute;ticas, reclama&ccedil;&otilde;es, den&uacute;ncias, elogios ou pedidos de informa&ccedil;&otilde;es.<br />

			<strong style="color: #000; font-size: 12pt;">&rarr; Procure-nos</strong><br />

			De segunda a sexta, em hor&aacute;rio normal de expediente

			<ul style="list-style-position: inside;">

				<li>Pelo telefone: LIGUE <strong>156</strong></li>

				<li>Pessoalmente na sede da Prefeitura</li>

				<li>E-mail:&nbsp;<a style="color: #000; font-weight: bold;" href="mailto:ouvidoria@ribeiraoclaro.pr.gov.br">ouvidoria@ribeiraoclaro.pr.gov.br</a></li>

				<li>Site:&nbsp;Preenchendo o formul&aacute;rio de atendimento abaixo.</li>

			</ul>

			Caso solicitado, a Ouvidoria manter&aacute; sob sigilo sua identifica&ccedil;&atilde;o.<br />

			Em caso de den&uacute;ncias, solicitamos que seja fornecido o m&aacute;ximo de informa&ccedil;&otilde;es para que o caso possa ser investigado.<br />

			Caso a den&uacute;ncia seja an&ocirc;nima, ela somente ser&aacute; analisada se forem encaminhados dados que possibilitem a sua verifica&ccedil;&atilde;o e nesse caso a Ouvidoria n&atilde;o tem o compromisso de informar o resultado ao denunciante.</span>

<?php

	}

	if($cliente == "12109") {

?>

			<br><br><strong>SIC F&iacute;sico</strong><br>

			<strong>C&Acirc;MARA MUNICIPAL DE LE&Oacute;POLIS</strong><br>

			Endere&ccedil;o: Rua Pedro Domingues de Souza, 182 - Centro - Le&oacute;polis – PR<br>

			Cep: 86330-000<br>

			Hor&aacute;rio: Segunda &agrave; Sexta-Feira, das 8:00 &agrave;s 11:30 - 13:00 &agrave;s 16:00<br>

			Telefone: (43) 4141–2994<br>

			Respons&aacute;vel: Eunice Yurika Gondo<br><br>

			<a href="http://www.ingadigital.com.br/transparencia/index.php?sessao=4177ac5c7dfa41" target="_blank" class="text-info">Clique aqui para baixar os Formul&aacute;rios de Atendimento.</a></strong>

<?php

	}
	if($cliente == "43"){

?>
	<br><br><a href="http://www.ingadigital.com.br/transparencia/index.php?sessao=fe024abd5arafe"><button type="submit" class="btn btn-primary" >Relat&oacute;rio por Ano</button></a>
	<a href="http://www.controlemunicipal.com.br/inga/sistema/arquivos/43/fluxogramav1_pdf.pdf" target="_blank"><button type="submit" class="btn btn-primary" http://www.ingadigital.com.br/transparencia/images/fluxogramav1_pdf.pdf>Fluxo da Ouvidoria</button></a>

<?php
		}
?>

		</div>
		
	<?
	if($cliente == "12113"){
	?>

	<a href="https://docs.google.com/forms/d/e/1FAIpQLSfz35ZQA-oJ5EAIEHhnDMV0L-nW5eDG4cIw-a5t2sSK8hQzMg/viewform?vc=0&c=0&w=1&fbzx=-2471007615498200173" class="btn btn-default" target="_blank" style="float: right; background-color: #624a9f; border-color: #624a9f; color: #FFF;">
		<i class="glyphicon glyphicon-list-alt"></i> Dê sua opinião sobre o serviço
	</a><br><br>
	<a href="http://www.ingabyte.com.br/sistema/arquivos/12113/110919102318_carta_site_versao_pdf_pdf.pdf" class="btn btn-default" target="_blank" style="float: right; background-color: #9264cb; border-color: #9264cb; color: #FFF;">
		<i class="glyphicon glyphicon-download-alt"></i> Carta de Serviços
	</a>

	<?}?>

		<div class="col-md-12">
			<h3 class="text-info">Realizar Solicita&ccedil;&atilde;o</h3>

			<form class="form-horizontal validar_formulario" id="form-ouvidoria" name="form-ouvidoria" action="" method="post" enctype="multipart/form-data">
				<?
				if($cliente != "12118"){
				?>
				<p class="clearbox">&nbsp;</p>
				
				<div class="form-group">

					<div class="checkbox col-sm-offset-2 col-sm-10" style=" height: 35px; background-color: #DDDDDD; border: 1px solid #AAAAAA; border-radius: 4px;">

						<label style="font-weight: bold;"><input type="checkbox" name="anonimo" id="anonimo" value="S"> Enviar como an&ocirc;nimo</label>

					</div>


				</div>
				<?
				}
				?>
				<p class="clearbox">&nbsp;</p>


				<div class="form-group">

					<label for="tipo" class="col-sm-2 control-label">Tipo</label>

					<div class="col-sm-10">

						<select name="tipo" id="tipo" class="form-control required" required>

							<option value="">Selecione o tipo de solicita&ccedil;&atilde;o</option>

							<option value="Sugest&atilde;o" <? if($tipo == "Sugest&atilde;o") echo "selected"; ?>>Sugest&atilde;o</option>

							<option value="Reclama&ccedil;&atilde;o" <? if($tipo == "Reclama&ccedil;&atilde;o") echo "selected"; ?>>Reclama&ccedil;&atilde;o</option>

							<option value="Informa&ccedil;&atilde;o" <? if($tipo == "Informa&ccedil;&atilde;o") echo "selected"; ?>>Informa&ccedil;&atilde;o</option>

							<option value="Den&uacute;ncias" <?php if($tipo == "Den&uacute;ncias") echo "selected";?>>Den&uacute;ncias</option>

							<option value="Elogios" <?php if($tipo == "Elogios") echo "selected";?>>Elogios</option>

						</select>

					</div>

				</div>

<?php 

	$stDepartamento = $conn->prepare("SELECT * 
										
										FROM pref_ouvidoria_departamento 
									   
									   WHERE id_cliente = :id_cliente 
									   
									     AND status_registro = :status_registro");

	$stDepartamento->execute(array("id_cliente" => $cliente, "status_registro" => "A"));

	$qryDepartamento = $stDepartamento->fetchAll();

	if(count($qryDepartamento)){	
		// var_dump($qryDepartamento);
?>

				<div class="form-group">

					<label for="id_departamento" class="col-sm-2 control-label">Departamento</label>

					<div class="col-sm-10">

						<select name="id_departamento" id="id_departamento" class="form-control required" required>

							<option value="">Selecione o departamento</option>

<?php 
		if ($cliente == "1027" && isset($_GET['ouvidoria_geral'])) {
			$id_departamento = "5";
		}
		if ($cliente == "1027" && isset($_GET['ouvidoria_saude'])) {
			$id_departamento = "6";
		}
		if ($cliente == "33" && isset($_GET['ouvidoria_geral'])) {
			$id_departamento = "5";
		}
		if ($cliente == "33" && isset($_GET['ouvidoria_saude'])) {
			$id_departamento = "6";
		}
		if ($cliente == "27" && isset($_GET['ouvidoria_saude'])) {
			$id_departamento = "8";
		}
		if ($cliente == "27" && isset($_GET['ouvidoria_tributacao'])) {
			$id_departamento = "13";
		}
		if ($cliente == "27" && isset($_GET['ouvidoria_geral'])) {
			$id_departamento = "7";
		}
		foreach ($qryDepartamento as $departamento) {

			echo "<option value='$departamento[id]'";

			if($departamento['id'] == $id_departamento) echo " selected";

				echo ">$departamento[descricao]</option>";

		}

?>

						</select>

					</div>

				</div>

<? 

	} 
	
?>


				

				<div class="form-group esconde_anonimo">

					<label for="nome" class="col-sm-2 control-label">Nome Completo</label>

					<div class="col-sm-10">

						<input type="text" name="nome" id="nome" class="form-control required" required value="<?= $nome ?>" placeholder="Informe seu nome...">

					</div>

				</div>

				<div class="form-group esconde_anonimo">

					<label for="cpf" class="col-sm-2 control-label">CPF</label>

					<div class="col-sm-10">

						<input type="text" name="cpf" id="cpf" class="form-control cpf" value="<?= $cpf ?>" placeholder="Informe seu CPF...">

					</div>

				</div>

				<div class="form-group esconde_anonimo">

					<label for="rg" class="col-sm-2 control-label">RG</label>

					<div class="col-sm-10">

						<input type="text" name="rg" id="rg" class="form-control" value="<?= $rg ?>" placeholder="Informe seu RG...">

					</div>

				</div>

				<div class="form-group esconde_anonimo">

					<label for="endereco" class="col-sm-2 control-label">Endere&ccedil;o</label>

					<div class="col-sm-10">

						<input type="text" name="endereco" id="endereco" class="form-control required" required value="<?= $endereco ?>" placeholder="Informe seu endere&ccedil;o...">

					</div>

				</div>

				<div class="form-group esconde_anonimo">

					<label for="bairro" class="col-sm-2 control-label">Bairro</label>

					<div class="col-sm-10">

						<input type="text" name="bairro" id="bairro" class="form-control required" required value="<?= $bairro ?>" placeholder="Informe seu bairro...">

					</div>

				</div>

				<div class="form-group esconde_anonimo">

					<label for="cidade" class="col-sm-2 control-label">Cidade</label>

					<div class="col-sm-10">

						<input type="text" name="cidade" id="cidade" class="form-control" value="<?= $cidade ?>" placeholder="Informe sua cidade...">

					</div>

				</div>

				<div class="form-group esconde_anonimo">

					<label for="cep" class="col-sm-2 control-label">CEP</label>

					<div class="col-sm-10">

						<input type="text" name="cep" id="cep" class="form-control cep" value="<?= $cep ?>" placeholder="Informe seu CEP...">

					</div>

				</div>

				<div class="form-group esconde_anonimo">

					<label for="telefone" class="col-sm-2 control-label">Telefone</label>

					<div class="col-sm-10">

						<input type="tel" name="telefone" id="telefone" class="form-control telefone" value="<?= $telefone ?>" placeholder="Informe seu telefone...">

					</div>

				</div>

				<div class="form-group" style="display: none;">

					<label for="telephone" class="col-sm-2 control-label">NÃO PREENCHA</label>

					<div class="col-sm-10">

						<input type="tel" name="telephone" id="telephone" class="form-control telephone" value="" placeholder="Informe seu telefone...">

					</div>

				</div>

				<div class="form-group esconde_anonimo">

					<label for="email" class="col-sm-2 control-label">E-mail</label>

					<div class="col-sm-10">

						<input type="email" name="email" id="email" class="form-control email" value="<?= $email ?>" placeholder="Informe seu e-mail...">

					</div>

				</div>

				<div class="form-group">

					<label for="solicitacao" class="col-sm-2 control-label">Solicita&ccedil;&atilde;o</label>

					<div class="col-sm-10">

						<textarea name="solicitacao" id="solicitacao" class="form-control" rows="6" placeholder="Descreva sua solicita&ccedil;&atilde;o..."><?= $solicitacao ?></textarea>

					</div>

				</div>

				<div class="form-group">

					<label for="anexo" class="col-sm-2 control-label">Anexo(s)</label>

					<div class="col-sm-10">

						<div class="input-group">

							<span class="input-group-btn">

								<span class="btn btn-primary btn-file">

									<i class="glyphicon glyphicon-folder-open"></i> Selecionar&hellip;

									<input type="file" name="anexo[]" id="anexo" multiple>

								</span>

							</span>

							<input type="text" class="form-control" readonly>

						</div>

						<span class="help-block">

							Selecione v&aacute;rios arquivos pressionando a tecla CTRL

						</span>

					</div>

				</div>

				<div class="form-group">

					<div class="col-sm-offset-2 col-sm-10 captcha" id="recaptcha1"></div>

				</div>

				<div class="form-group">

					<div class="col-sm-offset-2 col-sm-10">

						<input type="hidden" name="acao" value="cadastrar">

						<button type="reset" class="btn btn-danger">Limpar</button>

						<button type="submit" class="btn btn-primary">Enviar</button>

					</div>

				</div>

			</form>
		</div>

		<div class="col-md-12">
			<p>&nbsp;</p>
			<p>&nbsp;</p>

<?php
	
	$stCategoria = $conn->prepare("SELECT * 
									
									 FROM acesso_informacao_categoria 
									
									WHERE id_cliente = :id_cliente 
									  
									  AND status_registro = :status_registro 
								 
								 ORDER BY id DESC");

	$stCategoria->execute(array("id_cliente" => $cliente, "status_registro" => "A"));
	
	$qryCategoria = $stCategoria->fetchAll();

	if(count($qryCategoria)) {

?>

			<ul class="treeview">

<?php
	
		foreach ($qryCategoria as $categoria) {

?>

				<li><a href="#"><?= $categoria['descricao'] ?></a>
	
<?php

			$stArquivo = $conn->prepare("SELECT * 
										
										   FROM acesso_informacao 
										
										  WHERE id_categoria = :id_categoria 
											
											AND (id_subcategoria IS NULL OR id_subcategoria = '') 
											
											AND status_registro = :status_registro 
									
									   ORDER BY titulo DESC");
			
			$stArquivo->execute(array("id_categoria" => $categoria['id'], "status_registro" => "A"));
			
			$qryArquivo = $stArquivo->fetchAll();

			if(count($qryArquivo)) {

?>
		
					<ul>
		
<?php

				foreach($qryArquivo as $arquivo) {

					if($arquivo['arquivo'] != "") {

						$linkDocumento = $CAMINHOARQ . "/" . $arquivo['arquivo'];
						
						$imagem = "glyphicon-cloud-download";
						
						$titulo = empty($arquivo['titulo']) ? "Anexo" : $arquivo['titulo'];

					} else if($arquivo['link'] != "") {

						$linkDocumento = $arquivo['link'];
						
						$imagem = "glyphicon-globe";
						
						$titulo = empty($arquivo['titulo']) ? "Link" : $arquivo['titulo'];
					}

?>

						<li><a href="<?=$linkDocumento ?>" target="_blank"><i class="glyphicon <?= $imagem ?>"></i> <?= $titulo ?></a></li>
		
<?php 

				} 
		
?>
					
					</ul>

<?php 
			} 

			$stSubcategoria = $conn->prepare("SELECT * 
												
											    FROM acesso_informacao_subcategoria 
											
											   WHERE id_categoria = :id_categoria 
												
												 AND status_registro = :status_registro 
											
											ORDER BY id DESC");
			
			$stSubcategoria->execute(array("id_categoria" => $categoria['id'], "status_registro" => "A"));
			
			$qrySubcategoria = $stSubcategoria->fetchAll();

			if(count($qrySubcategoria)) {

?>
					
					<ul>

<?php

				foreach ($qrySubcategoria as $subcategoria) {

?>

						<li><a href="#"><?= $subcategoria['descricao'] ?></a>
			
<?php
				
					$stArquivo = $conn->prepare("SELECT * 
												
												   FROM acesso_informacao 
												
												  WHERE id_subcategoria = :id_subcategoria 
													
													AND status_registro = :status_registro 
											
											   ORDER BY titulo DESC");
					
					$stArquivo->execute(array("id_subcategoria" => $subcategoria['id'], "status_registro" => "A"));
					
					$qryArquivo = $stArquivo->fetchAll();

					if(count($qryArquivo)) {
			
?>

							<ul>
				
<?php

						foreach($qryArquivo as $arquivo) {

							if($arquivo['arquivo'] != "") {

								$linkDocumento = $CAMINHOARQ . "/" . $arquivo['arquivo'];
								
								$imagem = "glyphicon-cloud-download";
								
								$titulo = empty($arquivo['titulo']) ? "Anexo" : $arquivo['titulo'];

							} else if($arquivo['link'] != "") {

								$linkDocumento = $arquivo['link'];
								
								$imagem = "glyphicon-globe";
								
								$titulo = empty($arquivo['titulo']) ? "Link" : $arquivo['titulo'];
							}
				
?>
								<li><a href="<?= $linkDocumento ?>" target="_blank"><i class="glyphicon <?= $imagem ?>"></i> <?= $titulo ?></a></li>
				
<?php 

						} 
				
?>
							
							</ul>

<?php 

					} 
			
?>

						</li>

<?php 

				} 

?>

					</ul>
	
<?php 

			} 
		
?>

				</li>

<?php 
	
		} 

?>
			
			</ul>

			<p>&nbsp;</p>

<?php
	
	}

	if($cliente == "12148"){
		$stCategoria = $conn->prepare("SELECT * 
									     FROM relatorio_atendimento_categoria 
									    WHERE id_cliente = :id_cliente 
									      AND status_registro = :status_registro 
								     ORDER BY id DESC");
		$stCategoria->execute(array("id_cliente" => $cliente, "status_registro" => "A"));
		$qryCategoria = $stCategoria->fetchAll();

		if(count($qryCategoria)) {
?>

			<ul class="treeview">
	
<?php
			foreach ($qryCategoria as $categoria) {
?>

				<li><a href="#"><?= $categoria['descricao'] ?></a>
		
<?php
				$stArquivo = $conn->prepare("SELECT * 
											   FROM relatorio_atendimento 
											  WHERE id_categoria = :id_categoria 
												AND (id_subcategoria IS NULL OR id_subcategoria = '') 
												AND status_registro = :status_registro 
										   ORDER BY titulo DESC");
				$stArquivo->execute(array("id_categoria" => $categoria['id'], "status_registro" => "A"));
				$qryArquivo = $stArquivo->fetchAll();

				if(count($qryArquivo)) {
?>

			<ul>
			
<?php
					foreach($qryArquivo as $arquivo) {

						if($arquivo['arquivo'] != "") {

							$linkDocumento = $CAMINHOARQ . "/" . $arquivo['arquivo'];
							$imagem = "glyphicon-cloud-download";
							$titulo = empty($arquivo['titulo']) ? "Anexo" : $arquivo['titulo'];

						} else if($arquivo['link'] != "") {

							$linkDocumento = $arquivo['link'];
							$imagem = "glyphicon-globe";
							$titulo = empty($arquivo['titulo']) ? "Link" : $arquivo['titulo'];
						}
?>

				<li><a href="<?=$linkDocumento ?>" target="_blank"><i class="glyphicon <?= $imagem ?>"></i> <?= $titulo ?></a></li>
			
<?php 
					} 
?>

			</ul>
		
<?php 
				} 

				$stSubcategoria = $conn->prepare("SELECT * 
													FROM relatorio_atendimento_subcategoria 
												   WHERE id_categoria = :id_categoria 
													 AND status_registro = :status_registro 
											    ORDER BY id DESC");
				$stSubcategoria->execute(array("id_categoria" => $categoria['id'], "status_registro" => "A"));
				$qrySubcategoria = $stSubcategoria->fetchAll();

				if(count($qrySubcategoria)) {
?>

			<ul>
			
<?php
					foreach ($qrySubcategoria as $subcategoria) {
?>

				<li><a href="#"><?= $subcategoria['descricao'] ?></a>
				
<?php
						$stArquivo = $conn->prepare("SELECT * 
													   FROM relatorio_atendimento 
													  WHERE id_subcategoria = :id_subcategoria 
														AND status_registro = :status_registro 
												   ORDER BY titulo DESC");
						$stArquivo->execute(array("id_subcategoria" => $subcategoria['id'], "status_registro" => "A"));
						$qryArquivo = $stArquivo->fetchAll();

						if(count($qryArquivo)) {
?>

					<ul>
					
<?php
							foreach($qryArquivo as $arquivo) {

								if($arquivo['arquivo'] != "") {

									$linkDocumento = $CAMINHOARQ . "/" . $arquivo['arquivo'];
									$imagem = "glyphicon-cloud-download";
									$titulo = empty($arquivo['titulo']) ? "Anexo" : $arquivo['titulo'];

								} else if($arquivo['link'] != "") {

									$linkDocumento = $arquivo['link'];
									$imagem = "glyphicon-globe";
									$titulo = empty($arquivo['titulo']) ? "Link" : $arquivo['titulo'];
								}
?>

						<li><a href="<?= $linkDocumento ?>" target="_blank"><i class="glyphicon <?= $imagem ?>"></i> <?= $titulo ?></a></li>
					
<?php 
							} 
?>

					</ul>
				
<?php 
						} 
?>

				</li>
			
<?php 
					} 
?>

			</ul>
		
<?php 
				} 
?>

		</li>
	
<?php 
			} 
?>

	</ul>
	<p>&nbsp;</p>
<?php
		}

	}

	if($cliente == "11973") { 
		
?>

			<p>Em cumprimento &agrave; Lei Federal de Acesso &agrave; Informa&ccedil;&atilde;o P&uacute;blica (Lei nº 12.527/2011), a Prefeitura do Munic&iacute;pio de Pitangueiras coloca &agrave; disposi&ccedil;&atilde;o o Servi&ccedil;o de Informa&ccedil;&otilde;es ao Cidad&atilde;o – SIC.</p>

			<p>As informa&ccedil;&otilde;es poder&atilde;o ser obtidas junto &agrave; Secretaria de Administra&ccedil;&atilde;o e Finan&ccedil;as, da Prefeitura Municipal, sito &agrave; Avenida Central nº 408, ou atrav&eacute;s do telefone (43) 3257-1143, no hor&aacute;rio das 8h00min &agrave;s 11h30min e das 13h00min &agrave;s 17h00min</p>

			<p><b>Em cumprimento a Lei de acesso a informa&ccedil;&atilde;o</b>, este espaço &eacute; destinado para solicita&ccedil;&atilde;o de quaisquer informa&ccedil;&otilde;es a respeito da Administra&ccedil;&atilde;o P&uacute;blica.</p>

			<p>Se ainda houver necessidade de mais alguma informa&ccedil;&atilde;o, entre em contato conforme link abaixo para solicitar.</p><br>

<?php 

	} 
	
	if($cliente == "11924") { 
		
?>

			<p>Caso não tenha localizado o que procura no Portal, você pode fazer um pedido de informação por meio físico (SIC) ou eletrônico (e-SIC), da seguinte forma:<br><br>
				<b>SIC presencial</b><br><br>
				<b>Prefeitura de Catanduvas – (45) 3234-1313</b>
				<br><br>Endereço: Avenida dos Pioneiros, nº 500, Bairro Centro, CEP 85.470-000, Catanduvas – Paraná
				<br><br>Horário de Atendimento: Segunda à Sexta-Feira, das 08:30 às 11:30 - 13:30 às 17:00.
				<br><br>A Prefeitura Municipal de Catanduvas oferece ainda o e-SIC (Sistema Eletrônica do Serviço de Informações ao Cidadão), pelo qual qualquer cidadão pode formular seu pedido de informação de maneira fácil e rápida.
			</p>

<?php 

	} 
	
	if($cliente != "11973") { 
		
?>

			<p>Para solicitar informa&ccedil;&otilde;es adicionais do <strong>Portal da Transpar&ecirc;ncia</strong>, assim como outras informa&ccedil;&otilde;es p&uacute;blicas, clique no bot&atilde;o abaixo:</p>

<?php 

	} 
	
?>                                                     

			<div class="text-center">   
	
<?php

	if (count($confSistemaOuvidoria) <= 0 && $cliente != "12097") { 
		
?>


				<a href="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia . $ouvidoria . $complemento); ?>">
					
					<img src="<?= $CAMINHO ?>/images/ico_acesso_informacao.png" alt="Ouvidoria"/>
				
				</a>
		
<? 

 
	
	} 
	
	if($cliente != "11924") {
		
		if ($configuracaoTransparencia['exibir_fale_conosco'] == "S") {

			$iconeFaleConosco = $cliente == "11945" ? "ico_esic.png" : "ico_fale_conosco.png";

			if ($configuracaoTransparencia['fale_conosco_ouvidoria'] == "S") {

				if (count($confSistemaOuvidoria) > 0) {
					
?>

				<a href="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia . $sistemaGestaoOuvidoria . $complemento); ?>">
					
					<img src="<?= $CAMINHO ?>/images/<?= $iconeFaleConosco ?>" alt="Fale Conosco"/>
				
				</a>
				
<?php 
	
				} else { 
					
					if($cliente != "12097") {
						?>
					
					<a href="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia . $ouvidoria . $complemento); ?>">
					
					<img src="<?= $CAMINHO ?>/images/<?= $iconeFaleConosco ?>" alt="Fale Conosco"/>
				
				</a>
				<?php
			}
			?>
<?php

				}

			} else {
				
				if($cliente != "12097") {
					?>

				<a href="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia . $contato . $complemento); ?>">
					
					<img src="<?= $CAMINHO ?>/images/<?= $iconeFaleConosco ?>" alt="Fale Conosco"/>
				
				</a>

				<?php
			}
			?>
<?php

			}

		}

	}

	if($cliente == "11924"){

		if (count($confSistemaOuvidoria) > 0) {
			
?>

				<a href="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia . $sistemaGestaoOuvidoria . $complemento); ?>">
					
					<img src="<?= $CAMINHO ?>/images/ico_ouvidoria.png" alt="Ouvidoria"/>
				
				</a>

<?php 

		} else { 
			
?>

				<a href="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia . $ouvidoria . $complemento); ?>">
					
					<img src="<?= $CAMINHO ?>/images/ico_ouvidoria.png" alt="Ouvidoria"/>
				
				</a>
		
<?php
		
		}

?>

				<a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia . $relatorioAtendimento . $complemento); ?>">
					
					<img src="<?=$CAMINHO ?>/images/ico_relatorio_atendimentos.png" alt="Relat&oacute;rios Estat&iacute;sticos de Atendimento"/>
				
				</a>

<?php
	
	}

	
	if($cliente == "12000") {

?>

				<a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia . $relatorioAtendimento . $complemento); ?>">
					
					<img src="<?=$CAMINHO ?>/images/ico_relatorio_atendimentos.png" alt="Relat&oacute;rios Estat&iacute;sticos de Atendimento"/> 
				
				</a>
<br>
<?php
	
	}





	$stConfSicCidadao = $conn->prepare("SELECT id 
										  
										  FROM cliente_configuracao_transparencia_icone_cliente 
										 
										 WHERE id_cliente = :id_cliente 
										   
										   AND id_icone = :id_icone");
	
	$stConfSicCidadao->execute(array("id_cliente" => $cliente, "id_icone" => 51));
	
	$confSicCidadao = $stConfSicCidadao->fetchAll();
	
	if(count($confSicCidadao)) {

		if(empty($configuracaoTransparencia['caminho_sic_cidadao'])) {

			$linkSic = $CAMINHO . "/index.php?sessao=$sequencia" . $sicCidadao . "$complemento";

		} else {

			$linkSic = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=sic_cidadao&redir=link";

		}

?>

			<a href="<?=$linkSic ?>">
				
				<img src="<?=$CAMINHO ?>/images/ico_sic_fisico.png" alt="Servi&ccedil;o F&iacute;sico de Informa&ccedil;&atilde;o ao Cidad&atilde;o" />
			
			</a>

<?php 
	
	} 
	
?>

		</div>
		
	</div>

</div>

<div class="col-md-3">

	<div class="panel panel-primary">

		<div class="panel-heading">

			<h2 class="panel-title"><i class="glyphicon glyphicon-search"></i> Protocolo</h2>

		</div>

		<div class="panel-body">

			<form class="form-horizontal" action="" method="post">

				<div class="input-group">

					<input type="search" name="busca_protocolo" class="form-control" placeholder="Informe o n&ordm; do protocolo...">

					<span class="input-group-btn">

						<button class="btn btn-primary" type="submit">Buscar</button>

					</span>

				</div>

			</form>

		</div>

	</div>

	<div>	
			
<?php
							
	$pendente = 0;
	
	$emAnalise = 0;
	
	$finalizada = 0;
	
	$emAndamento = 0;

	$indeferida = 0;
	
	$informacao = 0;
	
	$reclamacao = 0;
	
	$sugestao = 0;
		
	$denuncia = 0;

	$elogio = 0;
				
	$sql = "SELECT pref_ouvidoria.*,
				   
				   pref_ouvidoria_departamento.descricao departamento
			  
			  FROM pref_ouvidoria
		 
		 LEFT JOIN pref_ouvidoria_departamento 
		        
				ON pref_ouvidoria.id_departamento = pref_ouvidoria_departamento.id
					
			 WHERE pref_ouvidoria.id_cliente = :id_cliente ";


	if($_POST['dataInicial'] != "" && $_POST['cancelar_form'] != 'cancelar'){
			
		$dataInicial = secure($_REQUEST['dataInicial']);
			
		$sql .= " AND pref_ouvidoria.data_solicitacao >= :dataInicial ";
		
	}

	if(isset($_GET['ouvidoria_saude'])){
			
		$sql .= " AND pref_ouvidoria_departamento.id = :id_departamento ";
		
	}

	if($_POST['dataFinal'] != ""  && $_POST['cancelar_form'] != 'cancelar' ){
		
		$dataFinal = secure($_REQUEST['dataFinal']);
		
		$sql .= " AND pref_ouvidoria.data_solicitacao <= :dataFinal ";
	}


	$sql .= " AND pref_ouvidoria.status_registro = :status_registro ORDER BY pref_ouvidoria.id DESC ";

	$stStatus = $conn->prepare($sql);
	
	$stStatus->bindValue(':id_cliente', $cliente , PDO::PARAM_INT);
	
	$stStatus->bindValue(':status_registro', "A" , PDO::PARAM_STR);

	if($dataInicial != ""  && $_POST['cancelar_form'] != 'cancelar' ){
		
		$stStatus->bindValue(':dataInicial', $dataInicial , PDO::PARAM_STR);
	
	}

	if(isset($_GET['ouvidoria_saude'])){
			
		$stStatus->bindValue(':id_departamento', $id_departamento, PDO::PARAM_STR);
		
	}

	if($dataFinal != ""  && $_POST['cancelar_form'] != 'cancelar'){
		
		$stStatus->bindValue(':dataFinal', $dataFinal , PDO::PARAM_STR);
	
	}

	$stStatus->execute();

	$lista = $stStatus->fetchAll();

	unset($_REQUEST['dataInicial']);
	
	unset($_REQUEST['dataFinal']);
			
	foreach ($lista as $item => $value) {

		$st2 = $conn->prepare("SELECT status 
								 
								 FROM pref_ouvidoria_andamento 
								 
								WHERE id_artigo = :id 
								
							 ORDER BY id DESC 
							 
							    LIMIT 1");
		
		$st2->execute(array("id" => $value['id'] ));
		
		$buscaSituacao = $st2->fetch();

		$st3 = $conn->prepare("SELECT data_solicitacao 
		  						 
								 FROM pref_ouvidoria 
								
								WHERE pref_ouvidoria.id_cliente = :id_cliente");
		
		$st3->bindValue(':id_cliente', $cliente , PDO::PARAM_INT);
		
		$st3->execute();
		
		$buscaData = $st3->fetch();

		//echo $buscaData['data_solicitacao'];
		
		$buscaDataOK = substr($buscaData['data_solicitacao'], 0, 10);
		
		if($buscaSituacao['status'] == "Pendente"){
			
			$pendente += 1;
			
		}
		
		if($buscaSituacao['status'] == "Em Análise" || $buscaSituacao['status'] == "Em An&aacute;lise" ){
		
			$emAnalise += 1;
			
		}
		
		if($buscaSituacao['status'] == "Finalizada"){
			
			$finalizada += 1;
			
		}
		
		if($buscaSituacao['status'] == "Em Andamento"){
			
			$emAndamento += 1;
			
		}

		if($buscaSituacao['status'] == "Indeferida"){
			
			$indeferida += 1;
			
		}
		
		if(utf8_encode($value['tipo']) == "Informação" || $value['tipo'] == "Informa&ccedil;&atilde;o"){
			
			$informacao += 1;
			
		}
		
		if(utf8_encode($value['tipo']) == "Reclamação" || $value['tipo'] == "Reclama&ccedil;&atilde;o"){
			
			$reclamacao += 1;
			
		}
		
		if(utf8_encode($value['tipo']) == "Sugestão" || $value['tipo'] == "Sugest&atilde;o"){
			
			$sugestao += 1;
			
		}

		if(utf8_encode($value['tipo']) == "Elogios"){
			
			$elogio += 1;
			
		}

		if(utf8_encode($value['tipo']) == "Denúncias" || $value['tipo'] == "Den&uacute;ncias"){
			
			$denuncia += 1;
			
		}
		
	} 
	
?>

		<div class="panel panel-primary">
			
			<div class="panel-heading">
			
				<h2 class="panel-title"><i class="glyphicon glyphicon-search"></i> Pesquisa por data</h2>
			
			</div>

			<div class="panel-body">
			
				<form  action="" method="post">
			
					<div>
			
						<div class="form-group">
			
							<label for="dataInicial">Data inicial:</label>
						
							<input class="form-control" id="dataInicial" type="date" name="dataInicial" value="<?=$dataInicial?>" class="form-control">
						
						</div>
						
						<div class="form-group">
						
							<label for="dataFinal">Data final:</label>
						
							<input class="form-control" id="dataFinal" type="date" name="dataFinal" value="<?=$dataFinal?>" class="form-control">
						
						</div>
						
						<span class="input-btn">
							
							<br><button class="btn btn-primary" type="submit">Filtrar</button>
							
							<input class="btn btn-default pull-right" type="submit" name="cancelar_form" value="cancelar">
						
						</span>
					
					</div>
				
				</form>
			
			</div>
		
		</div>
	
		<ul class="list-group panel panel-primary ">

			<li class="list-group-item active"><h2 class="panel-title">Relatório Estatistico</h2></li>
			
			<div class="">
				
				<div id="piechart2" style="height: 200px;"></div>
				
				<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
			    
				<script type="text/javascript">
			    	
					google.charts.load("current", {packages:["corechart"]});
			      
				  	google.charts.setOnLoadCallback(drawChart2);
			      
				  	function drawChart2() {
			        
						var data2 = google.visualization.arrayToDataTable([
			          
					  		['Grafico', 'Relatórios'],
			          
					  		['Pendente',  <? echo $pendente ?> ],
			          		
							['Em Análise',  <? echo $emAnalise ?> ],
			          
					  		['Finalizado', <? echo $finalizada ?> ],
			          
					  		['Em Andamento', <? echo $emAndamento ?> ],

							['Indeferida', <? echo $indeferida ?> ]
			         
					 	]);

				      	var options2 = {
				        
							legend: 'none',
				        
							pieSliceText: 'label',
				        
							title: '',
				        
							pieStartAngle: 100,
				        
							colors: ['#DC3912', '#FF9900', '#3366CC', '#109518', '#990099']
				      	};

				        var chart2 = new google.visualization.PieChart(document.getElementById('piechart2'));
				        
						chart2.draw(data2, options2);
				      
					  }

				</script>
				    
			</div>

			<li class="list-group-item">Total de pedidos<span class="badge"><?= count($lista)?></span></li>
			
			<li class="list-group-item">Pendente<span style="background-color: #DC3912;" class="badge"><?= $pendente ?></span></li> 
			
			<li class="list-group-item">Em an&aacute;lise<span style="background-color:#FF9900;" class="badge"><?= $emAnalise ?></span></li> 
			
			<li class="list-group-item">Finalizado<span style="background-color: #3366CC;" class="badge"><?= $finalizada ?></span></li>
			
			<li class="list-group-item">Em andamento<span style="background-color: #109518;" class="badge"><?= $emAndamento ?></span></li> 

			<li class="list-group-item">Indeferida<span style="background-color: #990099;" class="badge"><?= $indeferida ?></span></li> 
			
			<li class="list-group-item active">Tipos:</li>
			
			<div class="">
			
				<div id="piechart" style="height: 200px;"></div>
				
			    <script type="text/javascript">
			      
			    	google.charts.setOnLoadCallback(drawChart);
			      
				  	function drawChart() {
			        
						var data = google.visualization.arrayToDataTable([
			        	
			          		['Graficos', 'tipos'],
			          
					  		['informação',  <? echo $informacao ?>],
			          
					  		['reclamação',  <? echo $reclamacao ?>],
			          
					  		['sugestão', <? echo $sugestao ?> ],

							['elogio', <? echo $elogio ?> ],

							['denúncia', <? echo $denuncia ?> ]
			          	]);

			      		var options = {
			        
							legend: 'none',
			        
							pieSliceText: 'label',
			        
							title: '',
			        
							pieStartAngle: 100
			      		};

			        	var chart = new google.visualization.PieChart(document.getElementById('piechart'));
			        
						chart.draw(data, options);
			      	}

			    </script>
			  
			</div>
			  
			<li class="list-group-item">Informa&ccedil;&atilde;o<span style="background-color: #3366cc;" class="badge"><?= $informacao ?></span></li>
			  
			<li class="list-group-item">Reclama&ccedil;&atilde;o<span style="background-color:#dc3912;" class="badge"><?= $reclamacao ?></span></li>
			  
			<li class="list-group-item">Sugest&atilde;o<span style="background-color: #ff9900;" class="badge"><?= $sugestao ?></span></li>

			<li class="list-group-item">Elogio<span style="background-color: #109618;" class="badge"><?= $elogio ?></span></li> 

			<li class="list-group-item">Den&uacute;ncia<span style="background-color: #990099;" class="badge"><?= $denuncia ?></span></li>   
			
		</ul>
		
	</div>

</div>
</div>

<?php 

	if($cliente == "46" && isset($_GET['ouvidoria_geral'])) { 
		
?>

<p class="text-center">

	<a href="http://ubirata.pr.gov.br/arquivos/requerimento.doc" target="_blank">
	
		<img src="http://www.controlemunicipal.com.br/site/prefeitura/images/requerimento_protocolo_pessoal.png" class="imagem" />
		
	</a>
	
</p>

<?php 

	} 
	
?>
