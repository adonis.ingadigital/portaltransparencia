<h2>Relat&oacute;rio Viagem / Sa&uacute;de</h2>
<ol class="breadcrumb">
	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
	<li class="active">Relat&oacute;rio Viagem / Sa&uacute;de</li>
</ol>

<?php if($cliente === "1093"){ ?>
<div style="text-align: center;">
	<a href="<?=$CAMINHOCMGERAL ?>/licitacao/esProposta2016candidodeabreu.exe" target="_blank"><img src="<?=$CAMINHOCMGERAL ?>/images/esproposta.jpg" class="imagem" /></a>
</div>
<?php } ?>

<?php
$stCategoria = $conn->prepare("SELECT * FROM relatorio_viagem_saude_categoria WHERE id_cliente = :id_cliente AND status_registro = :status_registro ORDER BY id DESC");
$stCategoria->execute(array("id_cliente" => $cliente, "status_registro" => "A"));
$qryCategoria = $stCategoria->fetchAll();

if(count($qryCategoria)) {
?>
<ul class="treeview">
	<?php
	foreach ($qryCategoria as $categoria) {
	?>
	<li><a href="#"><?= $categoria['descricao'] ?></a>
		<?php
		$stArquivo = $conn->prepare("SELECT * FROM relatorio_viagem_saude WHERE id_categoria = :id_categoria AND (id_subcategoria IS NULL OR id_subcategoria = '') AND status_registro = :status_registro ORDER BY descricao DESC");
		$stArquivo->execute(array("id_categoria" => $categoria['id'], "status_registro" => "A"));
		$qryArquivo = $stArquivo->fetchAll();

		if(count($qryArquivo)) {
		?>
		<ul>
			<?php
			foreach($qryArquivo as $arquivo) {
                if($cliente == 43){
                    $caminhoArquivo = $CAMINHOCMGERAL."/licitacao/relatorio_viagem_saude.php?cliente=$cliente&arquivo=".$arquivo['arquivo'];
                }else{
                    $caminhoArquivo = $CAMINHOARQ."/".$arquivo['arquivo'];
                }
			?>
			<li><a href="<?=$caminhoArquivo?>" target="_blank"><i class="glyphicon glyphicon-cloud-download"></i> <?= $arquivo['descricao'] ?></a></li>
			<?php } ?>
		</ul>
		<?php } ?>
		<?php
		$stSubcategoria = $conn->prepare("SELECT * FROM relatorio_viagem_saude_subcategoria WHERE id_categoria = :id_categoria AND status_registro = :status_registro ORDER BY id DESC");
		$stSubcategoria->execute(array("id_categoria" => $categoria['id'], "status_registro" => "A"));
		$qrySubcategoria = $stSubcategoria->fetchAll();

		if(count($qrySubcategoria)) {
		?>
		<ul>
			<?php
			foreach ($qrySubcategoria as $subcategoria) {
			?>
			<li><a href="#"><?= $subcategoria['descricao'] ?></a>
				<?php
				$stArquivo = $conn->prepare("SELECT * FROM relatorio_viagem_saude WHERE id_subcategoria = :id_subcategoria AND status_registro = :status_registro ORDER BY descricao DESC");
				$stArquivo->execute(array("id_subcategoria" => $subcategoria['id'], "status_registro" => "A"));
				$qryArquivo = $stArquivo->fetchAll();

				if(count($qryArquivo)) {
				?>
				<ul>
					<?php
					foreach($qryArquivo as $arquivo) {
					  if($cliente == 43){
                          $caminhoArquivo = $CAMINHOCMGERAL."/licitacao/relatorio_viagem_saude.php?cliente=$cliente&arquivo=".$arquivo['arquivo'];
                      }else{
                         $caminhoArquivo = $CAMINHOARQ."/".$arquivo['arquivo'];
                      }?>
			         <li><a href="<?=$caminhoArquivo?>" target="_blank"><i class="glyphicon glyphicon-cloud-download"></i> <?= $arquivo['descricao'] ?></a></li>
					<?php } ?>
				</ul>
				<?php } ?>
			</li>
			<?php } ?>
		</ul>
		<?php } ?>
	</li>
	<?php } ?>
</ul>
<?php
}

$atualizacao = atualizacao("relatorio_viagem_saude", $cliente, $conn);
if($atualizacao != "") {
?>
<p class="text-right"><small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small></p>
<?php
}