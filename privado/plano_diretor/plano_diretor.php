<h2>Plano Diretor</h2>
<ol class="breadcrumb">
	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
	<li class="active">Plano Diretor</li>
</ol>

<?php
$stCategoria = $conn->prepare("SELECT * FROM pref_plano_diretor_categoria WHERE id_cliente = :id_cliente AND status_registro = :status_registro ORDER BY id DESC");
$stCategoria->execute(array("id_cliente" => $cliente, "status_registro" => "A"));
$qryCategoria = $stCategoria->fetchAll();

if(count($qryCategoria)) {
?>
<ul class="treeview">
	<?php
	foreach ($qryCategoria as $categoria) {
	?>
	<li><a href="#"><?= $categoria['categoria'] ?></a>
		<?php
		$stPlano = $conn->prepare("SELECT * FROM pref_plano_diretor WHERE id_categoria = :id_categoria AND status_registro = :status_registro ORDER BY titulo DESC");
		$stPlano->execute(array("id_categoria" => $categoria['id'], "status_registro" => "A"));
		$qryPlano = $stPlano->fetchAll();

		if(count($qryPlano)) {
		?>
		<ul>
			<?php
			foreach($qryPlano as $plano) {
			?>
			<li><a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$planoVisualiza.$complemento); ?>&id=<?= verifica($plano['id']) ?>"><i class="glyphicon glyphicon-list-alt"></i> <?= $plano['titulo'] ?></a></li>
			<?php } ?>
		</ul>
		<?php } ?>
	</li>
	<?php } ?>
</ul>
<?php
}

$atualizacao = atualizacao("plano_diretor", $cliente, $conn);
if($atualizacao != "") {
?>
<p class="text-right"><small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small></p>
<?php
}