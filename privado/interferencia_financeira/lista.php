<h2>Interfer&ecirc;ncias Financeiras</h2>
<ol class="breadcrumb">
	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
	<li class="active">Interfer&ecirc;ncias Financeiras</li>
</ol>

<?php if($cliente == "43") { ?>
<p>Atendendo aos requisitos do Par&aacute;grafo &Uacute;nico do Art. 13 da Lei 10.836 de janeiro de 2004, que cria o Programa Bolsa Fam&iacute;lia, e de acordo com o Decreto 5.209/2004, Art.32 &sect;1&ordm; diz que a RELA&Ccedil;&Atilde;O DOS BENEFICIARIOS PROGRAMA BOLSA FAMILIA, dever&aacute; ser amplamente divulgada pelo Poder Publico Municipal e do Distrito Federal.</p>
<p>O munic&iacute;pio divulgar&aacute; a lista de Benefici&aacute;rios de acordo com a folha de pagamento disponibilizada no M&ecirc;s de Refer&ecirc;ncia, pela Caixa Econ&ocirc;mica Federal e MDS.</p>
<?php
}

$stCategoria = $conn->prepare("SELECT * FROM interferencia_financeira_categoria WHERE id_cliente = :id_cliente AND status_registro = :status_registro ORDER BY id DESC");
$stCategoria->execute(array("id_cliente" => $cliente, "status_registro" => "A"));
$qryCategoria = $stCategoria->fetchAll();

if(count($qryCategoria)) {
?>
<ul class="treeview">
	<span style="font-weight: bold;">INTERFER&Ecirc;NCIA FINANCEIRA: S&Atilde;O OPERAÇ&Otilde;ES DE TRANSFER&Ecirc;NCIA ENTRE &Oacute;RG&Atilde;OS NO &Acirc;MBITO DE UM MESMO ENTE P&Uacute;BLICO</span>
	<?php
	foreach ($qryCategoria as $categoria) {
	?>
	<li><a href="#"><?= $categoria['descricao'] ?></a>
		<?php
		$stArquivo = $conn->prepare("SELECT * FROM interferencia_financeira WHERE id_categoria = :id_categoria AND (id_subcategoria IS NULL OR id_subcategoria = '') AND status_registro = :status_registro ORDER BY titulo DESC");
		$stArquivo->execute(array("id_categoria" => $categoria['id'], "status_registro" => "A"));
		$qryArquivo = $stArquivo->fetchAll();

		if(count($qryArquivo)) {
		?>
		<ul>
			<div class="panel-group" id="accordion_<?= $categoria['id'] ?>" role="tablist" aria-multiselectable="true">
				<?php
				foreach($qryArquivo as $arquivo) {

					if($arquivo['arquivo'] != "") {

						$linkDocumento = $CAMINHOARQ . "/" . $arquivo['arquivo'];
						$imagem = "glyphicon-cloud-download";
						$titulo = empty($arquivo['titulo']) ? "Anexo" : $arquivo['titulo'];
						$titulo2 = "Arquivo";

					} else if($arquivo['link'] != "") {

						$linkDocumento = $arquivo['link'];
						$imagem = "glyphicon-globe";
						$titulo = empty($arquivo['titulo']) ? "Link" : $arquivo['titulo'];
						$titulo2 = "Link";
					}
				?>
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="heading_<?= $arquivo['id'] ?>">
				     	<h4 class="panel-title">
				        	<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?= $arquivo['id'] ?>" aria-expanded="true" aria-controls="collapse_<?= $arquivo['id'] ?>">
				          		<i class="glyphicon glyphicon-triangle-right"></i> <?= $titulo?>
				        	</a>
				    	</h4>
				    </div>
				    <div id="collapse_<?= $arquivo['id'] ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_<?= $arquivo['id'] ?>">
						<div class="panel-body">
							<div class="panel panel-primary">
								<div class="panel-heading">
							    	<h2 class="panel-title"><i class="glyphicon glyphicon-cloud-download"></i> <?= $titulo2?></h2>
							  	</div>
								<li>
									<a href="<?=$linkDocumento ?>" target="_blank"><i class="glyphicon <?= $imagem ?>"></i> <?= $titulo ?></a>
								</li>
							</div>			
						</div>
					</div>
				</div>	
				<?php } ?>
			</div>
		</ul>
		<?php } ?>
		<?php
		$stSubcategoria = $conn->prepare("SELECT * FROM interferencia_financeira_subcategoria WHERE id_categoria = :id_categoria AND status_registro = :status_registro ORDER BY id DESC");
		$stSubcategoria->execute(array("id_categoria" => $categoria['id'], "status_registro" => "A"));
		$qrySubcategoria = $stSubcategoria->fetchAll();

		if(count($qrySubcategoria)) {
		?>
		<ul>
			<?php
			foreach ($qrySubcategoria as $subcategoria) {
			?>
			<li><a href="#"><?= $subcategoria['descricao'] ?></a>
				<?php
				$stArquivo = $conn->prepare("SELECT * FROM interferencia_financeira WHERE id_subcategoria = :id_subcategoria AND status_registro = :status_registro ORDER BY titulo DESC");
				$stArquivo->execute(array("id_subcategoria" => $subcategoria['id'], "status_registro" => "A"));
				$qryArquivo = $stArquivo->fetchAll();

				if(count($qryArquivo)) {
				?>
				<ul>
					<div class="panel-group" id="accordion_<?= $subcategoria['id'] ?>" role="tablist" aria-multiselectable="true">
						<?php
						foreach($qryArquivo as $arquivo) {

							if($arquivo['arquivo'] != "") {

								$linkDocumento = $CAMINHOARQ . "/" . $arquivo['arquivo'];
								$imagem = "glyphicon-cloud-download";
								$titulo = empty($arquivo['titulo']) ? "Anexo" : $arquivo['titulo'];
								$titulo2 = "Arquivo";

							} else if($arquivo['link'] != "") {

								$linkDocumento = $arquivo['link'];
								$imagem = "glyphicon-globe";
								$titulo = empty($arquivo['titulo']) ? "Link" : $arquivo['titulo'];
								$titulo2 = "Link";
							}
						?>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="heading_<?= $arquivo['id'] ?>">
						     	<h4 class="panel-title">
						        	<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?= $arquivo['id'] ?>" aria-expanded="true" aria-controls="collapse_<?= $arquivo['id'] ?>">
						          		<i class="glyphicon glyphicon-triangle-right"></i> <?= $arquivo['titulo'] ?>
						        	</a>
						    	</h4>
						    </div>
							<div id="collapse_<?= $arquivo['id'] ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_<?= $arquivo['id'] ?>">
								<div class="panel-body">
									<div class="panel panel-primary">
										<div class="panel-heading">
									    	<h2 class="panel-title"><i class="glyphicon glyphicon-cloud-download"></i> <?= $titulo2 ?></h2>
									  	</div>
										<li><a href="<?= $linkDocumento ?>" target="_blank"><i class="glyphicon <?= $imagem ?>"></i> <?= $titulo ?></a></li>
									</div>
								</div>
							</div>
						</div>
					</div>						
					<?php } ?>
				</ul>
				<?php } ?>
			</li>
			<?php } ?>
		</ul>
		<?php } ?>
	</li>
	<?php } ?>
</ul>
<?php
}

$atualizacao = atualizacao("interferencia_financeira", $cliente, $conn);
if($atualizacao != "") {
?>
<p class="text-right"><small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small></p>
<?php
}