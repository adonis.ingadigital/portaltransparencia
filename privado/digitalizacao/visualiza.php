<?php
$stmt = $conn->prepare("SELECT * FROM cliente_digitalizacao WHERE id_cliente = :id_cliente AND status_registro = :status_registro LIMIT 1");
$stmt->execute(array("id_cliente" => $cliente, "status_registro" => "A"));

$row = $stmt->fetch();

$servidor = $row['servidor'];
$database = $row['database'];
//$usuario = "root";
//$senha = "1553bb7b4a8712761c334cb357de5fd2";
//


$usuario = "root";
$senha = "1553bb7b4a8712761c334cb357de5fd2";

if($cliente == '111'){
    $usuario = "root";
    $senha = "1ng@1nf11sql";
} else {
    $usuario = "root_externo";
    $senha = "1553bb7b4a8712761c334cb357de5fd2";
} 

$id = secure($_GET['id']);
$id_tipo = secure($_GET['id_tipo']);

try {

    $conn_dig = new PDO("mysql:host=$servidor;dbname=$database;charset=utf8", $usuario, $senha);
    $conn_dig->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $conn_dig->prepare("SELECT documento.*,
	                        		   documento_tipo.descricao tipo,
	                        		   setor.descricao setor,
	    							   documento_categoria.descricao categoria,
	    							   documento_subcategoria.descricao subcategoria
	                        	  FROM documento
	                         LEFT JOIN documento_anexo ON documento_anexo.id_documento = documento.id
	                         LEFT JOIN documento_tipo ON documento_tipo.id = documento.id_tipo
	                         LEFT JOIN documento_categoria ON documento_categoria.id = documento.id_categoria
	                         LEFT JOIN documento_subcategoria ON documento_subcategoria.id = documento.id_subcategoria
	                         LEFT JOIN setor ON setor.id = documento_tipo.id_setor
	                        	 WHERE documento.id = :id
	                        	   AND documento.status_registro = :status_registro");
    $stmt->execute(array("id" => $id, "status_registro" => "A"));

    $documento = $stmt->fetch();

    if (empty($documento['id'])) {
        ?>
        <div class="jumbotron">
            <h1>P&aacute;gina n&atilde;o encontrada!</h1>
            <h3>A p&aacute;gina solicitada n&atilde;o foi localizada. Verifique a ortografia.</h3>
        </div>
        <?php
    } else {
        ?>
        <h2><?= $documento['tipo'] ?></h2>
        <?php
        $stTotalAnexo = $conn_dig->prepare("SELECT id
	                						  FROM documento_anexo
	                						 WHERE id_documento = :id_documento
	                						   AND status_registro = :status_registro");

        $stTotalAnexo->execute(array("id_documento" => $documento['id'], "status_registro" => "A"));
        $rsTotalAnexo = $stTotalAnexo->fetchAll();
        $numDoc = count($rsTotalAnexo);

        if ($_GET['id_anexo']) {

            $stAnexo = $conn_dig->prepare("SELECT *
	                            			 FROM documento_anexo
	                            			WHERE id_documento = :id_documento
	                                          AND id = :id
	                            			  AND status_registro = :status_registro
	                                     ORDER BY id LIMIT 1");

            $stAnexo->execute(array("id_documento" => $documento['id'], "id" => secure($_GET['id_anexo']), "status_registro" => "A"));
        } else {

            $stAnexo = $conn_dig->prepare("SELECT *
	                            			 FROM documento_anexo
	                            			WHERE id_documento = :id_documento
	                            			  AND status_registro = :status_registro
	                                     ORDER BY id LIMIT 1");

            $stAnexo->execute(array("id_documento" => $documento['id'], "status_registro" => "A"));
        }

        $rsAnexo = $stAnexo->fetch();
        ?>
        <ol class="breadcrumb">
            <li><a href="<?= $CAMINHO ?>">In&iacute;cio</a></li>
            <li>
                <a href="<?= $CAMINHO ?>/index.php?sessao=<?= $sequencia . $digitalizacaoLista . $complemento ?>&id_tipo=<?= $id_tipo ?>"><?= verifica($documento['tipo']) ?></a>
            </li>
            <?php if ($documento['categoria'] != "") { ?>
                <li>
                    <a href="<?= $CAMINHO ?>/index.php?sessao=<?= $sequencia . $digitalizacaoLista . $complemento ?>&id_tipo=<?= $id_tipo ?>&id_categoria=<?= $documento['id_categoria'] ?>"><?= verifica($documento['categoria']) ?></a>
                </li>
            <?php } ?>
            <?php if ($documento['subcategoria'] != "") { ?>
                <li>
                    <a href="<?= $CAMINHO ?>/index.php?sessao=<?= $sequencia . $digitalizacaoLista . $complemento ?>&id_tipo=<?= $id_tipo ?>&id_subcategoria=<?= $documento['id_subcategoria'] ?>"><?= verifica($documento['subcategoria']) ?></a>
                </li>
            <?php } ?>
            <li class="active"><?= $numDoc ?> Documento(s)</li>
        </ol>

        <div class="btn-toolbar" role="toolbar" aria-label="Ferramentas">
            <div class="btn-group" role="group" aria-label="Documento">
                <a href="<?= $CAMINHO ?>/index.php?sessao=<?= $sequencia . $digitalizacaoLista . $complemento ?>&id_tipo=<?= $id_tipo ?>"
                   class="btn btn-primary"><i class="glyphicon glyphicon-arrow-left"></i> Voltar</a>
                <?php
                if ($numDoc > 200) {
                    ?>
                    <a href="<?= $CAMINHO ?>/digitalizacao/zip/carregazip.php?id=<?= $documento["id"] ?>&cliente=<?=$cliente?>&servidor=<?= $servidor ?>&database=<?= $database ?>&val=<?=$cliente?>"
                       target="_blank" class="btn btn-primary"><i class="glyphicon glyphicon-compressed"></i> Baixar
                        anexos</a>
                    <?
                } else {
                    ?>
                    <a href="<?= $CAMINHO ?>/digitalizacao/zip/zip.php?id=<?= $documento["id"] ?>&cliente=<?=$cliente?>&servidor=<?= $servidor ?>&database=<?= $database ?>&val=<?=$cliente?>"
                       target="_blank" class="btn btn-primary"><i class="glyphicon glyphicon-compressed"></i> Baixar
                        anexos</a>
                <? } ?>
                <?php

                $linkTodosAnexos = $servidor;
                $linkTodosAnexos .= ";";
                $linkTodosAnexos .= $database;
                $linkTodosAnexos .= ";";
                $linkTodosAnexos .= $cliente;
                $linkTodosAnexos .= ";";
                $linkTodosAnexos .= $documento['id'];

                $linkTodosAnexos = base64_encode($linkTodosAnexos);

                ?>
                <a href="<?= $CAMINHO ?>/digitalizacao/todos_anexos_pdf.php?data=<?= $linkTodosAnexos ?>"
                   class="btn btn-primary" target="_blank">
                    <i class="glyphicon glyphicon-print"></i> Imprimir todos
                </a>
            </div>

            <div class="btn-group" role="group" aria-label="Pagina&ccedil;&atilde;o">
                <?php
                $stPrimeiro = $conn_dig->prepare("SELECT id
											    FROM documento_anexo
											   WHERE id_documento = :id_documento
												 AND status_registro = :status_registro
										    ORDER BY id ASC
											   LIMIT 1");

                $stPrimeiro->execute(array("id_documento" => $documento['id'], "status_registro" => "A"));
                $rsPrimeiro = $stPrimeiro->fetch();
                if (count($rsPrimeiro) > 1 && $rsPrimeiro['id'] != $rsAnexo['id']) {

                    echo "<a href='$CAMINHO/index.php?sessao=$_GET[sessao]&id_tipo=$id_tipo&id=$id&id_anexo=$rsPrimeiro[id]' class='btn btn-default' data-toggle='tooltip' data-placement='bottom' title='Primeiro'><i class='glyphicon glyphicon-backward'></i></a>";
                } else {

                    echo "<a href='#' class='btn btn-default disabled' data-toggle='tooltip' data-placement='bottom' title='Primeiro'><i class='glyphicon glyphicon-backward'></i></a>";
                }

                $stAnterior = $conn_dig->prepare("SELECT id
											    FROM documento_anexo
											   WHERE id < :id_anexo
												 AND id_documento = :id_documento
												 AND status_registro = :status_registro
										    ORDER BY id DESC
											   LIMIT 1");

                $stAnterior->execute(array("id_anexo" => $rsAnexo['id'], "id_documento" => $documento['id'], "status_registro" => "A"));
                $rsAnterior = $stAnterior->fetch();
                if (count($rsAnterior) > 1) {

                    echo "<a href='$CAMINHO/index.php?sessao=$_GET[sessao]&id_tipo=$id_tipo&id=$id&id_anexo=$rsAnterior[id]' class='btn btn-default' data-toggle='tooltip' data-placement='bottom' title='Anterior'><i class='glyphicon glyphicon-triangle-left'></i></a>";
                } else {

                    echo "<a href='#' class='btn btn-default disabled' data-toggle='tooltip' data-placement='bottom' title='Anterior'><i class='glyphicon glyphicon-triangle-left'></i></a>";
                }

                $stProximo = $conn_dig->prepare("SELECT id
											   FROM documento_anexo
											  WHERE id > :id_anexo
												AND id_documento = :id_documento
												AND status_registro = :status_registro
										   ORDER BY id ASC
											  LIMIT 1");

                $stProximo->execute(array("id_anexo" => $rsAnexo['id'], "id_documento" => $documento['id'], "status_registro" => "A"));
                $rsProximo = $stProximo->fetch();
                if (count($rsProximo) > 1) {

                    echo "<a href='$CAMINHO/index.php?sessao=$_GET[sessao]&id_tipo=$id_tipo&id=$id&id_anexo=$rsProximo[id]' class='btn btn-default' data-toggle='tooltip' data-placement='bottom' title='Pr&oacute;ximo'><i class='glyphicon glyphicon-triangle-right'></i></a>";
                } else {

                    echo "<a href='#' class='btn btn-default disabled' data-toggle='tooltip' data-placement='bottom' title='Pr&oacute;ximo'><i class='glyphicon glyphicon-triangle-right'></i></a>";
                }

                $stUltimo = $conn_dig->prepare("SELECT id
											  FROM documento_anexo
											 WHERE id_documento = :id_documento
											   AND status_registro = :status_registro
										  ORDER BY id DESC
											 LIMIT 1");

                $stUltimo->execute(array("id_documento" => $documento['id'], "status_registro" => "A"));
                $rsUltimo = $stUltimo->fetch();
                if (count($rsUltimo) > 1 && $rsUltimo['id'] != $rsAnexo['id']) {

                    echo "<a href='$CAMINHO/index.php?sessao=$_GET[sessao]&id_tipo=$id_tipo&id=$id&id_anexo=$rsUltimo[id]' class='btn btn-default' data-toggle='tooltip' data-placement='bottom' title='&Uacute;ltimo'><i class='glyphicon glyphicon-forward'></i></a>";
                } else {

                    echo "<a href='#' class='btn btn-default disabled' data-toggle='tooltip' data-placement='bottom' title='&Uacute;ltimo'><i class='glyphicon glyphicon-forward'></i></a>";
                }
                ?>
            </div>

            <a href="<?= $CAMINHO ?>/digitalizacao/download.php?id=<?= $rsAnexo["id"] ?>&cliente=<?=$cliente?>&servidor=<?= $servidor ?>&database=<?= $database ?>"
               class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Baixar o arquivo"
               target="_blank"><i class="glyphicon glyphicon-download-alt"></i></a>
        </div>

        <p class="clearfix">&nbsp;</p>

        <iframe
                class="frame"
                frameborder="0"
                scrolling="yes"
                marginheight="0"
                marginwidth="0"
                src="<?= $CAMINHO ?>/digitalizacao/pdf.php?id=<?= $rsAnexo["id"] ?>&servidor=<?= $servidor ?>&database=<?= $database ?>&val=<?=$cliente?>">
        </iframe>
        <?php
    }
} catch (PDOException $e) {

    echo "Erro ao conectar a $servidor: " . $e->getMessage();
}