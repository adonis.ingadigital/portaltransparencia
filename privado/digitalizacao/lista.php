<style>
    .sub-item.active, .sub-item.active:focus, .sub-item.active:hover {
        background-color: #468ecb !important;
        border-color: #468ecb !important;
        color: #fff !important;
    }

    .sub-sub-item.active, .sub-sub-item.active:focus, .sub-sub-item.active:hover {
        background-color: #65a0d4 !important;
        border-color: #65a0d4 !important;
        color: #fff !important;
    }

    .list-group-item.active {
        border-radius: 0 !important;
    }
</style>

<?php

    $stmt = $conn->prepare("SELECT * 
                              FROM cliente_digitalizacao 
                             WHERE id_cliente = :id_cliente 
                               AND status_registro = :status_registro 
                             LIMIT 1");
    $stmt->execute(array("id_cliente" => $cliente, "status_registro" => "A"));

    $row = $stmt->fetch();


    $servidor = $row['servidor'];
    $database = $row['database'];

    // $usuario = "root";
    // $senha = "1553bb7b4a8712761c334cb357de5fd2";

    $usuario = "root_externo";
    $senha = "1553bb7b4a8712761c334cb357de5fd2";


    if($cliente == '111'){
        $usuario = "root";
        $senha = "1ng@1nf11sql";
    } 

    if($cliente == '43'){
        $usuario = "leopolis";
        $senha = "1ng@1nf11sql";
    }


    $ids = explode(",", $_REQUEST['id_tipo']);
    $tipos = array();

    for ($i = 0; $i < count($ids); $i++) {
        $tipos[] = ':list' . $i;
        $vetorTipo[':list' . $i] = $ids[$i];
        $vetorCategoria[':list' . $i] = $ids[$i];
        $vetor[':list' . $i] = $ids[$i];
    }
    
    try {
        $conn_dig = new PDO("mysql:host=$servidor;dbname=$database;charset=utf8", $usuario, $senha);
        $conn_dig->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);




        $stmt = $conn_dig->prepare("SELECT * 
                                      FROM documento_tipo 
                                     WHERE id 
                                        IN (" . implode(',', $tipos) . ") 
                                       AND status_registro = :status_registro 
                                       AND acesso_externo = :acesso_externo 
                                  ORDER BY id DESC");
        $vetorTipo['status_registro'] = "A";
        $vetorTipo['acesso_externo'] = "S";
        $stmt->execute($vetorTipo);

        $tipo = $stmt->fetchAll();

        if (count($tipo) < 1) {
?>

<div class="jumbotron">
    <h1>P&aacute;gina n&atilde;o encontrada!</h1>
    <h3>A p&aacute;gina solicitada n&atilde;o foi localizada. Verifique a ortografia.</h3>
</div>

<?php
        } else {

            $count = 1;
            foreach ($tipo as $buscaTipo) {

                $titulo .= $buscaTipo['descricao'];
                if ($count != count($tipo)) 
                    $titulo .= " / ";
                $count++;
            }
?>

<h2><?= $count > 1 ? "Licita&ccedil;&otilde;es na &Iacute;ntegra" : $titulo ?></h2>
<ol class="breadcrumb">
    <li><a href="<?= $CAMINHO ?>">In&iacute;cio</a></li>
    <li class="active"><?= $count > 1 ? "Licita&ccedil;&otilde;es na &Iacute;ntegra" : $titulo ?></li>
</ol>

<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#form-diario" aria-expanded="false" aria-controls="form-diario">
    <i class="glyphicon glyphicon-search"></i> Pesquisar
</button>

<p class="clearfix"></p>

<form class="form-horizontal collapse row" id="form-diario"
    action="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia . $digitalizacaoLista . $complemento) ?>&id_tipo=<?= $_REQUEST['id_tipo'] ?>" method="post">
            
<?php 
            if (count($tipo) > 1) { 
?>

    <div class="form-group">
        <label for="id_tipo_digitalizacao" class="col-sm-4 col-md-2 control-label">Tipo</label>
        <div class="col-sm-8 col-md-10">
            <select name="id_tipo_busca" id="id_tipo_digitalizacao" class="form-control">
                <option value="">Selecione o Tipo</option>
                            
<?php
                foreach ($tipo as $buscaTipo) {
                    echo "<option value='$buscaTipo[id]'";
                    if ($_REQUEST['id_tipo_busca'] == $buscaTipo['id']) 
                        echo " selected";
                    echo ">$buscaTipo[descricao]</option>";
                }
?>

            </select>
        </div>
    </div>
            
<?php 
            } 
?>

    <div class="form-group">
        <label for="id_categoria_digitalizacao" class="col-sm-4 col-md-2 control-label">Categoria</label>
        <div class="col-sm-8 col-md-10">
            <p class="carregando_categoria form-control-static hidden">Aguarde, carregando...</p>
            <select name="id_categoria" id="id_categoria_digitalizacao" class="form-control">
                <option value="">Selecione a Categoria</option>
                        
<?php
            if (count($tipo) == 1 || $_REQUEST['id_tipo_busca'] != "") {

                $stCategoria = $conn_dig->prepare("SELECT * 
                                                     FROM documento_categoria 
                                                    WHERE id_tipo = :id_tipo 
                                                      AND status_registro = :status_registro 
                                                 ORDER BY descricao");
                if ($_REQUEST['id_tipo_busca'] != "") 
                    $stCategoria->execute(array("id_tipo" => secure($_REQUEST['id_tipo_busca']), "status_registro" => "A"));
                else 
                    $stCategoria->execute(array("id_tipo" => secure($_REQUEST['id_tipo']), "status_registro" => "A"));

                $qryCategoria = $stCategoria->fetchAll();

                if (count($qryCategoria)) {

                    foreach ($qryCategoria as $categoria) {
                        echo "<option value='$categoria[id]'";
                        if ($_REQUEST['id_categoria'] == $categoria['id']) 
                            echo " selected";
                        echo ">$categoria[descricao]</option>";
                    }
                }
            }
?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="id_subcategoria" class="col-sm-4 col-md-2 control-label">Subcategoria</label>
        <div class="col-sm-8 col-md-10">
            <p class="carregando_subcategoria form-control-static hidden">Aguarde, carregando...</p>
            <select name="id_subcategoria" id="id_subcategoria" class="form-control">
                <option value="">Selecione a Subcategoria</option>
                        
<?php
            if ($_REQUEST['id_categoria'] != "") {

                $stSubcategoria = $conn_dig->prepare("SELECT * 
                                                        FROM documento_subcategoria 
                                                       WHERE id_categoria = :id_categoria 
                                                         AND status_registro = :status_registro 
                                                    ORDER BY descricao");
                $stSubcategoria->execute(array("id_categoria" => $_REQUEST['id_categoria'], "status_registro" => "A"));
                $qrySubcategoria = $stSubcategoria->fetchAll();

                if (count($qrySubcategoria)) {

                    foreach ($qrySubcategoria as $subcategoria) {
                        echo "<option value='$subcategoria[id]'";
                        if ($_REQUEST['id_subcategoria'] == $subcategoria['id']) 
                            echo " selected";
                        echo ">$subcategoria[descricao]</option>";
                    }
                }
            }
?>

            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="palavra_chave" class="col-sm-4 col-md-2 control-label">Palavra-chave</label>
        <div class="col-sm-8 col-md-10">
            <input type="text" name="palavra_chave" id="palavra_chave" class="form-control" value="<?= $_POST['palavra_chave'] ?>"
                placeholder="Informe um trecho ou uma palavra constante no documento...">
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-4 col-md-offset-2 col-sm-8 col-md-10">
            <input type="hidden" name="id_cliente" id="id_cliente" value="<?= $cliente ?>">
            <button type="reset" class="btn btn-danger">Limpar</button>
            <button type="submit" class="btn btn-primary">Enviar</button>
        </div>
    </div>
</form>
<div class="row">
    <div class="col-md-3">
        <div class="list-group" id="main-menu">
                    
<?php
            $linkDigitalizacao = "$CAMINHO/index.php?sessao=" . verifica($sequencia . $digitalizacaoLista . $complemento);

            if($cliente != "111"){

                $sql = "SELECT documento.id_tipo,
                               documento_tipo.descricao,
                               COUNT(documento.id_tipo) AS quantidade
                          FROM documento
                          JOIN documento_tipo 
                            ON documento.id_tipo = documento_tipo.id
                         WHERE documento.id_tipo 
                            IN (
                        SELECT id
                          FROM documento_tipo AS sub_tipo 
                         WHERE sub_tipo.status_registro = :status_registro
                           AND sub_tipo.acesso_externo = :acesso_externo)
                           AND documento.status_registro = :status_registro";

                $stDocumentoTipo = $conn_dig->prepare($sql);
                $argsDocumentoTipo = array(
                    "ids" => implode(',', $ids),
                    "status_registro" => "A",
                    "acesso_externo" => "S"
                );

                $stDocumentoTipo->execute($argsDocumentoTipo);
                $documentoTipoFetch = $stDocumentoTipo->fetchAll();

                foreach ($documentoTipoFetch as $indiceDocumentoTipo => $tipoDocumento) {
                    $sqlCategoria = "SELECT documento.id_categoria,
                                            documento_categoria.descricao,
                                            COUNT(documento.id_categoria) AS quantidade
                                       FROM documento
                                       JOIN documento_categoria 
                                         ON documento.id_categoria  = documento_categoria.id
                                      WHERE documento.id_tipo = :id_tipo
                                        AND documento.status_registro = :status_registro
                                        AND documento_categoria.status_registro = :status_registro
                                   GROUP BY documento.id_categoria
                                   ORDER BY documento_categoria.descricao";
                    $argsCategoria = array(
                        "id_tipo" => $tipoDocumento['id_tipo'],
                        "status_registro" => "A"
                    );

                    $stDocumentoCategoria = $conn_dig->prepare($sqlCategoria);
                    $stDocumentoCategoria->execute($argsCategoria);
                    $documentoCategoriaFetch = $stDocumentoCategoria->fetchAll();
                    $totalDocumentoCategoria = count($documentoCategoriaFetch);

                    $isTipoSelecionado = (in_array($tipoDocumento['id_tipo'], $ids));
                    $linkTipo = "$linkDigitalizacao&id_tipo=$tipoDocumento[id_tipo]";
?>

            <a class="list-group-item <?= $isTipoSelecionado ? "active" : "" ?>" href="<?= $linkTipo ?>" data-parent="#main-menu">
                <span class="badge"><?= $tipoDocumento['quantidade'] ?></span>
                <?= $tipoDocumento['descricao'] ?>
            </a>

<? 
                    if ($totalDocumentoCategoria) { 
?>

            <div class="collapse <?= $isTipoSelecionado ? "in" : "" ?> list-group-level1" id="sub-menu<?= $indiceDocumentoTipo ?>">
                                
<?php 
                        foreach ($documentoCategoriaFetch as $indiceDocumentoCategoria => $documentoCategoria) {
                            $sqlSubCategoria = "SELECT documento.id_subcategoria,
                                                       documento_subcategoria.descricao,
                                                       COUNT(documento.id_subcategoria) AS quantidade
                                                  FROM documento
                                                  JOIN documento_subcategoria 
                                                    ON documento.id_subcategoria = documento_subcategoria.id
                                                 WHERE documento.id_categoria = :id_categoria
                                                   AND documento_subcategoria.id_categoria = :id_categoria
                                                   AND documento.status_registro = :status_registro
                                              GROUP BY documento.id_subcategoria
                                              ORDER BY documento.descricao";

                            $subcategoriaArgs = array(
                                "id_categoria" => $documentoCategoria['id_categoria'],
                                "status_registro" => "A"
                            );

                            $stSubCategoria = $conn_dig->prepare($sqlSubCategoria);
                            $stSubCategoria->execute($subcategoriaArgs);
                            $subCategoriaFetch = $stSubCategoria->fetchAll();
                            $totalSubCategoria = count($subCategoriaFetch);

                            $isCategoriaSelecionada = ($_REQUEST['id_categoria'] == $documentoCategoria['id_categoria']);
                            $linkCategoria = "$linkTipo&id_categoria=$documentoCategoria[id_categoria]";
?>

                <a class="list-group-item sub-item <?= $isCategoriaSelecionada ? "active" : "" ?>" href="<?= $linkCategoria ?>" data-parent="#sub-menu<?= $indiceDocumentoTipo ?>">
                    <span class="badge"><?= $documentoCategoria['quantidade'] ?></span>
                    &nbsp;
                    <span class="glyphicon glyphicon-menu-<?= $isCategoriaSelecionada ? "down" : "right" ?>"></span> <?= $documentoCategoria['descricao'] ?>
                </a>

<? 
                            if($totalSubCategoria) { 
?>

                <div class="collapse <?= $isCategoriaSelecionada ? "in" : "" ?> list-group-level2" id="sub-sub-menu<?= $indiceDocumentoCategoria ?>">
                                            
<?php 
                                foreach ($subCategoriaFetch as $indiceSubCategoria => $subCategoria) {
                                    $isSubCategoriaSelecionada = ($_REQUEST['id_subcategoria'] == $subCategoria['id_subcategoria']);
                                    $linkSubCategoria = "$linkCategoria&id_subcategoria=$subCategoria[id_subcategoria]"; 
?>

                    <a href="<?= $linkSubCategoria ?>" class="list-group-item sub-sub-item <?= $isSubCategoriaSelecionada ? "active" : "" ?> list-group-level2"
                        id="sub-sub-menu<?= $indiceSubCategoria ?>">
                        <span class="badge"><?= $subCategoria['quantidade'] ?></span>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <span class="glyphicon glyphicon-menu-right"></span> <?= $subCategoria['descricao'] ?>
                    </a>
                                            
<? 
                                } 
?>

                </div>
                                    
<? 
                            }
                        } 
?>

            </div>
                        
<? 
                    }
                }
            } else{
                
                $sql = "SELECT documento.id_tipo,
                               documento_tipo.descricao,
	                           COUNT(documento.id_tipo) AS quantidade
                          FROM documento 
                          JOIN documento_tipo 
                            ON documento.id_tipo = documento_tipo.id 
                         WHERE documento.status_registro = :status_registro 
                           AND documento_tipo.status_registro = :status_registro 
                           AND documento_tipo.acesso_externo = :acesso_externo
                      GROUP BY documento_tipo.descricao
                      ORDER BY documento_tipo.descricao ASC";

                $stDocumentoTipo = $conn_dig->prepare($sql);
                $stDocumentoTipo->execute(array(":status_registro" => "A", ":acesso_externo" => "S"));
                $documentoTipoFetch = $stDocumentoTipo->fetchAll();
                
                foreach($documentoTipoFetch as $tipoDocumento){
                    $isTipoSelecionado = $_GET['id_tipo'] == $tipoDocumento['id_tipo'];
                    $linkTipo = "$linkDigitalizacao&id_tipo=$tipoDocumento[id_tipo]";
?>

            <a class="list-group-item <?= $isTipoSelecionado ? "active" : "" ?>" href="<?= $linkTipo ?>" data-parent="#main-menu">
                <span class="badge"><?= $tipoDocumento['quantidade'] ?></span>
                <?= $tipoDocumento['descricao'] ?>
            </a>

<?
                }
            } 
?>

        </div>
    </div>
    <div class="col-md-9">
        <div class="table-responsive">
            <table class="table table-striped table-hover table-bordered table-condensed">
                <tr>
                    <th class="td_arquivo">&nbsp;</th>
                            
<?php 
            if (count($tipo) > 1) { 
?>

                    <th class="td_categoria">Tipo</th>
                    
<?php 
            } 
?>

                    <th class="td_categoria">Categoria</th>
                    <th class="td_subcategoria">Subcategoria</th>
                    <th>Indexa&ccedil;&atilde;o</th>
                </tr>
                        
<?php
            $pagina = isset($_REQUEST['pg']) ? $_REQUEST['pg'] : 1;
            $max = 20;
            $inicio = $max * ($pagina - 1);

            $link = $CAMINHO . "/index.php?sessao=" . $_GET['sessao'] . "&id_tipo=" . $_REQUEST['id_tipo'];

            $sql = "SELECT documento.id, 
                           documento.descricao, 
                           documento_tipo.descricao tipo, 
                           documento_categoria.descricao categoria, 
                           documento_subcategoria.descricao subcategoria
                      FROM documento
                 LEFT JOIN documento_tipo 
                        ON documento.id_tipo = documento_tipo.id
                 LEFT JOIN documento_categoria 
                        ON documento.id_categoria = documento_categoria.id
                 LEFT JOIN documento_subcategoria 
                        ON documento.id_subcategoria = documento_subcategoria.id
                     WHERE documento.id_tipo 
                        IN (" . implode(',', $tipos) . ")
                       AND documento_tipo.acesso_externo = :acesso_externo
                       AND documento.status_registro = :status_registro ";

            $vetor["acesso_externo"] = "S";
            $vetor["status_registro"] = "A";

            if ($_REQUEST['id_tipo_busca'] != "") {
                $sql .= "AND documento.id_tipo = :id_tipo_busca ";
                $vetor['id_tipo_busca'] = secure($_REQUEST['id_tipo_busca']);
                $link .= "&id_tipo_busca=" . $_REQUEST['id_tipo_busca'];
            }

            if ($_REQUEST['id_categoria'] != "") {
                $sql .= "AND documento.id_categoria = :id_categoria ";
                $vetor['id_categoria'] = secure($_REQUEST['id_categoria']);
                $link .= "&id_categoria=" . $_REQUEST['id_categoria'];
            }

            if ($_REQUEST['id_subcategoria'] != "") {
                $sql .= "AND documento.id_subcategoria = :id_subcategoria ";
                $vetor['id_subcategoria'] = secure($_REQUEST['id_subcategoria']);
                $link .= "&id_subcategoria=" . $_REQUEST['id_subcategoria'];
            }

            if ($_REQUEST['palavra_chave'] != "") {
                $sql .= "AND (documento.descricao 
                        LIKE :palavra_chave 
                          OR documento.id 
                          IN (
                      SELECT id_documento 
                        FROM documento_anexo 
                       WHERE status_registro = :status_registro 
                         AND MATCH (indexacao) AGAINST (:palavra_chave2 
                          IN BOOLEAN MODE))) ";
                $vetor['palavra_chave'] = "%" . secure(retira_acentos($_REQUEST['palavra_chave'])) . "%";
                $vetor['palavra_chave2'] = '"' . secure(retira_acentos($_REQUEST['palavra_chave'])) . '"';
                $link .= "&palavra_chave=" . $_REQUEST['palavra_chave'];
            }

            $stLinha = $conn_dig->prepare($sql);
            $stLinha->execute($vetor);
            $qryLinha = $stLinha->fetchAll();
            $totalLinha = count($qryLinha);

            if ($cliente == "111")
                $sql .= "ORDER BY documento.id DESC LIMIT $inicio, $max";
            else
                $sql .= "ORDER BY documento_categoria.descricao DESC, documento.id LIMIT $inicio, $max";

            $qryDocumento = $conn_dig->prepare($sql);

            $qryDocumento->execute($vetor);

            $documento = $qryDocumento->fetchAll();

            if (count($documento)) {
                foreach ($documento as $buscaDocumento) {
                    $stAnexo = $conn_dig->prepare("SELECT indexacao 
                                                     FROM documento_anexo 
                                                    WHERE id_documento = :id_documento 
                                                      AND status_registro = :status_registro 
                                                 ORDER BY id 
                                                    LIMIT 1");
                    $stAnexo->execute(array("id_documento" => $buscaDocumento['id'], "status_registro" => "A"));
                    $buscaAnexo = $stAnexo->fetch();

                    $indexacao = empty($buscaDocumento['descricao']) ? $buscaAnexo['indexacao'] : $buscaDocumento['descricao'] . " - " . $buscaAnexo['indexacao'];
?>

                <tr>
                    <td>
                        <a href="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia . $digitalizacaoVisualiza . $complemento) ?>&id_tipo=<?= $_REQUEST['id_tipo'] ?>&id=<?= $buscaDocumento['id'] ?>"
                            class="btn btn-primary"><i class="glyphicon glyphicon-search"></i></a></td>
                    
<?php 
                    if (count($tipo) > 1) { 
?>

                        <td><?= $buscaDocumento['tipo'] ?></td>
                        
<?php 
                    } 
?>

                        <td><?= $buscaDocumento['categoria'] ?></td>
                        <td><?= $buscaDocumento['subcategoria'] ?></td>
                        <td class="indexacao"><?= $indexacao ?></td>
                    </tr>
                                
<?php
                }

            }
?>

                </table>
            </div>
                
<?php
            $menos = $pagina - 1;
            $mais = $pagina + 1;
            $paginas = ceil($totalLinha / $max);
            if ($paginas > 1) {
?>

            <nav>
                <ul class="pagination">
                            
<?php 
                if ($pagina == 1) { 
?>

                    <li class="disabled">
                        <a href="#" aria-label="Primeiro" title="Primeiro" data-toggle="tooltip" data-placement="bottom">
                            <span aria-hidden="true"><i class="glyphicon glyphicon-backward"></i></span>
                        </a>
                    </li>
                    <li class="disabled">
                        <a href="#" aria-label="Anterior" title="Anterior" data-toggle="tooltip" data-placement="bottom">
                            <span aria-hidden="true">
                                <i class="glyphicon glyphicon-triangle-left"></i>
                            </span>
                        </a>
                    </li>
                            
<?php 
                } else { 
?>

                    <li>
                        <a href="<?= $link ?>&pg=1" aria-label="Primeiro" title="Primeiro" data-toggle="tooltip" data-placement="bottom">
                            <span aria-hidden="true">
                                <i class="glyphicon glyphicon-backward"></i>
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $link ?>&pg=<?= $menos ?>" aria-label="Anterior" title="Anterior" data-toggle="tooltip" data-placement="bottom">
                            <span aria-hidden="true">
                                <i class="glyphicon glyphicon-triangle-left"></i>
                            </span>
                        </a>
                    </li>
                                
<?php
                }

                if (($pagina - 4) < 1) 
                    $anterior = 1;
                else 
                    $anterior = $pagina - 4;

                if (($pagina + 4) > $paginas) 
                    $posterior = $paginas;
                else 
                    $posterior = $pagina + 4;

                for ($i = $anterior; $i <= $posterior; $i++) {
                    if ($i != $pagina) {
?>

                    <li><a href="<?= $link ?>&pg=<?= $i ?>"><?= $i ?></a></li>
                                
<?php 
                    } else { 
?>

                    <li class="active"><a href="#"><?= $i ?><span class="sr-only">(current)</span></a></li>
                                    
<?php
                    }
                }

                if ($mais <= $paginas) {
?>
                    
                    <li>   
                        <a href="<?= $link ?>&pg=<?= $mais ?>" aria-label="Pr&oacute;ximo" title="Pr&oacute;ximo" data-toggle="tooltip" data-placement="bottom">
                            <span aria-hidden="true">
                                <i class="glyphicon glyphicon-triangle-right"></i>
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $link ?>&pg=<?= $paginas ?>" aria-label="&Uacute;ltimo" title="&Uacute;ltimo" data-toggle="tooltip" data-placement="bottom">
                            <span aria-hidden="true">
                                <i class="glyphicon glyphicon-forward"></i>
                            </span>
                        </a>
                    </li>
                            
<?php 
                } else { 
?>

                    <li class="disabled">
                        <a href="#" aria-label="Pr&oacute;ximo" title="Pr&oacute;ximo" data-toggle="tooltip" data-placement="bottom">
                            <span aria-hidden="true">
                                <i class="glyphicon glyphicon-triangle-right"></i>
                            </span>
                        </a>
                    </li>
                    <li class="disabled">
                        <a href="#" aria-label="&Uacute;ltimo" title="&Uacute;ltimo" data-toggle="tooltip" data-placement="bottom">
                            <span aria-hidden="true">
                                <i class="glyphicon glyphicon-forward"></i>
                            </span>
                        </a>
                    </li>
                            
<?php 
                } 
?>

                </ul>
            </nav>
                    
<?php
            } 
?>

        </div>
    </div>
    
<?php 
        }

        $conn_dig = null;

    } catch (PDOException $e) {
        echo "Erro ao conectar a $servidor: " . $e->getMessage();
    }