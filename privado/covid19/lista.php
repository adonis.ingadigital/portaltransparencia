
<h2>Covid-19</h2>
<ol class="breadcrumb">
	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
	<li class="active">Covid-19</li>
</ol>

<?php
$stCategoria = $conn->prepare("SELECT * FROM covid_19_transparencia_categoria WHERE id_cliente = :id_cliente AND status_registro = :status_registro ORDER BY id DESC");
$stCategoria->execute(array("id_cliente" => $cliente, "status_registro" => "A"));
$qryCategoria = $stCategoria->fetchAll();


if(count($qryCategoria)) {
?>
<ul class="treeview">
	<?php
	foreach ($qryCategoria as $categoria) {
	?>
	<li><a href="#"><?= $categoria['descricao'] ?></a>
		<?php
		$stCovid = $conn->prepare("SELECT * FROM covid_19_transparencia WHERE id_categoria = :id_categoria AND (id_subcategoria IS NULL OR id_subcategoria = '') AND status_registro = :status_registro ORDER BY titulo DESC");
		$stCovid->execute(array("id_categoria" => $categoria['id'], "status_registro" => "A"));
		$qryCovid = $stCovid->fetchAll();

		if(count($qryCovid)) {
		?>
		<ul>
			<li>
				<div class="panel-group" id="accordion_<?= $categoria['id'] ?>" role="tablist" aria-multiselectable="true">
					<?php foreach ($qryCovid as $covid19) { ?>
					<div class="panel panel-default">
					    <div class="panel-heading" role="tab" id="heading_<?= $covid19['id'] ?>">
					     	<h4 class="panel-title">
					        	<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?= $covid19['id'] ?>" aria-expanded="true" aria-controls="collapse_<?= $covid19['id'] ?>">
					          		<i class="glyphicon glyphicon-triangle-right"></i> <?= $covid19['titulo']?>
					        	</a>
					    	</h4>
					    </div>
						<div id="collapse_<?= $covid19['id'] ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_<?= $covid19['id'] ?>">
							<div class="panel-body">
								<!-- 
								<?php if($covid19['data_inicio'] != "") { ?><strong>Data de In&iacute;cio:</strong> <?= formata_data($covid19['data_inicio']) ?><br><?php } ?>
								<?php if($covid19['data_fim'] != "") { ?><strong>Fim da Vig&ecirc;ncia:</strong> <?= formata_data($covid19['data_fim']) ?><br><?php } ?>
								<?php if($covid19['numero'] != "") { ?><strong>N&ordm; do Processo:</strong> <?= $covid19['numero'] ?><br><?php } ?>
								<?php if($covid19['valor'] != "") { ?><strong>Valor:</strong> <?= $covid19['valor'] ?><br><?php } ?>
								<?php if($covid19['contrapartida'] != "") { ?><strong>Contra partida:</strong> <?= $covid19['contrapartida'] ?><br><?php } ?>
								<?php if($covid19['proponente'] != "") { ?><strong>Proponente:</strong> <?= $covid19['proponente'] ?><br><?php } ?>
								<?php if($covid19['fornecedor'] != "") { ?><strong>Fornecedor:</strong> <?= $covid19['fornecedor'] ?><br><?php } ?>
								<?php if($covid19['objeto'] != "") { ?><strong>Objeto:</strong> <?= verifica($covid19['objeto']) ?><br><?php } ?> 
								-->

								<?php
								$stAnexo = $conn->prepare("SELECT * FROM covid_19_transparencia_anexo WHERE id_artigo = :id_artigo ORDER BY ordem ASC, id DESC");
								$stAnexo->execute(array("id_artigo" => $covid19['id']));
								$qryAnexo = $stAnexo->fetchAll();

								if(count($qryAnexo)) {
								?>
								<div class="panel panel-primary">
									<div class="panel-heading">
								    	<h2 class="panel-title"><i class="glyphicon glyphicon-cloud-download"></i> Anexos</h2>
								  	</div>
									<div class="panel-body">
									<?php
									foreach ($qryAnexo as $anexo) {
									?>
										<p><strong><a href="<?= $CAMINHOARQ ?>/<?= $anexo['arquivo'] ?>" target="_blank"><i class="glyphicon glyphicon-cloud-download"></i> <?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?></a></strong></p>
									<?php } ?>
									</div>
								</div>
								<?php } ?>

								<?php
								$stLink = $conn->prepare("SELECT * FROM covid_19_transparencia_link WHERE id_artigo = :id_artigo ORDER BY ordem ASC, id DESC");
								$stLink->execute(array("id_artigo" => $covid19['id']));
								$qryLink = $stLink->fetchAll();

								if(count($qryLink)) {
								?>
								<div class="panel panel-primary">
									<div class="panel-heading">
								    	<h2 class="panel-title"><i class="glyphicon glyphicon-globe"></i> Links</h2>
								  	</div>
									<div class="panel-body">
									<?php
									foreach ($qryLink as $linkcovid19) {
									?>
										<p><strong><a href="<?= $linkcovid19['link'] ?>" target="_blank"><i class="glyphicon glyphicon-globe"></i> <?= empty($linkcovid19['descricao']) ? "Link" : $linkcovid19['descricao'] ?></a></strong></p>
									<?php } ?>
									</div>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>
					<?php } ?>
				</div>
			</li>
		</ul>
		<?php } ?>
		<?php
		$stSubcategoria = $conn->prepare("SELECT * FROM covid_19_transparencia_subcategoria WHERE id_categoria = :id_categoria AND status_registro = :status_registro ORDER BY id");
		$stSubcategoria->execute(array("id_categoria" => $categoria['id'], "status_registro" => "A"));
		$qrySubcategoria = $stSubcategoria->fetchAll();

		if(count($qrySubcategoria)) {
		?>
		<ul>
			<?php
			foreach ($qrySubcategoria as $subcategoria) {
			?>
			<li><a href="#"><?= $subcategoria['descricao'] ?></a>
				<?php
				$stCovid = $conn->prepare("SELECT * FROM covid_19_transparencia WHERE id_subcategoria = :id_subcategoria AND status_registro = :status_registro ORDER BY titulo DESC");
				$stCovid->execute(array("id_subcategoria" => $subcategoria['id'], "status_registro" => "A"));
				$qryCovid = $stCovid->fetchAll();
				

				if(count($qryCovid)) {
				?>
				<ul>
					<li>
						<div class="panel-group" id="accordion_<?= $subcategoria['id'] ?>" role="tablist" aria-multiselectable="true">
							<?php foreach ($qryCovid as $covid19) { ?>
							<div class="panel panel-default">
							    <div class="panel-heading" role="tab" id="heading_<?= $covid19['id'] ?>">
							     	<h4 class="panel-title">
							        	<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?= $covid19['id'] ?>" aria-expanded="true" aria-controls="collapse_<?= $covid19['id'] ?>">
							          		<i class="glyphicon glyphicon-triangle-right"></i> <?= $covid19['titulo'] ?>
							        	</a>
							    	</h4>
							    </div>
								<div id="collapse_<?= $covid19['id'] ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_<?= $covid19['id'] ?>">
									<div class="panel-body">
										<?php  if($covid19['artigo'] != "") { ?><strong>Artigo:</strong> <?= verifica($covid19['artigo']) ?><br><?php } ?>
										<?php  if($covid19['objeto_adquirido'] != "") { ?><strong>Objeto Adquirido:</strong> <?= verifica($covid19['objeto_adquirido']) ?><br><?php } ?>
										<?php  if($covid19['objeto_adquirido_descricao'] != "") { ?><strong>Descrição do Objeto:</strong> <?= verifica($covid19['objeto_adquirido_descricao']) ?><br><?php } ?>
										<?php  if($covid19['numero_processo'] != "") { ?><strong>Número do Processo:</strong> <?= verifica($covid19['numero_processo']) ?><br><?php } ?>
										<?php  if($covid19['numero_contrato'] != "") { ?><strong>Número do Contrato:</strong> <?= verifica($covid19['numero_contrato']) ?><br><?php } ?>
										<?php  if($covid19['razao_social_favorecido'] != "") { ?><strong>Razão Social do Favorecido:</strong> <?= verifica($covid19['razao_social_favorecido']) ?><br><?php } ?>
										<?php  if($covid19['cpf_cnpj_favorecido'] != "") { ?><strong>CNPJ/CPF do Favorecido:</strong> <?= verifica($covid19['cpf_cnpj_favorecido']) ?><br><?php } ?>
										<?php  if($covid19['numero_empenho'] != "") { ?><strong>Número do Empenho:</strong> <?= verifica($covid19['numero_empenho']) ?><br><?php } ?>
										<?php  if($covid19['data_empenho'] != "") { ?><strong>Data do Empenho:</strong> <?= verifica($covid19['data_empenho']) ?><br><?php } ?>
										<?php  if($covid19['valor_empenho'] != "") { ?><strong>Valor do Empenho:</strong> <?= verifica($covid19['valor_empenho']) ?><br><?php } ?>
										<?php  if($covid19['quantidade_item'] != "") { ?><strong>Quantidade de Item:</strong> <?= verifica($covid19['quantidade_item']) ?><br><?php } ?>
										<?php  if($covid19['valor_unitario'] != "") { ?><strong>Valor Unitário:</strong> <?= verifica($covid19['valor_unitario']) ?><br><?php } ?>
										<?php
										$stAnexo = $conn->prepare("SELECT * FROM covid_19_transparencia_anexo WHERE id_artigo = :id_artigo ORDER BY ordem ASC, id DESC");
										$stAnexo->execute(array("id_artigo" => $covid19['id']));
										$qryAnexo = $stAnexo->fetchAll();

										if(count($qryAnexo)) {
										?>
										<div class="panel panel-primary">
											<div class="panel-heading">
										    	<h2 class="panel-title"><i class="glyphicon glyphicon-cloud-download"></i> Anexos</h2>
										  	</div>
											<div class="panel-body">
											<?php
											foreach ($qryAnexo as $anexo) {
											?>
												<p><strong><a href="<?= $CAMINHOARQ ?>/<?= $anexo['arquivo'] ?>" target="_blank"><i class="glyphicon glyphicon-cloud-download"></i> <?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?></a></strong></p>
											<?php } ?>
											</div>
										</div>
										<?php } ?>

										<?php
										$stLink = $conn->prepare("SELECT * FROM covid_19_transparencia_link WHERE id_artigo = :id_artigo ORDER BY ordem ASC, id DESC");
										$stLink->execute(array("id_artigo" => $covid19['id']));
										$qryLink = $stLink->fetchAll();

										if(count($qryLink)) {
										?>
										<div class="panel panel-primary">
											<div class="panel-heading">
										    	<h2 class="panel-title"><i class="glyphicon glyphicon-globe"></i> Links</h2>
										  	</div>
											<div class="panel-body">
											<?php
											foreach ($qryLink as $linkcovid19) {
											?>
												<p><strong><a href="<?= $linkcovid19['link'] ?>" target="_blank"><i class="glyphicon glyphicon-globe"></i> <?= empty($linkcovid19['descricao']) ? "Link" : $linkcovid19['descricao'] ?></a></strong></p>
											<?php } ?>
											</div>
										</div>
										<?php } ?>
									</div>
								</div>
							</div>
							<?php } ?>
						</div>
					</li>
				</ul>
				<?php } ?>
			</li>
			<?php } ?>
		</ul>
		<?php } ?>
	</li>
	<?php } ?>
</ul>
<?php
}

	$atualizacao = atualizacao("legislacao", $cliente, $conn);

	if($atualizacao != "") {
?>

<p class="text-right"><small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small></p>

<?php
	}
?>