<h2>Perguntas Frequentes</h2>
<ol class="breadcrumb">
	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
	<li class="active">Perguntas Frequentes</li>
</ol>

<?php
$stPergunta = $conn->prepare("SELECT * FROM pergunta_frequente WHERE id_cliente = :id_cliente AND status_registro = :status_registro ORDER BY titulo");
$stPergunta->execute(array("id_cliente" => $cliente, "status_registro" => "A"));
$qryPergunta = $stPergunta->fetchAll();

if(count($qryPergunta)) {
?>
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
	<?php foreach ($qryPergunta as $pergunta) { ?>
	<div class="panel panel-default">
	    <div class="panel-heading" role="tab" id="heading_<?= $pergunta['id'] ?>">
	     	<h4 class="panel-title">
	        	<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?= $pergunta['id'] ?>" aria-expanded="true" aria-controls="collapse_<?= $pergunta['id'] ?>">
	          	<i class="glyphicon glyphicon-triangle-right"></i> <?= verifica($pergunta['titulo']) ?>
	        	</a>
	    	</h4>
	    </div>
		<div id="collapse_<?= $pergunta['id'] ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_<?= $pergunta['id'] ?>">
			<div class="panel-body">
				<?= verifica($pergunta['artigo']) ?>
			</div>
		</div>
	</div>
	<?php } ?>
</div>
<?php
}

$atualizacao = atualizacao("pergunta_frequente", $cliente, $conn);
if($atualizacao != "") {
?>
<p class="text-right"><small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small></p>
<?php
}