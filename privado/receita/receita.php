<h2>Receitas</h2>
<ol class="breadcrumb">
	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
	<li class="active">Receitas</li>
</ol>

<?php
$stCategoria = $conn->prepare("SELECT * FROM receita_categoria WHERE id_cliente = :id_cliente AND status_registro = :status_registro ORDER BY id DESC");
$stCategoria->execute(array("id_cliente" => $cliente, "status_registro" => "A"));
$qryCategoria = $stCategoria->fetchAll();

if(count($qryCategoria)) {
?>
<ul class="treeview">
	<?php
	foreach ($qryCategoria as $categoria) {
	?>
	<li><a href="#"><?= $categoria['descricao'] ?></a>
		<?php
		$stArquivo = $conn->prepare("SELECT * FROM receita WHERE id_categoria = :id_categoria AND status_registro = :status_registro ORDER BY titulo DESC");
		$stArquivo->execute(array("id_categoria" => $categoria['id'], "status_registro" => "A"));
		$qryArquivo = $stArquivo->fetchAll();

		if(count($qryArquivo)) {
		?>
		<ul>
			<?php
			foreach($qryArquivo as $arquivo) {

				if($arquivo['arquivo'] != "") {

					$linkDocumento = $CAMINHOARQ . "/" . $arquivo['arquivo'];
					$imagem = "glyphicon-cloud-download";
					$titulo = empty($arquivo['titulo']) ? "Anexo" : $arquivo['titulo'];

				} else if($arquivo['link'] != "") {

					$linkDocumento = $arquivo['link'];
					$imagem = "glyphicon-globe";
					$titulo = empty($arquivo['titulo']) ? "Link" : $arquivo['titulo'];
				}
			?>
			<li><a href="<?=$linkDocumento ?>" target="_blank"><i class="glyphicon <?= $imagem ?>"></i> <?= $titulo ?></a></li>
			<?php } ?>
		</ul>
		<?php } ?>
	</li>
	<?php } ?>
</ul>
<?php
}

$atualizacao = atualizacao("receita", $cliente, $conn);
if($atualizacao != "") {
?>
<p class="text-right"><small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small></p>
<?php
}