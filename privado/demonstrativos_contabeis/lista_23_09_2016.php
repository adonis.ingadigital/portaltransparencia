<?php
$id = secure($_GET['id']);

$stSessao = $conn->prepare("SELECT * FROM demonstrativo_contabil_sessao WHERE id = :id");
$stSessao->execute(array("id" => $id));
$sessao = $stSessao->fetch();

$tituloTela = "Demonstrativos Cont&aacute;beis";
if($cliente == "1093" || $cliente == "105") $tituloTela = "Portal da Transpar&ecirc;ncia";
?>
<h2><?= $tituloTela ?> <span class="label label-default"><?= $sessao["descricao"] ?></span></h2>
<ol class="breadcrumb">
	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
	<li><a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$gastossessao.$complemento); ?>">&Oacute;rg&atilde;os</a></li>
	<li class="active"><?= $tituloTela ?></li>
</ol>

<?php if($cliente == "1162") { ?>
<p class="text-center"><a href="http://www.olivatti.com.br/contas/iracema/contas.htm" target="_blank"><img src="http://iracemadooeste.pr.gov.br/imagens/gastos.jpg" class="imagem"></a></p>
<?php
}

$stAno = $conn->prepare("SELECT * FROM demonstrativo_contabil_ano WHERE id_cliente = :id_cliente AND id_sessao = :id AND status_registro = :status_registro GROUP BY descricao DESC");
$stAno->execute(array("id_cliente" => $cliente, "id" => $id, "status_registro" => "A"));
$qryAno = $stAno->fetchAll();

if(empty($configuracaoTransparencia['ordem_demostrativo_contabil'])) $ordem = "ORDER BY id";
else $ordem = "ORDER BY $configuracaoTransparencia[ordem_demostrativo_contabil]";

if(count($qryAno)) {
?>
<div>
	<ul class="nav nav-pills" role="tablist">
	<?php foreach ($qryAno as $indice => $ano) { ?>
		<li role="presentation"<?php if($indice == 0) echo ' class="active"'; ?>><a href="<?= "#" . $ano['descricao'] ?>" aria-controls="<?= $ano['descricao'] ?>" role="tab" data-toggle="tab"><strong><?= $ano['descricao'] ?></strong></a></li>
	<?php } ?>
	</ul>

	<div class="tab-content">
	<?php foreach ($qryAno as $indice => $ano) { ?>
		<div role="tabpanel" class="tab-pane<?php if($indice == 0) echo " active"; ?>" id="<?= $ano['descricao'] ?>">
			<p>&nbsp;</p>
			<?php
			$stCategoria = $conn->prepare("SELECT * FROM demonstrativo_contabil_categoria WHERE id_cliente = :id_cliente AND id_ano = :id_ano AND status_registro = :status_registro ORDER BY descricao");
			$stCategoria->execute(array("id_cliente" => $cliente, "id_ano" => $ano['id'], "status_registro" => "A"));
			$qryCategoria = $stCategoria->fetchAll();

			if(count($qryCategoria)) {
			?>
			<ul class="treeview">
				<?php
				foreach ($qryCategoria as $categoria) {
				?>
				<li><a href="#"><?= $categoria['descricao'] ?></a>
					<?php
					$stArquivo = $conn->prepare("SELECT * FROM demonstrativo_contabil_arquivo WHERE id_categoria = :id_categoria AND (id_subcategoria IS NULL OR id_subcategoria = '') AND status_registro = :status_registro $ordem");
					$stArquivo->execute(array("id_categoria" => $categoria['id'], "status_registro" => "A"));
					$qryArquivo = $stArquivo->fetchAll();

					if(count($qryArquivo)) {
					?>
					<ul>
						<?php
						foreach($qryArquivo as $arquivo) {

							if($arquivo['arquivo'] != "") {

								$linkDocumento = $CAMINHOARQ . "/" . $arquivo['arquivo'];
								$imagem = "glyphicon-cloud-download";

							} else if($arquivo['link'] != "") {

								$linkDocumento = $arquivo['link'];
								$imagem = "glyphicon-globe";
							}
						?>
						<li><a href="<?= $linkDocumento ?>" target="_blank"><i class="glyphicon <?= $imagem ?>"></i> <?= $arquivo['descricao'] ?></a></li>
						<?php } ?>
					</ul>
					<?php } ?>
					<?php
					$stSubcategoria = $conn->prepare("SELECT * FROM demonstrativo_contabil_subcategoria WHERE id_categoria = :id_categoria AND status_registro = :status_registro ORDER BY id");
					$stSubcategoria->execute(array("id_categoria" => $categoria['id'], "status_registro" => "A"));
					$qrySubcategoria = $stSubcategoria->fetchAll();

					if(count($qrySubcategoria)) {
					?>
					<ul>
						<?php
						foreach ($qrySubcategoria as $subcategoria) {
						?>
						<li><a href="#"><?= $subcategoria['descricao'] ?></a>
							<?php
							$stArquivo = $conn->prepare("SELECT * FROM demonstrativo_contabil_arquivo WHERE id_subcategoria = :id_subcategoria AND status_registro = :status_registro $ordem");
							$stArquivo->execute(array("id_subcategoria" => $subcategoria['id'], "status_registro" => "A"));
							$qryArquivo = $stArquivo->fetchAll();

							if(count($qryArquivo)) {
							?>
							<ul>
								<?php
								foreach($qryArquivo as $arquivo) {

									if($arquivo['arquivo'] != "") {

										$linkDocumento = $CAMINHOARQ . "/" . $arquivo['arquivo'];
										$imagem = "glyphicon-cloud-download";

									} else if($arquivo['link'] != "") {

										$linkDocumento = $arquivo['link'];
										$imagem = "glyphicon-globe";
									}
								?>
								<li><a href="<?= $linkDocumento ?>" target="_blank"><i class="glyphicon <?= $imagem ?>"></i> <?= $arquivo['descricao'] ?></a></li>
								<?php } ?>
							</ul>
							<?php } ?>
						</li>
						<?php } ?>
					</ul>
					<?php } ?>
				</li>
				<?php } ?>

				<?php if($cliente == "11928" && $id == "111330") { ?>
				<li><a href="https://www.convenios.gov.br/siconv/proposta/ConsultarProposta/ConsultarProposta.do" target="_blank"><i class="glyphicon glyphicon-globe"></i> <span class="text-danger">CONV&Ecirc;NIO FEDERAL</span> - RELA&Ccedil;&Atilde;O DE CONV&Ecirc;NIOS FIRMADO ENTRE O MUNIC&Iacute;PIO DE P&Eacute;ROLA E O GOVERNO FEDERAL - ACESSE O PORTAL DE CONV&Ecirc;NIOS SICONV DO GOVERNO FEDERAL</a></li>
				<?php } ?>

				<?php if($cliente == "109" && $ano['id'] != "1111466") { ?>
				<li><a href="http://acessa.me/cjjb" target="_blank"><i class="glyphicon glyphicon-globe"></i> PRESTA&Ccedil;&Atilde;O DE CONTAS</a></li>
				<?php } ?>

				<?php if($cliente == "27" && $id == "111323") { ?>
				<li><a href="https://www.planalto.gov.br/ccivil_03/_ato2011-2014/2011/lei/l12527.htm" target="_blank"><i class="glyphicon glyphicon-globe"></i> Lei 12.527</a></li>
				<li><a href="http://www.atende.net/transparencia/portal.php?cliente=3144" target="_blank"><i class="glyphicon glyphicon-globe"></i> Portal da Transpar&ecirc;ncia</a></li>
				<?php } ?>

			</ul>
			<?php } ?>
		</div>
	<?php } ?>
	</div>
</div>
<?php
}

$atualizacao = atualizacao("demonstrativo_contabeis", $cliente, $conn);
if($atualizacao != "") {
?>
<p class="text-right"><small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small></p>
<?php
}