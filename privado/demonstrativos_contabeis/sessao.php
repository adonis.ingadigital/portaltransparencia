<?php $novoCliente = empty($_GET['nc']) ? $cliente : secure($_GET['nc']); ?>
<h2><?= $novoCliente == "1093" || $novoCliente == "105" ? "Portal da Transpar&ecirc;ncia" : "Demonstrativos Cont&aacute;beis <span class='label label-default'>&Oacute;rg&atilde;os</span>" ?></h2>
<ol class="breadcrumb">
	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
	<li class="active">&Oacute;rg&atilde;os</li>
</ol>

<?php if($novoCliente != "12066") { ?>
<p class="text-justify">A Lei Complementar Federal n&ordm; 101, de 04/05/2000 - Lei de Responsabilidade
      Fiscal - LRF, em seu artigo 48, apresenta o seguinte texto: "S&atilde;o
      instrumentos de transpar&ecirc;ncia da gest&atilde;o fiscal, aos quais
      ser&aacute;
      dada ampla divulga&ccedil;&atilde;o, inclusive em meios eletr&ocirc;nicos
      de acesso p&uacute;blico: os planos; or&ccedil;amentos e leis de diretrizes
      or&ccedil;ament&aacute;rias; as presta&ccedil;&otilde;es de contas e o
      respectivo parecer pr&eacute;vio; o Relat&oacute;rio Resumido da Execu&ccedil;&atilde;o
      Or&ccedil;ament&aacute;ria e o Relat&oacute;rio de Gest&atilde;o Fiscal;
      e as vers&otilde;es simplificadas desses documentos".</p>
<?php } ?>

<?php if($novoCliente == "113") { ?>
<p><a href="http://www.portaltransparencia.gov.br/convenios/ConveniosLista.asp?UF=pr&Estado=parana&CodMunicipio=9947&Municipio=GODOY+MOREIRA&CodOrgao=&Orgao=&TipoConsulta=0&Periodo=" target="_blank"><img src="<?=$CAMINHO ?>/images/transp.jpg"></a></p>
<?php
}

$stSessao = $conn->prepare("SELECT * FROM demonstrativo_contabil_sessao WHERE id_cliente = :id_cliente AND status_registro = :status_registro");
$stSessao->execute(array("id_cliente" => $novoCliente, "status_registro" => "A"));
$qrySessao = $stSessao->fetchAll();

if(count($qrySessao)) {
?>
<table class="table table-hover table-striped">
	<tr>
		<td class="text-center"><strong><?php if($novoCliente != "11994"){ ?><strong>ESCOLHA UM &Oacute;RG&Atilde;O  </strong><?php } else { ?><strong>ESCOLHA UMA OP&Ccedil;&Atilde;O </strong><?php } ?></strong></td>
	</tr>
	<?php foreach ($qrySessao as $sessao) { ?>
	<tr>
		<td>
			<a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$transparenciaAno.$complemento); ?>&id=<?= verifica($sessao['id']) ?>&nc=<?= $novoCliente ?>"><i class="glyphicon glyphicon-folder-open"></i><strong>&nbsp;&nbsp;<?= $sessao['descricao'] ?></strong></a>
		</td>
	</tr>
	<?php } ?>
</table>
<?php } ?>

<?php if($configuracaoTransparencia['utilizar_portal'] != "S") { ?>
<ul class="treeview">
<?php
$stCategoria = $conn->prepare("SELECT * FROM link_transparencia_categoria WHERE id_cliente = :id_cliente AND status_registro = :status_registro ORDER BY id DESC");
$stCategoria->execute(array("id_cliente" => $novoCliente, "status_registro" => "A"));
$qryCategoria = $stCategoria->fetchAll();

if(count($qryCategoria)) {

	foreach ($qryCategoria as $categoria) {
?>
	<li><a href="#"><?= $categoria['descricao'] ?></a>
		<?php
		$stLink = $conn->prepare("SELECT * FROM link_transparencia WHERE id_categoria = :id_categoria AND status_registro = :status_registro ORDER BY descricao DESC");
		$stLink->execute(array("id_categoria" => $categoria['id'], "status_registro" => "A"));
		$qryLink = $stLink->fetchAll();

		if(count($stLink)) {
		?>
		<ul>
			<?php
			foreach($qryLink as $link) {
			?>
			<li><a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$transparenciaOnlineVisualiza.$complemento); ?>&id=<?= verifica($link['id']); ?>&nc=<?= $novoCliente ?>&redir=link"><i class="glyphicon glyphicon-globe"></i> <?= $link['descricao'] ?></a></li>
			<?php } ?>
		</ul>
		<?php } ?>
	</li>
	<?php } ?>
<?php } ?>
<?php
$stLink = $conn->prepare("SELECT * FROM link_transparencia WHERE id_cliente = :id_cliente AND (id_categoria IS NULL OR id_categoria = '') AND status_registro = :status_registro ORDER BY descricao DESC");
$stLink->execute(array("id_cliente" => $novoCliente, "status_registro" => "A"));
$qryLink = $stLink->fetchAll();

if(count($stLink)) {

	foreach($qryLink as $link) {
	?>
	<li><a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$transparenciaOnlineVisualiza.$complemento); ?>&id=<?= verifica($link['id']); ?>&nc=<?= $novoCliente ?>&redir=link"><i class="glyphicon glyphicon-globe"></i> <?= $link['descricao'] ?></a></li>
	<?php } ?>
<?php } ?>
</ul>
<?php } ?>
<?php if($novoCliente == "16") { ?>
<p><a href="http://www.equiplano.com.br/equiplano/action/PublicAccountsAction?method=loadPublicAccounts&id=97" target="_blank"><img src="<?=$CAMINHOIMG ?>/contas_equiplano.png"></a></p>
<?php
}

$atualizacao = atualizacao("demonstrativo_contabeis", $novoCliente, $conn);
if($atualizacao != "") {
?>
<p class="text-right"><small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small></p>
<?php
}