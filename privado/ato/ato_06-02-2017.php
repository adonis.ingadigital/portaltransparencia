<h2>Atos Normativos</h2>
<ol class="breadcrumb">
	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
	<li class="active">Atos Normativos</li>
</ol>

<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#form-atos" aria-expanded="false" aria-controls="form-atos">
	<i class="glyphicon glyphicon-search"></i> Pesquisar
</button>
<br>
<br>

<!-- BUSCA -->
<?php
if ($_REQUEST['id_tipo'])
    $sql_id_tipo = "AND id = " . $_REQUEST['id_tipo'];
else
    $sql_id_tipo = "";

if ($_REQUEST['ano'])
    $sql_ano = "AND year(data_sessao) = " . $_REQUEST['ano'];
else
    $sql_ano = "";

if ($_REQUEST['autor'])
    $sql_autor = "AND autor LIKE '%" . $_REQUEST['autor']."%'";
else
    $sql_autor = "";

if ($_REQUEST['descricao'])
	$descricao = "AND descricao LIKE '%". $_REQUEST['descricao'] . "%'";
else
	$descricao = "";
?>
<form class="form-horizontal collapse" id="form-atos" action="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$atoLista.$complemento); ?>&nc=<?= $novoCliente ?><?= $action ?>" method="post">
	<div class="form-group">
		<label for="id_tipo" class="col-sm-2 control-label">Tipo</label>
		<div class="col-sm-10">
			<select name="id_tipo" id="id_tipo" class="form-control">
				<option value="">Selecione o Tipo da Lei</option>
				<?php
				$stAtoTipo = $conn->prepare("SELECT * FROM camara_tipo_ato WHERE id_cliente = :id_cliente AND status_registro = :status_registro ORDER BY id");
				$stAtoTipo->execute(array("id_cliente" => $cliente, "status_registro" => "A"));
				$qryAtoTipo = $stAtoTipo->fetchAll();

				if(count($qryAtoTipo)) {
					foreach ($qryAtoTipo as $tipoAto) {

						echo "<option value='$tipoAto[id]' ";
						if($tipoAto['id'] == $_REQUEST['id_tipo']) echo "selected";
						echo ">$tipoAto[tipo]</option>";

					}
				}
				?>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label for="ano" class="col-sm-2 control-label">Ano</label>
		<div class="col-sm-10">
			<select name="ano" id="ano" class="form-control">
				<option value="">Selecione o Ano da Lei</option>
				<?php
				$stAtoAno = $conn->prepare("SELECT DISTINCT year(data_sessao) AS ano
													   FROM camara_ato
													  WHERE id_cliente = :id_cliente
													    AND status_registro = :status_registro
													    AND length(year(data_sessao)) > 3");
				$stAtoAno->execute(array("id_cliente" => $cliente, "status_registro" => "A"));
				$qryAtoAno = $stAtoAno->fetchAll();

				if(count($qryAtoAno)) {
					foreach ($qryAtoAno as $ano) {

						echo "<option value='$ano[ano]' ";
						if($ano['ano'] == $_REQUEST['ano']) echo "selected";
						echo ">$ano[ano]</option>";

					}
				}
				?>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label for="ano" class="col-sm-2 control-label">Autor</label>
		<div class="col-sm-10">
			<select name="autor" id="autor" class="form-control">
				<option value="">Selecione o Autor</option>
				<?php
				$stAtoAutor = $conn->prepare("SELECT DISTINCT autor
														 FROM camara_ato
														 WHERE id_cliente = :id_cliente
														 AND status_registro = :status_registro
														 AND autor IS NOT NULL
														 AND autor != '' ");

				$stAtoAutor->execute(array("id_cliente" => $cliente, "status_registro" => "A"));
				$qryAtoAutor= $stAtoAutor->fetchAll();

				if(count($qryAtoAutor)) {
					foreach ($qryAtoAutor as $autor) {

						echo "<option value='$autor[autor]' ";
						if($autor['autor'] == $_REQUEST['autor']) echo "selected";
						echo ">$autor[autor]</option>";

					}
				}
				?>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label for="descricao" class="col-sm-2 control-label">Descri&ccedil;&atilde;o</label>
		<div class="col-sm-10">
			<input type="text" name="descricao" id="descricao" class="form-control" value="<?= $_POST['descricao'] ?>" placeholder="Descri&ccedil;&atilde;o...">
		</div>
	</div>

	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<input type="hidden" name="acao" value="cadastrar">
			<button type="reset" class="btn btn-danger">Limpar</button>
			<button type="submit" class="btn btn-primary">Enviar</button>
		</div>
	</div>
</form>
<!-- /BUSCA -->

<?php
$sqlTipoAto = "SELECT * FROM camara_tipo_ato WHERE id_cliente = :id_cliente AND status_registro = :status_registro " . $sql_id_tipo . " ORDER BY id";
$stTipoAto = $conn->prepare($sqlTipoAto);
$stTipoAto->execute(array("id_cliente" => $cliente, "status_registro" => "A"));
$qryTipoAto = $stTipoAto->fetchAll();

if(count($qryTipoAto)) {
?>
<ul class="treeview">
	<?php
	foreach ($qryTipoAto as $tipoAto) {
	?>
	<li>
		<a href="#"><?= $tipoAto['tipo'] ?></a>
		<?php
        $sql = "SELECT * FROM camara_ato WHERE id_tipo_ato = :id_tipo_ato " ;
        $sql .= $sql_autor . $sql_ano . $descricao . " AND status_registro = :status_registro ORDER BY data_sessao DESC";
//		echo $sql;
		$stArquivo = $conn->prepare($sql);
		$stArquivo->execute(array("id_tipo_ato" => $tipoAto['id'], "status_registro" => "A"));
		$qryArquivo = $stArquivo->fetchAll();

		if(count($qryArquivo)) {
		?>
		<ul>
			<?php
			foreach($qryArquivo as $arquivo) {
			?>
			<li><a href="<?=$CAMINHOARQ ?>/<?= $arquivo['arquivo'] ?>" target="_blank"><i class="glyphicon glyphicon-cloud-download"></i> <?= formata_data($arquivo['data_sessao']) ?> - <?= $arquivo['descricao'] ?><?php if($arquivo['autor'] != "") { ?> (Autor: <?= $arquivo['autor'] ?>)<?php } ?></a></li>
			<?php } ?>
		</ul>
		<?php } ?>
	</li>
	<?php } ?>
</ul>
<?php
}

$atualizacao = atualizacao("poder_legislativo/ato", $cliente, $conn);
if($atualizacao != "") {
?>
<p class="text-right"><small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small></p>
<?php
}