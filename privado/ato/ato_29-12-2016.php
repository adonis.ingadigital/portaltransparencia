<h2>Atos Oficiais</h2>
<ol class="breadcrumb">
	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
	<li class="active">Atos Oficiais</li>
</ol>

<?php
$stCategoria = $conn->prepare("SELECT * FROM camara_tipo_ato WHERE id_cliente = :id_cliente AND status_registro = :status_registro ORDER BY id");
$stCategoria->execute(array("id_cliente" => $cliente, "status_registro" => "A"));
$qryCategoria = $stCategoria->fetchAll();

if(count($qryCategoria)) {
?>
<ul class="treeview">
	<?php
	foreach ($qryCategoria as $categoria) {
	?>
	<li><a href="#"><?= $categoria['tipo'] ?></a>
		<?php
		$stArquivo = $conn->prepare("SELECT * FROM camara_ato WHERE id_tipo_ato = :id_tipo_ato AND status_registro = :status_registro ORDER BY data_sessao DESC");
		$stArquivo->execute(array("id_tipo_ato" => $categoria['id'], "status_registro" => "A"));
		$qryArquivo = $stArquivo->fetchAll();

		if(count($qryArquivo)) {
		?>
		<ul>
			<?php
			foreach($qryArquivo as $arquivo) {
			?>
			<li><a href="<?=$CAMINHOARQ ?>/<?= $arquivo['arquivo'] ?>" target="_blank"><i class="glyphicon glyphicon-cloud-download"></i> <?= formata_data($arquivo['data_sessao']) ?> - <?= $arquivo['descricao'] ?><?php if($arquivo['autor'] != "") { ?> (Autor: <?= $arquivo['autor'] ?>)<?php } ?></a></li>
			<?php } ?>
		</ul>
		<?php } ?>
	</li>
	<?php } ?>
</ul>
<?php
}

$atualizacao = atualizacao("poder_legislativo/ato", $cliente, $conn);
if($atualizacao != "") {
?>
<p class="text-right"><small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small></p>
<?php
}