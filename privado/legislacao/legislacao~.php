<?php
switch($_GET['tela']) {

	case "atos": $titulo = "Atos Normativos"; break;
	case "ppa":  $titulo = "PPA"; break;
	case "ldo":  $titulo = "LDO"; break;
	case "loa":  $titulo = "LOA"; break;
	default: 	 $titulo = "Legisla&ccedil;&atilde;o"; break;
}
?>
<h2><?= $titulo ?></h2>
<ol class="breadcrumb">
	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
	<li class="active"><?= $titulo ?></li>
</ol>

<?php
$novoCliente = empty($_GET['nc']) ? $cliente : secure($_GET['nc']);

$sqlTipo = "SELECT * FROM lei_tipo WHERE id IN (SELECT id_tipo FROM lei WHERE id_cliente = :id_cliente AND status_registro = :status_registro) ";
$sqlAno = "SELECT DISTINCT(ano) ano FROM lei WHERE id_cliente = :id_cliente AND status_registro = :status_registro AND ano <> '' ";

$vetorTipo['id_cliente'] = $novoCliente;
$vetorTipo['status_registro'] = "A";

if($_REQUEST['tipo'] != "") {

	$ids = explode(",", $_REQUEST['tipo']);
	$tipos = array();

	for($i=0; $i < count($ids); $i++) {

		$tipos[] = ':list' . $i;
		$vetorTipo[':list' . $i] = $ids[$i];
	}

	$sqlTipo .= "AND id IN (".implode(',', $tipos).") ";
	$sqlAno .= "AND id_tipo IN (".implode(',', $tipos).") ";

}

if($_REQUEST['ntipo'] != "") {

	$ids = explode(",", $_REQUEST['ntipo']);
	$tipos = array();

	for($i=0; $i < count($ids); $i++) {

		$tipos[] = ':nlist' . $i;
		$vetorTipo[':nlist' . $i] = $ids[$i];
	}

	$sqlTipo .= "AND id NOT IN (".implode(',', $tipos).") ";
	$sqlAno .= "AND id_tipo NOT IN (".implode(',', $tipos).") ";

}

$sqlTipo .= "ORDER BY descricao";
$sqlAno .= "ORDER BY ano DESC";

$stLeiTipo = $conn->prepare($sqlTipo);
$stLeiTipo->execute($vetorTipo);
$qryLeiTipo = $stLeiTipo->fetchAll();

$stAnoLei = $conn->prepare($sqlAno);
$stAnoLei->execute($vetorTipo);
$qryAnoLei = $stAnoLei->fetchAll();

$action = "";
if($_GET['tela'] != "") $action .= "&tela=" . $_GET['tela'];
if($_GET['tipo'] != "") $action .= "&tipo=" . $_GET['tipo'];
if($_GET['ntipo'] != "") $action .= "&ntipo=" . $_GET['ntipo'];

if($novoCliente == "46") {
?>
<p class="text-center"><a href="http://www.ubirata.pr.gov.br/leis/busca_rapida.php" target="_blank"><img src="http://ubirata.pr.gov.br/images/lei_digital.jpg" alt="LEGISLA&Ccedil;&Atilde;O DIGITAL" class="imagem" /></a></p>
<?php } ?>

<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#form-legislacao" aria-expanded="false" aria-controls="form-legislacao">
	<i class="glyphicon glyphicon-search"></i> Pesquisar
</button>

<?php
if ($novoCliente == '1152') {
	?>
	<a class="btn-success btn" href="http://diariooficialdoparana.com.br/site/publicacao/86" target="_blank">
		<i class="glyphicon glyphicon-folder-open"></i> &nbsp; Legisla&ccedil;&acirc;o e Atos Normativos Publicados pelo Poder Executivo
	</a>
	<?
}
?>

<?php if(!empty($configuracaoTransparencia['caminho_digitalizacao_legislacao']) && empty($_GET['tela'])) { ?>
<a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$digitalizacaoLista.$complemento); ?>&id_tipo=<?= $configuracaoTransparencia['caminho_digitalizacao_legislacao'] ?>" class="btn btn-primary"><i class="glyphicon glyphicon-cd"></i> Legisla&ccedil;&atilde;o Digital</a>
<?php } else if(!empty($configuracaoTransparencia['caminho_digitalizacao_atos']) && $_GET['tela'] == "atos") { ?>
<a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$digitalizacaoLista.$complemento); ?>&id_tipo=<?= $configuracaoTransparencia['caminho_digitalizacao_atos'] ?>" class="btn btn-primary"><i class="glyphicon glyphicon-cd"></i> Atos Normativos Digital</a>
<?php } ?>
<p class="clearfix"></p>

<form class="form-horizontal collapse" id="form-legislacao" action="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$legislacaoLista.$complemento); ?>&nc=<?= $novoCliente ?><?= $action ?>" method="post">
	<div class="form-group">
		<label for="id_tipo" class="col-sm-2 control-label">Tipo</label>
		<div class="col-sm-10">
			<select name="id_tipo" id="id_tipo" class="form-control">
				<option value="">Selecione o Tipo da Lei</option>
				<?php
				if(count($qryLeiTipo)) {

					foreach ($qryLeiTipo as $leiTipo) {

						echo "<option value='$leiTipo[id]' ";
						if($leiTipo['id'] == $_REQUEST['id_tipo']) echo "selected";
						echo ">$leiTipo[descricao]</option>";
					}
				}
				?>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label for="ano" class="col-sm-2 control-label">Ano</label>
		<div class="col-sm-10">
			<select name="ano" id="ano" class="form-control">
				<option value="">Selecione o Ano da Lei</option>
				<?php
				if(count($qryAnoLei)) {

					foreach ($qryAnoLei as $anoLei) {

						echo "<option value='$anoLei[ano]' ";
						if($anoLei['ano'] == $_REQUEST['ano']) echo "selected";
						echo ">$anoLei[ano]</option>";
					}
				}
				?>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label for="numero" class="col-sm-2 control-label">N&uacute;mero</label>
		<div class="col-sm-10">
			<input type="text" name="numero" id="numero" class="form-control" value="<?= $_POST['numero'] ?>" placeholder="Informe o n&uacute;mero da lei...">
		</div>
	</div>
	<div class="form-group">
		<label for="projeto" class="col-sm-2 control-label">Projeto</label>
		<div class="col-sm-10">
			<input type="text" name="projeto" id="projeto" class="form-control" value="<?= $_POST['projeto'] ?>" placeholder="Informe o projeto da lei...">
		</div>
	</div>
	<div class="form-group">
		<label for="sumula" class="col-sm-2 control-label">S&uacute;mula</label>
		<div class="col-sm-10">
			<input type="text" name="sumula" id="sumula" class="form-control" value="<?= $_POST['sumula'] ?>" placeholder="Informe um trecho da s&uacute;mula da lei...">
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<input type="hidden" name="acao" value="cadastrar">
			<button type="reset" class="btn btn-danger">Limpar</button>
			<button type="submit" class="btn btn-primary">Enviar</button>
		</div>
	</div>
</form>

<?php
$pagina = isset($_REQUEST['pagina']) ? $_REQUEST['pagina'] : 1;
$max = 20;
$inicio = $max * ($pagina - 1);

$link = $CAMINHO . "/index.php?sessao=" . $_GET['sessao'] . "&nc=" . $novoCliente;

if($_GET['tela'] != "") $link .= "&tela=" . $_GET['tela'];

$sql = "SELECT lei.*, lei_tipo.descricao tipo
		  FROM lei, lei_tipo
		 WHERE lei.id_tipo = lei_tipo.id
		   AND lei.id_cliente = :id_cliente
		   AND lei.status_registro = :status_registro ";

$vetor["id_cliente"] = $novoCliente;
$vetor["status_registro"] = "A";

if($_REQUEST['tipo'] != "") {

	$ids = explode(",", $_REQUEST['tipo']);
	$tipos = array();

	for($i=0; $i < count($ids); $i++) {

	    $tipos[] = ':list' . $i;
	    $vetor[':list' . $i] = $ids[$i];
	}

	$sql .= "AND lei.id_tipo IN (".implode(',', $tipos).") ";
	$link .= "&tipo=" . $_REQUEST['tipo'];

}

if($_REQUEST['ntipo'] != "") {

	$ids = explode(",", $_REQUEST['ntipo']);
	$tipos = array();

	for($i=0; $i < count($ids); $i++) {

	    $tipos[] = ':nlist' . $i;
	    $vetor[':nlist' . $i] = $ids[$i];
	}

	$sql .= "AND lei.id_tipo NOT IN (".implode(',', $tipos).") ";
	$link .= "&ntipo=" . $_REQUEST['ntipo'];

}

if($_REQUEST['id_tipo'] != "") {

	$sql .= "AND lei.id_tipo = :id_tipo ";
	$vetor['id_tipo'] = secure($_REQUEST['id_tipo']);
	$link .= "&id_tipo=" . $_REQUEST['id_tipo'];

}

if($_REQUEST['ano'] != "") {

	$sql .= "AND lei.ano = :ano ";
	$vetor["ano"] = secure($_REQUEST['ano']);
	$link .= "&ano=" . $_REQUEST['ano'];

}

if($_REQUEST['numero'] != "") {

	$sql .= "AND lei.numero LIKE :numero ";
	$vetor["numero"] = "%" . secure($_REQUEST['numero']) . "%";
	$link .= "&numero=" . $_REQUEST['numero'];
}

if($_REQUEST['projeto'] != "") {

	$sql .= "AND lei.projeto LIKE :projeto ";
	$vetor["projeto"] = "%" . secure($_REQUEST['projeto']) . "%";
	$link .= "&projeto=" . $_REQUEST['projeto'];
}

if($_REQUEST['sumula'] != "") {

	$sql .= "AND lei.sumula LIKE :sumula ";
	$vetor["sumula"] = "%" . secure($_REQUEST['sumula']) . "%";
	$link .= "&sumula=" . $_REQUEST['sumula'];
}

$stLinha = $conn->prepare($sql);
$stLinha->execute($vetor);
$qryLinha = $stLinha->fetchAll();
$totalLinha = count($qryLinha);

if($novoCliente == '12056') {

	if(empty($configuracaoTransparencia['ordem_legislacao'])) $ordem = "ORDER BY lei.id_tipo = '21' DESC, lei.ano DESC, CAST(lei.numero as SIGNED) DESC";
	else $ordem = "ORDER BY lei.id_tipo = '21' DESC, $configuracaoTransparencia[ordem_legislacao]";

} else {

	if(empty($configuracaoTransparencia['ordem_legislacao'])) $ordem = "ORDER BY lei.ano DESC, CAST(lei.numero as SIGNED) DESC";
	else $ordem = "ORDER BY $configuracaoTransparencia[ordem_legislacao]";

}

$sql .= "$ordem LIMIT $inicio, $max";

$stLei = $conn->prepare($sql);
$stLei->execute($vetor);
$qryLei = $stLei->fetchAll();

if(count($qryLei)) {
?>
<p class="clearfix"></p>
<div class="row">

	<?php if(count($qryLeiTipo) > 1) { ?>
	<div class="col-md-3">
		<div class="list-group">
			<?php
			foreach ($qryLeiTipo as $leiTipo) {

				$stTotal = $conn->prepare("SELECT COUNT(id) AS total
											 FROM lei
											WHERE id_cliente = :id_cliente
											  AND id_tipo = :id_tipo
											  AND status_registro = :status_registro");
				$stTotal->execute(array("id_cliente" => $novoCliente, "id_tipo" => $leiTipo['id'], "status_registro" => "A"));
				$totalLeiTipo = $stTotal->fetch();
			?>
			<a class="list-group-item <?php if($_REQUEST['id_tipo'] == $leiTipo['id']) echo "active"; ?>" href="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$legislacaoLista.$complemento); ?>&nc=<?= $novoCliente ?>&id_tipo=<?= $leiTipo['id'] ?><?= $action ?>">
				<span class="badge"><?= $totalLeiTipo['total'] ?></span>
				<?= $leiTipo['descricao'] ?>
			</a>
			<?php } ?>
		</div>
	</div>
	<?php } ?>

	<div class="<?php if(count($qryLeiTipo) > 1) echo "col-md-9"; else echo "col-md-12"; ?>">
		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
			<?php foreach ($qryLei as $lei) { ?>
			<div class="panel panel-default">
			    <div class="panel-heading" role="tab" id="heading_<?= $lei['id'] ?>">
			     	<h4 class="panel-title">
			        	<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?= $lei['id'] ?>" aria-expanded="true" aria-controls="collapse_<?= $lei['id'] ?>">
			          		<div class="row">
			          			<div class="col-sm-7"><i class="glyphicon glyphicon-triangle-right"></i> <strong class="text-danger"> <?= $lei['tipo'] ?></strong></div>
			          			<div class="col-sm-5"><strong>N&ordm;</strong> <?= $lei['numero']?>/<?= $lei['ano']?><?php if($lei['projeto'] != "") { ?> - <strong>Projeto:</strong> <?= $lei['projeto'] ?><? } ?></div>
			          		</div>
			          		<div class="row">
			          			<div class="col-sm-12 texto_resumido"><strong>S&uacute;mula:</strong> <?= strip_tags($lei['sumula']) ?></div>
			          		</div>
			        	</a>
			    	</h4>
			    </div>
				<div id="collapse_<?= $lei['id'] ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_<?= $lei['id'] ?>">
					<div class="panel-body">
						<p><strong>S&uacute;mula:</strong> <?= verifica($lei['sumula']) ?></p>
						<?php if($lei['observacao'] != "") { ?>
						<p><strong>Observa&ccedil;&otilde;es:</strong> <?= verifica($lei['observacao']) ?></p>
						<?php } ?>

						<?php
						$stAnexo = $conn->prepare("SELECT * FROM lei_anexo WHERE id_artigo = :id_artigo ORDER BY ordem ASC, id DESC");
						$stAnexo->execute(array("id_artigo" => $lei['id']));
						$qryAnexo = $stAnexo->fetchAll();

						if(count($qryAnexo)) {
						?>
						<div class="panel panel-primary">
							<div class="panel-heading">
						    	<h2 class="panel-title"><i class="glyphicon glyphicon-cloud-download"></i> <?= $novoCliente == "1093" && ($_GET['tela'] == "ldo" || $_GET['tela'] == "loa" || $_GET['tela'] == "ppa") ? "Lei" : "Anexos" ?></h2>
						  	</div>
							<div class="panel-body">
							<?php
							foreach ($qryAnexo as $anexo) {
							?>
								<p><strong><a href="<?= str_replace($cliente, $novoCliente, $CAMINHOARQ) ?>/<?= $anexo['arquivo'] ?>" target="_blank"><i class="glyphicon glyphicon-cloud-download"></i> <?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?></a></strong></p>
							<?php } ?>
							</div>
						</div>
						<?php } ?>

						<?php
						$stLink = $conn->prepare("SELECT * FROM lei_link WHERE id_artigo = :id_artigo ORDER BY ordem ASC, id DESC");
						$stLink->execute(array("id_artigo" => $lei['id']));
						$qryLink = $stLink->fetchAll();

						if(count($qryLink)) {
						?>
						<div class="panel panel-primary">
							<div class="panel-heading">
						    	<h2 class="panel-title"><i class="glyphicon glyphicon-globe"></i> <?= $novoCliente == "1093" && ($_GET['tela'] == "ldo" || $_GET['tela'] == "loa" || $_GET['tela'] == "ppa") ? "Anexos" : "Links" ?></h2>
						  	</div>
							<div class="panel-body">
							<?php
							foreach ($qryLink as $linkLei) {
							?>
								<p><strong><a href="<?= $linkLei['link'] ?>" target="_blank"><i class="glyphicon glyphicon-globe"></i> <?= empty($linkLei['descricao']) ? "Link" : $linkLei['descricao'] ?></a></strong></p>
							<?php } ?>
							</div>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
</div>

<?php
$menos = $pagina - 1;
$mais = $pagina + 1;
$paginas = ceil($totalLinha / $max);
if($paginas > 1) {
?>
<nav>
	<ul class="pagination">
		<?php if($pagina == 1) { ?>
	    <li class="disabled"><a href="#" aria-label="Anterior"><span aria-hidden="true">&laquo;</span></a></li>
	    <?php } else { ?>
	    <li><a href="<?= $link ?>&pagina=<?= $menos ?>" aria-label="Anterior"><span aria-hidden="true">&laquo;</span></a></li>
	    <?php
		}

	    if(($pagina - 4) < 1) $anterior = 1;
	    else $anterior = $pagina - 4;

	    if(($pagina + 4) > $paginas) $posterior = $paginas;
	    else $posterior = $pagina + 4;

	    for($i = $anterior; $i <= $posterior; $i++) {

	    	if($i != $pagina) {
	    ?>
	    	<li><a href="<?= $link ?>&pagina=<?= $i ?>"><?= $i ?></a></li>
	    	<?php } else { ?>
	    	<li class="active"><a href="#"><?= $i ?><span class="sr-only">(current)</span></a></li>
	   	<?php
	    	}
	    }

	    if($mais <= $paginas) {
	   	?>
	   	<li><a href="<?= $link ?>&pagina=<?= $mais ?>" aria-label="Pr&oacute;ximo"><span aria-hidden="true">&raquo;</span></a></li>
	   	<?php } ?>
	</ul>
</nav>
<?php } ?>

<?php } else { ?>
<h4>Nenhum registro encontrado.</h4>
<?php
}

$atualizacao = atualizacao("legislacao", $cliente, $conn);
if($atualizacao != "") {
?>
<p class="text-right"><small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small></p>
<?php
}