<h2>Bolsa Atl&eacute;ta</h2>
<ol class="breadcrumb">
	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
	<li class="active">Bolsa Atl&eacute;ta</li>
</ol>

<?php
$stCategoria = $conn->prepare("SELECT * FROM bolsa_atleta_categoria WHERE id_cliente = :id_cliente AND status_registro = :status_registro ORDER BY id DESC");
$stCategoria->execute(array("id_cliente" => $cliente, "status_registro" => "A"));
$qryCategoria = $stCategoria->fetchAll();

if(count($qryCategoria)) {
?>
<ul class="treeview">
	<?php
	foreach ($qryCategoria as $categoria) {
	?>
	<li><a href="#"><?= $categoria['descricao'] ?></a>
		<?php
		$stBolsaAtleta = $conn->prepare("SELECT * FROM bolsa_atleta WHERE id_categoria = :id_categoria AND (id_subcategoria IS NULL OR id_subcategoria = '') AND status_registro = :status_registro ORDER BY titulo DESC");
		$stBolsaAtleta->execute(array("id_categoria" => $categoria['id'], "status_registro" => "A"));
		$qryBolsaAtleta = $stBolsaAtleta->fetchAll();

		if(count($qryBolsaAtleta)) {
		?>
		<ul>
			<li>
				<div class="panel-group" id="accordion_<?= $categoria['id'] ?>" role="tablist" aria-multiselectable="true">
					<?php foreach ($qryBolsaAtleta as $bolsaAtleta) { ?>
					<div class="panel panel-default">
					    <div class="panel-heading" role="tab" id="heading_<?= $bolsaAtleta['id'] ?>">
					     	<h4 class="panel-title">
					        	<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?= $bolsaAtleta['id'] ?>" aria-expanded="true" aria-controls="collapse_<?= $bolsaAtleta['id'] ?>">
					          		<i class="glyphicon glyphicon-triangle-right"></i> <?= $bolsaAtleta['titulo']?>
					        	</a>
					    	</h4>
					    </div>
						<div id="collapse_<?= $bolsaAtleta['id'] ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_<?= $bolsaAtleta['id'] ?>">
							<div class="panel-body">
								<?php if($bolsaAtleta['data_inicio'] != "") { ?><strong>Data de In&iacute;cio:</strong> <?= formata_data($bolsaAtleta['data_inicio']) ?><br><?php } ?>
								<?php if($bolsaAtleta['data_fim'] != "") { ?><strong>Fim da Vig&ecirc;ncia:</strong> <?= formata_data($bolsaAtleta['data_fim']) ?><br><?php } ?>
								<?php if($bolsaAtleta['numero'] != "") { ?><strong>N&ordm; do Processo:</strong> <?= $bolsaAtleta['numero'] ?><br><?php } ?>
								<?php if($bolsaAtleta['valor'] != "") { ?><strong>Valor:</strong> <?= $bolsaAtleta['valor'] ?><br><?php } ?>
								<?php if($bolsaAtleta['contrapartida'] != "") { ?><strong>Contra partida:</strong> <?= $bolsaAtleta['contrapartida'] ?><br><?php } ?>
								<?php if($bolsaAtleta['proponente'] != "") { ?><strong>Proponente:</strong> <?= $bolsaAtleta['proponente'] ?><br><?php } ?>
								<?php if($bolsaAtleta['fornecedor'] != "") { ?><strong>Fornecedor:</strong> <?= $bolsaAtleta['fornecedor'] ?><br><?php } ?>
								<?php if($bolsaAtleta['objeto'] != "") { ?><strong>Objeto:</strong> <?= verifica($bolsaAtleta['objeto']) ?><br><?php } ?>

								<?php
								$stAnexo = $conn->prepare("SELECT * FROM bolsa_atleta_anexo WHERE id_artigo = :id_artigo ORDER BY ordem ASC, id DESC");
								$stAnexo->execute(array("id_artigo" => $bolsaAtleta['id']));
								$qryAnexo = $stAnexo->fetchAll();

								if(count($qryAnexo)) {
								?>
								<div class="panel panel-primary">
									<div class="panel-heading">
								    	<h2 class="panel-title"><i class="glyphicon glyphicon-cloud-download"></i> Anexos</h2>
								  	</div>
									<div class="panel-body">
									<?php
									foreach ($qryAnexo as $anexo) {
									?>
										<p><strong><a href="<?= $CAMINHOARQ ?>/<?= $anexo['arquivo'] ?>" target="_blank"><i class="glyphicon glyphicon-cloud-download"></i> <?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?></a></strong></p>
									<?php } ?>
									</div>
								</div>
								<?php } ?>

								<?php
								$stLink = $conn->prepare("SELECT * FROM bolsa_atleta_link WHERE id_artigo = :id_artigo ORDER BY ordem ASC, id DESC");
								$stLink->execute(array("id_artigo" => $bolsaAtleta['id']));
								$qryLink = $stLink->fetchAll();

								if(count($qryLink)) {
								?>
								<div class="panel panel-primary">
									<div class="panel-heading">
								    	<h2 class="panel-title"><i class="glyphicon glyphicon-globe"></i> Links</h2>
								  	</div>
									<div class="panel-body">
									<?php
									foreach ($qryLink as $linkBolsaAtleta) {
									?>
										<p><strong><a href="<?= $linkBolsaAtleta['link'] ?>" target="_blank"><i class="glyphicon glyphicon-globe"></i> <?= empty($linkBolsaAtleta['descricao']) ? "Link" : $linkBolsaAtleta['descricao'] ?></a></strong></p>
									<?php } ?>
									</div>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>
					<?php } ?>
				</div>
			</li>
		</ul>
		<?php } ?>
		<?php
		$stSubcategoria = $conn->prepare("SELECT * FROM bolsa_atleta_subcategoria WHERE id_categoria = :id_categoria AND status_registro = :status_registro ORDER BY id DESC");
		$stSubcategoria->execute(array("id_categoria" => $categoria['id'], "status_registro" => "A"));
		$qrySubcategoria = $stSubcategoria->fetchAll();

		if(count($qrySubcategoria)) {
		?>
		<ul>
			<?php
			foreach ($qrySubcategoria as $subcategoria) {
			?>
			<li><a href="#"><?= $subcategoria['descricao'] ?></a>
				<?php
				$stBolsaAtleta = $conn->prepare("SELECT * FROM bolsa_atleta WHERE id_subcategoria = :id_subcategoria AND status_registro = :status_registro ORDER BY titulo DESC");
				$stBolsaAtleta->execute(array("id_subcategoria" => $subcategoria['id'], "status_registro" => "A"));
				$qryBolsaAtleta = $stBolsaAtleta->fetchAll();

				if(count($qryBolsaAtleta)) {
				?>
				<ul>
					<li>
						<div class="panel-group" id="accordion_<?= $subcategoria['id'] ?>" role="tablist" aria-multiselectable="true">
							<?php foreach ($qryBolsaAtleta as $bolsaAtleta) { ?>
							<div class="panel panel-default">
							    <div class="panel-heading" role="tab" id="heading_<?= $bolsaAtleta['id'] ?>">
							     	<h4 class="panel-title">
							        	<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?= $bolsaAtleta['id'] ?>" aria-expanded="true" aria-controls="collapse_<?= $bolsaAtleta['id'] ?>">
							          		<i class="glyphicon glyphicon-triangle-right"></i> <?= $bolsaAtleta['titulo'] ?>
							        	</a>
							    	</h4>
							    </div>
								<div id="collapse_<?= $bolsaAtleta['id'] ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_<?= $bolsaAtleta['id'] ?>">
									<div class="panel-body">
										<?php if($bolsaAtleta['data_inicio'] != "") { ?><strong>Data de In&iacute;cio:</strong> <?= formata_data($bolsaAtleta['data_inicio']) ?><br><?php } ?>
										<?php if($bolsaAtleta['data_fim'] != "") { ?><strong>Fim da Vig&ecirc;ncia:</strong> <?= formata_data($bolsaAtleta['data_fim']) ?><br><?php } ?>
										<?php if($bolsaAtleta['numero'] != "") { ?><strong>N&ordm; do Processo:</strong> <?= $bolsaAtleta['numero'] ?><br><?php } ?>
										<?php if($bolsaAtleta['valor'] != "") { ?><strong>Valor:</strong> <?= $bolsaAtleta['valor'] ?><br><?php } ?>
										<?php if($bolsaAtleta['contrapartida'] != "") { ?><strong>Contra partida:</strong> <?= $bolsaAtleta['contrapartida'] ?><br><?php } ?>
										<?php if($bolsaAtleta['proponente'] != "") { ?><strong>Proponente:</strong> <?= $bolsaAtleta['proponente'] ?><br><?php } ?>
										<?php if($bolsaAtleta['fornecedor'] != "") { ?><strong>Fornecedor:</strong> <?= $bolsaAtleta['fornecedor'] ?><br><?php } ?>
										<?php if($bolsaAtleta['objeto'] != "") { ?><strong>Objeto:</strong> <?= $bolsaAtleta['objeto'] ?><br><?php } ?>

										<?php
										$stAnexo = $conn->prepare("SELECT * FROM bolsa_atleta_anexo WHERE id_artigo = :id_artigo ORDER BY ordem ASC, id DESC");
										$stAnexo->execute(array("id_artigo" => $bolsaAtleta['id']));
										$qryAnexo = $stAnexo->fetchAll();

										if(count($qryAnexo)) {
										?>
										<div class="panel panel-primary">
											<div class="panel-heading">
										    	<h2 class="panel-title"><i class="glyphicon glyphicon-cloud-download"></i> Anexos</h2>
										  	</div>
											<div class="panel-body">
											<?php
											foreach ($qryAnexo as $anexo) {
											?>
												<p><strong><a href="<?= $CAMINHOARQ ?>/<?= $anexo['arquivo'] ?>" target="_blank"><i class="glyphicon glyphicon-cloud-download"></i> <?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?></a></strong></p>
											<?php } ?>
											</div>
										</div>
										<?php } ?>

										<?php
										$stLink = $conn->prepare("SELECT * FROM bolsa_atleta_link WHERE id_artigo = :id_artigo ORDER BY ordem ASC, id DESC");
										$stLink->execute(array("id_artigo" => $bolsaAtleta['id']));
										$qryLink = $stLink->fetchAll();

										if(count($qryLink)) {
										?>
										<div class="panel panel-primary">
											<div class="panel-heading">
										    	<h2 class="panel-title"><i class="glyphicon glyphicon-globe"></i> Links</h2>
										  	</div>
											<div class="panel-body">
											<?php
											foreach ($qryLink as $linkBolsaAtleta) {
											?>
												<p><strong><a href="<?= $linkBolsaAtleta['link'] ?>" target="_blank"><i class="glyphicon glyphicon-globe"></i> <?= empty($linkBolsaAtleta['descricao']) ? "Link" : $linkBolsaAtleta['descricao'] ?></a></strong></p>
											<?php } ?>
											</div>
										</div>
										<?php } ?>
									</div>
								</div>
							</div>
							<?php } ?>
						</div>
					</li>
				</ul>
				<?php } ?>
			</li>
			<?php } ?>
		</ul>
		<?php } ?>
	</li>
	<?php } ?>
</ul>
<?php
}

$atualizacao = atualizacao("bolsa_atleta", $cliente, $conn);
if($atualizacao != "") {
?>
<p class="text-right"><small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small></p>
<?php
}