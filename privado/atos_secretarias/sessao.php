<h2>Atos das Secretarias</h2>
<ol class="breadcrumb">
	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
	<li class="active">Secret&aacute;ria</li>
</ol>

<?php

$stSessao = $conn->prepare("SELECT * FROM pref_secretaria WHERE id_cliente = :id_cliente AND status_registro = :status_registro");
$stSessao->execute(array("id_cliente" => $cliente, "status_registro" => "A"));
$qrySessao = $stSessao->fetchAll();

if(count($qrySessao)) {
	$w = True;
?>
<table class="table table-hover table-striped">
	<?php foreach ($qrySessao as $sessao) {
		$stSecretaria = $conn->prepare("SELECT * FROM atos_secretarias_comp WHERE id_secretaria = :id_secretaria");
		$stSecretaria->execute(array("id_secretaria" => $sessao['id']));
		$secre = $stSecretaria->fetch();
		if($secre != null){
			if($w == True){
				?>

				<tr>
					<td class="text-center"><strong>ESCOLHA UMA SECRETARIA </strong></td>
				</tr>

		<?php
			$w = False;
			} ?>

			<tr>
				<td>
					<a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$atosSecretariasLista.$complemento); ?>&id_secretaria=<?= verifica($sessao['id']) ?>"><i class="glyphicon glyphicon-folder-open"></i><strong>&nbsp;&nbsp;<?= $sessao['secretaria'] ?></strong></a>
				</td>
			</tr>

		<?php } ?>
	<?php }
	if($w == True){ ?>

		<tr>
			<td class="text-center"><strong>N&Atilde;O POSSUI ATOS VINCULADOS AS SECRETARIAS </strong></td>
		</tr>

		<?php
		$w = False;
		}
	?>
</table>
<?php } ?>

<?php
$atualizacao = atualizacao("atos_secretarias", $cliente, $conn);
if($atualizacao != "") {
	?>
	<p class="text-right"><small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small></p>
	<?php
}