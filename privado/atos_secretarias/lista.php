<?php
$id_secretaria = secure($_GET["id_secretaria"]);

$stSecretaria = $conn->prepare("SELECT * FROM pref_secretaria WHERE id_cliente = :id_cliente AND status_registro = :status_registro AND id = :id_secretaria");
$stSecretaria->execute(array("id_cliente" =>$cliente, "status_registro" => "A", "id_secretaria" => $id_secretaria));
$secre = $stSecretaria->fetch();
?>
<h2>Atos das Secretarias <span class="label label-default"><?= $secre["secretaria"] ?></span></h2>
<ol class="breadcrumb">
    <li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
    <li><a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$atosSecretarias.$complemento); ?>">Atos das Secretarias</a></li>
    <li class="active"><?= $secre["secretaria"] ?></li>
</ol>

<?php

$stCategoria = $conn->prepare("SELECT * FROM atos_secretarias_categoria WHERE id_cliente = :id_cliente AND status_registro = :status_registro AND EXISTS(SELECT 1 FROM atos_secretarias WHERE atos_secretarias.id_categoria = atos_secretarias_categoria.id AND atos_secretarias.status_registro = :status_registro AND EXISTS(SELECT 1 FROM atos_secretarias_comp WHERE atos_secretarias_comp.id_ato = atos_secretarias.id AND atos_secretarias_comp.id_secretaria = :id_secretaria)) ORDER BY id DESC");
$stCategoria->execute(array("id_cliente" => $cliente, "status_registro" => "A", "id_secretaria" => $id_secretaria));
$qryCategoria = $stCategoria->fetchAll();

if(count($qryCategoria)) {
    ?>
    <ul class="treeview">
        <?php
        foreach ($qryCategoria as $categoria) {
            ?>
            <li><a href="#"><?= $categoria['descricao'] ?></a>
                <?php
                $stArquivo = $conn->prepare("SELECT * FROM atos_secretarias WHERE id_categoria = :id_categoria AND (id_subcategoria IS NULL OR id_subcategoria = '') AND status_registro = :status_registro AND EXISTS(SELECT 1 FROM atos_secretarias_comp WHERE atos_secretarias_comp.id_ato = atos_secretarias.id AND atos_secretarias_comp.id_secretaria = :id_secretaria) ORDER BY id DESC");
                $stArquivo->execute(array("id_categoria" => $categoria['id'], "status_registro" => "A", "id_secretaria" => $id_secretaria));
                $qryArquivo = $stArquivo->fetchAll();

                if(count($qryArquivo)) {
                    ?>
                    <ul>
                        <?php
                        foreach($qryArquivo as $arquivo) {

                            if($arquivo['arquivo'] != "") {

                                $linkDocumento = $CAMINHOARQ . "/" . $arquivo['arquivo'];
                                $imagem = "glyphicon-cloud-download";
                                $titulo = empty($arquivo['titulo']) ? "Anexo" : $arquivo['titulo'];

                            } else if($arquivo['link'] != "") {

                                $linkDocumento = $arquivo['link'];
                                $imagem = "glyphicon-globe";
                                $titulo = empty($arquivo['titulo']) ? "Link" : $arquivo['titulo'];
                            }
                            ?>
                            <li><a href="<?=$linkDocumento ?>" target="_blank"><i class="glyphicon <?= $imagem ?>"></i> <?= $titulo ?></a></li>
                        <?php } ?>
                    </ul>
                <?php } ?>
                <?php
                $stSubcategoria = $conn->prepare("SELECT * FROM atos_secretarias_subcategoria WHERE id_categoria = :id_categoria AND status_registro = :status_registro AND EXISTS(SELECT 1 FROM atos_secretarias WHERE atos_secretarias.id_subcategoria = atos_secretarias_subcategoria.id AND atos_secretarias.status_registro = :status_registro AND EXISTS(SELECT 1 FROM atos_secretarias_comp WHERE atos_secretarias_comp.id_ato = atos_secretarias.id AND atos_secretarias_comp.id_secretaria = :id_secretaria)) ORDER BY id DESC");
                $stSubcategoria->execute(array("id_categoria" => $categoria['id'], "status_registro" => "A", "id_secretaria" => $id_secretaria));
                $qrySubcategoria = $stSubcategoria->fetchAll();

                if(count($qrySubcategoria)) {
                    ?>
                    <ul>
                        <?php
                        foreach ($qrySubcategoria as $subcategoria) {
                            ?>
                            <li><a href="#"><?= $subcategoria['descricao'] ?></a>
                                <?php
                                $stArquivo = $conn->prepare("SELECT * FROM atos_secretarias WHERE id_subcategoria = :id_subcategoria AND status_registro = :status_registro AND EXISTS(SELECT 1 FROM atos_secretarias_comp WHERE atos_secretarias_comp.id_ato = atos_secretarias.id AND atos_secretarias_comp.id_secretaria = :id_secretaria) ORDER BY id DESC");
                                $stArquivo->execute(array("id_subcategoria" => $subcategoria['id'], "status_registro" => "A", "id_secretaria" => $id_secretaria));
                                $qryArquivo = $stArquivo->fetchAll();

                                if(count($qryArquivo)) {
                                    ?>
                                    <ul>
                                        <?php
                                        foreach($qryArquivo as $arquivo) {

                                            if($arquivo['arquivo'] != "") {

                                                $linkDocumento = $CAMINHOARQ . "/" . $arquivo['arquivo'];
                                                $imagem = "glyphicon-cloud-download";
                                                $titulo = empty($arquivo['titulo']) ? "Anexo" : $arquivo['titulo'];

                                            } else if($arquivo['link'] != "") {

                                                $linkDocumento = $arquivo['link'];
                                                $imagem = "glyphicon-globe";
                                                $titulo = empty($arquivo['titulo']) ? "Link" : $arquivo['titulo'];
                                            }
                                            ?>
                                            <li><a href="<?=$linkDocumento ?>" target="_blank"><i class="glyphicon <?= $imagem ?>"></i> <?= $titulo ?></a></li>
                                        <?php } ?>
                                    </ul>
                                <?php } ?>
                            </li>
                        <?php } ?>
                    </ul>
                <?php } ?>
            </li>
        <?php } ?>
    </ul>
    <?php
}

$atualizacao = atualizacao("atos_secretarias", $cliente, $conn);
if($atualizacao != "") {
    ?>
    <p class="text-right"><small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small></p>
    <?php
}