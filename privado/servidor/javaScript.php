<?php if ($cliente == '12109') { ?>
    <script>
        jQuery.fn.hideEmptyColumns = function () {
            $(this).each(function (index) {
                $(this).hideEmptyColumnsFromUniqueTable();
            });
        };

        jQuery.fn.hideEmptyColumnsFromUniqueTable = function () {
            var $this = $(this);
            var rows = $this.find("tr");
            var columns = rows.eq(0).find("th");
            var countRows = rows.length;
            var countColumns = columns.length;

            columnIsEmpty = function (rowIndex, columnIndex) {
                if (rowIndex < countRows) {
                    var currentRow = rows.eq(rowIndex);
                    var columnsThisRow = currentRow.find("td");
                    var currentCell = columnsThisRow.eq(columnIndex);
                    var valueCell = currentCell.text().trim();

                    return (columnIsEmpty(++rowIndex, columnIndex) && valueCell === "");
                }
                return true;
            }

            hideColumn = function (columnIndex, rowIndex) {
                rowIndex = rowIndex || 0;
                var currentRow = rows.eq(rowIndex);
                var columnsThisRow = currentRow.find(rowIndex === 0 ? "th" : "td");
                var currentCell = columnsThisRow.eq(columnIndex);
                if (rowIndex < countRows)
                    hideColumn(columnIndex, ++rowIndex);

                $(currentCell).remove();
            }

            execute = function (columnIndex) {
                if (columnIndex < countColumns)
                    execute(++columnIndex);

                if (columnIsEmpty(1, columnIndex))
                    hideColumn(columnIndex);
            }

            execute(1);
        };

        $(document).ready(function () {
            $(".table-responsive").hideEmptyColumns();
        });
    </script>
<? } ?>