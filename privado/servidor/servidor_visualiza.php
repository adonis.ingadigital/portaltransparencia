<?php
$id = secure($_GET['id']);

$stServidor = $conn->prepare("SELECT * FROM servidor WHERE id = :id AND status_registro = :status_registro");
$stServidor->execute(array("id" => $id, "status_registro" => "A"));
$servidor = $stServidor->fetch();

switch($servidor['tipo']) {

	case "1": $tipoServidor = "Ativo"; break;
	case "2": $tipoServidor = "Afastado/Exonerado"; break;
	case "3": $tipoServidor = "Inativo/Pensionista"; break;
	case "4": $tipoServidor = "Tempor&aacute;rio"; break;
}

switch($servidor['forma_investidura']) {

	case "1": $formaContratacao = "Concurso P&uacute;blico"; break;
	case "2": $formaContratacao = "Livre Nomea&ccedil;&atilde;o"; break;
	case "3": $formaContratacao = "Art. 19 - ADCT/88"; break;
    case "4": $formaContratacao = "Pensionista"; break;
}
?>
<h2>Servidores</h2>
<ol class="breadcrumb">
	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
	<li><a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$servidorLista.$complemento); ?>">Servidores</a></li>
	<li class="active"><?= $servidor['nome'] ?></li>
</ol>

<h3><?= $servidor['nome'] ?></h3>
<p><strong>Tipo / Status do Servidor:&nbsp;</strong><?= $tipoServidor ?></p>
<p><strong>Forma de Contrata&ccedil;&atilde;o:&nbsp;</strong><?= $formaContratacao ?></p>
<?php if($servidor['lotacao'] != "") { ?><p><strong>Lota&ccedil;&atilde;o:&nbsp;</strong><?= $servidor['lotacao'] ?></p><?php } ?>
<?php if($servidor['cargo'] != "") { ?><p><strong>Cargo:&nbsp;</strong><?= $servidor['cargo'] ?></p><?php } ?>
<?php if($servidor['carga_horaria'] != "") { ?><p><strong>Carga Hor&aacute;ria:&nbsp;</strong><?= $servidor['carga_horaria'] ?></p><?php } ?>
<?php if($servidor['expediente'] != "") { ?><p><strong>Expediente:&nbsp;</strong><?= $servidor['expediente'] ?></p><?php } ?>
<?php if($servidor['cedido_por'] != "") { ?><p><strong>Cedido Por:&nbsp;</strong><?= $servidor['cedido_por'] ?></p><?php } ?>
<?php if($servidor['cedido_a'] != "") { ?><p><strong>Cedido &agrave;:&nbsp;</strong><?= $servidor['cedido_a'] ?></p><?php } ?>
<?php if($servidor['data_admissao'] != "") { ?><p><strong>Data de Admiss&atilde;o:&nbsp;</strong><?= formata_data($servidor['data_admissao']) ?></p><?php } ?>
<?php if($servidor['data_fim_contrato'] != "") { ?><p><strong>Data de Fim do Contrato:&nbsp;</strong><?= formata_data($servidor['data_fim_contrato']) ?></p><?php } ?>
<?php if($servidor['salario'] != "") { ?><p><strong>Sal&aacute;rio:&nbsp;</strong><?= $servidor['salario'] ?></p><?php } ?>
<?php if($servidor['matricula'] != "") { ?><p><strong>Matr&iacute;cula:&nbsp;</strong><?= $servidor['matricula'] ?></p><?php } ?>
<?php if($servidor['arquivo'] != "") { ?><p><strong>Processo: </strong><a href="<?= $CAMINHOARQ ?>/<?= $servidor['arquivo'] ?>" target="_blank" class="label label-primary"><i class="glyphicon glyphicon-cloud-download"></i> Baixar anexo</a></p><?php } ?>
<a href="<?=$CAMINHO?>/index.php?sessao=<?= $sequencia.$servidorLista.$complemento?>" class="btn btn-primary"><i class="glyphicon glyphicon-arrow-left"></i> Voltar</a>