<h2>Servidores</h2>
<ol class="breadcrumb">
    <li><a href="<?= $CAMINHO ?>">In&iacute;cio</a></li>
    <li class="active">Servidores</li>
</ol>

<?php
$stServidor1 = $conn->prepare("SELECT * FROM servidor WHERE id_cliente = :id_cliente AND tipo = :tipo AND status_registro = :status_registro ORDER BY nome ASC");
$stServidor1->execute(array("id_cliente" => $cliente, "tipo" => 1, "status_registro" => "A"));
$qryServidor1 = $stServidor1->fetchAll();

if (count($qryServidor1)) {
    ?>
    <h4>Servidores Ativos</h4>
    <div class="table-responsive">
        <table class="table table-striped table-hover table-bordered table-condensed">
            <tr>
                <th>&nbsp;</th>
                <th>Nome</th>
                <th>Matr&iacute;cula</th>
                <th>Cargo</th>
                <th>Data de Admiss&atilde;o</th>
                <th>Data do Fim do Contrato</th>
                <?php if ($cliente != "12109") { ?>
                    <th>Sal&aacute;rio</th>
                <? } ?>
            </tr>
            <?php foreach ($qryServidor1 as $servidor) { ?>
                <tr>
                    <td>
                        <a href="<?= $CAMINHO ?>/index.php?sessao=<?= $sequencia . $servidorVisualiza . $complemento ?>&id=<?= $servidor['id'] ?>"
                           class="btn btn-primary" data-toggle="tooltip" data-placement="right" title="Visualizar">
                            <i class="glyphicon glyphicon-search"></i>
                        </a>
                    </td>
                    <td>
                        <?= $servidor['nome'] ?>
                    </td>
                    <td><?= $servidor['matricula'] ?></td>
                    <td><?= $servidor['cargo'] ?></td>
                    <td><?= formata_data($servidor['data_admissao']) ?></td>
                    <td><?= formata_data($servidor['data_fim_contrato']) ?></td>
                    <?php if ($cliente != "12109") { ?>
                        <td><?= $servidor['salario'] ?></td>
                    <? } ?>
                </tr>
            <?php } ?>
        </table>
    </div>
    <?php
}

$stServidor2 = $conn->prepare("SELECT * FROM servidor WHERE id_cliente = :id_cliente AND tipo = :tipo AND status_registro = :status_registro ORDER BY nome ASC");
$stServidor2->execute(array("id_cliente" => $cliente, "tipo" => 2, "status_registro" => "A"));
$qryServidor2 = $stServidor2->fetchAll();

if (count($qryServidor2)) {
    ?>
    <h4>Servidores Afastados / Exonerados</h4>
    <div class="table-responsive">
        <table class="table table-striped table-hover table-bordered table-condensed">
            <tr>
                <th>&nbsp;</th>
                <th>Nome</th>
                <th>Matr&iacute;cula</th>
                <th>Cargo</th>
                <th>Data de Admiss&atilde;o</th>
                <th>Data do Fim do Contrato</th>

                <?php if ($cliente != "12109") { ?>
                    <th>Sal&aacute;rio</th>
                <? } ?>
            </tr>
            <?php foreach ($qryServidor2 as $servidor) { ?>
                <tr>
                    <td>
                        <a href="<?= $CAMINHO ?>/index.php?sessao=<?= $sequencia . $servidorVisualiza . $complemento ?>&id=<?= $servidor['id'] ?>"
                           class="btn btn-primary" data-toggle="tooltip" data-placement="right" title="Visualizar">
                            <i class="glyphicon glyphicon-search"></i>
                        </a>
                    </td>
                    <td><?= $servidor['nome'] ?></td>
                    <td><?= $servidor['matricula'] ?></td>
                    <td><?= $servidor['cargo'] ?></td>
                    <td><?= formata_data($servidor['data_admissao']) ?></td>
                    <td><?= formata_data($servidor['data_fim_contrato']) ?></td>

                    <?php if ($cliente != "12109") { ?>
                        <td><?= $servidor['salario'] ?></td>
                    <? } ?>
                </tr>
            <?php } ?>
        </table>
    </div>
    <?php
}

$stServidor3 = $conn->prepare("SELECT * FROM servidor WHERE id_cliente = :id_cliente AND tipo = :tipo AND status_registro = :status_registro ORDER BY nome ASC");
$stServidor3->execute(array("id_cliente" => $cliente, "tipo" => 3, "status_registro" => "A"));
$qryServidor3 = $stServidor3->fetchAll();

if (count($qryServidor3)) {
    ?>
    <h4>Servidores Inativos / Pensionistas</h4>
    <div class="table-responsive">
        <table class="table table-striped table-hover table-bordered table-condensed">
            <tr>
                <th>&nbsp;</th>
                <th>Nome</th>
                <th>Matr&iacute;cula</th>
                <th>Cargo</th>
                <th>Data de Admiss&atilde;o</th>
                <th>Data do Fim do Contrato</th>

                <?php if ($cliente != "12109") { ?>
                    <th>Sal&aacute;rio</th>
                    <?
                } ?>
            </tr>
            <?php foreach ($qryServidor3 as $servidor) { ?>
                <tr>
                    <td>
                        <a href="<?= $CAMINHO ?>/index.php?sessao=<?= $sequencia . $servidorVisualiza . $complemento ?>&id=<?= $servidor['id'] ?>"
                           class="btn btn-primary" data-toggle="tooltip" data-placement="right" title="Visualizar">
                            <i class="glyphicon glyphicon-search"></i>
                        </a>
                    </td>
                    <td><?= $servidor['nome'] ?></td>
                    <td><?= $servidor['matricula'] ?></td>
                    <td><?= $servidor['cargo'] ?></td>
                    <td><?= formata_data($servidor['data_admissao']) ?></td>
                    <td><?= formata_data($servidor['data_fim_contrato']) ?></td>

                    <?php if ($cliente != "12109") { ?>
                        <td><?= $servidor['salario'] ?></td>
                    <? } ?>
                </tr>
            <?php } ?>
        </table>
    </div>
    <?php
}


$stServidor4 = $conn->prepare("SELECT * FROM servidor WHERE id_cliente = :id_cliente AND tipo = :tipo AND status_registro = :status_registro ORDER BY nome ASC");
$stServidor4->execute(array("id_cliente" => $cliente, "tipo" => 4, "status_registro" => "A"));
$qryServidor4 = $stServidor4->fetchAll();

if (count($qryServidor4)) {
    ?>
    <h4>Servidores Tempor&aacute;rios</h4>
    <div class="table-responsive">
        <table class="table table-striped table-hover table-bordered table-condensed">
            <tr>
                <th>&nbsp;</th>
                <th>Nome</th>
                <th>Matr&iacute;cula</th>
                <th>Cargo</th>
                <th>Data de Admiss&atilde;o</th>
                <th>Data do Fim do Contrato</th>

                <?php if ($cliente != "12109") { ?>
                    <th>Sal&aacute;rio</th>
                <? } ?>
            </tr>
            <?php foreach ($qryServidor4 as $servidor) { ?>
                <tr>
                    <td>
                        <a href="<?= $CAMINHO ?>/index.php?sessao=<?= $sequencia . $servidorVisualiza . $complemento ?>&id=<?= $servidor['id'] ?>"
                           class="btn btn-primary" data-toggle="tooltip" data-placement="right" title="Visualizar">
                            <i class="glyphicon glyphicon-search"></i>
                        </a>
                    </td>
                    <td><?= $servidor['nome'] ?></td>
                    <td><?= $servidor['matricula'] ?></td>
                    <td><?= $servidor['cargo'] ?></td>
                    <td><?= formata_data($servidor['data_admissao']) ?></td>
                    <td><?= formata_data($servidor['data_fim_contrato']) ?></td>

                    <?php if ($cliente != "12109") { ?>
                        <td><?= $servidor['salario'] ?></td>
                    <? } ?>
                </tr>
            <?php } ?>
        </table>
    </div>
    <?php
}

$stCategoria = $conn->prepare("SELECT * FROM servidor_categoria WHERE id_cliente = :id_cliente AND status_registro = :status_registro ORDER BY descricao ASC");
$stCategoria->execute(array("id_cliente" => $cliente, "status_registro" => "A"));
$qryCategoria = $stCategoria->fetchAll();

if (count($qryCategoria)) {
    ?>
    <ul class="treeview">
        <?php
        foreach ($qryCategoria as $categoria) {
            ?>
            <li><a href="#"><?= $categoria['descricao'] ?></a>
                <?php
                $stArquivo = $conn->prepare("SELECT * FROM servidor_anexo_link WHERE id_categoria = :id_categoria AND (id_subcategoria IS NULL OR id_subcategoria = '') AND status_registro = :status_registro ORDER BY titulo DESC");
                $stArquivo->execute(array("id_categoria" => $categoria['id'], "status_registro" => "A"));
                $qryArquivo = $stArquivo->fetchAll();

                if (count($qryArquivo)) {
                    ?>
                    <ul>
                        <?php
                        foreach ($qryArquivo as $arquivo) {

                            if ($arquivo['arquivo'] != "") {

                                $linkDocumento = $CAMINHOARQ . "/" . $arquivo['arquivo'];
                                $imagem = "glyphicon-cloud-download";
                                $titulo = empty($arquivo['titulo']) ? "Anexo" : $arquivo['titulo'];

                            } else if ($arquivo['link'] != "") {

                                $linkDocumento = $arquivo['link'];
                                $imagem = "glyphicon-globe";
                                $titulo = empty($arquivo['titulo']) ? "Link" : $arquivo['titulo'];
                            }
                            ?>
                            <li><a href="<?= $linkDocumento ?>" target="_blank"><i
                                            class="glyphicon <?= $imagem ?>"></i> <?= $titulo ?></a></li>
                        <?php } ?>
                    </ul>
                <?php } ?>
                <?php
                $stSubcategoria = $conn->prepare("SELECT * FROM servidor_subcategoria WHERE id_categoria = :id_categoria AND status_registro = :status_registro ORDER BY id DESC");
                $stSubcategoria->execute(array("id_categoria" => $categoria['id'], "status_registro" => "A"));
                $qrySubcategoria = $stSubcategoria->fetchAll();

                if (count($qrySubcategoria)) {
                    ?>
                    <ul>
                        <?php
                        foreach ($qrySubcategoria as $subcategoria) {
                            ?>
                            <li><a href="#"><?= $subcategoria['descricao'] ?></a>
                                <?php
                                $stArquivo = $conn->prepare("SELECT * FROM servidor_anexo_link WHERE id_subcategoria = :id_subcategoria AND status_registro = :status_registro ORDER BY titulo DESC");
                                $stArquivo->execute(array("id_subcategoria" => $subcategoria['id'], "status_registro" => "A"));
                                $qryArquivo = $stArquivo->fetchAll();

                                if (count($qryArquivo)) {
                                    ?>
                                    <ul>
                                        <?php
                                        foreach ($qryArquivo as $arquivo) {

                                            if ($arquivo['arquivo'] != "") {

                                                $linkDocumento = $CAMINHOARQ . "/" . $arquivo['arquivo'];
                                                $imagem = "glyphicon-cloud-download";
                                                $titulo = empty($arquivo['titulo']) ? "Anexo" : $arquivo['titulo'];

                                            } else if ($arquivo['link'] != "") {

                                                $linkDocumento = $arquivo['link'];
                                                $imagem = "glyphicon-globe";
                                                $titulo = empty($arquivo['titulo']) ? "Link" : $arquivo['titulo'];
                                            }
                                            ?>
                                            <li><a href="<?= $linkDocumento ?>" target="_blank"><i
                                                            class="glyphicon <?= $imagem ?>"></i> <?= $titulo ?></a>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                <?php } ?>
                            </li>
                        <?php } ?>
                    </ul>
                <?php } ?>
            </li>
        <?php } ?>
    </ul>
    <?php
}

$atualizacao = atualizacao("servidores", $cliente, $conn);
if ($atualizacao != "") {
    ?>
    <p class="text-right">
        <small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima
                atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small>
    </p>
    <?php
}
include_once "javaScript.php";