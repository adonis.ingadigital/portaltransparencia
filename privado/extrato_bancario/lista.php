<h2>Extrato Banc&aacute;rios</h2>
<ol class="breadcrumb">
	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
	<li class="active">Extrato</li>
</ol>

<?php
$stAno = $conn->prepare("SELECT ano FROM extrato_bancario WHERE id_categoria IN (SELECT id FROM extrato_bancario_categoria WHERE id_cliente = :id_cliente AND status_registro = :status_registro) AND status_registro = :status_registro GROUP BY ano DESC");
$stAno->execute(array("id_cliente" => $cliente, "status_registro" => "A"));
$qryAno = $stAno->fetchAll();

if(count($qryAno)) {
?>
<div>
	<ul class="nav nav-pills" role="tablist">
	<?php foreach ($qryAno as $indice => $ano) { ?>
		<li role="presentation"<?php if($indice == 0) echo ' class="active"'; ?>><a href="<?= "#" . $ano['ano'] ?>" aria-controls="<?= $ano['ano'] ?>" role="tab" data-toggle="tab"><strong><?= $ano['ano'] ?></strong></a></li>
	<?php } ?>
	</ul>

	<div class="tab-content">
	<?php foreach ($qryAno as $indice => $ano) { ?>
		<div role="tabpanel" class="tab-pane<?php if($indice == 0) echo " active"; ?>" id="<?= $ano['ano'] ?>">
			<p>&nbsp;</p>
			<?php
			$stCategoria = $conn->prepare("SELECT * FROM extrato_bancario_categoria WHERE id_cliente = :id_cliente AND id IN (SELECT id_categoria FROM extrato_bancario WHERE ano = :ano AND status_registro = :status_registro) AND status_registro = :status_registro ORDER BY id DESC");
			$stCategoria->execute(array("id_cliente" => $cliente, "ano" => $ano['ano'], "status_registro" => "A"));
			$qryCategoria = $stCategoria->fetchAll();

			if(count($qryCategoria)) {
			?>
			<ul class="treeview">
				<?php
				foreach ($qryCategoria as $categoria) {
				?>
				<li><a href="#"><?= $categoria['descricao'] ?></a>
					<?php
					$stArquivo = $conn->prepare("SELECT * FROM extrato_bancario WHERE id_categoria = :id_categoria AND (id_subcategoria IS NULL OR id_subcategoria = '') AND ano = :ano AND status_registro = :status_registro ORDER BY titulo DESC");
					$stArquivo->execute(array("id_categoria" => $categoria['id'], "ano" => $ano['ano'], "status_registro" => "A"));
					$qryArquivo = $stArquivo->fetchAll();

					if(count($qryArquivo)) {
					?>
					<ul>
						<?php
						foreach($qryArquivo as $arquivo) {

							if($arquivo['arquivo'] != "") {

								$linkDocumento = $CAMINHOARQ . "/" . $arquivo['arquivo'];
								$imagem = "glyphicon-cloud-download";
								$titulo = empty($arquivo['titulo']) ? "Anexo" : $arquivo['titulo'];

							} else if($arquivo['link'] != "") {

								$linkDocumento = $arquivo['link'];
								$imagem = "glyphicon-globe";
								$titulo = empty($arquivo['titulo']) ? "Link" : $arquivo['titulo'];
							}
						?>
						<li><a href="<?=$linkDocumento ?>" target="_blank"><i class="glyphicon <?= $imagem ?>"></i> <?= $titulo ?></a></li>
						<?php } ?>
					</ul>
					<?php } ?>
					<?php
					$stSubcategoria = $conn->prepare("SELECT * FROM extrato_bancario_subcategoria WHERE id_categoria = :id_categoria AND id IN (SELECT id_subcategoria FROM extrato_bancario WHERE ano = :ano AND status_registro = :status_registro) AND status_registro = :status_registro ORDER BY id DESC");
					$stSubcategoria->execute(array("id_categoria" => $categoria['id'], "ano" => $ano['ano'], "status_registro" => "A"));
					$qrySubcategoria = $stSubcategoria->fetchAll();

					if(count($qrySubcategoria)) {
					?>
					<ul>
						<?php
						foreach ($qrySubcategoria as $subcategoria) {
						?>
						<li><a href="#"><?= $subcategoria['descricao'] ?></a>
							<?php
							$stArquivo = $conn->prepare("SELECT * FROM extrato_bancario WHERE id_subcategoria = :id_subcategoria AND status_registro = :status_registro AND ano = :ano ORDER BY titulo DESC");
							$stArquivo->execute(array("id_subcategoria" => $subcategoria['id'], "ano" => $ano['ano'], "status_registro" => "A"));
							$qryArquivo = $stArquivo->fetchAll();

							if(count($qryArquivo)) {
							?>
							<ul>
								<?php
								foreach($qryArquivo as $arquivo) {

									if($arquivo['arquivo'] != "") {

										$linkDocumento = $CAMINHOARQ . "/" . $arquivo['arquivo'];
										$imagem = "glyphicon-cloud-download";
										$titulo = empty($arquivo['titulo']) ? "Anexo" : $arquivo['titulo'];

									} else if($arquivo['link'] != "") {

										$linkDocumento = $arquivo['link'];
										$imagem = "glyphicon-globe";
										$titulo = empty($arquivo['titulo']) ? "Link" : $arquivo['titulo'];
									}
								?>
								<li><a href="<?= $linkDocumento ?>" target="_blank"><i class="glyphicon <?= $imagem ?>"></i> <?= $titulo ?></a></li>
								<?php } ?>
							</ul>
							<?php } ?>
						</li>
						<?php } ?>
					</ul>
					<?php } ?>
				</li>
				<?php } ?>
			</ul>
			<?php } ?>
		</div>
	<?php } ?>
	</div>
</div>
<?php
}

$atualizacao = atualizacao("extrato_bancario", $cliente, $conn);
if($atualizacao != "") {
?>
<p class="text-right"><small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small></p>
<?php
}