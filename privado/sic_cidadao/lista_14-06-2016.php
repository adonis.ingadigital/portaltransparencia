<h2>Servi&ccedil;o F&iacute;sico de Informa&ccedil;&atilde;o ao Cidad&atilde;o</h2>
<ol class="breadcrumb">
	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
	<li class="active">Servi&ccedil;o F&iacute;sico de Informa&ccedil;&atilde;o ao Cidad&atilde;o</li>
</ol>

<?php
$stSic = $conn->prepare("SELECT * FROM sic_cidadao WHERE id_cliente = :id_cliente AND status_registro = :status_registro ORDER BY titulo");
$stSic->execute(array("id_cliente" => $cliente, "status_registro" => "A"));
$qrySic = $stSic->fetchAll();

if(count($qrySic)) {
?>
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
	<?php foreach ($qrySic as $sic) { ?>
	<div class="panel panel-default">
	    <div class="panel-heading" role="tab" id="heading_<?= $sic['id'] ?>">
	     	<h4 class="panel-title">
	        	<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?= $sic['id'] ?>" aria-expanded="true" aria-controls="collapse_<?= $sic['id'] ?>">
	          	<i class="glyphicon glyphicon-triangle-right"></i> <?= verifica($sic['titulo']) ?>
	        	</a>
	    	</h4>
	    </div>
		<div id="collapse_<?= $sic['id'] ?>" class="panel-collapse <?php if(count($qrySic) > 1) echo "collapse"; ?>" role="tabpanel" aria-labelledby="heading_<?= $sic['id'] ?>">
			<div class="panel-body">
				<?php if($sic['orgao'] != "") { ?><p><strong>&Oacute;rg&atilde;o:</strong> <?= $sic['orgao'] ?></p><?php } ?>
				<?php if($sic['responsavel'] != "") { ?><p><strong>Respons&aacute;vel:</strong> <?= $sic['responsavel'] ?></p><?php } ?>
				<?php if($sic['endereco'] != "") { ?><p><strong>Endere&ccedil;o:</strong> <?= $sic['endereco'] ?></p><?php } ?>
				<?php if($sic['telefone'] != "") { ?><p><strong>Telefone:</strong> <?= $sic['telefone'] ?></p><?php } ?>
				<?php if($sic['horario_atendimento'] != "") { ?><p><strong>Hor&aacute;rio de Atendimento:</strong> <?= $sic['horario_atendimento'] ?></p><?php } ?>
				<?php if($sic['observacao'] != "") { ?><p><strong>Observa&ccedil;&otilde;es:</strong> <?= $sic['observacao'] ?></p><?php } ?>
			</div>
		</div>
	</div>
	<?php } ?>
</div>
<?php
}

$atualizacao = atualizacao("sic_cidadao", $cliente, $conn);
if($atualizacao != "") {
?>
<p class="text-right"><small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small></p>
<?php
}