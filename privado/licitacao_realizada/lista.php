<h2>Licita&ccedil;&atilde;o Realizada</h2>
<ol class="breadcrumb">
	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
	<li class="active">Licita&ccedil;&atilde;o Realizada</li>
</ol>

<?php
// var_dump($cliente);
$stCategoria = $conn->prepare("SELECT * FROM licitacao_realizada_categoria WHERE id_cliente = :id_cliente AND status_registro = :status_registro ORDER BY descricao DESC");
$stCategoria->execute(array("id_cliente" => $cliente, "status_registro" => "A"));
$qryCategoria = $stCategoria->fetchAll();

$stLicitacaoRealizada = $conn->prepare("SELECT licitacao_edital.*, 
	 											   licitacao_modalidade.descricao modalidade
		        						      FROM licitacao_edital, licitacao_modalidade
		        						     WHERE licitacao_edital.id_modalidade = licitacao_modalidade.id
										       AND licitacao_edital.id_cliente = :id_cliente
											   AND licitacao_edital.id_autarquia IS NULL
											   AND licitacao_edital.em_andamento = :em_andamento
		        						       AND licitacao_edital.status_registro = :status_registro
		        						       AND licitacao_edital.data_abertura <= CURRENT_DATE()
									      ORDER BY licitacao_edital.data_abertura DESC, 
										           licitacao_edital.hora_abertura ASC 
											 LIMIT 15");

	$stLicitacaoRealizada->execute(array(":id_cliente" => $cliente, ":em_andamento" => "F", ":status_registro" => "A"));
	$qryLicitacaoRealizada = $stLicitacaoRealizada->fetchAll();
?>
<?if($cliente == '43'){ 
?>
<div role="tabpanel" class="tab-pane" id="todos-anteriores">

<?php
	if(count($qryLicitacaoRealizada)) {
?>

						<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
							<p></p>

<?php 
		foreach ($qryLicitacaoRealizada as $licitacao) { 
?>

							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="heading_<?= $licitacao['id'] ?>">
									<h4 class="panel-title">
										<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?= $licitacao['id'] ?>" aria-expanded="true" aria-controls="collapse_<?= $licitacao['id'] ?>">
											<div class="row">

<?php
			if($cliente == "12101"){
				$stSecretaria = $conn->prepare("SELECT nome_menu 
												  FROM pref_secretaria 
												 WHERE id_cliente = :id_cliente 
												   AND id = :id 
												   AND status_registro = :status_registro 
												 LIMIT 1");
				$stSecretaria->execute(array("id_cliente" => $cliente, "id" => $licitacao['id_secretaria'], "status_registro" => "A"));
				$qrySecretaria = $stSecretaria->fetch();
?>

												<div class="col-sm-4"><i class="glyphicon glyphicon-triangle-right a"></i> <strong><?= $licitacao['titulo'] ?></strong></div>
												<div class="col-sm-2"><strong>Publica&ccedil;&atilde;o:</strong> <?= formata_data($licitacao['data_publicacao']) ?></div>
												<div class="col-sm-3"><strong>Secretaria: </strong><?= $qrySecretaria['nome_menu'] ?></div>  
									
<?
				if(!empty($licitacao['data_disputa'])){
?>

												<div class="col-sm-3">
													<strong>Abertura:</strong> <?= formata_data($licitacao['data_abertura']) ?><? if(!empty($licitacao['hora_abertura'])) echo " &agrave;s " . $licitacao['hora_abertura'] ?><br>
													<strong>Disputa:</strong> <?= formata_data($licitacao['data_disputa']) ?><? if(!empty($licitacao['hora_disputa'])) echo " &agrave;s " . $licitacao['hora_disputa'] ?>
												</div>

<?
				} else {
?>

												<div class="col-sm-3"><strong>Abertura:</strong> <?= formata_data($licitacao['data_abertura']) ?><? if(!empty($licitacao['hora_abertura'])) echo " &agrave;s " . $licitacao['hora_abertura'] ?></div>			
									
<?php
				}
			} else {
?>

												<div class="col-sm-12 espacoFlexivel espacoLinhas" style="padding: 0;">
										
<?
				if(!empty($licitacao['data_disputa'])){
?>

													<div class="col-sm-4">
														<strong>Abertura:</strong> <?= formata_data($licitacao['data_abertura']) ?><? if(!empty($licitacao['hora_abertura'])) echo " &agrave;s " . $licitacao['hora_abertura'] ?>
													</div>
													<div class="col-sm-4">
														<strong>Disputa:</strong> <?= formata_data($licitacao['data_disputa']) ?><? if(!empty($licitacao['hora_disputa'])) echo " &agrave;s " . $licitacao['hora_disputa'] ?>
													</div>
										
<? 
				} else {
?>
									
							  						<div class="col-sm-4"><strong>Abertura:</strong> <?= formata_data($licitacao['data_abertura']) ?><? if(!empty($licitacao['hora_abertura'])) echo " &agrave;s " . $licitacao['hora_abertura'] ?></div>			
										
<? 
				} 
?>
												</div>	
												<div class="col-sm-12 espacoLinhas"><i class="glyphicon glyphicon-triangle-right c"></i> <strong><?= $licitacao['titulo'] ?></strong></div>
												<div class="col-sm-4"><strong>Publica&ccedil;&atilde;o:</strong> <?= formata_data($licitacao['data_publicacao']) ?></div>
									
<?
				if(!empty($licitacao['id_autarquia']) && $licitacao['id_autarquia'] != NULL){
					$autarquia = $conn->prepare("SELECT autarquia 
												   FROM pref_autarquia 
												  WHERE id = :id 
													AND status_registro = :status_registro");
					$autarquia->execute(array("id" => $licitacao['id_autarquia'], "status_registro" => "A"));
					$autarquia = $autarquia->fetch();
?>

												<div class="col-sm-12"><strong>Autarquia: </strong><?= $autarquia['autarquia'] ?></div>
								
<?
				} 
			} 
?>

											</div>
										</a>
									</h4>
								</div>
								<div id="collapse_<?= $licitacao['id'] ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_<?= $licitacao['id'] ?>">
									<div class="panel-body">
										<p><strong>Modalidade:</strong> <?= $licitacao['modalidade'] ?></p>

<? 
			if($licitacao['valor_maximo'] != "") { 
?>
		
										<p><strong>Valor M&aacute;ximo:</strong> <?= $licitacao['valor_maximo'] ?></p>
							
<? 
			} 
?>
								
										<p><strong>Objeto:</strong> <?= verifica($licitacao['objeto']) ?></p>

<?php
			$stAnexo = $conn->prepare("SELECT * 
										 FROM licitacao_edital_anexo 
										WHERE id_edital = :id_edital 
										  AND status_registro = :status_registro 
									 ORDER BY ordem ASC, 
									 		  id DESC");

			$stAnexo->execute(array("id_edital" => $licitacao['id'], "status_registro" => "A"));
			$qryAnexo = $stAnexo->fetchAll();

			if(count($qryAnexo)) {
?>

										<div class="panel panel-primary">
											<div class="panel-heading">
												<h2 class="panel-title"><i class="glyphicon glyphicon-cloud-download"></i> Anexos</h2>
											</div>

											<div class="panel-body">

<?php
				foreach ($qryAnexo as $anexo) {
					$ata = true;
					if($cliente == '12101' && $anexo['id_categoria'] == '18' ){
						$ata = false;
					}
?>

												<p>
													<strong>
														<a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$licitacaoDownload.$complemento); ?>&nc=<?= $cliente ?>&id=<?= verifica($anexo['id']) ?>" class="text-danger">
															<i class="glyphicon glyphicon-cloud-download"></i> 
															<?= empty($anexo['descricao']) ? "Anexo" : utf8_decode($anexo['descricao']) ?>
															<?
																if(!$ata){
																	echo ' - Ata ';
																}
															?>
														</a>
													</strong>
												</p>

<?php 
				} 
?>

											</div>
										</div>

<?php 
			} 
?>

									</div>
								</div>
							</div>

<?php 
		} 
?>

							<p></p>
						</div>

<?php 
	} 
?>

					</div>

<?}	?>

<?php
if(count($qryCategoria)) {
?>
<ul class="treeview">

	<?php
	foreach ($qryCategoria as $categoria) {
	?>
	<li><a href="#"><?= $categoria['descricao'] ?></a>
		<?php
		$stArquivo = $conn->prepare("SELECT * FROM licitacao_realizada WHERE id_categoria = :id_categoria AND (id_subcategoria IS NULL OR id_subcategoria = '') AND status_registro = :status_registro ORDER BY titulo DESC");
		$stArquivo->execute(array("id_categoria" => $categoria['id'], "status_registro" => "A"));
		$qryArquivo = $stArquivo->fetchAll();

		if(count($qryArquivo)) {

		?>
		<ul>
			<?php
			foreach($qryArquivo as $arquivo) {

				if($arquivo['arquivo'] != "") {

					$linkDocumento = $CAMINHOARQ . "/" . $arquivo['arquivo'];
					$imagem = "glyphicon-cloud-download";
					$titulo = empty($arquivo['titulo']) ? "Anexo" : $arquivo['titulo'];

				} else if($arquivo['link'] != "") {

					$linkDocumento = $arquivo['link'];
					$imagem = "glyphicon-globe";
					$titulo = empty($arquivo['titulo']) ? "Link" : $arquivo['titulo'];
				}
			?>
			<li><a href="<?=$linkDocumento ?>" target="_blank"><i class="glyphicon <?= $imagem ?>"></i> <?= $titulo ?></a></li>
			<?php } ?>
		</ul>
		<?php } ?>
		<?php
		$stSubcategoria = $conn->prepare("SELECT * FROM licitacao_realizada_subcategoria WHERE id_categoria = :id_categoria AND status_registro = :status_registro ORDER BY id DESC");
		$stSubcategoria->execute(array("id_categoria" => $categoria['id'], "status_registro" => "A"));
		$qrySubcategoria = $stSubcategoria->fetchAll();

		if(count($qrySubcategoria)) {
		?>
		<ul>
			<?php
			foreach ($qrySubcategoria as $subcategoria) {
			?>
			<li><a href="#"><?= $subcategoria['descricao'] ?></a>
				<?php
				if ($cliente == "12103"){
					$stArquivo = $conn->prepare("SELECT * FROM licitacao_realizada WHERE id_subcategoria = :id_subcategoria AND status_registro = :status_registro ORDER BY titulo ASC");
					$stArquivo->execute(array("id_subcategoria" => $subcategoria['id'], "status_registro" => "A"));
					$qryArquivo = $stArquivo->fetchAll();
				}else {
					$stArquivo = $conn->prepare("SELECT * FROM licitacao_realizada WHERE id_subcategoria = :id_subcategoria AND status_registro = :status_registro ORDER BY id,titulo ASC");
					$stArquivo->execute(array("id_subcategoria" => $subcategoria['id'], "status_registro" => "A"));
					$qryArquivo = $stArquivo->fetchAll();
				}
				if(count($qryArquivo)) {
				?>
				<ul>
					<?php
					foreach($qryArquivo as $arquivo) {

						if($arquivo['arquivo'] != "") {

							$linkDocumento = $CAMINHOARQ . "/" . $arquivo['arquivo'];
							$imagem = "glyphicon-cloud-download";
							$titulo = empty($arquivo['titulo']) ? "Anexo" : $arquivo['titulo'];

						} else if($arquivo['link'] != "") {

							$linkDocumento = $arquivo['link'];
							$imagem = "glyphicon-globe";
							$titulo = empty($arquivo['titulo']) ? "Link" : $arquivo['titulo'];
						}
					?>
					<li><a href="<?=$linkDocumento ?>" target="_blank"><i class="glyphicon <?= $imagem ?>"></i> <?= $titulo ?></a></li>
					<?php } ?>
				</ul>
				<?php } ?>
			</li>
			<?php } ?>
		</ul>
		<?php } ?>
	</li>
	<?php } ?>
</ul>
<?php
}

$atualizacao = atualizacao("licitacao_realizada", $cliente, $conn);
if($atualizacao != "") {
?>
<p class="text-right"><small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small></p>
<?php
}

