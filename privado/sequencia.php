<?php

$seq = substr($_GET['sessao'], -4, 2);



$arraySequencia = array(



	"00" => "inicio.php",

	"12" => "busca.php",

	"01" => "sobre.php",

	"02" => "acesso_informacao.php",

	"03" => "perguntas/perguntas.php",

	"cl" => "lista_cliente.php",



	//CONTATO

	"ct" => "contato/contato.php",



	//ENDERECOS OFICIAIS

	"16" => "endereco_oficial/endereco.php",



	//LINKS

	"a2" => "links/links.php",

	//EMAIL
	"e2" => "emails/emails.php",

	//DOWNLOADS

	"dw" => "downloads/downloads.php",

	//Relatorio Viagem Saude

	"rl" => "relatorioViagem/relatorioViagem.php",

	//Chamamento Publico

	"cm" => "chamamentoPublico/chamamentoPublico.php",


	//PLANO DIRETOR

	"pd" => "plano_diretor/plano_diretor.php",

	"pz" => "plano_diretor/visualiza_artigo.php",



	//OUVIDORIA

	"uv" => "ouvidoria/ouvidoria.php",

	"u2" => "ouvidoria/ouvidoria2.php",

	"op" => "ouvidoria/visualiza_protocolo.php",



	//SISTEMA DE GESTAO

	"a8" => ($cliente == '12113' ? "ouvidoria/ouvidoria.php" : "sistema_gestao/login.php"),
	// "a8" => "sistema_gestao/login.php",

	"a9" => "sistema_gestao/protocolo/form_protocolo.php",

	"b0" => "sistema_gestao/protocolo/visualiza_protocolo.php",

	"b2" => "sistema_gestao/ouvidoria/form_ouvidoria.php",

	"b3" => "sistema_gestao/ouvidoria/visualiza_solicitacao.php",

	"hy" => "sistema_gestao/ouvidoria/realiza_solicitacao.php",



	//DIARIO OFICIAL

	"d1" => "diario/diario.php",



	//AUDIENCIA PUBLICA

	"09" => "audiencia_publica/audiencia_publica.php",

	"56" => "audiencia_publica/audiencia_publica_visualiza.php",



	//CONVENIO

	"10" => "convenio/convenio.php",



	//RECEITAS

	"rc" => "receita/receita.php",



	//DESPESAS

	"dp" => "despesa/despesa.php",



	//PLANO DE CONTAS

	"t4" => "plano_conta/plano_conta.php",



	//ORGANOGRAMA

	"08" => "organograma/organograma.php",

	"w8" => "organograma/organograma_visualiza.php",



	//TRANSPARENCIA ONLINE

	"07" => "transparencia_online/transparencia_online.php",

	"43" => "transparencia_online/transparencia_online_visualiza.php",



	//SERVIDOR

	"yy" => "servidor/servidor.php",

	"f2" => "servidor/servidor_visualiza.php",



	//VEREADOR

	"vv" => "vereador/vereador.php",



	//NFE

	"nf" => "nfe/nfe.php",



	//LICITACAO

	"3m" => ($cliente == '1073' ? "licitacao/licitacao2.php" : "licitacao/licitacao.php"),

	"3k" => "licitacao/licitacao_lista.php",

	"ld" => "licitacao/login.php",

	"8u" => "licitacao/alterar_cadastro.php",

	"lc" => ($cliente == '12101' ? "licitacao/licitacao_download_paranavai.php" : "licitacao/licitacao_download.php"),

	"po" => "licitacao/licitacao_video.php",

	"yw" => "licitacao/licitacao_aviso.php",

	"7w" => "licitacao/licitacao_realizada.php",

	"zl" => "licitacao/licitacao_cadastro.php",


	//CONCURSO

	"cs" => "concurso/concurso.php",

	"c3" => "concurso/inscricao.php",

	//CONCURSO PSS

	"c8" => "pss/lista.php",
	"c7" => "pss/login.php",

	//LEGISLACAO

	"lj" => "legislacao/legislacao.php",

	//teste
	"ly" => "legislacao/legislacao_teste.php",



	//CONTRATO

	"d2" => "contrato/contrato.php",



	//DIARIAS

	"d3" => "diaria/diaria.php",

	"d4" => "diaria/diaria_visualiza.php",



	//OUTROS DOCUMENTOS

	"od" => "outros_documentos/lista.php",



	//ATOS OFICIAIS

	"ai" => "ato/ato.php",



	//PATRIMONIO

	"ov" => "patrimonio/patrimonio.php",



	//REEMBOLSO

	"rb" => "reembolso/reembolso.php",



	//PERCENTUAL DE APLICACAO NA SAUDE

	"as" => "percentual_aplicacao_saude/lista.php",



	//CESSAO / DOACAO / PERMUTA DE BENS

	"cp" => "cessao/lista.php",



	//RELATORIO DE EXECUCAO ORCAMENTARIA

	"eo" => "relatorio_execucacao_orcamentaria/lista.php",



	//OPERACOES FINANCEIRAS

	"of" => "operacao_financeira/lista.php",



	//CONTROLE DE ESTOQUE

	"ce" => "estoque/lista.php",

	//CONTROLE DE ESTOQUE SAUDE
	"ws" => "estoque_saude/lista.php",


	//CARTAO CORPORATIVO

	"cc" => "cartao_corporativo/lista.php",



	//JUSTIFICATIVA DE CONTRATACAO DIRETA

	"jd" => "justificativa_contratacao_direta/lista.php",



	//AQUISICAO DE PASSAGENS AEREAS

	"pa" => "passagem_aerea/lista.php",



	//MOVIMENTACAO DOS FUNDOS

	"mf" => "movimentacao_fundo/lista.php",



	//PERCENTUAL DE APLICACAO NA EDUCACAO

	"ae" => "percentual_aplicacao_educacao/lista.php",



	//RELATORIO DE GESTAO FISCAL

	"rf" => "relatorio_gestao_fiscal/lista.php",



	//EXECUCAO ORCAMENTARIA EM TEMPO REAL

	"et" => "execucao_orcamentaria/lista.php",



	//EXTRATO DE CONTA UNICA

	"cu" => "extrato_conta_unica/lista.php",



	//ADIANTAMENTOS

	"ad" => "adiantamento/lista.php",



	//NOTIFICACOES

	"nt" => "notificacao/lista.php",



	//REGIMENTO INTERNO

	"rg" => "regimento/lista.php",



	//REPASSES / TRANSFERENCIAS

	"rt" => "repasse_transferencia/lista.php",



	//BOLSA FAMILIA

	"bf" => "bolsa_familia/lista.php",



	//TRANSFERENCIAS VOLUNTARIAS

	"tv" => "transferencia_voluntaria/lista.php",



	//CONTROLE INTERNO

	"ci" => "controle_interno/lista.php",



	//DOWNLOAD FORMULARIOS DE ATENDIMENTO

	"fa" => "formulario_atendimento/lista.php",



	//RELATORIOS ESTATISTICOS DE ATENDIMENTO

	"ra" => "relatorio_atendimento/lista.php",



	//SIC CIDADAO

	"sc" => "sic_cidadao/lista.php",



	//ATA DE REGISTRO DE PRECOS
	"ap" => ($cliente == '12101' ? "ata_registro_preco/lista_paranavai.php" : "ata_registro_preco/lista.php"),
	"al" => "ata_registro_preco/login.php",
	"rr" => "ata_registro_preco/alterar_cadastro.php",
	"rj" => "ata_registro_preco/download.php",



	//PRESTACAO DE CONTAS

	"pc" => "prestacao_contas/lista.php",



	//ANEXOS DA LEI 4320/64

	"l4" => "lei_4320/lista.php",



	//LEI DE RESPONSABILIDADE FISCAL

	"lf" => "lei_reponsabilidade_fiscal/lista.php",



	//COMPRA DIRETA

	"cd" => "compra_direta/lista.php",



	//FROTA DE VEICULOS

	"fv" => "veiculo/lista.php",



	//LICITACOES NA INTEGRA

	"li" => "licitacao_integra/lista.php",



	//CODIGO TRIBUTARIO

	"cr" => "codigo_tributario/lista.php",



	//ESTATUTO DO SERVIDOR

	"es" => "estatuto_servidor/lista.php",



	//INSTRUCAO NORMATIVA

	"in" => "instrucao_normativa/lista.php",

	"ik" => "instrucao_normativa_novo/lista.php",



	//RPPS - FUNDO DE PREVIDENCIA

	"rp" => "rpps/lista.php",



	//EXTRATO DE CONTAS

	"ec" => "extrato_conta/lista.php",



	//SERVIDORES CEDIDOS

	"sd" => "servidor_cedido/lista.php",



	//SERVIDORES TEMPORARIOS

	"st" => "servidor_temporario/lista.php",



	//RECURSOS DA EDUCACAO

	"re" => "recurso_educacao/lista.php",



	//CREDORES

	"co" => "credor/lista.php",



	//PLANO MUNICIPAL DE EDUCACAO

	"pe" => "plano_educacao/lista.php",



	//PLANO MUNICIPAL DE HABITACAO

	"ph" => "plano_habitacao/lista.php",



    //AJUDA DE CUSTOS

	"ac" => "ajuda_custo/lista.php",



    //PROGRAMAS E ACOES

	"pr" => "programa_acao/lista.php",



    //VERBAS DE GABINETE

	"vb" => "verba_gabinete/lista.php",



    //JURIDICO

	"jr" => "juridico/lista.php",



    //SERVIDORES RECEBIDOS

	"sr" => "servidor_recebido/lista.php",



	//RELATORIO SAUDE

	"rs" => "relatorio_saude/lista.php",



	//VERBAS DE REPRESENTACAO DE GABINETE

	"mv" => "verba_representacao/lista.php",



	//DIGITALIZACAO

	 "dl" => ($cliente == '65' ? "licitacao/licitacao.php" : "digitalizacao/lista.php"), 

	"dv" => "digitalizacao/visualiza.php",



	//DEMONSTRATIVOS CONTABEIS

	"gs" => "demonstrativos_contabeis/sessao.php",

	"y1" => "demonstrativos_contabeis/lista.php",



	//PORTAIS DE NOMEACOES

	"ps" => "portarias_nomeacoes/lista.php",



	//ATOS DA SECRETARIAS

	"w9" => "atos_secretarias/sessao.php",

	"hf" => "atos_secretarias/lista.php",



    //PRECATORIO

    "pt" => "precatorio/precatorio.php",



    // EXTRATOS BANCARIOS

    "eb" => "extrato_bancario/lista.php",



    // DOCUMENTOS FICAIS

    "df" => "documentos_fiscais/lista.php",



    // PLANO DE CARREIRA

    "pl" => "plano_carreira/lista.php",



    // DESTINAÇÃO DOS RECURSOS (FUNDEB)

    "fd" => "fundeb/lista.php",



    // REMUNERAÇAO AGENTES PÚBLICOS

    "ag" => "rem_agentes_publicos/lista.php",



    // QUADRO FUNCIONAL

    "qf" => "quadro_funcional/lista.php",



    // RESSARCIMENTO DE COMBUSTIVEIS

    "ro" => "res_combustivel/lista.php",



    // RESSARCIMENTO DE COMBUSTIVEIS

    "la" => "licitacoes_abertas/lista.php",



	// CONTROLE DE DISTRIBUIÇÃO DE MEDICAMENTOS

	"8j" => "controle_medicamentos/lista.php",



	// CONTROLE DE LICITAÇÃO EM ANDAMENTO

	"5a" => "licitacao_andamento/lista.php",



	// CONTROLE DE LICITAÇÃO REALIZADA

	"3r" => "licitacao_realizada/lista.php",



	// CONTROLE AVISO LICITAÇÃO

	"wk" => "aviso_licitacao/lista.php",



	// CONTROLE DISPENSA LICITAÇÃO

	"s4" => "dispensa_licitacao/lista.php",



	// CONTROLE INEXIGIBILIDADE LICITAÇÃO

	"g3" => "inexigibilidade_licitacao/lista.php",



	// CONTROLE SECRETARIA EXECUTIVA

	"jw" => "secretaria_executiva/lista.php",



	// CONTRACHEQUE

	"ch" => "contracheque/lista.php",



	// PROJETOS DE LEIS

	"k9" => "projetos_leis/lista.php",



  // SALÁRIO DE SERVIDORES

  "ss" => "servidor_salario/lista.php",

   // LAUDOS TECNICOS

  "lt" => "laudos_tecnicos/lista.php",

  //ASSISTÊNCIA SOCIAL

  "a4" => "assistencia_social/lista.php",


	//INTERFERÊNCIAS FINANCEIRAS

	"if" => "interferencia_financeira/lista.php",



	//SERVIDORES EM FÉRIAS OU DE LICENÇA

	"fl" => "servidor_ferias_licenca/lista.php",

	//MINISTERIO PUBLICO FEDERAL

	"pf" => "ministerio_pf/lista.php",

	//PLANO DE GERENCIAMENTO DE RESÍDUOS

	"pn" => "plano_residuo/plano_residuo.php",
	"pv" => "plano_residuo/visualiza_artigo.php",
	
	//INVESTIMENTOS

	"iv" => "investimento/lista.php",

	//CÁLCULO ATUARIAL
	
	"ca" => "calculo_atuarial/lista.php",

	//CENSO PREVIDENCIARIO
	
	"ev" => "censo_previdenciario/lista.php",


	//LISTA DE ESPERA CRECHES
 	"el" => "lista_creche/lista.php",


 	//Quantitativo Cargos
	"qc" => "quantitativo_cargos/lista.php",

	//Servidores Ativos
	"sa" => "servidores_ativos/lista.php",

	//Servidores Inativos
	"si" => "servidores_inativos/lista.php",

	//acoes programas do governo
	"af" => "acoes_programas/lista.php",

	//Servidores comissionados
	"so" => "servidores_comissionados/lista.php",

	//Servidores efetivos
	"se" => "servidores_efetivos/lista.php",

	//Licenciamento Ambiental
	"lh" => "licenciamento_ambiental/lista.php",

	//Galeria portal Obras
	"go" => "portal_obras/lista.php",

	//acoes programas
	"ah" => "acoes_programas/lista.php",

	//Leis e Atos
	"tl" => "leis_atos/lista.php",

	//Estagiarios
	"e1" => "estagiarios/lista.php",

	//Plano municipal de saude
	"pm" => "plano_saude/lista.php",

	//Cardápio de Alimentação Escolar
	"aa" => "cardapio_escolar/lista.php",

	//Transporte Escolar
	"te" => "transporte_escolar/lista.php",

	//Empenhos
	"em" => "empenhos/lista.php",

	//Contas Apresentadas Pelo Chefe do Poder 
	"ep" => "contas_chefe_poder_executivo/lista.php",

	//Contratação Direta
	"cn" => "contratacao_direta/lista.php",

	//Bolsa Atleta
	"ba" => "bolsa_atleta/bolsa_atleta.php",
	
	//Julgamento de Contas
	"jc" => "julgamento_contas/lista.php",

	//Consórcio
	"cq" => "consorcio/lista.php",

	//Tabela de vencimento
	"td" => "tabela_vencimento/lista.php",

	//Lista de espera do SUS
	"ls" => "lista_espera_sus/lista.php",

	//Lista de espera Consulta Esecializada
	"le" => "lista_espera_consulta/lista.php",

	//Logística de Distribuição
	"l2" => "logistica_distribuicao/lista.php",

	//Covid 19
	"c1" => "covid19/lista.php"

);



if (!isset($_GET['sessao']) || $seq == "00") {



	include "../privado/inicio.php";



} else if (array_key_exists($seq, $arraySequencia)) {



	if(file_exists("../privado/" . $arraySequencia[$seq])) {


		require_once "../privado/" . $arraySequencia[$seq];


	} else {



		include "../privado/404.php";

	}



} else {



	include "../privado/404.php";

}
