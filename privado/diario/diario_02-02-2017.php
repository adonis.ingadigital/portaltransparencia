<h2>Di&aacute;rio Oficial Eletr&ocirc;nico</h2>
<ol class="breadcrumb">
	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
	<li class="active">Di&aacute;rio Oficial</li>
</ol>

<?php
$novoCliente = empty($_GET['nc']) ? $cliente : secure($_GET['nc']);

if($novoCliente == "11992") {
?>
<p>Criado de acordo com a Lei Municipal 1205/2013.</p>
<?php } ?>

<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#form-diario" aria-expanded="false" aria-controls="form-diario">
	<i class="glyphicon glyphicon-search"></i> Busca Avan&ccedil;ada
</button>
<?php if($novoCliente == "43") { ?>
<a href="https://drive.google.com/folderview?id=0B0c1rvwCRXmpaVJDSUV5Zlpfclk&usp=sharing" class="btn btn-success" target="_blank"><i class="glyphicon glyphicon-list"></i> &Iacute;ndice de Publica&ccedil;&otilde;es</a>
<?php } ?>
<p class="clearfix"></p>

<form class="form-horizontal collapse row" id="form-diario" action="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$diarioLista.$complemento); ?>&nc=<?= $novoCliente ?>" method="post">
	<div class="col-sm-7 col-md-8">
		<div class="form-group">
			<label for="data_documento" class="col-sm-4 col-md-2 control-label">Data Publica&ccedil;&atilde;o</label>
			<div class="col-sm-8 col-md-10">
				<input type="text" name="data_documento" id="data_documento" class="form-control data" value="<?= $_POST['data_documento'] ?>" placeholder="Informe a data da publica&ccedil;&atilde;o do documento...">
			</div>
		</div>
		<div class="form-group">
			<label for="id_ano" class="col-sm-4 col-md-2 control-label">Ano</label>
			<div class="col-sm-8 col-md-10">
				<select name="id_ano" id="id_ano" class="form-control">
					<option value="">Selecione o Ano</option>
					<?php
					$stAno = $conn->prepare("SELECT * FROM di_ano WHERE id_cliente = :id_cliente AND status_registro = :status_registro ORDER BY ano");
					$stAno->execute(array("id_cliente" => $novoCliente, "status_registro" => "A"));
					$qryAno = $stAno->fetchAll();

					if(count($qryAno)) {

						foreach ($qryAno as $ano) {

							echo "<option value='$ano[id]'>$ano[ano]</option>";
						}
					}
					?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label for="id_categoria" class="col-sm-4 col-md-2 control-label">Categoria</label>
			<div class="col-sm-8 col-md-10">
				<p class="carregando_categoria form-control-static hidden">Aguarde, carregando...</p>
				<select name="id_categoria" id="id_categoria" class="form-control">
					<option value="">Selecione a Categoria</option>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label for="id_subcategoria" class="col-sm-4 col-md-2 control-label">Subcategoria</label>
			<div class="col-sm-8 col-md-10">
				<p class="carregando_subcategoria form-control-static hidden">Aguarde, carregando...</p>
				<select name="id_subcategoria" id="id_subcategoria" class="form-control">
					<option value="">Selecione a Subcategoria</option>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label for="palavra_chave" class="col-sm-4 col-md-2 control-label">Palavra-chave</label>
			<div class="col-sm-8 col-md-10">
				<input type="text" name="palavra_chave" id="palavra_chave" class="form-control" value="<?= $_POST['palavra_chave'] ?>" placeholder="Informe um trecho ou uma palavra constante no documento...">
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-4 col-md-offset-2 col-sm-8 col-md-10">
				<input type="hidden" name="acao" value="cadastrar">
				<button type="reset" class="btn btn-danger">Limpar</button>
				<button type="submit" class="btn btn-primary">Enviar</button>
			</div>
		</div>
	</div>
	<div class="col-sm-5 col-md-4">
		<div class="calendario_diario text-center">
  			<iframe src="http://www.controlemunicipal.com.br/site/diario/calendario_diario.php?data=<?=(empty($_REQUEST['data_documento']) ? date('d/m/Y') : $_REQUEST['data_documento']) ?>&id_cliente=<?= $novoCliente ?>&url=<?=urlencode($CAMINHO) ?>" width="251" height="255" frameborder="0" marginheight="0" marginwidth="0" scrolling="no" allowTransparency="true"></iframe>
  		</div>
	</div>
</form>

<p class="clearfix"></p>
<p><strong>Observa&ccedil;&atilde;o: </strong>Os documentos s&atilde;o assinados digitalmente com o certificado digital. Para visualizar a assinatura corretamente, &eacute; necess&aacute;rio o programa Adobe Reader.</p>
<?php
$pagina = isset($_REQUEST['pagina']) ? $_REQUEST['pagina'] : 1;
$max = 10;
$inicio = $max * ($pagina - 1);

$link = $CAMINHO . "/index.php?sessao=" . $_GET['sessao'];
$caminhoArquivo = "http://www.controlemunicipal.com.br/inga/sistema/arquivos/diario/$novoCliente";

$sql = "SELECT di_arquivo.*,
			   di_ano.ano,
			   di_categoria.categoria,
			   di_subcategoria.subcategoria
		  FROM di_arquivo
	 LEFT JOIN di_ano ON di_arquivo.id_ano = di_ano.id
	 LEFT JOIN di_categoria ON di_arquivo.id_categoria = di_categoria.id
	 LEFT JOIN di_subcategoria ON di_arquivo.id_subcategoria = di_subcategoria.id
	 LEFT JOIN cliente ON di_arquivo.id_cliente = cliente.id
		 WHERE di_arquivo.id_cliente = :id_cliente
		   AND di_arquivo.status_registro = :status_registro ";

$vetor["id_cliente"] = $novoCliente;
$vetor["status_registro"] = "A";

if($_REQUEST['data_documento'] != "") {

	$sql .= "AND DATE(di_arquivo.data_insercao) = :data_documento ";
	$vetor["data_documento"] = formata_data_banco($_REQUEST['data_documento']);
	$link .= "&data_documento=" . $_REQUEST['data_documento'];
}

if($_REQUEST['id_ano'] != "") {

	$sql .= "AND di_arquivo.id_ano = :id_ano ";
	$vetor["id_ano"] = $_REQUEST['id_ano'];
	$link .= "&id_ano=" . $_REQUEST['id_ano'];

}

if($_REQUEST['id_categoria'] != "") {

	$sql .= "AND di_arquivo.id_categoria = :id_categoria ";
	$vetor["id_categoria"] = $_REQUEST['id_categoria'];
	$link .= "&id_categoria=" . $_REQUEST['id_categoria'];

}

if($_REQUEST['id_subcategoria'] != "") {

	$sql .= "AND di_arquivo.id_subcategoria = :id_subcategoria ";
	$vetor["id_subcategoria"] = $_REQUEST['id_subcategoria'];
	$link .= "&id_subcategoria=" . $_REQUEST['id_subcategoria'];

}

if($_REQUEST['palavra_chave'] != "") {

	$sql .= "AND (di_arquivo.descricao LIKE :palavra_chave OR di_arquivo.indexacao LIKE :palavra_chave) ";
	$vetor["palavra_chave"] = "%" . $_REQUEST['palavra_chave'] . "%";
	$link .= "&palavra_chave=" . $_REQUEST['palavra_chave'];
}

$stLinha = $conn->prepare($sql);
$stLinha->execute($vetor);
$qryLinha = $stLinha->fetchAll();
$totalLinha = count($qryLinha);

if(empty($configuracaoTransparencia['ordem_diario'])) $sql .= "ORDER BY di_arquivo.sequencia DESC, di_arquivo.data_insercao DESC ";
else $sql .= "ORDER BY $configuracaoTransparencia[ordem_diario] ";

$sql .= "LIMIT $inicio, $max";

$stDiario = $conn->prepare($sql);
$stDiario->execute($vetor);
$qryDiario = $stDiario->fetchAll();

if(count($qryDiario)) {
?>
<div class="table-responsive">
	<table class="table table-striped table-hover table-bordered table-condensed">
		<tr>
			<th>&nbsp;</th>
			<th>Data Publica&ccedil;&atilde;o</th>
			<?php if($novoCliente == "43" || $novoCliente == "11992") { ?><th>Edi&ccedil;&atilde;o</th><?php } ?>
			<th>Arquivo</th>
			<th>Categoria</th>
			<th>Subcategoria</th>
		</tr>
		<?php foreach ($qryDiario as $diario) { ?>
		<tr>
			<td>
				<a href="http://www.controlemunicipal.com.br/site/diario/publicacao.php?id=<?=$diario['id'] ?>&id_cliente=<?=$novoCliente ?>" class="btn btn-primary" data-toggle="tooltip" data-placement="right" title="Visualizar" target="_blank">
					<i class="glyphicon glyphicon-search"></i>
				</a>
			</td>
			<td class="text-right"><?= formata_data_hora($diario['data_insercao']) ?></td>
			<?php if($novoCliente == "43" || $novoCliente == "11992") { ?><td><?= $diario['sequencia'] ?></td><?php } ?>
			<td><?= $diario['descricao'] ?></td>
			<td><?= $diario['categoria'] ?></td>
			<td><?= $diario['subcategoria'] ?></td>
		</tr>
		<?php } ?>
	</table>
</div>
<?php
$menos = $pagina - 1;
$mais = $pagina + 1;
$paginas = ceil($totalLinha / $max);
if($paginas > 1) {
?>
<nav>
	<ul class="pagination">
		<?php if($pagina == 1) { ?>
	    <li class="disabled"><a href="#" aria-label="Anterior"><span aria-hidden="true">&laquo;</span></a></li>
	    <?php } else { ?>
	    <li><a href="<?= $link ?>&pagina=<?= $menos ?>" aria-label="Anterior"><span aria-hidden="true">&laquo;</span></a></li>
	    <?php
		}

	    if(($pagina - 4) < 1) $anterior = 1;
	    else $anterior = $pagina - 4;

	    if(($pagina + 4) > $paginas) $posterior = $paginas;
	    else $posterior = $pagina + 4;

	    for($i = $anterior; $i <= $posterior; $i++) {

	    	if($i != $pagina) {
	    ?>
	    	<li><a href="<?= $link ?>&pagina=<?= $i ?>"><?= $i ?></a></li>
	    	<?php } else { ?>
	    	<li class="active"><a href="#"><?= $i ?><span class="sr-only">(current)</span></a></li>
	   	<?php
	    	}
	    }

	    if($mais <= $paginas) {
	   	?>
	   	<li><a href="<?= $link ?>&pagina=<?= $mais ?>" aria-label="Pr&oacute;ximo"><span aria-hidden="true">&raquo;</span></a></li>
	   	<?php } ?>
	</ul>
</nav>
<?php } ?>

<div class="well">
	<h3>Certifica&ccedil;&atilde;o Digital</h3>
	<img src="<?= $CAMINHO ?>/images/icpbrasil.png" alt="Certificado Digital" class="pull-left">
	A Certifica&ccedil;&atilde;o Digital &eacute; um conjunto de tecnologias e procedimentos que visam garantir a validade de um Certifica&ccedil;&atilde;o Digital, a ICP-BRASIL &eacute; a infraestrutura Legal Brasileira para Certifica&ccedil;&atilde;o Digital, de acordo com a Medida Provis&oacute;ria 2200 que estabelece e normatiza estas condi&ccedil;&otilde;es.<br />Sendo assim, s&atilde;o considerados legalmente v&aacute;lidos, no &acirc;mbito nacional, apenas os certificados emitidos por autoridades credenciadas junto &agrave; ICP-BRASIL. Com o uso de Certificados Digitais &eacute; poss&iacute;vel anexar assinaturas digitais em arquivos digitais e assim atribuir-lhe o status de documento v&aacute;lido e original tamb&eacute;m de acordo com a Lei 11.419.
	<p class="clearfix"></p>
</div>

<?php } else { ?>
<h4>Nenhum registro encontrado.</h4>
<?php
}

$atualizacao = atualizacao("cadastro/arquivo", $cliente, $conn);
if($atualizacao != "") {
?>
<p class="text-right"><small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small></p>
<?php
}