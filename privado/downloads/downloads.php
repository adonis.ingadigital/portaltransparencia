<? if($cliente == '12041') {?>
<h2>Documentos Administrativos</h2>
<ol class="breadcrumb">
  <li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
  <li class="active">Documentos Administrativos</li>
</ol>
<?}else{?>
<h2>Downloads</h2>
<ol class="breadcrumb">
  <li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
  <li class="active">Downloads</li>
</ol>
<?}

if($cliente === "1093"){ ?>
<div style="text-align: center;">
  <a href="<?=$CAMINHOCMGERAL ?>/licitacao/esProposta2016candidodeabreu.exe" target="_blank"><img
      src="<?=$CAMINHOCMGERAL ?>/images/esproposta.jpg" class="imagem" /></a>
</div>
<?php } ?>

<?php
$stCategoria = $conn->prepare("SELECT * FROM download_categoria WHERE id_cliente = :id_cliente AND status_registro = :status_registro ORDER BY descricao DESC");
$stCategoria->execute(array("id_cliente" => $cliente, "status_registro" => "A"));
$qryCategoria = $stCategoria->fetchAll();

if(count($qryCategoria)) {
?>
<ul class="treeview">
  <?php
	foreach ($qryCategoria as $categoria) {
	?>
  <li><a href="#"><?= $categoria['descricao'] ?></a>
    <?php
		$stSubcategoria = $conn->prepare("SELECT * FROM download_subcategoria WHERE id_categoria = :id_categoria AND status_registro = :status_registro ORDER BY descricao asc");
		$stSubcategoria->execute(array("id_categoria" => $categoria['id'], "status_registro" => "A"));
		$qrySubcategoria = $stSubcategoria->fetchAll();

		if(count($qrySubcategoria)) {
		?>
    <ul>
      <?php
			foreach ($qrySubcategoria as $subcategoria) {
			?>
      <li><a href="#"><?= $subcategoria['descricao'] ?></a>
        <?php
				$stArquivo = $conn->prepare("SELECT * FROM download WHERE id_subcategoria = :id_subcategoria AND status_registro = :status_registro ORDER BY descricao asc");
				$stArquivo->execute(array("id_subcategoria" => $subcategoria['id'], "status_registro" => "A"));
				$qryArquivo = $stArquivo->fetchAll();

				if(count($qryArquivo)) {
				?>
        <ul>
          <?php
					foreach($qryArquivo as $arquivo) {
					  if($cliente == 43){
                          $caminhoArquivo = $CAMINHOCMGERAL."/licitacao/download.php?cliente=$cliente&arquivo=".$arquivo['arquivo'];
                      }else{
                         $caminhoArquivo = $CAMINHOARQ."/".$arquivo['arquivo'];
                      }?>
          <li><a href="<?=$caminhoArquivo?>" target="_blank"><i class="glyphicon glyphicon-cloud-download"></i>
              <?= $arquivo['descricao'] ?></a></li>
          <?php } ?>
        </ul>
        <?php } ?>
      </li>
      <?php } ?>
    </ul>
    <?php } ?>

    <?php
		$stArquivo = $conn->prepare("SELECT * FROM download WHERE id_categoria = :id_categoria AND (id_subcategoria IS NULL OR id_subcategoria = '') AND status_registro = :status_registro ORDER BY descricao DESC");
		$stArquivo->execute(array("id_categoria" => $categoria['id'], "status_registro" => "A"));
		$qryArquivo = $stArquivo->fetchAll();

		if(count($qryArquivo)) {
		?>
    <ul>
      <?php
			foreach($qryArquivo as $arquivo) {
                if($cliente == 43){
                    $caminhoArquivo = $CAMINHOCMGERAL."/licitacao/download.php?cliente=$cliente&arquivo=".$arquivo['arquivo'];
                }else{
                    $caminhoArquivo = $CAMINHOARQ."/".$arquivo['arquivo'];
                }
			?>
      <li><a href="<?=$caminhoArquivo?>" target="_blank"><i class="glyphicon glyphicon-cloud-download"></i>
          <?= $arquivo['descricao'] ?></a></li>
      <?php } ?>
    </ul>
    <?php } ?>


  </li>
  <?php } ?>
</ul>
<?php
}

$atualizacao = atualizacao("download", $cliente, $conn);
if($atualizacao != "") {
?>
<p class="text-right"><small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima
      atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small></p>
<?php
}