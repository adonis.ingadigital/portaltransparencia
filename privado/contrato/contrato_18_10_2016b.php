<h2>Contratos e Aditivos</h2>
<?php $novoCliente = empty($_GET['nc']) ? $cliente : secure($_GET['nc']); ?>
<ol class="breadcrumb">
	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
	<li class="active">Contratos e Aditivos</li>
</ol>

<?php
$stAta = $conn->prepare("SELECT id FROM cliente_configuracao_transparencia_icone_cliente WHERE id_cliente = :id_cliente AND id_icone = :id_icone");
$stAta->execute(array("id_cliente" => $novoCliente, "id_icone" => 52));
$confAta = $stAta->fetchAll();
?>

<?php if($novoCliente == "12126") { ?>
	<p>
		<a href="http://177.92.23.218:7474/esportal/slclicitacao.load.logic" target="_blank"><b>INFORMATIVO:</b> O Poder Legislativo Municipal de Miraselva, mantém suas atividades administrativas centralizadas ao Poder Executivo Municipal, desta forma, as demais licitações públicas encontram-se no Portal de Transparência do Município de Miraselva.</a>
	</p>
<?php }?>

<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#form-contrato" aria-expanded="false" aria-controls="form-contrato">
	<i class="glyphicon glyphicon-search"></i> Pesquisar
</button>

<?php
if(count($confAta)) {
	if(empty($configuracaoTransparencia['caminho_ata_registro_preco'])) {

		$linkAta = $CAMINHO . "/index.php?sessao=$sequencia" . $ataRegistroPreco . "$complemento&nc= $novoCliente" ;

	} else {

		$linkAta = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=ata_registro_preco&redir=link&nc= $novoCliente";

	}
?>
<a href="<?= $linkAta ?>" class="btn btn-success"><i class="glyphicon glyphicon-list"></i> Ata de Registro de Pre&ccedil;os</a>
<?php } ?>

<?php if($novoCliente == "46") { ?>
<a href="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$outrosDocumentos.$complemento); ?>&nc=<?= $novoCliente ?>" class="btn btn-success"><i class="glyphicon glyphicon-list"></i> Relat&oacute;rio de Contratos de 2013 &agrave; 2015</a>
<?php } ?>

<?php if($novoCliente == "1162") { ?>
<a href="https://e-gov.betha.com.br/transparencia/con_contratos.faces?mun=BeuVrJ70ho2N2-8TCr7bai1IO1udU-8S" class="btn btn-success" target="_blank"><i class="glyphicon glyphicon-list"></i> Relat&oacute;rios</a>
<?php } ?>

<?php if($novoCliente == "48") { ?>
<a href="http://187.49.83.12:7474/esportal/slccontrato.load.logic" class="btn btn-success" target="_blank"><i class="glyphicon glyphicon-list"></i> Relat&oacute;rios</a>
<?php } ?>

<form class="form-horizontal collapse" id="form-contrato" action="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$contrato.$complemento); ?>&nc=<?= $novoCliente ?>" method="post">
	<p class="clearfix">&nbsp;</p>
	<div class="form-group">
		<label for="contratado" class="col-sm-2 control-label">Contratado</label>
		<div class="col-sm-10">
			<input type="text" name="contratado" id="contratado" class="form-control" value="<?= $_POST['contratado'] ?>" placeholder="Informe o nome do contratado...">
		</div>
	</div>
	<div class="form-group">
		<label for="assinatura" class="col-sm-2 control-label">Data da Assinatura</label>
		<div class="col-sm-10">
			<input type="text" name="assinatura" id="assinatura" class="form-control data" value="<?= $_POST['assinatura'] ?>" placeholder="Informe a data da assinatura...">
		</div>
	</div>
	<div class="form-group">
		<label for="ano" class="col-sm-2 control-label">Ano</label>
		<div class="col-sm-10">
			<input type="text" name="ano" id="ano" class="form-control ano" value="<?= $_POST['ano'] ?>" placeholder="Informe o ano da assinatura...">
		</div>
	</div>
	<div class="form-group">
		<label for="objeto" class="col-sm-2 control-label">Objeto</label>
		<div class="col-sm-10">
			<input type="text" name="objeto" id="objeto" class="form-control" value="<?= $_POST['objeto'] ?>" placeholder="Informe um trecho do objeto...">
		</div>
	</div>
	<div class="form-group">
		<label for="numero" class="col-sm-2 control-label">N&uacute;mero</label>
		<div class="col-sm-10">
			<input type="text" name="numero" id="numero" class="form-control" value="<?= $_POST['numero'] ?>" placeholder="Informe o n&uacute;mero do contrato...">
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<input type="hidden" name="acao" value="cadastrar">
			<button type="reset" class="btn btn-danger">Limpar</button>
			<button type="submit" class="btn btn-primary">Enviar</button>
		</div>
	</div>
</form>

<?php
$pagina = isset($_REQUEST['pagina']) ? $_REQUEST['pagina'] : 1;
$max = 10;
$inicio = $max * ($pagina - 1);

$link = $CAMINHO . "/index.php?sessao=" . $_GET['sessao'];

$sql = "SELECT * FROM contrato_transparencia WHERE id_cliente = :id_cliente AND status_registro = :status_registro ";
$vetor["id_cliente"] = $novoCliente;
$vetor["status_registro"] = "A";


if($_REQUEST['contratado'] != "") {

	$sql .= "AND contratado LIKE :contratado ";
	$vetor["contratado"] = "%" . $_REQUEST['contratado'] . "%";
	$link .= "&contratado=" . $_REQUEST['contratado'];
}

if($_REQUEST['assinatura'] != "") {

	$sql .= "AND data_assinatura = :data_assinatura ";
	$vetor["data_assinatura"] = formata_data_banco($_REQUEST['assinatura']);
	$link .= "&data_assinatura=" . $_REQUEST['data_assinatura'];

}

if($_REQUEST['ano'] != "") {

	$sql .= "AND YEAR(data_assinatura) = :ano ";
	$vetor["ano"] = $_REQUEST['ano'];
	$link .= "&ano=" . $_REQUEST['ano'];
}

if($_REQUEST['numero'] != "") {

	$sql .= "AND numero LIKE :numero ";
	$vetor["numero"] = "%" . $_REQUEST['numero'] . "%";
	$link .= "&numero=" . $_REQUEST['numero'];
}

if($_REQUEST['objeto'] != "") {

	$sql .= "AND objeto LIKE :objeto ";
	$vetor["objeto"] = "%" . $_REQUEST['objeto'] . "%";
	$link .= "&objeto=" . $_REQUEST['objeto'];
}

$stLinha = $conn->prepare($sql);
$stLinha->execute($vetor);
$qryLinha = $stLinha->fetchAll();
$totalLinha = count($qryLinha);

if($novoCliente == "43")
	$sql .= "ORDER BY numero DESC, id DESC LIMIT $inicio, $max";
else
	$sql .= "ORDER BY data_assinatura DESC, id DESC LIMIT $inicio, $max";

$stContrato = $conn->prepare($sql);
$stContrato->execute($vetor);
$qryContrato = $stContrato->fetchAll();

if(count($qryContrato)){

?>
<p class="clearfix"></p>
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
	<?php foreach ($qryContrato as $contrato) { ?>
	<div class="panel panel-default">
	    <div class="panel-heading" role="tab" id="heading_<?= $contrato['id'] ?>">
	     	<h4 class="panel-title">
	        	<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?= $contrato['id'] ?>" aria-expanded="true" aria-controls="collapse_<?= $contrato['id'] ?>">
	          		<div class="row">
	          			<div class="col-sm-6"><i class="glyphicon glyphicon-triangle-right"></i> <strong>Contratado:</strong> <?= $contrato['contratado'] ?></div>
	          			<div class="col-sm-3"><strong>Processo:</strong> <?= $contrato['processo'] ?></div>
	          			<div class="col-sm-3"><strong>N&uacute;mero:</strong> <?= $contrato['numero'] ?></div>
	          		</div>
	        	</a>
	    	</h4>
	    </div>
		<div id="collapse_<?= $contrato['id'] ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_<?= $contrato['id'] ?>">
			<div class="panel-body">
				<?php if($contrato['cnpj'] != "") { ?><p><strong>CNPJ / CPF:</strong> <?= $contrato['cnpj'] ?></p><?php } ?>
				<p><strong>Domic&iacute;lio:</strong> <?= $contrato['domicilio'] ?></p>
				<p><strong>Data da Assinatura:</strong> <?= formata_data($contrato['data_assinatura']) ?></p>
				<p><strong>Vig&ecirc;ncia:</strong> <?= $contrato['vigencia'] ?></p>
				<p><strong>N&uacute;mero:</strong> <?= $contrato['numero'] ?></p>
				<?php if($contrato['modalidade'] != "") { ?><p><strong>Modalidade:</strong> <?= $contrato['modalidade'] ?></p><?php } ?>
				<p><strong>Valor Global:</strong> <?= $contrato['valor_global'] ?></p>
				<p><strong>Objeto:</strong> <?= verifica($contrato['objeto']) ?></p>
				<?php if($contrato['aditivos'] != "") { ?><p><strong>Aditivos:</strong> <?= $contrato['aditivos'] ?></p><?php } ?>

				<?php
				$stAnexo = $conn->prepare("SELECT * FROM contrato_transparencia_anexo WHERE id_artigo = :id_artigo ORDER BY ordem ASC, id DESC");
				$stAnexo->execute(array("id_artigo" => $contrato['id']));
				$qryAnexo = $stAnexo->fetchAll();

				if(count($qryAnexo)) {
				?>
				<div class="panel panel-primary">
					<div class="panel-heading">
				    	<h2 class="panel-title"><i class="glyphicon glyphicon-cloud-download"></i> Anexos</h2>
				  	</div>
					<div class="panel-body">
						<?php
						foreach ($qryAnexo as $anexo) {
						?>
						<p><strong><a href="<?= $CAMINHOARQ ?>/<?= $anexo['arquivo'] ?>" target="_blank"><i class="glyphicon glyphicon-cloud-download"></i> <?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?></a></strong></p>
						<?php } ?>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
	<?php } ?>
</div>

<?php
$menos = $pagina - 1;
$mais = $pagina + 1;
$paginas = ceil($totalLinha / $max);
if($paginas > 1) {
?>
<nav>
	<ul class="pagination">
		<?php if($pagina == 1) { ?>
	    <li class="disabled"><a href="#" aria-label="Anterior"><span aria-hidden="true">&laquo;</span></a></li>
	    <?php } else { ?>
	    <li><a href="<?= $link ?>&pagina=<?= $menos ?>" aria-label="Anterior"><span aria-hidden="true">&laquo;</span></a></li>
	    <?php
		}

	    if(($pagina - 4) < 1) $anterior = 1;
	    else $anterior = $pagina - 4;

	    if(($pagina + 4) > $paginas) $posterior = $paginas;
	    else $posterior = $pagina + 4;

	    for($i = $anterior; $i <= $posterior; $i++) {

	    	if($i != $pagina) {
	    ?>
	    	<li><a href="<?= $link ?>&pagina=<?= $i ?>"><?= $i ?></a></li>
	    	<?php } else { ?>
	    	<li class="active"><a href="#"><?= $i ?><span class="sr-only">(current)</span></a></li>
	   	<?php
	    	}
	    }

	    if($mais <= $paginas) {
	   	?>
	   	<li><a href="<?= $link ?>&pagina=<?= $mais ?>" aria-label="Pr&oacute;ximo"><span aria-hidden="true">&raquo;</span></a></li>
	   	<?php } ?>
	</ul>
</nav>
<?php } ?>

<?php } else { ?>
<h4>Nenhum registro encontrado.</h4>
<?php
}

$atualizacao = atualizacao("contratos", $novoCliente, $conn);
if($atualizacao != "") {
?>
<p class="text-right"><small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small></p>
<?php
}