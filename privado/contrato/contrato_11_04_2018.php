<style>
    .sub-item.active, .sub-item.active:focus, .sub-item.active:hover {
        background-color: #468ecb !important;
        border-color: #468ecb !important;
        color: #fff !important;
    }

    .sub-sub-item.active, .sub-sub-item.active:focus, .sub-sub-item.active:hover {
        background-color: #65a0d4 !important;
        border-color: #65a0d4 !important;
        color: #fff !important;
    }

    .list-group-item.active {
        border-radius: 0 !important;
    }
</style>
<h2>Contratos e Aditivos</h2>
<?php $novoCliente = empty($_GET['nc']) ? $cliente : secure($_GET['nc']); ?>
<ol class="breadcrumb">
    <li><a href="<?= $CAMINHO ?>">In&iacute;cio</a></li>
    <li class="active">Contratos e Aditivos</li>
</ol>

<?php

$sqlCategoria = "SELECT `contrato_transparencia`.`id_categoria`,
                                        `contrato_transparencia_categoria`.`descricao`,
                                        COUNT(`contrato_transparencia`.`id_categoria`) AS 'quantidade'
                                   FROM `contrato_transparencia`
                                   JOIN `contrato_transparencia_categoria`
                                     ON `contrato_transparencia`.`id_categoria` = `contrato_transparencia_categoria`.`id`
                                  WHERE `contrato_transparencia`.`id_cliente` = :id_cliente
                                    AND `contrato_transparencia`.`status_registro` = :status_registro
                               GROUP BY `contrato_transparencia`.`id_categoria`
                               ORDER BY `contrato_transparencia_categoria`.`descricao`";
$categoriaArgs = array(
    "id_cliente" => $novoCliente,
    "status_registro" => "A"
);

$stCategoria = $conn->prepare($sqlCategoria);
$stCategoria->execute($categoriaArgs);
$qryCategoria = $stCategoria->fetchAll();

if ($novoCliente != 33) {

    $stAta = $conn->prepare("SELECT id FROM cliente_configuracao_transparencia_icone_cliente WHERE id_cliente = :id_cliente AND id_icone = :id_icone");
    $stAta->execute(array("id_cliente" => $novoCliente, "id_icone" => 52));
    $confAta = $stAta->fetchAll();

    if ($novoCliente == "12126") { ?>
        <p>
            <a href="http://177.92.23.218:7474/esportal/slccontrato.load.logic" target="_blank">
                <b>INFORMATIVO:</b> O Poder Legislativo Municipal de Miraselva, mantém suas atividades administrativas
                centralizadas ao Poder Executivo Municipal. <br><br>

                Desta forma, os Contratos e Aditivos são realizados pelo Poder Executivo, para maiores informações
                clique no Link abaixo: <br><br>

                <b>Contratos e Aditivos<br>
            </a>
            <br><br>
        </p>
    <?php }

    if ($novoCliente == "53") { ?>
        <p>
            <a href="http://177.73.209.254:7474/esportal/slccontrato.load.logic" target="_blank"><b>Contrato:</b> é o
                instrumento dado à administração pública para dirigir-se e atuar perante seus administrados sempre que
                necessite adquirir bens ou serviços dos particulares. As contratações do ente público são regidas pela
                Lei nº 8.666/93 e suas alterações.
                <br/><br/>
                <b>Aditivo:</b> é um instrumento utilizado para formalizar a alteração de alguma cláusula do contrato
                entre a administração pública e o contratado.
                <br/><br/>
                <b>Contratos e Aditivos</b></a>
        </p>
    <?php }


    if ($novoCliente == "12128") { ?>
        <p>

            <b>INFORMATIVO:</b> O Poder Legislativo Municipal de Prado Ferreira, mantém suas atividades administrativas
            centralizadas ao Poder Executivo Municipal. <br><br>

            Desta forma, os Contratos e Aditivos são realizados pelo Poder Executivo, para maiores informações clique no
            Link abaixo: <br><br>

            <b>Contratos e Aditivos: [ <a target="_self" onclick="RedirectUrl()" style="cursor: pointer">LINK</a> ]

                <br><br>
        </p>

        <script>
            function RedirectUrl() {
                window.location.href = "http://179.107.10.3:7474/esportal/slccontrato.load.logic";
            }
        </script>
    <?php }


    ?>

    <?php if ($novoCliente != "12126") {
        if ($novoCliente != "53") {
            ?>
            <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#form-contrato"
                    aria-expanded="false" aria-controls="form-contrato">
                <i class="glyphicon glyphicon-search"></i> Pesquisar
            </button>
        <?php }
    } 
?>

<?php
	if($novoCliente == "12168"){
?>

	<a href="http://187.87.208.214/pronimtb/index.asp?acao=1&item=1" class="btn btn-default" target="_blank">
		<i class="glyphicon glyphicon-list-alt"></i> Contratos e Aditivos Anteriores à 2018
	</a>

<?
	}
?>

    <? if($novoCliente == "11986"){ ?>
        <a href="https://e-gov.betha.com.br/transparencia/con_contratos.faces?mun=LbsTD4qAj-0MJtVi3JASfgVQKZ2qzmm8" class="btn btn-success" target="_blank">
            <i class="glyphicon glyphicon-list-alt"></i> Consulta de Contratos a partir de 2016</a>
    <? } ?>

    <?php
    if (count($confAta)) {
        if (empty($configuracaoTransparencia['caminho_contrato_transparencia'])) {

            $linkAta = $CAMINHO . "/index.php?sessao=$sequencia" . $ataRegistroPreco . "$complemento&nc= $novoCliente";

        } else {

            $linkAta = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=contrato_transparencia&redir=link&nc= $novoCliente";

        }
        ?>
        <a href="<?= $linkAta ?>" class="btn btn-success"><i class="glyphicon glyphicon-list"></i> Ata de Registro de
            Pre&ccedil;os</a>
    <?php } ?>

    <?php if ($novoCliente == "46") { ?>
        <a href="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia . $outrosDocumentos . $complemento); ?>&nc=<?= $novoCliente ?>"
           class="btn btn-success"><i class="glyphicon glyphicon-list"></i> Relat&oacute;rio de Contratos de
            2013 &agrave; 2015</a>
    <?php } ?>

    <?php if ($novoCliente == "1162") { ?>
        <a href="https://e-gov.betha.com.br/transparencia/con_contratos.faces?mun=BeuVrJ70ho2N2-8TCr7bai1IO1udU-8S"
           class="btn btn-success" target="_blank"><i class="glyphicon glyphicon-list"></i> Relat&oacute;rios</a>
    <?php } ?>

    <?php if ($novoCliente == "48") { ?>
        <a href="http://187.49.83.12:7474/esportal/slccontrato.load.logic" class="btn btn-success" target="_blank"><i
                    class="glyphicon glyphicon-list"></i> Relat&oacute;rios</a>
    <?php } ?>

    <form class="form-horizontal collapse" id="form-contrato"
          action="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia . $contrato . $complemento); ?>&nc=<?= $novoCliente ?>"
          method="post">
        <p class="clearfix">&nbsp;</p>
        <div class="form-group">
            <label for="contratado" class="col-sm-2 control-label">Contratado</label>
            <div class="col-sm-10">
                <input type="text" name="contratado" id="contratado" class="form-control"
                       value="<?= $_POST['contratado'] ?>" placeholder="Informe o nome do contratado...">
            </div>
        </div>
        <div class="form-group">
            <label for="assinatura" class="col-sm-2 control-label">Data da Assinatura</label>
            <div class="col-sm-10">
                <input type="text" name="assinatura" id="assinatura" class="form-control data"
                       value="<?= $_POST['assinatura'] ?>" placeholder="Informe a data da assinatura...">
            </div>
        </div>
        <div class="form-group">
            <label for="ano" class="col-sm-2 control-label">Ano</label>
            <div class="col-sm-10">
                <input type="text" name="ano" id="ano" class="form-control ano" value="<?= $_POST['ano'] ?>"
                       placeholder="Informe o ano da assinatura...">
            </div>
        </div>
        <div class="form-group">
            <label for="objeto" class="col-sm-2 control-label">Objeto</label>
            <div class="col-sm-10">
                <input type="text" name="objeto" id="objeto" class="form-control" value="<?= $_POST['objeto'] ?>"
                       placeholder="Informe um trecho do objeto...">
            </div>
        </div>
        <div class="form-group">
            <label for="numero" class="col-sm-2 control-label">N&uacute;mero</label>
            <div class="col-sm-10">
                <input type="text" name="numero" id="numero" class="form-control" value="<?= $_POST['numero'] ?>"
                       placeholder="Informe o n&uacute;mero do contrato...">
            </div>
        </div>
        <div class="form-group">
            <label for="id_categoria_contrato" class="col-sm-4 col-md-2 control-label">Categoria</label>
            <div class="col-sm-8 col-md-10">
                <p class="carregando_categoria form-control-static hidden">Aguarde, carregando...</p>
                <select name="id_categoria" id="id_categoria_contrato" class="form-control" style="display: block;">
                    <option value="">»&nbsp;Selecione a categoria</option>
                    <?php foreach ($qryCategoria as $indiceCategoria => $itemCategoria) { ?>
                        <option value="<?= $itemCategoria['id_categoria'] ?>"><?= $itemCategoria['descricao'] ?></option>
                    <? } ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="id_subcategoria" class="col-sm-4 col-md-2 control-label">Subcategoria</label>
            <div class="col-sm-8 col-md-10">
                <p class="carregando_categoria form-control-static hidden">Aguarde, carregando...</p>
                <select name="id_subcategoria" id="id_subcategoria" class="form-control" style="display: block;">
                    <option value="">»&nbsp;Selecione a subcategoria</option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <input type="hidden" name="acao" value="cadastrar">
                <button type="reset" class="btn btn-danger">Limpar</button>
                <button type="submit" class="btn btn-primary">Enviar</button>
            </div>
        </div>
    </form>

    <?php
    $pagina = isset($_REQUEST['pagina']) ? $_REQUEST['pagina'] : 1;
    $max = 10;
    $inicio = $max * ($pagina - 1);

    $link = $CAMINHO . "/index.php?sessao=" . $_GET['sessao'];

    $sql = "SELECT * FROM contrato_transparencia WHERE id_cliente = :id_cliente AND status_registro = :status_registro ";
    $vetor["id_cliente"] = $novoCliente;
    $vetor["status_registro"] = "A";


    if ($_REQUEST['contratado'] != "") {

        $sql .= "AND contratado LIKE :contratado ";
        $vetor["contratado"] = "%" . $_REQUEST['contratado'] . "%";
        $link .= "&contratado=" . $_REQUEST['contratado'];
    }

    if ($_REQUEST['assinatura'] != "") {

        $sql .= "AND data_assinatura = :data_assinatura ";
        $vetor["data_assinatura"] = formata_data_banco($_REQUEST['assinatura']);
        $link .= "&data_assinatura=" . $_REQUEST['data_assinatura'];

    }

    if ($_REQUEST['ano'] != "") {

        $sql .= "AND YEAR(data_assinatura) = :ano ";
        $vetor["ano"] = $_REQUEST['ano'];
        $link .= "&ano=" . $_REQUEST['ano'];
    }

    if ($_REQUEST['numero'] != "") {

        $sql .= "AND numero LIKE :numero ";
        $vetor["numero"] = "%" . $_REQUEST['numero'] . "%";
        $link .= "&numero=" . $_REQUEST['numero'];
    }

    if ($_REQUEST['objeto'] != "") {

        $sql .= "AND objeto LIKE :objeto ";
        $vetor["objeto"] = "%" . $_REQUEST['objeto'] . "%";
        $link .= "&objeto=" . $_REQUEST['objeto'];
    }

    if ($_REQUEST['id_categoria'] != '') {

        $sql .= " AND id_categoria = :id_categoria ";
        $vetor['id_categoria'] = $_REQUEST['id_categoria'];
        $link .= "&id_categoria=" . $_REQUEST['id_categoria'];
    }

    if ($_REQUEST['id_subcategoria'] != '') {
        $sql .= " AND id_subcategoria = :id_subcategoria ";
        $vetor['id_subcategoria'] = $_REQUEST['id_subcategoria'];
        $link .= "&id_subcategoria=" . $_REQUEST['id_subcategoria'];
    }

    $stLinha = $conn->prepare($sql);
    $stLinha->execute($vetor);
    $qryLinha = $stLinha->fetchAll();
    $totalLinha = count($qryLinha);

    if ($novoCliente == "43")
        $sql .= "ORDER BY numero DESC, id DESC LIMIT $inicio, $max";
    else
        $sql .= "ORDER BY data_assinatura DESC, id DESC LIMIT $inicio, $max";

    $stContrato = $conn->prepare($sql);
    $stContrato->execute($vetor);
    $qryContrato = $stContrato->fetchAll();

    if (count($qryContrato)) {

        ?>
        <p class="clearfix"></p>
        <div class="col-md-3">
            <div class="list-group" id="main-menu">
                <?php

                $linkContrato = "$CAMINHO/index.php?sessao=" . verifica($sequencia . $contrato . $complemento);

                foreach ($qryCategoria as $indiceCategoria => $itemCategoria) {
                    $isCategoriaSelecionada = ($_REQUEST['id_categoria'] == $itemCategoria['id_categoria']);
                    $linkCategoria = $linkContrato . "&id_categoria=" . $itemCategoria['id_categoria'];

                    $sqlSubCategoria = "SELECT `contrato_transparencia`.`id_subcategoria`,
                                               `contrato_transparencia_subcategoria`.`descricao`,
                                               COUNT(`contrato_transparencia`.`id_subcategoria`) AS 'quantidade'
                                          FROM `contrato_transparencia`
                                          JOIN `contrato_transparencia_subcategoria`
                                            ON `contrato_transparencia`.`id_subcategoria` = `contrato_transparencia_subcategoria`.`id`
                                         WHERE `contrato_transparencia`.`id_cliente` = :id_cliente
                                           AND `contrato_transparencia`.`status_registro` = :status_registro
                                           AND `contrato_transparencia`.`id_categoria` = :id_categoria
                                      GROUP BY `contrato_transparencia`.`id_subcategoria`
                                      ORDER BY `contrato_transparencia_subcategoria`.`descricao`";

                    $subCategoriaArgs = array(
                        "id_cliente" => $novoCliente,
                        "status_registro" => "A",
                        "id_categoria" => $itemCategoria['id_categoria']
                    );

                    $stSubCategoria = $conn->prepare($sqlSubCategoria);
                    $stSubCategoria->execute($subCategoriaArgs);
                    $qrySubCategoria = $stSubCategoria->fetchAll();
                    $existeSub = (count($qrySubCategoria) > 0);

                    ?>
                    <a href="<?= $linkCategoria ?>"
                       class="list-group-item <?= ($isCategoriaSelecionada ? "active" : "") ?>">
                        <span class="badge"><?= $itemCategoria['quantidade'] ?></span>
                        <?= $itemCategoria['descricao'] ?>
                    </a>
                    <?php if ($existeSub) { ?>
                        <div class="collapse list-group-level1 <?= ($isCategoriaSelecionada ? "in" : "") ?>"
                             id="sub-menu<?= $indiceCategoria ?>">
                            <?php foreach ($qrySubCategoria as $indiceSubCategoria => $itemSubCategoria) {
                                $linkSubCategoria = $linkCategoria . "&id_subcategoria=" . $itemSubCategoria['id_subcategoria'];
                                $isSubCategoriaSelecionada = ($_REQUEST['id_subcategoria'] == $itemSubCategoria['id_subcategoria']);
                                ?>
                                <a href="<?= $linkSubCategoria ?>"
                                   class="list-group-item sub-item <?= ($isSubCategoriaSelecionada ? "active" : "") ?>"
                                   data-parent="sub-menu<?= $indiceCategoria ?>">
                                    <span class="badge"><?= $itemSubCategoria['quantidade'] ?></span>
                                    &nbsp;
                                    <span class="glyphicon glyphicon-menu-right"></span>
                                    <?= $itemSubCategoria['descricao'] ?>
                                </a>
                            <? } ?>
                        </div>
                    <? } ?>
                <? } ?>
            </div>
        </div>
        <div class="col-md-9">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <?php foreach ($qryContrato as $contrato) { ?>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading_<?= $contrato['id'] ?>">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapse_<?= $contrato['id'] ?>" aria-expanded="true"
                                   aria-controls="collapse_<?= $contrato['id'] ?>">
                                    <div class="row">
                                        <!-- IF OURIZONA-SUPORTE-ADONIS -->
                                        <?php if($cliente == "12113"){ ?>
                                            <div class="col-sm-6">
                                            <strong><?= $itemSubCategoria['descricao'] ?></strong>
                                            </div>
                                            <div class="col-sm-6">
                                            <span>Para mais informações sobre os Contratos e Adtivos,<a href="https://e-gov.betha.com.br/transparencia/01031-021/con_contratos.faces?mun=F8aqbCbXOyo7ypFgBbJYhvWnddJxIGNT" target="_blank">clique aqui!</a></span>
                                            </div>
                                        <?php }else{ ?>
                                        <div class="col-sm-6"><i class="glyphicon glyphicon-triangle-right"></i>
                                            <strong>Contratado:</strong> <?= $contrato['contratado'] ?>
                                        </div>
                                        <div class="col-sm-3"><strong>Processo:</strong> <?= $contrato['processo'] ?>
                                        </div>
                                        <div class="col-sm-3"><strong>N&uacute;mero:</strong> <?= $contrato['numero'] ?>
                                        </div>
                                        <?php } ?>
                                        <!-- IF OURIZONA-SUPORTE-ADONIS -->
                                    </div>
                                </a>
                            </h4>
                        </div>
                        <div id="collapse_<?= $contrato['id'] ?>" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="heading_<?= $contrato['id'] ?>">
                            <div class="panel-body">
                                <?php if ($contrato['cnpj'] != "") { ?><p><strong>CNPJ / CPF:</strong> <?= $contrato['cnpj'] ?></p><?php } ?>
                                <?php if ($contrato['domicilio'] != "") { ?><p><strong>Domic&iacute;lio:</strong> <?= $contrato['domicilio'] ?></p><?php } ?>
                                <?php if ($contrato['data_assinatura'] != "") { ?><p><strong>Data da Assinatura:</strong> <?= formata_data($contrato['data_assinatura']) ?></p><?php } ?>
                                <?php if ($contrato['vigencia'] != "") { ?><p><strong>Vig&ecirc;ncia:</strong> <?= $contrato['vigencia'] ?></p><?php } ?>
                                <?php if ($contrato['numero'] != "") { ?><p><strong>N&uacute;mero:</strong> <?= $contrato['numero'] ?></p><?php } ?>
                                <?php if ($contrato['modalidade'] != "") { ?><p><strong>Modalidade:</strong> <?= $contrato['modalidade'] ?></p><?php } ?>
                                <?php if ($contrato['valor_global'] != "") { ?><p><strong>Valor Global:</strong> <?= $contrato['valor_global'] ?></p><?php } ?>
                                <?php if ($contrato['objeto'] != "") { ?><p><strong>Objeto:</strong> <?= verifica($contrato['objeto']) ?></p><?php } ?>
                                <?php if ($contrato['aditivos'] != "") { ?><p>
                                    <strong>Aditivos:</strong> <?= $contrato['aditivos'] ?></p><?php } ?>

                                <?php
                                $stAnexo = $conn->prepare("SELECT * FROM contrato_transparencia_anexo WHERE id_artigo = :id_artigo ORDER BY ordem ASC, id DESC");
                                $stAnexo->execute(array("id_artigo" => $contrato['id']));
                                $qryAnexo = $stAnexo->fetchAll();

                                if (count($qryAnexo)) {
                                    ?>
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h2 class="panel-title"><i class="glyphicon glyphicon-cloud-download"></i>
                                                Anexos</h2>
                                        </div>
                                        <div class="panel-body">
                                            <?php
                                            foreach ($qryAnexo as $anexo) {
                                                ?>
                                                <p><strong><a href="<?= $CAMINHOARQ ?>/<?= $anexo['arquivo'] ?>"
                                                              target="_blank"><i
                                                                    class="glyphicon glyphicon-cloud-download"></i> <?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?>
                                                        </a></strong></p>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php }

                                $stLink = $conn->prepare("SELECT * FROM contrato_transparencia_link WHERE id_artigo = :id_artigo ORDER BY ordem ASC, id DESC");
                                $stLink->execute(array("id_artigo" => $contrato['id']));
                                $qryLink = $stLink->fetchAll();

                                if (count($qryLink)) {
                                    ?>
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h2 class="panel-title"><i class="glyphicon glyphicon-link"></i>
                                                Links</h2>
                                        </div>
                                        <div class="panel-body">
                                            <?php
                                            foreach ($qryLink as $link) {
                                                ?>
                                                <p><strong><a href="<?= $link['link'] ?>"
                                                              target="_blank"><i class="glyphicon glyphicon-link"></i> <?= empty($link['descricao']) ? "Link" : $link['descricao'] ?>
                                                        </a></strong></p>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>

        <?php
        $menos = $pagina - 1;
        $mais = $pagina + 1;
        $paginas = ceil($totalLinha / $max);
        if ($paginas > 1) {
            ?>
            <nav>
                <ul class="pagination">
                    <?php if ($pagina == 1) { ?>
                        <li class="disabled"><a href="#" aria-label="Anterior"><span
                                        aria-hidden="true">&laquo;</span></a></li>
                    <?php } else { ?>
                        <li><a href="<?= $link ?>&pagina=<?= $menos ?>" aria-label="Anterior"><span
                                        aria-hidden="true">&laquo;</span></a></li>
                        <?php
                    }

                    if (($pagina - 4) < 1) $anterior = 1;
                    else $anterior = $pagina - 4;

                    if (($pagina + 4) > $paginas) $posterior = $paginas;
                    else $posterior = $pagina + 4;

                    for ($i = $anterior; $i <= $posterior; $i++) {

                        if ($i != $pagina) {
                            ?>
                            <li><a href="<?= $link ?>&pagina=<?= $i ?>"><?= $i ?></a></li>
                        <?php } else { ?>
                            <li class="active"><a href="#"><?= $i ?><span class="sr-only">(current)</span></a></li>
                            <?php
                        }
                    }

                    if ($mais <= $paginas) {
                        ?>
                        <li><a href="<?= $link ?>&pagina=<?= $mais ?>" aria-label="Pr&oacute;ximo"><span
                                        aria-hidden="true">&raquo;</span></a></li>
                    <?php } ?>
                </ul>
            </nav>
        <?php } ?>

    <?php } else {
        if ($novoCliente != "53") {
            ?>

            <h4>Nenhum registro encontrado.</h4>
            <?php
        }
    }

    $atualizacao = atualizacao("contratos", $novoCliente, $conn);
    if ($atualizacao != "") {
        ?>
        <p class="text-right">
            <small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima
                    atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small>
        </p>
        <?php
    }
} else {
    $stCategoria = $conn->prepare("SELECT * FROM contrato_transparencia_categoria WHERE (id_cliente = :id_cliente OR id_cliente = 1) AND status_registro = :status_registro ORDER BY descricao DESC");

    $stCategoria->execute(array("id_cliente" => $novoCliente, "status_registro" => "A"));
    $qryCategoria = $stCategoria->fetchAll();

    if (count($qryCategoria)) {
        ?>
        <ul class="treeview">
            <?php
            foreach ($qryCategoria as $categoria) {
                ?>
                <li><a href="#"><?= $categoria['descricao'] ?></a>
                    <?php
                    $stArquivo = $conn->prepare("SELECT contrato_transparencia.id, 
														contrato_transparencia.id_categoria, 
														contrato_transparencia_link.descricao descricao,
														contrato_transparencia_link.link link 
												   FROM contrato_transparencia 
											  LEFT JOIN contrato_transparencia_link ON contrato_transparencia.id = contrato_transparencia_link.id_artigo 
												  WHERE contrato_transparencia.id_categoria = :id_categoria 
													AND (contrato_transparencia.id_subcategoria IS NULL OR contrato_transparencia.id_subcategoria = '') 
													AND contrato_transparencia.status_registro = :status_registro 
													AND contrato_transparencia.id_cliente = :id_cliente
											   ORDER BY id DESC;");
                    $stArquivo->execute(array("id_categoria" => $categoria['id'], "status_registro" => "A", "id_cliente" => $novoCliente));
                    $qryArquivo = $stArquivo->fetchAll();

                    if (count($qryArquivo)) {
                        ?>
                        <ul>
                            <?php
                            foreach ($qryArquivo as $arquivo) {

                                if ($arquivo['link'] != "") {

                                    $linkDocumento = $arquivo['link'];
                                    $imagem = "glyphicon-globe";
                                    $titulo = empty($arquivo['descricao']) ? "Link" : $arquivo['descricao'];
                                }
                                ?>
                                <li><a href="<?= $linkDocumento ?>" target="_blank"><i
                                                class="glyphicon <?= $imagem ?>"></i> <?= $titulo ?></a></li>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                    <?php
                    $stSubcategoria = $conn->prepare("SELECT * FROM contrato_transparencia_subcategoria WHERE id_categoria = :id_categoria AND status_registro = :status_registro ORDER BY descricao ASC");

                    $stSubcategoria->execute(array("id_categoria" => $categoria['id'], "status_registro" => "A"));
                    $qrySubcategoria = $stSubcategoria->fetchAll();

                    if (count($qrySubcategoria)) {
                        ?>
                        <ul>
                            <?php
                            foreach ($qrySubcategoria as $subcategoria) {
                                ?>
                                <li><a href="#"><?= $subcategoria['descricao'] ?></a>
                                    <?php
                                    $stArquivo = $conn->prepare("SELECT contrato_transparencia.id, 
																		contrato_transparencia.id_categoria, 
																		contrato_transparencia_link.descricao descricao,
																		contrato_transparencia_link.link link 
																   FROM contrato_transparencia 
															  LEFT JOIN contrato_transparencia_link ON contrato_transparencia.id = contrato_transparencia_link.id_artigo
																  WHERE contrato_transparencia.id_subcategoria = :id_subcategoria 
																    AND contrato_transparencia.status_registro = :status_registro 
																    AND contrato_transparencia.id_cliente = :id_cliente
															   ORDER BY id DESC");
                                    $stArquivo->execute(array("id_subcategoria" => $subcategoria['id'], "status_registro" => "A", "id_cliente" => $novoCliente));
                                    $qryArquivo = $stArquivo->fetchAll();

                                    if (count($qryArquivo)) {
                                        ?>
                                        <ul>
                                            <?php
                                            foreach ($qryArquivo as $arquivo) {

                                                if ($arquivo['link'] != "") {

                                                    $linkDocumento = $arquivo['link'];
                                                    $imagem = "glyphicon-globe";
                                                    $titulo = empty($arquivo['descricao']) ? "Link" : $arquivo['descricao'];
                                                }
                                                ?>
                                                <li><a href="<?= $linkDocumento ?>" target="_blank"><i
                                                                class="glyphicon <?= $imagem ?>"></i> <?= $titulo ?>
                                                    </a>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    <?php } ?>
                                </li>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                </li>
            <?php } ?>
        </ul>
        <?php
    }

    $atualizacao = atualizacao("contratos", $novoCliente, $conn);
    if ($atualizacao != "") {
        ?>
        <p class="text-right">
            <small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima
                    atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small>
        </p>
    <?php }
}
