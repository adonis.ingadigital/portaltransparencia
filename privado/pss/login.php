<?php
$titulo = "Login - Inscri&ccedil;&otilde;es PSS";
if($_POST['acao'] == 'sair'){
    session_unset();
}
if($_SESSION['login'] != '' && $_SESSION['senha'] != '' || $_GET['pag'] == 'recupera_senha'){
    
    include 'tela.php';

}else{
    
    if($_POST['acao'] == 'cadastro' || $_POST['acao'] == 'login'){
        if($_POST['acao'] == 'cadastro'){
            include "cadastro.php";
        }
        if($_POST['acao'] == 'login'){
            include "home.php";
        }
    }else{
    ?>
    <h2><?= $titulo ?></h2>

    <ol class="breadcrumb">

        <li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>

        <li class="active"><?= $titulo ?></li>

    </ol>
    <!-- Adicionando Javascript -->
    <script type="text/javascript" >

    function limpa_formulário_cep() {
            //Limpa valores do formulário de cep.
            document.getElementById('rua').value=("");
            document.getElementById('bairro').value=("");
            document.getElementById('cidade').value=("");
            document.getElementById('ibge').value=("");
    }

    function meu_callback(conteudo) {
        if (!("erro" in conteudo)) {
            //Atualiza os campos com os valores.
            document.getElementById('rua').value=(conteudo.logradouro);
            document.getElementById('bairro').value=(conteudo.bairro);
            document.getElementById('cidade').value=(conteudo.localidade);
            document.getElementById('ibge').value=(conteudo.ibge);
        } //end if.
        else {
            //CEP não Encontrado.
            limpa_formulário_cep();
            alert("CEP não encontrado.");
        }
    }
        
    function pesquisacep(valor) {

        //Nova variável "cep" somente com dígitos.
        var cep = valor.replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
        if (cep != "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if(validacep.test(cep)) {

                //Preenche os campos com "..." enquanto consulta webservice.
                document.getElementById('rua').value="...";
                document.getElementById('bairro').value="...";
                document.getElementById('cidade').value="...";
                document.getElementById('ibge').value="...";

                //Cria um elemento javascript.
                var script = document.createElement('script');

                //Sincroniza com o callback.
                script.src = 'https://viacep.com.br/ws/'+ cep + '/json/?callback=meu_callback';

                //Insere script no documento e carrega o conteúdo.
                document.body.appendChild(script);

            } //end if.
            else {
                //cep é inválido.
                limpa_formulário_cep();
                alert("Formato de CEP inválido.");
            }
        } //end if.
        else {
            //cep sem valor, limpa formulário.
            limpa_formulário_cep();
        }
    };

    </script>
    <style>
    .form-signin
    {
        max-width: 330px;
        padding: 15px;
        margin: 0 auto;
    }
    .form-signin .form-signin-heading, .form-signin .checkbox
    {
        margin-bottom: 10px;
    }
    .form-signin .checkbox
    {
        font-weight: normal;
    }
    .form-signin .form-control
    {
        position: relative;
        font-size: 16px;
        height: auto;
        padding: 10px;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }
    .form-signin .form-control:focus
    {
        z-index: 2;
    }
    .form-signin input[type="text"]
    {
        margin-bottom: -1px;
        border-bottom-left-radius: 0;
        border-bottom-right-radius: 0;
    }
    .form-signin input[type="password"]
    {
        margin-bottom: 10px;
        border-top-left-radius: 0;
        border-top-right-radius: 0;
    }
    .account-wall
    {
        margin-top: 20px;
        padding: 40px 0px 20px 0px;
        background-color: #f7f7f7;
        -moz-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        -webkit-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    }
    .login-title
    {
        color: #555;
        font-size: 18px;
        font-weight: 400;
        display: block;
    }
    .profile-img
    {
        width: 96px;
        height: 96px;
        margin: 0 auto 10px;
        display: block;
        -moz-border-radius: 50%;
        -webkit-border-radius: 50%;
        border-radius: 50%;
    }
    .need-help
    {
        margin-top: 10px;
    }
    .new-account
    {
        display: block;
        margin-top: 10px;
    }
    .no-padding{
        padding: 0 !important;
    }
    </style>
    <div class="container">
        <div class="row">
            <?php if($_GET['msg_sucesso'] != ''){?>
            <div class="alert alert-<?=$_GET['type']?>">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <strong><?=$_GET['msg_sucesso']?></strong>
            </div>
            <?php } ?>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <h1 class="text-center login-title">Acesso à PSS</h1>
                <div class="account-wall">
                    <img class="profile-img" src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120"
                        alt="">
                    <form class="form-signin" method="post">
                        <input type="text" class="form-control cpf" placeholder="CPF" value="<?=$_POST['cpf']?>" name="cpf" required autofocus>
                        <input type="password" class="form-control" name="senha" placeholder="Password" required>
                        <input type="hidden" name="acao" value="login">
                        <button class="btn btn-lg btn-primary btn-block" type="submit">
                            Entrar</button>
                        <a href="http://www.ingadigital.com.br/transparencia/index.php?sessao=15ad55c926c715&pag=recupera_senha" class="pull-right need-help">Esqueci minha senha</a><span class="clearfix"></span>
                    </form>
                </div>
                <a data-toggle="collapse" data-target="#cadastro" class="text-center new-account">Deseja se Cadastrar?</a>
            </div>
        </div>
    </div>

    <div id="cadastro" class="collapse">
        <div class="row" style="margin-top: 15px;">
            <div class="container">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="text-center">Formulário de Cadastro</h4>
                    </div>
                    <div class="panel-body">
                        <form method="post">
                            <div class="form-group">
                                <label for="">Dados Pessoais:</label>
                                <div class="input-group">
                                    <span class="input-group-addon">Nome Completo:<h11 style="color:red">*</h11></span>
                                    <input id="nome" type="text" class="form-control required" name="nome" required value="<?=$_POST['nome']?>" placeholder="Maria Pereira dos Santos">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">CPF:<h11 style="color:red">*</h11></span>
                                    <input id="cpf" type="text" class="form-control cpf required" name="cpf" value="<?=$_POST['cpf']?>" placeholder="111.111.111-11" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">RG:</span>
                                    <input id="rg" type="text" class="form-control rg required" name="rg" value="<?=$_POST['rg']?>" placeholder="12.345.678-9">
                                </div>
                            </div>

                            <div class="form-group col-md-6 col-xs-12" style="padding:0">
                                <div class="input-group">
                                    <span class="input-group-addon">Data expidição RG:</span>
                                    <input id="data_expedicao_rg" type="date" class="form-control required" name="data_expedicao_rg" value="<?=$_POST['data_expedicao_rg']?>" placeholder="DD/MM/AAAA">
                                </div>
                            </div>

                            <div class="form-group col-md-6 col-xs-12" style="padding:0">
                                <div class="input-group">
                                    <span class="input-group-addon">Orgão expedidor:</span>
                                    <input id="orgao_expedidor_rg" type="text" class="form-control required" name="orgao_expedidor_rg" value="<?=$_POST['orgao_expedidor_rg']?>" placeholder="SSP/PR">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class='input-group'>
                                    <span class="input-group-addon">Data Nascimento:<h11 style="color:red">*</h11></span>
                                    <input type='text' name="data_nascimento" placeholder="12/01/2001" value="<?=$_POST['data_nascimento']?>" class="form-control data required" required/>
                                </div>
                            </div>
                            

                            <div class="form-group col-md-4 col-xs-12" style="padding:0">
                                <div class="input-group">
                                    <span class="input-group-addon" for="sexo">Sexo:<h11 style="color:red">*</h11></span>
                                    <select required id="sexo" name="sexo" class="form-control required">
                                    <option value="">Sexo</option>
                                    <option value="M">Masculino</option>
                                    <option value="F">Feminino</option>
                                    <option value="I">Indefinido</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-8 col-xs-12" style="padding:0">
                                <div class="input-group">
                                    <span class="input-group-addon" for="Filhos">Deficiência:<h11 style="color:red">*</h11></span>
                                    <span class="input-group-addon">     
                                        <label class="radio-inline" for="radios-0">
                                    <input type="radio" name="tem_deficiencia" id="tem_deficiencia" value="N" onclick="desabilita('quais_deficiencias')" class="semdeficiencia required" required checked>
                                    Não
                                    </label> 
                                    <label class="radio-inline" for="radios-1">
                                    <input type="radio" name="tem_deficiencia" id="tem_deficiencia" value="S" onclick="habilita('quais_deficiencias')">
                                    Sim
                                    </label>
                                    </span>
                                    <input id="quais_deficiencias" name="quais_deficiencias" class="form-control" type="text" placeholder="Quais?">
                                </div>
                            </div>

                            <div class="form-group col-md-6 col-xs-12" style="padding:0">
                                <div class="input-group"> 
                                    <span class="input-group-addon" for="radios">Índio:<h11 style="color:red">*</h11></span>
                                    <span class="input-group-addon">
                                        <label required class="radio-inline" for="radios-0">
                                        <input name="indio" id="indio" value="N" type="radio" required checked>
                                        Não
                                        </label> 
                                        <label class="radio-inline" for="radios-1">
                                        <input name="indio" id="indio" value="S" type="radio">
                                        Sim
                                        </label>
                                    </span>
                                </div>
                            </div>

                            <div class="form-group col-md-6 col-xs-12" style="padding:0">
                                <div class="input-group"> 
                                    <span class="input-group-addon" for="radios">Afrodescendente:<h11 style="color:red">*</h11></span>
                                    <span class="input-group-addon">
                                        <label required class="radio-inline" for="radios-0">
                                        <input name="afrodescendente" id="afrodescendente" value="N" type="radio" required checked>
                                        Não
                                        </label> 
                                        <label class="radio-inline" for="radios-1">
                                        <input name="afrodescendente" id="afrodescendente" value="S" type="radio">
                                        Sim
                                        </label>
                                    </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="">Dados do Endereço:</label>
                                <div class='input-group'>
                                    <span class="input-group-addon">CEP:<h11 style="color:red">*</h11></span>
                                    <input type='text' name="cep" placeholder="86945-000" id="cep" size="10" maxlength="9" value="<?=$_POST['cep']?>" onblur="pesquisacep(this.value);" class="form-control cep required"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class='input-group'>
                                    <span class="input-group-addon">Cidade:<h11 style="color:red">*</h11></span>
                                    <input type='text' name="cidade" placeholder="Sua Cidade" id="cidade" value="<?=$_POST['cidade']?>" class="form-control required" required/>
                                    <input type="hidden" name="id_cidade" id="ibge">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class='input-group'>
                                    <span class="input-group-addon">Logradouro:<h11 style="color:red">*</h11></span>
                                    <input type='text' name="endereco" placeholder="Av. Exemplo" id="rua" value="<?=$_POST['endereco']?>" class="form-control required" required/>
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4 no-padding">
                                <div class='input-group'>
                                    <span class="input-group-addon">Numero:<h11 style="color:red">*</h11></span>
                                    <input type='text' name="numero_endereco" placeholder="333" value="<?=$_POST['numero_endereco']?>" class="form-control required" required/>
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4 no-padding">
                                <div class='input-group'>
                                    <span class="input-group-addon">Complemento:</span>
                                    <input type='text' name="complemento" placeholder="Sobreloja" value="<?=$_POST['complemento']?>" class="form-control required"/>
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4 no-padding">
                                <div class='input-group'>
                                    <span class="input-group-addon">Bairro:<h11 style="color:red">*</h11></span>
                                    <input type='text' name="bairro" placeholder="Centro" id="bairro" value="<?=$_POST['bairro']?>" class="form-control required" required/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Dados de Contato e da conta:</label>
                                <div class='input-group'>
                                    <span class="input-group-addon">Email:<h11 style="color:red">*</h11></span>
                                    <input type='mail' name="email" placeholder="maria@gmail.com" id="email" value="<?=$_POST['email']?>" class="form-control email required" required/>
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6 no-padding">  
                                <div class='input-group'>
                                    <span class="input-group-addon">Telefone Fixo:</span>
                                    <input type='text' name="telefone_fixo" placeholder="(99)9999-9999" id="telefone_fixo" value="<?=$_POST['telefone_fixo']?>" class="form-control telefone"/>
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6 no-padding">
                                <div class='input-group'>
                                    <span class="input-group-addon">Telefone Celular:<h11 style="color:red">*</h11></span>
                                    <input type='text' name="telefone_celular" placeholder="(00)00000-0000" id="telefone_celular" value="<?=$_POST['telefone_celular']?>" class="form-control telefone required" required/>
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6 no-padding">
                                <div class='input-group'>
                                    <span class="input-group-addon">Digite uma Senha:<h11 style="color:red">*</h11></span>
                                    <input type='password' name="senha" id="password" class="form-control required" required/>
                                </div>
                            </div>
                            <div class="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6 no-padding">
                                <div class='input-group'>
                                    <span class="input-group-addon">Repita sua Senha:<h11 style="color:red">*</h11></span>
                                    <input type='password' name="senha_confirma" id="confirm_password" class="form-control required" required/>
                                </div>
                            </div>
                            <div class="form-group text-center">
                                <input type="hidden" name="acao" value="cadastro">
                                <button class="btn btn-success" type="submit">Enviar</button>
                            </div>
                        </form>
                    </div>
                </div>   
            </div>
        </div>
    </div>
    <script>
    var password = document.getElementById("password"), confirm_password = document.getElementById("confirm_password");

    function validatePassword(){
        if(password.value != confirm_password.value) {
            confirm_password.setCustomValidity("Senhas diferentes!");
        } else {
            confirm_password.setCustomValidity('');
        }
    }
    password.onchange = validatePassword;
    confirm_password.onkeyup = validatePassword;

    function desabilita(i){
        document.getElementById(i).disabled = true;   
    }
    function habilita(i){
        document.getElementById(i).disabled = false;
    }

    window.onload = function(){
        var check = document.getElementsByClassName('semdeficiencia');
        
        if (check[0].checked == true){ 
            document.getElementById('quais_deficiencias').disabled = true;  
        }
    }
    </script>
    <?php
    }
}
?>