<?php
    switch ($_GET['pag']) {
        case 'inicio':
            include 'inicio.php';
            break;
        
        case 'editar':
            include 'editar.php';
            break;
        
        case 'cadastro':
            include 'cadastro.php';
            break;

        case 'minhas_inscricoes':
            include 'minhas_inscricoes.php';
            break;
        
        case 'lista_concurso':
            include 'lista_concurso.php';
            break;
        
        case 'vaga':
            include 'vaga.php';
            break;
        
        case 'recupera_senha':
            include 'recupera_senha.php';
            break;

        case 'imprimir_inscricao':
            include 'imprimir_inscricao.php';
            break;

        default:
            include 'inicio.php';
            break;
    }
?>