<?php
    if($_POST['acao'] = 'recupera'){
        include "modelo_email.php";
        foreach ($_REQUEST as $campo => $valor) {
            $$campo = secure($valor);
        }

        $stUsuario = $conn->prepare("SELECT * FROM concurso_pss_candidato WHERE email = :email AND status_registro = :status_registro");
        $stUsuario->execute(array("email" => $email, "status_registro" => "A"));
        $buscaUsuario = $stUsuario->fetch(PDO::FETCH_ASSOC);

		if(!empty($buscaUsuario['id'])) {
            $nome = $buscaUsuario['nome'];
            $senha = $buscaUsuario['senha'];
            $remetente = $buscaUsuario['email'];
            $to[0]['email'] = $buscaUsuario['email'];
            $to[0]['nome'] = "PSS";
            $headers = "$nome <$remetente> \r\n";
            $subject = "Recupera&ccedil;&atilde;o de Senha ( $nome ) ";
            $titulo = "Recupera&ccedil;&atilde;o de Senha ( $nome )";
            $corpo = "<p>Ol&aacute; $nome, conforme solicitado segue a sua senha atual: '$senha'.</p>";
            $link = "http://www.ingadigital.com.br/transparencia/index.php?sessao=15ad55c926c715";
            $titulo_botao = "Tela de Login";
            $rodape = "<p>&nbsp;</p><p>Origem: ".$_SERVER['REMOTE_ADDR'].", enviado em " . date("d/m/Y H:i") . " por $nome</p>";

            $html = recupera_senha_email($titulo,$corpo,$link,$titulo_botao,$rodape);
            $vai = envia_email_aws($to, html_entity_decode($subject), $html, $headers, [], './ingadigital.com.br/privado/transparencia/pss/recupera_senha.php:27');
        
            if($vai) {
                
                echo "<script>window.location='http://www.ingadigital.com.br/transparencia/index.php?sessao=15ad55c926c715&msg_sucesso=".urlencode("Um e-mail foi enviado para o endere&ccedil;o $to")."&type=success'</script>";
            
            

            } else {

                $msg = "Erro ao enviar o email, tente novamente mais tarde.";

            }
        }
    }
?>
<h2>Recuperar Senha Concurso PSS</h2>
<ol class="breadcrumb">
	<li class="active">Recuperar Senha Concurso PSS</li>
</ol>
<style>
.form-signin
{
    max-width: 330px;
    padding: 15px;
    margin: 0 auto;
}
.form-signin .form-signin-heading, .form-signin .checkbox
{
    margin-bottom: 10px;
}
.form-signin .checkbox
{
    font-weight: normal;
}
.form-signin .form-control
{
    position: relative;
    font-size: 16px;
    height: auto;
    padding: 10px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}
.form-signin .form-control:focus
{
    z-index: 2;
}
.form-signin input[type="text"]
{
    margin-bottom: -1px;
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
}
.form-signin input[type="password"]
{
    margin-bottom: 10px;
    border-top-left-radius: 0;
    border-top-right-radius: 0;
}
.account-wall
{
    margin-top: 20px;
    padding: 40px 0px 20px 0px;
    background-color: #f7f7f7;
    -moz-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    -webkit-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
}
.login-title
{
    color: #555;
    font-size: 18px;
    font-weight: 400;
    display: block;
}
.profile-img
{
    width: 96px;
    height: 96px;
    margin: 0 auto 10px;
    display: block;
    -moz-border-radius: 50%;
    -webkit-border-radius: 50%;
    border-radius: 50%;
}
.need-help
{
    margin-top: 10px;
}
.new-account
{
    display: block;
    margin-top: 10px;
}
</style>
<div class="container">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <h1 class="text-center login-title">Recupera&ccedil;&atilde;o de senha</h1>
            <div class="account-wall">
                <form class="form-signin" method="post">
                    <input type="text" class="form-control" placeholder="Digite seu email" name="email" required autofocus>
                    <input type="hidden" name="acao" value="recupera">
                    <button class="btn btn-lg btn-primary btn-block" type="submit" style="margin-top: 20px;">
                        Enviar</button>
                </form>
            </div>
        </div>
    </div>
</div>