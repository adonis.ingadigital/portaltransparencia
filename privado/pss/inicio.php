<?php
    $stmtverificaCPF = $conn->prepare("SELECT * FROM concurso_pss_candidato WHERE cpf = :cpf AND id_cliente = :id_cliente");
    $stmtverificaCPF->execute(['cpf' => $_SESSION['login'], 'id_cliente' => $cliente]); 
    $verificaCPF = $stmtverificaCPF->fetch();
    $titulo = "Seja bem-vindo, ".$verificaCPF['nome'];
?>
<style>
.no-padding{
    padding: 0 !important;
}
.float-right{
    position: absolute;
    top: 21%;
    right: 1%;
}
</style>
<ul class="list-group">
    <li class="list-group-item">
        <h4 class="text-center"><?= $titulo ?></h4>
        <form class="float-right" action="http://www.ingadigital.com.br/transparencia/index.php?sessao=15ad55c926c715" method="post">
            <input type="hidden" name="acao" value="sair">
            <button class="btn btn-danger text-right" type="submit">Sair</button>
        </form>
    </li>
</ul>
<style>
.icone-pss{
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    font-size: 120%;
    font-family: 'Exo', sans-serif;
    color: #337AB7;
}
.glyphicon{
    font-size: 180%;
}
.float-right{
    position: absolute;
    top: 6%;
    right: 0%;
}
</style>
<div class="panel panel-info">
    <div class="panel-heading" style="position: relative;">
        <h3 class="panel-title">Op&ccedil;&otilde;es do Sistema</h3>
    </div>
    <div class="panel-body">
        <a href="http://www.ingadigital.com.br/transparencia/index.php?sessao=15ad55c926c715&pag=editar">
            <div class="col-xs-4 icone-pss">
                <i class="glyphicon glyphicon-pencil"></i> Editar Cadastro
            </div>
        </a>
        <a href="http://www.ingadigital.com.br/transparencia/index.php?sessao=15ad55c926c715&pag=minhas_inscricoes">
            <div class="col-xs-4 icone-pss">
            <i class="glyphicon glyphicon-user"></i> Minhas Incri&ccedil;&otilde;es
            </div>
        </a>
        <a href="http://www.ingadigital.com.br/transparencia/index.php?sessao=15ad55c926c715&pag=lista_concurso">
            <div class="col-xs-4 icone-pss">
            <i class="glyphicon glyphicon-book"></i> Increver-se
            </div>
        </a>
    </div>
</div>
