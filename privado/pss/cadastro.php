<?php
if(!isset($_POST['id'])){
    foreach ($_REQUEST as $campo => $valor) {
        $$campo = secure($valor);
    }
    $stmtverificaCPF = $conn->prepare("SELECT cpf FROM concurso_pss_candidato WHERE cpf = :cpf AND id_cliente = :id_cliente");
    $stmtverificaCPF->execute(['cpf' => $cpf, 'id_cliente' => $cliente]); 
    $verificaCPF = $stmtverificaCPF->fetch();
    $diahora = date("Y-m-d H:i:s");
    if($verificaCPF['cpf'] === NULL && $senha != ''){
        $stmtCandidato = $conn->prepare('INSERT INTO concurso_pss_candidato    (id_cliente, 
                                                                                id_municipio, 
                                                                                data_cadastro, 
                                                                                nome, 
                                                                                cpf, 
                                                                                rg, 
                                                                                data_expedicao_rg,
                                                                                orgao_expedidor_rg,
                                                                                sexo,
                                                                                afrodescendente,
                                                                                indio,
                                                                                tem_deficiencia,
                                                                                quais_deficiencias,
                                                                                data_nascimento, 
                                                                                endereco, 
                                                                                numero_endereco,
                                                                                complemento,
                                                                                bairro,
                                                                                cep,
                                                                                email,
                                                                                senha,
                                                                                telefone_fixo,
                                                                                telefone_celular)    VALUES(:id_cliente,
                                                                                                            :id_municipio,
                                                                                                            :data_cadastro,
                                                                                                            :nome,
                                                                                                            :cpf,
                                                                                                            :rg,
                                                                                                            :data_expedicao_rg,
                                                                                                            :orgao_expedidor_rg,
                                                                                                            :sexo,
                                                                                                            :afrodescendente,
                                                                                                            :indio,
                                                                                                            :tem_deficiencia,
                                                                                                            :quais_deficiencias,
                                                                                                            :data_nascimento,
                                                                                                            :endereco,
                                                                                                            :numero_endereco,
                                                                                                            :complemento,
                                                                                                            :bairro,
                                                                                                            :cep,
                                                                                                            :email,
                                                                                                            :senha,
                                                                                                            :telefone_fixo,
                                                                                                            :telefone_celular)');
        $stmtCandidato->execute(array(
            ':id_cliente'         => $cliente,
            ':id_municipio'       => $id_cidade,
            ':data_cadastro'      => $diahora,
            ':nome'               => $nome,
            ':cpf'                => $cpf,
            ':rg'                 => $rg,
            ':data_expedicao_rg'  => $data_expedicao_rg,
            ':orgao_expedidor_rg' => $orgao_expedidor_rg,
            ':sexo'               => $sexo,
            ':afrodescendente'    => $afrodescendente,
            ':indio'              => $indio,
            ':tem_deficiencia'    => $tem_deficiencia,
            ':quais_deficiencias' => $quais_deficiencias,
            ':data_nascimento'    => formata_data_banco($data_nascimento),
            ':endereco'           => $endereco,
            ':numero_endereco'    => $numero_endereco,
            ':complemento'        => $complemento,
            ':bairro'             => $bairro,
            ':cep'                => $cep,
            ':email'              => $email,
            ':senha'              => $senha,
            ':telefone_fixo'      => $telefone_fixo,
            ':telefone_celular'   => $telefone_celular,
        ));
        if($stmtCandidato){

            include "home.php";
        }
    }else{
    ?>
        <div class="alert alert-danger">
            <strong>O seu CPF já está sendo utilizado, entre em contato!</strong> Caso tenha perdido seu acesso entre em <a href="">Recupere sua Senha</a>
        </div>
    <?php   
        $_POST['acao'] = NULL;
        include "login.php";
    }
}else{
    foreach ($_REQUEST as $campo => $valor) {
        $$campo = secure($valor);
    }
    $stmtverificaCadastro = $conn->prepare("SELECT cpf FROM concurso_pss_candidato WHERE id = :id");
    $stmtverificaCadastro->execute(['id' => $id]); 
    $verificaCadastro = $stmtverificaCadastro->fetch();
    $diahora = date("Y-m-d H:i:s");

    if ($verificaCadastro) {

    $stmt = $conn->prepare('UPDATE concurso_pss_candidato SET   id_municipio = :id_municipio,
                                                                data_atualizacao = :data_atualizacao,
                                                                nome = :nome,
                                                                cpf = :cpf,
                                                                rg = :rg,
                                                                data_expedicao_rg = :data_expedicao_rg,
                                                                orgao_expedidor_rg = :orgao_expedidor_rg,
                                                                sexo = :sexo,
                                                                afrodescendente = :afrodescendente,
                                                                indio = :indio,
                                                                tem_deficiencia = :tem_deficiencia,
                                                                quais_deficiencias =:quais_deficiencias,
                                                                data_nascimento = :data_nascimento,
                                                                endereco = :endereco,
                                                                numero_endereco = :numero_endereco,
                                                                complemento = :complemento,
                                                                bairro = :bairro,
                                                                cep = :cep,
                                                                email = :email,
                                                                senha = :senha,
                                                                telefone_fixo = :telefone_fixo,
                                                                telefone_celular = :telefone_celular
                                                        WHERE id = :id');
    $stmt->execute(array(
        ':id'                 => $id,
        ':id_municipio'       => $id_cidade,
        ':data_atualizacao'   => $diahora,
        ':nome'               => $nome,
        ':cpf'                => $cpf,
        ':rg'                 => $rg,
        ':data_expedicao_rg'  => $data_expedicao_rg,
        ':orgao_expedidor_rg' => $orgao_expedidor_rg,
        ':sexo'               => $sexo,
        ':afrodescendente'    => $afrodescendente,
        ':indio'              => $indio,
        ':tem_deficiencia'    => $tem_deficiencia,
        ':quais_deficiencias' => $quais_deficiencias,
        ':data_nascimento'    => formata_data_banco($data_nascimento),
        ':endereco'           => $endereco,
        ':numero_endereco'    => $numero_endereco,
        ':complemento'        => $complemento,
        ':bairro'             => $bairro,
        ':cep'                => $cep,
        ':email'              => $email,
        ':senha'              => $senha,
        ':telefone_fixo'      => $telefone_fixo,
        ':telefone_celular'   => $telefone_celular,
    ));
    if($stmt){
        $_GET['pag'] = 'inicio';
        include "tela.php";
    }
    } else {
    ?>
        <div class="alert alert-danger">
            <strong>Ocorreu um erro, tente novamente</strong>
        </div>
    <?php
        $_GET['pag'] = 'inicio';
        include "tela.php";
    }
}
?>