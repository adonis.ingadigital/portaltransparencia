<style>
.float-right {
    position: absolute;
    right: 47%;
    top: 0%;
}
</style>
<h2 style="position: relative;">Processo Seletivo Simplificado</h2>
<ol class="breadcrumb">
	<li><a href="http://www.ingadigital.com.br/transparencia/index.php?sessao=15ad55c926c715">In&iacute;cio</a></li>
	<li class="active">PSS</li>
</ol>
<?php

	$pagina = isset($_REQUEST['pagina']) ? $_REQUEST['pagina'] : 1;
	$max = 10;
	$inicio = $max * ($pagina - 1);

	$link = $CAMINHO . "/index.php?sessao=" . $_GET['sessao'];

	$sql = "    SELECT * 
                FROM concurso_pss
                WHERE id_cliente = :id_cliente 
                AND status_registro = :status_registro ";
	$vetor["id_cliente"] = $cliente;
	$vetor["status_registro"] = "A";

	$stLinha = $conn->prepare($sql);
	$stLinha->execute($vetor);
	$qryLinha = $stLinha->fetchAll();
	$totalLinha = count($qryLinha);

	$sql .= "ORDER BY data_publicacao DESC, id DESC LIMIT $inicio, $max";

	$stConcurso = $conn->prepare($sql);
	$stConcurso->execute($vetor);
	$qryConcurso = $stConcurso->fetchAll();

	if(count($qryConcurso)) {
?>
<?php 
		foreach ($qryConcurso as $concurso) { 
?>
	
	<div class="panel panel-default">
        <div class="panel-heading" role="tab" id="heading_<?= $concurso['id'] ?>">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?= $concurso['id'] ?>" aria-expanded="true" aria-controls="collapse_<?= $concurso['id'] ?>">
                    <i class="glyphicon glyphicon-triangle-right"></i> <strong><?= $concurso['titulo'] ?></strong>
                </a>
            </h4>
        </div>
		<div id="collapse_<?= $concurso['id'] ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_<?= $concurso['id'] ?>">
			<div class="panel-body">
				<p><strong>Data da Publica&ccedil;&atilde;o:</strong> <?= formata_data($concurso['data_publicacao']) ?></p>
				<p><strong>Data da Prova:</strong> <?= formata_data($concurso['data_concurso']) ?></p>
				<?= verifica($concurso['artigo']) ?>
				
            <?
            $stVaga = $conn->prepare("  SELECT * 
                                        FROM concurso_pss_vaga 
                                        WHERE id_cliente = :id_cliente 
                                        AND id_artigo = :id_artigo 
                                        AND status_registro = :status_registro");
			$stVaga->execute(array("id_cliente" => $cliente, "id_artigo" => $concurso['id'], "status_registro" => "A"));
			$qryVaga = $stVaga->fetchAll();
			if(count($qryVaga)) {?>
				<div class="panel panel-danger">
					<div class="panel-heading">
                        <h2 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i> CARGOS RELACIONADAS</h2>
                    </div>
					<div class="panel-body">

                        <?php foreach($qryVaga as $vaga) { ?>
						<p>
							<a href="http://www.ingadigital.com.br/transparencia/index.php?sessao=15ad55c926c715&pag=vaga&id_vaga=<?= $vaga['id'] ?>">
								<strong><i class="glyphicon glyphicon-triangle-right"></i>&nbsp;<?= $vaga['vaga'] ?></strong>
							</a>
						</p>
                        <?php } ?>

					</div>
				</div>
            <?php } 
            $stAnexo = $conn->prepare(" SELECT * 
                                        FROM concurso_pss_anexo 
                                        WHERE id_artigo = :id_artigo 
                                        ORDER BY ordem ASC, id DESC");
			$stAnexo->execute(array("id_artigo" => $concurso['id']));
			$qryAnexo = $stAnexo->fetchAll();
			if(count($qryAnexo)) { ?>
				<div class="panel panel-primary">
					<div class="panel-heading">
	                    <h2 class="panel-title"><i class="glyphicon glyphicon-cloud-download"></i> Anexos</h2>
				    </div>
					<div class="panel-body">
					
                    <?php foreach($qryAnexo as $anexo) {?>
						<p><strong><a href="<?= $CAMINHOARQ ?>/<?= $anexo['arquivo'] ?>" target="_blank"><i class="glyphicon glyphicon-cloud-download"></i> <?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?></a></strong></p>
                    <?php } ?>

					</div>
				</div>
            <?php } 
            $stLink = $conn->prepare("  SELECT * 
                                        FROM concurso_pss_link 
                                        WHERE id_artigo = :id_artigo 
                                        ORDER BY ordem ASC, id DESC");
			$stLink->execute(array("id_artigo" => $concurso['id']));
			$qryLink = $stLink->fetchAll();

			if(count($qryLink)) { ?>

                
				<div class="panel panel-primary">
                    <div class="panel-heading">
                        <h2 class="panel-title"><i class="glyphicon glyphicon-globe"></i> Links</h2>
                    </div>
					<div class="panel-body">
                    <?php foreach ($qryLink as $linkConcurso) { ?>
						<p><strong><a href="<?= $linkConcurso['link'] ?>" target="_blank"><i class="glyphicon glyphicon-globe"></i> <?= empty($linkConcurso['descricao']) ? "Link" : $linkConcurso['descricao'] ?></a></strong></p>
                    <?php } ?>
					</div>
				</div>
            <?php } ?>
			</div>
		</div>
	</div>
            <?php } ?>
</div>

<?php   $menos = $pagina - 1;
        $mais = $pagina + 1;
        $paginas = ceil($totalLinha / $max);
        if($paginas > 1) {?>
<nav>
	<ul class="pagination">
        <?php if($pagina == 1) { ?>
        <li class="disabled"><a href="#" aria-label="Anterior"><span aria-hidden="true">&laquo;</span></a></li>
        <?php } else { ?>
        <li><a href="<?= $link ?>&pagina=<?= $menos ?>" aria-label="Anterior"><span aria-hidden="true">&laquo;</span></a></li>
        <?php }
        if(($pagina - 4) < 1) 
            $anterior = 1;
        else 
            $anterior = $pagina - 4;
        if(($pagina + 4) > $paginas) 
            $posterior = $paginas;
        else 
            $posterior = $pagina + 4;
        for($i = $anterior; $i <= $posterior; $i++) {
            if($i != $pagina) {
        ?>
        <li><a href="<?= $link ?>&pagina=<?= $i ?>"><?= $i ?></a></li>
            <?php } else { ?>
        <li class="active"><a href="#"><?= $i ?><span class="sr-only">(current)</span></a></li>
        <?php                }                           }
            if($mais <= $paginas) { ?>
        <li><a href="<?= $link ?>&pagina=<?= $mais ?>" aria-label="Pr&oacute;ximo"><span aria-hidden="true">&raquo;</span></a></li>
        <?php } ?>
	</ul>
</nav>
<?php } 
    } else { 
?>

<h4>Nenhum registro encontrado.</h4>

<?php
	}
	$atualizacao = atualizacao("concurso", $cliente, $conn);
	if($atualizacao != "") {
?>

<p class="text-right"><small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small></p>

<?php
	}