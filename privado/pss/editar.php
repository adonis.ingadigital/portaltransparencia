<?php
    $stmtverificaCPF = $conn->prepare("SELECT * FROM concurso_pss_candidato WHERE cpf = :cpf");
    $stmtverificaCPF->execute(['cpf' => $_SESSION['login']]); 
    $verificaCPF = $stmtverificaCPF->fetch();

    $sexos = array("Masculinho" => "M",
                    "Feminino" => "F",
                    "Indefinido" => "I");

?>
<style>
.no-padding{
    padding: 0 !important;
}
.float-right{
    position: absolute;
    top: 21%;
    right: 1%;
}
</style>

<ul class="list-group">
    <li class="list-group-item">
        <h4 class="text-center">Editar dados Cadastrais</h4>
        <form class="float-right" action="http://www.ingadigital.com.br/transparencia/index.php?sessao=15ad55c926c715&pag=inicio" method="post">
            <button class="btn btn-warning text-right" type="submit">Voltar</button>
        </form>
    </li>
</ul>

<form action="http://www.ingadigital.com.br/transparencia/index.php?sessao=15ad55c926c715&pag=cadastro" method="post">
    <ul class="list-group">
        <li class="list-group-item active">Dados Pessoais</li>
        <li class="list-group-item">
            <div class="row">
                <div class="form-group col-xs-12 col-sm-12 col-md-8 col-lg-8">
                    <div class="input-group">
                        <span class="input-group-addon">Nome Completo:</span>
                        <input id="nome" type="text" class="form-control required" name="nome" value="<?=$verificaCPF['nome']?>">
                        <input type="hidden" name="id" value="<?=$verificaCPF['id']?>">
                    </div>
                </div>
                <div class="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="input-group">
                        <span class="input-group-addon">CPF:</span>
                        <input id="cpf" type="text" class="form-control cpf required" name="cpf" value="<?=$verificaCPF['cpf']?>">
                    </div>
                </div>
                <div class="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="input-group">
                        <span class="input-group-addon">RG:</span>
                        <input id="rg" type="text" class="form-control rg required" name="rg" value="<?=$verificaCPF['rg']?>">
                    </div>
                </div>

                <div class="form-group col-md-6 col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon">Data expidição RG:</span>
                        <input id="data_expedicao_rg" type="date" class="form-control required" name="data_expedicao_rg" value="<?=$verificaCPF['data_expedicao_rg']?>" placeholder="DD/MM/AAAA">
                    </div>
                </div>

                <div class="form-group col-md-6 col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon">Orgão expedidor:</span>
                        <input id="orgao_expedidor_rg" type="text" class="form-control required" name="orgao_expedidor_rg" value="<?=$verificaCPF['orgao_expedidor_rg']?>" placeholder="SSP/PR">
                    </div>
                </div>

                <div class="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class='input-group'>
                        <span class="input-group-addon">Data Nascimento:</span>
                        <input type='text' name="data_nascimento" value="<?=date("d/m/Y", strtotime($verificaCPF['data_nascimento']))?>" class="form-control data required"/>
                    </div>
                </div>
                <div class="form-group col-md-4 col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon" for="sexo">Sexo:<h11 style="color:red">*</h11></span>
                        <select required id="sexo" name="sexo" class="form-control required">

                        
                       <? foreach ($sexos as $key => $value) {?>
                            <option value='$value' <?=$value == $verificaCPF['sexo'] ? 'selected' : ''?> ><?=$key?></option>
                        <? } ?>
                        
                        </select>
                    </div>
                </div>
                <div class="form-group col-md-8 col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon" for="Filhos">Deficiência:<h11 style="color:red">*</h11></span>
                        <span class="input-group-addon">     
                            <label class="radio-inline" for="radios-0">

                        <input type="radio" name="tem_deficiencia" id="tem_deficiencia" value="N" onclick="desabilita('quais_deficiencias')" class="semdeficiencia required" required <?=$verificaCPF['tem_deficiencia'] == 'N' ? 'checked' : '' ?> >
                        Não
                        </label> 
                        <label class="radio-inline" for="radios-1">
                        <input type="radio" name="tem_deficiencia" id="tem_deficiencia" value="S" onclick="habilita('quais_deficiencias')" <?=$verificaCPF['tem_deficiencia'] == 'S' ? 'checked' : ''?> >
                        Sim
                        </label>
                        </span>
                        <input id="quais_deficiencias" name="quais_deficiencias" class="form-control" value="<?=$verificaCPF['quais_deficiencias']?>"  type="text" placeholder="Quais?">
                    </div>
                </div>

                <div class="form-group col-md-6 col-xs-12">
                    <div class="input-group"> 
                        <span class="input-group-addon" for="radios">Índio:<h11 style="color:red">*</h11></span>
                        <span class="input-group-addon">
                            <label required class="radio-inline" for="radios-0">
                            <input name="indio" id="indio" value="N" type="radio" required checked>
                            Não
                            </label> 
                            <label class="radio-inline" for="radios-1">
                            <input name="indio" id="indio" value="S" type="radio">
                            Sim
                            </label>
                        </span>
                    </div>
                </div>

                <div class="form-group col-md-6 col-xs-12">
                    <div class="input-group"> 
                        <span class="input-group-addon" for="radios">Afrodescendente:<h11 style="color:red">*</h11></span>
                        <span class="input-group-addon">
                            <label required class="radio-inline" for="radios-0">
                            <input name="afrodescendente" id="afrodescendente" value="N" type="radio" required checked>
                            Não
                            </label> 
                            <label class="radio-inline" for="radios-1">
                            <input name="afrodescendente" id="afrodescendente" value="S" type="radio">
                            Sim
                            </label>
                        </span>
                    </div>
                </div>
            </div>
        </li>
    </ul>
    
    <ul class="list-group">
        <li class="list-group-item active">Dados do Endereço</li>
        <li class="list-group-item">
            <div class="row">
                <div class="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class='input-group'>
                        <span class="input-group-addon">CEP:</span>
                        <input type='text' name="cep" id="cep" size="10" maxlength="9" value="<?=$verificaCPF['cep']?>" onblur="pesquisacep(this.value);" class="form-control cep required"/>
                    </div>
                </div>
                <div class="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class='input-group'>
                        <span class="input-group-addon">Cidade:</span>
                        <input type='text' name="cidade" id="cidade" value="<?=$verificaCPF['cidade']?>" class="form-control required"/>
                        <input type="hidden" name="id_cidade" value="<?=$verificaCPF['id_municipio']?>" id="ibge">
                    </div>
                </div>
                <div class="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class='input-group'>
                        <span class="input-group-addon">Bairro:</span>
                        <input type='text' name="bairro" id="bairro" value="<?=$verificaCPF['bairro']?>" class="form-control required"/>
                    </div>
                </div>
                <div class="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class='input-group'>
                        <span class="input-group-addon">Logradouro:</span>
                        <input type='text' name="endereco" id="rua" value="<?=$verificaCPF['endereco']?>" class="form-control required"/>
                    </div>
                </div>
                <div class="form-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <div class='input-group'>
                        <span class="input-group-addon">Numero:</span>
                        <input type='text' name="numero_endereco" value="<?=$verificaCPF['numero_endereco']?>" class="form-control required"/>
                    </div>
                </div>
                <div class="form-group col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <div class='input-group'>
                        <span class="input-group-addon">Complemento:</span>
                        <input type='text' name="complemento" value="<?=$verificaCPF['complemento']?>" class="form-control required"/>
                    </div>
                </div>
            </div>
        </li>
    </ul>
    
    <ul class="list-group">
        <li class="list-group-item active">Dados de Contato e da conta</li>
        <li class="list-group-item">
            <div class="row">
                <div class="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class='input-group'>
                        <span class="input-group-addon">Email:</span>
                        <input type='mail' name="email" id="email" value="<?=$verificaCPF['email']?>" class="form-control email required"/>
                    </div>
                </div>
                <div class="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">  
                    <div class='input-group'>
                        <span class="input-group-addon">Telefone Fixo:</span>
                        <input type='text' name="telefone_fixo" id="telefone_fixo" value="<?=$verificaCPF['telefone_fixo']?>" class="form-control telefone"/>
                    </div>
                </div>
                <div class="form-group col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class='input-group'>
                        <span class="input-group-addon">Telefone Celular:</span>
                        <input type='text' name="telefone_celular" id="telefone_celular" value="<?=$verificaCPF['telefone_celular']?>" class="form-control telefone required"/>
                    </div>
                </div>
                <div class="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class='input-group'>
                        <span class="input-group-addon">Digite uma Senha:</span>
                        <input type='password' name="senha" id="password" value="<?=$verificaCPF['senha']?>" class="form-control required"/>
                    </div>
                </div>
                <div class="form-group col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class='input-group'>
                        <span class="input-group-addon">Repita sua Senha:</span>
                        <input type='password' name="senha_confirma" id="confirm_password" value="<?=$verificaCPF['senha']?>"  class="form-control required"/>
                    </div>
                </div>
            </div>
        </li>
    </ul>

    <div class="col-xs-12 text-center">
        <input type="hidden" name="acao" value="editar">
        <button class="btn btn-success" type="submit" style="width: 100%;">Salvar Altera&ccedil;&otilde;es</button>
    </div>
</form>

<script>
var password = document.getElementById("password"), confirm_password = document.getElementById("confirm_password");

function validatePassword(){
    if(password.value != confirm_password.value) {
        confirm_password.setCustomValidity("Senhas diferentes!");
    } else {
        confirm_password.setCustomValidity('');
    }
}
password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;
</script>
<!-- Adicionando Javascript -->
<script type="text/javascript" >
document.addEventListener("load", pesquisacep('<?=$verificaCPF['cep']?>'));

function limpa_formulário_cep() {
        //Limpa valores do formulário de cep.
        document.getElementById('cidade').value=("");
        document.getElementById('ibge').value=("");
}

function meu_callback(conteudo) {
    if (!("erro" in conteudo)) {
        //Atualiza os campos com os valores.
        document.getElementById('cidade').value=(conteudo.localidade);
        document.getElementById('ibge').value=(conteudo.ibge);
    } //end if.
    else {
        //CEP não Encontrado.
        limpa_formulário_cep();
        alert("CEP não encontrado.");
    }
}
    
function pesquisacep(valor) {

    //Nova variável "cep" somente com dígitos.
    var cep = valor.replace(/\D/g, '');

    //Verifica se campo cep possui valor informado.
    if (cep != "") {

        //Expressão regular para validar o CEP.
        var validacep = /^[0-9]{8}$/;

        //Valida o formato do CEP.
        if(validacep.test(cep)) {

            //Preenche os campos com "..." enquanto consulta webservice.
            document.getElementById('cidade').value="...";
            document.getElementById('ibge').value="...";

            //Cria um elemento javascript.
            var script = document.createElement('script');

            //Sincroniza com o callback.
            script.src = 'https://viacep.com.br/ws/'+ cep + '/json/?callback=meu_callback';

            //Insere script no documento e carrega o conteúdo.
            document.body.appendChild(script);

        } //end if.
        else {
            //cep é inválido.
            limpa_formulário_cep();
            alert("Formato de CEP inválido.");
        }
    } //end if.
    else {
        //cep sem valor, limpa formulário.
        limpa_formulário_cep();
    }
};
function desabilita(i){
        document.getElementById(i).disabled = true;   
    }
    function habilita(i){
        document.getElementById(i).disabled = false;
    }

    window.onload = function(){
        var check = document.getElementsByClassName('semdeficiencia');
        
        if (check[0].checked == true){ 
            document.getElementById('quais_deficiencias').disabled = true;  
        }
    }
</script>