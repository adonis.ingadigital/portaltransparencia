<?php
    $sql = "SELECT cpi.*, cpivr.*, cpv.vaga nome_vaga, cpc.nome, cpc.cpf, cpvr.requisito nome_requisito,cpi.id meuID
            FROM concurso_pss_inscricao cpi
            LEFT JOIN concurso_pss_inscricao_vaga_requisito cpivr
            ON cpi.id = cpivr.id_inscricao
            LEFT JOIN concurso_pss_vaga cpv
            ON cpv.id = cpi.id_vaga
            LEFT JOIN concurso_pss_candidato cpc
            ON cpi.id_candidato = cpc.id
            LEFT JOIN concurso_pss_vaga_requisito cpvr
            ON cpvr.id = cpivr.id_requisito
            WHERE cpi.id = :id
            AND cpi.id_cliente = :id_cliente
            AND cpi.status_registro = :status_registro
            -- ORDER BY cpivr.pontuacao_requisito DESC
            ORDER BY cpivr.id asc
            LIMIT 1";

    $vetor["id_cliente"] = $cliente;
    $vetor["id"] = secure($_GET['id']);
    $vetor["status_registro"] = "A";

    $stRelatorio = $conn->prepare($sql);
    $stRelatorio->execute($vetor);
    $qryRelatorio = $stRelatorio->fetch();


    $sql = "SELECT c.*, m.nome
            FROM cliente c
            LEFT JOIN municipio m
            ON c.id_municipio = m.id
            WHERE c.id = :id";
    $vetorCliente["id"] = $cliente;
    $stCliente = $conn->prepare($sql);
    $stCliente->execute($vetorCliente);
    $qryCliente = $stCliente->fetch();  
    if ($qryRelatorio) {
?>
<style>
.no-padding{
    padding: 0 !important;
}
.float-right{
    position: absolute;
    right: 2%;
    top: 26%;
}
.alinha-center-col{
    display: flex;
    align-items: center;
    flex-direction: column;
    justify-content: center;
}
.branco{
    color: #fff;
}
.img-logo{
    top: -30%;
    right: 47%;
    height: 60px;
    width: 60px;
    position: absolute;
    opacity: 0.2;
    filter: grayscale(1);
}
.float{
	position:fixed;
	width:60px;
	height:60px;
	bottom:40px;
	right:40px;
	background-color:#0C9;
	color:#FFF;
	border-radius:50px;
	text-align:center;
	box-shadow: 2px 2px 3px #999;
}

.my-float{
	margin-top:22px;
}
/*CSS para impressão*/
@media print {
    * {
        background:transparent !important;
        color:#000 !important;
        text-shadow:none !important;
        filter:none !important;
        -ms-filter:none !important;
    }

    body {
        margin:0;
        padding:0;
        line-height: 1.4em;
    }
    @page {
        margin: 0.5cm;
    }
    nav, footer, video, audio, object, embed { 
        display:none; 
    }
    .print {
        display:block;
    }

    .no-print { 
        display:none; 
    }
    .form-inline, .panel{
        display:none;
    }
    .list-group-item.active{
        border: 1px solid #DDD !important;
    }
}
</style>
<ul class="no-print list-group">
    <li class="list-group-item">
        <h2 class="text-center">Comprovante de Inscrição</h2>
        <form class="float-right" action="http://www.ingadigital.com.br/transparencia/index.php?sessao=15ad55c926c715&pag=inicio" method="post">
            <button class="btn btn-warning text-right" type="submit">Inicio</button>
        </form>
    </li>
</ul>
<div class="row print">
    <div class="container">
        <ul class="list-group">
            <li class="list-group-item active">
                <h4 class="alinha-center-col branco" style="position: relative;">
                    PROCESSO SELETIVO SIMPLIFICADO DA PREFEITURA DE <?=$qryCliente['nome']?>
                    <img class="img-logo" src="http://www.controlemunicipal.com.br/inga/sistema/imagens/1/<?=$qryCliente['logo']?>" alt="logo">
                    <small class="branco">Comprovante de Inscrição</small>
                </h4>
            </li>
            <li class="list-group-item">
                <h5 class="text-center col-xs-12"><strong>Dados do candidato</strong></h5>
                <p><small>N° da Inscrição:</small> <strong><?=$qryRelatorio['id']?></strong></p>
                <p><small>Candidato:</small> <strong><?=$qryRelatorio['nome']?></strong></p>
                <p><small>CPF:</small> <strong><?=$qryRelatorio['cpf']?></strong></p>
                <p><small>Cargo:</small> <strong><?=$qryRelatorio['nome_vaga']?></strong></p>
                <p><small>Pontuação:</small> <strong><?=$qryRelatorio['pontuacao_inscricao']?></strong></p>
                <p><small>Data da Inscrição:</small> <strong><?=date("d/m/Y", strtotime($qryRelatorio['data_cadastro']))?></strong></p>
            </li>
        </ul>
        <a onclick="imprimir()" class="float no-print">
            <i class="glyphicon glyphicon-print my-float"></i>
        </a>
    </div>  
</div>
<?php }?>
<script>
    function imprimir(){
        window.print(); 
    }
</script>