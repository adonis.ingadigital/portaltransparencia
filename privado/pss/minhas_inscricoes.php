<style>
.no-padding{
    padding: 0 !important;
}
.float-right{
    position: absolute;
    top: 21%;
    right: 1%;
}
</style>
<ul class="list-group">
    <li class="list-group-item">
        <h4 class="text-center">Minhas Inscri&ccedil;&otilde;es</h4>
        <form class="float-right" action="http://www.ingadigital.com.br/transparencia/index.php?sessao=15ad55c926c715&pag=inicio" method="post">
            <button class="btn btn-warning text-right" type="submit">Voltar</button>
        </form>
    </li>
</ul>

<style>
.alinha{
    display: flex;
    align-items: center;
    justify-content: center;
}
small{
    color: #777;
}
p{
    font-size: 16px;
}
</style>
<?php
	$pagina = isset($_REQUEST['pagina']) ? $_REQUEST['pagina'] : 1;
	$max = 10;
	$inicio = $max * ($pagina - 1);

	$link = $CAMINHO . "/index.php?sessao=" . $_GET['sessao'];

    $sql = "SELECT cpi.id, cpi.data_cadastro dta_inscricao, cp.data_publicacao, cp.data_concurso, cp.titulo, cpv.vaga, cpi.pontuacao_inscricao
            FROM concurso_pss_inscricao cpi
            LEFT JOIN concurso_pss cp
            ON cp.id = cpi.id_concurso
            LEFT JOIN concurso_pss_vaga cpv
            ON cpv.id = cpi.id_vaga
            LEFT JOIN concurso_pss_candidato cpc
            ON cpc.id = cpi.id_candidato
            WHERE cpc.cpf = :cpf
            AND cpi.status_registro = :status_registro ";

	$vetor["cpf"] = $_SESSION['login'];
	$vetor["status_registro"] = "A";

	$stLinha = $conn->prepare($sql);
	$stLinha->execute($vetor);
	$qryLinha = $stLinha->fetchAll();
	$totalLinha = count($qryLinha);

	$sql .= "ORDER BY dta_inscricao DESC, id DESC LIMIT $inicio, $max";

	$stMinhasIncricoes = $conn->prepare($sql);
	$stMinhasIncricoes->execute($vetor);
	$qryMinhasIncricoes = $stMinhasIncricoes->fetchAll();

	if(count($qryMinhasIncricoes)) {
?>
<?php 
		foreach ($qryMinhasIncricoes as $minhasIncricoes) { 
?>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0 !important;">
            <div class="panel panel-default">
                <div class="panel-heading alinha" data-toggle="collapse" data-target="#vaga<?=$minhasIncricoes['id']?>">
                    <i class="glyphicon glyphicon-triangle-right"></i> PSS: <strong><?=$minhasIncricoes['titulo']?></strong>
                </div>
                <div id="vaga<?=$minhasIncricoes['id']?>" class="collapse">
                    <div class="panel-body">
                        <p><small>Cargo:</small> <?=$minhasIncricoes['vaga']?></p>
                        <p><small>Pontuação:</small> <strong><?=$minhasIncricoes['pontuacao_inscricao']?></strong></p>
                        <p><small>Data da Inscri&ccedil;&atilde;o:</small> <?=date("d/m/Y", strtotime($minhasIncricoes['dta_inscricao']))?></p>
                        <p><small>Data da Publica&ccedil;&atilde;o:</small> <?=date("d/m/Y", strtotime($minhasIncricoes['data_publicacao']))?></p>
                        <p><small>Data do PSS:</small> <?=date("d/m/Y", strtotime($minhasIncricoes['data_concurso']))?></p>
                    </div>
                </div>
            </div>
        </div>
        <?php   } ?>

<?php   
        $menos = $pagina - 1;
        $mais = $pagina + 1;
        $paginas = ceil($totalLinha / $max);
        if($paginas > 1) {?>
<nav>
	<ul class="pagination">
        <?php if($pagina == 1) { ?>
        <li class="disabled"><a href="#" aria-label="Anterior"><span aria-hidden="true">&laquo;</span></a></li>
        <?php } else { ?>
        <li><a href="<?= $link ?>&pag=minhas_inscricoes&pagina=<?= $menos ?>" aria-label="Anterior"><span aria-hidden="true">&laquo;</span></a></li>
        <?php }
        if(($pagina - 4) < 1) 
            $anterior = 1;
        else 
            $anterior = $pagina - 4;
        if(($pagina + 4) > $paginas) 
            $posterior = $paginas;
        else 
            $posterior = $pagina + 4;
        for($i = $anterior; $i <= $posterior; $i++) {
            if($i != $pagina) {
        ?>
        <li><a href="<?= $link ?>&pag=minhas_inscricoes&pagina=<?= $i ?>"><?= $i ?></a></li>
            <?php } else { ?>
        <li class="active"><a href="#"><?= $i ?><span class="sr-only">(current)</span></a></li>
        <?php                }                           }
            if($mais <= $paginas) { ?>
        <li><a href="<?= $link ?>&pag=minhas_inscricoes&pagina=<?= $mais ?>" aria-label="Pr&oacute;ximo"><span aria-hidden="true">&raquo;</span></a></li>
        <?php } ?>
	</ul>
</nav>
<?php } 
    } else { 
?>

<h4>Nenhum registro encontrado.</h4>

<?php
	}
	$atualizacao = atualizacao("concurso", $cliente, $conn);
	if($atualizacao != "") {
?>

<p class="text-right"><small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small></p>

<?php
	}