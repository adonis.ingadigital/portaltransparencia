<style>
.no-padding{
    padding: 0 !important;
}
.label-check small, .label-radio small{
    font-size: 70%;
    font-weight: 700;
    color: orange;
}
</style>
<ul class="list-group">
    <li class="list-group-item">
        <h4 class="text-center">Cargo</h4>
        <form class="float-right" action="http://www.ingadigital.com.br/transparencia/index.php?sessao=15ad55c926c715&pag=inicio" method="post">
            <button class="btn btn-warning text-right" type="submit">Voltar</button>
        </form>
    </li>
</ul>
<?php
    $sql = "    SELECT * 
                FROM concurso_pss_vaga
                WHERE id_cliente = :id_cliente 
                AND id = :id
                AND status_registro = :status_registro ";
    $vetor["id_cliente"] = $cliente;
    $vetor["id"] = secure($_GET['id_vaga']);
    $vetor["status_registro"] = "A";
    
	$stVaga = $conn->prepare($sql);
	$stVaga->execute($vetor);
    $qryVaga = $stVaga->fetch();
    
    if($_POST['acao'] == 'inscrever'){
        $in_array = array();
        foreach ($_POST['requisitos'] as $key => $requisito) {
            $in_array[] = $requisito;
        }

        $in  = str_repeat('?,', count($in_array) - 1) . '?';

        $sql = "SELECT * 
                FROM concurso_pss_vaga_requisito
                WHERE id_cliente = $cliente 
                AND id_artigo = $qryVaga[id]
                AND status_registro = 'A'
                AND id IN ($in) ";

        $stVagaRequisito = $conn->prepare($sql);
        $stVagaRequisito->execute($in_array);
        $qryVagaRequisito = $stVagaRequisito->fetchAll();

        $total_pontos = 0;

        foreach ($qryVagaRequisito as $key => $VagaRequisito) {
            if($VagaRequisito['requisito_minimo'] != 0){
                $total_pontos += $VagaRequisito['pontuacao'];
                $total_ponto_requisito[$VagaRequisito['id']] = $VagaRequisito['pontuacao'];
            }else if($VagaRequisito['cumulativo'] != 0){
                $total_pontos += $VagaRequisito['pontuacao'];
                $total_ponto_requisito[$VagaRequisito['id']] = $VagaRequisito['pontuacao'];
            }else{
                $temp = 0;
                $qtde_ponto = 'qtde_pnts'.$VagaRequisito['id'];
                $valor_ponto = $VagaRequisito['valor_ponto'];
                $temp = $valor_ponto * $_POST[$qtde_ponto];
                $total_ponto_requisito[$VagaRequisito['id']] = $temp;
                if($temp > $VagaRequisito['pontuacao']){
                    $total_pontos += $VagaRequisito['pontuacao'];
                }else{
                    $total_pontos += $temp;
                }
            }
        }

        if($total_pontos >= $qryVaga['pontuacao_minima']){
            foreach ($_REQUEST as $campo => $valor) {
                $$campo = secure($valor);
            }

            $diahora = date("Y-m-d H:i:s");
            $stmtNovaVaga = $conn->prepare('INSERT INTO concurso_pss_inscricao    ( id_cliente, 
                                                                                    id_concurso, 
                                                                                    id_candidato, 
                                                                                    id_vaga,
                                                                                    pontuacao_inscricao,
                                                                                    data_cadastro)    
                                                                                                        VALUES( :id_cliente,
                                                                                                                :id_concurso,
                                                                                                                :id_candidato,
                                                                                                                :id_vaga,
                                                                                                                :pontuacao_inscricao,
                                                                                                                :data_cadastro)');
            $stmtNovaVaga->execute(array(
                ':id_cliente'      => $cliente,
                ':id_concurso'     => $id_concurso,
                ':id_candidato'    => $id_candidato,
                ':id_vaga'         => $id_vaga,
                ':pontuacao_inscricao' => $total_pontos,
                ':data_cadastro'   => $diahora
            ));
            if($stmtNovaVaga){
                $id_inscricao =  $conn->lastInsertId();
                foreach ($requisitos as $key => $requisito) {
                    $stmtNovaVagaRequisito = $conn->prepare('INSERT INTO concurso_pss_inscricao_vaga_requisito    (     id_inscricao, 
                                                                                                                        id_requisito, 
                                                                                                                        pontuacao_requisito)    
                                                                                                                                VALUES( :id_inscricao,
                                                                                                                                        :id_requisito,
                                                                                                                                        :pontuacao_requisito)');
                    $stmtNovaVagaRequisito->execute(array(
                    ':id_inscricao'             => $id_inscricao,
                    ':id_requisito'             => $requisito,
                    ':pontuacao_requisito'      => $total_ponto_requisito[$requisito]
                    ));
                }
                echo"<script>
                        window.location = 'http://www.ingadigital.com.br/transparencia/index.php?sessao=15ad55c926c715&pag=imprimir_inscricao&id=$id_inscricao';
                    </script>";
            }
        }else{
            echo "  <div class='alert alert-danger'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                        <strong>Sua pontuação não atingiu o valor minimo, verifique os requisitos!</strong>
                    </div>";
        }
    }
?>
<link rel="stylesheet" href="http://www.ingadigital.com.br/transparencia/css/concurso_vaga_pss.css">
<?php
	if(count($qryVaga)) { ?>
    <div class="container vaga">
        <ul class="list-group">
            <li class="list-group-item active">
                <h3 class="panel-title text-center"><strong><?=$qryVaga['vaga']?></strong></h3>
            </li>
            <li class="list-group-item">
                Pontua&ccedil;&atilde;o M&iacute;nima do Cargo: <strong><?=$qryVaga['pontuacao_minima']?></strong>
            </li>
            <li class="list-group-item">
                Pontua&ccedil;&atilde;o M&aacute;xima do Cargo: <strong><?=$qryVaga['pontuacao_maxima']?></strong>
            </li>
        </ul>
        
        <form action="" method="post">
            <ul class="list-group">
                <li class="list-group-item active">
                    Requisitos Minimos
                </li>
                <?php
                $sql = "SELECT * 
                        FROM concurso_pss_vaga_requisito
                        WHERE id_cliente = :id_cliente 
                        AND id_artigo = :id_artigo
                        AND requisito_minimo = :requisito_minimo
                        AND cumulativo = :cumulativo
                        AND status_registro = :status_registro ";
                        
                $vetorVaga["id_cliente"] = $cliente;
                $vetorVaga["id_artigo"] = $qryVaga['id'];
                $vetorVaga["requisito_minimo"] = 1;
                $vetorVaga["cumulativo"] = 0;
                $vetorVaga["status_registro"] = "A";

                $stVagaRequisito = $conn->prepare($sql);
                $stVagaRequisito->execute($vetorVaga);
                $qryVagaRequisito = $stVagaRequisito->fetchAll();
                foreach ($qryVagaRequisito as $key => $VagaRequisito) { ?>
                
                <li class="list-group-item no-padding alinhamento">
                    <div class="campo-check">
                        <label class="cont-check">
                            <input type="checkbox" name="requisitos[]" value="<?=$VagaRequisito['id']?>" checked required>

                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="label-check">
                        <span style="width: 70%"><?=$VagaRequisito['requisito']?></span>
                        <small>&nbsp;( Valor Ponto: <?=($VagaRequisito['valor_ponto'] === NULL)? $VagaRequisito['pontuacao'] : $VagaRequisito['valor_ponto']?> )</small>
                        <span class="label label-danger float-right">Pontuação máxima: <?=$VagaRequisito['pontuacao']?></span>
                    </div>
                    
                </li>
                <?php } ?>
            </ul>
            <?php } ?>
            <ul class="list-group">
                <?php
                $sql = "SELECT * 
                        FROM concurso_pss_vaga_requisito
                        WHERE id_cliente = :id_cliente 
                        AND id_artigo = :id_artigo
                        AND requisito_minimo = :requisito_minimo
                        AND cumulativo = :cumulativo
                        AND status_registro = :status_registro ";
                        
                $vetorVaga["id_cliente"] = $cliente;
                $vetorVaga["id_artigo"] = $qryVaga['id'];
                $vetorVaga["requisito_minimo"] = 0;
                $vetorVaga["cumulativo"] = 1;
                $vetorVaga["status_registro"] = "A";

                $stVagaRequisito = $conn->prepare($sql);
                $stVagaRequisito->execute($vetorVaga);
                $qryVagaRequisito = $stVagaRequisito->fetchAll();
                if($qryVagaRequisito){?>
                <li class="list-group-item active">
                    Requisitos Não Cumulativos <small>(Selecione apenas uma op&ccedil;&atilde;o abaixo)</small>
                </li>
                <?php
                foreach ($qryVagaRequisito as $key => $VagaRequisito) { ?>
                <li class="list-group-item no-padding alinhamento">
                    <div class="campo-check">
                        <label class="cont-radio">
                            <input type="radio" onchange="" name="requisitos[]" value="<?=$VagaRequisito['id']?>">
                            <span class="radiomark"></span>
                        </label>
                    </div>
                    <div class="label-radio">
                        <span style="width: 70%"><?=$VagaRequisito['requisito']?></span>
                        <small>&nbsp;( Valor Ponto: <?=($VagaRequisito['valor_ponto'] === NULL)? $VagaRequisito['pontuacao'] : $VagaRequisito['valor_ponto']?> )</small>
                        <span class="label label-danger float-right">Pontuação máxima: <?=$VagaRequisito['pontuacao']?></span>
                    </div>
                    
                </li>
                <?php } }?>
            </ul>
            <ul class="list-group">
                <li class="list-group-item active">
                    Requisitos
                </li>
                <?php
                $sql = "SELECT * 
                        FROM concurso_pss_vaga_requisito
                        WHERE id_cliente = :id_cliente 
                        AND id_artigo = :id_artigo
                        AND requisito_minimo = :requisito_minimo
                        AND cumulativo = :cumulativo
                        AND status_registro = :status_registro 
                        ORDER BY id DESC ";

                $vetorVaga["id_cliente"] = $cliente;
                $vetorVaga["id_artigo"] = $qryVaga['id'];
                $vetorVaga["requisito_minimo"] = 0;
                $vetorVaga["cumulativo"] = 0;
                $vetorVaga["status_registro"] = "A";

                $stVagaRequisito = $conn->prepare($sql);
                $stVagaRequisito->execute($vetorVaga);
                $qryVagaRequisito = $stVagaRequisito->fetchAll();
                foreach ($qryVagaRequisito as $key => $VagaRequisito) { ?>
                
                <li class="list-group-item no-padding alinhamento">
                    <div class="campo-check">
                        <label class="cont-check">
                            <input type="checkbox" id="requisito<?=$VagaRequisito['id']?>" onchange="validaCampo(<?=$VagaRequisito['id']?>, <?=$VagaRequisito['requisito_trabalho']?>)" name="requisitos[]" value="<?=$VagaRequisito['id']?>">
                            <input type="hidden" name="valor_ponto<?=$VagaRequisito['id']?>" value="<?=($VagaRequisito['valor_ponto'] === NULL)? $VagaRequisito['pontuacao'] : $VagaRequisito['valor_ponto']?>">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="label-check">
                        <span style="width: 70%"><?=$VagaRequisito['requisito']?></span>
                        <small>&nbsp;( Valor Ponto: <?=($VagaRequisito['valor_ponto'] === NULL)? $VagaRequisito['pontuacao'] : $VagaRequisito['valor_ponto']?> )</small>
                        <span class="label label-danger float-right">Pontuação máxima: <?=$VagaRequisito['pontuacao']?></span>
                    </div>
                </li>
                <?php
                if($VagaRequisito['valor_ponto'] !== NULL){?>
                    <?php
                    if($VagaRequisito['requisito_trabalho'] == 1){?>
                    <li class="list-group-item success">
                        <input type="number" id="campo_traba<?=$VagaRequisito['id']?>" name="" onchange="calc_meses(this, <?=$VagaRequisito['valor_ponto']?>, <?=$VagaRequisito['pontuacao']?>, <?=$VagaRequisito['id']?>)" class="form-control" value="" placeholder="Digite a quantidade de meses trabalhados(Ex: 7 meses)">
                        <input type="hidden" id="qtde_pnts<?=$VagaRequisito['id']?>" name="qtde_pnts<?=$VagaRequisito['id']?>" value="">
                    </li>
                    <?php
                    }else{?>
                    <li class="list-group-item success">
                        <input type="number" name="qtde_pnts<?=$VagaRequisito['id']?>" id="qtde_pnts<?=$VagaRequisito['id']?>" class="form-control" value="" placeholder="Digite a quantidade de titulos desse requisito">
                    </li>
                <?php } 
                }else{?>
                    <input type="hidden" id="qtde_pnts<?=$VagaRequisito['id']?>" name="qtde_pnts<?=$VagaRequisito['id']?>" value="1">
                <?php } } ?>
            </ul>
            <?php
                $sql = "SELECT * 
                        FROM concurso_pss_candidato
                        WHERE cpf = :cpf
                        AND status_registro = :status_registro ";
                $vetorCandidato["cpf"] = $_SESSION['login'];
                $vetorCandidato["status_registro"] = "A";

                $stCandidato = $conn->prepare($sql);
                $stCandidato->execute($vetorCandidato);
                $qryCandidato = $stCandidato->fetch();
            ?>
            <ul class="list-group">
                <li class="list-group-item active">Dados do Candidato</li>
                <li class="list-group-item">Nome: <?=$qryCandidato['nome']?></li>
                <li class="list-group-item">CPF: <?=$qryCandidato['cpf']?></li>
                <li class="list-group-item">E-mail: <?=$qryCandidato['email']?></li>
                <li class="list-group-item no-padding alinhamento">
                    <div class="campo-check">
                        <label class="cont-check">
                            <input type="checkbox" onchange="verifica_termo()" name="termo_reponsa" id="termo_reponsa" value="">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="label-check">
                        Eu <strong>&nbsp;aceito os termos de responsabilidade&nbsp;</strong> presentes no edital relacionados a validade dos documentos aqui afirmados.
                    </div>
                </li>
            </ul>
            <div class="botao-enviar text-center">
                <input type="hidden" name="id_concurso" value="<?=$qryVaga['id_artigo']?>">
                <input type="hidden" name="id_candidato" value="<?=$qryCandidato['id']?>">
                <input type="hidden" name="id_vaga" value="<?=$qryVaga['id']?>">
                <input type="hidden" name="acao" value="inscrever">
                <button class="btn btn-success" id="envia_form" type="button" data-toggle="modal" data-target="#ModalConfirma" disabled>Inscrever-se</button>
            </div>

            <!-- Modal -->
            <div id="ModalConfirma" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header" style=" background: #337ab7;">
                            <button type="button" class="close" data-dismiss="modal" style="font-size: 140%; color: black;opacity: 1; text-shadow: none !important;">&times;</button>
                            <h4 class="modal-title text-center" style="font-size: 120%; color: #fff;">Envio de Inscrição</h4>
                        </div>
                        <div class="modal-body">
                            <p class="text-danger text-center"><strong>Você tem certeza que deseja enviar os dados dessa inscrição?</strong></p>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-success" type="submit">Inscrever-se</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
<script>
    function calc_meses(input, ponto, total, key){
        var resto = input.value % 12;
        var meses = (input.value - resto)/12;
        var pontos_inteiros = meses;
        if(resto < 7 && resto > 0){
            pontos_inteiros += 0.5;
        }else if(resto > 6){
            pontos_inteiros += 1;
        }
        total_pontos = pontos_inteiros * ponto;

        if(total_pontos >= total){
            var nome = "qtde_pnts"+key;
            document.getElementById(nome).value = total;
        }else{
            var nome = "qtde_pnts"+key;
            document.getElementById(nome).value = total_pontos/ponto;
        }
        console.log(total_pontos);
        
    }
    function verifica_termo()
    {
        if (document.getElementById('termo_reponsa').checked) 
        {
            document.getElementById('envia_form').disabled = false;
        } else {
            document.getElementById('envia_form').disabled = true;
        }
    }
    function validaCampo(key, trabalho){
        if(document.getElementById('requisito'+key).checked){
            if(trabalho == 1){
                document.getElementById("campo_traba"+key).required = true;
            }else{
                document.getElementById("qtde_pnts"+key).required = true;
            }
        }else{
            if(trabalho == 1){
                document.getElementById("campo_traba"+key).required = false;
                
            }else{
                document.getElementById("qtde_pnts"+key).required = false;
                
            }
        }
    }
</script>