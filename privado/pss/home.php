<?php
    foreach ($_REQUEST as $campo => $valor) {
        $$campo = secure($valor);
    }

    $stmtverificaCPF = $conn->prepare("SELECT * FROM concurso_pss_candidato WHERE cpf = :cpf AND id_cliente = :id_cliente");
    $stmtverificaCPF->execute(['cpf' => $cpf, 'id_cliente' => $cliente]); 
    $verificaCPF = $stmtverificaCPF->fetch();

    if($verificaCPF){
        if ($cpf == $verificaCPF['cpf'] && $senha == $verificaCPF['senha']) {
            $_SESSION['login'] = $verificaCPF['cpf'];
            $_SESSION['senha'] = $verificaCPF['senha'];
            include 'tela.php';

        }else{
        ?>
            <div class="alert alert-danger">
                <strong>Sua senha está incorreta!</strong> Por favor tente novamente.
            </div>
        <?php   
            $_POST['acao'] = NULL;
            include "login.php";
        }
    }else{
    ?>
        <div class="alert alert-danger">
            <strong>Você ainda não possui cadastro!</strong> Por favor efetue o cadastro para continuar.
        </div>
    <?php   
        $_POST['acao'] = NULL;
        include "login.php";
    }
?>