

<? if($cliente == 12039){ ?>
	<p class="text-center">
		<a href="http://abatia.pr.gov.br/transparencia/?id_cliente=12039&sessao=57a8e785593m57" class="col-lg-3 col-xs-12 btn btn-success btn-lg" style="margin-bottom: 15px; float: right; margin-left: 10px; font-size: 13px; padding-bottom: 15px; text-transform: uppercase;padding-top: 15px;">
			<i class="glyphicon glyphicon-book"></i> Acesse a Licita&ccedil;&atilde;o na &Iacute;ntegra
		</a>
	</p>
<?}?>

<h2>Licita&ccedil;&otilde;es na &Iacute;ntegra</h2>

<?php
    if($cliente == "12087"){
?>

<h4 style="color: red">Aviso Importante!</h4>
<p>Devido ao tamanho dos arquivos ".pdf" das licitações, alguns navegadores podem não conseguir carregar as informações referentes às Licitações na Integra. Caso isso ocorra, recomendamos que seja feito o download do arquivo, clicando com o botão direito do mouse no link e escolhendo a opção 'Salvar link como...'.</p>

<? 
    } 
?>

<ol class="breadcrumb">
    <li><a href="<?= $CAMINHO ?>">In&iacute;cio</a></li>
    <li class="active">Licita&ccedil;&otilde;es na &Iacute;ntegra</li>
</ol>
<?php
    if($cliente == 17){
?>
<button class="btn btn-info" onclick="window.location.href='http://www.ingadigital.com.br/transparencia/index.php?sessao=77c6c8a34ddl77&id_tipo=1,5,6'">Licita&ccedil;&atilde;o de 2014 &agrave; 2017</button>
<?php
    }
?>
<?php
    $stCategoria = $conn->prepare("SELECT * 
                                     FROM licitacao_integra_categoria 
                                    WHERE id_cliente = :id_cliente 
                                      AND status_registro = :status_registro 
                                 ORDER BY descricao DESC");

    $stCategoria->execute(array("id_cliente" => $cliente, "status_registro" => "A"));
    $qryCategoria = $stCategoria->fetchAll();

    if ($_REQUEST['id_subcategoria'] != "") {
        $whereArgs['id_subcategoria'] = $_REQUEST['id_subcategoria'];
        $whereClause = "AND id_subcategoria = :id_subcategoria";
        $link .= "&id_subcategoria=" . $_REQUEST['id_subcategoria'];
    }

    if ($_REQUEST['id_subsubcategoria'] != "") {
        $whereArgs2['id_subsubcategoria'] = $_REQUEST['id_subsubcategoria'];
        $whereClause2 = "AND id_subsubcategoria = :id_subsubcategoria";
        $link2 .= "&id_subsubcategoria=" . $_REQUEST['id_subsubcategoria'];
    }

    // ini_set('display_errors', 1);
    // ini_set('display_startup_errors', 1);
    // error_reporting(E_ALL);
    if (count($qryCategoria)) {
?>

<ul class="treeview">
        
<?php
        foreach ($qryCategoria as $categoria) {
?>

    <li><a href="#"><?= $categoria['descricao'] ?></a>
                
<?php
            $sql = "SELECT * 
                      FROM licitacao_integra 
                     WHERE id_categoria = :id_categoria 
                       AND (id_subcategoria IS NULL 
                        OR id_subcategoria = '')  
                       AND status_registro = :status_registro 
                  ORDER BY titulo DESC";
            $stArquivo = $conn->prepare($sql);

            $args = array(
                "id_categoria" => $categoria['id'],
                "status_registro" => "A"
            );

            // $args = ($whereArgs ? array($args, $whereArgs) : $args);

            $stArquivo->execute($args);
            $qryArquivo = $stArquivo->fetchAll();

            if (count($qryArquivo)) {
?>

        <ul>
            
<?php
                foreach ($qryArquivo as $arquivo) {

                    if ($arquivo['arquivo'] != "") {
                        $linkDocumento = $CAMINHOARQ . "/" . $arquivo['arquivo'];
                        $imagem = "glyphicon-cloud-download";
                        $titulo = empty($arquivo['titulo']) ? "Anexo" : $arquivo['titulo'];

                    } else if ($arquivo['link'] != "") {
                        $linkDocumento = $arquivo['link'];
                        $imagem = "glyphicon-globe";
                        $titulo = empty($arquivo['titulo']) ? "Link" : $arquivo['titulo'];
                    }
?>

    <li><a href="<?= $linkDocumento ?>" target="_blank"><i class="glyphicon <?= $imagem ?>"></i> <?= $titulo ?></a></li>
            
<?php 
                } 
?>
    </ul>
                
<?php 
            } 

            $stSubcategoria = $conn->prepare("SELECT * 
                                                FROM licitacao_integra_subcategoria 
                                               WHERE id_categoria = :id_categoria 
                                                 AND status_registro = :status_registro 
                                            ORDER BY descricao ASC");

            $stSubcategoria->execute(array("id_categoria" => $categoria['id'], "status_registro" => "A"));
            $qrySubcategoria = $stSubcategoria->fetchAll();

            if (count($qrySubcategoria)) {
?>

    <ul>
                        
<?php
                foreach ($qrySubcategoria as $subcategoria) {
?>

    <li><a href="#"><?= $subcategoria['descricao'] ?></a>
                                
<?php
                    if ($cliente == "12088") {
                        $stArquivo = $conn->prepare("SELECT * 
                                                       FROM licitacao_integra 
                                                      WHERE id_subcategoria = :id_subcategoria 
                                                        AND status_registro = :status_registro 
                                                   ORDER BY titulo+0 DESC");
                        $stArquivo->execute(array("id_subcategoria" => $subcategoria['id'], "status_registro" => "A"));
                        $qryArquivo = $stArquivo->fetchAll();
                    } else {
                        $sql = "SELECT * 
                                  FROM licitacao_integra 
                                 WHERE id_subcategoria = :id_subcategoria 
                                   AND (id_subsubcategoria IS NULL 
                                    OR id_subsubcategoria = '') 
                                   AND status_registro = :status_registro 
                              ORDER BY titulo DESC";
                            $stArquivo = $conn->prepare($sql);
                            $args2 = array(
                                "id_subcategoria" => $subcategoria['id'],
                                "status_registro" => "A"
                            );
                            $stArquivo->execute($args2);
                            $qryArquivo = $stArquivo->fetchAll();
                    }
                    if (count($qryArquivo)) {
?>

        <ul>
                                    
                    <?php
                        foreach ($qryArquivo as $arquivo) {
                            if ($arquivo['arquivo'] != "") {

                                $linkDocumento = $CAMINHOARQ . "/" . $arquivo['arquivo'];
                                $imagem = "glyphicon-cloud-download";
                                $titulo = empty($arquivo['titulo']) ? "Anexo" : $arquivo['titulo'];

                            } else if ($arquivo['link'] != "") {

                                $linkDocumento = $arquivo['link'];
                                $imagem = "glyphicon-globe";
                                $titulo = empty($arquivo['titulo']) ? "Link" : $arquivo['titulo'];
                            }
                    ?>
                                            
        <li><a href="<?= $linkDocumento ?>" target="_blank"><i class="glyphicon <?= $imagem ?>"></i> <?= $titulo ?></a></li>
                                        
                    <?php 
                        } 
                    ?>
                                    
        </ul>

                <?php 
                    } 
                    $stSubsubcategoria = $conn->prepare("SELECT * 
                                                           FROM licitacao_integra_subsubcategoria 
                                                          WHERE id_subcategoria = :id_subcategoria 
                                                            AND status_registro = :status_registro 
                                                       ORDER BY descricao ASC");

				    $stSubsubcategoria->execute(array("id_subcategoria" => $subcategoria['id'], "status_registro" => "A"));
				    $qrySubsubcategoria = $stSubsubcategoria->fetchAll();
				    if(count($qrySubsubcategoria)){ 
                ?>
                                    
        <ul>
                                        
                    <?php
				        foreach ($qrySubsubcategoria as $subsubcategoria) {
				    ?>
                                            
        <li><a href="#"><?= $subsubcategoria['descricao'] ?></a>
                                            
                    <?php
                            $stSubsubcategoria = $conn->prepare("SELECT * 
                                                                   FROM licitacao_integra 
                                                                  WHERE id_subsubcategoria = :id_subsubcategoria 
                                                                    AND status_registro = :status_registro 
                                                               ORDER BY titulo DESC");
                            $stSubsubcategoria->execute(array("id_subsubcategoria" => $subsubcategoria['id'], "status_registro" => "A"));
                            $qrySubsubArquivo = $stSubsubcategoria->fetchAll();
                            if (count($qrySubsubArquivo)) {
                    ?>

			<ul>
                                                
                    <?php
                                foreach ($qrySubsubArquivo as $arquivo) {
                                    if ($arquivo['arquivo'] != "") {

                                        $linkDocumento = $CAMINHOARQ . "/" . $arquivo['arquivo'];
                                        $imagem = "glyphicon-cloud-download";
                                        $titulo = empty($arquivo['titulo']) ? "Anexo" : $arquivo['titulo'];

                                    } else if ($arquivo['link'] != "") {

                                        $linkDocumento = $arquivo['link'];
                                        $imagem = "glyphicon-globe";
                                        $titulo = empty($arquivo['titulo']) ? "Link" : $arquivo['titulo'];
                                    }
                    ?>
                                                        
            <li><a href="<?= $linkDocumento ?>" target="_blank"><i class="glyphicon <?= $imagem ?>"></i> <?= $titulo ?></a></li>
                                                    
                    <?php 
                                } 
                    ?>
                                                    
            </ul>
                    
                                                
                        <?php 
                            } 
                        ?>
        </li>
                    
                    <?php
                        } 
                    ?>
                                    
        </ul>
                                
                <? 
                    } 
                ?>
            
    </li>
                        
            <?php 
                } 
            ?>

    </ul>
                
        <?php 
            } 
        ?>
            
</li>
        
<?php 
        } 
?>

</ul>
    
<?php
    }

    $atualizacao = atualizacao("licitacao_integra", $cliente, $conn);
    if ($atualizacao != "") {
?>

    <p class="text-right">
        <small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small>
    </p>
    
<?php
    }