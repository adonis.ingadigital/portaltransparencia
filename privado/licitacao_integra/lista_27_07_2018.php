<h2>Licita&ccedil;&otilde;es na &Iacute;ntegra</h2>
<?php
    if($cliente == 12087){
?>
<h4 style="color: red">Aviso Importante!</h4>
<p>Devido ao tamanho dos arquivos ".pdf" das licitações, alguns navegadores podem não conseguir carregar as informações referentes às Licitações na Integra. Caso isso ocorra, recomendamos que seja feito o download do arquivo, clicando com o botão direito do mouse no link e escolhendo a opção 'Salvar link como...'.</p>
<? } ?>
<ol class="breadcrumb">
    <li><a href="<?= $CAMINHO ?>">In&iacute;cio</a></li>
    <li class="active">Licita&ccedil;&otilde;es na &Iacute;ntegra</li>
</ol>

<?php

$stCategoria = $conn->prepare("SELECT * FROM licitacao_integra_categoria WHERE id_cliente = :id_cliente AND status_registro = :status_registro ORDER BY descricao DESC");

$stCategoria->execute(array("id_cliente" => $cliente, "status_registro" => "A"));
$qryCategoria = $stCategoria->fetchAll();

if ($_REQUEST['id_subcategoria'] != "") {
    $whereArgs['id_subcategoria'] = $_REQUEST['id_subcategoria'];
    $whereClause = "AND id_subcategoria = :id_subcategoria";
    $link .= "&id_subcategoria=" . $_REQUEST['id_subcategoria'];
}
if (count($qryCategoria)) {
    ?>
    <ul class="treeview">
        <?php
        foreach ($qryCategoria as $categoria) {
            ?>
            <li><a href="#"><?= $categoria['descricao'] ?></a>
                <?php
                $sql = "SELECT * FROM licitacao_integra WHERE id_categoria = :id_categoria AND (id_subcategoria IS NULL OR id_subcategoria = '') $whereClause AND status_registro = :status_registro ORDER BY titulo DESC";
                $stArquivo = $conn->prepare($sql);

                $args = array(
                    "id_categoria" => $categoria['id'],
                    "status_registro" => "A"
                );

                $args = ($whereArgs ? array($args, $whereArgs) : $args);

                $stArquivo->execute($args);
                $qryArquivo = $stArquivo->fetchAll();

                if (count($qryArquivo)) {
                    ?>
                    <ul>
                        <?php
                        foreach ($qryArquivo as $arquivo) {

                            if ($arquivo['arquivo'] != "") {

                                $linkDocumento = $CAMINHOARQ . "/" . $arquivo['arquivo'];
                                $imagem = "glyphicon-cloud-download";
                                $titulo = empty($arquivo['titulo']) ? "Anexo" : $arquivo['titulo'];

                            } else if ($arquivo['link'] != "") {

                                $linkDocumento = $arquivo['link'];
                                $imagem = "glyphicon-globe";
                                $titulo = empty($arquivo['titulo']) ? "Link" : $arquivo['titulo'];
                            }
                            ?>
                            <li><a href="<?= $linkDocumento ?>" target="_blank"><i
                                        class="glyphicon <?= $imagem ?>"></i> <?= $titulo ?></a></li>
                        <?php } ?>
                    </ul>
                <?php } ?>
                <?php
                $stSubcategoria = $conn->prepare("SELECT * FROM licitacao_integra_subcategoria WHERE id_categoria = :id_categoria AND status_registro = :status_registro ORDER BY descricao ASC");

                $stSubcategoria->execute(array("id_categoria" => $categoria['id'], "status_registro" => "A"));
                $qrySubcategoria = $stSubcategoria->fetchAll();

                if (count($qrySubcategoria)) {
                    ?>
                    <ul>
                        <?php
                        foreach ($qrySubcategoria as $subcategoria) {
                            ?>
                            <li><a href="#"><?= $subcategoria['descricao'] ?></a>
                                <?php
                                if ($cliente == "12088") {
                                    $stArquivo = $conn->prepare("SELECT * FROM licitacao_integra WHERE id_subcategoria = :id_subcategoria AND status_registro = :status_registro ORDER BY titulo+0 DESC");
                                    $stArquivo->execute(array("id_subcategoria" => $subcategoria['id'], "status_registro" => "A"));
                                    $qryArquivo = $stArquivo->fetchAll();
                                } else {
                                    $stArquivo = $conn->prepare("SELECT * FROM licitacao_integra WHERE id_subcategoria = :id_subcategoria AND status_registro = :status_registro ORDER BY titulo DESC");
                                    $stArquivo->execute(array("id_subcategoria" => $subcategoria['id'], "status_registro" => "A"));
                                    $qryArquivo = $stArquivo->fetchAll();
                                }
                                if (count($qryArquivo)) {
                                    ?>
                                    <ul>
                                        <?php
                                        foreach ($qryArquivo as $arquivo) {

                                            if ($arquivo['arquivo'] != "") {

                                                $linkDocumento = $CAMINHOARQ . "/" . $arquivo['arquivo'];
                                                $imagem = "glyphicon-cloud-download";
                                                $titulo = empty($arquivo['titulo']) ? "Anexo" : $arquivo['titulo'];

                                            } else if ($arquivo['link'] != "") {

                                                $linkDocumento = $arquivo['link'];
                                                $imagem = "glyphicon-globe";
                                                $titulo = empty($arquivo['titulo']) ? "Link" : $arquivo['titulo'];
                                            }
                                            ?>
                                            <li><a href="<?= $linkDocumento ?>" target="_blank"><i
                                                        class="glyphicon <?= $imagem ?>"></i> <?= $titulo ?></a></li>
                                        <?php } ?>
                                    </ul>
                                <?php } ?>
                            </li>
                        <?php } ?>

                    </ul>
                <?php } ?>
            </li>
        <?php } ?>
    </ul>
    <?php
}

$atualizacao = atualizacao("licitacao_integra", $cliente, $conn);
if ($atualizacao != "") {
    ?>
    <p class="text-right">
        <small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima
                atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small>
    </p>
    <?php
}