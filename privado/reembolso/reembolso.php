<h2>Reembolso</h2>
<ol class="breadcrumb">
	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
	<li class="active">Reembolso</li>
</ol>

<ul class="treeview">
<?php
$streembolso = $conn->prepare("SELECT * FROM reembolso WHERE id_cliente = :id_cliente AND (id_categoria IS NULL OR id_categoria = '') AND status_registro = :status_registro ORDER BY titulo");
$streembolso->execute(array("id_cliente" => $cliente, "status_registro" => "A"));
$qryreembolso = $streembolso->fetchAll();

if(count($qryreembolso)) {

	foreach($qryreembolso as $reembolso) {

		if($reembolso['arquivo'] != "") {

			$linkreembolso = $CAMINHOARQ . "/" . $reembolso['arquivo'];
			$targetreembolso = "_blank";
			$imagem = "glyphicon-cloud-download";

		} else {

			$linkreembolso = "#";
			$targetreembolso = "_self";
			$imagem = "glyphicon glyphicon-chevron-right";

		}
	?>
	<li>
		<a href="<?= $linkreembolso ?>" target="<?= $targetreembolso ?>"><span class="text-primary"><i class="glyphicon <?= $imagem ?>"></i>&nbsp;&nbsp;<?= $reembolso['titulo'] ?></span></a><br>
		<?php if($reembolso['data'] != "") { ?><strong style="margin-left: 22px;">Data:</strong> <?= formata_data($reembolso['data']) ?><br><?php } ?>
        <?php if($reembolso['data_complementar'] != "") { ?><strong style="margin-left: 22px;">Descri&ccedil;&atilde;o:</strong> <?= $reembolso['data_complementar'] ?><br><?php } ?>
        <?php if($reembolso['lotacao'] != "") { ?><strong style="margin-left: 22px;">Cargo / Lota&ccedil;&atilde;o:</strong> <?= $reembolso['lotacao'] ?><br><?php } ?>
        <?php if($reembolso['motivo'] != "") { ?><strong style="margin-left: 22px;">Motivo:</strong> <?= $reembolso['motivo'] ?><br><?php } ?>
        <?php if($reembolso['valor'] != "") { ?><strong style="margin-left: 22px;">Valor:</strong> <?= $reembolso['valor'] ?><br><?php } ?>
        <?php if($reembolso['destino'] != "") { ?><strong style="margin-left: 22px;">Destino:</strong> <?= verifica($reembolso['destino']) ?><?php } ?>
	</li>
	<?php
	}
}

$stCategoria = $conn->prepare("SELECT * FROM reembolso_categoria WHERE id_cliente = :id_cliente AND status_registro = :status_registro ORDER BY id DESC");
$stCategoria->execute(array("id_cliente" => $cliente, "status_registro" => "A"));
$qryCategoria = $stCategoria->fetchAll();

if(count($qryCategoria)) {

	foreach ($qryCategoria as $categoria) {
	?>
	<li><a href="#"><?= $categoria['descricao'] ?></a>
		<?php
		$streembolso = $conn->prepare("SELECT * FROM reembolso WHERE id_cliente = :id_cliente AND id_categoria = :id_categoria AND (id_subcategoria IS NULL OR id_subcategoria = '') AND status_registro = :status_registro ORDER BY titulo");
		$streembolso->execute(array("id_cliente" => $cliente, "id_categoria" => $categoria['id'], "status_registro" => "A"));
		$qryreembolso = $streembolso->fetchAll();

		if(count($qryreembolso)) {
		?>
		<ul>
			<?php
			foreach($qryreembolso as $reembolso) {

				if($reembolso['arquivo'] != "") {

					$linkreembolso = $CAMINHOARQ . "/" . $reembolso['arquivo'];
					$targetreembolso = "_blank";
					$imagem = "glyphicon-cloud-download";

				} else {

					$linkreembolso = "#";
					$targetreembolso = "_self";
					$imagem = "glyphicon glyphicon-chevron-right";

				}
			?>
			<li>
				<a href="<?= $linkreembolso ?>" target="<?= $targetreembolso ?>"><span class="text-primary"><i class="glyphicon <?= $imagem ?>"></i>&nbsp;&nbsp;<?= $reembolso['titulo'] ?></span></a><br>
				<?php if($reembolso['data'] != "") { ?><strong style="margin-left: 22px;">Data:</strong> <?= formata_data($reembolso['data']) ?><br><?php } ?>
                <?php if($reembolso['data_complementar'] != "") { ?><strong style="margin-left: 22px;">Descri&ccedil;&atilde;o:</strong> <?= $reembolso['data_complementar'] ?><br><?php } ?>
                <?php if($reembolso['lotacao'] != "") { ?><strong style="margin-left: 22px;">Cargo / Lota&ccedil;&atilde;o:</strong> <?= $reembolso['lotacao'] ?><br><?php } ?>
                <?php if($reembolso['motivo'] != "") { ?><strong style="margin-left: 22px;">Motivo:</strong> <?= $reembolso['motivo'] ?><br><?php } ?>
                <?php if($reembolso['valor'] != "") { ?><strong style="margin-left: 22px;">Valor:</strong> <?= $reembolso['valor'] ?><br><?php } ?>
                <?php if($reembolso['destino'] != "") { ?><strong style="margin-left: 22px;">Destino:</strong> <?= verifica($reembolso['destino']) ?><?php } ?>
			</li>
			<?php } ?>
		</ul>
		<?php } ?>
		<?php
		$stSubcategoria = $conn->prepare("SELECT * FROM reembolso_subcategoria WHERE id_categoria = :id_categoria AND status_registro = :status_registro ORDER BY id ASC");
		$stSubcategoria->execute(array("id_categoria" => $categoria['id'], "status_registro" => "A"));
		$qrySubcategoria = $stSubcategoria->fetchAll();

		if(count($qrySubcategoria)) {
		?>
		<ul>
			<?php
			foreach ($qrySubcategoria as $subcategoria) {
			?>
			<li><a href="#"><?= $subcategoria['descricao'] ?></a>
				<?php
				$streembolso = $conn->prepare("SELECT * FROM reembolso WHERE id_cliente = :id_cliente AND id_subcategoria = :id_subcategoria AND status_registro = :status_registro ORDER BY titulo");
				$streembolso->execute(array("id_cliente" => $cliente, "id_subcategoria" => $subcategoria['id'], "status_registro" => "A"));
				$qryreembolso = $streembolso->fetchAll();

				if(count($qryreembolso)) {
				?>
				<ul>
					<?php
					foreach($qryreembolso as $reembolso) {

						if($reembolso['arquivo'] != "") {

							$linkreembolso = $CAMINHOARQ . "/" . $reembolso['arquivo'];
							$targetreembolso = "_blank";
							$imagem = "glyphicon-cloud-download";

						} else {

							$linkreembolso = "#";
							$targetreembolso = "_self";
							$imagem = "glyphicon glyphicon-chevron-right";

						}
					?>
					<li>
						<a href="<?= $linkreembolso ?>" target="<?= $targetreembolso ?>"><span class="text-primary"><i class="glyphicon <?= $imagem ?>"></i>&nbsp;&nbsp;<?= $reembolso['titulo'] ?></span></a><br>
						<?php if($reembolso['data'] != "") { ?><strong style="margin-left: 22px;">Data:</strong> <?= formata_data($reembolso['data']) ?><br><?php } ?>
		                <?php if($reembolso['data_complementar'] != "") { ?><strong style="margin-left: 22px;">Descri&ccedil;&atilde;o:</strong> <?= $reembolso['data_complementar'] ?><br><?php } ?>
		                <?php if($reembolso['lotacao'] != "") { ?><strong style="margin-left: 22px;">Cargo / Lota&ccedil;&atilde;o:</strong> <?= $reembolso['lotacao'] ?><br><?php } ?>
		                <?php if($reembolso['motivo'] != "") { ?><strong style="margin-left: 22px;">Motivo:</strong> <?= $reembolso['motivo'] ?><br><?php } ?>
		                <?php if($reembolso['valor'] != "") { ?><strong style="margin-left: 22px;">Valor:</strong> <?= $reembolso['valor'] ?><br><?php } ?>
		                <?php if($reembolso['destino'] != "") { ?><strong style="margin-left: 22px;">Destino:</strong> <?= verifica($reembolso['destino']) ?><?php } ?>
					</li>
					<?php } ?>
				</ul>
				<?php } ?>
			</li>
			<?php } ?>
		</ul>
		<?php } ?>
	</li>
	<?php } ?>
<?php } ?>
</ul>

<?php
$atualizacao = atualizacao("reembolso", $cliente, $conn);
if($atualizacao != "") {
?>
<p class="text-right"><small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small></p>
<?php
}