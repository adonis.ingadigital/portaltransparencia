<h2>Servidores comissionados</h2>
<ol class="breadcrumb">
	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
	<li class="active">Servidores comissionados</li>
</ol>

<?php
$stCategoria = $conn->prepare("SELECT * FROM servidores_comissionados_categoria WHERE id_cliente = :id_cliente AND status_registro = :status_registro ORDER BY id DESC");
$stCategoria->execute(array("id_cliente" => $cliente, "status_registro" => "A"));
$qryCategoria = $stCategoria->fetchAll();

if(count($qryCategoria)) {
?>
<ul class="treeview">
	<?php
	foreach ($qryCategoria as $categoria) {
	?>
	<li><a href="#"><?= $categoria['descricao'] ?></a>
		<?php

		if ($cliente == "46"){
			$stArquivo = $conn->prepare("SELECT * FROM servidores_comissionados WHERE id_categoria = :id_categoria AND (id_subcategoria IS NULL OR id_subcategoria = '') AND status_registro = :status_registro ORDER BY id DESC");
			$stArquivo->execute(array("id_categoria" => $categoria['id'], "status_registro" => "A"));
			$qryArquivo = $stArquivo->fetchAll();
		}else {
			$stArquivo = $conn->prepare("SELECT * FROM servidores_comissionados WHERE id_categoria = :id_categoria AND (id_subcategoria IS NULL OR id_subcategoria = '') AND status_registro = :status_registro ORDER BY id DESC");
			$stArquivo->execute(array("id_categoria" => $categoria['id'], "status_registro" => "A"));
			$qryArquivo = $stArquivo->fetchAll();
		}

		if(count($qryArquivo)) {
		?>
		<ul>
			<?php
			foreach($qryArquivo as $arquivo) {

				if($arquivo['arquivo'] != "") {

					$linkDocumento = $CAMINHOARQ . "/" . $arquivo['arquivo'];
					$imagem = "glyphicon-cloud-download";
					$descricao = empty($arquivo['descricao']) ? "Anexo" : $arquivo['descricao'];

				} else if($arquivo['link'] != "") {

					$linkDocumento = $arquivo['link'];
					$imagem = "glyphicon-globe";
					$descricao = empty($arquivo['descricao']) ? "Link" : $arquivo['descricao'];
				}
			?>
			<li><a href="<?=$linkDocumento ?>" target="_blank"><i class="glyphicon <?= $imagem ?>"></i> <?= $descricao ?></a></li>
			<?php } ?>
		</ul>
		<?php } ?>
		<?php
		$stSubcategoria = $conn->prepare("SELECT * FROM servidores_comissionados_subcategoria WHERE id_categoria = :id_categoria AND status_registro = :status_registro ORDER BY id DESC");
		$stSubcategoria->execute(array("id_categoria" => $categoria['id'], "status_registro" => "A"));
		$qrySubcategoria = $stSubcategoria->fetchAll();

		if(count($qrySubcategoria)) {
		?>
		<ul>
			<?php
			foreach ($qrySubcategoria as $subcategoria) {
			?>
			<li><a href="#"><?= $subcategoria['descricao'] ?></a>
				<?php

				if ($cliente == "46"){
					$stArquivo = $conn->prepare("SELECT * FROM servidores_comissionados WHERE id_subcategoria = :id_subcategoria AND status_registro = :status_registro ORDER BY id DESC");
					$stArquivo->execute(array("id_subcategoria" => $subcategoria['id'], "status_registro" => "A"));
					$qryArquivo = $stArquivo->fetchAll();
				}else {
					$stArquivo = $conn->prepare("SELECT * FROM servidores_comissionados WHERE id_subcategoria = :id_subcategoria AND status_registro = :status_registro ORDER BY id DESC");
					$stArquivo->execute(array("id_subcategoria" => $subcategoria['id'], "status_registro" => "A"));
					$qryArquivo = $stArquivo->fetchAll();
				}

				if(count($qryArquivo)) {
				?>
				<ul>
					<?php
					foreach($qryArquivo as $arquivo) {

						if($arquivo['arquivo'] != "") {

							$linkDocumento = $CAMINHOARQ . "/" . $arquivo['arquivo'];
							$imagem = "glyphicon-cloud-download";
							$descricao = empty($arquivo['descricao']) ? "Anexo" : $arquivo['descricao'];

						} else if($arquivo['link'] != "") {

							$linkDocumento = $arquivo['link'];
							$imagem = "glyphicon-globe";
							$descricao = empty($arquivo['descricao']) ? "Link" : $arquivo['descricao'];
						}
					?>
					<li><a href="<?= $linkDocumento ?>" target="_blank"><i class="glyphicon <?= $imagem ?>"></i> <?= $descricao ?></a></li>
					<?php } ?>
				</ul>
				<?php } ?>
			</li>
			<?php } ?>
		</ul>
		<?php } ?>
	</li>
	<?php } ?>
</ul>
<?php
}

$atualizacao = atualizacao("servidores_comissionados", $cliente, $conn);
if($atualizacao != "") {
?>
<p class="text-right"><small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small></p>
<?php
}