<h2>Compra Direta</h2>
<ol class="breadcrumb">
	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
	<li class="active">Compra Direta</li>
</ol>




<?php if($cliente == "12113") { ?>



<style> u { text-decoration: underline; color: #E08200; } </style>

<p style="font-size: large">
<!-- 
	<strong>Comunicado Importante: a partir de 01/10/2016 este departamento n&atilde;o utilizar&aacute; mais o e-mail <u>licita.ortigueira@gmail.com</u> logo, o e-mail oficial/institucional ser&aacute; <u>licitacao@ortigueira.pr.gov.br</u> assim, toda a comunica&ccedil;&atilde;o via e-mail dever&aacute; ser obrigatoriamente no e-mail: <u>licitacao@ortigueira.pr.gov.br</u> para tanto, este &oacute;rg&atilde;o n&atilde;o ser&aacute; respons&aacute;vel caso ocorram desencontro de informa&ccedil;&otilde;es entre os e-mails informados conforme datas citadas.</strong> -->
	<a href="https://e-gov.betha.com.br/transparencia/01031-014/con_comprasdiretas.faces?mun=F8aqbCbXOyo7ypFgBbJYhvWnddJxIGNT"><strong>Para demais informações, acesse o link direto da Contabilidade: <u>https://e-gov.betha.com.br/transparencia/01031-014/con_comprasdiretas.faces?mun=F8aqbCbXOyo7ypFgBbJYhvWnddJxIGNT</u> </strong></a>

</p>


<?php } ?>




<?php
$stCategoria = $conn->prepare("SELECT * FROM compra_direta_categoria WHERE id_cliente = :id_cliente AND status_registro = :status_registro ORDER BY id DESC");
$stCategoria->execute(array("id_cliente" => $cliente, "status_registro" => "A"));
$qryCategoria = $stCategoria->fetchAll();

if(count($qryCategoria)) {
?>
<ul class="treeview">
	<?php
	foreach ($qryCategoria as $categoria) {
	?>
	<li><a href="#"><?= $categoria['descricao'] ?></a>
		<?php
		$stArquivo = $conn->prepare("SELECT * FROM compra_direta WHERE id_categoria = :id_categoria AND (id_subcategoria IS NULL OR id_subcategoria = '') AND status_registro = :status_registro ORDER BY titulo DESC");
		$stArquivo->execute(array("id_categoria" => $categoria['id'], "status_registro" => "A"));
		$qryArquivo = $stArquivo->fetchAll();

		if(count($qryArquivo)) {
		?>
		<ul>
			<?php
			foreach($qryArquivo as $arquivo) {

				if($arquivo['arquivo'] != "") {

					$linkDocumento = $CAMINHOARQ . "/" . $arquivo['arquivo'];
					$imagem = "glyphicon-cloud-download";
					$titulo = empty($arquivo['titulo']) ? "Anexo" : $arquivo['titulo'];

				} else if($arquivo['link'] != "") {

					$linkDocumento = $arquivo['link'];
					$imagem = "glyphicon-globe";
					$titulo = empty($arquivo['titulo']) ? "Link" : $arquivo['titulo'];
				}
			?>
			<li><a href="<?=$linkDocumento ?>" target="_blank"><i class="glyphicon <?= $imagem ?>"></i> <?= $titulo ?></a></li>
			<?php } ?>
		</ul>
		<?php } ?>
		<?php
		$stSubcategoria = $conn->prepare("SELECT * FROM compra_direta_subcategoria WHERE id_categoria = :id_categoria AND status_registro = :status_registro ORDER BY id DESC");
		$stSubcategoria->execute(array("id_categoria" => $categoria['id'], "status_registro" => "A"));
		$qrySubcategoria = $stSubcategoria->fetchAll();

		if(count($qrySubcategoria)) {
		?>
		<ul>
			<?php
			foreach ($qrySubcategoria as $subcategoria) {
			?>
			<li><a href="#"><?= $subcategoria['descricao'] ?></a>
				<?php
				$stArquivo = $conn->prepare("SELECT * FROM compra_direta WHERE id_subcategoria = :id_subcategoria AND status_registro = :status_registro ORDER BY titulo DESC");
				$stArquivo->execute(array("id_subcategoria" => $subcategoria['id'], "status_registro" => "A"));
				$qryArquivo = $stArquivo->fetchAll();

				if(count($qryArquivo)) {
				?>
				<ul>
					<?php
					foreach($qryArquivo as $arquivo) {

						if($arquivo['arquivo'] != "") {

							$linkDocumento = $CAMINHOARQ . "/" . $arquivo['arquivo'];
							$imagem = "glyphicon-cloud-download";
							$titulo = empty($arquivo['titulo']) ? "Anexo" : $arquivo['titulo'];

						} else if($arquivo['link'] != "") {

							$linkDocumento = $arquivo['link'];
							$imagem = "glyphicon-globe";
							$titulo = empty($arquivo['titulo']) ? "Link" : $arquivo['titulo'];
						}
					?>
					<li><a href="<?= $linkDocumento ?>" target="_blank"><i class="glyphicon <?= $imagem ?>"></i> <?= $titulo ?></a></li>
					<?php } ?>
				</ul>
				<?php } ?>
			</li>
			<?php } ?>
		</ul>
		<?php } ?>
	</li>
	<?php } ?>
</ul>
<?php
}

$atualizacao = atualizacao("compra_direta", $cliente, $conn);
if($atualizacao != "") {
?>
<p class="text-right"><small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small></p>
<?php
}