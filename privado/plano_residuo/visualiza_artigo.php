<?php
$id = secure($_GET['id']);

$stPlano = $conn->prepare("SELECT pref_plano_residuo.*, pref_plano_residuo_categoria.categoria
							 FROM pref_plano_residuo, pref_plano_residuo_categoria
							WHERE pref_plano_residuo.id_categoria = pref_plano_residuo_categoria.id
							  AND pref_plano_residuo.id = :id
							  AND pref_plano_residuo.status_registro = :status_registro");

$stPlano->execute(array("id" => $id, "status_registro" => "A"));
$planoResiduo = $stPlano->fetch();
?>
<h2>Plano de Gerenciamento de Res&iacute;duos <span class="label label-default"><?= $planoResiduo['categoria'] ?></span></h2>
<ol class="breadcrumb">
	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
	<li><a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$planoResiduo.$complemento); ?>">Plano de Gerenciamento de Res&iacute;duos</a></li>
	<li class="active"><?= $planoResiduo['titulo'] ?></li>
</ol>

<h3><?= $planoResiduo['titulo'] ?></h3>

<div class="row">
	<?php if($planoResiduo['artigo'] != "") { ?>
	<div class="col-md-9 col-sm-8">
		<?= verifica($planoResiduo['artigo']) ?>
	</div>
	<?php } ?>

	<div class="col-md-3 col-sm-4">
		<?php
		$stFoto = $conn->prepare("SELECT * FROM pref_plano_residuo_foto WHERE id_artigo = :id ORDER BY ordem ASC, id DESC");
		$stFoto->execute(array("id" => $planoResiduo['id']));
		$qryFoto = $stFoto->fetchAll();

		if(count($qryFoto)) {

			foreach ($qryFoto as $indice => $foto) {
			?>
			<a href="<?=$CAMINHOIMG ?>/gd_<?= verifica($foto['foto']) ?>" class="thumbnail fancybox-thumbs <?php if($indice > 0) echo "hidden"; ?>" data-fancybox-group="thumb" data-toggle="tooltip" data-placement="top" title="<?= verifica($foto['legenda']) ?> - <?= verifica($foto['credito']) ?>">
				<img src="<?=$CAMINHOIMG ?>/tb_<?= verifica($foto['foto']) ?>" alt="<?= verifica($foto['legenda']) ?> - <?= verifica($foto['credito']) ?>">
			</a>
		<?php
			}
		}

		$stVideo = $conn->prepare("SELECT * FROM pref_plano_residuo_video WHERE id_artigo = :id ORDER BY ordem ASC, id DESC");
		$stVideo->execute(array("id" => $planoResiduo['id']));
		$qryVideo = $stVideo->fetchAll();

		if(count($qryVideo)) {

			foreach ($qryVideo as $indice => $buscaVideo) {

				$video = video($buscaVideo['link']);
			?>
			<a href="<?=$video['embed'] ?>" class="thumbnail fancybox-media <?php if($indice > 0) echo "hidden"; ?>" data-fancybox-group="fancybox-media" data-toggle="tooltip" data-placement="bottom" title="<?= verifica(empty($buscaVideo['titulo']) ? $video['title'] : $buscaVideo['titulo']) ?>">
				<img src="<?= $video['img'] ?>" alt="<?= verifica(empty($buscaVideo['titulo']) ? $video['title'] : $buscaVideo['titulo']) ?>">
			</a>
		<?php
			}
		}

		$stAnexo = $conn->prepare("SELECT * FROM pref_plano_residuo_anexo WHERE id_artigo = :id ORDER BY ordem ASC, id DESC");
		$stAnexo->execute(array("id" => $planoResiduo['id']));
		$qryAnexo = $stAnexo->fetchAll();

		if(count($qryAnexo)) {
		?>
		<h4 class="text-primary"><i class="glyphicon glyphicon-cloud-download"></i>&nbsp;Anexos</strong></h4>
		<?php foreach ($qryAnexo as $anexo) { ?>
			<p><strong><a href="<?= $CAMINHOARQ ?>/<?= $anexo['arquivo'] ?>" target="_blank"><i class="glyphicon glyphicon-cloud-download"></i> <?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?></a></strong></p>
			<?php } ?>
		<?php } ?>
	</div>
</div>