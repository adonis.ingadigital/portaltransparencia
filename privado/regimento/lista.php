<h2>Regimento Interno</h2>
<ol class="breadcrumb">
	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
	<li class="active">Regimento Interno</li>
</ol>

<?php
$stRegimento = $conn->prepare("SELECT * FROM camara_regimento_interno WHERE id_cliente = :id_cliente AND status_registro = :status_registro ORDER BY id");
$stRegimento->execute(array("id_cliente" => $cliente, "status_registro" => "A"));
$qryRegimento = $stRegimento->fetchAll();

if(count($qryRegimento)) {
?>
<div class="table-responsive">
	<table class="table table-striped table-hover table-bordered table-condensed">
		<tr>
			<th class="col-sm-1 col-xs-2">Arquivo</th>
			<th>Descri&ccedil;&atilde;o</th>
		</tr>
		<?php foreach ($qryRegimento as $regimento) { ?>
		<tr>
			<td>
				<?php if($regimento['arquivo'] != "") { ?>
				<a href="<?= $CAMINHOARQ ?>/<?= $regimento['arquivo'] ?>" class="btn btn-warning" data-toggle="tooltip" data-placement="right" title="Baixar arquivo" target="_blank">
					<i class="glyphicon glyphicon-cloud-download"></i>
				</a>
				<?php } else { ?>
				<button type="button" class="btn btn-warning" disabled><i class="glyphicon glyphicon-cloud-download"></i></button>
				<?php } ?>
			</td>
			<td class="col-sm-11  col-xs-10"><?= verifica($regimento['descricao']) ?></td>
		</tr>
		<?php } ?>
	</table>
</div>
<?php
}

$atualizacao = atualizacao("regimento_interno", $cliente, $conn);
if($atualizacao != "") {
?>
<p class="text-right"><small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small></p>
<?php
}