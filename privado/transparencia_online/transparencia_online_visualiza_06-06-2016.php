<?php
$id = secure($_GET['id']);

if($_GET['origem'] != "") {

	$link = $configuracaoTransparencia["caminho_$_GET[origem]"];

} else {

	$stLink = $conn->prepare("SELECT * FROM link_transparencia WHERE id_cliente = :id_cliente AND status_registro = :status_registro AND id = :id");
	$stLink->execute(array("id_cliente" => $cliente, "status_registro" => "A", "id" => $id));
	$buscaLink = $stLink->fetch();
	$link = $buscaLink['link'];

	?>
	<h2>Transpar&ecirc;ncia Online</h2>
	<ol class="breadcrumb">
		<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
		<li><a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$transparenciaOnline.$complemento); ?>">Transpar&ecirc;ncia Online</a></li>
		<li class="active"><?= $buscaLink['descricao'] ?></li>
	</ol>
	<?php

}

if(empty($link)) {
?>
<div class="jumbotron">
	<h1>Link n&atilde;o encontrado!</h1>
	<h3>A p&aacute;gina solicitada n&atilde;o foi localizada. Verifique a ortografia.</h3>
</div>
<?php
} else {
?>
<iframe
	class="frame"
	frameborder="0"
	scrolling="yes"
	marginheight="0"
	marginwidth="0"
   	src="<?= $link ?>">
</iframe>
<?php
}