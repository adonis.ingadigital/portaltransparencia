<h2>Transpar&ecirc;ncia Online</h2>
<ol class="breadcrumb">
	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
	<li class="active">Transpar&ecirc;ncia Online</li>
</ol>

<ul class="treeview">
<?php
$stCategoria = $conn->prepare("SELECT * FROM link_transparencia_categoria WHERE id_cliente = :id_cliente AND status_registro = :status_registro ORDER BY id DESC");
$stCategoria->execute(array("id_cliente" => $cliente, "status_registro" => "A"));
$qryCategoria = $stCategoria->fetchAll();

if(count($qryCategoria)) {

	foreach ($qryCategoria as $categoria) {
?>
	<li><a href="#"><?= $categoria['descricao'] ?></a>
		<?php
		$stLink = $conn->prepare("SELECT * FROM link_transparencia WHERE id_categoria = :id_categoria AND status_registro = :status_registro ORDER BY descricao DESC");
		$stLink->execute(array("id_categoria" => $categoria['id'], "status_registro" => "A"));
		$qryLink = $stLink->fetchAll();

		if(count($stLink)) {
		?>
		<ul>
			<?php
			foreach($qryLink as $link) {
			?>
			<li><a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$transparenciaOnlineVisualiza.$complemento); ?>&id=<?= verifica($link['id']); ?>&redir=link"><i class="glyphicon glyphicon-globe"></i> <?= $link['descricao'] ?></a></li>
			<?php } ?>
		</ul>
		<?php } ?>
	</li>
	<?php } ?>
<?php } ?>
<?php
$stLink = $conn->prepare("SELECT * FROM link_transparencia WHERE id_cliente = :id_cliente AND (id_categoria IS NULL OR id_categoria = '') AND status_registro = :status_registro ORDER BY descricao DESC");
$stLink->execute(array("id_cliente" => $cliente, "status_registro" => "A"));
$qryLink = $stLink->fetchAll();

if(count($stLink)) {

	foreach($qryLink as $link) {
	?>
	<li><a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$transparenciaOnlineVisualiza.$complemento); ?>&id=<?= verifica($link['id']); ?>&redir=link"><i class="glyphicon glyphicon-globe"></i> <?= $link['descricao'] ?></a></li>
	<?php } ?>
<?php } ?>
</ul>