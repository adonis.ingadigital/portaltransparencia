<h2>Endere&ccedil;os Oficiais</h2>
<ol class="breadcrumb">
	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
	<li class="active">Endere&ccedil;os Oficiais</li>
</ol>

<?php
$stCategoria = $conn->prepare("SELECT * FROM endereco_oficial_categoria WHERE id_cliente = :id_cliente AND status_registro = :status_registro ORDER BY id DESC");
$stCategoria->execute(array("id_cliente" => $cliente, "status_registro" => "A"));
$qryCategoria = $stCategoria->fetchAll();

if(count($qryCategoria)) {
?>
<ul class="treeview">
	<?php
	foreach ($qryCategoria as $categoria) {
	?>
	<li><a href="#"><?= $categoria['descricao'] ?></a>
		<?php
		$stEndereco = $conn->prepare("SELECT endereco_oficial.*, municipio.nome municipio, estado.uf estado
									   FROM endereco_oficial
								  LEFT JOIN municipio ON endereco_oficial.id_municipio = municipio.id
								  LEFT JOIN estado ON municipio.id_estado = estado.id
									  WHERE endereco_oficial.id_categoria = :id_categoria
									  	AND endereco_oficial.status_registro = :status_registro
								   ORDER BY endereco_oficial.titulo");
		$stEndereco->execute(array("id_categoria" => $categoria['id'], "status_registro" => "A"));
		$qryEndereco = $stEndereco->fetchAll();

		if(count($qryEndereco)) {
		?>
		<ul>
			<?php
			foreach($qryEndereco as $endereco) {
			?>
			<li style="font-weight: normal;">
				<address>
					<strong><?= $endereco['titulo'] ?></strong><br>
					<i class="glyphicon glyphicon-road"></i> <?= $endereco['endereco'] ?>, <?= $endereco['bairro'] ?> - <?= $endereco['complemento'] != "" ? "$endereco[complemento] - " : "" ?><br>
					<i class="glyphicon glyphicon-map-marker"></i> <strong>CEP:</strong> <?= $endereco['cep'] ?> - <?= $endereco['municipio'] ?> - <?= $endereco['estado'] ?><br>
					<? if($endereco['telefone'] != "") { ?><i class="glyphicon glyphicon-phone-alt"></i> <strong>Telefone(s):</strong> <?= $endereco['telefone'] ?><br><? } ?>
					<? if($endereco['email'] != "") { ?><i class="glyphicon glyphicon-envelope"></i> <strong>E-mail:</strong> <?= $endereco['email'] ?><br><? } ?>
					<? if($endereco['site'] != "") { ?><i class="glyphicon glyphicon-globe"></i> <strong>Website:</strong> <a href="<?= $endereco['site'] ?>" target="_blank"><?= $endereco['site'] ?></a><br><? } ?>
					<? if($endereco['horario_atendimento'] != "") { ?><i class="glyphicon glyphicon glyphicon-time"></i> <strong>Hor&aacute;rio de Atendimento:</strong> <?= $endereco['horario_atendimento'] ?><? } ?>
				</address>
			</li>
			<?php } ?>
		</ul>
		<?php } ?>
	</li>
	<?php } ?>
</ul>
<?php
}

$atualizacao = atualizacao("endereco_oficial", $cliente, $conn);
if($atualizacao != "") {
?>
<p class="text-right"><small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small></p>
<?php
}