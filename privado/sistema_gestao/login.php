<h2>Ouvidoria <span class="label label-default">Login</span></h2>
<ol class="breadcrumb">
	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
	<li class="active">Ouvidoria</li>
</ol>

<?php
include "../../privado/transparencia/recaptchalib.php";

$tela = empty($_GET['tela']) ? $sistemaGestaoOuvidoria : $_GET['tela'];

if($_POST['acao'] == "cadastrar") {

	foreach ($_POST as $campo => $valor) {
		$$campo = secure($valor);
	}

	if($tipo == "F") {

		$sql = "SELECT id FROM geral_contribuinte WHERE id_cliente = :id_cliente AND cpf = :cpf_cnpj AND status_registro = :status_registro";
		$cpf_cnpj = $cpf;

	} else {

		$sql = "SELECT id FROM geral_contribuinte WHERE id_cliente = :id_cliente AND cnpj = :cpf_cnpj AND status_registro = :status_registro";
		$cpf_cnpj = $cnpj;

	}

	$stConf = $conn->prepare($sql);
	$stConf->execute(array("id_cliente" => $cliente, "cpf_cnpj" => $cpf_cnpj, "status_registro" => "A"));
	$conf = $stConf->fetchAll();

	$cap = new GoogleRecaptcha();
	$verified = $cap->VerifyCaptcha($_POST['g-recaptcha-response']);

	if(!$verified) {

		echo'<p class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				O Captcha n&atilde;o foi resolvido! Verifique.
			</p>';

	} else if($tipo == "F" && count($conf) > 0) {

		echo'<p class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				Aten&ccedil;&atildeo! J&aacute; existe um cadastro com este CPF.
			</p>';

	} else if($tipo == "J" && count($conf) > 0) {

		echo'<p class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				Aten&ccedil;&atildeo! J&aacute; existe um cadastro com este CNPJ.
			</p>';

	} else if(empty($nome)) {

		echo'<p class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				Preencha o campo Nome!
			</p>';

	} else if($tipo == "F" && validaCPF($cpf) == false) {

		echo'<p class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				O CPF informado &eacute; inv&aacute;lido!
			</p>';

	} else if($tipo == "J" && validaCNPJ($cnpj) == false) {

		echo'<p class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				O CNPJ informado &eacute; inv&aacute;lido!
			</p>';

	} else if(empty($email) || filter_var($email, FILTER_VALIDATE_EMAIL) === false) {

		echo'<p class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				Forne&ccedil;a um e-mail v&aacute;lido!
			</p>';

	} else if(empty($senha) || ($senha != $conf_senha)) {

		echo'<p class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				As senhas informadas n&atilde;o conferem!
			</p>';

	} else if(empty($endereco) || empty($bairro) || empty($cep)) {

		echo'<p class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				Informe seu endere&ccedil;o, bairro e CEP!
			</p>';

	} else if(empty($id_municipio)) {

		echo'<p class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				Informe seu estado e sua cidade!
			</p>';

	} else {

		$stCadastro = $conn->prepare("INSERT INTO geral_contribuinte (id_cliente,
																	  id_municipio,
																	  id_escolaridade,
																	  id_renda_familiar,
																	  tipo,
																	  nome,
																	  endereco,
																	  complemento,
																	  bairro,
																	  cep,
																	  rg,
																	  cpf,
																	  cnpj,
																	  telefone_fixo,
																	  telefone_celular,
																	  email,
																	  senha)
											VALUES (?,
													?,
													?,
													?,
													?,
													?,
													?,
													?,
													?,
													?,
													?,
													?,
													?,
													?,
													?,
													?,
													?)");

		$id_escolaridade = empty($id_escolaridade) ? NULL : $id_escolaridade;
		$id_renda_familiar = empty($id_renda_familiar) ? NULL : $id_renda_familiar;

		$stCadastro->bindParam(1, $cliente, PDO::PARAM_INT);
		$stCadastro->bindParam(2, $id_municipio, PDO::PARAM_INT);
		$stCadastro->bindParam(3, $id_escolaridade, PDO::PARAM_INT);
		$stCadastro->bindParam(4, $id_renda_familiar, PDO::PARAM_INT);
		$stCadastro->bindParam(5, $tipo, PDO::PARAM_STR);
		$stCadastro->bindParam(6, $nome, PDO::PARAM_STR);
		$stCadastro->bindParam(7, $endereco, PDO::PARAM_STR);
		$stCadastro->bindParam(8, $complemento_endereco, PDO::PARAM_STR);
		$stCadastro->bindParam(9, $bairro, PDO::PARAM_STR);
		$stCadastro->bindParam(10, $cep, PDO::PARAM_STR);
		$stCadastro->bindParam(11, $rg, PDO::PARAM_STR);
		$stCadastro->bindParam(12, $cpf, PDO::PARAM_STR);
		$stCadastro->bindParam(13, $cnpj, PDO::PARAM_STR);
		$stCadastro->bindParam(14, $telefone_fixo, PDO::PARAM_STR);
		$stCadastro->bindParam(15, $telefone_celular, PDO::PARAM_STR);
		$stCadastro->bindParam(16, $email, PDO::PARAM_STR);
		$stCadastro->bindParam(17, $senha, PDO::PARAM_STR);

		$cad = $stCadastro->execute();

		if($cad) {

			$id_contribuinte = $conn->lastInsertId();
			$login = muda_nome($nome, 4) . str_pad($id_contribuinte, 4, "0", STR_PAD_LEFT);
			$stLogin = $conn->prepare("UPDATE geral_contribuinte SET login = :login WHERE id = :id_contribuinte");
			$stLogin->execute(array("login" => $login, "id_contribuinte" => $id_contribuinte));

			$_SESSION['login_sistema_gestao'] = true;
			$_SESSION['id_contribuinte'] = $id_contribuinte;

			echo "<script>window.location='$CAMINHO/index.php?sessao=$sequencia".$tela."$complemento&cadastro=1".($_GET['id'] ? "&id=" . secure($_GET['id']) : "")."'</script>";

		} else {

			echo'<p class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					N&atilde;o foi poss&iacute;vel realizar seu cadastro! Tente novamente mais tarde.
				</p>';
		}

	}

} else if($_POST['acao'] == "logar") {

	foreach ($_POST as $campo => $valor) {
		$$campo = secure($valor);
	}

	$cap = new GoogleRecaptcha();
	$verified = $cap->VerifyCaptcha($_POST['g-recaptcha-response']);

	if(!$verified) {

		echo'<p class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				O Captcha n&atilde;o foi resolvido! Verifique.
			</p>';

	} else {

		$login_login = ($tipoLogin == "cpf" ? $loginCPF : $loginCNPJ);

		$stLogin = $conn->prepare("SELECT id, nome FROM geral_contribuinte WHERE id_cliente = :cliente AND ($tipoLogin = :login OR $tipoLogin = :login2) AND senha = :senha AND status_registro = :status_registro LIMIT 1");
		$stLogin->execute(array("cliente" => $cliente, "login" => $login_login, "login2" => preg_replace("#[^0-9]#", "", $login_login), "senha" => $login_senha, "status_registro" => "A"));
		$buscaLogin = $stLogin->fetch();

		if($buscaLogin["id"] != "") {

			$_SESSION['login_sistema_gestao'] = true;
			$_SESSION['id_contribuinte'] = $buscaLogin['id'];

			echo "<script>window.location='$CAMINHO/index.php?sessao=$sequencia".$tela."$complemento".($_GET['id'] ? "&id=" . secure($_GET['id']) : "")."'</script>";

		} else {

			echo'<p class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					Aten&ccedil;&atilde;o! Login e/ou senha n&atilde;o encontrados.
				</p>';

		}
	}

} else if($_POST['acao'] == "rec_senha") {

	$cap = new GoogleRecaptcha();
	$verified = $cap->VerifyCaptcha($_POST['g-recaptcha-response']);

	if(!$verified) {

		echo'<p class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				O Captcha n&atilde;o foi resolvido! Verifique.
			</p>';

	} else {

		if(!empty($_POST["recSenhaCNPJ"])) {

			$stSenha = $conn->prepare("SELECT * FROM geral_contribuinte WHERE id_cliente = :cliente AND (cnpj = :cnpj OR cnpj = :cnpj2) AND status_registro = :status_registro");
			$stSenha->execute(array("cliente" => $cliente, "cnpj" => secure($_POST['recSenhaCNPJ']), "cnpj2" => preg_replace("#[^0-9]#", "", secure($_POST['recSenhaCNPJ'])), "status_registro" => "A"));

		} else if(!empty($_POST["recSenhaCPF"])) {

			$stSenha = $conn->prepare("SELECT * FROM geral_contribuinte WHERE id_cliente = :cliente AND (cpf = :cpf OR cpf = :cpf2) AND status_registro = :status_registro");
			$stSenha->execute(array("cliente" => $cliente, "cpf" => secure($_POST['recSenhaCPF']), "cpf2" => preg_replace("#[^0-9]#", "", secure($_POST['recSenhaCPF'])), "status_registro" => "A"));

		}

		$senha = $stSenha->fetch();

		if($senha["id"] != "") {

			if(!empty($senha['email']) || filter_var($senha['email'], FILTER_VALIDATE_EMAIL) !== false) {

				$subject = html_entity_decode("Recupera&ccedil;&atilde;o da senha da Ouvidoria da $dadosCliente[razao_social]");

				$html .= "<p>Ol&aacute; <b>$senha[nome]</b>, segue abaixo sua senha de acesso a Ouvidoria da $dadosCliente[razao_social]:</p>";
				$html .= "<p><strong>Sua senha &eacute;:&nbsp;&nbsp;$senha[senha]</strong></p>";
				$html .= "<p>&nbsp;</p>";
				$html .= "<p>Caso haja alguma informa&ccedil;&atilde;o errada, entre em contato com $dadosCliente[razao_social].</p>";
				$html .= "<p>&nbsp;</p><p>Atenciosamente,</p><p>$dadosCliente[razao_social]</p>";

				$headers = "$dadosCliente[razao_social] <".(empty($dadosCliente['email']) ? "contato@ingadigital.com.br" : $dadosCliente['email']).">";
				$to[0]['email'] = $senha['email'];
				$to[0]['nome'] = "Recuperação de Senha";
				$vai = envia_email_aws($to, $subject, $html, $headers, [], './ingadigital.com.br/privado/transparencia/sistema_gestao/login.php:273:');

				if($vai) {

					echo'<p class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							Login encontrado, estamos enviando um e-mail para o endere&ccedil;o <strong>'.$senha["email"].'</strong>!
						</p>';

				} else {

					echo'<p class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						Aten&ccedil;&atilde;o! Algum problema ocorreu com o envio do e-mail. Entre em contato com a '.$dadosCliente["razao_social"].'
					</p>';

				}

			} else {

				echo'<p class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					Aten&ccedil;&atilde;o! N&atilde;o encontramos um e-mail v&aacute;lido em seu cadastro. Entre em contato com a '.$dadosCliente["razao_social"].'
				</p>';

			}

		} else {

			echo'<p class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					Aten&ccedil;&atilde;o! Cadastro n&atilde;o encontrado.
				</p>';

		}

	}

} else if($_REQUEST['acao'] == "logout") {

	session_destroy();
}
?>

<p>&nbsp;</p>
<div class="well">
<?php
$stTexto = $conn->prepare("SELECT texto_apresentacao_ouvidoria FROM ouvidoria_configuracao WHERE id_cliente = :id_cliente AND status_registro = :status_registro");
$stTexto->execute(array("id_cliente" => $cliente, "status_registro" => "A"));
$textoApresentacao = $stTexto->fetch();

if($textoApresentacao['texto_apresentacao_ouvidoria'] != "") {

	echo verifica($textoApresentacao['texto_apresentacao_ouvidoria']);

} else {
?>
	Ouvidoria - Servi&ccedil;o aberto ao cidad&atilde;o para recebimento de den&uacute;ncias, reclama&ccedil;&otilde;es e sugest&otilde;es, no que diz respeito ao atendimento dos servi&ccedil;os disponibilizados pela Prefeitura Municipal.</br>
	Para encaminhar a sua manifesta&ccedil;&atilde;o &agrave; Ouvidoria fa&ccedil;a seu cadastro ou digite seu login e senha cadastrados anteriormente.</br>
	O login e senha ser&atilde;o utilizados para consultas e manifesta&ccedil;&otilde;es futuras.
<?php } ?>
</div>

<div class="row">
	<div class="col-md-5">
		<div class="panel panel-primary">

			<div class="panel-heading">
		    	<h2 class="panel-title"><i class="glyphicon glyphicon-user"></i> Efetue seu login ou cadastre-se para continuar</h2>
		  	</div>
			<div class="panel-body">
				<form class="form-horizontal validar_formulario" name="login" action="" method="post">
					<div class="form-group">
						<label class="col-sm-4 control-label">Pessoa</label>
						<div class="col-sm-8">
							<label class="radio-inline">
								<input type="radio" name="tipoLogin" id="tipoLoginCpf" value="cpf" checked> F&iacute;sica
							</label>
							<label class="radio-inline">
								<input type="radio" name="tipoLogin" id="tipoLoginCnpj" value="cnpj"> Jur&iacute;dica
							</label>
						</div>
					</div>
					<div class="form-group">
						<label for="loginCPF" id="label_login" class="col-sm-4 control-label">CPF</label>
						<div class="col-sm-8">
							<input type="text" name="loginCPF" id="loginCPF" class="form-control cpf required" required placeholder="Informe seu cpf...">
							<input type="text" name="loginCNPJ" id="loginCNPJ" class="form-control hidden" placeholder="Informe seu cnpj...">
						</div>
					</div>
					<div class="form-group">
						<label for="login_senha" class="col-sm-4 control-label">Senha</label>
						<div class="col-sm-8">
							<input type="password" name="login_senha" id="login_senha" class="form-control required" required placeholder="Informe sua senha...">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-4 col-sm-8 captcha_panel" id="recaptcha1"></div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-4 col-sm-8">
							<input type="hidden" name="acao" value="logar">
							<button type="reset" class="btn btn-danger">Limpar</button>
							<button type="submit" class="btn btn-primary">Enviar</button>
						</div>
					</div>
				</form>
			</div>
			<div class="panel-footer text-right">
				<a href="#" role="button" class="text-danger" data-toggle="modal" data-target="#esqueci_senha"><i class="glyphicon glyphicon-lock"></i> Esqueci minha senha</a>
			</div>
		</div>
	</div>
	<div class="col-md-7"></div>
</div>

<div class="row">
	<div class="col-md-5 text-center">
		<button type="button" class="btn btn-primary btn-lg" data-toggle="collapse" data-target="#form-ouvidoria" aria-expanded="false" aria-controls="form-ouvidoria">
			<i class="glyphicon glyphicon-plus-sign"></i> Efetuar Cadastro
		</button>
	</div>
	<div class="col-md-7"></div>
</div>

<form class="form-horizontal collapse validar_formulario_com_senha" id="form-ouvidoria" action="" method="post">
	<div class="form-group">
		<label class="col-sm-2 control-label">Pessoa</label>
		<div class="col-sm-10">
			<label class="radio-inline">
				<input type="radio" name="tipo" id="tipoF" value="F" checked> F&iacute;sica
			</label>
			<label class="radio-inline">
				<input type="radio" name="tipo" id="tipoJ" value="J"> Jur&iacute;dica
			</label>
		</div>
	</div>
	<div class="form-group">
		<label for="nome" class="col-sm-2 control-label label_nome">Nome</label>
		<div class="col-sm-10">
			<input type="text" name="nome" id="nome" class="form-control required" required value="<?= $nome ?>" placeholder="Informe seu nome...">
		</div>
	</div>
	<div class="form-group for_fis">
		<label for="cpf" class="col-sm-2 control-label">CPF</label>
		<div class="col-sm-10">
			<input type="text" name="cpf" id="cpf" class="form-control cpf required" required value="<?= $cpf ?>" placeholder="Informe seu CPF...">
		</div>
	</div>
	<div class="form-group for_fis">
		<label for="rg" class="col-sm-2 control-label">RG</label>
		<div class="col-sm-10">
			<input type="text" name="rg" id="rg" class="form-control required" required value="<?= $rg ?>" placeholder="Informe seu RG...">
		</div>
	</div>
	<div class="form-group for_jur hidden">
		<label for="cnpj" class="col-sm-2 control-label">CNPJ</label>
		<div class="col-sm-10">
			<input type="text" name="cnpj" id="cnpj" class="form-control cnpj" value="<?= $cnpj ?>" placeholder="Informe seu CNPJ...">
		</div>
	</div>
	<!--
	<div class="form-group for_fis">
		<label for="id_escolaridade" class="col-sm-2 control-label">Escolaridade</label>
		<div class="col-sm-10">
			<select name="id_escolaridade" id="id_escolaridade" class="form-control required" required>
				<option value="">Selecione o n&iacute;vel de escolaridade</option>
				<?php
				$stEscolaridade = $conn->prepare("SELECT * FROM geral_escolaridade WHERE status_registro = :status_registro ORDER BY id");
				$stEscolaridade->execute(array("status_registro" => "A"));
				$qryEscolaridade = $stEscolaridade->fetchAll();

				if(count($qryEscolaridade)) {

					foreach ($qryEscolaridade as $escolaridade) {

						echo "<option value='$escolaridade[id]' ";
						if($escolaridade['id'] == $id_escolaridade) echo "selected";
						echo ">$escolaridade[descricao]</option>";
					}
				}
				?>
			</select>
		</div>
	</div>
	<div class="form-group for_fis">
		<label for="id_renda_familiar" class="col-sm-2 control-label">Renda Familiar</label>
		<div class="col-sm-10">
			<select name="id_renda_familiar" id="id_renda_familiar" class="form-control required" required>
				<option value="">Selecione o n&iacute;vel de renda</option>
				<?php
				$stRenda = $conn->prepare("SELECT * FROM geral_renda_familiar WHERE status_registro = :status_registro ORDER BY id");
				$stRenda->execute(array("status_registro" => "A"));
				$qryRenda = $stRenda->fetchAll();

				if(count($qryRenda)) {

					foreach ($qryRenda as $renda) {

						echo "<option value='$renda[id]' ";
						if($renda['id'] == $id_renda_familiar) echo "selected";
						echo ">$renda[descricao]</option>";
					}
				}
				?>
			</select>
		</div>
	</div>
	-->
	<div class="form-group">
		<label for="endereco" class="col-sm-2 control-label">Endere&ccedil;o</label>
		<div class="col-sm-10">
			<input type="text" name="endereco" id="endereco" class="form-control required" required value="<?= $endereco ?>" placeholder="Informe seu endere&ccedil;o...">
		</div>
	</div>
	<div class="form-group">
		<label for="complemento_endereco" class="col-sm-2 control-label">Complemento</label>
		<div class="col-sm-10">
			<input type="text" name="complemento_endereco" id="complemento_endereco" class="form-control" value="<?= $complemento_endereco ?>" placeholder="Informe o complemento do seu endere&ccedil;o...">
		</div>
	</div>
	<div class="form-group">
		<label for="bairro" class="col-sm-2 control-label">Bairro</label>
		<div class="col-sm-10">
			<input type="text" name="bairro" id="bairro" class="form-control required" required value="<?= $bairro ?>" placeholder="Informe seu bairro...">
		</div>
	</div>
	<div class="form-group">
		<label for="cep" class="col-sm-2 control-label">CEP</label>
		<div class="col-sm-10">
			<input type="text" name="cep" id="cep" class="form-control cep required" required value="<?= $cep ?>" placeholder="Informe seu CEP...">
		</div>
	</div>
	<div class="form-group">
		<label for="id_estado" class="col-sm-2 control-label">Estado</label>
		<div class="col-sm-10">
			<select name="id_estado" id="id_estado" class="form-control required" required>
				<option value="">Selecione o Estado</option>
				<?php
				$stEstado = $conn->prepare("SELECT * FROM estado WHERE status_registro = :status_registro ORDER BY nome");
				$stEstado->execute(array("status_registro" => "A"));
				$qryEstado = $stEstado->fetchAll();

				if(count($qryEstado)) {

					foreach ($qryEstado as $buscaEstado) {

						echo "<option value='$buscaEstado[id]' ";
						if($buscaEstado['id'] == $id_estado) echo "selected";
						echo ">$buscaEstado[nome]</option>";
					}
				}
				?>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label for="id_municipio" class="col-sm-2 control-label">Munic&iacute;pio</label>
		<div class="col-sm-10">
			<p class="carregando form-control-static hidden">Aguarde, carregando...</p>
			<select name="id_municipio" id="id_municipio" class="form-control required" required>
				<option value="">Selecione o Munic&iacute;pio</option>
				<?php
				if($id_estado != "") {

					$stMunicipio = $conn->prepare("SELECT * FROM municipio WHERE id_estado = :id_estado AND status_registro = :status_registro ORDER BY nome");
					$stMunicipio->execute(array("id_estado" => $id_estado, "status_registro" => "A"));
					$qryMunicipio = $stMunicipio->fetchAll();

					if(count($qryMunicipio)) {

						foreach ($qryMunicipio as $municipio) {

							echo "<option value='$municipio[id]' ";
							if($municipio['id'] == $id_municipio) echo "selected";
							echo ">$municipio[nome]</option>";
						}

					}

				}
				?>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label for="telefone_fixo" class="col-sm-2 control-label">Telefone Fixo</label>
		<div class="col-sm-10">
			<input type="tel" name="telefone_fixo" id="telefone_fixo" class="form-control telefone" value="<?= $telefone_fixo ?>" placeholder="Informe seu telefone fixo...">
		</div>
	</div>
	<div class="form-group">
		<label for="telefone_celular" class="col-sm-2 control-label">Telefone Celular</label>
		<div class="col-sm-10">
			<input type="tel" name="telefone_celular" id="telefone_celular" class="form-control telefone" value="<?= $telefone_celular ?>" placeholder="Informe seu telefone celular...">
		</div>
	</div>
	<div class="form-group">
		<label for="email" class="col-sm-2 control-label">E-mail</label>
		<div class="col-sm-10">
			<input type="email" name="email" id="email" class="form-control email required" required value="<?= $email ?>" placeholder="Informe seu e-mail...">
		</div>
	</div>
	<div class="form-group">
		<label for="senha" class="col-sm-2 control-label">Senha</label>
		<div class="col-sm-10">
			<input type="password" name="senha" id="senha" class="form-control required" required placeholder="Informe sua senha...">
		</div>
	</div>
	<div class="form-group">
		<label for="conf_senha" class="col-sm-2 control-label">Confirme a Senha</label>
		<div class="col-sm-10">
			<input type="password" name="conf_senha" id="conf_senha" class="form-control required" required placeholder="Confirme sua senha...">
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10 captcha" id="recaptcha2"></div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<input type="hidden" name="acao" value="cadastrar">
			<button type="reset" class="btn btn-danger">Limpar</button>
			<button type="submit" class="btn btn-primary">Enviar</button>
		</div>
	</div>
</form>

<div class="modal fade" id="esqueci_senha">
	<div class="modal-dialog">
		<form class="form-horizontal" name="esqueci_senha" action="" method="post">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Esqueci minha senha</h4>
	      		</div>
		      	<div class="modal-body">
					<p>Informe o CPF ou CNPJ abaixo que enviaremos sua senha para o e-mail informado no cadastro:</p>
					<div class="form-group">
						<label class="col-sm-4 control-label">Pessoa</label>
						<div class="col-sm-8">
							<label class="radio-inline">
								<input type="radio" name="tipoLogin" id="recSenhaCpf" value="cpf" checked> F&iacute;sica
							</label>
							<label class="radio-inline">
								<input type="radio" name="tipoLogin" id="recSenhaCnpj" value="cnpj"> Jur&iacute;dica
							</label>
						</div>
					</div>
					<div class="form-group">
						<label for="recSenhaCPF" id="label_rec_senha" class="col-sm-4 control-label">CPF</label>
						<div class="col-sm-8">
							<input type="text" name="recSenhaCPF" id="recSenhaCPF" class="form-control cpf required" required placeholder="Informe seu cpf...">
							<input type="text" name="recSenhaCNPJ" id="recSenhaCNPJ" class="form-control hidden" placeholder="Informe seu cnpj...">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-4 col-sm-8 captcha" id="recaptcha3"></div>
					</div>
				</div>
      			<div class="modal-footer">
      				<input type="hidden" name="acao" value="rec_senha">
        			<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        			<button type="submit" class="btn btn-primary">Enviar</button>
      			</div>
	    	</div>
    	</form>
  	</div>
</div>