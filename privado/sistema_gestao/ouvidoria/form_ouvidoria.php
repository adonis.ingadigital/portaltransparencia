<?php

if($_SESSION['login_sistema_gestao'] == false) {



	echo "<script>window.location='$CAMINHO/index.php?sessao=$sequencia".$sistemaGestaoLogin."$complemento&tela=$sistemaGestaoOuvidoria'</script>";



} else {



?>

<h2>Ouvidoria</h2>

<ol class="breadcrumb">

	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>

	<li class="active">Ouvidoria</li>

</ol>



<?php

if($_GET['cadastro'] == "1") {



	echo'<p class="alert alert-success">

			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

			Cadastro efetuado com sucesso!

		</p>';

}



include "../../privado/transparencia/recaptchalib.php";



$stConfiguracao = $conn->prepare("SELECT * FROM ouvidoria_configuracao WHERE id_cliente = :id_cliente AND status_registro = :status_registro ORDER BY id LIMIT 1");

$stConfiguracao->execute(array("id_cliente" => $cliente, "status_registro" => "A"));

$configuracao = $stConfiguracao->fetch();



$stContribuinte = $conn->prepare("SELECT * FROM geral_contribuinte WHERE id = :id");

$stContribuinte->execute(array("id" => $_SESSION['id_contribuinte']));

$contribuinte = $stContribuinte->fetch();



if($_POST['acao'] == "cadastrar") {



	foreach ($_POST as $campo => $valor) {

		$$campo = secure($valor);

	}



	$cap = new GoogleRecaptcha();

	$verified = $cap->VerifyCaptcha($_POST['g-recaptcha-response']);



	if(!$verified) {



		echo'<p class="alert alert-danger">

				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				O Captcha n&atilde;o foi resolvido! Verifique.

			</p>';



	} else if(empty($id_instituicao)) {



		echo'<p class="alert alert-danger">

				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				Informe a institui&ccedil;&atilde;o!

			</p>';



	} else if(empty($descricao)) {



		echo'<p class="alert alert-danger">

				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				Informe o conte&uacute;do de sua solicita&ccedil;&atilde;o!

			</p>';



	} else {



		$stCadastro = $conn->prepare("INSERT INTO ouvidoria_acionamento (id_instituicao,

																		 id_contribuinte,

																		 id_tipo_mensagem,

																		 id_assunto,

																		 id_meio_recebimento,

																		 id_estado_acionamento,

																		 data_acionamento,

																		 descricao)

															VALUES (?,

																	?,

																	?,

																	?,

																	5,

																	1,

																	NOW(),

																	?)");



		$id_tipo_mensagem = empty($id_tipo_mensagem) ? NULL : $id_tipo_mensagem;

		$id_assunto = empty($id_assunto) ? NULL : $id_assunto;



		$stCadastro->bindParam(1, $id_instituicao, PDO::PARAM_INT);

		$stCadastro->bindParam(2, $contribuinte["id"], PDO::PARAM_INT);

		$stCadastro->bindParam(3, $id_tipo_mensagem, PDO::PARAM_INT);

		$stCadastro->bindParam(4, $id_assunto, PDO::PARAM_INT);

		$stCadastro->bindParam(5, $descricao, PDO::PARAM_STR);



		$cad = $stCadastro->execute();



		if($cad) {



			$id = $conn->lastInsertId();

			$numero_protocolo = str_pad($id, 4, "0", STR_PAD_LEFT).date("dmy").date("Hi");



			$stProtocolo = $conn->prepare("UPDATE ouvidoria_acionamento SET protocolo = :protocolo WHERE id = :id");

			$stProtocolo->bindParam("protocolo", $numero_protocolo, PDO::PARAM_INT);

			$stProtocolo->bindParam("id", $id, PDO::PARAM_INT);

			$stProtocolo->execute();



			//Anexos

			$extensoes = array("gif","png","jpg","jpeg", "pdf", "txt", "doc", "docx", "xls", "xlsx", "csv", "ppt", "pptx", "pps", "ppsx");

			$destino = "../../controlemunicipal/www/inga/sistema/arquivos/$cliente";

			$msgFiles = "";



			for ($i = 0; $i < count($_FILES['anexo']['name']); $i++) {



				if($_FILES['anexo']['size'][$i] > 0) {



					$ext = strtolower(array_pop(explode(".", $_FILES['anexo']['name'][$i])));

					$nome = date("dmyHis") . "_" . nomear_pasta($_FILES['anexo']['name'][$i]) . "." . $ext;



					if(array_search($ext, $extensoes) !== false) {



						if(move_uploaded_file($_FILES['anexo']['tmp_name'][$i], $destino . "/" . $nome)) {



							$stAnexo = $conn->prepare("INSERT INTO ouvidoria_acionamento_anexo (id_acionamento,

																								descricao,

																								arquivo)

																VALUES (?,

																		?,

																		?)");



							$stAnexo->bindParam(1, $id, PDO::PARAM_INT);

							$stAnexo->bindParam(2, $_FILES['anexo']['name'][$i], PDO::PARAM_STR);

							$stAnexo->bindParam(3, $nome, PDO::PARAM_STR);



							if(!$stAnexo->execute()) {



								$msgFiles .= $_FILES['anexo']['name'][$i] . " erro ao salvar o arquivo<br>";

							}



						} else {



							$msgFiles .= $_FILES['anexo']['name'][$i] . " erro ao mover o arquivo para a pasta<br>";

						}



					} else {



						$msgFiles .= $_FILES['anexo']['name'][$i] . " n&atilde;o possui um form&aacute;to v&aacute;lido<br>";

					}



				}

			}



			if($msgFiles != "") {



				echo'<p class="alert alert-danger">

						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

						Um ou mais arquivos enviados apresentaram algum problema:<br>'.$msgFiles.'

					</p>';

			}



			$stInstituicao = $conn->prepare("SELECT * FROM ouvidoria_instituicao WHERE id_cliente = :id_cliente AND id = :id_instituicao");

			$stInstituicao->execute(array("id_cliente" => $cliente, "id_instituicao" => $id_instituicao));

			$emailBuscaInstituicao = $stInstituicao->fetch();



			$stTipoMensagem = $conn->prepare("SELECT * FROM ouvidoria_tipo_mensagem WHERE id_cliente = :id_cliente AND id = :id_tipo_mensagem");

			$stTipoMensagem->execute(array("id_cliente" => $cliente, "id_tipo_mensagem" => $id_tipo_mensagem));

			$emailBuscaTipoMensagem = $stTipoMensagem->fetch();



			$stAssunto = $conn->prepare("SELECT * FROM ouvidoria_assunto WHERE id = :id_assunto");

			$stAssunto->execute(array("id_assunto" => $id_assunto));

			$emailBuscaAssunto = $stAssunto->fetch();



			if(!empty($contribuinte['email']) || filter_var($contribuinte['email'], FILTER_VALIDATE_EMAIL) !== false) {



				$subject = html_entity_decode("$dadosCliente[razao_social] - Nova Solicita&ccedil;&atilde;o na Ouvidoria");



				$data	 = date("d/m/Y");

				$hora    = date("H:i");



				$html    = "<p>OUVIDORIA DA $dadosCliente[razao_social]</p>";

				if($configuracao['texto_resposta_momento_acionamento'] != "")

					$html .= "<p>" . $configuracao['texto_resposta_momento_acionamento'] . "</p><p>&nbsp;</p>";

				$html   .= "<p><strong>Contribuinte:&nbsp;</strong>$contribuinte[nome]</p>";

				$html   .= "<p><strong>Data e hora da solicita&ccedil;&atilde;o:&nbsp;</strong>$data&nbsp;-&nbsp;$hora</p>";

				$html   .= "<p><strong>N&uacute;mero do Protocolo:&nbsp;</strong>$numero_protocolo</p>";

				$html   .= "<p><strong>Institui&ccedil;&atilde;o:&nbsp;</strong>$emailBuscaInstituicao[descricao]</p>";

				$html   .= "<p><strong>Tipo de Mensagem:&nbsp;</strong>$emailBuscaTipoMensagem[descricao]</p>";

				$html   .= "<p><strong>Assunto:&nbsp;</strong>$emailBuscaAssunto[descricao]</p>";

				$html   .= "<p><strong>Descri&ccedil;&atilde;o:&nbsp;</strong>".nl2br($descricao)."</p>";

				$html   .= "<p>&nbsp;</p>";



				$headers = "$dadosCliente[razao_social] <".(empty($dadosCliente['email']) ? "contato@ingadigital.com.br" : $dadosCliente['email']).">";
				$to[0]['email'] = $contribuinte['email'];
				$to[1]['email'] = $emailBuscaInstituicao['email_responsavel'];
				$to[0]['nome'] = "Ouvidoria";
				$to[1]['nome'] = "Ouvidoria";
				
				$vai = envia_email_aws($to, $subject, $html, $headers, [], './ingadigital.com.br/privado/transparencia/sistema_gestao/ouvidoria/form_ouvidoria.php:381:');



				if($vai) {



					echo'<p class="alert alert-success">

							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

							Solicita&ccedil;&atilde;o efetuada com sucesso! Estamos enviando um e-mail com os dados de sua solicita&ccedil;&atilde;o para o e-mail informado no seu cadastro da Ouvidoria!

						</p>';



				} else {



					echo'<p class="alert alert-warning">

							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

							Solicita&ccedil;&atilde;o efetuada com sucesso, por&eacute;m houve um erro no envio do e-mail de confirma&ccedil;&atilde;o

						</p>';



				}



				$msg = '<div class="panel panel-success">

							<div class="panel-heading">

						    	<h2 class="panel-title"><i class="glyphicon glyphicon-user"></i> SOLICITA&Ccedil;&Atilde;O ENVIADA COM SUCESSO</h2>

						  	</div>

							<div class="panel-body">

								<p>Anote o n&uacute;mero do seu protocolo para consultar o andamento da solicita&ccedil;&atilde;o posteriormente: <span class="label label-default">'.$numero_protocolo.'</span></p>

								<p><a href="'.$CAMINHOCMGERAL.'/impressao/ouvidoria.php?id='.$id.'" target="_blank"><i class="glyphicon glyphicon-print"></i> Clique aqui para imprimir sua solicita&ccedil;&atilde;o.</a></p>

							</div>

						</div>

						<div class="clearfix"></div>';



				echo $msg;



			} else {



				echo'<p class="alert alert-danger">

						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

						'.$contribuinte['nome'].', n&atilde;o encontramos um e-mail v&aacute;lido em seu cadastro, entre em contato com '.$nome_cliente['razao_social'].'!

					</p>';



			}



		} else {



			echo'<p class="alert alert-danger">

					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

					N&atilde;o foi poss&iacute;vel realizar sua solicita&ccedil;&atilde;o! Tente novamente mais tarde.

				</p>';

		}



	}



} else if($_POST['numero_protocolo']) {



	$stProtocolo = $conn->prepare("SELECT * FROM ouvidoria_acionamento WHERE protocolo = :protocolo AND id_contribuinte = :id_contribuinte AND status_registro = :status_registro");

	$stProtocolo->execute(array("protocolo" => secure($_POST['numero_protocolo']), "id_contribuinte" => $contribuinte["id"], "status_registro" => "A"));

	$protocolo = $stProtocolo->fetch();



	if($protocolo["id"] != "") {



		$msg = '<div class="panel panel-success">

					<div class="panel-heading">

				    	<h2 class="panel-title"><i class="glyphicon glyphicon-user"></i> SOLICITA&Ccedil;&Atilde;O ENCONTRADA</h2>

				  	</div>

					<div class="panel-body">

						<p>Anote o n&uacute;mero do seu protocolo para consultar o andamento da solicita&ccedil;&atilde;o posteriormente: <span class="label label-default">'.$protocolo['protocolo'].'</span></p>

						<p><a href="'.$CAMINHO.'/index.php?sessao='.$sequencia.$sistemaGestaoOuvidoriaVisualiza.$complemento.'&id='.$protocolo['id'].'"><i class="glyphicon glyphicon-search"></i> Visualizar andamento da solicita&ccedil;&atilde;o.</a></p>

						<p><a href="'.$CAMINHOCMGERAL.'/impressao/ouvidoria.php?id='.$protocolo['id'].'" target="_blank"><i class="glyphicon glyphicon-print"></i> Clique aqui para imprimir sua solicita&ccedil;&atilde;o.</a></p>

					</div>

				</div>

				<div class="clearfix"></div>';



		echo $msg;



	} else {



		echo'<p class="alert alert-danger">

				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

				N&atilde;o foi encontrado nenhum acionamento com o protocolo <strong>'.$_POST['numero_protocolo'].'</strong>.

			</p>';



	}



}



$stHistorico = $conn->prepare("SELECT ouvidoria_acionamento.*,

									  ouvidoria_instituicao.id_cliente,

									  ouvidoria_instituicao.descricao instituicao,

									  ouvidoria_tipo_mensagem.descricao tipo_mensagem,

									  ouvidoria_estado_acionamento.descricao estado_acionamento,

									  ouvidoria_assunto.descricao assunto

							     FROM ouvidoria_acionamento

						    LEFT JOIN ouvidoria_instituicao ON ouvidoria_acionamento.id_instituicao = ouvidoria_instituicao.id

						    LEFT JOIN ouvidoria_tipo_mensagem ON ouvidoria_acionamento.id_tipo_mensagem = ouvidoria_tipo_mensagem.id

						    LEFT JOIN ouvidoria_assunto ON ouvidoria_acionamento.id_assunto = ouvidoria_assunto.id

						    LEFT JOIN ouvidoria_estado_acionamento ON ouvidoria_acionamento.id_estado_acionamento = ouvidoria_estado_acionamento.id

						        WHERE ouvidoria_acionamento.id_contribuinte = :id

						          AND ouvidoria_acionamento.status_registro = :status_registro

							 ORDER BY ouvidoria_acionamento.id DESC");



$stHistorico->execute(array("id" => $contribuinte["id"], "status_registro" => "A"));

$qryHistorico = $stHistorico->fetchAll();

?>

<strong>Ol&aacute; <?= $contribuinte['nome'] ?>, seja bem vindo(a)! Selecione o que deseja fazer:</strong>

<a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$sistemaGestaoLogin.$complemento) ?>&acao=logout&tela=<?= $sistemaGestaoOuvidoria ?>" class="pull-right"><strong class="text-danger"><i class="glyphicon glyphicon-off"></i> Sair.</strong></a>

<p class="clearfix"></p>



<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#solicitacao">

	<i class="glyphicon glyphicon-plus-sign"></i> Realizar nova solicita&ccedil;&atilde;o

</button>

<?php if($configuracao['texto_apresentacao_acionamento'] != "") { ?>

<p class="clearfix"></p>

<div class="well pull-right"><?= $configuracao['texto_apresentacao_acionamento'] ?></div>

<?php } ?>



<div class="modal fade" id="solicitacao">

	<div class="modal-dialog">

		<form class="form-horizontal validar_formulario" name="solicitacao" enctype="multipart/form-data" action="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$sistemaGestaoOuvidoria.$complemento); ?>" method="post">

			<div class="modal-content">

				<div class="modal-header">

					<button type="button" class="close" data-dismiss="modal" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>

					<h4 class="modal-title">Realizar nova solicita&ccedil;&atilde;o</h4>

	      		</div>

		      	<div class="modal-body">

					<div class="form-group">

						<label for="id_instituicao" class="col-sm-4 control-label">Institui&ccedil;&atilde;o</label>

						<div class="col-sm-8">

							<select name="id_instituicao" id="id_instituicao" class="form-control required" required>

								<option value="">Selecione a Institui&ccedil;&atilde;o</option>

								<?php

								$stInstituicao = $conn->prepare("SELECT * FROM ouvidoria_instituicao WHERE id_cliente = :id_cliente AND status_registro = :status_registro ORDER BY descricao");

								$stInstituicao->execute(array("id_cliente" => $cliente, "status_registro" => "A"));

								$qryInstituicao = $stInstituicao->fetchAll();



								if(count($qryInstituicao)) {



									foreach ($qryInstituicao as $buscaInstituicao) {



										echo "<option value='$buscaInstituicao[id]'>$buscaInstituicao[descricao]</option>";

									}

								}

								?>

							</select>

						</div>

					</div>

					<div class="form-group">

						<label for="id_tipo_mensagem" class="col-sm-4 control-label">Tipo de Mensagem</label>

						<div class="col-sm-8">

							<p class="carregando_tipo form-control-static hidden">Aguarde, carregando...</p>

							<select name="id_tipo_mensagem" id="id_tipo_mensagem" class="form-control">

								<option value="">Selecione o Tipo de Mensagem</option>

							</select>

						</div>

					</div>

					<div class="form-group">

						<label for="id_assunto" class="col-sm-4 control-label">Assunto</label>

						<div class="col-sm-8">

							<p class="carregando_assunto form-control-static hidden">Aguarde, carregando...</p>

							<select name="id_assunto" id="id_assunto" class="form-control">

								<option value="">Selecione o Assunto</option>

							</select>

						</div>

					</div>

					<div class="form-group">

						<label for="descricao" class="col-sm-4 control-label">Descri&ccedil;&atilde;o</label>

						<div class="col-sm-8">

							<textarea name="descricao" rows="3" class="form-control required" required placeholder="Informe o conte&uacute;do de sua solicita&ccedil;&atilde;o..."></textarea>

						</div>

					</div>

					<?php if($configuracao['permite_anexo_acionamento'] == "S") { ?>

					<div class="form-group">

						<label for="anexo" class="col-sm-4 control-label">Anexo(s)</label>

						<div class="col-sm-8">

							<div class="input-group">

				                <span class="input-group-btn">

				                    <span class="btn btn-primary btn-file">

				                        <i class="glyphicon glyphicon-folder-open"></i> Selecionar&hellip;

				                        <input type="file" name="anexo[]" id="anexo" multiple>

				                    </span>

				                </span>

				                <input type="text" class="form-control" readonly>

				            </div>

				            <span class="help-block">

				                Selecione v&aacute;rios arquivos pressionando a tecla CTRL

				            </span>

						</div>

					</div>

					<?php } ?>

					<div class="form-group">

						<div class="col-sm-offset-4 col-sm-8 captcha" id="recaptcha1"></div>

					</div>

				</div>

      			<div class="modal-footer">

      				<input type="hidden" name="acao" value="cadastrar">

        			<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>

        			<button type="submit" class="btn btn-primary">Enviar</button>

      			</div>

	    	</div>

    	</form>

  	</div>

</div>



<h3>Consultar Protocolo</h3>

<form class="form-horizontal" action="" method="post">

	<div class="input-group">

	    <input type="search" name="numero_protocolo" class="form-control" placeholder="Informe o n&uacute;mero do protocolo...">

		<span class="input-group-btn">

			<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>

		</span>

	</div>

</form>



<h3>Hist&oacute;rico de protocolos solicitados</h3>

<div class="table-responsive">

	<table class="table table-striped table-hover table-bordered table-condensed">

		<tr>

			<th>&nbsp;</th>

			<th>Protocolo</th>

			<th>Data Solicita&ccedil;&atilde;o</th>

			<th>Institui&ccedil;&atilde;o</th>

			<th>Tipo de Mensagem</th>

			<th>Assunto</th>

			<th>Estado do Acionamento</th>

		</tr>

		<?php

		if(count($qryHistorico)) {

		foreach ($qryHistorico as $historico) {

		?>

		<tr>

			<td>

				<a href="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$sistemaGestaoOuvidoriaVisualiza.$complemento); ?>&id=<?= $historico['id'] ?>" class="btn btn-primary" data-toggle="tooltip" data-placement="right" title="Visualizar">

					<i class="glyphicon glyphicon-search"></i>

				</a>

			</td>

			<td class="text-right"><?= $historico['protocolo'] ?></td>

			<td class="text-right"><?= formata_data_hora($historico['data_acionamento']) ?></td>

			<td><?= $historico['instituicao'] ?></td>

			<td><?= $historico['tipo_mensagem'] ?></td>

			<td><?= $historico['assunto'] ?></td>

			<td><?= $historico['estado_acionamento'] ?></td>

		</tr>

		<?php } ?>

		<?php } ?>

	</table>

</div>

<?php

}