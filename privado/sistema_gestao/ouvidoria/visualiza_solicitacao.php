<?php
if($_SESSION['login_sistema_gestao'] == false) {

	echo "<script>window.location='$CAMINHO/index.php?sessao=$sequencia".$sistemaGestaoLogin."$complemento&tela=$sistemaGestaoOuvidoria'</script>";

} else {

$id = secure($_REQUEST['id']);

$stSolicitacao = $conn->prepare("SELECT ouvidoria_acionamento.*,
									  	geral_contribuinte.nome,
									  	geral_contribuinte.login,
									  	geral_contribuinte.senha,
									 	ouvidoria_instituicao.id_cliente,
									  	ouvidoria_instituicao.descricao instituicao,
									  	ouvidoria_tipo_mensagem.descricao tipo_mensagem,
									  	ouvidoria_assunto.descricao assunto,
									  	ouvidoria_estado_acionamento.descricao estado_acionamento
							       FROM ouvidoria_acionamento
							  LEFT JOIN geral_contribuinte ON ouvidoria_acionamento.id_contribuinte = geral_contribuinte.id
							  LEFT JOIN ouvidoria_instituicao ON ouvidoria_acionamento.id_instituicao = ouvidoria_instituicao.id
							  LEFT JOIN ouvidoria_tipo_mensagem ON ouvidoria_acionamento.id_tipo_mensagem = ouvidoria_tipo_mensagem.id
							  LEFT JOIN ouvidoria_assunto ON ouvidoria_acionamento.id_assunto = ouvidoria_assunto.id
							  LEFT JOIN ouvidoria_estado_acionamento ON ouvidoria_acionamento.id_estado_acionamento = ouvidoria_estado_acionamento.id
							      WHERE ouvidoria_acionamento.id = :id
							      	AND ouvidoria_acionamento.status_registro = :status_registro");

$stSolicitacao->execute(array("id" => $id, "status_registro" => "A"));
$buscaSolicitacao = $stSolicitacao->fetch();

$stAnexo = $conn->prepare("SELECT * FROM ouvidoria_acionamento_anexo WHERE id_acionamento = :id_acionamento AND status_registro = :status_registro ORDER BY id DESC");
$stAnexo->execute(array("id_acionamento" => $id, "status_registro" => "A"));
$qryAnexo = $stAnexo->fetchAll();
?>
<h2>Ouvidoria</h2>
<ol class="breadcrumb">
	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
	<li><a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$sistemaGestaoOuvidoria.$complemento); ?>">Ouvidoria</a></li>
	<li class="active">Solicita&ccedil;&atilde;o</li>
</ol>

<a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$sistemaGestaoLogin.$complemento) ?>&acao=logout&tela=<?= $sistemaGestaoOuvidoria ?>" class="pull-right"><strong class="text-danger"><i class="glyphicon glyphicon-off"></i> Sair.</strong></a>
<h3>PROTOCOLO <span class="label label-default"><?= $buscaSolicitacao['protocolo'] ?></span></h3>

<a href="<?= $CAMINHOCMGERAL ?>/impressao/ouvidoria.php?id=<?= $buscaSolicitacao['id'] ?>" class="btn btn-primary" target="_blank"><i class="glyphicon glyphicon-print"></i> Imprimir Solicita&ccedil;&atilde;o</a>
<p><strong>Data do acionamento:</strong> <?= formata_data_hora($buscaSolicitacao['data_acionamento']) ?></p>
<p><strong>Institui&ccedil;&atilde;o:</strong> <?= $buscaSolicitacao['instituicao'] ?></p>
<p><strong>Tipo de Mensagem:</strong> <?= $buscaSolicitacao['tipo_mensagem'] ?></p>
<p><strong>Assunto:</strong> <?= $buscaSolicitacao['assunto'] ?></p>
<p><strong>Descri&ccedil;&atilde;o:</strong> <?= $buscaSolicitacao['descricao'] ?></p>
<p><strong>Estado do Acionamento:</strong> <?= $buscaSolicitacao['estado_acionamento'] ?></p>
<?php if($buscaSolicitacao['descricao_parecer'] != "") { ?>
<p>&nbsp;</p>
<p><strong>PARECER FINAL:</strong> <?= $buscaSolicitacao['descricao_parecer'] ?></p>
<?php } ?>
<?php
if(count($qryAnexo)) {
?>
<div class="panel panel-primary">
	<div class="panel-heading">
    	<h2 class="panel-title"><i class="glyphicon glyphicon-cloud-download"></i> Anexos</h2>
  	</div>
	<div class="panel-body">
	<?php
	foreach ($qryAnexo as $anexo) {
	?>
		<p><strong><a href="<?= $CAMINHOARQ ?>/<?= $anexo['arquivo'] ?>" target="_blank"><i class="glyphicon glyphicon-cloud-download"></i> <?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?></a></strong></p>
	<?php } ?>
	</div>
</div>
<?php } ?>
<a href="<?=$CAMINHO?>/index.php?sessao=<?= $sequencia.$sistemaGestaoOuvidoria.$complemento?>" class="btn btn-primary"><i class="glyphicon glyphicon-arrow-left"></i> Voltar</a>
<?php
}