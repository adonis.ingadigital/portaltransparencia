<h2>LEI DE ACESSO &Agrave; INFORMA&Ccedil;&Atilde;O</h2>

<ol class="breadcrumb">
	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
	<li class="active">Lei de Acesso &agrave; informa&ccedil;&atilde;o</li>
</ol>

<img src="<?= $CAMINHO ?>/images/acesso_informacao.jpg" alt="ACESSO &Agrave; INFORMA&Ccedil;&Atilde;O" style="float: left; margin: 0px 8px 8px 0px;" />

<?php if($cliente != "11973") { ?>
<p>A lei Federal n&uacute;mero <u><a href="http://www.planalto.gov.br/ccivil_03/_Ato2011-2014/2011/Lei/L12527.htm" target="_blank">12.527/2011</a></u> garante ao cidad&atilde;o o direito constitucional de acesso &agrave;s informa&ccedil;&otilde;es p&uacute;blicas.</p>
<p>&nbsp;</p>
<p>A <?= $dadosCliente['razao_social'] ?> disponibiliza, no <strong>Portal da Transpar&ecirc;ncia</strong>, as seguintes informa&ccedil;&otilde;es:</p>
<ul style="padding: 0 0 0 40px; display:table;">
	<li>Informa&ccedil;&otilde;es sobre fun&ccedil;&otilde;es, compet&ecirc;ncias, estrutura organizacional, quem &eacute; quem e agenda de autoridades da <?= $dadosCliente['razao_social'] ?>.</li>
	<li>Dados sobre programas, a&ccedil;&otilde;es, projetos e atividades da <?= $dadosCliente['razao_social'] ?>.</li>
	<li>Informa&ccedil;&otilde;es referentes ao resultado de inspe&ccedil;&otilde;es, auditorias, presta&ccedil;&otilde;es e tomada de contas realizadas na <?= $dadosCliente['razao_social'] ?>.</li>
	<li>Detalhes sobre repasses e transfer&ecirc;ncias de recursos efetuados pela <?= $dadosCliente['razao_social'] ?>.</li>
	<li>Informa&ccedil;&otilde;es sobre a execu&ccedil;&atilde;o or&ccedil;ament&aacute;ria e financeira da <?= $dadosCliente['razao_social'] ?>.</li>
	<li>Informa&ccedil;&otilde;es sobre licita&ccedil;&otilde;es, contratos, contrata&ccedil;&otilde;es, e atas de registro de pre&ccedil;os firmadas pela <?= $dadosCliente['razao_social'] ?>.</li>
	<li>Informa&ccedil;&otilde;es sobre provimento de cargos e rela&ccedil;&atilde;o dos servidores p&uacute;blicos lotados ou em exerc&iacute;cio na <?= $dadosCliente['razao_social'] ?>.</li>
</ul>
<p>&nbsp;</p>

<?php } ?> 

<?php
$stCategoria = $conn->prepare("SELECT * FROM acesso_informacao_categoria WHERE id_cliente = :id_cliente AND status_registro = :status_registro ORDER BY id DESC");
$stCategoria->execute(array("id_cliente" => $cliente, "status_registro" => "A"));
$qryCategoria = $stCategoria->fetchAll();

if(count($qryCategoria)) {
?>
<ul class="treeview">
	<?php
	foreach ($qryCategoria as $categoria) {
	?>
	<li><a href="#"><?= $categoria['descricao'] ?></a>
		<?php
		$stArquivo = $conn->prepare("SELECT * FROM acesso_informacao WHERE id_categoria = :id_categoria AND (id_subcategoria IS NULL OR id_subcategoria = '') AND status_registro = :status_registro ORDER BY titulo DESC");
		$stArquivo->execute(array("id_categoria" => $categoria['id'], "status_registro" => "A"));
		$qryArquivo = $stArquivo->fetchAll();

		if(count($qryArquivo)) {
		?>
		<ul>
			<?php
			foreach($qryArquivo as $arquivo) {

				if($arquivo['arquivo'] != "") {

					$linkDocumento = $CAMINHOARQ . "/" . $arquivo['arquivo'];
					$imagem = "glyphicon-cloud-download";
					$titulo = empty($arquivo['titulo']) ? "Anexo" : $arquivo['titulo'];

				} else if($arquivo['link'] != "") {

					$linkDocumento = $arquivo['link'];
					$imagem = "glyphicon-globe";
					$titulo = empty($arquivo['titulo']) ? "Link" : $arquivo['titulo'];
				}
			?>
			<li><a href="<?=$linkDocumento ?>" target="_blank"><i class="glyphicon <?= $imagem ?>"></i> <?= $titulo ?></a></li>
			<?php } ?>
		</ul>
		<?php } ?>
		<?php
		$stSubcategoria = $conn->prepare("SELECT * FROM acesso_informacao_subcategoria WHERE id_categoria = :id_categoria AND status_registro = :status_registro ORDER BY id DESC");
		$stSubcategoria->execute(array("id_categoria" => $categoria['id'], "status_registro" => "A"));
		$qrySubcategoria = $stSubcategoria->fetchAll();

		if(count($qrySubcategoria)) {
		?>
		<ul>
			<?php
			foreach ($qrySubcategoria as $subcategoria) {
			?>
			<li><a href="#"><?= $subcategoria['descricao'] ?></a>
				<?php
				$stArquivo = $conn->prepare("SELECT * FROM acesso_informacao WHERE id_subcategoria = :id_subcategoria AND status_registro = :status_registro ORDER BY titulo DESC");
				$stArquivo->execute(array("id_subcategoria" => $subcategoria['id'], "status_registro" => "A"));
				$qryArquivo = $stArquivo->fetchAll();

				if(count($qryArquivo)) {
				?>
				<ul>
					<?php
					foreach($qryArquivo as $arquivo) {

						if($arquivo['arquivo'] != "") {

							$linkDocumento = $CAMINHOARQ . "/" . $arquivo['arquivo'];
							$imagem = "glyphicon-cloud-download";
							$titulo = empty($arquivo['titulo']) ? "Anexo" : $arquivo['titulo'];

						} else if($arquivo['link'] != "") {

							$linkDocumento = $arquivo['link'];
							$imagem = "glyphicon-globe";
							$titulo = empty($arquivo['titulo']) ? "Link" : $arquivo['titulo'];
						}
					?>
					<li><a href="<?= $linkDocumento ?>" target="_blank"><i class="glyphicon <?= $imagem ?>"></i> <?= $titulo ?></a></li>
					<?php } ?>
				</ul>
				<?php } ?>
			</li>
			<?php } ?>
		</ul>
		<?php } ?>
	</li>
	<?php } ?>
</ul>
<p>&nbsp;</p>
<?php
}
?> 

<?php if($cliente == "11973") { ?>

<p>Em cumprimento &agrave; Lei Federal de Acesso &agrave; Informa&ccedil;&atilde;o P&uacute;blica (Lei nº 12.527/2011), a Prefeitura do Munic&iacute;pio de Pitangueiras coloca &agrave; disposi&ccedil;&atilde;o o Servi&ccedil;o de Informa&ccedil;&otilde;es ao Cidad&atilde;o – SIC.</p>

<p>As informa&ccedil;&otilde;es poder&atilde;o ser obtidas junto &agrave; Secretaria de Administra&ccedil;&atilde;o e Finan&ccedil;as, da Prefeitura Municipal, sito &agrave; Avenida Central nº 408, ou atrav&eacute;s do telefone (43) 3257-1143, no hor&aacute;rio das 8h00min &agrave;s 11h30min e das 13h00min &agrave;s 17h00min</p>

<p><b>Em cumprimento a Lei de acesso a informa&ccedil;&atilde;o</b>, este espaço &eacute; destinado para solicita&ccedil;&atilde;o de quaisquer informa&ccedil;&otilde;es a respeito da Administra&ccedil;&atilde;o P&uacute;blica.</p>

<p>Se ainda houver necessidade de mais alguma informa&ccedil;&atilde;o, entre em contato conforme link abaixo para solicitar.</p><br>

<?php } ?>                         

<?php if($cliente == "11924") { ?>
<p>
Caso não tenha localizado o que procura no Portal, você pode fazer um pedido de informação por meio físico (SIC) ou eletrônico (e-SIC), da seguinte forma:<br>
<br><b>SIC presencial</b>
<br><br><b>Prefeitura de Catanduvas – (45) 3234-1313</b>
<br><br>Endereço: Avenida dos Pioneiros, nº 500, Bairro Centro, CEP 85.470-000, Catanduvas – Paraná
<br><br>Horário de Atendimento: Segunda à Sexta-Feira, das 08:30 às 11:30 - 13:30 às 17:00.
<br><br>A Prefeitura Municipal de Catanduvas oferece ainda o e-SIC (Sistema Eletrônica do Serviço de Informações ao Cidadão), pelo qual qualquer cidadão pode formular seu pedido de informação de maneira fácil e rápida.
</p>                                                                            
<?php } ?>

<?php if($cliente != "11973") { ?>                     
<p>Para solicitar informa&ccedil;&otilde;es adicionais do <strong>Portal da Transpar&ecirc;ncia</strong>, assim como outras informa&ccedil;&otilde;es p&uacute;blicas, clique no bot&atilde;o abaixo:</p>
<?php } ?>                                                     

<div class="text-center">   
	<?php

		if (count($confSistemaOuvidoria) <= 0 && $cliente != "12097") { ?>
				<?
			if($cliente == "41") {
					?>
				<a href="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia . $ouvidoria . $complemento); ?>">
					<img src="<?= $CAMINHO ?>/images/ico_acesso_informacao_sjpatrocinio.png" alt="Ouvidoria"/>
				</a>
				<?php } else if($cliente == "59"){ ?>
				<a href="https://saojoaodoivai.1doc.com.br/b.php?pg=wp/wp&itd=5" target="_blank">
					<img src="<?= $CAMINHO ?>/images/ico_acesso_informacao.png" alt="Ouvidoria"/>
				</a>
				
				<?php } else if($cliente == "12046"){ ?>
				<a href="https://sistema.ouvidorias.gov.br/publico/PR/SantaMariana/Manifestacao/RegistrarManifestacao" target="_blank">
					<img src="<?= $CAMINHO ?>/images/ico_acesso_informacao.png" alt="Ouvidoria"/>
				</a>

				<?php } else if($cliente == "109"){ ?>
				<a href="https://sistema.ouvidorias.gov.br/publico/PR/TURVO/Manifestacao/RegistrarManifestacao" target="_blank">
					<img src="<?= $CAMINHO ?>/images/ico_acesso_informacao.png" alt="Ouvidoria"/>
				</a>
				
				<?php } else { ?>
					
				<a href="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia . $ouvidoria . $complemento); ?>">
					<img src="<?= $CAMINHO ?>/images/ico_acesso_informacao.png" alt="Ouvidoria"/>
				</a>
				<?php } ?>
		<? } ?>                                                                                            
	<?php
	if($cliente != "11924") {
		if ($configuracaoTransparencia['exibir_fale_conosco'] == "S") {

			$iconeFaleConosco = $cliente == "11945" ? "ico_esic.png" : "ico_fale_conosco.png";

			if ($configuracaoTransparencia['fale_conosco_ouvidoria'] == "S") {

				if (count($confSistemaOuvidoria) > 0) {
					?>
					<a href="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia . $sistemaGestaoOuvidoria . $complemento); ?>">
						<img src="<?= $CAMINHO ?>/images/<?= $iconeFaleConosco ?>" alt="Fale Conosco"/>
					</a>
				<?php } else { 
						
					if($cliente != "12097") {
					
					?>
					
						<a href="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia . $ouvidoria . $complemento); ?>">
						<img src="<?= $CAMINHO ?>/images/<?= $iconeFaleConosco ?>" alt="Fale Conosco"/>
					</a>
					
					<?php
				}
			}
			} else {

				if($cliente != "12097") {
					
					?>
				<a href="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia . $contato . $complemento); ?>">
					<img src="<?= $CAMINHO ?>/images/<?= $iconeFaleConosco ?>" alt="Fale Conosco"/>
				</a>
				<?php
			}
		}
		}
	}
	if($cliente == "11924" || $cliente == "12066" || $cliente == "1050") {

		if (count($confSistemaOuvidoria) > 0) {
			?>
			<a href="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia . $sistemaGestaoOuvidoria . $complemento); ?>">
				<img src="<?= $CAMINHO ?>/images/ico_ouvidoria.png" alt="Ouvidoria"/>
			</a>
		<?php } else { ?>
			<a href="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia . $ouvidoria . $complemento); ?>">
				<img src="<?= $CAMINHO ?>/images/ico_ouvidoria.png" alt="Ouvidoria"/>
			</a>
			<a href="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia . $formularioAtendimento . $complemento); ?>">
				<img src="<?= $CAMINHO ?>/images/ico_formularios_atendimento.png" alt="Download Formulários de Atendimento"/>
			</a>
		<?php
		}
		?>

		<a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$relatorioAtendimento.$complemento); ?>">
			<img src="<?=$CAMINHO ?>/images/ico_relatorio_atendimentos.png" alt="Relat&oacute;rios Estat&iacute;sticos de Atendimento" />
		</a>

	<?php
	}
	if($cliente == "12056") { ?>
		
		<a href="http://www.ingadigital.com.br/transparencia/index.php?id_cliente=<?=$cliente?>&sessao=d19f9047ddscd1">
			<img src="http://www.ingadigital.com.br/transparencia/images/ico_sic_fisico.png" alt="Serviço Físico de Informação ao Cidadão">
		</a>

	<?php } ?> 
	<?php
	if($cliente == "1190") {

		if (count($confSistemaOuvidoria) > 0) {
			?>
			<a href="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia . $sistemaGestaoOuvidoria . $complemento); ?>">
				<img src="<?= $CAMINHO ?>/images/ico_ouvidoria.png" alt="Ouvidoria"/>
			</a>
		<?php } else { ?>
			<a href="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia . $ouvidoria . $complemento); ?>">
				<img src="<?= $CAMINHO ?>/images/ico_ouvidoria.png" alt="Ouvidoria"/>
			</a>
			<a href="http://www.munhozdemello.pr.gov.br/index.php?sessao=b054603368b1b0&id=259">
				<img src="<?= $CAMINHO ?>/images/ico_formularios_atendimento.png" alt="Download Formulários de Atendimento"/>
			</a>
			
		<?php
		}
		?>



	<?php
	}

	$stConfSicCidadao = $conn->prepare("SELECT id FROM cliente_configuracao_transparencia_icone_cliente WHERE id_cliente = :id_cliente AND id_icone = :id_icone");
	$stConfSicCidadao->execute(array("id_cliente" => $cliente, "id_icone" => 51));
	$confSicCidadao = $stConfSicCidadao->fetchAll();
	if(count($confSicCidadao)) {

		if(empty($configuracaoTransparencia['caminho_sic_cidadao'])) {

			$linkSic = $CAMINHO . "/index.php?sessao=$sequencia" . $sicCidadao . "$complemento";

		} else {

			$linkSic = $CAMINHO . "/index.php?sessao=$sequencia" . $transparenciaOnlineVisualiza . "$complemento&origem=sic_cidadao&redir=link";

		}


	?>
	<a href="<?=$linkSic ?>">
		<img src="<?=$CAMINHO ?>/images/ico_sic_fisico.png" alt="Servi&ccedil;o F&iacute;sico de Informa&ccedil;&atilde;o ao Cidad&atilde;o" />
	</a>
	<?php } ?>
</div>
<?
$atualizacao = atualizacao("acesso_informacao", $cliente, $conn);

if($atualizacao != "") {
?>
<br><p class="text-right"><small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small></p>
<?php
} ?>