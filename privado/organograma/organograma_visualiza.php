<h2>Organograma</h2>
<ol class="breadcrumb">
	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
	<li class="active">Organograma</li>
</ol>

<?php
$id = secure($_REQUEST['id']);

if(empty($id)) {

	$stOrganograma = $conn->prepare("SELECT * FROM organograma WHERE id_cliente = :id_cliente AND status_registro = :status_registro ORDER BY id DESC LIMIT 1");
	$stOrganograma->execute(array("id_cliente" => $cliente, "status_registro" => "A"));

} else {

	$stOrganograma = $conn->prepare("SELECT * FROM organograma WHERE id_cliente = :id_cliente AND id = :id AND status_registro = :status_registro");
	$stOrganograma->execute(array("id_cliente" => $cliente, "id" => $id, "status_registro" => "A"));

}

$organograma = $stOrganograma->fetch();
?>
<h3><?= $organograma['titulo'] ?></h3>
<?= verifica($organograma['artigo']) ?>

<?php if($organograma['imagem'] != ""){ ?>
<img src="<?=$CAMINHOIMG ?>/<?= $organograma['imagem'] ?>" alt="<?= $organograma['titulo'] ?>" class="imagem">
<?php } ?>

<p>&nbsp;</p>
<?php if($organograma['arquivo'] != ""){ ?>
<p><strong><a href="<?= $CAMINHOARQ ?>/<?= $organograma['arquivo'] ?>" target="_blank"><i class="glyphicon glyphicon-cloud-download"></i> Baixar Anexo</a></strong></p>
<?php } ?>

<?php if($organograma['link'] != ""){ ?>
<p><strong><a href="<?= $organograma['link'] ?>" target="_blank"><i class="glyphicon glyphicon-globe"></i> <?= $organograma['link'] ?></a></strong></p>
<?php
}

$atualizacao = atualizacao("organograma", $cliente, $conn);
if($atualizacao != "") {
?>
<p class="text-right"><small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small></p>
<?php
}