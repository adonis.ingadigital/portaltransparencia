<h2>Organograma</h2>
<ol class="breadcrumb">
	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
	<li class="active">Organograma</li>
</ol>

<?php
$stOrganograma = $conn->prepare("SELECT * FROM organograma WHERE id_cliente = :id_cliente AND status_registro = :status_registro ORDER BY id DESC");
$stOrganograma->execute(array("id_cliente" => $cliente, "status_registro" => "A"));
$qryOrganograma = $stOrganograma->fetchAll();

if(count($qryOrganograma) == 1) {

	echo "<script>window.location='$CAMINHO/index.php?sessao=$sequencia"."$organogramaVisualiza"."$complemento';</script>";

} else if(count($qryOrganograma) > 1) {
?>
<table class="table table-hover table-striped">
	<?php foreach ($qryOrganograma as $organograma) { ?>
	<tr>
		<td>
			<a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$organogramaVisualiza.$complemento); ?>&id=<?= verifica($organograma['id']) ?>"><i class="glyphicon glyphicon-triangle-right"></i> <strong><?= $organograma['titulo'] ?></strong></a>
		</td>
	</tr>
	<?php } ?>
</table>
<?php
}

$atualizacao = atualizacao("organograma", $cliente, $conn);
if($atualizacao != "") {
?>
<p class="text-right"><small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small></p>
<?php
}