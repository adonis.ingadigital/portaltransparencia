<h2>Selecione a Entidade</h2>

<h3>Ops, desculpe-nos! Houve algum problema, selecione abaixo a entidade desejada</h3>

<?php
if($_GET['busca']) $wh = "AND cliente.razao_social LIKE '%".secure($_GET['busca'])."%' ";

$stCliente = $conn->query("SELECT cliente.*
							 FROM cliente, cliente_configuracao_transparencia
							WHERE cliente_configuracao_transparencia.id_cliente = cliente.id
							  AND cliente.status_registro = 'A'
							  AND cliente.razao_social NOT LIKE '%modelo%' $wh
						 ORDER BY cliente.razao_social");

$qryCliente = $stCliente->fetchAll(PDO::FETCH_ASSOC);
if(count($qryCliente) > 0) {
?>
<p class="clearfix"></p>
<form class="form-inline" action="" method="get">
	<div class="input-group">
	    <input type="search" name="busca" class="form-control" value="<?= $_GET['busca'] ?>" placeholder="Pesquisar...">
	    <input type="hidden" name="sessao" value="<?= verifica($sequencia.$listaCliente.$complemento); ?>" />
	    <input type="hidden" name="url" value="<?= $_GET['url'] ?>" />
		<span class="input-group-btn">
		    <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-search"></i></button>
		</span>
	</div>
</form>
<p class="clearfix"></p>

<table class="table table-hover table-striped">
<?php
	foreach ($qryCliente as $cliente) {
?>
	<tr>
		<td><a href="<?= $_GET['url'] ?>&id_cliente=<?= $cliente['id'] ?>"><i class="glyphicon glyphicon-ok"></i> <strong><?= $cliente['nome_fantasia'] ?></strong></a></td>
	</tr>
	<?php } ?>
</table>
<?php } ?>