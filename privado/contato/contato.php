<h2>Fale Conosco</h2>
<ol class="breadcrumb">
	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
	<li class="active">Fale Conosco</li>
</ol>
<style>
.some{
	display: none;
}
</style>
<?php


include "../../privado/transparencia/recaptchalib.php";

if($_POST['acao'] == "cadastrar" && $_POST['telephone'] == "") {

	foreach ($_POST as $campo => $valor) { $$campo = secure($valor);}

	$cap = new GoogleRecaptcha();
	$verified = $cap->VerifyCaptcha($_POST['g-recaptcha-response']);

	if(!$verified) {

		echo'<p class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				O Captcha n&atilde;o foi resolvido! Verifique.
			</p>';

	} else if(empty($nome)) {

		echo'<p class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				Preencha o campo Nome!
			</p>';

	} else if(!empty($email) && filter_var($email, FILTER_VALIDATE_EMAIL) === false) {

		echo'<p class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				Forne&ccedil;a um e-mail v&aacute;lido!
			</p>';

	} else if(empty($municipio) || empty($estado)) {

		echo'<p class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				Informe seu estado e sua cidade!
			</p>';

	} else if(empty($mensagem)) {

		echo'<p class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				Preencha o campo Mensagem!
			</p>';

	} else {

		$email_destino = filter_var($setor, FILTER_VALIDATE_EMAIL) === false ? $dadosCliente['email'] : $setor;
		


		if(filter_var($email_destino, FILTER_VALIDATE_EMAIL) !== false){

			$data	 = date("d/m/Y");
			$hora    = date("H:i");

			$subject = html_entity_decode("CONTATO - Enviado pelo site da $dadosCliente[nome_fantasia]");

			$html    = "<table width=\"100%\">";
			$html   .= "<tr>";
			$html   .= "<td colspan=\"2\" style=\"font-size:10px;\">$data &nbsp; $hora</td>";
			$html   .= "</tr>";
			$html   .= "<tr>";
			$html   .= "<td width=\"30%\"><strong>Nome: </strong></td>";
			$html   .= "<td width=\"70%\">$nome</td>";
			$html   .= "</tr>";

			$html   .= "<tr>";
			$html   .= "<td><strong>E-mail: </strong></td>";
			$html   .= "<td>$email</td>";
			$html   .= "</tr>";

			$html   .= "<tr>";
			$html   .= "<td><strong>Cidade: </strong></td>";
			$html   .= "<td>$municipio - $estado</td>";
			$html   .= "</tr>";

			$html   .= "<tr>";
			$html   .= "<td><strong>Mensagem: </strong></td>";
			$html   .= "<td>$mensagem</td>";
			$html   .= "</tr>";

			$html   .= "<tr>";
			$html   .= "<td><strong>Endere&ccedil;o IP do remetente: </strong></td>";
			$html   .= "<td>".$_SERVER['REMOTE_ADDR']."</td>";
			$html   .= "</tr>";

			$html   .= "</table>";

			$headers  = html_entity_decode($dadosCliente['nome_fantasia']) . " <$email_destino>";

			$to[0]['email'] = $email_destino;
			$to[0]['nome'] = "Contato";
			$vai = envia_email_aws($to, $subject, $html, $headers, [], './ingadigital.com.br/privado/transparencia/contato/contato.php:106:');
		} 

		if($vai) {

			echo'<p class="alert alert-success">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					E-mail enviado com sucesso!
				</p>';

		} else {

			echo'<p class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					N&atilde;o foi poss&iacute;vel enviar seu e-mail! Tente novamente mais tarde.
				</p>';

		}

	}
}

$stSetor = $conn->prepare("SELECT * FROM cliente_fale_conosco_setor WHERE id_cliente = :id_cliente AND status_registro = :status_registro");
$stSetor->execute(array("id_cliente" => $cliente, "status_registro" => "A"));
$qrySetor = $stSetor->fetchAll();

$stEstado = $conn->prepare("SELECT * FROM estado WHERE status_registro = :status_registro ORDER BY nome");
$stEstado->execute(array("status_registro" => "A"));
$qryEstado = $stEstado->fetchAll();
?>
<form class="form-horizontal validar_formulario" action="" method="post">
	<?php
	
	if(count($qrySetor) > 0) { ?>
	<div class="form-group">
		<label for="setor" class="col-sm-2 control-label">Setor</label>
		<div class="col-sm-10">
			<select name="setor" id="setor" class="form-control required" required>
				<option value="">Selecione o Setor</option>
				<?php
				foreach ($qrySetor as $setor) {

					echo "<option value='$setor[email]'";
					if($_POST['setor'] == $setor['email']) echo " selected";
					echo ">$setor[descricao]</option>";
				}
				?>
			</select>
		</div>
	</div>
	<?php } ?>
	<div class="form-group">
		<label for="nome" class="col-sm-2 control-label">Nome</label>
		<div class="col-sm-10">
			<input type="text" name="nome" id="nome" class="form-control required" required value="<?= $_POST['nome'] ?>" placeholder="Digite seu nome...">
		</div>
	</div>
	<div class="form-group">
		<label for="email" class="col-sm-2 control-label">E-mail</label>
		<div class="col-sm-10">
			<input type="email" name="email" id="email" class="form-control email" value="<?= $_POST['email'] ?>" placeholder="Digite seu e-mail...">
			<input class="some" name="telephone" value="">
		</div>
	</div>
	<div class="form-group">
		<label for="municipio" class="col-sm-2 control-label">Cidade</label>
		<div class="col-sm-10">
			<input type="text" name="municipio" id="municipio" class="form-control required" required value="<?= $_POST['municipio'] ?>" placeholder="Informe sua cidade...">
		</div>
	</div>
	<div class="form-group">
		<label for="estado" class="col-sm-2 control-label">Estado</label>
		<div class="col-sm-10">
			<select name="estado" id="estado" class="form-control required" required>
				<option value="">Selecione o Estado</option>
				<?php
				if(count($qryEstado)) {

					foreach ($qryEstado as $buscaEstado) {

						echo "<option value='$buscaEstado[uf]' ";
						if($buscaEstado['uf'] == $_POST['estado']) echo "selected";
						echo ">$buscaEstado[nome]</option>";
					}
				}
				?>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label for="mensagem" class="col-sm-2 control-label">Mensagem</label>
		<div class="col-sm-10">
			<textarea name="mensagem" id="mensagem" class="form-control required" required rows="6" placeholder="Digite sua mensagem..."><?= $_POST['mensagem'] ?></textarea>
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10 captcha" id="recaptcha1"></div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<input type="hidden" name="acao" value="cadastrar">
			<button type="reset" class="btn btn-danger">Limpar</button>
			<button type="submit" class="btn btn-primary">Enviar</button>
		</div>
	</div>
</form>