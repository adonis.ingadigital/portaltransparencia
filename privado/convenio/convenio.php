<h2>Conv&ecirc;nios</h2>
<ol class="breadcrumb">
	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
	<li class="active">Conv&ecirc;nios</li>
</ol>

<?php
$stCategoria = $conn->prepare("SELECT * FROM convenio_categoria WHERE id_cliente = :id_cliente AND status_registro = :status_registro ORDER BY id DESC");
$stCategoria->execute(array("id_cliente" => $cliente, "status_registro" => "A"));
$qryCategoria = $stCategoria->fetchAll();

if(count($qryCategoria)) {
?>
<ul class="treeview">
	<?php
	foreach ($qryCategoria as $categoria) {
	?>
	<li><a href="#"><?= $categoria['descricao'] ?></a>
		<?php
		$stConvenio = $conn->prepare("SELECT * FROM convenio WHERE id_categoria = :id_categoria AND (id_subcategoria IS NULL OR id_subcategoria = '') AND status_registro = :status_registro ORDER BY titulo DESC");
		$stConvenio->execute(array("id_categoria" => $categoria['id'], "status_registro" => "A"));
		$qryConvenio = $stConvenio->fetchAll();

		if(count($qryConvenio)) {
		?>
		<ul>
			<li>
				<div class="panel-group" id="accordion_<?= $categoria['id'] ?>" role="tablist" aria-multiselectable="true">
					<?php foreach ($qryConvenio as $convenio) { ?>
					<div class="panel panel-default">
					    <div class="panel-heading" role="tab" id="heading_<?= $convenio['id'] ?>">
					     	<h4 class="panel-title">
					        	<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?= $convenio['id'] ?>" aria-expanded="true" aria-controls="collapse_<?= $convenio['id'] ?>">
					          		<i class="glyphicon glyphicon-triangle-right"></i> <?= $convenio['titulo']?>
					        	</a>
					    	</h4>
					    </div>
						<div id="collapse_<?= $convenio['id'] ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_<?= $convenio['id'] ?>">
							<div class="panel-body">
								<?php if($convenio['data_inicio'] != "") { ?><strong>Data de In&iacute;cio:</strong> <?= formata_data($convenio['data_inicio']) ?><br><?php } ?>
								<?php if($convenio['data_fim'] != "") { ?><strong>Fim da Vig&ecirc;ncia:</strong> <?= formata_data($convenio['data_fim']) ?><br><?php } ?>
								<?php if($convenio['numero'] != "") { ?><strong>N&ordm; do Processo:</strong> <?= $convenio['numero'] ?><br><?php } ?>
								<?php if($convenio['valor'] != "") { ?><strong>Valor:</strong> <?= $convenio['valor'] ?><br><?php } ?>
								<?php if($convenio['contrapartida'] != "") { ?><strong>Contra partida:</strong> <?= $convenio['contrapartida'] ?><br><?php } ?>
								<?php if($convenio['proponente'] != "") { ?><strong>Proponente:</strong> <?= $convenio['proponente'] ?><br><?php } ?>
								<?php if($convenio['fornecedor'] != "") { ?><strong>Fornecedor:</strong> <?= $convenio['fornecedor'] ?><br><?php } ?>
								<?php if($convenio['objeto'] != "") { ?><strong>Objeto:</strong> <?= verifica($convenio['objeto']) ?><br><?php } ?>

								<?php
								$stAnexo = $conn->prepare("SELECT * FROM convenio_anexo WHERE id_artigo = :id_artigo ORDER BY ordem ASC, id DESC");
								$stAnexo->execute(array("id_artigo" => $convenio['id']));
								$qryAnexo = $stAnexo->fetchAll();

								if(count($qryAnexo)) {
								?>
								<div class="panel panel-primary">
									<div class="panel-heading">
								    	<h2 class="panel-title"><i class="glyphicon glyphicon-cloud-download"></i> Anexos</h2>
								  	</div>
									<div class="panel-body">
									<?php
									foreach ($qryAnexo as $anexo) {
									?>
										<p><strong><a href="<?= $CAMINHOARQ ?>/<?= $anexo['arquivo'] ?>" target="_blank"><i class="glyphicon glyphicon-cloud-download"></i> <?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?></a></strong></p>
									<?php } ?>
									</div>
								</div>
								<?php } ?>

								<?php
								$stLink = $conn->prepare("SELECT * FROM convenio_link WHERE id_artigo = :id_artigo ORDER BY ordem ASC, id DESC");
								$stLink->execute(array("id_artigo" => $convenio['id']));
								$qryLink = $stLink->fetchAll();

								if(count($qryLink)) {
								?>
								<div class="panel panel-primary">
									<div class="panel-heading">
								    	<h2 class="panel-title"><i class="glyphicon glyphicon-globe"></i> Links</h2>
								  	</div>
									<div class="panel-body">
									<?php
									foreach ($qryLink as $linkConvenio) {
									?>
										<p><strong><a href="<?= $linkConvenio['link'] ?>" target="_blank"><i class="glyphicon glyphicon-globe"></i> <?= empty($linkConvenio['descricao']) ? "Link" : $linkConvenio['descricao'] ?></a></strong></p>
									<?php } ?>
									</div>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>
					<?php } ?>
				</div>
			</li>
		</ul>
		<?php } ?>
		<?php
		$stSubcategoria = $conn->prepare("SELECT * FROM convenio_subcategoria WHERE id_categoria = :id_categoria AND status_registro = :status_registro ORDER BY id DESC");
		$stSubcategoria->execute(array("id_categoria" => $categoria['id'], "status_registro" => "A"));
		$qrySubcategoria = $stSubcategoria->fetchAll();

		if(count($qrySubcategoria)) {
		?>
		<ul>
			<?php
			foreach ($qrySubcategoria as $subcategoria) {
			?>
			<li><a href="#"><?= $subcategoria['descricao'] ?></a>
				<?php
				$stConvenio = $conn->prepare("SELECT * FROM convenio WHERE id_subcategoria = :id_subcategoria AND status_registro = :status_registro ORDER BY titulo DESC");
				$stConvenio->execute(array("id_subcategoria" => $subcategoria['id'], "status_registro" => "A"));
				$qryConvenio = $stConvenio->fetchAll();

				if(count($qryConvenio)) {
				?>
				<ul>
					<li>
						<div class="panel-group" id="accordion_<?= $subcategoria['id'] ?>" role="tablist" aria-multiselectable="true">
							<?php foreach ($qryConvenio as $convenio) { ?>
							<div class="panel panel-default">
							    <div class="panel-heading" role="tab" id="heading_<?= $convenio['id'] ?>">
							     	<h4 class="panel-title">
							        	<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?= $convenio['id'] ?>" aria-expanded="true" aria-controls="collapse_<?= $convenio['id'] ?>">
							          		<i class="glyphicon glyphicon-triangle-right"></i> <?= $convenio['titulo'] ?>
							        	</a>
							    	</h4>
							    </div>
								<div id="collapse_<?= $convenio['id'] ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_<?= $convenio['id'] ?>">
									<div class="panel-body">
										<?php if($convenio['data_inicio'] != "") { ?><strong>Data de In&iacute;cio:</strong> <?= formata_data($convenio['data_inicio']) ?><br><?php } ?>
										<?php if($convenio['data_fim'] != "") { ?><strong>Fim da Vig&ecirc;ncia:</strong> <?= formata_data($convenio['data_fim']) ?><br><?php } ?>
										<?php if($convenio['numero'] != "") { ?><strong>N&ordm; do Processo:</strong> <?= $convenio['numero'] ?><br><?php } ?>
										<?php if($convenio['valor'] != "") { ?><strong>Valor:</strong> <?= $convenio['valor'] ?><br><?php } ?>
										<?php if($convenio['contrapartida'] != "") { ?><strong>Contra partida:</strong> <?= $convenio['contrapartida'] ?><br><?php } ?>
										<?php if($convenio['proponente'] != "") { ?><strong>Proponente:</strong> <?= $convenio['proponente'] ?><br><?php } ?>
										<?php if($convenio['fornecedor'] != "") { ?><strong>Fornecedor:</strong> <?= $convenio['fornecedor'] ?><br><?php } ?>
										<?php if($convenio['objeto'] != "") { ?><strong>Objeto:</strong> <?= $convenio['objeto'] ?><br><?php } ?>

										<?php
										$stAnexo = $conn->prepare("SELECT * FROM convenio_anexo WHERE id_artigo = :id_artigo ORDER BY ordem ASC, id DESC");
										$stAnexo->execute(array("id_artigo" => $convenio['id']));
										$qryAnexo = $stAnexo->fetchAll();

										if(count($qryAnexo)) {
										?>
										<div class="panel panel-primary">
											<div class="panel-heading">
										    	<h2 class="panel-title"><i class="glyphicon glyphicon-cloud-download"></i> Anexos</h2>
										  	</div>
											<div class="panel-body">
											<?php
											foreach ($qryAnexo as $anexo) {
											?>
												<p><strong><a href="<?= $CAMINHOARQ ?>/<?= $anexo['arquivo'] ?>" target="_blank"><i class="glyphicon glyphicon-cloud-download"></i> <?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?></a></strong></p>
											<?php } ?>
											</div>
										</div>
										<?php } ?>

										<?php
										$stLink = $conn->prepare("SELECT * FROM convenio_link WHERE id_artigo = :id_artigo ORDER BY ordem ASC, id DESC");
										$stLink->execute(array("id_artigo" => $convenio['id']));
										$qryLink = $stLink->fetchAll();

										if(count($qryLink)) {
										?>
										<div class="panel panel-primary">
											<div class="panel-heading">
										    	<h2 class="panel-title"><i class="glyphicon glyphicon-globe"></i> Links</h2>
										  	</div>
											<div class="panel-body">
											<?php
											foreach ($qryLink as $linkConvenio) {
											?>
												<p><strong><a href="<?= $linkConvenio['link'] ?>" target="_blank"><i class="glyphicon glyphicon-globe"></i> <?= empty($linkConvenio['descricao']) ? "Link" : $linkConvenio['descricao'] ?></a></strong></p>
											<?php } ?>
											</div>
										</div>
										<?php } ?>
									</div>
								</div>
							</div>
							<?php } ?>
						</div>
					</li>
				</ul>
				<?php } ?>
			</li>
			<?php } ?>
		</ul>
		<?php } ?>
	</li>
	<?php } ?>
</ul>
<?php
}

$atualizacao = atualizacao("convenio", $cliente, $conn);
if($atualizacao != "") {
?>
<p class="text-right"><small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small></p>
<?php
}