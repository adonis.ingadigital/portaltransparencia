<h2>Di&aacute;rias</h2>
<ol class="breadcrumb">
	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
	<li class="active">Di&aacute;rias</li>
</ol>

<?php if($cliente != "11999" && $cliente != "12064" && $cliente != "12126"){ ?>

<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#form-diaria" aria-expanded="false" aria-controls="form-diaria">
	<i class="glyphicon glyphicon-search"></i> Pesquisar
</button>

<? } ?>

<p class="clearfix"></p>

<form class="form-horizontal collapse" id="form-diaria" action="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$diariaLista.$complemento); ?>" method="post">
	<div class="form-group">
		<label for="titulo" class="col-sm-2 control-label">T&iacute;tulo</label>
		<div class="col-sm-10">
			<input type="text" name="titulo" id="titulo" class="form-control" value="<?= $_POST['titulo'] ?>" placeholder="Informe o t&iacute;tulo da di&aacute;ria...">
		</div>
	</div>
	<div class="form-group">
		<label for="beneficiario" class="col-sm-2 control-label">Benefici&aacute;rio</label>
		<div class="col-sm-10">
			<input type="text" name="beneficiario" id="beneficiario" class="form-control" value="<?= $_POST['beneficiario'] ?>" placeholder="Informe o nome do benefici&aacute;rio...">
		</div>
	</div>
	<div class="form-group">
		<label for="rg_beneficiario" class="col-sm-2 control-label">RG do Benefici&aacute;rio</label>
		<div class="col-sm-10">
			<input type="text" name="rg_beneficiario" id="rg_beneficiario" class="form-control" value="<?= $_POST['rg_beneficiario'] ?>" placeholder="Informe o RG do benefici&aacute;rio...">
		</div>
	</div>
	<div class="form-group">
		<label for="data_inicio_viagem" class="col-sm-2 control-label">Data Inicial da Viagem</label>
		<div class="col-sm-10">
			<input type="text" name="data_inicio_viagem" id="data_inicio_viagem" class="form-control data" value="<?= $_POST['data_inicio_viagem'] ?>" placeholder="Informe a data inicial da viagem...">
		</div>
	</div>
	<div class="form-group">
		<label for="data_fim_viagem" class="col-sm-2 control-label">Data Final da Viagem</label>
		<div class="col-sm-10">
			<input type="text" name="data_fim_viagem" id="data_fim_viagem" class="form-control data" value="<?= $_POST['data_fim_viagem'] ?>" placeholder="Informe a data final da viagem...">
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<input type="hidden" name="acao" value="cadastrar">
			<button type="reset" class="btn btn-danger">Limpar</button>
			<button type="submit" class="btn btn-primary">Enviar</button>
		</div>
	</div>
</form>

<?php
$sqlCategoria = "SELECT * FROM diaria_categoria WHERE id_cliente = :id_cliente AND status_registro = :status_registro ";
$sqlDiariaSemCategoria = "SELECT * FROM diaria WHERE id_categoria = :id_categoria AND (id_subcategoria IS NULL OR id_subcategoria = '') AND status_registro = :status_registro ";
$sqlSubcategoria = "SELECT * FROM diaria_subcategoria WHERE id_categoria = :id_categoria AND status_registro = :status_registro ";
$sqlDiaria = "SELECT * FROM diaria WHERE id_subcategoria = :id_subcategoria AND status_registro = :status_registro ";
$vetor['status_registro'] = "A";

if($_POST) {

	$sqlCategoria .= "AND id IN (SELECT id_categoria FROM diaria WHERE status_registro = :status_registro ";
	$sqlSubcategoria .= "AND id IN (SELECT id_subcategoria FROM diaria WHERE status_registro = :status_registro ";
	$sqlBusca = "";

	if($_POST['titulo'] != "") {

		$sqlBusca .= "AND titulo LIKE :titulo ";
		$vetor["titulo"] = "%" . secure($_POST['titulo']) . "%";

	}

	if($_POST['beneficiario'] != "") {

		$sqlBusca .= "AND beneficiario LIKE :beneficiario ";
		$vetor["beneficiario"] = "%" . secure($_POST['beneficiario']) . "%";

	}

	if($_POST['rg_beneficiario'] != "") {

		$sqlBusca .= "AND (rg_beneficiario LIKE :rg_beneficiario OR rg_beneficiario LIKE :rg_beneficiario2) ";
		$vetor["rg_beneficiario"] = "%" . secure($_POST['rg_beneficiario']) . "%";
		$vetor["rg_beneficiario2"] = "%" . secure(preg_replace('#[^0-9]#', '', $_POST['rg_beneficiario'])) . "%";

	}

	if($_POST['data_inicio_viagem'] != "") {

		$sqlBusca .= "AND data_inicio_viagem = :data_inicio_viagem ";
		$vetor['data_inicio_viagem'] = formata_data_banco($_POST['data_inicio_viagem']);

	}

	if($_POST['data_fim_viagem'] != "") {

		$sqlBusca .= "AND data_fim_viagem = :data_fim_viagem ";
		$vetor['data_fim_viagem'] = formata_data_banco($_POST['data_fim_viagem']);

	}

	$sqlCategoria .= "$sqlBusca) ";
	$sqlDiariaSemCategoria .= $sqlBusca;
	$sqlSubcategoria .= "$sqlBusca) ";
	$sqlDiaria .= $sqlBusca;

}

$stCategoria = $conn->prepare("$sqlCategoria ORDER BY id DESC");
$vetorCat = $vetor;
$vetorCat['id_cliente'] = $cliente;
$stCategoria->execute($vetorCat);
$qryCategoria = $stCategoria->fetchAll();

if(count($qryCategoria)) {
?>
<ul class="treeview">
	<?php
	foreach ($qryCategoria as $categoria) {
	?>
	<li><a href="#"><?= $categoria['descricao'] ?></a>
		<?php
		if($cliente == "48") $orderDiaria = "data_inicio_viagem DESC";
		else if($cliente == "43") $orderDiaria = "titulo DESC";
		else $orderDiaria = "titulo ASC";

		$stDiaria = $conn->prepare("$sqlDiariaSemCategoria ORDER BY $orderDiaria");
		$vetorDiariaSemCategoria = $vetor;
		$vetorDiariaSemCategoria['id_categoria'] = $categoria['id'];
		$stDiaria->execute($vetorDiariaSemCategoria);
		$qryDiaria = $stDiaria->fetchAll();

		if(count($qryDiaria)) {
		?>
		<ul>
			<?php
			foreach($qryDiaria as $diaria) {

				$stAnexo = $conn->prepare("SELECT COUNT(id) total FROM diaria_anexo WHERE id_artigo = :id_artigo ORDER BY ordem ASC, id DESC");
				$stAnexo->execute(array("id_artigo" => $diaria['id']));
				$totalAnexo = $stAnexo->fetch();

				if($totalAnexo['total'] > 0 ||
					$diaria['destino'] != "" ||
					$diaria['beneficiario'] != "" ||
					$diaria['cargo'] != "" ||
					$diaria['justificativa'] != "" ||
					$diaria['data_inicio_viagem'] != "" ||
					$diaria['meio_transporte'] != "" ||
					$diaria['custo_meio_transporte'] != "" ||
					$diaria['quantidade_diaria'] != "" ||
					$diaria['valor_unitario_diaria'] != "" ||
					$diaria['certificado'] != "") {

					$linkDiaria = $CAMINHO . "/index.php?sessao=$sequencia".$diariaVisualiza."$complemento&id=" . $diaria['id'];
					$targetDiaria = "_self";
					$imagem = "glyphicon-chevron-right";

				} else if($diaria['link'] != "") {

					$linkDiaria = $diaria['link'];
					$targetDiaria = "_blank";
					$imagem = "glyphicon-globe";

				} else if($diaria['arquivo'] != "") {

					$linkDiaria = $CAMINHOARQ . "/" . $diaria['arquivo'];
					$targetDiaria = "_blank";
					$imagem = "glyphicon-cloud-download";

				} else {

					$linkDiaria = "#";
					$targetDiaria = "_self";
					$imagem = "glyphicon-chevron-right";

				}
			?>
			<li>
				<a href="<?= $linkDiaria ?>" target="<?= $targetDiaria ?>"><span class="text-primary"><i class="glyphicon <?= $imagem ?>"></i>&nbsp;&nbsp;<?= $diaria['titulo'] ?></span></a>
			</li>
			<?php } ?>
		</ul>
		<?php } ?>
		<?php
		$stSubcategoria = $conn->prepare("$sqlSubcategoria ORDER BY id DESC");
		$vetorSubcategoria = $vetor;
		$vetorSubcategoria['id_categoria'] = $categoria['id'];
		$stSubcategoria->execute($vetorSubcategoria);
		$qrySubcategoria = $stSubcategoria->fetchAll();

		if(count($qrySubcategoria)) {
		?>
		<ul>
			<?php
			foreach ($qrySubcategoria as $subcategoria) {
			?>
			<li><a href="#"><?= $subcategoria['descricao'] ?></a>
				<?php
				$stDiaria = $conn->prepare("$sqlDiaria ORDER BY $orderDiaria");
				$vetorDiaria = $vetor;
				$vetorDiaria['id_subcategoria'] = $subcategoria['id'];
				$stDiaria->execute($vetorDiaria);
				$qryDiaria = $stDiaria->fetchAll();

				if(count($qryDiaria)) {
				?>
				<ul>
					<?php
					foreach($qryDiaria as $diaria) {

						$stAnexo = $conn->prepare("SELECT COUNT(id) total FROM diaria_anexo WHERE id_artigo = :id_artigo ORDER BY ordem ASC, id DESC");
						$stAnexo->execute(array("id_artigo" => $diaria['id']));
						$totalAnexo = $stAnexo->fetch();

						if($totalAnexo['total'] > 0 ||
						   	$diaria['destino'] != "" ||
							$diaria['beneficiario'] != "" ||
							$diaria['cargo'] != "" ||
							$diaria['justificativa'] != "" ||
							$diaria['data_inicio_viagem'] != "" ||
							$diaria['meio_transporte'] != "" ||
							$diaria['custo_meio_transporte'] != "" ||
							$diaria['quantidade_diaria'] != "" ||
							$diaria['valor_unitario_diaria'] != "" ||
							$diaria['certificado'] != "") {

							$linkDiaria = $CAMINHO . "/index.php?sessao=$sequencia".$diariaVisualiza."$complemento&id=" . $diaria['id'];
							$targetDiaria = "_self";
							$imagem = "glyphicon-chevron-right";

						} else if($diaria['link'] != "") {

							$linkDiaria = $diaria['link'];
							$targetDiaria = "_blank";
							$imagem = "glyphicon-globe";

						} else if($diaria['arquivo'] != "") {

							$linkDiaria = $CAMINHOARQ . "/" . $diaria['arquivo'];
							$targetDiaria = "_blank";
							$imagem = "glyphicon-cloud-download";

						} else {

							$linkDiaria = "#";
							$targetDiaria = "_self";
							$imagem = "glyphicon-chevron-right";

						}
					?>
					<li>
						<a href="<?= $linkDiaria ?>" target="<?= $targetDiaria ?>"><span class="text-primary"><i class="glyphicon <?= $imagem ?>"></i>&nbsp;&nbsp;<?= $diaria['titulo'] ?></span></a>
					</li>
					<?php } ?>
				</ul>
				<?php } ?>
			</li>
			<?php } ?>
		</ul>
		<?php } ?>
	</li>
	<?php } ?>
</ul>
<?php
}

$atualizacao = atualizacao("diaria", $cliente, $conn);
if($atualizacao != "") {
?>
<p class="text-right"><small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small></p>
<?php
}