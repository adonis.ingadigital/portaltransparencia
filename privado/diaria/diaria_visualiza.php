<?php
$id = secure($_GET['id']);

$stDiaria = $conn->prepare("SELECT diaria.*, diaria_categoria.descricao categoria, diaria_subcategoria.descricao subcategoria 
							  FROM diaria 
						 LEFT JOIN diaria_categoria ON diaria.id_categoria = diaria_categoria.id 
						 LEFT JOIN diaria_subcategoria ON diaria.id_subcategoria = diaria_subcategoria.id
							 WHERE diaria.id = :id 
							   AND diaria.status_registro = :status_registro");
$stDiaria->execute(array("id" => $id, "status_registro" => "A"));
$diaria = $stDiaria->fetch();
?>
<h2>Di&aacute;rias</h2>
<ol class="breadcrumb">
	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
	<li><a href="<?=$CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$diariaLista.$complemento); ?>">Di&aacute;rias</a></li>
	<?php if($diaria['categoria'] != "") { ?><li><?= $diaria['categoria'] ?></li><?php } ?>
	<?php if($diaria['subcategoria'] != "") { ?><li><?= $diaria['subcategoria'] ?></li><?php } ?>
	<li class="active"><?= $diaria['titulo'] ?></li>
</ol>

<h3><?= $diaria['titulo'] ?></h3>
<?php if($diaria['arquivo'] != "") { ?><a href="<?= $CAMINHOARQ ?>/<?= $diaria['arquivo'] ?>" target="_blank" class="label label-primary"><i class="glyphicon glyphicon-cloud-download"></i> Baixar anexo</a><br><br><?php } ?>
<?php if($diaria['link'] != "") { ?><a href="<?= $diaria['link'] ?>" target="_blank" class="label label-primary"><i class="glyphicon glyphicon-globe"></i> Acessar Link</a><br><?php } ?>
<?php if($diaria['destino'] != "") { ?><strong>Destino:</strong> <?= $diaria['destino'] ?><br><?php } ?>
<?php if($diaria['beneficiario'] != "") { ?><strong>Benefici&aacute;rio:</strong> <?= $diaria['beneficiario'] ?><br><?php } ?>
<?php if($diaria['rg_beneficiario'] != "") { ?><strong>RG do Benefici&aacute;rio:</strong> <?= $diaria['rg_beneficiario'] ?><br><?php } ?>
<?php if($diaria['cargo'] != "") { ?><strong>Cargo:</strong> <?= $diaria['cargo'] ?><br><?php } ?>
<?php if($diaria['justificativa'] != "") { ?><strong>Justificativa:</strong> <?= $diaria['justificativa'] ?><br><?php } ?>
<?php if($diaria['data_inicio_viagem'] != "") { ?><strong>Data Inicial da Viagem:</strong> <?= formata_data($diaria['data_inicio_viagem']) ?><br><?php } ?>
<?php if($diaria['data_fim_viagem'] != "") { ?><strong>Data Final da Viagem:</strong> <?= formata_data($diaria['data_fim_viagem']) ?><br><?php } ?>
<?php if($diaria['meio_transporte'] != "") { ?><strong>Meio de Transporte:</strong> <?= $diaria['meio_transporte'] ?><br><?php } ?>
<?php if($diaria['custo_meio_transporte'] != "") { ?><strong>Custo do Meio de Transporte:</strong> <?= $diaria['custo_meio_transporte'] ?><br><?php } ?>
<?php if($diaria['quantidade_diaria'] != "") { ?><strong>Quantidade de Di&aacute;rias:</strong> <?= $diaria['quantidade_diaria'] ?><br><?php } ?>
<?php if($diaria['valor_unitario_diaria'] != "") { ?><strong>Valor Unit&aacute;rio da Di&aacute;ria:</strong> <?= $diaria['valor_unitario_diaria'] ?><br><?php } ?>
<?php if($diaria['valor_total_diaria'] != "") { ?><strong>Valor Total da Di&aacute;ria:</strong> <?= $diaria['valor_total_diaria'] ?><br><?php } ?>
<?php if($diaria['certificado'] != "") { ?><a href="<?= $CAMINHOARQ ?>/<?= $diaria['certificado'] ?>" target="_blank"class="label label-primary"><i class="glyphicon glyphicon-cloud-download"></i>Baixar Certificado</a><br><?php } ?>

<div class="clearfix">&nbsp;</div>
<?php
$stAnexo = $conn->prepare("SELECT * FROM diaria_anexo WHERE id_artigo = :id_artigo ORDER BY ordem ASC, id DESC");
$stAnexo->execute(array("id_artigo" => $diaria['id']));
$qryAnexo = $stAnexo->fetchAll();

if(count($qryAnexo)) {
?>
<div class="panel panel-primary">
	<div class="panel-heading">
		<h2 class="panel-title"><i class="glyphicon glyphicon-cloud-download"></i> Anexos</h2>
	</div>
	<div class="panel-body">
	<?php
	foreach ($qryAnexo as $anexo) {
		?>
		<p><strong><a href="<?= $CAMINHOARQ ?>/<?= $anexo['arquivo'] ?>" target="_blank"><i class="glyphicon glyphicon-cloud-download"></i> <?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?></a></strong></p>
	<?php } ?>
	</div>
</div>
<?php } ?>

<a href="<?=$CAMINHO?>/index.php?sessao=<?= $sequencia.$diariaLista.$complemento?>" class="btn btn-primary"><i class="glyphicon glyphicon-arrow-left"></i> Voltar</a>