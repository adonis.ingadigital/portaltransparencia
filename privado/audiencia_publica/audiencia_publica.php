<?php

$complemento_titulo = "";
if($cliente == "45"){
	if(isset($_GET['ap']) && $_GET['ap'] == "1"){
		$ap = true;
		$complemento_titulo = " para Avalia&ccedil;&atilde;o de Metas Fiscais";
	}
	if(!isset($_GET['ap']) || $_GET['ap'] != "1"){
		$ap = false;
	}
}

?>

<h2>Audi&ecirc;ncia P&uacute;blica <?= $complemento_titulo ?></h2>
<ol class="breadcrumb">
	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
	<li class="active">Audi&ecirc;ncia P&uacute;blica <?= $complemento_titulo ?></li>
</ol>

<?php
$stCategoria = $conn->prepare("SELECT * FROM audiencia_publica_categoria WHERE id_cliente = :id_cliente AND status_registro = :status_registro ORDER BY id DESC");
$stCategoria->execute(array("id_cliente" => $cliente, "status_registro" => "A"));
$qryCategoria = $stCategoria->fetchAll();

if(count($qryCategoria)) {
?>
<ul class="treeview">
	<?php
	foreach ($qryCategoria as $categoria) {
	?>
	<li><a href="#"><?= $categoria['descricao'] ?></a>
		<?php
		$stAudiencia = $conn->prepare("SELECT * FROM audiencia_publica WHERE id_categoria = :id_categoria AND (id_subcategoria IS NULL OR id_subcategoria = '') AND status_registro = :status_registro ORDER BY titulo DESC");
		$stAudiencia->execute(array("id_categoria" => $categoria['id'], "status_registro" => "A"));
		$qryAudiencia = $stAudiencia->fetchAll();

		if(count($qryAudiencia)) {
		?>
		<ul>
			<li>
				<div class="panel-group" id="accordion_<?= $categoria['id'] ?>" role="tablist" aria-multiselectable="true">
					<?php foreach ($qryAudiencia as $audiencia_publica) { ?>
					<div class="panel panel-default">
					    <div class="panel-heading" role="tab" id="heading_<?= $audiencia_publica['id'] ?>">
					     	<h4 class="panel-title">
					        	<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?= $audiencia_publica['id'] ?>" aria-expanded="true" aria-controls="collapse_<?= $audiencia_publica['id'] ?>">
					          		<i class="glyphicon glyphicon-triangle-right"></i> <?= $audiencia_publica['titulo']?>
					        	</a>
					    	</h4>
					    </div>
						<div id="collapse_<?= $audiencia_publica['id'] ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_<?= $audiencia_publica['id'] ?>">
							<div class="panel-body">
								<?php if($audiencia_publica['data_inicio'] != "") { ?><strong>Data de In&iacute;cio:</strong> <?= formata_data($audiencia_publica['data_inicio']) ?><br><?php } ?>
								<?php if($audiencia_publica['data_fim'] != "") { ?><strong>Fim da Vig&ecirc;ncia:</strong> <?= formata_data($audiencia_publica['data_fim']) ?><br><?php } ?>
								<?php if($audiencia_publica['numero'] != "") { ?><strong>N&ordm; do Processo:</strong> <?= $audiencia_publica['numero'] ?><br><?php } ?>
								<?php if($audiencia_publica['valor'] != "") { ?><strong>Valor:</strong> <?= $audiencia_publica['valor'] ?><br><?php } ?>
								<?php if($audiencia_publica['contrapartida'] != "") { ?><strong>Contra partida:</strong> <?= $audiencia_publica['contrapartida'] ?><br><?php } ?>
								<?php if($audiencia_publica['proponente'] != "") { ?><strong>Proponente:</strong> <?= $audiencia_publica['proponente'] ?><br><?php } ?>
								<?php if($audiencia_publica['fornecedor'] != "") { ?><strong>Fornecedor:</strong> <?= $audiencia_publica['fornecedor'] ?><br><?php } ?>
								<?php if($audiencia_publica['objeto'] != "") { ?><strong>Objeto:</strong> <?= verifica($audiencia_publica['objeto']) ?><br><?php } ?>

								<?php
								$stAnexo = $conn->prepare("SELECT * FROM audiencia_publica_anexo WHERE id_artigo = :id_artigo ORDER BY ordem ASC, id DESC");
								$stAnexo->execute(array("id_artigo" => $audiencia_publica['id']));
								$qryAnexo = $stAnexo->fetchAll();

								if(count($qryAnexo)) {
								?>
								<div class="panel panel-primary">
									<div class="panel-heading">
								    	<h2 class="panel-title"><i class="glyphicon glyphicon-cloud-download"></i> Anexos</h2>
								  	</div>
									<div class="panel-body">
									<?php
									foreach ($qryAnexo as $anexo) {
									?>
										<p><strong><a href="<?= $CAMINHOARQ ?>/<?= $anexo['arquivo'] ?>" target="_blank"><i class="glyphicon glyphicon-cloud-download"></i> <?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?></a></strong></p>
									<?php } ?>
									</div>
								</div>
								<?php } ?>

								<?php
								$stLink = $conn->prepare("SELECT * FROM audiencia_publica_link WHERE id_artigo = :id_artigo ORDER BY ordem ASC, id DESC");
								$stLink->execute(array("id_artigo" => $audiencia_publica['id']));
								$qryLink = $stLink->fetchAll();

								if(count($qryLink)) {
								?>
								<div class="panel panel-primary">
									<div class="panel-heading">
								    	<h2 class="panel-title"><i class="glyphicon glyphicon-globe"></i> Links</h2>
								  	</div>
									<div class="panel-body">
									<?php
									foreach ($qryLink as $linkaudiencia_publica) {
									?>
										<p><strong><a href="<?= $linkaudiencia_publica['link'] ?>" target="_blank"><i class="glyphicon glyphicon-globe"></i> <?= empty($linkaudiencia_publica['descricao']) ? "Link" : $linkaudiencia_publica['descricao'] ?></a></strong></p>
									<?php } ?>
									</div>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>
					<?php } ?>
				</div>
			</li>
		</ul>
		<?php } ?>
		<?php
		$stSubcategoria = $conn->prepare("SELECT * FROM audiencia_publica_subcategoria WHERE id_categoria = :id_categoria AND status_registro = :status_registro ORDER BY id");
		$stSubcategoria->execute(array("id_categoria" => $categoria['id'], "status_registro" => "A"));
		$qrySubcategoria = $stSubcategoria->fetchAll();

		if(count($qrySubcategoria)) {
		?>
		<ul>
			<?php
			foreach ($qrySubcategoria as $subcategoria) {
			?>
			<li><a href="#"><?= $subcategoria['descricao'] ?></a>
				<?php
				$stAudiencia = $conn->prepare("SELECT * FROM audiencia_publica WHERE id_subcategoria = :id_subcategoria AND status_registro = :status_registro ORDER BY titulo DESC");
				$stAudiencia->execute(array("id_subcategoria" => $subcategoria['id'], "status_registro" => "A"));
				$qryAudiencia = $stAudiencia->fetchAll();

				if(count($qryAudiencia)) {
				?>
				<ul>
					<li>
						<div class="panel-group" id="accordion_<?= $subcategoria['id'] ?>" role="tablist" aria-multiselectable="true">
							<?php foreach ($qryAudiencia as $audiencia_publica) { ?>
							<div class="panel panel-default">
							    <div class="panel-heading" role="tab" id="heading_<?= $audiencia_publica['id'] ?>">
							     	<h4 class="panel-title">
							        	<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?= $audiencia_publica['id'] ?>" aria-expanded="true" aria-controls="collapse_<?= $audiencia_publica['id'] ?>">
							          		<i class="glyphicon glyphicon-triangle-right"></i> <?= $audiencia_publica['titulo'] ?>
							        	</a>
							    	</h4>
							    </div>
								<div id="collapse_<?= $audiencia_publica['id'] ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_<?= $audiencia_publica['id'] ?>">
									<div class="panel-body">
										<?php  if($audiencia_publica['artigo'] != "") { ?><strong>Artigo:</strong> <?= verifica($audiencia_publica['artigo']) ?><br><?php } ?>
										<?php
										$stAnexo = $conn->prepare("SELECT * FROM audiencia_publica_anexo WHERE id_artigo = :id_artigo ORDER BY ordem ASC, id DESC");
										$stAnexo->execute(array("id_artigo" => $audiencia_publica['id']));
										$qryAnexo = $stAnexo->fetchAll();

										if(count($qryAnexo)) {
										?>
										<div class="panel panel-primary">
											<div class="panel-heading">
										    	<h2 class="panel-title"><i class="glyphicon glyphicon-cloud-download"></i> Anexos</h2>
										  	</div>
											<div class="panel-body">
											<?php
											foreach ($qryAnexo as $anexo) {
											?>
												<p><strong><a href="<?= $CAMINHOARQ ?>/<?= $anexo['arquivo'] ?>" target="_blank"><i class="glyphicon glyphicon-cloud-download"></i> <?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?></a></strong></p>
											<?php } ?>
											</div>
										</div>
										<?php } ?>

										<?php
										$stLink = $conn->prepare("SELECT * FROM audiencia_publica_link WHERE id_artigo = :id_artigo ORDER BY ordem ASC, id DESC");
										$stLink->execute(array("id_artigo" => $audiencia_publica['id']));
										$qryLink = $stLink->fetchAll();

										if(count($qryLink)) {
										?>
										<div class="panel panel-primary">
											<div class="panel-heading">
										    	<h2 class="panel-title"><i class="glyphicon glyphicon-globe"></i> Links</h2>
										  	</div>
											<div class="panel-body">
											<?php
											foreach ($qryLink as $linkaudiencia_publica) {
											?>
												<p><strong><a href="<?= $linkaudiencia_publica['link'] ?>" target="_blank"><i class="glyphicon glyphicon-globe"></i> <?= empty($linkaudiencia_publica['descricao']) ? "Link" : $linkaudiencia_publica['descricao'] ?></a></strong></p>
											<?php } ?>
											</div>
										</div>
										<?php } ?>
									</div>
								</div>
							</div>
							<?php } ?>
						</div>
					</li>
				</ul>
				<?php } ?>
			</li>
			<?php } ?>
		</ul>
		<?php } ?>
	</li>
	<?php } ?>
</ul>
<?php
}

	$atualizacao = atualizacao("legislacao", $cliente, $conn);

	if($atualizacao != "") {
?>

<p class="text-right"><small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small></p>

<?php
	}
?>