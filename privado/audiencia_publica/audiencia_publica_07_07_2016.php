<h2>Audi&ecirc;ncia P&uacute;blica</h2>
<ol class="breadcrumb">
	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
	<li class="active">Audi&ecirc;ncia P&uacute;blica</li>
</ol>

<?php
$pagina = isset($_REQUEST['pagina']) ? $_REQUEST['pagina'] : 1;
$max = 10;
$inicio = $max * ($pagina - 1);

$link = $CAMINHO . "/index.php?sessao=" . $_GET['sessao'];

$sql = "SELECT * FROM audiencia_publica WHERE id_cliente = :id_cliente AND status_registro = :status_registro ";

$stLinha = $conn->prepare($sql);
$stLinha->execute(array("id_cliente" => $cliente, "status_registro" => "A"));
$qryLinha = $stLinha->fetchAll();
$totalLinha = count($qryLinha);

$sql .= "ORDER BY data_audiencia DESC, id DESC LIMIT $inicio, $max";

$stAudiencia = $conn->prepare($sql);
$stAudiencia->execute(array("id_cliente" => $cliente, "status_registro" => "A"));
$qryAudiencia = $stAudiencia->fetchAll();

if(count($qryAudiencia)) {
?>
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
	<?php foreach ($qryAudiencia as $audiencia) { ?>
	<div class="panel panel-default">
	    <div class="panel-heading" role="tab" id="heading_<?= $audiencia['id'] ?>">
	     	<h4 class="panel-title">
	        	<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?= $audiencia['id'] ?>" aria-expanded="true" aria-controls="collapse_<?= $audiencia['id'] ?>">
	          		<div class="row">
	          			<div class="col-sm-4"><i class="glyphicon glyphicon-triangle-right"></i> <strong><?= $audiencia['titulo'] ?></strong></div>
	          			<div class="col-sm-4"><strong>Publica&ccedil;&atilde;o:</strong> <?= formata_data_hora($audiencia['data_publicacao']) ?></div>
	          			<div class="col-sm-4"><strong>Audi&ecirc;ncia:</strong> <?= formata_data($audiencia['data_audiencia']) ?></div>
	          		</div>
	        	</a>
	    	</h4>
	    </div>
		<div id="collapse_<?= $audiencia['id'] ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_<?= $audiencia['id'] ?>">
			<div class="panel-body">
				<?= verifica($audiencia['artigo']) ?>

				<?php
				$stAnexo = $conn->prepare("SELECT * FROM audiencia_publica_anexo WHERE id_artigo = :id_artigo ORDER BY ordem ASC, id DESC");
				$stAnexo->execute(array("id_artigo" => $audiencia['id']));
				$qryAnexo = $stAnexo->fetchAll();

				if(count($qryAnexo)) {
				?>
				<div class="panel panel-primary">
					<div class="panel-heading">
				    	<h2 class="panel-title"><i class="glyphicon glyphicon-cloud-download"></i> Anexos</h2>
				  	</div>
					<div class="panel-body">
					<?php
					foreach ($qryAnexo as $anexo) {
					?>
						<p><strong><a href="<?= $CAMINHOARQ ?>/<?= $anexo['arquivo'] ?>" target="_blank"><i class="glyphicon glyphicon-cloud-download"></i> <?= empty($anexo['descricao']) ? "Anexo" : $anexo['descricao'] ?></a></strong></p>
					<?php } ?>
					</div>
				</div>
				<?php } ?>

				<?php
				$stLink = $conn->prepare("SELECT * FROM audiencia_publica_link WHERE id_artigo = :id_artigo ORDER BY ordem ASC, id DESC");
				$stLink->execute(array("id_artigo" => $audiencia['id']));
				$qryLink = $stLink->fetchAll();

				if(count($qryLink)) {
				?>
				<div class="panel panel-primary">
					<div class="panel-heading">
				    	<h2 class="panel-title"><i class="glyphicon glyphicon-globe"></i> Links</h2>
				  	</div>
					<div class="panel-body">
					<?php
					foreach ($qryLink as $linkAudiencia) {
					?>
						<p><strong><a href="<?= $linkAudiencia['link'] ?>" target="_blank"><i class="glyphicon glyphicon-globe"></i> <?= empty($linkAudiencia['descricao']) ? "Link" : $linkAudiencia['descricao'] ?></a></strong></p>
					<?php } ?>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
	<?php } ?>
</div>

<?php
$menos = $pagina - 1;
$mais = $pagina + 1;
$paginas = ceil($totalLinha / $max);
if($paginas > 1) {
?>
<nav>
	<ul class="pagination">
		<?php if($pagina == 1) { ?>
	    <li class="disabled"><a href="#" aria-label="Anterior"><span aria-hidden="true">&laquo;</span></a></li>
	    <?php } else { ?>
	    <li><a href="<?= $link ?>&pagina=<?= $menos ?>" aria-label="Anterior"><span aria-hidden="true">&laquo;</span></a></li>
	    <?php
		}

	    if(($pagina - 4) < 1) $anterior = 1;
	    else $anterior = $pagina - 4;

	    if(($pagina + 4) > $paginas) $posterior = $paginas;
	    else $posterior = $pagina + 4;

	    for($i = $anterior; $i <= $posterior; $i++) {

	    	if($i != $pagina) {
	    ?>
	    	<li><a href="<?= $link ?>&pagina=<?= $i ?>"><?= $i ?></a></li>
	    	<?php } else { ?>
	    	<li class="active"><a href="#"><?= $i ?><span class="sr-only">(current)</span></a></li>
	   	<?php
	    	}
	    }

	    if($mais <= $paginas) {
	   	?>
	   	<li><a href="<?= $link ?>&pagina=<?= $mais ?>" aria-label="Pr&oacute;ximo"><span aria-hidden="true">&raquo;</span></a></li>
	   	<?php } ?>
	</ul>
</nav>
<?php } ?>

<?php } else { ?>
<h4>Nenhum registro encontrado.</h4>
<?php
}

$atualizacao = atualizacao("audiencia_publica", $cliente, $conn);
if($atualizacao != "") {
?>
<p class="text-right"><small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small></p>
<?php
}