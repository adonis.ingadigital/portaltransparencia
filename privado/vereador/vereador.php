<h2>Vereadores</h2>
<ol class="breadcrumb">
	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
	<li class="active">Vereadores</li>
</ol>

<?php
$stVereador = $conn->prepare("SELECT * FROM vereador WHERE id_cliente = :id_cliente AND status_registro = :status_registro ORDER BY nome");
$stVereador->execute(array("id_cliente" => $cliente, "status_registro" => "A"));
$qryVereador = $stVereador->fetchAll();

if(count($qryVereador)) {
?>
<div class="table-responsive">
	<table class="table table-striped table-hover table-bordered table-condensed">
		<tr>
			<th>Nome</th>
			<th>Cargo</th>
			<th class="<?= ($cliente == '12141' ? 'hidden' : '') ?>">Sal&aacute;rio</th>
			<th>Anexo</th>
		</tr>
		<?php foreach ($qryVereador as $vereador) { ?>
		<tr>
			<td><?= $vereador['nome'] ?></td>
			<td><?= $vereador['cargo'] ?></td>
			<td class="<?= ($cliente == '12141' ? 'hidden' : '') ?>"><?= formata_valor($vereador['salario']) ?></td>
			<td>
				<?php if($vereador['arquivo'] != "") { ?>
				<a href="<?= $CAMINHOARQ ?>/<?= $vereador['arquivo'] ?>" class="btn btn-warning" data-toggle="tooltip" data-placement="left" title="Baixar arquivo" target="_blank">
					<?php if($cliente != 12141){ ?>
						<i class="glyphicon glyphicon-cloud-download"></i>
					<?php }else{ ?>
						<img src="<?= $CAMINHOARQ ?>/<?= $vereador['arquivo'] ?>"  />
					<?php } ?>
				</a>
				<?php } else { ?>
				<button type="button" class="btn btn-warning" disabled><i class="glyphicon glyphicon-cloud-download"></i></button>
				<?php } ?>
			</td>
		</tr>
		<?php } ?>
	</table>
</div>
<?php
}

$stCategoria = $conn->prepare("SELECT * FROM vereador_categoria WHERE id_cliente = :id_cliente AND status_registro = :status_registro ORDER BY id DESC");
$stCategoria->execute(array("id_cliente" => $cliente, "status_registro" => "A"));
$qryCategoria = $stCategoria->fetchAll();

if(count($qryCategoria)) {
	?>
<ul class="treeview">
	<?php
	foreach ($qryCategoria as $categoria) {
	?>
	<li><a href="#"><?= $categoria['descricao'] ?></a>
		<?php
		$stArquivo = $conn->prepare("SELECT * FROM vereador_anexo_link WHERE id_categoria = :id_categoria AND status_registro = :status_registro ORDER BY titulo DESC");
		$stArquivo->execute(array("id_categoria" => $categoria['id'], "status_registro" => "A"));
		$qryArquivo = $stArquivo->fetchAll();

		if(count($qryArquivo)) {
		?>
		<ul>
			<?php
			foreach($qryArquivo as $arquivo) {

				if($arquivo['arquivo'] != "") {

					$linkDocumento = $CAMINHOARQ . "/" . $arquivo['arquivo'];
					$imagem = "glyphicon-cloud-download";

				} else if($arquivo['link'] != "") {

					$linkDocumento = $arquivo['link'];
					$imagem = "glyphicon-globe";
				}
			?>

						<li><a href="<?=$linkDocumento ?>" target="_blank"><i class="glyphicon <?= $imagem ?>"></i> <?= $arquivo['titulo'] ?></a></li>

				<?php } ?>


		</ul>
		<?php } ?>
	</li>
	<?php } ?>
</ul>
<?php
}

$atualizacao = atualizacao("vereadores", $cliente, $conn);
if($atualizacao != "") {
?>
<p class="text-right"><small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small></p>
<?php
}
