<h2>SOBRE O PORTAL</h2>

<ol class="breadcrumb">
	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
	<li class="active">Sobre o Portal</li>
</ol>

<h3><?= !empty($configuracaoTransparencia['sobre_titulo']) ? $configuracaoTransparencia['sobre_titulo'] : "O Portal da Transpar&ecirc;ncia do Poder Executivo  Municipal &eacute; um instrumento de controle social  que possibilita ao cidad&atilde;o acompanhar a  arrecada&ccedil;&atilde;o das receitas e a aplica&ccedil;&atilde;o dos  recursos p&uacute;blicos." ?></h3>
<?php
if(!empty($configuracaoTransparencia['sobre_artigo']) && $configuracaoTransparencia['sobre_artigo'] != "<br>") {

	echo verifica($configuracaoTransparencia['sobre_artigo']);

} else {

	echo "<p>O portal disponibiliza diariamente informa&ccedil;&otilde;es, obtidas do Sistema Integrado de Planejamento e Gest&atilde;o Fiscal (SIGEF), sobre as receitas, os gastos na manuten&ccedil;&atilde;o dos servi&ccedil;os p&uacute;blicos e os investimentos realizados.</p>";
	echo "<p>Nele &eacute; poss&iacute;vel obter informa&ccedil;&otilde;es detalhadas sobre os pagamentos efetuados aos fornecedores, pagamentos de di&aacute;rias aos servidores, conv&ecirc;nios e repasses aos munic&iacute;pios, subven&ccedil;&otilde;es sociais, al&eacute;m de outras informa&ccedil;&otilde;es de interesse da sociedade.</p>";
}