<?php if($cliente == "11999" or $cliente == "12128" or $cliente == "12064" or $cliente == "12126" or $cliente =="12101") { ?>
	<h2>Adiantamento</h2>
	<ol class="breadcrumb">
		<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
		<li class="active">Adiantamento</li>
	</ol>
<?php } else if($cliente == "12094") { ?>
	<h2>Adiantamento Para Viagens ou Desembolso</h2>
	<ol class="breadcrumb">
		<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
		<li class="active">Adiantamento Para Viagens ou Desembolso</li>
	</ol>
<?php } else if($cliente == "12066")  { ?>
	<h2>Adiantamento de Viagens</h2>
	<ol class="breadcrumb">
		<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
		<li class="active">Adiantamento de Viagens</li>
	</ol>
<?php } else { ?>
	<h2>Adiantamento de Di&aacute;rias</h2>
	<ol class="breadcrumb">
		<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
		<li class="active">Adiantamento de Di&aacute;rias</li>
	</ol>
<?php } ?>

<?php if($cliente == "11999" or $cliente == "12128" or $cliente == "12126") { ?>
	<p>&nbsp;</p>
<?php } else { ?>	
	<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#form-adiantamento" aria-expanded="false" aria-controls="form-adiantamento">
	<i class="glyphicon glyphicon-search"></i> Pesquisar
</button>
<?php } ?> 

<p class="clearfix"></p>

<form class="form-horizontal collapse" id="form-adiantamento" action="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$adiantamento.$complemento); ?>" method="post">
	<div class="form-group">
		<label for="titulo" class="col-sm-2 control-label">T&iacute;tulo</label>
		<div class="col-sm-10">
			<input type="text" name="titulo" id="titulo" class="form-control" value="<?= $_POST['titulo'] ?>" placeholder="Informe o t&iacute;tulo do adiantamento">
		</div>
	</div>
	<div class="form-group">
		<label for="beneficiario" class="col-sm-2 control-label">Benefici&aacute;rio</label>
		<div class="col-sm-10">
			<input type="text" name="beneficiario" id="beneficiario" class="form-control" value="<?= $_POST['beneficiario'] ?>" placeholder="Informe o nome do benefici&aacute;rio...">
		</div>
	</div>
	<div class="form-group">
		<label for="rg_beneficiario" class="col-sm-2 control-label">RG do Benefici&aacute;rio</label>
		<div class="col-sm-10">
			<input type="text" name="rg_beneficiario" id="rg_beneficiario" class="form-control" value="<?= $_POST['rg_beneficiario'] ?>" placeholder="Informe o RG do benefici&aacute;rio...">
		</div>
	</div>
	<div class="form-group">
		<label for="data_inicio_viagem" class="col-sm-2 control-label">Data Inicial da Viagem</label>
		<div class="col-sm-10">
			<input type="text" name="data_inicio_viagem" id="data_inicio_viagem" class="form-control data" value="<?= $_POST['data_inicio_viagem'] ?>" placeholder="Informe a data inicial da viagem...">
		</div>
	</div>
	<div class="form-group">
		<label for="data_fim_viagem" class="col-sm-2 control-label">Data Final da Viagem</label>
		<div class="col-sm-10">
			<input type="text" name="data_fim_viagem" id="data_fim_viagem" class="form-control data" value="<?= $_POST['data_fim_viagem'] ?>" placeholder="Informe a data final da viagem...">
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<input type="hidden" name="acao" value="cadastrar">
			<button type="reset" class="btn btn-danger">Limpar</button>
			<button type="submit" class="btn btn-primary">Enviar</button>
		</div>
	</div>
</form>

<?php
$sqlCategoria = "SELECT * FROM adiantamento_categoria WHERE id_cliente = :id_cliente AND status_registro = :status_registro ";
$sqlDiariaSemCategoria = "SELECT * FROM adiantamento WHERE id_categoria = :id_categoria AND (id_subcategoria IS NULL OR id_subcategoria = '') AND status_registro = :status_registro ";
$sqlSubcategoria = "SELECT * FROM adiantamento_subcategoria WHERE id_categoria = :id_categoria AND status_registro = :status_registro ";
$sqlDiaria = "SELECT * FROM adiantamento WHERE id_subcategoria = :id_subcategoria AND status_registro = :status_registro ";
$vetor['status_registro'] = "A";

if($_POST) {

	$sqlCategoria .= "AND id IN (SELECT id_categoria FROM adiantamento WHERE status_registro = :status_registro ";
	$sqlSubcategoria .= "AND id IN (SELECT id_subcategoria FROM adiantamento WHERE status_registro = :status_registro ";
	$sqlBusca = "";

	if($_POST['titulo'] != "") {

		$sqlBusca .= "AND titulo LIKE :titulo ";
		$vetor["titulo"] = "%" . secure($_POST['titulo']) . "%";

	}

	if($_POST['beneficiario'] != "") {

		$sqlBusca .= "AND beneficiario LIKE :beneficiario ";
		$vetor["beneficiario"] = "%" . secure($_POST['beneficiario']) . "%";

	}

	if($_POST['rg_beneficiario'] != "") {

		$sqlBusca .= "AND (rg_beneficiario LIKE :rg_beneficiario OR rg_beneficiario LIKE :rg_beneficiario2) ";
		$vetor["rg_beneficiario"] = "%" . secure($_POST['rg_beneficiario']) . "%";
		$vetor["rg_beneficiario2"] = "%" . secure(preg_replace('#[^0-9]#', '', $_POST['rg_beneficiario'])) . "%";

	}

	if($_POST['data_inicio_viagem'] != "") {

		$sqlBusca .= "AND data_inicio_viagem = :data_inicio_viagem ";
		$vetor['data_inicio_viagem'] = formata_data_banco($_POST['data_inicio_viagem']);

	}

	if($_POST['data_fim_viagem'] != "") {

		$sqlBusca .= "AND data_fim_viagem = :data_fim_viagem ";
		$vetor['data_fim_viagem'] = formata_data_banco($_POST['data_fim_viagem']);

	}

	$sqlCategoria .= "$sqlBusca) ";
	$sqlDiariaSemCategoria .= $sqlBusca;
	$sqlSubcategoria .= "$sqlBusca) ";
	$sqlDiaria .= $sqlBusca;

}

$stCategoria = $conn->prepare("$sqlCategoria ORDER BY id DESC");
$vetorCat = $vetor;
$vetorCat['id_cliente'] = $cliente;
$stCategoria->execute($vetorCat);
$qryCategoria = $stCategoria->fetchAll();

if(count($qryCategoria)) {
?>
<ul class="treeview">
	<?php
	foreach ($qryCategoria as $categoria) {
	?>
	<li><a href="#"><?= $categoria['descricao'] ?></a>
		<?php
		$stDiaria = $conn->prepare("$sqlDiariaSemCategoria ORDER BY titulo" . ($cliente == "43" ? " DESC" : " ASC"));
		$vetorDiariaSemCategoria = $vetor;
		$vetorDiariaSemCategoria['id_categoria'] = $categoria['id'];
		$stDiaria->execute($vetorDiariaSemCategoria);
		$qryDiaria = $stDiaria->fetchAll();

		if(count($qryDiaria)) {
		?>
		<ul>
			<?php
			foreach($qryDiaria as $adiantamento) {

				if($adiantamento['link'] != "") {

					$linkAdiantamento = $adiantamento['link'];
					$targetDiaria = "_blank";
					$imagem = "glyphicon-globe";

				} else if($adiantamento['arquivo'] != "") {

					$linkAdiantamento = $CAMINHOARQ . "/" . $adiantamento['arquivo'];
					$targetDiaria = "_blank";
					$imagem = "glyphicon-cloud-download";

				} else {

					$linkAdiantamento = "#";
					$targetDiaria = "_self";
					$imagem = "glyphicon glyphicon-chevron-right";

				}
			?>
			<li>
				<a href="<?= $linkAdiantamento ?>" target="_blank"><span class="text-primary"><i class="glyphicon <?= $imagem ?>"></i>&nbsp;&nbsp;<?= $adiantamento['titulo'] ?></span></a>
				<?php if($adiantamento['beneficiario'] != "") { ?><strong style="margin-left: 22px;">Benefici&aacute;rio:</strong> <?= $adiantamento['beneficiario'] ?><br><?php } ?>
                <?php if($adiantamento['rg_beneficiario'] != "") { ?><strong style="margin-left: 22px;">RG do Benefici&aacute;rio:</strong> <?= $adiantamento['rg_beneficiario'] ?><br><?php } ?>
                <?php if($adiantamento['justificativa'] != "") { ?><strong style="margin-left: 22px;">Justificativa:</strong> <?= $adiantamento['justificativa'] ?><br><?php } ?>
                <?php if($adiantamento['data_inicio_viagem'] != "") { ?><strong style="margin-left: 22px;">Data Inicial da Viagem:</strong> <?= formata_data($adiantamento['data_inicio_viagem']) ?><br><?php } ?>
                <?php if($adiantamento['data_fim_viagem'] != "") { ?><strong style="margin-left: 22px;">Data Final da Viagem:</strong> <?= formata_data($adiantamento['data_fim_viagem']) ?><br><?php } ?>
                <?php if($adiantamento['meio_transporte'] != "") { ?><strong style="margin-left: 22px;">Meio de Transporte:</strong> <?= $adiantamento['meio_transporte'] ?><br><?php } ?>
                <?php if($adiantamento['custo_meio_transporte'] != "") { ?><strong style="margin-left: 22px;">Custo do Meio de Transporte:</strong> <?= $adiantamento['custo_meio_transporte'] ?><br><?php } ?>
                <?php if($adiantamento['quantidade_adiantamento'] != "") { ?><strong style="margin-left: 22px;">Quantidade Unit&aacute;ria de Adiantamento:</strong> <?= $adiantamento['quantidade_adiantamento'] ?><br><?php } ?>
                <?php if($adiantamento['valor_unitario_adiantamento'] != "") { ?><strong style="margin-left: 22px;">Valor Unit&aacute;rio de Adiantamento:</strong> <?= $adiantamento['valor_unitario_adiantamento'] ?><br><?php } ?>
			</li>
			<?php } ?>
		</ul>
		<?php } ?>
		<?php
		$stSubcategoria = $conn->prepare("$sqlSubcategoria ORDER BY id DESC");
		$vetorSubcategoria = $vetor;
		$vetorSubcategoria['id_categoria'] = $categoria['id'];
		$stSubcategoria->execute($vetorSubcategoria);
		$qrySubcategoria = $stSubcategoria->fetchAll();

		if(count($qrySubcategoria)) {
		?>
		<ul>
			<?php
			foreach ($qrySubcategoria as $subcategoria) {
			?>
			<li><a href="#"><?= $subcategoria['descricao'] ?></a>
				<?php
				$stDiaria = $conn->prepare("$sqlDiaria ORDER BY titulo" . ($cliente == "43" ? " DESC" : " ASC"));
				$vetorDiaria = $vetor;
				$vetorDiaria['id_subcategoria'] = $subcategoria['id'];
				$stDiaria->execute($vetorDiaria);
				$qryDiaria = $stDiaria->fetchAll();

				if(count($qryDiaria)) {
				?>
				<ul>
					<?php
					foreach($qryDiaria as $adiantamento) {

						if($adiantamento['link'] != "") {

							$linkAdiantamento = $adiantamento['link'];
							$targetDiaria = "_blank";
							$imagem = "glyphicon-globe";

						} else if($adiantamento['arquivo'] != "") {

							$linkAdiantamento = $CAMINHOARQ . "/" . $adiantamento['arquivo'];
							$targetDiaria = "_blank";
							$imagem = "glyphicon-cloud-download";

						} else {

							$linkAdiantamento = "#";
							$targetDiaria = "_self";
							$imagem = "glyphicon glyphicon-chevron-right";

						}
					?>
					<li>
						<a href="<?= $linkAdiantamento ?>" target="_blank"><span class="text-primary"><i class="glyphicon <?= $imagem ?>"></i>&nbsp;&nbsp;<?= $adiantamento['titulo'] ?></span></a><br>
						<?php if($adiantamento['beneficiario'] != "") { ?><strong style="margin-left: 22px;">Benefici&aacute;rio:</strong> <?= $adiantamento['beneficiario'] ?><br><?php } ?>
		                <?php if($adiantamento['rg_beneficiario'] != "") { ?><strong style="margin-left: 22px;">RG do Benefici&aacute;rio:</strong> <?= $adiantamento['rg_beneficiario'] ?><br><?php } ?>
		                <?php if($adiantamento['justificativa'] != "") { ?><strong style="margin-left: 22px;">Justificativa:</strong> <?= $adiantamento['justificativa'] ?><br><?php } ?>
		                <?php if($adiantamento['data_inicio_viagem'] != "") { ?><strong style="margin-left: 22px;">Data Inicial da Viagem:</strong> <?= formata_data($adiantamento['data_inicio_viagem']) ?><br><?php } ?>
		                <?php if($adiantamento['data_fim_viagem'] != "") { ?><strong style="margin-left: 22px;">Data Final da Viagem:</strong> <?= formata_data($adiantamento['data_fim_viagem']) ?><br><?php } ?>
		                <?php if($adiantamento['meio_transporte'] != "") { ?><strong style="margin-left: 22px;">Meio de Transporte:</strong> <?= $adiantamento['meio_transporte'] ?><br><?php } ?>
		                <?php if($adiantamento['custo_meio_transporte'] != "") { ?><strong style="margin-left: 22px;">Custo do Meio de Transporte:</strong> <?= $adiantamento['custo_meio_transporte'] ?><br><?php } ?>
		                <?php if($adiantamento['quantidade_adiantamento'] != "") { ?><strong style="margin-left: 22px;">Quantidade Unit&aacute;ria de Adiantamento:</strong> <?= $adiantamento['quantidade_adiantamento'] ?><br><?php } ?>
		                <?php if($adiantamento['valor_unitario_adiantamento'] != "") { ?><strong style="margin-left: 22px;">Valor Unit&aacute;rio de Adiantamento:</strong> <?= $adiantamento['valor_unitario_adiantamento'] ?><br><?php } ?>
					</li>
					<?php } ?>
				</ul>
				<?php } ?>
			</li>
			<?php } ?>
		</ul>
		<?php } ?>
	</li>
	<?php } ?>
</ul>
<?php
}

$atualizacao = atualizacao("adiantamento", $cliente, $conn);
if($atualizacao != "") {
?>
<p class="text-right"><small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small></p>
<?php
}