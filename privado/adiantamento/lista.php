<?php 
	$ordenacao = secure($_POST['ordenacao']);
	$ordem = secure($_POST['ordem']);
	if(empty($ordem) || $ordem == NULL) {
		$ordem = 1;
	}
	if(empty($ordenacao) || $ordenacao == NULL) {
		$ordenacao = 1;
	}
	if($cliente == 12144) $coluna = "titulo DESC";
		else{
			$ordem = ($ordem == 1) ? "ASC" : "DESC";
			$coluna = ($ordenacao == 1) ? "data_inicio_viagem" : "titulo";
			$coluna .= " " . $ordem;
	}
?>

<h2>Adiantamento</h2>
<ol class="breadcrumb">
	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
	<li class="active">Adiantamento</li>
</ol>


<?php if($cliente == "11999" or $cliente == "12128" or $cliente == "12126" or $cliente == "12141" or $cliente == "1162") { ?>
	<p>&nbsp;</p>
<?php } else { ?>	
	<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#form-adiantamento" aria-expanded="false" aria-controls="form-adiantamento">
	<i class="glyphicon glyphicon-search"></i> Pesquisar
</button>
<?php } ?> 

<p class="clearfix"></p>

<?php

	if($cliente != "1162"){

?>

<form class="form-horizontal collapse" id="form-adiantamento" action="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$adiantamento.$complemento); ?>" method="post">
	<div class="form-group">
		<label for="titulo" class="col-sm-2 control-label">T&iacute;tulo</label>
		<div class="col-sm-10">
			<input type="text" name="titulo" id="titulo" class="form-control" value="<?= $_POST['titulo'] ?>" placeholder="Informe o t&iacute;tulo do adiantamento">
		</div>
	</div>
	<div class="form-group">
		<label for="beneficiario" class="col-sm-2 control-label">Benefici&aacute;rio</label>
		<div class="col-sm-10">
			<input type="text" name="beneficiario" id="beneficiario" class="form-control" value="<?= $_POST['beneficiario'] ?>" placeholder="Informe o nome do benefici&aacute;rio...">
		</div>
	</div>
	<div class="form-group">
		<label for="rg_beneficiario" class="col-sm-2 control-label">RG do Benefici&aacute;rio</label>
		<div class="col-sm-10">
			<input type="text" name="rg_beneficiario" id="rg_beneficiario" class="form-control" value="<?= $_POST['rg_beneficiario'] ?>" placeholder="Informe o RG do benefici&aacute;rio...">
		</div>
	</div>
	<div class="form-group">
		<label for="data_inicio_viagem" class="col-sm-2 control-label">Data Inicial da Viagem</label>
		<div class="col-sm-10">
			<input type="text" name="data_inicio_viagem" id="data_inicio_viagem" class="form-control data" value="<?= $_POST['data_inicio_viagem'] ?>" placeholder="Informe a data inicial da viagem...">
		</div>
	</div>
	<div class="form-group">
		<label for="data_fim_viagem" class="col-sm-2 control-label">Data Final da Viagem</label>
		<div class="col-sm-10">
			<input type="text" name="data_fim_viagem" id="data_fim_viagem" class="form-control data" value="<?= $_POST['data_fim_viagem'] ?>" placeholder="Informe a data final da viagem...">
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<input type="hidden" name="acao" value="cadastrar">
			<button type="reset" class="btn btn-danger">Limpar</button>
			<button type="submit" class="btn btn-primary">Enviar</button>
		</div>
	</div>
</form>

<form id="form-ordenacao" action="<?= $CAMINHO ?>/index.php?sessao=<?= verifica($sequencia.$adiantamento.$complemento); ?>" method="post">
	<div class="row">
		<div class="col-sm-12 col-md-6 col-lg-6">
			<div class="form-group">
				<label for="ordenacao">Selecione a ordenação:</label>
				<select name="ordenacao" onchange="$('#form-ordenacao').submit();" class="form-control" id="ordenacao">
					<option value="1" <?= ($ordenacao == 1) ? 'selected' : '' ?> >Por data</option>
					<option value="2" <?= ($ordenacao == 2) ? 'selected' : '' ?> >Por titulo</option>
				</select>
			</div>
		</div>
		<div class="col-sm-12 col-md-6 col-lg-6">
			<div class="form-group">
				<label for="ordem">Selecione a ordem:</label>
				<select name="ordem" onchange="$('#form-ordenacao').submit();" class="form-control" id="ordem">
					<option value="1" <?= ($ordem == 1) ? 'selected' : '' ?> >Ordem Crescente</option>
					<option value="2" <?= ($ordem == 2) ? 'selected' : '' ?> >Ordem Decrescente</option>
				</select>
			</div>
		</div>
	</div>
	<!-- <input type="hidden" name="acao" value="ordenar"> -->
	<!-- <button type="submit" class="btn btn-info">Ordenar</button> -->
  </form>

<?
	}
?>

<?php
$sqlCategoria = "SELECT * FROM adiantamento_categoria WHERE id_cliente = :id_cliente AND status_registro = :status_registro ";
$sqlDiariaSemCategoria = "SELECT * FROM adiantamento WHERE id_categoria = :id_categoria AND (id_subcategoria IS NULL OR id_subcategoria = '') AND status_registro = :status_registro ";
$sqlSubcategoria = "SELECT * FROM adiantamento_subcategoria WHERE id_categoria = :id_categoria AND status_registro = :status_registro ";
$sqlDiaria = "SELECT * FROM adiantamento WHERE id_subcategoria = :id_subcategoria AND status_registro = :status_registro ";
$vetor['status_registro'] = "A";

if($_POST) {

	$sqlCategoria .= "AND id IN (SELECT id_categoria FROM adiantamento WHERE status_registro = :status_registro ";
	$sqlSubcategoria .= "AND id IN (SELECT id_subcategoria FROM adiantamento WHERE status_registro = :status_registro ";
	$sqlBusca = "";

	if($_POST['titulo'] != "") {

		$sqlBusca .= "AND titulo LIKE :titulo ";
		$vetor["titulo"] = "%" . secure($_POST['titulo']) . "%";

	}

	if($_POST['beneficiario'] != "") {

		$sqlBusca .= "AND beneficiario LIKE :beneficiario ";
		$vetor["beneficiario"] = "%" . secure($_POST['beneficiario']) . "%";

	}

	if($_POST['rg_beneficiario'] != "") {

		$sqlBusca .= "AND (rg_beneficiario LIKE :rg_beneficiario OR rg_beneficiario LIKE :rg_beneficiario2) ";
		$vetor["rg_beneficiario"] = "%" . secure($_POST['rg_beneficiario']) . "%";
		$vetor["rg_beneficiario2"] = "%" . secure(preg_replace('#[^0-9]#', '', $_POST['rg_beneficiario'])) . "%";

	}

	if($_POST['data_inicio_viagem'] != "") {

		$sqlBusca .= "AND data_inicio_viagem = :data_inicio_viagem ";
		$vetor['data_inicio_viagem'] = formata_data_banco($_POST['data_inicio_viagem']);

	}

	if($_POST['data_fim_viagem'] != "") {

		$sqlBusca .= "AND data_fim_viagem = :data_fim_viagem ";
		$vetor['data_fim_viagem'] = formata_data_banco($_POST['data_fim_viagem']);

	}

	$sqlCategoria .= "$sqlBusca) ";
	$sqlDiariaSemCategoria .= $sqlBusca;
	$sqlSubcategoria .= "$sqlBusca) ";
	$sqlDiaria .= $sqlBusca;

}

$stCategoria = $conn->prepare("$sqlCategoria ORDER BY id DESC");
$vetorCat = $vetor;
$vetorCat['id_cliente'] = $cliente;
$stCategoria->execute($vetorCat);
$qryCategoria = $stCategoria->fetchAll();

if(count($qryCategoria)) {
?>
<ul class="treeview">
	<?php
	foreach ($qryCategoria as $categoria) {
	?>
	<li><a href="#"><?= $categoria['descricao'] ?></a>
		<?php
		$stDiaria = $conn->prepare("$sqlDiariaSemCategoria ORDER BY $coluna");
		$vetorDiariaSemCategoria = $vetor;
		$vetorDiariaSemCategoria['id_categoria'] = $categoria['id'];
		$stDiaria->execute($vetorDiariaSemCategoria);
		$qryDiaria = $stDiaria->fetchAll();

		if(count($qryDiaria)) {
		?>
		<ul>
			<?php
			foreach($qryDiaria as $adiantamento) {

				if($adiantamento['link'] != "") {

					$linkAdiantamento = $adiantamento['link'];
					$targetDiaria = "_blank";
					$imagem = "glyphicon-globe";

				} else if($adiantamento['arquivo'] != "") {

					$linkAdiantamento = $CAMINHOARQ . "/" . $adiantamento['arquivo'];
					$targetDiaria = "_blank";
					$imagem = "glyphicon-cloud-download";

				} else {

					$linkAdiantamento = "#";
					$targetDiaria = "_self";
					$imagem = "glyphicon glyphicon-chevron-right";

				}
			?>
			<li>
				<a href="<?= $linkAdiantamento ?>" target="_blank">
					<span class="text-primary">
						<i class="glyphicon <?= $imagem ?>"></i>&nbsp;&nbsp;<?= $adiantamento['titulo'] ?>
					</span>
				</a>
				<?php if($adiantamento['beneficiario'] != "") { ?>
					<strong style="margin-left: 22px;">
						<span style="color:#3c3c3c !important;">
							Benefici&aacute;rio:&nbsp;
						</span>
					</strong>
					<?= $adiantamento['beneficiario'] ?>
					<br>
				<?php } ?>
                <?php if($adiantamento['rg_beneficiario'] != "") { ?>
					<strong style="margin-left: 22px;">
						<span style="color:#3c3c3c !important;">
							RG do Benefici&aacute;rio:&nbsp;
						</span>
					</strong>
					<?= $adiantamento['rg_beneficiario'] ?>
					<br>
				<?php } ?>
                <?php if($adiantamento['justificativa'] != "") { ?>
					<strong style="margin-left: 22px;">
						<span style="color:#3c3c3c !important;">
							Justificativa:&nbsp;
						</span>
					</strong>
					<?= $adiantamento['justificativa'] ?>
					<br>
				<?php } ?>
                <?php if($adiantamento['data_inicio_viagem'] != "") { ?>
					<strong style="margin-left: 22px;">
						<span style="color:#3c3c3c !important;">
							Data Inicial da Viagem:&nbsp;
						</span>
					</strong>
					<?= formata_data($adiantamento['data_inicio_viagem']) ?>
					<br>
				<?php } ?>
                <?php if($adiantamento['data_fim_viagem'] != "") { ?>
					<strong style="margin-left: 22px;">
						<span style="color:#3c3c3c !important;">
							Data Final da Viagem:&nbsp;
						</span>
					</strong>
					<?= formata_data($adiantamento['data_fim_viagem']) ?>
					<br>
				<?php } ?>
                <?php if($adiantamento['meio_transporte'] != "") { ?>
					<strong style="margin-left: 22px;">
						<span style="color:#3c3c3c !important;">
							Meio de Transporte:&nbsp;
						</span>
					</strong>
					<?= $adiantamento['meio_transporte'] ?>
					<br>
				<?php } ?>
                <?php if($adiantamento['custo_meio_transporte'] != "") { ?>
					<strong style="margin-left: 22px;">
						<span style="color:#3c3c3c !important;">
							Custo do Meio de Transporte:&nbsp;
						</span>
					</strong>
					<?= $adiantamento['custo_meio_transporte'] ?>
					<br>
				<?php } ?>
                <?php if($adiantamento['quantidade_adiantamento'] != "") { ?>
					<strong style="margin-left: 22px;">
						<span style="color:#3c3c3c !important;">
							Quantidade Unit&aacute;ria de Adiantamento:&nbsp;
						</span>
					</strong>
					<?= $adiantamento['quantidade_adiantamento'] ?>
					<br>
				<?php } ?>
                <?php if($adiantamento['valor_unitario_adiantamento'] != "") { ?>
					<strong style="margin-left: 22px;">
						<span style="color:#3c3c3c !important;">
							Valor Unit&aacute;rio de Adiantamento:&nbsp;
						</span>
					</strong>
					<?= $adiantamento['valor_unitario_adiantamento'] ?>
					<br>
				<?php } ?>
			</li>
			<?php } ?>
		</ul>
		<?php } ?>
		<?php
		$stSubcategoria = $conn->prepare("$sqlSubcategoria ORDER BY descricao ASC");
		$vetorSubcategoria = $vetor;
		$vetorSubcategoria['id_categoria'] = $categoria['id'];
		$stSubcategoria->execute($vetorSubcategoria);
		$qrySubcategoria = $stSubcategoria->fetchAll();

		if(count($qrySubcategoria)) {
		?>
		<ul>
			<?php
			foreach ($qrySubcategoria as $subcategoria) {
			?>
			<li><a href="#"><?= $subcategoria['descricao'] ?></a>
				<?php
				$stDiaria = $conn->prepare("$sqlDiaria ORDER BY $coluna");
				$vetorDiaria = $vetor;
				$vetorDiaria['id_subcategoria'] = $subcategoria['id'];
				$stDiaria->execute($vetorDiaria);
				$qryDiaria = $stDiaria->fetchAll();

				if(count($qryDiaria)) {
				?>
				<ul>
					<?php
					foreach($qryDiaria as $adiantamento) {

						if($adiantamento['link'] != "") {

							$linkAdiantamento = $adiantamento['link'];
							$targetDiaria = "_blank";
							$imagem = "glyphicon-globe";

						} else if($adiantamento['arquivo'] != "") {

							$linkAdiantamento = $CAMINHOARQ . "/" . $adiantamento['arquivo'];
							$targetDiaria = "_blank";
							$imagem = "glyphicon-cloud-download";

						} else {

							$linkAdiantamento = "#";
							$targetDiaria = "_self";
							$imagem = "glyphicon glyphicon-chevron-right";

						}
					?>
					<li>
						<a href="<?= $linkAdiantamento ?>" target="_blank">
							<span class="text-primary"><i class="glyphicon <?= $imagem ?>"></i>&nbsp;&nbsp;<?= $adiantamento['titulo'] ?></span>
						</a>
						<br>
						<?php if($adiantamento['beneficiario'] != "") { ?>
							<strong style="margin-left: 22px;">
								<span style="color:#3c3c3c !important;">
									Benefici&aacute;rio:&nbsp;
								</span>
							</strong>
							<?= $adiantamento['beneficiario'] ?>
							<br>
						<?php } ?>
		                <?php if($adiantamento['rg_beneficiario'] != "") { ?>
							<strong style="margin-left: 22px;">
								<span style="color:#3c3c3c !important;">
									RG do Benefici&aacute;rio:&nbsp;
								</span>
							</strong>
							<?= $adiantamento['rg_beneficiario'] ?>
							<br>
						<?php } ?>
		                <?php if($adiantamento['justificativa'] != "") { ?>
							<strong style="margin-left: 22px;">
								<span style="color:#3c3c3c !important;">
									<span style="color:#3c3c3c !important;">
								</span>
									Justificativa:&nbsp;
								</span>
							</strong>
							<?= $adiantamento['justificativa'] ?>
							<br>
						<?php } ?>
		                <?php if($adiantamento['data_inicio_viagem'] != "") { ?>
							<strong style="margin-left: 22px;">
								<span style="color:#3c3c3c !important;">
									Data Inicial da Viagem:&nbsp;
								</span>
							</strong>
							<?= formata_data($adiantamento['data_inicio_viagem']) ?>
							<br>
						<?php } ?>
		                <?php if($adiantamento['data_fim_viagem'] != "") { ?>
							<strong style="margin-left: 22px;">
								<span style="color:#3c3c3c !important;">
									Data Final da Viagem:&nbsp;
								</span>
							</strong>
							<?= formata_data($adiantamento['data_fim_viagem']) ?>
							<br>
						<?php } ?>
		                <?php if($adiantamento['meio_transporte'] != "") { ?>
							<strong style="margin-left: 22px;">
								<span style="color:#3c3c3c !important;">
									Meio de Transporte:&nbsp;
								</span>
							</strong>
							<?= $adiantamento['meio_transporte'] ?>
							<br>
						<?php } ?>
		                <?php if($adiantamento['custo_meio_transporte'] != "") { ?>
							<strong style="margin-left: 22px;">
								<span style="color:#3c3c3c !important;">
									Custo do Meio de Transporte:&nbsp;
								</span>
							</strong>
							<?= $adiantamento['custo_meio_transporte'] ?>
							<br>
						<?php } ?>
		                <?php if($adiantamento['quantidade_adiantamento'] != "") { ?>
							<strong style="margin-left: 22px;">
								<span style="color:#3c3c3c !important;">
									Quantidade Unit&aacute;ria de Adiantamento:&nbsp;
								</span>
							</strong>
							<?= $adiantamento['quantidade_adiantamento'] ?>
							<br>
						<?php } ?>
		                <?php if($adiantamento['valor_unitario_adiantamento'] != "") { ?>
							<strong style="margin-left: 22px;">
								<span style="color:#3c3c3c !important;">
									Valor Unit&aacute;rio de Adiantamento:&nbsp;
								</span>
							</strong>
							<?= $adiantamento['valor_unitario_adiantamento'] ?>
							<br>
						<?php } ?>
					</li>
					<?php } ?>
				</ul>
				<?php } ?>
			</li>
			<?php } ?>
		</ul>
		<?php } ?>
	</li>
	<?php } ?>
</ul>
<?php
}

$atualizacao = atualizacao("adiantamento", $cliente, $conn);
if($atualizacao != "") {
?>
<p class="text-right"><small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small></p>
<?php
}