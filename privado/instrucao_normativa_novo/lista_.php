<h2>Instru&ccedil;&atilde;o Normativa</h2>
	<ol class="breadcrumb">
		<li>
			<a href="<?=$CAMINHO ?>">In&iacute;cio</a>
		</li>
		<li class="active">Instru&ccedil;&atilde;o Normativa</li>
	</ol>

	<?php
		$stCategoria = $conn->prepare("SELECT *
										FROM instrucao_normativa_categoria
										WHERE id_cliente = :id_cliente
										AND status_registro = :status_registro
										ORDER BY id DESC
		");
		$stCategoria->execute(array("id_cliente" => $cliente, "status_registro" => "A"));
		$qryCategoria = $stCategoria->fetchAll();
		if(count($qryCategoria)) { ?>
			<ul class="treeview">
				<?php
					foreach ($qryCategoria as $categoria) { ?>
						<li>
							<a class="h4" href="#">
								<?=$categoria['descricao'] ?>
							</a>
							<?php
								$stArquivo = $conn->prepare("SELECT * 
																FROM instrucao_normativa 
																WHERE id_categoria = :id_categoria 
																AND (id_subcategoria IS NULL OR id_subcategoria = '') 
																AND status_registro = :status_registro 
																ORDER BY titulo DESC");
								$stArquivo->execute(array("id_categoria" => $categoria['id'], "status_registro" => "A"));
								$qryArquivo = $stArquivo->fetchAll();
							if(count($qryArquivo)) {
								?>
								<ul>
									<?php
										foreach($qryArquivo as $arquivo) {
											if($arquivo['arquivo'] != "") {
												$linkDocumento = $CAMINHOARQ . "/" . $arquivo['arquivo'];
												$imagem = "glyphicon-cloud-download";
												$titulo = empty($arquivo['titulo']) ? "Anexo" : $arquivo['titulo'];
											} else if($arquivo['link'] != "") {
												$linkDocumento = $arquivo['link'];
												$imagem = "glyphicon-globe";
												$titulo = empty($arquivo['titulo']) ? "Link" : $arquivo['titulo'];
											}
											?>
											<li>
												<div style="padding: 10px; border: 1px solid lightgrey; max-width:90%; margin: 10px 0px 20px 0px;">
													<div style="padding: 5px; background-color: #f5f5f5;">
															<div class="h4">
																<?=$arquivo['titulo'] ?>
															</div>
													</div>
														<hr style="margin-top:10px !important;">
														<p>
															<?=str_replace("&quot;", "", $arquivo['artigo']) ?>
														</p>
														<a href="<?=$linkDocumento ?>" target="_blank" style="text-decoration: none;color: #0028c1;">
															<i class="glyphicon <?= $imagem ?>"></i> <?= $titulo ?>
														</a>
												</div>
											</li>
										<?php }
										?>
								</ul>
							<?php }
							?>
							<?php 
								$stSubcategoria = $conn->prepare("SELECT *
																	FROM instrucao_normativa_subcategoria
																	WHERE id_categoria = :id_categoria 
																	AND status_registro = :status_registro
																	ORDER BY id DESC
																	");
								$stSubcategoria->execute(array("id_categoria" => $categoria['id'], "status_registro" => "A"));
								$qrySubcategoria = $stSubcategoria->fetchAll();
								if(count($qrySubcategoria)) { ?>
									<ul>
										<?php
											foreach ($qrySubcategoria as $subcategoria) {
												if($subcategoria['descricao'] != "") { ?>
													<li>
													<a class="h4" href="#">
															<?=$subcategoria['descricao'] ?>
													</a>

													<?php
														$stArquivo = $conn->prepare("SELECT * 
																						FROM instrucao_normativa 
																						WHERE id_categoria = :id_categoria 
																						AND id_subcategoria = :id_subcategoria
																						AND status_registro = :status_registro 
																						ORDER BY titulo DESC");
														$stArquivo->execute(array("id_categoria" => $categoria['id'], "id_subcategoria" => $subcategoria['id'], "status_registro" => "A"));
														$qryArquivo = $stArquivo->fetchAll();
													if(count($qryArquivo)) {
														?>
														<ul>
															<?php
																foreach($qryArquivo as $arquivo) {
																	if($arquivo['arquivo'] != "") {
																		$linkDocumento = $CAMINHOARQ . "/" . $arquivo['arquivo'];
																		$imagem = "glyphicon-cloud-download";
																		$titulo = empty($arquivo['titulo']) ? "Anexo" : $arquivo['titulo'];
																	} else if($arquivo['link'] != "") {
																		$linkDocumento = $arquivo['link'];
																		$imagem = "glyphicon-globe";
																		$titulo = empty($arquivo['titulo']) ? "Link" : $arquivo['titulo'];
																	}
																	?>
																	<li>
																		<div style="padding: 10px; border: 1px solid lightgrey; max-width:90%; margin: 10px 0px 20px 0px;">
																			<div style="padding: 5px; background-color: #f5f5f5;">
																					<div class="h4">
																						<?=$arquivo['titulo'] ?>
																					</div>
																			</div>
																			<hr style="margin-top:10px !important;">
																			<p>
																				<?=str_replace("&quot;", "", $arquivo['artigo']) ?>
																			</p>
																			<a href="<?=$linkDocumento ?>" target="_blank" style="text-decoration: none;color: #0028c1;">
																				<i class="glyphicon <?= $imagem ?>"></i> <?= $titulo ?>
																			</a>
																		</div>
																	</li>
																<?php }
																?>
														</ul>
													<?php }
													?>
												<?php } 
												?>
											<?php } 
										?>
									</ul>
								<?php } ?>
						</li>
				<?php } ?>
			</ul>
		<?php } ?>