<h2>Emails Oficiais</h2>
<ol class="breadcrumb">
	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
	<li class="active">Emails Oficiais</li>
</ol>


	<?php
	$stArquivo = $conn->prepare("SELECT * FROM geral_email WHERE id_cliente = :id_cliente AND status_registro = :status_registro ORDER BY titulo ASC");
	$stArquivo->execute(array("id_cliente" => $cliente ,"status_registro" => "A"));
	$qryArquivo = $stArquivo->fetchAll();
	echo "<div class='panel-group'>";
	foreach ($qryArquivo as $key => $arquivo){ ?>
		<div class="panel panel-default">
			<div class="panel-body"><a href="mailto:<?=$arquivo['email']?>" target="_blank"><i class="glyphicon glyphicon-envelope"></i> <strong><?= $arquivo['titulo'] ?></strong> - <?= $arquivo['email']?></a></div>
		</div>
	<?php }?>
	</div>
<?php

$atualizacao = atualizacao("acesso_informacao", $cliente, $conn);
if($atualizacao != "") {
?>
<p class="text-right"><small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small></p>
<?php
}