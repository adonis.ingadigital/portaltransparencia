<h2>Links &Uacute;teis</h2>
<ol class="breadcrumb">
	<li><a href="<?=$CAMINHO ?>">In&iacute;cio</a></li>
	<li class="active">Links &Uacute;teis</li>
</ol>

<?php
$stLink = $conn->prepare("SELECT * FROM link WHERE id_cliente = :id_cliente AND status_registro = :status_registro ORDER BY id DESC");
$stLink->execute(array("id_cliente" => $cliente, "status_registro" => "A"));
$qryLink = $stLink->fetchAll();

if(count($qryLink)) {
?>
<table class="table table-hover table-striped">
	<?php foreach ($qryLink as $link) { ?>
	<tr>
		<td>
			<a href="<?= $link['link'] ?>" target="_blank"><i class="glyphicon glyphicon-globe"></i> <strong><?= $link['titulo'] ?></strong></a>
		</td>
	</tr>
	<?php } ?>
</table>
<?php
}

$atualizacao = atualizacao("link_util", $cliente, $conn);
if($atualizacao != "") {
?>
<p class="text-right"><small><strong><i class="glyphicon glyphicon-time"></i> &Uacute;ltima atualiza&ccedil;&atilde;o:</strong> <?= $atualizacao ?></small></p>
<?php
}